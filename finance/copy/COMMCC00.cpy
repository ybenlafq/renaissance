      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *************************************************************             
      *    COMMAREA CONTROLE DE CAISSE                                          
      **************************************************************            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-CC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES AIDA ------------------------------------- 100          
     1    02 FILLER-COM-AIDA      PIC  X(100).                                  
      * ZONES RESERVEES CICS ------------------------------------- 020          
   101    02 COMM-CICS-APPLID     PIC  X(008).                                  
   109    02 COMM-CICS-NETNAM     PIC  X(008).                                  
   117    02 COMM-CICS-TRANSA     PIC  X(004).                                  
      * DATE DU JOUR --------------------------------------------- 100          
   121    02 COMM-DATE-SIECLE     PIC  9(002).                                  
   123    02 COMM-DATE-ANNEE      PIC  9(002).                                  
   125    02 COMM-DATE-MOIS       PIC  9(002).                                  
   127    02 COMM-DATE-JOUR       PIC  9(002).                                  
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
   129    02 COMM-DATE-QNTA       PIC  9(003).                                  
   132    02 COMM-DATE-QNT0       PIC  9(005).                                  
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
   137    02 COMM-DATE-BISX       PIC  9(001).                                  
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
   138    02 COMM-DATE-JSM        PIC  9(001).                                  
      *   LIBELLES DU JOUR COURT - LONG                                         
   139    02 COMM-DATE-JSM-LC     PIC  X(003).                                  
   142    02 COMM-DATE-JSM-LL     PIC  X(008).                                  
      *   LIBELLES DU MOIS COURT - LONG                                         
   150    02 COMM-DATE-MOIS-LC    PIC  X(002).                                  
   153    02 COMM-DATE-MOIS-LL    PIC  X(008).                                  
      *   DIFFERENTES FORMES DE DATE                                            
   161    02 COMM-DATE-SSAAMMJJ   PIC  X(008).                                  
   169    02 COMM-DATE-AAMMJJ     PIC  X(006).                                  
   175    02 COMM-DATE-JJMMSSAA   PIC  X(008).                                  
   183    02 COMM-DATE-JJMMAA     PIC  X(006).                                  
   189    02 COMM-DATE-JJ-MM-AA   PIC  X(008).                                  
   197    02 COMM-DATE-JJ-MM-SSAA PIC  X(010).                                  
      *   TRAITEMENT NUMERO DE SEMAINE                                          
          02 COMM-DATE-WEEK.                                                    
   207       05 COMM-DATE-SEMSS   PIC  9(002).                                  
   209       05 COMM-DATE-SEMAA   PIC  9(002).                                  
   211       05 COMM-DATE-SEMNU   PIC  9(002).                                  
   213    02 COMM-DATE-FILLER     PIC  X(008).                                  
      * ATTRIBUTS BMS======================================== 4 + AAAA          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   221*   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
   223    02 COMM-CC00-ZMAP       PIC  X(001).                                  
      * ZONES APPLICATIVES CC00 GENERALES  ===M================== BBBB          
      *                                                                         
      *   02 COMM-CC00.                                                         
      *      03 COMM-CC00-ENTITE        PIC X(5).                               
      *      03 COMM-CC00-LENTITE       PIC X(30).                              
      *      03 COMM-CC00-SOCIETE       PIC X(5).                               
      *      03 COMM-CC00-LSOCIETE      PIC X(30).                              
      *      03 COMM-CC00-ETABL         PIC X(3).                               
      *      03 COMM-CC00-LETABL        PIC X(30).                              
      *      03 COMM-CC00-AUX           PIC XXX.                                
      *      03 COMM-CC00-LAUX          PIC X(30).                              
      *      03 COMM-CC00-FOUR          PIC X(08).                              
      *      03 COMM-CC00-LFOUR         PIC X(30).                              
          02 COMM-CC00-CACID.                                                   
  227           04 COMM-CC00-CSERVICE   PIC X(4).                               
  231           04 FILLER               PIC X(4).                               
      ******* PAGINATION ***********************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  233 *   02 COMM-CC00-PAGE      PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-PAGE      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  235 *   02 COMM-CC00-NBP       PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-NBP       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  237 *   02 COMM-CC00-PAGE-B    PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-PAGE-B    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  239 *   02 COMM-CC00-NBP-B     PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-NBP-B     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  241 *   02 COMM-CC00-PAGE-T    PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-PAGE-T    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  243 *   02 COMM-CC00-NBP-T     PIC S9(4) COMP.                                
      *--                                                                       
          02 COMM-CC00-NBP-T     PIC S9(4) COMP-5.                              
      *}                                                                        
      ******* NIVEAU D'ACCES *******************************************        
  248     02 COMM-CC00-NOM-PROG  PIC X(5).                                      
      ******* EXERCICE/PERIODE******************************************        
      *      03 COMM-CC00-EXERCICE  PIC X(4).                                   
      *      03 COMM-CC00-PERIODE   PIC X(2).                                   
  251     02 COMM-CC00-DEVISE    PIC X(3).                                      
      ******* AFFICHAGE REGLEMENTS - TRANSACTION FT56 ******************        
      *      03 COMM-CC00-REG-CBANQUE      PIC X(5).                            
      *      03 COMM-CC00-REG-METHREG      PIC X(3).                            
      *      03 COMM-CC00-REG-NREG         PIC X(7).                            
      *      03 COMM-CC00-REG-NEXER        PIC X(4).                            
      *      03 COMM-CC00-REG-RPROG        PIC X(5).                            
      *      03 COMM-CC00-REG-PAGE         PIC S9(4) COMP.                      
      *      03 COMM-CC00-REG-NBP          PIC S9(4) COMP.                      
      *      03 COMM-CC00-REG-CDEVISE      PIC X(3).                            
      *      03 COMM-CC00-CIMP             PIC X(04).                           
      ******* CONTROLE SUPPRESSION PAR PF21 ****************************        
      *      03 COMM-CC00-PF21      PIC X.                                      
      ******* DEVISE DE BASE *******************************************        
  276      02 COMM-CC00-LDEVISE   PIC X(25).                                    
  277      02 COMM-CC00-NBDECIM   PIC 9.                                        
      ******* TRANSACTION DE RETOUR POUR PARAMETRAGE (DEPUIS TFT91) ****        
  282      02 COMM-CC00-RETOUR        PIC X(05).                                
      ******* RETOURS DES IMPRESSIONS IMMEDIATES    ********************        
      *      03 COMM-CC00-EG60-MESS     PIC X(79).                              
      *      03 COMM-CC00-EG60-CDRET    PIC X(4).                               
      ******* GCT2: PLANS DE COMPTE, SECTION, RUBRIQUE *****************        
      *      03 COMM-CC00-CPCG          PIC X(4).                               
      *      03 COMM-CC00-CPDS          PIC X(4).                               
      *      03 COMM-CC00-CPDR          PIC X(4).                               
      ******* GCT2: MENU FTSOC DYNAMIQUE : GESTION DE PAGE *************        
      *      03 COMM-CC00-PAGE-S        PIC S9(4) COMP.                         
      *      03 COMM-CC00-NBP-S         PIC S9(4) COMP.                         
      ******* PRE-SAISIE : REFERENCE DE PRE-SAISIE ********************         
  297     02 COMM-CC00-REFDOC        PIC X(15).                                 
      ******* PASSAGE AU LETTRAGE A 10 POSITIONS : REMPLACE DANS LES  *         
      ******* PROGRAMMES CONCERNES LE CHAMP  COMM-FT43-LETTRAGE       *         
      *      03 COMM-CC00-LETTRAGE      PIC X(10).                              
AD    *      03 COMM-CC00-LETTRAGE-MAX  PIC X(10).                              
      ******* GCT2: ANCIEN PLAN DE SECTION, RUBRIQUE *******************01271400
      *      03 COMM-CC00-ANCCPDS       PIC X(4).                       01272300
      *      03 COMM-CC00-ANCCPDR       PIC X(4).                       01272300
      *      03 FILLER                  PIC X(10).                              
      ******* RETOUR POUR FTV1, FTV5, FT21, FT60, FT94 *****************        
      *      03 COMM-CC00-RPROG         PIC X(05).                              
      *      03 FILLER                  PIC X(43).                              
          02 FILLER                     PIC X(799).                             
      * ZONES APPLICATIVES CC00 ****************************************        
          02 COMM-CC00-REDEFINES        PIC X(3000).                            
          02 COMM-CC00-APPLI REDEFINES COMM-CC00-REDEFINES.                     
      *----------------------------------------------------------------*        
            03 COMM-CC00-LACID                  PIC X(30).                      
            03 FILLER                           PIC X(20).                      
            03 COMM-CC11.                                                       
               05 COMM-CC11-NSOCIETE            PIC X(03).                      
               05 COMM-CC11-NLIEU               PIC X(03).                      
               05 COMM-CC11-DCAISSE             PIC X(08).                      
      * "COMM-CC13" EST UTILISE PAR TCC12 , TCC13 ET TCC14                      
            03 COMM-CC13.                                                       
               05 COMM-CC13-NSOCIETE            PIC X(03).                      
               05 COMM-CC13-NLIEU               PIC X(03).                      
               05 COMM-CC13-DCAIDEB             PIC X(08).                      
               05 COMM-CC13-DCAIFIN             PIC X(08).                      
               05 COMM-CC13-DRERDEB             PIC X(08).                      
               05 COMM-CC13-DRERFIN             PIC X(08).                      
               05 COMM-CC13-DREADEB             PIC X(08).                      
               05 COMM-CC13-DREAFIN             PIC X(08).                      
               05 COMM-CC13-DCAISSE             PIC X(10).                      
               05 FILLER                        PIC X(01).                      
      *      05 FILLER                          PIC X(27).                      
            03 COMM-CC21.                                                       
               05 COMM-CC21-NSOCIETE            PIC X(03).                      
               05 COMM-CC21-NLIEU               PIC X(03).                      
               05 COMM-CC21-NCAISSE             PIC X(03).                      
               05 FILLER                        PIC X(41).                      
            03 COMM-CC22.                                                       
               05 COMM-CC22-AUTO-ECART          PIC X(01).                      
               05 COMM-CC22-AUTO-TOUS-CODE      PIC X(01).                      
               05 FILLER                        PIC X(48).                      
      * "COMM-CC23" EST UTILISE PAR TCC23 ET TCC24                              
            03 COMM-CC23.                                                       
               05 COMM-CC23-RPROG               PIC X(05).                      
               05 COMM-CC23-NSOCIETE            PIC X(03).                      
               05 COMM-CC23-NLIEU               PIC X(03).                      
               05 COMM-CC23-LLIEU               PIC X(20).                      
               05 COMM-CC23-NSOCCOMPT           PIC X(05).                      
               05 COMM-CC23-NLIEUCOMPT          PIC X(03).                      
               05 COMM-CC23-NCAISSE             PIC X(03).                      
               05 COMM-CC23-DCAISSE             PIC X(08).                      
               05 COMM-CC23-LCPAIMT             PIC X(20).                      
               05 COMM-CC23-NCOMPTE             PIC X(08).                      
               05 COMM-CC23-AUTORISE            PIC X(01).                      
               05 COMM-CC23-RAZ.                                                
                  07 COMM-CC23-MODE             PIC 9.                          
                  07 COMM-CC23-CALENDRIER OCCURS 3.                             
                     09 COMM-CC23-EXERCICE      PIC X(04).                      
                     09 COMM-CC23-PERIODE       PIC X(02).                      
                     09 COMM-CC23-DDEBUT        PIC X(08).                      
                     09 COMM-CC23-DFIN          PIC X(08).                      
                  07 COMM-CC23-NEXER            PIC X(04).                      
                  07 COMM-CC23-NPER             PIC X(02).                      
                  07 COMM-CC23-DCOMPTABLE       PIC X(08).                      
                  07 COMM-CC23-MESSAGE          PIC X(01).                      
               05 COMM-CC23-CPAICPT             PIC X(03).                      
               05 FILLER                        PIC X(56).                      
            03 COMM-CC25.                                                       
               05 COMM-CC25-NSOCIETE            PIC X(03).                      
               05 COMM-CC25-NLIEU               PIC X(03).                      
               05 COMM-CC25-LLIEU               PIC X(20).                      
               05 COMM-CC25-DDEB                PIC X(10).                      
               05 COMM-CC25-DFIN                PIC X(10).                      
               05 COMM-CC25-WSAISIE             PIC X(01).                      
               05 COMM-CC25-CPAICPT             PIC X(03).                      
               05 COMM-CC25-LPAICPT             PIC X(20).                      
               05 FILLER                        PIC X(30).                      
            03 COMM-FTS5.                                                       
               05 COMM-FTS5-LSOCIETE            PIC X(30).                      
               05 COMM-FTS5-LETABL              PIC X(30).                      
               05 COMM-FTS5-ENTITE              PIC X(05).                      
               05 COMM-FTS5-JOURNAL             PIC X(03).                      
               05 COMM-FTS5-NATURE              PIC X(03).                      
               05 COMM-FTS5-CPCG                PIC X(03).                      
               05 COMM-FTS5-CPDS                PIC X(03).                      
               05 COMM-FTS5-CPDR                PIC X(03).                      
               05 COMM-FTS5-TAB-CPTE OCCURS 9.                                  
                  07 COMM-FTS5-TAB-CPT         PIC X(8).                        
                                                                                
