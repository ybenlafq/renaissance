      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *******************************************************                   
      * DEFINITION DE LA TS UTILISEE DANS TFG11 ET TFG14                        
      *******************************************************                   
          02 TS14-NOM.                                                          
             03 FILLER             PIC X(4) VALUE 'FG14'.                       
             03 TS14-TERM          PIC X(4) VALUE SPACES.                       
          02 TS14-DATA.                                                         
             03 TS14-LIGNE-DATA    OCCURS 8.                                    
                05 TS14-FLAG-ECRIT-I   PIC X(01).                               
                05 TS14-FLAG-ECRIT-F   PIC X(01).                               
                05 TS14-FLAG-MODIF     PIC X(01).                               
                05 TS14-NLIGNE         PIC X(03).                               
                05 TS14-NLIGNE-9       REDEFINES TS14-NLIGNE PIC 9(03).         
                05 TS14-NSEQ           PIC X(02).                               
                05 TS14-NSEQ-9         REDEFINES TS14-NSEQ   PIC 9(02).         
                05 TS14-LCOMMENT       PIC X(70).                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
78 *8 *   02 TS14-LONG           PIC S9(4) COMP VALUE +624.                     
      *                                                                         
      *--                                                                       
          02 TS14-LONG           PIC S9(4) COMP-5 VALUE +624.                   
                                                                                
      *}                                                                        
