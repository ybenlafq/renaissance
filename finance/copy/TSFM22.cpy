      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM22-LONG             PIC S9(4) COMP-3 VALUE +77.                 
       01  TS-FM22-RECORD.                                                      
           05 TS-FM22-FONCTION   PIC X(5).                                      
           05 TS-FM22-CDEVISE    PIC X(3).                                      
           05 TS-FM22-LDEVISE    PIC X(25).                                     
           05 TS-FM22-NBDECIM    PIC 9.                                         
           05 TS-FM22-LDEV       PIC X(5).                                      
           05 TS-FM22-LSUB       PIC X(5).                                      
           05 TS-FM22-LSUBDIVIS  PIC X(25).                                     
           05 TS-FM22-CCAISSE    PIC X.                                         
           05 TS-FM22-WEURO      PIC X.                                         
           05 TS-FM22-CDEVISENUM PIC 9(3).                                      
                                                                                
