      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFTI00 AU 07/08/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,01,BI,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,05,BI,A,                          *        
      *                           33,02,BI,A,                          *        
      *                           35,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFTI00.                                                        
            05 NOMETAT-IFTI00           PIC X(6) VALUE 'IFTI00'.                
            05 RUPTURES-IFTI00.                                                 
           10 IFTI00-NENTITE            PIC X(05).                      007  005
           10 IFTI00-NSOCIETE           PIC X(05).                      012  005
           10 IFTI00-CINTERFACE         PIC X(05).                      017  005
           10 IFTI00-CTYPE              PIC X(01).                      022  001
           10 IFTI00-CTYPOPER           PIC X(05).                      023  005
           10 IFTI00-CNATOPER           PIC X(05).                      028  005
           10 IFTI00-CODE               PIC X(02).                      033  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFTI00-SEQUENCE           PIC S9(04) COMP.                035  002
      *--                                                                       
           10 IFTI00-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFTI00.                                                   
           10 IFTI00-ANOD               PIC X(01).                      037  001
           10 IFTI00-ANOF               PIC X(01).                      038  001
           10 IFTI00-ANOG               PIC X(01).                      039  001
           10 IFTI00-CPAYS              PIC X(03).                      040  003
           10 IFTI00-CRUBRIQUE1         PIC X(05).                      043  005
           10 IFTI00-CRUBRIQUE2         PIC X(05).                      048  005
           10 IFTI00-CRUBRIQUE3         PIC X(05).                      053  005
           10 IFTI00-CSECTION1          PIC X(05).                      058  005
           10 IFTI00-CSECTION2          PIC X(05).                      063  005
           10 IFTI00-CSECTION3          PIC X(05).                      068  005
           10 IFTI00-CTVA               PIC X(01).                      073  001
           10 IFTI00-DEFFET             PIC X(08).                      074  008
           10 IFTI00-LIBANO             PIC X(25).                      082  025
           10 IFTI00-LINTERFACE         PIC X(25).                      107  025
           10 IFTI00-NLIEU              PIC X(03).                      132  003
           10 IFTI00-NSOC               PIC X(03).                      135  003
           10 IFTI00-WGROUPE            PIC X(01).                      138  001
           10 IFTI00-NBANOMALIES        PIC S9(06)      COMP-3.         139  004
           10 IFTI00-WTAUXTVA           PIC S9(03)V9(2) COMP-3.         143  003
            05 FILLER                      PIC X(367).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFTI00-LONG           PIC S9(4)   COMP  VALUE +145.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFTI00-LONG           PIC S9(4) COMP-5  VALUE +145.           
                                                                                
      *}                                                                        
