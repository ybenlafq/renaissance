      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPS01   EPS01                                              00000020
      ***************************************************************** 00000030
       01   EPS01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGETL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGETF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
           02 MLIGNEI OCCURS   16 TIMES .                               00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MXI     PIC X.                                          00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGCPLTL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGCPLTF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCGCPLTI     PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNGCPLTL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNGCPLTF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNGCPLTI     PIC X(8).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNLIEUI      PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNVENTEI     PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCLIL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNOMCLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNOMCLIF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNOMCLII     PIC X(15).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDVENTEL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDVENTEF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDVENTEI     PIC X(10).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCMARQI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EPS01   EPS01                                              00000880
      ***************************************************************** 00000890
       01   EPS01O REDEFINES EPS01I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(2).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGETA   PIC X.                                          00001140
           02 MPAGETC   PIC X.                                          00001150
           02 MPAGETP   PIC X.                                          00001160
           02 MPAGETH   PIC X.                                          00001170
           02 MPAGETV   PIC X.                                          00001180
           02 MPAGETO   PIC X(2).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNSOCA    PIC X.                                          00001210
           02 MNSOCC    PIC X.                                          00001220
           02 MNSOCP    PIC X.                                          00001230
           02 MNSOCH    PIC X.                                          00001240
           02 MNSOCV    PIC X.                                          00001250
           02 MNSOCO    PIC X(3).                                       00001260
           02 MLIGNEO OCCURS   16 TIMES .                               00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MXA     PIC X.                                          00001290
             03 MXC     PIC X.                                          00001300
             03 MXP     PIC X.                                          00001310
             03 MXH     PIC X.                                          00001320
             03 MXV     PIC X.                                          00001330
             03 MXO     PIC X.                                          00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MCGCPLTA     PIC X.                                     00001360
             03 MCGCPLTC     PIC X.                                     00001370
             03 MCGCPLTP     PIC X.                                     00001380
             03 MCGCPLTH     PIC X.                                     00001390
             03 MCGCPLTV     PIC X.                                     00001400
             03 MCGCPLTO     PIC X(5).                                  00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNGCPLTA     PIC X.                                     00001430
             03 MNGCPLTC     PIC X.                                     00001440
             03 MNGCPLTP     PIC X.                                     00001450
             03 MNGCPLTH     PIC X.                                     00001460
             03 MNGCPLTV     PIC X.                                     00001470
             03 MNGCPLTO     PIC X(8).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNLIEUA      PIC X.                                     00001500
             03 MNLIEUC PIC X.                                          00001510
             03 MNLIEUP PIC X.                                          00001520
             03 MNLIEUH PIC X.                                          00001530
             03 MNLIEUV PIC X.                                          00001540
             03 MNLIEUO      PIC X(3).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MNVENTEA     PIC X.                                     00001570
             03 MNVENTEC     PIC X.                                     00001580
             03 MNVENTEP     PIC X.                                     00001590
             03 MNVENTEH     PIC X.                                     00001600
             03 MNVENTEV     PIC X.                                     00001610
             03 MNVENTEO     PIC X(7).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MNOMCLIA     PIC X.                                     00001640
             03 MNOMCLIC     PIC X.                                     00001650
             03 MNOMCLIP     PIC X.                                     00001660
             03 MNOMCLIH     PIC X.                                     00001670
             03 MNOMCLIV     PIC X.                                     00001680
             03 MNOMCLIO     PIC X(15).                                 00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MDVENTEA     PIC X.                                     00001710
             03 MDVENTEC     PIC X.                                     00001720
             03 MDVENTEP     PIC X.                                     00001730
             03 MDVENTEH     PIC X.                                     00001740
             03 MDVENTEV     PIC X.                                     00001750
             03 MDVENTEO     PIC X(10).                                 00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNCODICA     PIC X.                                     00001780
             03 MNCODICC     PIC X.                                     00001790
             03 MNCODICP     PIC X.                                     00001800
             03 MNCODICH     PIC X.                                     00001810
             03 MNCODICV     PIC X.                                     00001820
             03 MNCODICO     PIC X(7).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCFAMA  PIC X.                                          00001850
             03 MCFAMC  PIC X.                                          00001860
             03 MCFAMP  PIC X.                                          00001870
             03 MCFAMH  PIC X.                                          00001880
             03 MCFAMV  PIC X.                                          00001890
             03 MCFAMO  PIC X(5).                                       00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MCMARQA      PIC X.                                     00001920
             03 MCMARQC PIC X.                                          00001930
             03 MCMARQP PIC X.                                          00001940
             03 MCMARQH PIC X.                                          00001950
             03 MCMARQV PIC X.                                          00001960
             03 MCMARQO      PIC X(5).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(78).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
