      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CCLET CARTES CADEAUX LETTRAGE          *        
      *----------------------------------------------------------------*        
       01  RVCCLET .                                                            
           05  CCLET-CTABLEG2    PIC X(15).                                     
           05  CCLET-CTABLEG2-REDEF REDEFINES CCLET-CTABLEG2.                   
               10  CCLET-NOM             PIC X(05).                             
           05  CCLET-WTABLEG     PIC X(80).                                     
           05  CCLET-WTABLEG-REDEF  REDEFINES CCLET-WTABLEG.                    
               10  CCLET-LETTRAGE        PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCCLET-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCLET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CCLET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCLET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CCLET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
