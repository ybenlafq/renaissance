      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM16-LONG             PIC S9(4) COMP-3 VALUE +58.                 
       01  TS-FM16-RECORD.                                                      
           10 TS-FM16-NDEMANDE      PIC X(2).                                   
           10 TS-FM16-DCREATION     PIC X(10).                                  
           10 TS-FM16-CACID         PIC X(8).                                   
           10 TS-FM16-USER          PIC X(25).                                  
           10 TS-FM16-WMFICHE       PIC X.                                      
           10 TS-FM16-WPAPIER       PIC X.                                      
           10 TS-FM16-NETAB         PIC X(3).                                   
           10 TS-FM16-NJOURNALMIN   PIC X(03).                                  
           10 TS-FM16-NJOURNALMAX   PIC X(03).                                  
           10 TS-FM16-WJOURNALGAL   PIC X.                                      
           10 TS-FM16-WTTRIECRIT    PIC X.                                      
                                                                                
