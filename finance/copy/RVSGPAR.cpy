      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SGPAR PARAMETRAGE PAIE                 *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSGPAR .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSGPAR .                                                            
      *}                                                                        
           05  SGPAR-CTABLEG2    PIC X(15).                                     
           05  SGPAR-CTABLEG2-REDEF REDEFINES SGPAR-CTABLEG2.                   
               10  SGPAR-CPROG           PIC X(05).                             
           05  SGPAR-WTABLEG     PIC X(80).                                     
           05  SGPAR-WTABLEG-REDEF  REDEFINES SGPAR-WTABLEG.                    
               10  SGPAR-WFLAG           PIC X(01).                             
               10  SGPAR-QPVUNIT         PIC X(03).                             
               10  SGPAR-COMMENT         PIC X(15).                             
               10  SGPAR-WFLAG2          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSGPAR-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSGPAR-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SGPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SGPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
