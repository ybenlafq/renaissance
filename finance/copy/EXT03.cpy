      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT03   EXT03                                              00000020
      ***************************************************************** 00000030
       01   EXT03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCETATI   PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNPAGEI   PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNBPAGESI      PIC X(2).                                  00000270
           02 M73I OCCURS   15 TIMES .                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQPROL    COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MWSEQPROL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSEQPROF    PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MWSEQPROI    PIC X(6).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRODL      COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLPRODL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLPRODF      PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLPRODI      PIC X(11).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR1L     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MAGRPR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR1F     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MAGRPR1I     PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR2L     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MAGRPR2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR2F     PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MAGRPR2I     PIC X(5).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR3L     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MAGRPR3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR3F     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MAGRPR3I     PIC X(5).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR4L     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MAGRPR4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR4F     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MAGRPR4I     PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR5L     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MAGRPR5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR5F     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MAGRPR5I     PIC X(5).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR6L     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MAGRPR6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR6F     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MAGRPR6I     PIC X(5).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR7L     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MAGRPR7L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR7F     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MAGRPR7I     PIC X(5).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR8L     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MAGRPR8L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR8F     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MAGRPR8I     PIC X(5).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPR9L     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MAGRPR9L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPR9F     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MAGRPR9I     PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAGRPRAL     COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MAGRPRAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAGRPRAF     PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MAGRPRAI     PIC X(5).                                  00000760
      * MESSAGE ERREUR                                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      * CODE TRANSACTION                                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(15).                                      00000900
      * CICS DE TRAVAIL                                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      * NETNAME                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      * CODE TERMINAL                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(5).                                       00001050
      ***************************************************************** 00001060
      * SDF: EXT03   EXT03                                              00001070
      ***************************************************************** 00001080
       01   EXT03O REDEFINES EXT03I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
      * DATE DU JOUR                                                    00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
      * HEURE                                                           00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCETATA   PIC X.                                          00001280
           02 MCETATC   PIC X.                                          00001290
           02 MCETATP   PIC X.                                          00001300
           02 MCETATH   PIC X.                                          00001310
           02 MCETATV   PIC X.                                          00001320
           02 MCETATO   PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNPAGEA   PIC X.                                          00001350
           02 MNPAGEC   PIC X.                                          00001360
           02 MNPAGEP   PIC X.                                          00001370
           02 MNPAGEH   PIC X.                                          00001380
           02 MNPAGEV   PIC X.                                          00001390
           02 MNPAGEO   PIC ZZ.                                         00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNBPAGESA      PIC X.                                     00001420
           02 MNBPAGESC PIC X.                                          00001430
           02 MNBPAGESP PIC X.                                          00001440
           02 MNBPAGESH PIC X.                                          00001450
           02 MNBPAGESV PIC X.                                          00001460
           02 MNBPAGESO      PIC ZZ.                                    00001470
           02 M73O OCCURS   15 TIMES .                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MWSEQPROA    PIC X.                                     00001500
             03 MWSEQPROC    PIC X.                                     00001510
             03 MWSEQPROP    PIC X.                                     00001520
             03 MWSEQPROH    PIC X.                                     00001530
             03 MWSEQPROV    PIC X.                                     00001540
             03 MWSEQPROO    PIC Z(6).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MLPRODA      PIC X.                                     00001570
             03 MLPRODC PIC X.                                          00001580
             03 MLPRODP PIC X.                                          00001590
             03 MLPRODH PIC X.                                          00001600
             03 MLPRODV PIC X.                                          00001610
             03 MLPRODO      PIC X(11).                                 00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MAGRPR1A     PIC X.                                     00001640
             03 MAGRPR1C     PIC X.                                     00001650
             03 MAGRPR1P     PIC X.                                     00001660
             03 MAGRPR1H     PIC X.                                     00001670
             03 MAGRPR1V     PIC X.                                     00001680
             03 MAGRPR1O     PIC X(5).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MAGRPR2A     PIC X.                                     00001710
             03 MAGRPR2C     PIC X.                                     00001720
             03 MAGRPR2P     PIC X.                                     00001730
             03 MAGRPR2H     PIC X.                                     00001740
             03 MAGRPR2V     PIC X.                                     00001750
             03 MAGRPR2O     PIC X(5).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MAGRPR3A     PIC X.                                     00001780
             03 MAGRPR3C     PIC X.                                     00001790
             03 MAGRPR3P     PIC X.                                     00001800
             03 MAGRPR3H     PIC X.                                     00001810
             03 MAGRPR3V     PIC X.                                     00001820
             03 MAGRPR3O     PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MAGRPR4A     PIC X.                                     00001850
             03 MAGRPR4C     PIC X.                                     00001860
             03 MAGRPR4P     PIC X.                                     00001870
             03 MAGRPR4H     PIC X.                                     00001880
             03 MAGRPR4V     PIC X.                                     00001890
             03 MAGRPR4O     PIC X(5).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MAGRPR5A     PIC X.                                     00001920
             03 MAGRPR5C     PIC X.                                     00001930
             03 MAGRPR5P     PIC X.                                     00001940
             03 MAGRPR5H     PIC X.                                     00001950
             03 MAGRPR5V     PIC X.                                     00001960
             03 MAGRPR5O     PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MAGRPR6A     PIC X.                                     00001990
             03 MAGRPR6C     PIC X.                                     00002000
             03 MAGRPR6P     PIC X.                                     00002010
             03 MAGRPR6H     PIC X.                                     00002020
             03 MAGRPR6V     PIC X.                                     00002030
             03 MAGRPR6O     PIC X(5).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MAGRPR7A     PIC X.                                     00002060
             03 MAGRPR7C     PIC X.                                     00002070
             03 MAGRPR7P     PIC X.                                     00002080
             03 MAGRPR7H     PIC X.                                     00002090
             03 MAGRPR7V     PIC X.                                     00002100
             03 MAGRPR7O     PIC X(5).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MAGRPR8A     PIC X.                                     00002130
             03 MAGRPR8C     PIC X.                                     00002140
             03 MAGRPR8P     PIC X.                                     00002150
             03 MAGRPR8H     PIC X.                                     00002160
             03 MAGRPR8V     PIC X.                                     00002170
             03 MAGRPR8O     PIC X(5).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MAGRPR9A     PIC X.                                     00002200
             03 MAGRPR9C     PIC X.                                     00002210
             03 MAGRPR9P     PIC X.                                     00002220
             03 MAGRPR9H     PIC X.                                     00002230
             03 MAGRPR9V     PIC X.                                     00002240
             03 MAGRPR9O     PIC X(5).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MAGRPRAA     PIC X.                                     00002270
             03 MAGRPRAC     PIC X.                                     00002280
             03 MAGRPRAP     PIC X.                                     00002290
             03 MAGRPRAH     PIC X.                                     00002300
             03 MAGRPRAV     PIC X.                                     00002310
             03 MAGRPRAO     PIC X(5).                                  00002320
      * MESSAGE ERREUR                                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLIBERRA  PIC X.                                          00002350
           02 MLIBERRC  PIC X.                                          00002360
           02 MLIBERRP  PIC X.                                          00002370
           02 MLIBERRH  PIC X.                                          00002380
           02 MLIBERRV  PIC X.                                          00002390
           02 MLIBERRO  PIC X(78).                                      00002400
      * CODE TRANSACTION                                                00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCODTRAA  PIC X.                                          00002430
           02 MCODTRAC  PIC X.                                          00002440
           02 MCODTRAP  PIC X.                                          00002450
           02 MCODTRAH  PIC X.                                          00002460
           02 MCODTRAV  PIC X.                                          00002470
           02 MCODTRAO  PIC X(4).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MZONCMDA  PIC X.                                          00002500
           02 MZONCMDC  PIC X.                                          00002510
           02 MZONCMDP  PIC X.                                          00002520
           02 MZONCMDH  PIC X.                                          00002530
           02 MZONCMDV  PIC X.                                          00002540
           02 MZONCMDO  PIC X(15).                                      00002550
      * CICS DE TRAVAIL                                                 00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCICSA    PIC X.                                          00002580
           02 MCICSC    PIC X.                                          00002590
           02 MCICSP    PIC X.                                          00002600
           02 MCICSH    PIC X.                                          00002610
           02 MCICSV    PIC X.                                          00002620
           02 MCICSO    PIC X(5).                                       00002630
      * NETNAME                                                         00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNETNAMA  PIC X.                                          00002660
           02 MNETNAMC  PIC X.                                          00002670
           02 MNETNAMP  PIC X.                                          00002680
           02 MNETNAMH  PIC X.                                          00002690
           02 MNETNAMV  PIC X.                                          00002700
           02 MNETNAMO  PIC X(8).                                       00002710
      * CODE TERMINAL                                                   00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MSCREENA  PIC X.                                          00002740
           02 MSCREENC  PIC X.                                          00002750
           02 MSCREENP  PIC X.                                          00002760
           02 MSCREENH  PIC X.                                          00002770
           02 MSCREENV  PIC X.                                          00002780
           02 MSCREENO  PIC X(5).                                       00002790
                                                                                
