      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SIGA - TIERS DES CPTES DE TIERS                                 00000020
      ***************************************************************** 00000030
       01   ESG03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTYPSOCI  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEI    PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MPAGEMAXI      PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNSOCI    PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(24).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNETABI   PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETABL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETABF   PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLETABI   PIC X(22).                                      00000430
           02 MLIGNEI OCCURS   12 TIMES .                               00000440
             03 MSELECTD OCCURS   2 TIMES .                             00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000460
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000470
               04 FILLER     PIC X(4).                                  00000480
               04 MSELECTI   PIC X.                                     00000490
             03 MCOMPTED OCCURS   2 TIMES .                             00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCOMPTEL   COMP PIC S9(4).                            00000510
      *--                                                                       
               04 MCOMPTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCOMPTEF   PIC X.                                     00000520
               04 FILLER     PIC X(4).                                  00000530
               04 MCOMPTEI   PIC X(8).                                  00000540
             03 METABADMD OCCURS   2 TIMES .                            00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 METABADML  COMP PIC S9(4).                            00000560
      *--                                                                       
               04 METABADML COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 METABADMF  PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 METABADMI  PIC X(3).                                  00000590
             03 MTIERSD OCCURS   2 TIMES .                              00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTIERSL    COMP PIC S9(4).                            00000610
      *--                                                                       
               04 MTIERSL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MTIERSF    PIC X.                                     00000620
               04 FILLER     PIC X(4).                                  00000630
               04 MTIERSI    PIC X(10).                                 00000640
      * ZONE CMD AIDA                                                   00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBERRI  PIC X(79).                                      00000690
      * CODE TRANSACTION                                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      * CICS DE TRAVAIL                                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCICSI    PIC X(5).                                       00000790
      * NETNAME                                                         00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MNETNAMI  PIC X(8).                                       00000840
      * CODE TERMINAL                                                   00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * SIGA - TIERS DES CPTES DE TIERS                                 00000910
      ***************************************************************** 00000920
       01   ESG03O REDEFINES ESG03I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
      * DATE DU JOUR                                                    00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
      * HEURE                                                           00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MTIMJOUA  PIC X.                                          00001050
           02 MTIMJOUC  PIC X.                                          00001060
           02 MTIMJOUP  PIC X.                                          00001070
           02 MTIMJOUH  PIC X.                                          00001080
           02 MTIMJOUV  PIC X.                                          00001090
           02 MTIMJOUO  PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTYPSOCA  PIC X.                                          00001120
           02 MTYPSOCC  PIC X.                                          00001130
           02 MTYPSOCP  PIC X.                                          00001140
           02 MTYPSOCH  PIC X.                                          00001150
           02 MTYPSOCV  PIC X.                                          00001160
           02 MTYPSOCO  PIC X(3).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC Z9.                                         00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEMAXA      PIC X.                                     00001260
           02 MPAGEMAXC PIC X.                                          00001270
           02 MPAGEMAXP PIC X.                                          00001280
           02 MPAGEMAXH PIC X.                                          00001290
           02 MPAGEMAXV PIC X.                                          00001300
           02 MPAGEMAXO      PIC Z9.                                    00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNSOCA    PIC X.                                          00001330
           02 MNSOCC    PIC X.                                          00001340
           02 MNSOCP    PIC X.                                          00001350
           02 MNSOCH    PIC X.                                          00001360
           02 MNSOCV    PIC X.                                          00001370
           02 MNSOCO    PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLSOCA    PIC X.                                          00001400
           02 MLSOCC    PIC X.                                          00001410
           02 MLSOCP    PIC X.                                          00001420
           02 MLSOCH    PIC X.                                          00001430
           02 MLSOCV    PIC X.                                          00001440
           02 MLSOCO    PIC X(24).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNETABA   PIC X.                                          00001470
           02 MNETABC   PIC X.                                          00001480
           02 MNETABP   PIC X.                                          00001490
           02 MNETABH   PIC X.                                          00001500
           02 MNETABV   PIC X.                                          00001510
           02 MNETABO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLETABA   PIC X.                                          00001540
           02 MLETABC   PIC X.                                          00001550
           02 MLETABP   PIC X.                                          00001560
           02 MLETABH   PIC X.                                          00001570
           02 MLETABV   PIC X.                                          00001580
           02 MLETABO   PIC X(22).                                      00001590
           02 MLIGNEO OCCURS   12 TIMES .                               00001600
             03 DFHMS1 OCCURS   2 TIMES .                               00001610
               04 FILLER     PIC X(2).                                  00001620
               04 MSELECTA   PIC X.                                     00001630
               04 MSELECTC   PIC X.                                     00001640
               04 MSELECTP   PIC X.                                     00001650
               04 MSELECTH   PIC X.                                     00001660
               04 MSELECTV   PIC X.                                     00001670
               04 MSELECTO   PIC X.                                     00001680
             03 DFHMS2 OCCURS   2 TIMES .                               00001690
               04 FILLER     PIC X(2).                                  00001700
               04 MCOMPTEA   PIC X.                                     00001710
               04 MCOMPTEC   PIC X.                                     00001720
               04 MCOMPTEP   PIC X.                                     00001730
               04 MCOMPTEH   PIC X.                                     00001740
               04 MCOMPTEV   PIC X.                                     00001750
               04 MCOMPTEO   PIC X(8).                                  00001760
             03 DFHMS3 OCCURS   2 TIMES .                               00001770
               04 FILLER     PIC X(2).                                  00001780
               04 METABADMA  PIC X.                                     00001790
               04 METABADMC  PIC X.                                     00001800
               04 METABADMP  PIC X.                                     00001810
               04 METABADMH  PIC X.                                     00001820
               04 METABADMV  PIC X.                                     00001830
               04 METABADMO  PIC X(3).                                  00001840
             03 DFHMS4 OCCURS   2 TIMES .                               00001850
               04 FILLER     PIC X(2).                                  00001860
               04 MTIERSA    PIC X.                                     00001870
               04 MTIERSC    PIC X.                                     00001880
               04 MTIERSP    PIC X.                                     00001890
               04 MTIERSH    PIC X.                                     00001900
               04 MTIERSV    PIC X.                                     00001910
               04 MTIERSO    PIC X(10).                                 00001920
      * ZONE CMD AIDA                                                   00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIBERRA  PIC X.                                          00001950
           02 MLIBERRC  PIC X.                                          00001960
           02 MLIBERRP  PIC X.                                          00001970
           02 MLIBERRH  PIC X.                                          00001980
           02 MLIBERRV  PIC X.                                          00001990
           02 MLIBERRO  PIC X(79).                                      00002000
      * CODE TRANSACTION                                                00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCODTRAA  PIC X.                                          00002030
           02 MCODTRAC  PIC X.                                          00002040
           02 MCODTRAP  PIC X.                                          00002050
           02 MCODTRAH  PIC X.                                          00002060
           02 MCODTRAV  PIC X.                                          00002070
           02 MCODTRAO  PIC X(4).                                       00002080
      * CICS DE TRAVAIL                                                 00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSP    PIC X.                                          00002130
           02 MCICSH    PIC X.                                          00002140
           02 MCICSV    PIC X.                                          00002150
           02 MCICSO    PIC X(5).                                       00002160
      * NETNAME                                                         00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNETNAMA  PIC X.                                          00002190
           02 MNETNAMC  PIC X.                                          00002200
           02 MNETNAMP  PIC X.                                          00002210
           02 MNETNAMH  PIC X.                                          00002220
           02 MNETNAMV  PIC X.                                          00002230
           02 MNETNAMO  PIC X(8).                                       00002240
      * CODE TERMINAL                                                   00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
