      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFM23                                          *  00020000
      *       TR : FM23  GESTION PARAMETRAGE MODELE GCT              *  00030000
      *       PG : TFM23 CREATION/MODIFICATION DES ENTITE MODELES    *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-FM23-LONG         PIC S9(5) COMP-3 VALUE 600.             00080006
       01  TS-FM23-DONNEES.                                             00090000
           05 TS-FM23-LIGNE OCCURS 12.                                          
              10 TS-FM23-COMPTE       PIC X(06).                        00150000
              10 TS-FM23-LCOMPTE      PIC X(30).                        00150000
              10 TS-FM23-CMASQUE      PIC X(06).                        00150000
              10 TS-FM23-NAUX         PIC X(03).                        00150000
              10 TS-FM23-WAUXILIARISE PIC X(01).                        00150000
              10 TS-FM23-WCOLLECTIF   PIC X(01).                        00150000
              10 TS-FM23-WCLOTURE     PIC X(01).                        00150000
      *-------FLAG -----------------------------------                  00070000
              10 TS-FM23-FLAG         PIC X.                            00150000
              10 TS-FM23-SEL          PIC X.                            00150000
                                                                                
