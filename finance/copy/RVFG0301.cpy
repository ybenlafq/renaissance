           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG0301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG0301                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0301.                                                            
           02  FG03-NSOCCRE                                                     
               PIC X(0003).                                                     
           02  FG03-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  FG03-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  FG03-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  FG03-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  FG03-CTYPFACT                                                    
               PIC X(0002).                                                     
           02  FG03-NATFACT                                                     
               PIC X(0005).                                                     
           02  FG03-CVENT                                                       
               PIC X(0005).                                                     
           02  FG03-DEFFET                                                      
               PIC X(0008).                                                     
           02  FG03-MONTHT                                                      
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG03-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  FG03-TXESCPT                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FG03-DFIN                                                        
               PIC X(0008).                                                     
           02  FG03-FREQ                                                        
               PIC X(0002).                                                     
           02  FG03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FG03-SECORIG                                                     
               PIC X(0006).                                                     
           02  FG03-SECDEST                                                     
               PIC X(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG0301                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0301-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NSOCCRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NSOCCRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-CTYPFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-CTYPFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-NATFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-NATFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-CVENT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-CVENT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-MONTHT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-MONTHT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-TXESCPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-TXESCPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-DFIN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-DFIN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-FREQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-FREQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-SECORIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-SECORIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG03-SECDEST-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG03-SECDEST-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
