      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFI7200                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFI7200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFI7200.                                                            
      *}                                                                        
      *                       REGLEF                                            
           10 FI72-REGLEF          PIC X(15).                                   
      *                       NLIGNE                                            
           10 FI72-NLIGNE          PIC S9(2)V USAGE COMP-3.                     
      *                       NLIGNED                                           
           10 FI72-NLIGNED         PIC S9(2)V USAGE COMP-3.                     
      *                       ARTICLE                                           
           10 FI72-ARTICLE         PIC X(7).                                    
      *                       LIBELLE                                           
           10 FI72-LIBELLE         PIC X(20).                                   
      *                       MTFIXE_HT                                         
           10 FI72-MTFIXE-HT       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CTXTVA                                            
           10 FI72-CTXTVA          PIC X(1).                                    
      *                       TX_PRIX                                           
           10 FI72-TX-PRIX         PIC S9(4)V9(1) USAGE COMP-3.                 
      *                       FC1                                               
           10 FI72-FC1             PIC X(1).                                    
      *                       FC2                                               
           10 FI72-FC2             PIC X(1).                                    
      *                       FC3                                               
           10 FI72-FC3             PIC X(1).                                    
      *                       FC4                                               
           10 FI72-FC4             PIC X(1).                                    
      *                       FTAXE                                             
           10 FI72-FTAXE           PIC X(1).                                    
      *                       FDEDUP                                            
           10 FI72-FDEDUP          PIC X(1).                                    
      *                       DCREATION                                         
           10 FI72-DCREATION       PIC X(8).                                    
      *                       DSYST                                             
           10 FI72-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 16      *        
      ******************************************************************        
                                                                                
