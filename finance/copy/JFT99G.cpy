      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JFT99G AU 17/06/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JFT99G.                                                        
            05 NOMETAT-JFT99G           PIC X(6) VALUE 'JFT99G'.                
            05 RUPTURES-JFT99G.                                                 
           10 JFT99G-CINTERFACE         PIC X(05).                      007  005
           10 JFT99G-CTYPOPER           PIC X(05).                      012  005
           10 JFT99G-CNATOPER           PIC X(05).                      017  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JFT99G-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 JFT99G-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JFT99G.                                                   
           10 JFT99G-COMPTE             PIC X(06).                      024  006
           10 JFT99G-CONTREP            PIC X(06).                      030  006
           10 JFT99G-DEFFET             PIC X(10).                      036  010
           10 JFT99G-GEO                PIC X(01).                      046  001
           10 JFT99G-LINTERFACE         PIC X(30).                      047  030
           10 JFT99G-NLETTRAGE          PIC X(01).                      077  001
           10 JFT99G-NLETTRAGEC         PIC X(01).                      078  001
           10 JFT99G-NTIERS             PIC X(01).                      079  001
           10 JFT99G-NTIERSC            PIC X(01).                      080  001
           10 JFT99G-SENS               PIC X(01).                      081  001
           10 JFT99G-TVA                PIC X(01).                      082  001
           10 JFT99G-WANAL              PIC X(01).                      083  001
           10 JFT99G-WANALC             PIC X(01).                      084  001
           10 JFT99G-WCUMUL             PIC X(01).                      085  001
           10 JFT99G-WCUMULC            PIC X(01).                      086  001
           10 JFT99G-WGROUPE            PIC X(01).                      087  001
            05 FILLER                      PIC X(425).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JFT99G-LONG           PIC S9(4)   COMP  VALUE +087.           
      *                                                                         
      *--                                                                       
        01  DSECT-JFT99G-LONG           PIC S9(4) COMP-5  VALUE +087.           
                                                                                
      *}                                                                        
