      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT06   EXT06                                              00000020
      ***************************************************************** 00000030
       01   EXT06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONSLL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCONSLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCONSLF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCONSLI   PIC X(5).                                       00000190
           02 M75I OCCURS   15 TIMES .                                  00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOC-1L     COMP PIC S9(4).                            00000210
      *--                                                                       
             03 MNSOC-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSOC-1F     PIC X.                                     00000220
             03 FILLER  PIC X(4).                                       00000230
             03 MNSOC-1I     PIC X(3).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEU-1L    COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MNLIEU-1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNLIEU-1F    PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MNLIEU-1I    PIC X(3).                                  00000280
             03 MCAGR-1D OCCURS   3 TIMES .                             00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCAGR-1L   COMP PIC S9(4).                            00000300
      *--                                                                       
               04 MCAGR-1L COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCAGR-1F   PIC X.                                     00000310
               04 FILLER     PIC X(4).                                  00000320
               04 MCAGR-1I   PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQ-1L     COMP PIC S9(4).                            00000340
      *--                                                                       
             03 MNSEQ-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSEQ-1F     PIC X.                                     00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MNSEQ-1I     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIF-1L   COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MWACTIF-1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWACTIF-1F   PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MWACTIF-1I   PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOC-2L     COMP PIC S9(4).                            00000420
      *--                                                                       
             03 MNSOC-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSOC-2F     PIC X.                                     00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MNSOC-2I     PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEU-2L    COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MNLIEU-2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNLIEU-2F    PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MNLIEU-2I    PIC X(3).                                  00000490
             03 MCAGR-2D OCCURS   3 TIMES .                             00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCAGR-2L   COMP PIC S9(4).                            00000510
      *--                                                                       
               04 MCAGR-2L COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCAGR-2F   PIC X.                                     00000520
               04 FILLER     PIC X(4).                                  00000530
               04 MCAGR-2I   PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQ-2L     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNSEQ-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSEQ-2F     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNSEQ-2I     PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIF-2L   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MWACTIF-2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWACTIF-2F   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MWACTIF-2I   PIC X.                                     00000620
      * MESSAGE ERREUR                                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLIBERRI  PIC X(78).                                      00000670
      * CODE TRANSACTION                                                00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MZONCMDI  PIC X(15).                                      00000760
      * CICS DE TRAVAIL                                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      * NETNAME                                                         00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      * CODE TERMINAL                                                   00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MSCREENI  PIC X(5).                                       00000910
      ***************************************************************** 00000920
      * SDF: EXT06   EXT06                                              00000930
      ***************************************************************** 00000940
       01   EXT06O REDEFINES EXT06I.                                    00000950
           02 FILLER    PIC X(12).                                      00000960
      * DATE DU JOUR                                                    00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MDATJOUA  PIC X.                                          00000990
           02 MDATJOUC  PIC X.                                          00001000
           02 MDATJOUP  PIC X.                                          00001010
           02 MDATJOUH  PIC X.                                          00001020
           02 MDATJOUV  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
      * HEURE                                                           00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCONSLA   PIC X.                                          00001140
           02 MCONSLC   PIC X.                                          00001150
           02 MCONSLP   PIC X.                                          00001160
           02 MCONSLH   PIC X.                                          00001170
           02 MCONSLV   PIC X.                                          00001180
           02 MCONSLO   PIC X(5).                                       00001190
           02 M75O OCCURS   15 TIMES .                                  00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MNSOC-1A     PIC X.                                     00001220
             03 MNSOC-1C     PIC X.                                     00001230
             03 MNSOC-1P     PIC X.                                     00001240
             03 MNSOC-1H     PIC X.                                     00001250
             03 MNSOC-1V     PIC X.                                     00001260
             03 MNSOC-1O     PIC X(3).                                  00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MNLIEU-1A    PIC X.                                     00001290
             03 MNLIEU-1C    PIC X.                                     00001300
             03 MNLIEU-1P    PIC X.                                     00001310
             03 MNLIEU-1H    PIC X.                                     00001320
             03 MNLIEU-1V    PIC X.                                     00001330
             03 MNLIEU-1O    PIC X(3).                                  00001340
             03 DFHMS1 OCCURS   3 TIMES .                               00001350
               04 FILLER     PIC X(2).                                  00001360
               04 MCAGR-1A   PIC X.                                     00001370
               04 MCAGR-1C   PIC X.                                     00001380
               04 MCAGR-1P   PIC X.                                     00001390
               04 MCAGR-1H   PIC X.                                     00001400
               04 MCAGR-1V   PIC X.                                     00001410
               04 MCAGR-1O   PIC X(5).                                  00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MNSEQ-1A     PIC X.                                     00001440
             03 MNSEQ-1C     PIC X.                                     00001450
             03 MNSEQ-1P     PIC X.                                     00001460
             03 MNSEQ-1H     PIC X.                                     00001470
             03 MNSEQ-1V     PIC X.                                     00001480
             03 MNSEQ-1O     PIC X(3).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MWACTIF-1A   PIC X.                                     00001510
             03 MWACTIF-1C   PIC X.                                     00001520
             03 MWACTIF-1P   PIC X.                                     00001530
             03 MWACTIF-1H   PIC X.                                     00001540
             03 MWACTIF-1V   PIC X.                                     00001550
             03 MWACTIF-1O   PIC X.                                     00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MNSOC-2A     PIC X.                                     00001580
             03 MNSOC-2C     PIC X.                                     00001590
             03 MNSOC-2P     PIC X.                                     00001600
             03 MNSOC-2H     PIC X.                                     00001610
             03 MNSOC-2V     PIC X.                                     00001620
             03 MNSOC-2O     PIC X(3).                                  00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MNLIEU-2A    PIC X.                                     00001650
             03 MNLIEU-2C    PIC X.                                     00001660
             03 MNLIEU-2P    PIC X.                                     00001670
             03 MNLIEU-2H    PIC X.                                     00001680
             03 MNLIEU-2V    PIC X.                                     00001690
             03 MNLIEU-2O    PIC X(3).                                  00001700
             03 DFHMS2 OCCURS   3 TIMES .                               00001710
               04 FILLER     PIC X(2).                                  00001720
               04 MCAGR-2A   PIC X.                                     00001730
               04 MCAGR-2C   PIC X.                                     00001740
               04 MCAGR-2P   PIC X.                                     00001750
               04 MCAGR-2H   PIC X.                                     00001760
               04 MCAGR-2V   PIC X.                                     00001770
               04 MCAGR-2O   PIC X(5).                                  00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNSEQ-2A     PIC X.                                     00001800
             03 MNSEQ-2C     PIC X.                                     00001810
             03 MNSEQ-2P     PIC X.                                     00001820
             03 MNSEQ-2H     PIC X.                                     00001830
             03 MNSEQ-2V     PIC X.                                     00001840
             03 MNSEQ-2O     PIC X(3).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MWACTIF-2A   PIC X.                                     00001870
             03 MWACTIF-2C   PIC X.                                     00001880
             03 MWACTIF-2P   PIC X.                                     00001890
             03 MWACTIF-2H   PIC X.                                     00001900
             03 MWACTIF-2V   PIC X.                                     00001910
             03 MWACTIF-2O   PIC X.                                     00001920
      * MESSAGE ERREUR                                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIBERRA  PIC X.                                          00001950
           02 MLIBERRC  PIC X.                                          00001960
           02 MLIBERRP  PIC X.                                          00001970
           02 MLIBERRH  PIC X.                                          00001980
           02 MLIBERRV  PIC X.                                          00001990
           02 MLIBERRO  PIC X(78).                                      00002000
      * CODE TRANSACTION                                                00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCODTRAA  PIC X.                                          00002030
           02 MCODTRAC  PIC X.                                          00002040
           02 MCODTRAP  PIC X.                                          00002050
           02 MCODTRAH  PIC X.                                          00002060
           02 MCODTRAV  PIC X.                                          00002070
           02 MCODTRAO  PIC X(4).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MZONCMDA  PIC X.                                          00002100
           02 MZONCMDC  PIC X.                                          00002110
           02 MZONCMDP  PIC X.                                          00002120
           02 MZONCMDH  PIC X.                                          00002130
           02 MZONCMDV  PIC X.                                          00002140
           02 MZONCMDO  PIC X(15).                                      00002150
      * CICS DE TRAVAIL                                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCICSA    PIC X.                                          00002180
           02 MCICSC    PIC X.                                          00002190
           02 MCICSP    PIC X.                                          00002200
           02 MCICSH    PIC X.                                          00002210
           02 MCICSV    PIC X.                                          00002220
           02 MCICSO    PIC X(5).                                       00002230
      * NETNAME                                                         00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNETNAMA  PIC X.                                          00002260
           02 MNETNAMC  PIC X.                                          00002270
           02 MNETNAMP  PIC X.                                          00002280
           02 MNETNAMH  PIC X.                                          00002290
           02 MNETNAMV  PIC X.                                          00002300
           02 MNETNAMO  PIC X(8).                                       00002310
      * CODE TERMINAL                                                   00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MSCREENA  PIC X.                                          00002340
           02 MSCREENC  PIC X.                                          00002350
           02 MSCREENP  PIC X.                                          00002360
           02 MSCREENH  PIC X.                                          00002370
           02 MSCREENV  PIC X.                                          00002380
           02 MSCREENO  PIC X(5).                                       00002390
                                                                                
