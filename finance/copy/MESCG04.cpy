      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ ENVOI DONNEES ARTICLE VERS SAP                        
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
            10 MQ10-MSGID                  PIC X(24).                           
            10 MQ10-CORRELID               PIC X(24).                           
         02 WS-CODRET                      PIC X(02).                           
         02 WS-MESSAGE.                                                         
            05 MESS-ENTETE.                                                     
               10 MES-TYPE                 PIC X(03).                           
               10 MES-NSOCMSG              PIC X(03).                           
               10 MES-NLIEUMSG             PIC X(03).                           
               10 MES-NSOCDST              PIC X(03).                           
               10 MES-NLIEUDST             PIC X(03).                           
               10 MES-NORD                 PIC 9(08).                           
               10 MES-LPROG                PIC X(10).                           
               10 MES-DJOUR                PIC X(08).                           
               10 MES-WSID                 PIC X(10).                           
               10 MES-USER                 PIC X(10).                           
               10 MES-CHRONO               PIC 9(07).                           
               10 MES-NBRMSG               PIC 9(07).                           
               10 MES-NBRENR               PIC 9(05).                           
               10 MES-TAILLE               PIC 9(05).                           
               10 MES-FILLER               PIC X(20).                           
            05 MES-MESSAGE                 PIC X(10000).                        
      *     MESSAGE DETAIL                                                      
            05 MES-MESSAGE-PLAT REDEFINES MES-MESSAGE.                          
               10 MES-PLAT-FIXE.                                                
                  15 MES-CFONC                 PIC X(03).                       
                  15 MES-NCODIC                PIC X(07).                       
                  15 MES-LREFFOURN             PIC X(20).                       
                  15 MES-CFAM                  PIC X(05).                       
                  15 MES-CMARQ                 PIC X(05).                       
                  15 MES-DEFSTATUT             PIC X(08).                       
                  15 MES-DCREATION             PIC X(08).                       
                  15 MES-CAPPRO                PIC X(05).                       
      *           15 MES-CHEFPROD              PIC X(05).                       
                  15 MES-CORIGPROD             PIC X(05).                       
                  15 MES-QPOIDS                PIC 9(07).                       
                  15 MES-QLARGEUR              PIC 9(03).                       
                  15 MES-QPROFONDEUR           PIC 9(03).                       
                  15 MES-QHAUTEUR              PIC 9(03).                       
                  15 MES-QPOIDSDE              PIC 9(07).                       
                  15 MES-QLARGEURDE            PIC 9(03).                       
                  15 MES-QPROFONDEURDE         PIC 9(03).                       
                  15 MES-QHAUTEURDE            PIC 9(03).                       
                  15 MES-CNOMDOU               PIC X(08).                       
                  15 MES-PMONTANT              PIC 9(07).                       
                  15 MES-NEANA                 PIC X(13).                       
               10 MES-PLAT-VAR.                                                 
                  15 MES-NEAN-INACTIF OCCURS 99.                                
                     20 MES-NEAN                  PIC X(13).                    
               10 FILLER                    PIC X(8582).                        
                                                                                
