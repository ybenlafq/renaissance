      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT04   EXT04                                              00000020
      ***************************************************************** 00000030
       01   EXT04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXTRL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MEXTRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEXTRF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MEXTRI    PIC X(5).                                       00000190
           02 MTABLED OCCURS   4 TIMES .                                00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTABLEL      COMP PIC S9(4).                            00000210
      *--                                                                       
             03 MTABLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTABLEF      PIC X.                                     00000220
             03 FILLER  PIC X(4).                                       00000230
             03 MTABLEI      PIC X(6).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONSLL   COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MCONSLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCONSLF   PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MCONSLI   PIC X(5).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MCETATI   PIC X(10).                                      00000320
           02 MCRUPTD OCCURS   5 TIMES .                                00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRUPTL      COMP PIC S9(4).                            00000340
      *--                                                                       
             03 MCRUPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCRUPTF      PIC X.                                     00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MCRUPTI      PIC X(3).                                  00000370
      * MESSAGE ERREUR                                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MLIBERRI  PIC X(78).                                      00000420
      * CODE TRANSACTION                                                00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCODTRAI  PIC X(4).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MZONCMDI  PIC X(15).                                      00000510
      * CICS DE TRAVAIL                                                 00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCICSI    PIC X(5).                                       00000560
      * NETNAME                                                         00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNETNAMI  PIC X(8).                                       00000610
      * CODE TERMINAL                                                   00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSCREENI  PIC X(5).                                       00000660
      ***************************************************************** 00000670
      * SDF: EXT04   EXT04                                              00000680
      ***************************************************************** 00000690
       01   EXT04O REDEFINES EXT04I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
      * DATE DU JOUR                                                    00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MDATJOUA  PIC X.                                          00000740
           02 MDATJOUC  PIC X.                                          00000750
           02 MDATJOUP  PIC X.                                          00000760
           02 MDATJOUH  PIC X.                                          00000770
           02 MDATJOUV  PIC X.                                          00000780
           02 MDATJOUO  PIC X(10).                                      00000790
      * HEURE                                                           00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MTIMJOUA  PIC X.                                          00000820
           02 MTIMJOUC  PIC X.                                          00000830
           02 MTIMJOUP  PIC X.                                          00000840
           02 MTIMJOUH  PIC X.                                          00000850
           02 MTIMJOUV  PIC X.                                          00000860
           02 MTIMJOUO  PIC X(5).                                       00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MEXTRA    PIC X.                                          00000890
           02 MEXTRC    PIC X.                                          00000900
           02 MEXTRP    PIC X.                                          00000910
           02 MEXTRH    PIC X.                                          00000920
           02 MEXTRV    PIC X.                                          00000930
           02 MEXTRO    PIC X(5).                                       00000940
           02 DFHMS1 OCCURS   4 TIMES .                                 00000950
             03 FILLER       PIC X(2).                                  00000960
             03 MTABLEA      PIC X.                                     00000970
             03 MTABLEC PIC X.                                          00000980
             03 MTABLEP PIC X.                                          00000990
             03 MTABLEH PIC X.                                          00001000
             03 MTABLEV PIC X.                                          00001010
             03 MTABLEO      PIC X(6).                                  00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MCONSLA   PIC X.                                          00001040
           02 MCONSLC   PIC X.                                          00001050
           02 MCONSLP   PIC X.                                          00001060
           02 MCONSLH   PIC X.                                          00001070
           02 MCONSLV   PIC X.                                          00001080
           02 MCONSLO   PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MCETATA   PIC X.                                          00001110
           02 MCETATC   PIC X.                                          00001120
           02 MCETATP   PIC X.                                          00001130
           02 MCETATH   PIC X.                                          00001140
           02 MCETATV   PIC X.                                          00001150
           02 MCETATO   PIC X(10).                                      00001160
           02 DFHMS2 OCCURS   5 TIMES .                                 00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MCRUPTA      PIC X.                                     00001190
             03 MCRUPTC PIC X.                                          00001200
             03 MCRUPTP PIC X.                                          00001210
             03 MCRUPTH PIC X.                                          00001220
             03 MCRUPTV PIC X.                                          00001230
             03 MCRUPTO      PIC X(3).                                  00001240
      * MESSAGE ERREUR                                                  00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MLIBERRA  PIC X.                                          00001270
           02 MLIBERRC  PIC X.                                          00001280
           02 MLIBERRP  PIC X.                                          00001290
           02 MLIBERRH  PIC X.                                          00001300
           02 MLIBERRV  PIC X.                                          00001310
           02 MLIBERRO  PIC X(78).                                      00001320
      * CODE TRANSACTION                                                00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCODTRAA  PIC X.                                          00001350
           02 MCODTRAC  PIC X.                                          00001360
           02 MCODTRAP  PIC X.                                          00001370
           02 MCODTRAH  PIC X.                                          00001380
           02 MCODTRAV  PIC X.                                          00001390
           02 MCODTRAO  PIC X(4).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MZONCMDA  PIC X.                                          00001420
           02 MZONCMDC  PIC X.                                          00001430
           02 MZONCMDP  PIC X.                                          00001440
           02 MZONCMDH  PIC X.                                          00001450
           02 MZONCMDV  PIC X.                                          00001460
           02 MZONCMDO  PIC X(15).                                      00001470
      * CICS DE TRAVAIL                                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCICSA    PIC X.                                          00001500
           02 MCICSC    PIC X.                                          00001510
           02 MCICSP    PIC X.                                          00001520
           02 MCICSH    PIC X.                                          00001530
           02 MCICSV    PIC X.                                          00001540
           02 MCICSO    PIC X(5).                                       00001550
      * NETNAME                                                         00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNETNAMA  PIC X.                                          00001580
           02 MNETNAMC  PIC X.                                          00001590
           02 MNETNAMP  PIC X.                                          00001600
           02 MNETNAMH  PIC X.                                          00001610
           02 MNETNAMV  PIC X.                                          00001620
           02 MNETNAMO  PIC X(8).                                       00001630
      * CODE TERMINAL                                                   00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MSCREENA  PIC X.                                          00001660
           02 MSCREENC  PIC X.                                          00001670
           02 MSCREENP  PIC X.                                          00001680
           02 MSCREENH  PIC X.                                          00001690
           02 MSCREENV  PIC X.                                          00001700
           02 MSCREENO  PIC X(5).                                       00001710
                                                                                
