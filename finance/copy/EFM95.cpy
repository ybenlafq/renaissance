      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SAP - correspondance FM95                                       00000020
      ***************************************************************** 00000030
       01   EFM95I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCNATI    PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTCORL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MCINTCORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCINTCORF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCINTCORI      PIC X(5).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCORL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MCTYPCORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPCORF      PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCTYPCORI      PIC X(5).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATCORL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MCNATCORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCNATCORF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCNATCORI      PIC X(5).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCINTERI  PIC X.                                          00000550
           02 MTABI OCCURS   11 TIMES .                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MSELI   PIC X.                                          00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MCTYPOPERI   PIC X(5).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MCNATOPERI   PIC X(5).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINTLL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCINTLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCINTLF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCINTLI      PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPLL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MCTYPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPLF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MCTYPLI      PIC X(5).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATLL      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MCNATLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCNATLF      PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCNATLI      PIC X(5).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINTERLL    COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MCINTERLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCINTERLF    PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MCINTERLI    PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTIERSLL    COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MCTIERSLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTIERSLF    PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MCTIERSLI    PIC X(4).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSOCLL      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MCSOCLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCSOCLF      PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MCSOCLI      PIC X(3).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEULL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MCLIEULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCLIEULF     PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCLIEULI     PIC X(3).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPIECELL    COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MCPIECELL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPIECELF    PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MCPIECELI    PIC X(2).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDATELL     COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MCDATELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCDATELF     PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MCDATELI     PIC X(10).                                 00001040
      * ZONE CMD AIDA                                                   00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(79).                                      00001090
      * CODE TRANSACTION                                                00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      * CICS DE TRAVAIL                                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MCICSI    PIC X(5).                                       00001190
      * NETNAME                                                         00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      * CODE TERMINAL                                                   00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * SAP - correspondance FM95                                       00001310
      ***************************************************************** 00001320
       01   EFM95O REDEFINES EFM95I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
      * DATE DU JOUR                                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
      * HEURE                                                           00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MTIMJOUA  PIC X.                                          00001450
           02 MTIMJOUC  PIC X.                                          00001460
           02 MTIMJOUP  PIC X.                                          00001470
           02 MTIMJOUH  PIC X.                                          00001480
           02 MTIMJOUV  PIC X.                                          00001490
           02 MTIMJOUO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MPAGEA    PIC X.                                          00001520
           02 MPAGEC    PIC X.                                          00001530
           02 MPAGEP    PIC X.                                          00001540
           02 MPAGEH    PIC X.                                          00001550
           02 MPAGEV    PIC X.                                          00001560
           02 MPAGEO    PIC Z9.                                         00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNBPA     PIC X.                                          00001590
           02 MNBPC     PIC X.                                          00001600
           02 MNBPP     PIC X.                                          00001610
           02 MNBPH     PIC X.                                          00001620
           02 MNBPV     PIC X.                                          00001630
           02 MNBPO     PIC Z9.                                         00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCINTERFACEA   PIC X.                                     00001660
           02 MCINTERFACEC   PIC X.                                     00001670
           02 MCINTERFACEP   PIC X.                                     00001680
           02 MCINTERFACEH   PIC X.                                     00001690
           02 MCINTERFACEV   PIC X.                                     00001700
           02 MCINTERFACEO   PIC X(5).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLINTERFACEA   PIC X.                                     00001730
           02 MLINTERFACEC   PIC X.                                     00001740
           02 MLINTERFACEP   PIC X.                                     00001750
           02 MLINTERFACEH   PIC X.                                     00001760
           02 MLINTERFACEV   PIC X.                                     00001770
           02 MLINTERFACEO   PIC X(30).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCTYPA    PIC X.                                          00001800
           02 MCTYPC    PIC X.                                          00001810
           02 MCTYPP    PIC X.                                          00001820
           02 MCTYPH    PIC X.                                          00001830
           02 MCTYPV    PIC X.                                          00001840
           02 MCTYPO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MCNATA    PIC X.                                          00001870
           02 MCNATC    PIC X.                                          00001880
           02 MCNATP    PIC X.                                          00001890
           02 MCNATH    PIC X.                                          00001900
           02 MCNATV    PIC X.                                          00001910
           02 MCNATO    PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCINTCORA      PIC X.                                     00001940
           02 MCINTCORC PIC X.                                          00001950
           02 MCINTCORP PIC X.                                          00001960
           02 MCINTCORH PIC X.                                          00001970
           02 MCINTCORV PIC X.                                          00001980
           02 MCINTCORO      PIC X(5).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MCTYPCORA      PIC X.                                     00002010
           02 MCTYPCORC PIC X.                                          00002020
           02 MCTYPCORP PIC X.                                          00002030
           02 MCTYPCORH PIC X.                                          00002040
           02 MCTYPCORV PIC X.                                          00002050
           02 MCTYPCORO      PIC X(5).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCNATCORA      PIC X.                                     00002080
           02 MCNATCORC PIC X.                                          00002090
           02 MCNATCORP PIC X.                                          00002100
           02 MCNATCORH PIC X.                                          00002110
           02 MCNATCORV PIC X.                                          00002120
           02 MCNATCORO      PIC X(5).                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCINTERA  PIC X.                                          00002150
           02 MCINTERC  PIC X.                                          00002160
           02 MCINTERP  PIC X.                                          00002170
           02 MCINTERH  PIC X.                                          00002180
           02 MCINTERV  PIC X.                                          00002190
           02 MCINTERO  PIC X.                                          00002200
           02 MTABO OCCURS   11 TIMES .                                 00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MSELA   PIC X.                                          00002230
             03 MSELC   PIC X.                                          00002240
             03 MSELP   PIC X.                                          00002250
             03 MSELH   PIC X.                                          00002260
             03 MSELV   PIC X.                                          00002270
             03 MSELO   PIC X.                                          00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MCTYPOPERA   PIC X.                                     00002300
             03 MCTYPOPERC   PIC X.                                     00002310
             03 MCTYPOPERP   PIC X.                                     00002320
             03 MCTYPOPERH   PIC X.                                     00002330
             03 MCTYPOPERV   PIC X.                                     00002340
             03 MCTYPOPERO   PIC X(5).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MCNATOPERA   PIC X.                                     00002370
             03 MCNATOPERC   PIC X.                                     00002380
             03 MCNATOPERP   PIC X.                                     00002390
             03 MCNATOPERH   PIC X.                                     00002400
             03 MCNATOPERV   PIC X.                                     00002410
             03 MCNATOPERO   PIC X(5).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MCINTLA      PIC X.                                     00002440
             03 MCINTLC PIC X.                                          00002450
             03 MCINTLP PIC X.                                          00002460
             03 MCINTLH PIC X.                                          00002470
             03 MCINTLV PIC X.                                          00002480
             03 MCINTLO      PIC X(5).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MCTYPLA      PIC X.                                     00002510
             03 MCTYPLC PIC X.                                          00002520
             03 MCTYPLP PIC X.                                          00002530
             03 MCTYPLH PIC X.                                          00002540
             03 MCTYPLV PIC X.                                          00002550
             03 MCTYPLO      PIC X(5).                                  00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MCNATLA      PIC X.                                     00002580
             03 MCNATLC PIC X.                                          00002590
             03 MCNATLP PIC X.                                          00002600
             03 MCNATLH PIC X.                                          00002610
             03 MCNATLV PIC X.                                          00002620
             03 MCNATLO      PIC X(5).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MCINTERLA    PIC X.                                     00002650
             03 MCINTERLC    PIC X.                                     00002660
             03 MCINTERLP    PIC X.                                     00002670
             03 MCINTERLH    PIC X.                                     00002680
             03 MCINTERLV    PIC X.                                     00002690
             03 MCINTERLO    PIC X.                                     00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MCTIERSLA    PIC X.                                     00002720
             03 MCTIERSLC    PIC X.                                     00002730
             03 MCTIERSLP    PIC X.                                     00002740
             03 MCTIERSLH    PIC X.                                     00002750
             03 MCTIERSLV    PIC X.                                     00002760
             03 MCTIERSLO    PIC X(4).                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MCSOCLA      PIC X.                                     00002790
             03 MCSOCLC PIC X.                                          00002800
             03 MCSOCLP PIC X.                                          00002810
             03 MCSOCLH PIC X.                                          00002820
             03 MCSOCLV PIC X.                                          00002830
             03 MCSOCLO      PIC X(3).                                  00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MCLIEULA     PIC X.                                     00002860
             03 MCLIEULC     PIC X.                                     00002870
             03 MCLIEULP     PIC X.                                     00002880
             03 MCLIEULH     PIC X.                                     00002890
             03 MCLIEULV     PIC X.                                     00002900
             03 MCLIEULO     PIC X(3).                                  00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MCPIECELA    PIC X.                                     00002930
             03 MCPIECELC    PIC X.                                     00002940
             03 MCPIECELP    PIC X.                                     00002950
             03 MCPIECELH    PIC X.                                     00002960
             03 MCPIECELV    PIC X.                                     00002970
             03 MCPIECELO    PIC X(2).                                  00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MCDATELA     PIC X.                                     00003000
             03 MCDATELC     PIC X.                                     00003010
             03 MCDATELP     PIC X.                                     00003020
             03 MCDATELH     PIC X.                                     00003030
             03 MCDATELV     PIC X.                                     00003040
             03 MCDATELO     PIC X(10).                                 00003050
      * ZONE CMD AIDA                                                   00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MLIBERRA  PIC X.                                          00003080
           02 MLIBERRC  PIC X.                                          00003090
           02 MLIBERRP  PIC X.                                          00003100
           02 MLIBERRH  PIC X.                                          00003110
           02 MLIBERRV  PIC X.                                          00003120
           02 MLIBERRO  PIC X(79).                                      00003130
      * CODE TRANSACTION                                                00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
      * CICS DE TRAVAIL                                                 00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MCICSA    PIC X.                                          00003240
           02 MCICSC    PIC X.                                          00003250
           02 MCICSP    PIC X.                                          00003260
           02 MCICSH    PIC X.                                          00003270
           02 MCICSV    PIC X.                                          00003280
           02 MCICSO    PIC X(5).                                       00003290
      * NETNAME                                                         00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNETNAMA  PIC X.                                          00003320
           02 MNETNAMC  PIC X.                                          00003330
           02 MNETNAMP  PIC X.                                          00003340
           02 MNETNAMH  PIC X.                                          00003350
           02 MNETNAMV  PIC X.                                          00003360
           02 MNETNAMO  PIC X(8).                                       00003370
      * CODE TERMINAL                                                   00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MSCREENA  PIC X.                                          00003400
           02 MSCREENC  PIC X.                                          00003410
           02 MSCREENP  PIC X.                                          00003420
           02 MSCREENH  PIC X.                                          00003430
           02 MSCREENV  PIC X.                                          00003440
           02 MSCREENO  PIC X(4).                                       00003450
                                                                                
