      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVFI7100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFI7100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFI7100.                                                            
      *}                                                                        
      *                       REGLEF                                            
           10 FI71-REGLEF          PIC X(15).                                   
      *                       NLIGNE                                            
           10 FI71-NLIGNE          PIC S9(2)V USAGE COMP-3.                     
      *                       INTITULE                                          
           10 FI71-INTITULE        PIC X(30).                                   
      *                       COMMENT                                           
           10 FI71-COMMENT         PIC X(25).                                   
      *                       FLUX                                              
           10 FI71-FLUX            PIC X(12).                                   
      *                       TITRE                                             
           10 FI71-TITRE           PIC X(35).                                   
      *                       NREGROUP                                          
           10 FI71-NREGROUP        PIC X(5).                                    
      *                       WENVOISAP                                         
           10 FI71-WENVOISAP       PIC X(1).                                    
      *                       LREGROUP                                          
           10 FI71-LREGROUP        PIC X(35).                                   
      *                       FPRMP                                             
           10 FI71-FPRMP           PIC X(1).                                    
      *                       FSOCE                                             
           10 FI71-FSOCE           PIC X(1).                                    
      *                       FSOCE_NSOC                                        
           10 FI71-FSOCE-NSOC      PIC X(3).                                    
      *                       FSOCE_LIEU                                        
           10 FI71-FSOCE-LIEU      PIC X(3).                                    
      *                       FSOCD                                             
           10 FI71-FSOCD           PIC X(1).                                    
      *                       FSOCD_NSOC                                        
           10 FI71-FSOCD-NSOC      PIC X(3).                                    
      *                       FSOCD_LIEU                                        
           10 FI71-FSOCD-LIEU      PIC X(3).                                    
      *                       FSOCV                                             
           10 FI71-FSOCV           PIC X(1).                                    
      *                       FSOCV_NSOC                                        
           10 FI71-FSOCV-NSOC      PIC X(3).                                    
      *                       FSOCV_LIEU                                        
           10 FI71-FSOCV-LIEU      PIC X(3).                                    
      *                       PEC_LIGNE                                         
           10 FI71-PEC-LIGNE       PIC X(1).                                    
      *                       FPRIX_FIC                                         
           10 FI71-FPRIX-FIC       PIC X(1).                                    
      *                       MTFIXE_HT                                         
           10 FI71-MTFIXE-HT       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CTXTVA                                            
           10 FI71-CTXTVA          PIC X(1).                                    
      *                       APPLI_PRMP                                        
           10 FI71-APPLI-PRMP      PIC X(1).                                    
      *                       FC1                                               
           10 FI71-FC1             PIC X(1).                                    
      *                       FC2                                               
           10 FI71-FC2             PIC X(1).                                    
      *                       FC3                                               
           10 FI71-FC3             PIC X(1).                                    
      *                       FC4                                               
           10 FI71-FC4             PIC X(1).                                    
      *                       FTAXE                                             
           10 FI71-FTAXE           PIC X(1).                                    
      *                       IDVETUSTE                                         
           10 FI71-IDVETUSTE       PIC X(10).                                   
      *                       DCREATION                                         
           10 FI71-DCREATION       PIC X(8).                                    
      *                       DSYST                                             
           10 FI71-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       LIBELLE                                           
           10 FI71-LIBELLE         PIC X(40).                                   
      *                       LREMISE                                           
           10 FI71-LREMISE         PIC X(10).                                   
      *                       TX_REM                                            
           10 FI71-TX-REM          PIC S9(2)V USAGE COMP-3.                     
      *                       FC5                                               
           10 FI71-FC5             PIC X(1).                                    
      *                       FC6                                               
           10 FI71-FC6             PIC X(1).                                    
      *                       FC7                                               
           10 FI71-FC7             PIC X(1).                                    
      *                       FC8                                               
           10 FI71-FC8             PIC X(1).                                    
      *                       FC9                                               
           10 FI71-FC9             PIC X(1).                                    
      *                       FC10                                              
           10 FI71-FC10            PIC X(1).                                    
      *                       FC11                                              
           10 FI71-FC11            PIC X(1).                                    
      *                       FC12                                              
           10 FI71-FC12            PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 43      *        
      ******************************************************************        
                                                                                
