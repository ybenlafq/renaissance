      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM02-LONG             PIC S9(4) COMP-3 VALUE +546.                
       01  TS-FM02-RECORD.                                                      
           05 TS-FM02-LIGNE         OCCURS 14.                                  
              10 TS-FM02-WTYPAUX    PIC X.                                      
              10 TS-FM02-NAUX       PIC X(3).                                   
              10 TS-FM02-LAUX       PIC X(30).                                  
              10 TS-FM02-WSAISIE    PIC X.                                      
              10 TS-FM02-WAUXNCG    PIC X.                                      
              10 TS-FM02-WLETTRE    PIC X.                                      
              10 TS-FM02-WRELANCE   PIC X.                                      
              10 TS-FM02-WAUXARF    PIC X.                                      
                                                                                
