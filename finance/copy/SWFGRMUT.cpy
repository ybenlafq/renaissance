      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DU FICHIER POUR EDITION DU                                
      *       RECAPITULATIF DES MUTATIONS                                       
      *                                                                         
      * L = 100                                                                 
      *                                                                         
      *                                                                         
       01         FFGRMUT-DSECT.                                                
           03    FFGRMUT-IDENTIFIANT.                                           
001           05 FFGRMUT-NSOCC-ORIG    PIC X(03).                               
004           05 FFGRMUT-NSOCC-DEST    PIC X(03).                               
007           05 FFGRMUT-NSOCORIG      PIC X(03).                               
010           05 FFGRMUT-NLIEUORIG     PIC X(03).                               
013           05 FFGRMUT-NSOCDEST      PIC X(03).                               
016           05 FFGRMUT-NLIEUDEST     PIC X(03).                               
019           05 FFGRMUT-CENREG        PIC X(01).                               
020           05 FFGRMUT-DMUTATION     PIC X(08).                               
028           05 FFGRMUT-NMUTATION     PIC X(07).                               
035           05 FFGRMUT-CVENT         PIC X(05).                               
040           05 FFGRMUT-CTAUXTVA      PIC X(05).                               
045           05 FFGRMUT-CTYPFACT      PIC X(02).                               
047           05 FFGRMUT-NUMFACT       PIC X(07).                               
           03    FFGRMUT-MONTANTS.                                              
054           05 FFGRMUT-QNBPIECES     PIC S9(7)     COMP-3.                    
058           05 FFGRMUT-MONTHT        PIC S9(11)V99 COMP-3.                    
065           05 FFGRMUT-MPCF          PIC S9(11)V99 COMP-3.                    
072           05 FFGRMUT-MFRMG         PIC S9(11)V99 COMP-3.                    
079           05 FFGRMUT-MFRTP         PIC S9(11)V99 COMP-3.                    
086        03 FILLER                   PIC X(15).                               
-=>100*                                                                         
                                                                                
