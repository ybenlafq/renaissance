      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE REGLES DE CUMUL                                     00000020
      ***************************************************************** 00000030
       01   EIF12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODPAIL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MMODPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODPAIF  PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MMODPAII  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCTAL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MTYPCTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPCTAF  PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MTYPCTAI  PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPMTL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTYPMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPMTF  PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MCTYPMTI  PIC X(5).                                       00000270
           02 FILLER  OCCURS   10 TIMES .                               00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRANGL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MRANGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRANGF  PIC X.                                          00000300
             03 FILLER  PIC X(2).                                       00000310
             03 MRANGI  PIC X(2).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCHAMPL   COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNOMCHAMPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNOMCHAMPF   PIC X.                                     00000340
             03 FILLER  PIC X(2).                                       00000350
             03 MNOMCHAMPI   PIC X(10).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000380
             03 FILLER  PIC X(2).                                       00000390
             03 MMASQUEI     PIC X(5).                                  00000400
      * MESSAGE ERREUR                                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MLIBERRI  PIC X(79).                                      00000450
      * CODE TRANSACTION                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(2).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      * ZONE CMD AIDA                                                   00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MZONCMDI  PIC X(15).                                      00000550
      * CICS DE TRAVAIL                                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MCICSI    PIC X(5).                                       00000600
      * NETNAME                                                         00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      * CODE TERMINAL                                                   00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MSCREENI  PIC X(5).                                       00000700
      ***************************************************************** 00000710
      * PARAMETRAGE REGLES DE CUMUL                                     00000720
      ***************************************************************** 00000730
       01   EIF12O REDEFINES EIF12I.                                    00000740
           02 FILLER    PIC X(12).                                      00000750
      * DATE DU JOUR                                                    00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MDATJOUA  PIC X.                                          00000780
           02 MDATJOUC  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
      * HEURE                                                           00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUO  PIC X(5).                                       00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MMODPAIA  PIC X.                                          00000890
           02 MMODPAIC  PIC X.                                          00000900
           02 MMODPAIH  PIC X.                                          00000910
           02 MMODPAIO  PIC X(3).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTYPCTAA  PIC X.                                          00000940
           02 MTYPCTAC  PIC X.                                          00000950
           02 MTYPCTAH  PIC X.                                          00000960
           02 MTYPCTAO  PIC X(3).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MCTYPMTA  PIC X.                                          00000990
           02 MCTYPMTC  PIC X.                                          00001000
           02 MCTYPMTH  PIC X.                                          00001010
           02 MCTYPMTO  PIC X(5).                                       00001020
           02 FILLER  OCCURS   10 TIMES .                               00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MRANGA  PIC X.                                          00001050
             03 MRANGC  PIC X.                                          00001060
             03 MRANGH  PIC X.                                          00001070
             03 MRANGO  PIC ZZ.                                         00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MNOMCHAMPA   PIC X.                                     00001100
             03 MNOMCHAMPC   PIC X.                                     00001110
             03 MNOMCHAMPH   PIC X.                                     00001120
             03 MNOMCHAMPO   PIC X(10).                                 00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MMASQUEA     PIC X.                                     00001150
             03 MMASQUEC     PIC X.                                     00001160
             03 MMASQUEH     PIC X.                                     00001170
             03 MMASQUEO     PIC X(5).                                  00001180
      * MESSAGE ERREUR                                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MLIBERRA  PIC X.                                          00001210
           02 MLIBERRC  PIC X.                                          00001220
           02 MLIBERRH  PIC X.                                          00001230
           02 MLIBERRO  PIC X(79).                                      00001240
      * CODE TRANSACTION                                                00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCODTRAA  PIC X.                                          00001270
           02 MCODTRAC  PIC X.                                          00001280
           02 MCODTRAH  PIC X.                                          00001290
           02 MCODTRAO  PIC X(4).                                       00001300
      * ZONE CMD AIDA                                                   00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MZONCMDA  PIC X.                                          00001330
           02 MZONCMDC  PIC X.                                          00001340
           02 MZONCMDH  PIC X.                                          00001350
           02 MZONCMDO  PIC X(15).                                      00001360
      * CICS DE TRAVAIL                                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCICSA    PIC X.                                          00001390
           02 MCICSC    PIC X.                                          00001400
           02 MCICSH    PIC X.                                          00001410
           02 MCICSO    PIC X(5).                                       00001420
      * NETNAME                                                         00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNETNAMA  PIC X.                                          00001450
           02 MNETNAMC  PIC X.                                          00001460
           02 MNETNAMH  PIC X.                                          00001470
           02 MNETNAMO  PIC X(8).                                       00001480
      * CODE TERMINAL                                                   00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MSCREENA  PIC X.                                          00001510
           02 MSCREENC  PIC X.                                          00001520
           02 MSCREENH  PIC X.                                          00001530
           02 MSCREENO  PIC X(5).                                       00001540
                                                                                
