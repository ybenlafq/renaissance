      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                    TS  BIS DE TGC11                           * 00000020
      ***************************************************************** 00000030
       01 TSGC1B-DATA              PIC X(57).                           00000040
       01 TSGC1B-DATA-1 REDEFINES TSGC1B-DATA.                          00000050
          05 TSGC1B-CPREST-1       PIC X(5).                            00000060
          05 TSGC1B-LPRESTATION-1  PIC X(20).                           00000070
          05 TSGC1B-FILLER-1       PIC X(32).                           00000080
       01 TSGC1B-DATA-2 REDEFINES TSGC1B-DATA.                          00000090
          05 TSGC1B-NCODIC-2       PIC X(7).                            00000100
          05 TSGC1B-CTYPPREST-2    PIC X(5).                            00000110
          05 TSGC1B-LTPREST-2      PIC X(20).                           00000120
          05 TSGC1B-CMARQ-2        PIC X(5).                            00000130
          05 TSGC1B-LMARQ-2        PIC X(20).                           00000140
       01 TSGC1B-DATA-3 REDEFINES TSGC1B-DATA.                          00000150
          05 TSGC1B-COFFRE-3       PIC X(7).                            00000160
          05 TSGC1B-NCODIC-3       PIC X(7).                            00000170
          05 TSGC1B-FILLER-1       PIC X(43).                           00000180
                                                                                
