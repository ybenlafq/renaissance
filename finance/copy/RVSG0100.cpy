      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSG0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSG0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSG0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSG0100.                                                            
      *}                                                                        
           02  SG01-NSOC                                                        
               PIC X(0003).                                                     
           02  SG01-NETAB                                                       
               PIC X(0003).                                                     
           02  SG01-RUBPAIE                                                     
               PIC X(0003).                                                     
           02  SG01-CONTRAT                                                     
               PIC X(0002).                                                     
           02  SG01-CPTEBIL                                                     
               PIC X(0006).                                                     
           02  SG01-SENSBIL                                                     
               PIC X(0001).                                                     
           02  SG01-CPTECHG                                                     
               PIC X(0006).                                                     
           02  SG01-SENSCHG                                                     
               PIC X(0001).                                                     
           02  SG01-CSECTION                                                    
               PIC X(0003).                                                     
           02  SG01-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  SG01-CPTECONT                                                    
               PIC X(0006).                                                     
           02  SG01-SENSCONT                                                    
               PIC X(0001).                                                     
           02  SG01-SECCONT                                                     
               PIC X(0003).                                                     
           02  SG01-RUBRCONT                                                    
               PIC X(0006).                                                     
           02  SG01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSG0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSG0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSG0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-RUBPAIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-RUBPAIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-CONTRAT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-CONTRAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-CPTEBIL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-CPTEBIL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-SENSBIL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-SENSBIL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-CPTECHG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-CPTECHG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-SENSCHG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-SENSCHG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-CSECTION-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-CSECTION-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-CPTECONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-CPTECONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-SENSCONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-SENSCONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-SECCONT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-SECCONT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-RUBRCONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-RUBRCONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
