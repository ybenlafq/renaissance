      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DU FICHIER POUR INSERTION SUR BASE D'IMPRESSION           
      *                  GENERALISEE (JUSQU'A 132 COLONNES)                     
      *                                                                         
       01         FFGEDG-DSECT.                                                 
      *                                                                         
           03     FFGEDG-IDENT.                                                 
001          05   FFGEDG-CETAT         PIC X(06).                               
007          05   FFGEDG-DATE          PIC X(08).                               
015          05   FFGEDG-DEST          PIC X(09).                               
024          05   FFGEDG-DOCUMENT      PIC X(15).                               
039          05   FFGEDG-NSEQ          PIC S9(7) COMP-3.                        
043        03     FFGEDG-ASA           PIC X(01).                               
044        03     FFGEDG-LIGNE         PIC X(132).                              
-=>175*                                                                         
                                                                                
