      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000010
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000020
      **************************************************************    00000030
                                                                        00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-XT00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *                                                                         
      *--                                                                       
       01  COM-XT00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
                                                                        00000060
      *}                                                                        
       01  Z-COMMAREA.                                                  00000070
                                                                        00000080
      * ZONES RESERVEES AIDA ------------------------------------- 100  00000090
          02 FILLER-COM-AIDA      PIC  X(100).                          00000091
                                                                        00000092
      * ZONES RESERVEES CICS ------------------------------------- 020  00000093
          02 COMM-CICS-APPLID     PIC  X(8).                            00000094
          02 COMM-CICS-NETNAM     PIC  X(8).                            00000095
          02 COMM-CICS-TRANSA     PIC  X(4).                            00000096
                                                                        00000097
      * DATE DU JOUR --------------------------------------------- 100  00000098
          02 COMM-DATE-SIECLE     PIC  99.                              00000099
          02 COMM-DATE-ANNEE      PIC  99.                              00000100
          02 COMM-DATE-MOIS       PIC  99.                              00000110
          02 COMM-DATE-JOUR       PIC  99.                              00000120
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000130
          02 COMM-DATE-QNTA       PIC  999.                             00000140
          02 COMM-DATE-QNT0       PIC  99999.                           00000150
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000160
          02 COMM-DATE-BISX       PIC  9.                               00000170
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000180
          02 COMM-DATE-JSM        PIC  9.                               00000190
      *   LIBELLES DU JOUR COURT - LONG                                 00000191
          02 COMM-DATE-JSM-LC     PIC  XXX.                             00000192
          02 COMM-DATE-JSM-LL     PIC  X(8).                            00000193
      *   LIBELLES DU MOIS COURT - LONG                                 00000194
          02 COMM-DATE-MOIS-LC    PIC  XXX.                             00000195
          02 COMM-DATE-MOIS-LL    PIC  X(8).                            00000196
      *   DIFFERENTES FORMES DE DATE                                    00000197
          02 COMM-DATE-SSAAMMJJ   PIC  X(8).                            00000198
          02 COMM-DATE-AAMMJJ     PIC  X(6).                            00000199
          02 COMM-DATE-JJMMSSAA   PIC  X(8).                            00000200
          02 COMM-DATE-JJMMAA     PIC  X(6).                            00000210
          02 COMM-DATE-JJ-MM-AA   PIC  X(8).                            00000220
          02 COMM-DATE-JJ-MM-SSAA PIC  X(10).                           00000230
      *   TRAITEMENT NUMERO DE SEMAINE                                  00000240
          02 COMM-DATE-WEEK.                                            00000250
             05 COMM-DATE-SEMSS   PIC  99.                              00000260
             05 COMM-DATE-SEMAA   PIC  99.                              00000270
             05 COMM-DATE-SEMNU   PIC  99.                              00000280
          02 COMM-DATE-FILLER     PIC  X(08).                           00000290
                                                                        00000291
      * ATTRIBUTS BMS======================================== 4 + AAAA  00000292
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000293
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-XT00-ZMAP       PIC  X(2400).                         00000294
                                                                        00000295
      * ZONES APPLICATIVES XT00 ================================= BBBB  00000296
          02 COMM-XT00-EXTR       PIC  X(5).                            00000302
          02 COMM-XT00-CONSL      PIC  X(5).                            00000302
          02 COMM-XT00-AGRCL      PIC  X(5).                            00000302
          02 COMM-XT00-AGRPR      PIC  X(5).                            00000302
          02 COMM-XT00-CETAT      PIC  X(10).                           00000302
          02 COMM-XT00-WSEQPRO    PIC  S9(7) PACKED-DECIMAL.            00000302
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-XT00-NBPAGES    PIC  S9(4) BINARY.                    00000303
      *--                                                                       
          02 COMM-XT00-NBPAGES    PIC  S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-XT00-PAGEAFF    PIC  S9(4) BINARY.                    00000303
      *--                                                                       
          02 COMM-XT00-PAGEAFF    PIC  S9(4) COMP-5.                            
      *}                                                                        
          02 COMM-XT00-NSOC-1     PIC  X(3)  OCCURS 15.                         
          02 COMM-XT00-NLIEU-1    PIC  X(3)  OCCURS 15.                         
          02 COMM-XT00-NSOC-2     PIC  X(3)  OCCURS 15.                         
          02 COMM-XT00-NLIEU-2    PIC  X(3)  OCCURS 15.                         
          02 COMM-XT00-PF3        PIC  X.                                       
                                                                                
