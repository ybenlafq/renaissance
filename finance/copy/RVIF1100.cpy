      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVIF1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIF1100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF1100.                                                            
      *}                                                                        
           02  IF11-CORGANISME                                                  
               PIC X(0003).                                                     
           02  IF11-DRECEPT                                                     
               PIC X(0008).                                                     
           02  IF11-HRECEPT                                                     
               PIC X(0004).                                                     
           02  IF11-NIDENT                                                      
               PIC X(0020).                                                     
           02  IF11-CMODPAI                                                     
               PIC X(0005).                                                     
           02  IF11-DFINANCE                                                    
               PIC X(0008).                                                     
           02  IF11-DOPER                                                       
               PIC X(0008).                                                     
           02  IF11-DREMISE                                                     
               PIC X(0008).                                                     
           02  IF11-NREMISE                                                     
               PIC X(0006).                                                     
           02  IF11-CODANO                                                      
               PIC X(0002).                                                     
           02  IF11-MTBRUT                                                      
               PIC S9(7)V99 COMP-3.                                             
           02  IF11-MTCOM                                                       
               PIC S9(5)V99 COMP-3.                                             
           02  IF11-MTNET                                                       
               PIC S9(7)V99 COMP-3.                                             
           02  IF11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIF1100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF1100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF1100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-CORGANISME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-CORGANISME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-DRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-DRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-HRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-HRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-NIDENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-NIDENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-CMODPAI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-CMODPAI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-DFINANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-DFINANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-DREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-DREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-NREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-NREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-CODANO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-CODANO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-MTBRUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-MTBRUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-MTCOM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-MTCOM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-MTNET-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-MTNET-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
