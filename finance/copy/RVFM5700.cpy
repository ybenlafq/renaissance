      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM5700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM5700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5700.                                                            
           02  FM57-CODE                                                        
               PIC X(0002).                                                     
           02  FM57-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM57-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM5700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM57-CODE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM57-CODE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM57-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM57-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM57-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM57-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
