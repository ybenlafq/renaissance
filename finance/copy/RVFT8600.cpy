      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFT8600                                     00020000
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT8600                 00060000
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFT8600.                                                    00090000
           02  FT86-SOCREF                                              00100001
               PIC X(0005).                                             00110000
           02  FT86-CINTERFACE                                          00120001
               PIC X(0005).                                             00130001
           02  FT86-CNATOPER                                            00140000
               PIC X(0005).                                             00150000
           02  FT86-CTYPOPER                                            00160000
               PIC X(0005).                                             00170000
           02  FT86-CRITERE1                                            00180000
               PIC X(0005).                                             00190002
           02  FT86-CRITERE2                                            00200000
               PIC X(0005).                                             00210002
           02  FT86-CRITERE3                                            00220000
               PIC X(0005).                                             00230002
           02  FT86-RUBRIQUE                                            00240000
               PIC X(0006).                                             00250000
           02  FT86-RUBRIQUEC                                           00260003
               PIC X(0006).                                             00270003
           02  FT86-DEFFET                                              00280003
               PIC X(0008).                                             00290003
           02  FT86-DMAJ                                                00300000
               PIC X(0008).                                             00310000
           02  FT86-DSYST                                               00320000
               PIC S9(13) COMP-3.                                       00330000
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVFT8600                          00360000
      *---------------------------------------------------------        00370000
      *                                                                 00380000
       01  RVFT8600-FLAGS.                                              00390000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-SOCREF-F                                            00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  FT86-SOCREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CINTERFACE-F                                        00420001
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  FT86-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CNATOPER-F                                          00440001
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  FT86-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CTYPOPER-F                                          00460001
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  FT86-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CRITERE1-F                                          00480001
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  FT86-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CRITERE2-F                                          00500001
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  FT86-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-CRITERE3-F                                          00520001
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  FT86-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-RUBRIQUE-F                                          00540001
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  FT86-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-RUBRIQUEC-F                                         00560003
      *        PIC S9(4) COMP.                                          00570003
      *--                                                                       
           02  FT86-RUBRIQUEC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-DEFFET-F                                            00580000
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  FT86-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-DMAJ-F                                              00600000
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  FT86-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT86-DSYST-F                                             00620000
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  FT86-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
