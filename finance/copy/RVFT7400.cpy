      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT7400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT7400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT7400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT7400.                                                            
      *}                                                                        
           02  FT74-NSOC                                                        
               PIC X(0003).                                                     
           02  FT74-NETAB                                                       
               PIC X(0003).                                                     
           02  FT74-CNOMPROG                                                    
               PIC X(0008).                                                     
           02  FT74-DDATE                                                       
               PIC X(0008).                                                     
           02  FT74-NBMVT                                                       
               PIC S9(5) COMP-3.                                                
           02  FT74-NTOTPIECE                                                   
               PIC S9(5) COMP-3.                                                
           02  FT74-NVALPIECE                                                   
               PIC S9(5) COMP-3.                                                
           02  FT74-NRECPIECE                                                   
               PIC S9(5) COMP-3.                                                
           02  FT74-CDEMANDE                                                    
               PIC X(45).                                                       
           02  FT74-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT7400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT7400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT7400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-CNOMPROG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-CNOMPROG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-DDATE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-DDATE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NBMVT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NBMVT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NTOTPIECE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NTOTPIECE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NVALPIECE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NVALPIECE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-NRECPIECE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-NRECPIECE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-CDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-CDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT74-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT74-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
