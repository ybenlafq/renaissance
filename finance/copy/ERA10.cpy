      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA10   ERA10                                              00000020
      ***************************************************************** 00000030
       01   ERA10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * MAGASIN CEDANT                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNSOCI    PIC X(3).                                       00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MNLIEUI   PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MZONCMDI  PIC X(2).                                       00000280
      * MAGASIN CEDANT                                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNDEML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNDEMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNDEMI    PIC X(5).                                       00000330
      * MAGASIN CEDANT                                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MNOMI     PIC X(25).                                      00000380
      * MAGASIN CEDANT                                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDECHEANI      PIC X(10).                                 00000430
      * MESSAGE ERREUR                                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLIBERRI  PIC X(78).                                      00000480
      * CODE TRANSACTION                                                00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODTRAI  PIC X(4).                                       00000530
      * CICS DE TRAVAIL                                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      * NETNAME                                                         00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNETNAMI  PIC X(8).                                       00000630
      * CODE TERMINAL                                                   00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MSCREENI  PIC X(5).                                       00000680
      ***************************************************************** 00000690
      * SDF: ERA10   ERA10                                              00000700
      ***************************************************************** 00000710
       01   ERA10O REDEFINES ERA10I.                                    00000720
           02 FILLER    PIC X(12).                                      00000730
      * DATE DU JOUR                                                    00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
      * HEURE                                                           00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
      * MAGASIN CEDANT                                                  00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MNSOCA    PIC X.                                          00000920
           02 MNSOCC    PIC X.                                          00000930
           02 MNSOCP    PIC X.                                          00000940
           02 MNSOCH    PIC X.                                          00000950
           02 MNSOCV    PIC X.                                          00000960
           02 MNSOCO    PIC X(3).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNLIEUA   PIC X.                                          00000990
           02 MNLIEUC   PIC X.                                          00001000
           02 MNLIEUP   PIC X.                                          00001010
           02 MNLIEUH   PIC X.                                          00001020
           02 MNLIEUV   PIC X.                                          00001030
           02 MNLIEUO   PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MZONCMDA  PIC X.                                          00001060
           02 MZONCMDC  PIC X.                                          00001070
           02 MZONCMDP  PIC X.                                          00001080
           02 MZONCMDH  PIC X.                                          00001090
           02 MZONCMDV  PIC X.                                          00001100
           02 MZONCMDO  PIC X(2).                                       00001110
      * MAGASIN CEDANT                                                  00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MNDEMA    PIC X.                                          00001140
           02 MNDEMC    PIC X.                                          00001150
           02 MNDEMP    PIC X.                                          00001160
           02 MNDEMH    PIC X.                                          00001170
           02 MNDEMV    PIC X.                                          00001180
           02 MNDEMO    PIC X(5).                                       00001190
      * MAGASIN CEDANT                                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNOMA     PIC X.                                          00001220
           02 MNOMC     PIC X.                                          00001230
           02 MNOMP     PIC X.                                          00001240
           02 MNOMH     PIC X.                                          00001250
           02 MNOMV     PIC X.                                          00001260
           02 MNOMO     PIC X(25).                                      00001270
      * MAGASIN CEDANT                                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MDECHEANA      PIC X.                                     00001300
           02 MDECHEANC PIC X.                                          00001310
           02 MDECHEANP PIC X.                                          00001320
           02 MDECHEANH PIC X.                                          00001330
           02 MDECHEANV PIC X.                                          00001340
           02 MDECHEANO      PIC X(10).                                 00001350
      * MESSAGE ERREUR                                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLIBERRA  PIC X.                                          00001380
           02 MLIBERRC  PIC X.                                          00001390
           02 MLIBERRP  PIC X.                                          00001400
           02 MLIBERRH  PIC X.                                          00001410
           02 MLIBERRV  PIC X.                                          00001420
           02 MLIBERRO  PIC X(78).                                      00001430
      * CODE TRANSACTION                                                00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCODTRAA  PIC X.                                          00001460
           02 MCODTRAC  PIC X.                                          00001470
           02 MCODTRAP  PIC X.                                          00001480
           02 MCODTRAH  PIC X.                                          00001490
           02 MCODTRAV  PIC X.                                          00001500
           02 MCODTRAO  PIC X(4).                                       00001510
      * CICS DE TRAVAIL                                                 00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCICSA    PIC X.                                          00001540
           02 MCICSC    PIC X.                                          00001550
           02 MCICSP    PIC X.                                          00001560
           02 MCICSH    PIC X.                                          00001570
           02 MCICSV    PIC X.                                          00001580
           02 MCICSO    PIC X(5).                                       00001590
      * NETNAME                                                         00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNETNAMA  PIC X.                                          00001620
           02 MNETNAMC  PIC X.                                          00001630
           02 MNETNAMP  PIC X.                                          00001640
           02 MNETNAMH  PIC X.                                          00001650
           02 MNETNAMV  PIC X.                                          00001660
           02 MNETNAMO  PIC X(8).                                       00001670
      * CODE TERMINAL                                                   00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSCREENA  PIC X.                                          00001700
           02 MSCREENC  PIC X.                                          00001710
           02 MSCREENP  PIC X.                                          00001720
           02 MSCREENH  PIC X.                                          00001730
           02 MSCREENV  PIC X.                                          00001740
           02 MSCREENO  PIC X(5).                                       00001750
                                                                                
