      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFT63                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6300.                                                            
      *}                                                                        
           10 FT63-NSOC            PIC X(5).                                    
           10 FT63-NETAB           PIC X(3).                                    
           10 FT63-LETABC          PIC X(15).                                   
           10 FT63-LETABL          PIC X(30).                                   
           10 FT63-CMASQUE         PIC X(3).                                    
           10 FT63-DCLOTURE        PIC X(8).                                    
           10 FT63-DMAJ            PIC X(8).                                    
           10 FT63-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
