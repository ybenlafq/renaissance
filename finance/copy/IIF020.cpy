      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIF020 AU 13/11/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,08,BI,A,                          *        
      *                           33,08,BI,A,                          *        
      *                           41,08,BI,A,                          *        
      *                           49,06,BI,A,                          *        
      *                           55,05,BI,A,                          *        
      *                           60,19,BI,A,                          *        
      *                           79,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIF020.                                                        
            05 NOMETAT-IIF020           PIC X(6) VALUE 'IIF020'.                
            05 RUPTURES-IIF020.                                                 
           10 IIF020-NSOCCOMPT          PIC X(03).                      007  003
           10 IIF020-NETAB              PIC X(03).                      010  003
           10 IIF020-CORGANISME         PIC X(03).                      013  003
           10 IIF020-CMODPAI            PIC X(03).                      016  003
           10 IIF020-NSOCIETE           PIC X(03).                      019  003
           10 IIF020-NLIEU              PIC X(03).                      022  003
           10 IIF020-DOPER              PIC X(08).                      025  008
           10 IIF020-DFINANCE           PIC X(08).                      033  008
           10 IIF020-DREMISE            PIC X(08).                      041  008
           10 IIF020-NREMISE            PIC X(06).                      049  006
           10 IIF020-WNSEQ              PIC 9(05).                      055  005
           10 IIF020-NPORTEUR           PIC X(19).                      060  019
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIF020-SEQUENCE           PIC S9(04) COMP.                079  002
      *--                                                                       
           10 IIF020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIF020.                                                   
           10 IIF020-LLIEU              PIC X(20).                      081  020
           10 IIF020-MTBRUT             PIC S9(09)V9(2) COMP-3.         101  006
           10 IIF020-MTCOM              PIC S9(07)V9(2) COMP-3.         107  005
           10 IIF020-MTNET              PIC S9(09)V9(2) COMP-3.         112  006
            05 FILLER                      PIC X(395).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIF020-LONG           PIC S9(4)   COMP  VALUE +117.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIF020-LONG           PIC S9(4) COMP-5  VALUE +117.           
                                                                                
      *}                                                                        
