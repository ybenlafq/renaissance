      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FIFAC FACTURAT� CONSOMMABLES           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFIFAC.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFIFAC.                                                             
      *}                                                                        
           05  FIFAC-CTABLEG2    PIC X(15).                                     
           05  FIFAC-CTABLEG2-REDEF REDEFINES FIFAC-CTABLEG2.                   
               10  FIFAC-NCODIC          PIC X(07).                             
           05  FIFAC-WTABLEG     PIC X(80).                                     
           05  FIFAC-WTABLEG-REDEF  REDEFINES FIFAC-WTABLEG.                    
               10  FIFAC-WACTIF          PIC X(01).                             
               10  FIFAC-CVENT           PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFIFAC-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFIFAC-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FIFAC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FIFAC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FIFAC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FIFAC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
