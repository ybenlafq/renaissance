      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCTXP GC TYPES DE TAUX DE DECOMMISSI   *        
      *----------------------------------------------------------------*        
       01  RVGCTXP.                                                             
           05  GCTXP-CTABLEG2    PIC X(15).                                     
           05  GCTXP-CTABLEG2-REDEF REDEFINES GCTXP-CTABLEG2.                   
               10  GCTXP-CTYPTXP         PIC X(05).                             
           05  GCTXP-WTABLEG     PIC X(80).                                     
           05  GCTXP-WTABLEG-REDEF  REDEFINES GCTXP-WTABLEG.                    
               10  GCTXP-WACTIF          PIC X(01).                             
               10  GCTXP-LIBELLE         PIC X(20).                             
               10  GCTXP-WGESTION        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGCTXP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCTXP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCTXP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCTXP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCTXP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
