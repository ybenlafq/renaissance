      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE FT1000                       
      ******************************************************************        
      *                                                                         
       CLEF-FT1000             SECTION.                                         
      *                                                                         
           MOVE 'RVFT1000          '       TO   TABLE-NAME.                     
           MOVE 'FT1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-FT1000. EXIT.                                                   
                EJECT                                                           
                                                                                
