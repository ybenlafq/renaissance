      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INTERRO SUIVI DES REJETS                                        00000020
      ***************************************************************** 00000030
       01   EIF51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGANL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCORGANF  PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MCORGANI  PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORGANL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLORGANF  PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MLORGANI  PIC X(25).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MDRECEPTI      PIC X(10).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDTRAITL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MDDTRAITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDTRAITF      PIC X.                                     00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MDDTRAITI      PIC X(10).                                 00000390
           02 FILLER  OCCURS   11 TIMES .                               00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000420
             03 FILLER  PIC X(2).                                       00000430
             03 MNSOCIETEI   PIC X(3).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000460
             03 FILLER  PIC X(2).                                       00000470
             03 MNLIEUI      PIC X(3).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODPAIL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCMODPAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCMODPAIF    PIC X.                                     00000500
             03 FILLER  PIC X(2).                                       00000510
             03 MCMODPAII    PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODANOL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCODANOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODANOF     PIC X.                                     00000540
             03 FILLER  PIC X(2).                                       00000550
             03 MCODANOI     PIC X(2).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIDENTL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MNIDENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNIDENTF     PIC X.                                     00000580
             03 FILLER  PIC X(2).                                       00000590
             03 MNIDENTI     PIC X(20).                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINANCEL   COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MDFINANCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDFINANCEF   PIC X.                                     00000620
             03 FILLER  PIC X(2).                                       00000630
             03 MDFINANCEI   PIC X(10).                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDOPERL      COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MDOPERL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDOPERF      PIC X.                                     00000660
             03 FILLER  PIC X(2).                                       00000670
             03 MDOPERI      PIC X(10).                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDREMISEL    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MDREMISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDREMISEF    PIC X.                                     00000700
             03 FILLER  PIC X(2).                                       00000710
             03 MDREMISEI    PIC X(10).                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREMISEL    COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MNREMISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNREMISEF    PIC X.                                     00000740
             03 FILLER  PIC X(2).                                       00000750
             03 MNREMISEI    PIC X(6).                                  00000760
      * MESSAGE ERREUR                                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MLIBERRI  PIC X(79).                                      00000810
      * CODE TRANSACTION                                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      * ZONE CMD AIDA                                                   00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MZONCMDI  PIC X(15).                                      00000910
      * CICS DE TRAVAIL                                                 00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MCICSI    PIC X(5).                                       00000960
      * NETNAME                                                         00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNETNAMI  PIC X(8).                                       00001010
      * CODE TERMINAL                                                   00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MSCREENI  PIC X(5).                                       00001060
      ***************************************************************** 00001070
      * INTERRO SUIVI DES REJETS                                        00001080
      ***************************************************************** 00001090
       01   EIF51O REDEFINES EIF51I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
      * DATE DU JOUR                                                    00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUO  PIC X(10).                                      00001170
      * HEURE                                                           00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUH  PIC X.                                          00001220
           02 MTIMJOUO  PIC X(5).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNPAGEA   PIC X.                                          00001250
           02 MNPAGEC   PIC X.                                          00001260
           02 MNPAGEH   PIC X.                                          00001270
           02 MNPAGEO   PIC ZZ.                                         00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNBPAGESA      PIC X.                                     00001300
           02 MNBPAGESC PIC X.                                          00001310
           02 MNBPAGESH PIC X.                                          00001320
           02 MNBPAGESO      PIC ZZ.                                    00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCORGANA  PIC X.                                          00001350
           02 MCORGANC  PIC X.                                          00001360
           02 MCORGANH  PIC X.                                          00001370
           02 MCORGANO  PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLORGANA  PIC X.                                          00001400
           02 MLORGANC  PIC X.                                          00001410
           02 MLORGANH  PIC X.                                          00001420
           02 MLORGANO  PIC X(25).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDRECEPTA      PIC X.                                     00001450
           02 MDRECEPTC PIC X.                                          00001460
           02 MDRECEPTH PIC X.                                          00001470
           02 MDRECEPTO      PIC X(10).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDDTRAITA      PIC X.                                     00001500
           02 MDDTRAITC PIC X.                                          00001510
           02 MDDTRAITH PIC X.                                          00001520
           02 MDDTRAITO      PIC X(10).                                 00001530
           02 FILLER  OCCURS   11 TIMES .                               00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MNSOCIETEA   PIC X.                                     00001560
             03 MNSOCIETEC   PIC X.                                     00001570
             03 MNSOCIETEH   PIC X.                                     00001580
             03 MNSOCIETEO   PIC X(3).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MNLIEUA      PIC X.                                     00001610
             03 MNLIEUC PIC X.                                          00001620
             03 MNLIEUH PIC X.                                          00001630
             03 MNLIEUO      PIC X(3).                                  00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MCMODPAIA    PIC X.                                     00001660
             03 MCMODPAIC    PIC X.                                     00001670
             03 MCMODPAIH    PIC X.                                     00001680
             03 MCMODPAIO    PIC X(5).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MCODANOA     PIC X.                                     00001710
             03 MCODANOC     PIC X.                                     00001720
             03 MCODANOH     PIC X.                                     00001730
             03 MCODANOO     PIC X(2).                                  00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MNIDENTA     PIC X.                                     00001760
             03 MNIDENTC     PIC X.                                     00001770
             03 MNIDENTH     PIC X.                                     00001780
             03 MNIDENTO     PIC X(20).                                 00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MDFINANCEA   PIC X.                                     00001810
             03 MDFINANCEC   PIC X.                                     00001820
             03 MDFINANCEH   PIC X.                                     00001830
             03 MDFINANCEO   PIC X(10).                                 00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MDOPERA      PIC X.                                     00001860
             03 MDOPERC PIC X.                                          00001870
             03 MDOPERH PIC X.                                          00001880
             03 MDOPERO      PIC X(10).                                 00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MDREMISEA    PIC X.                                     00001910
             03 MDREMISEC    PIC X.                                     00001920
             03 MDREMISEH    PIC X.                                     00001930
             03 MDREMISEO    PIC X(10).                                 00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MNREMISEA    PIC X.                                     00001960
             03 MNREMISEC    PIC X.                                     00001970
             03 MNREMISEH    PIC X.                                     00001980
             03 MNREMISEO    PIC X(6).                                  00001990
      * MESSAGE ERREUR                                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLIBERRA  PIC X.                                          00002020
           02 MLIBERRC  PIC X.                                          00002030
           02 MLIBERRH  PIC X.                                          00002040
           02 MLIBERRO  PIC X(79).                                      00002050
      * CODE TRANSACTION                                                00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCODTRAA  PIC X.                                          00002080
           02 MCODTRAC  PIC X.                                          00002090
           02 MCODTRAH  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
      * ZONE CMD AIDA                                                   00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDH  PIC X.                                          00002160
           02 MZONCMDO  PIC X(15).                                      00002170
      * CICS DE TRAVAIL                                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCICSA    PIC X.                                          00002200
           02 MCICSC    PIC X.                                          00002210
           02 MCICSH    PIC X.                                          00002220
           02 MCICSO    PIC X(5).                                       00002230
      * NETNAME                                                         00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNETNAMA  PIC X.                                          00002260
           02 MNETNAMC  PIC X.                                          00002270
           02 MNETNAMH  PIC X.                                          00002280
           02 MNETNAMO  PIC X(8).                                       00002290
      * CODE TERMINAL                                                   00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MSCREENA  PIC X.                                          00002320
           02 MSCREENC  PIC X.                                          00002330
           02 MSCREENH  PIC X.                                          00002340
           02 MSCREENO  PIC X(5).                                       00002350
                                                                                
