      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu PARAMETRAGE TYPES COMM                                     00000020
      ***************************************************************** 00000030
       01   EGC41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCNDL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCTYPCNDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPCNDF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCTYPCNDI      PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELLEI      PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWINCCONDL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MWINCCONDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWINCCONDF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWINCCONDI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRGLTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCRGLTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRGLTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCRGLTI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFACTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFACTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFACTI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMTVENTEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MWMTVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWMTVENTEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MWMTVENTEI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSEQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSEQF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSEQI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MZONCMDI  PIC X(15).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBERRI  PIC X(58).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODTRAI  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCICSI    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNETNAMI  PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCREENI  PIC X(4).                                       00000650
      ***************************************************************** 00000660
      * menu PARAMETRAGE TYPES COMM                                     00000670
      ***************************************************************** 00000680
       01   EGC41O REDEFINES EGC41I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MDATJOUA  PIC X.                                          00000720
           02 MDATJOUC  PIC X.                                          00000730
           02 MDATJOUP  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUV  PIC X.                                          00000760
           02 MDATJOUO  PIC X(10).                                      00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MTIMJOUA  PIC X.                                          00000790
           02 MTIMJOUC  PIC X.                                          00000800
           02 MTIMJOUP  PIC X.                                          00000810
           02 MTIMJOUH  PIC X.                                          00000820
           02 MTIMJOUV  PIC X.                                          00000830
           02 MTIMJOUO  PIC X(5).                                       00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MCTYPCNDA      PIC X.                                     00000860
           02 MCTYPCNDC PIC X.                                          00000870
           02 MCTYPCNDP PIC X.                                          00000880
           02 MCTYPCNDH PIC X.                                          00000890
           02 MCTYPCNDV PIC X.                                          00000900
           02 MCTYPCNDO      PIC X(5).                                  00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MLIBELLEA      PIC X.                                     00000930
           02 MLIBELLEC PIC X.                                          00000940
           02 MLIBELLEP PIC X.                                          00000950
           02 MLIBELLEH PIC X.                                          00000960
           02 MLIBELLEV PIC X.                                          00000970
           02 MLIBELLEO      PIC X(20).                                 00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MWINCCONDA     PIC X.                                     00001000
           02 MWINCCONDC     PIC X.                                     00001010
           02 MWINCCONDP     PIC X.                                     00001020
           02 MWINCCONDH     PIC X.                                     00001030
           02 MWINCCONDV     PIC X.                                     00001040
           02 MWINCCONDO     PIC X(3).                                  00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MCRGLTA   PIC X.                                          00001070
           02 MCRGLTC   PIC X.                                          00001080
           02 MCRGLTP   PIC X.                                          00001090
           02 MCRGLTH   PIC X.                                          00001100
           02 MCRGLTV   PIC X.                                          00001110
           02 MCRGLTO   PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCFACTA   PIC X.                                          00001140
           02 MCFACTC   PIC X.                                          00001150
           02 MCFACTP   PIC X.                                          00001160
           02 MCFACTH   PIC X.                                          00001170
           02 MCFACTV   PIC X.                                          00001180
           02 MCFACTO   PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MWMTVENTEA     PIC X.                                     00001210
           02 MWMTVENTEC     PIC X.                                     00001220
           02 MWMTVENTEP     PIC X.                                     00001230
           02 MWMTVENTEH     PIC X.                                     00001240
           02 MWMTVENTEV     PIC X.                                     00001250
           02 MWMTVENTEO     PIC X(3).                                  00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNSEQA    PIC X.                                          00001280
           02 MNSEQC    PIC X.                                          00001290
           02 MNSEQP    PIC X.                                          00001300
           02 MNSEQH    PIC X.                                          00001310
           02 MNSEQV    PIC X.                                          00001320
           02 MNSEQO    PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MZONCMDA  PIC X.                                          00001350
           02 MZONCMDC  PIC X.                                          00001360
           02 MZONCMDP  PIC X.                                          00001370
           02 MZONCMDH  PIC X.                                          00001380
           02 MZONCMDV  PIC X.                                          00001390
           02 MZONCMDO  PIC X(15).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLIBERRA  PIC X.                                          00001420
           02 MLIBERRC  PIC X.                                          00001430
           02 MLIBERRP  PIC X.                                          00001440
           02 MLIBERRH  PIC X.                                          00001450
           02 MLIBERRV  PIC X.                                          00001460
           02 MLIBERRO  PIC X(58).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCODTRAA  PIC X.                                          00001490
           02 MCODTRAC  PIC X.                                          00001500
           02 MCODTRAP  PIC X.                                          00001510
           02 MCODTRAH  PIC X.                                          00001520
           02 MCODTRAV  PIC X.                                          00001530
           02 MCODTRAO  PIC X(4).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCICSA    PIC X.                                          00001560
           02 MCICSC    PIC X.                                          00001570
           02 MCICSP    PIC X.                                          00001580
           02 MCICSH    PIC X.                                          00001590
           02 MCICSV    PIC X.                                          00001600
           02 MCICSO    PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNETNAMA  PIC X.                                          00001630
           02 MNETNAMC  PIC X.                                          00001640
           02 MNETNAMP  PIC X.                                          00001650
           02 MNETNAMH  PIC X.                                          00001660
           02 MNETNAMV  PIC X.                                          00001670
           02 MNETNAMO  PIC X(8).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSCREENA  PIC X.                                          00001700
           02 MSCREENC  PIC X.                                          00001710
           02 MSCREENP  PIC X.                                          00001720
           02 MSCREENH  PIC X.                                          00001730
           02 MSCREENV  PIC X.                                          00001740
           02 MSCREENO  PIC X(4).                                       00001750
                                                                                
