      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ ENVOI FOURNISSEUR SAP                                 
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
            10 MQ10-MSGID                  PIC X(24).                           
            10 MQ10-CORRELID               PIC X(24).                           
         02 WS-CODRET                      PIC X(02).                           
         02 WS-MESSAGE.                                                         
            05 MESS-ENTETE.                                                     
               10 MES-TYPE                 PIC X(03).                           
               10 MES-NSOCMSG              PIC X(03).                           
               10 MES-NLIEUMSG             PIC X(03).                           
               10 MES-NSOCDST              PIC X(03).                           
               10 MES-NLIEUDST             PIC X(03).                           
               10 MES-NORD                 PIC 9(08).                           
               10 MES-LPROG                PIC X(10).                           
               10 MES-DJOUR                PIC X(08).                           
               10 MES-WSID                 PIC X(10).                           
               10 MES-USER                 PIC X(10).                           
               10 MES-CHRONO               PIC 9(07).                           
               10 MES-NBRMSG               PIC 9(07).                           
               10 MES-NBRENR               PIC 9(05).                           
               10 MES-TAILLE               PIC 9(05).                           
               10 MES-FILLER               PIC X(20).                           
            05 MES-MESSAGE                 PIC X(2200).                         
      *     MESSAGE DETAIL                                                      
            05 MES-MESSAGE-PLAT REDEFINES MES-MESSAGE.                          
               10 MES-CFONC                 PIC X(03).                          
               10 MES-NENTCDE               PIC X(05).                          
               10 MES-LENTCDE               PIC X(20).                          
               10 MES-LENTCDEADR1           PIC X(32).                          
               10 MES-LENTCDEADR2           PIC X(32).                          
               10 MES-NENTCDEADR3           PIC X(05).                          
               10 MES-LENTCDEADR4           PIC X(26).                          
               10 MES-NENTCDETEL            PIC X(15).                          
               10 MES-CENTCDETELX           PIC X(15).                          
               10 MES-CDEVISE               PIC X(03).                          
               10 MES-CPAYS                 PIC X(02).                          
               10 MES-WIMPORT               PIC X(01).                          
               10 MES-CECO                  PIC X(03).                          
               10 MES-CCOMPTE               PIC X(02).                          
               10 MES-WEDI                  PIC X(01).                          
               10 MES-DEFFETEDI             PIC X(08).                          
               10 FILLER                    PIC X(2027).                        
                                                                                
