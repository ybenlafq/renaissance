      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS SAP *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MCG03                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MCG03-LONG-COMMAREA       PIC S9(4) COMP VALUE +309.             
      *--                                                                       
       01 COMM-MCG03-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +309.           
      *}                                                                        
       01 COMM-MCG03-APPLI.                                                     
      *                                                                         
          02 COMM-MCG03-ENTREE.                                                 
      *------------------------------------------------------------09           
             05 COMM-MCG03-PGRM         PIC X(5).                               
             05 COMM-MCG03-NTERMID      PIC X(4).                               
      *      DONNEES GENERALES ----------------------------- 100                
             05 COMM-MCG03-CFONC        PIC X(03).                              
             05 COMM-MCG03-NENTCDE      PIC X(05).                              
             05 FILLER                  PIC X(92).                              
      *                                                                         
          02 COMM-MCG03-SORTIE.                                                 
      *            MESSAGE DE SORTIE ----------------------- 100                
             05 COMM-MCG03-MESSAGE.                                             
                  10 COMM-MCG03-CODRET              PIC X(1).                   
                     88 COMM-MCG03-OK              VALUE ' '.                   
                     88 COMM-MCG03-ERR             VALUE '1'.                   
                  10 COMM-MCG03-LIBERR             PIC X(58).                   
                  10 FILLER                        PIC X(41).                   
          02  FILLER                               PIC X(100).                  
      *                                                                         
                                                                                
