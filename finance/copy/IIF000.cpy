      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIF000 AU 11/11/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,20,BI,A,                          *        
      *                           45,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIF000.                                                        
            05 NOMETAT-IIF000           PIC X(6) VALUE 'IIF000'.                
            05 RUPTURES-IIF000.                                                 
           10 IIF000-NSOCCOMPT          PIC X(03).                      007  003
           10 IIF000-NLIEUCOMPT         PIC X(03).                      010  003
           10 IIF000-CTYPCTA            PIC X(03).                      013  003
           10 IIF000-CORGANISME         PIC X(03).                      016  003
           10 IIF000-NSOCIETE           PIC X(03).                      019  003
           10 IIF000-NLIEU              PIC X(03).                      022  003
           10 IIF000-NIDENT             PIC X(20).                      025  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIF000-SEQUENCE           PIC S9(04) COMP.                045  002
      *--                                                                       
           10 IIF000-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIF000.                                                   
           10 IIF000-CODANO             PIC X(02).                      047  002
           10 IIF000-NREMISE            PIC X(06).                      049  006
           10 IIF000-TYPANO             PIC X(01).                      055  001
           10 IIF000-MTBRUT             PIC S9(09)V9(2) COMP-3.         056  006
           10 IIF000-MTCOM              PIC S9(07)V9(2) COMP-3.         062  005
           10 IIF000-MTNET              PIC S9(09)V9(2) COMP-3.         067  006
           10 IIF000-DFINANCE           PIC X(08).                      073  008
           10 IIF000-DOPER              PIC X(08).                      081  008
           10 IIF000-DREMISE            PIC X(08).                      089  008
            05 FILLER                      PIC X(416).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIF000-LONG           PIC S9(4)   COMP  VALUE +096.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIF000-LONG           PIC S9(4) COMP-5  VALUE +096.           
                                                                                
      *}                                                                        
