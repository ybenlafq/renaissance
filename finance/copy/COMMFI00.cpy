      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -AIDA ********************************************************* 00000010
      *  ZONES DE COMMAREA                                              00000020
      ***************************************************************** 00000030
      **************************************************************    00000040
      * COMMAREA SPECIFIQUE PRG TFI00 (MENU)             TR: FI00  *    00000050
      *                                                            *    00000080
      *           POUR L'ADMINISTATION DES DONNEES                 *    00000094
      **************************************************************    00000095
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000096
      **************************************************************    00000097
      *                                                                 00000098
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000099
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000100
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000110
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00000120
      *                                                                 00000130
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00000140
      * COMPRENANT :                                                    00000150
      * 1 - LES ZONES RESERVEES A AIDA                                  00000160
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00000170
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00000180
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00000190
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00000191
      *                                                                 00000192
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00000193
      * PAR AIDA                                                        00000194
      *                                                                 00000195
      *-------------------------------------------------------------    00000196
      *                                                                 00000197
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-FI00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000198
      *--                                                                       
       01  COM-FI00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000199
       01  Z-COMMAREA.                                                  00000200
      *                                                                 00000210
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000220
          02 FILLER-COM-AIDA      PIC X(100).                           00000230
      *                                                                 00000240
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000250
          02 COMM-CICS-APPLID     PIC X(8).                             00000260
          02 COMM-CICS-NETNAM     PIC X(8).                             00000270
          02 COMM-CICS-TRANSA     PIC X(4).                             00000280
      *                                                                 00000290
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000291
          02 COMM-DATE-SIECLE     PIC XX.                               00000292
          02 COMM-DATE-ANNEE      PIC XX.                               00000293
          02 COMM-DATE-MOIS       PIC XX.                               00000294
          02 COMM-DATE-JOUR       PIC XX.                               00000295
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000296
          02 COMM-DATE-QNTA       PIC 999.                              00000297
          02 COMM-DATE-QNT0       PIC 99999.                            00000298
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000299
          02 COMM-DATE-BISX       PIC 9.                                00000300
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000310
          02 COMM-DATE-JSM        PIC 9.                                00000320
      *   LIBELLES DU JOUR COURT - LONG                                 00000330
          02 COMM-DATE-JSM-LC     PIC XXX.                              00000340
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00000350
      *   LIBELLES DU MOIS COURT - LONG                                 00000360
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00000370
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00000380
      *   DIFFERENTES FORMES DE DATE                                    00000390
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00000391
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00000392
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00000393
          02 COMM-DATE-JJMMAA     PIC X(6).                             00000394
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00000395
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00000396
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000397
          02 COMM-DATE-WEEK.                                            00000398
             05  COMM-DATE-SEMSS  PIC 99.                               00000399
             05  COMM-DATE-SEMAA  PIC 99.                               00000400
             05  COMM-DATE-SEMNU  PIC 99.                               00000410
          02 COMM-DATE-FILLER     PIC X(08).                            00000420
      *                                                                 00000430
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00000440
      *                                                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000460
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00000470
      *                                                                 00000480
      * RESTE ---------------------------->-------------------->-3724-* 00000490
      * ZONES SPECIFIQUE POUR FI00 ------->-------------------->--724-* 00000491
      *                                                                 00000492
          02 COMM-FI00-APPLI.                                           00000493
             03  COMM-FI00-FONCT      PIC X(03).                        00000494
             03  COMM-FI00-NENTCDE    PIC X(05).                        00000495
             03  COMM-FI00-CFAM       PIC X(05).                        00000496
             03  COMM-FI00-CPROFIL    PIC X(05).                        00000498
             03  COMM-FI00-LPROFIL    PIC X(20).                        00000499
             03  COMM-FI00-CODRET     PIC X(01).                        00000498
             03  COMM-FI00-MESSAGE    PIC X(60).                        00000499
             03  COMM-FI00-FILLER     PIC X(625).                       00000500
      * ZONES LIBRE POUR LES SOUS TRANSACTIONS >--------------->-3000-* 00000501
             03  COMM-FI00-LIBRE      PIC X(3000).                      00000510
      *                                                                 00000600
                                                                                
