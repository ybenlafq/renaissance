      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM6500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM6500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6500.                                                            
           02  FM65-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM65-CMASQUEC                                                    
               PIC X(0006).                                                     
           02  FM65-CMASQUES                                                    
               PIC X(0006).                                                     
           02  FM65-CMASQUER                                                    
               PIC X(0006).                                                     
           02  FM65-CMASQUEE                                                    
               PIC X(0003).                                                     
           02  FM65-CMASQUEA                                                    
               PIC X(0005).                                                     
           02  FM65-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FM65-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM6500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-CMASQUEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-CMASQUEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-CMASQUES-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-CMASQUES-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-CMASQUER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-CMASQUER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-CMASQUEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-CMASQUEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-CMASQUEA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-CMASQUEA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM65-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM65-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
