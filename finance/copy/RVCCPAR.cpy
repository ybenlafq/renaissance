      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CCPAR CONTROLE CAISSES : PARAMETRES    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCCPAR.                                                             
           05  CCPAR-CTABLEG2    PIC X(15).                                     
           05  CCPAR-CTABLEG2-REDEF REDEFINES CCPAR-CTABLEG2.                   
               10  CCPAR-TYPPAR          PIC X(03).                             
           05  CCPAR-WTABLEG     PIC X(80).                                     
           05  CCPAR-WTABLEG-REDEF  REDEFINES CCPAR-WTABLEG.                    
               10  CCPAR-ELTPAR          PIC X(12).                             
               10  CCPAR-WACTIF          PIC X(01).                             
               10  CCPAR-LIBELLE         PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCCPAR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCPAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CCPAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCPAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CCPAR-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
