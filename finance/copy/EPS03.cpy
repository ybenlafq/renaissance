      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPS03   EPS03                                              00000020
      ***************************************************************** 00000030
       01   EPS03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGETL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGETF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
           02 MLIGNEI OCCURS   16 TIMES .                               00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGCPLTL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MCGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGCPLTF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCGCPLTI     PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNGCPLTL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNGCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNGCPLTF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNGCPLTI     PIC X(8).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNLIEUI      PIC X(3).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNVENTEI     PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCLIL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNOMCLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNOMCLIF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNOMCLII     PIC X(15).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTCL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MMTTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMTTCF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MMTTCI  PIC X(7).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTREMBL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MTREMBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTREMBF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MTREMBI      PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCHEQUEL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCHEQUEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCHEQUEF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCHEQUEI    PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(78).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: EPS03   EPS03                                              00000800
      ***************************************************************** 00000810
       01   EPS03O REDEFINES EPS03I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEP    PIC X.                                          00001010
           02 MPAGEH    PIC X.                                          00001020
           02 MPAGEV    PIC X.                                          00001030
           02 MPAGEO    PIC X(2).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MPAGETA   PIC X.                                          00001060
           02 MPAGETC   PIC X.                                          00001070
           02 MPAGETP   PIC X.                                          00001080
           02 MPAGETH   PIC X.                                          00001090
           02 MPAGETV   PIC X.                                          00001100
           02 MPAGETO   PIC X(2).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNSOCA    PIC X.                                          00001130
           02 MNSOCC    PIC X.                                          00001140
           02 MNSOCP    PIC X.                                          00001150
           02 MNSOCH    PIC X.                                          00001160
           02 MNSOCV    PIC X.                                          00001170
           02 MNSOCO    PIC X(3).                                       00001180
           02 MLIGNEO OCCURS   16 TIMES .                               00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MCGCPLTA     PIC X.                                     00001210
             03 MCGCPLTC     PIC X.                                     00001220
             03 MCGCPLTP     PIC X.                                     00001230
             03 MCGCPLTH     PIC X.                                     00001240
             03 MCGCPLTV     PIC X.                                     00001250
             03 MCGCPLTO     PIC X(5).                                  00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MNGCPLTA     PIC X.                                     00001280
             03 MNGCPLTC     PIC X.                                     00001290
             03 MNGCPLTP     PIC X.                                     00001300
             03 MNGCPLTH     PIC X.                                     00001310
             03 MNGCPLTV     PIC X.                                     00001320
             03 MNGCPLTO     PIC X(8).                                  00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MNLIEUA      PIC X.                                     00001350
             03 MNLIEUC PIC X.                                          00001360
             03 MNLIEUP PIC X.                                          00001370
             03 MNLIEUH PIC X.                                          00001380
             03 MNLIEUV PIC X.                                          00001390
             03 MNLIEUO      PIC X(3).                                  00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MNVENTEA     PIC X.                                     00001420
             03 MNVENTEC     PIC X.                                     00001430
             03 MNVENTEP     PIC X.                                     00001440
             03 MNVENTEH     PIC X.                                     00001450
             03 MNVENTEV     PIC X.                                     00001460
             03 MNVENTEO     PIC X(7).                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MNOMCLIA     PIC X.                                     00001490
             03 MNOMCLIC     PIC X.                                     00001500
             03 MNOMCLIP     PIC X.                                     00001510
             03 MNOMCLIH     PIC X.                                     00001520
             03 MNOMCLIV     PIC X.                                     00001530
             03 MNOMCLIO     PIC X(15).                                 00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MMTTCA  PIC X.                                          00001560
             03 MMTTCC  PIC X.                                          00001570
             03 MMTTCP  PIC X.                                          00001580
             03 MMTTCH  PIC X.                                          00001590
             03 MMTTCV  PIC X.                                          00001600
             03 MMTTCO  PIC ZZZ9,99.                                    00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MTREMBA      PIC X.                                     00001630
             03 MTREMBC PIC X.                                          00001640
             03 MTREMBP PIC X.                                          00001650
             03 MTREMBH PIC X.                                          00001660
             03 MTREMBV PIC X.                                          00001670
             03 MTREMBO      PIC ZZZ9,99.                               00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MNCHEQUEA    PIC X.                                     00001700
             03 MNCHEQUEC    PIC X.                                     00001710
             03 MNCHEQUEP    PIC X.                                     00001720
             03 MNCHEQUEH    PIC X.                                     00001730
             03 MNCHEQUEV    PIC X.                                     00001740
             03 MNCHEQUEO    PIC X(7).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(78).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
