      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA103 AU 09/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA103.                                                        
            05 NOMETAT-IRA103           PIC X(6) VALUE 'IRA103'.                
            05 RUPTURES-IRA103.                                                 
           10 IRA103-NSOCIETE           PIC X(03).                      007  003
           10 IRA103-TYPREGL            PIC X(12).                      010  012
           10 IRA103-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA103-NDEMANDE           PIC X(05).                      027  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA103-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 IRA103-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA103.                                                   
           10 IRA103-BANQUE             PIC X(20).                      034  020
           10 IRA103-CBANQ              PIC X(05).                      054  005
           10 IRA103-NOM                PIC X(25).                      059  025
           10 IRA103-MTREMBTOT          PIC 9(09)V9(2).                 084  011
           10 IRA103-QTE                PIC 9(01)     .                 095  001
           10 IRA103-DATEPARAM          PIC X(08).                      096  008
           10 IRA103-DECHEANCE          PIC X(08).                      104  008
            05 FILLER                      PIC X(401).                          
                                                                                
