      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFF6600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFF6600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF6600.                                                            
           02  FF66-PREFT1                                                      
               PIC X(0001).                                                     
           02  FF66-DEXCPT                                                      
               PIC X(0004).                                                     
           02  FF66-NPIECE                                                      
               PIC X(0006).                                                     
           02  FF66-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FF66-NTIERS                                                      
               PIC X(0009).                                                     
           02  FF66-SOCDEST                                                     
               PIC X(0003).                                                     
           02  FF66-HTRETRO                                                     
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FF66-CTAUXTVA                                                    
               PIC X(0001).                                                     
           02  FF66-CRIST                                                       
               PIC X(0001).                                                     
           02  FF66-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FF66-COMMENT                                                     
               PIC X(0063).                                                     
           02  FF66-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFF6600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF6600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-PREFT1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-PREFT1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-DEXCPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-DEXCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-NTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-NTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-SOCDEST-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-SOCDEST-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-HTRETRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-HTRETRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-CRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-CRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-COMMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-COMMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF66-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF66-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
