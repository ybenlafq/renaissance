      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - TABLES GENERALISEES                                       00000020
      ***************************************************************** 00000030
       01   EFM40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MNENTITEI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLENTITEI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEXERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNEXERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNEXERF   PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MNEXERI   PIC X(4).                                       00000330
           02 LIGNEI OCCURS   12 TIMES .                                00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPERL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNPERL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNPERF  PIC X.                                          00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MNPERI  PIC X(2).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEBUTL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDDEBUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDEBUTF     PIC X.                                     00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MDDEBUTI     PIC X(10).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MDFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDFINF  PIC X.                                          00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MDFINI  PIC X(10).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MLIBELLEI    PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MLIBERRI  PIC X(79).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MSCREENI  PIC X(4).                                       00000660
      ***************************************************************** 00000670
      * GCT - TABLES GENERALISEES                                       00000680
      ***************************************************************** 00000690
       01   EFM40O REDEFINES EFM40I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUO  PIC X(10).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MTIMJOUA  PIC X.                                          00000780
           02 MTIMJOUC  PIC X.                                          00000790
           02 MTIMJOUH  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MPAGEA    PIC X.                                          00000830
           02 MPAGEC    PIC X.                                          00000840
           02 MPAGEH    PIC X.                                          00000850
           02 MPAGEO    PIC Z9.                                         00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MNBPA     PIC X.                                          00000880
           02 MNBPC     PIC X.                                          00000890
           02 MNBPH     PIC X.                                          00000900
           02 MNBPO     PIC Z9.                                         00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MNENTITEA      PIC X.                                     00000930
           02 MNENTITEC PIC X.                                          00000940
           02 MNENTITEH PIC X.                                          00000950
           02 MNENTITEO      PIC X(5).                                  00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MLENTITEA      PIC X.                                     00000980
           02 MLENTITEC PIC X.                                          00000990
           02 MLENTITEH PIC X.                                          00001000
           02 MLENTITEO      PIC X(20).                                 00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNEXERA   PIC X.                                          00001030
           02 MNEXERC   PIC X.                                          00001040
           02 MNEXERH   PIC X.                                          00001050
           02 MNEXERO   PIC X(4).                                       00001060
           02 LIGNEO OCCURS   12 TIMES .                                00001070
             03 FILLER       PIC X(2).                                  00001080
             03 MNPERA  PIC X.                                          00001090
             03 MNPERC  PIC X.                                          00001100
             03 MNPERH  PIC X.                                          00001110
             03 MNPERO  PIC X(2).                                       00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MDDEBUTA     PIC X.                                     00001140
             03 MDDEBUTC     PIC X.                                     00001150
             03 MDDEBUTH     PIC X.                                     00001160
             03 MDDEBUTO     PIC X(10).                                 00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MDFINA  PIC X.                                          00001190
             03 MDFINC  PIC X.                                          00001200
             03 MDFINH  PIC X.                                          00001210
             03 MDFINO  PIC X(10).                                      00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MLIBELLEA    PIC X.                                     00001240
             03 MLIBELLEC    PIC X.                                     00001250
             03 MLIBELLEH    PIC X.                                     00001260
             03 MLIBELLEO    PIC X(20).                                 00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLIBERRA  PIC X.                                          00001290
           02 MLIBERRC  PIC X.                                          00001300
           02 MLIBERRH  PIC X.                                          00001310
           02 MLIBERRO  PIC X(79).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MCODTRAA  PIC X.                                          00001340
           02 MCODTRAC  PIC X.                                          00001350
           02 MCODTRAH  PIC X.                                          00001360
           02 MCODTRAO  PIC X(4).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCICSA    PIC X.                                          00001390
           02 MCICSC    PIC X.                                          00001400
           02 MCICSH    PIC X.                                          00001410
           02 MCICSO    PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MSCREENA  PIC X.                                          00001440
           02 MSCREENC  PIC X.                                          00001450
           02 MSCREENH  PIC X.                                          00001460
           02 MSCREENO  PIC X(4).                                       00001470
                                                                                
