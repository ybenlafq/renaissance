      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRA0002                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRA0002                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRA0002.                                                            
           02  RA00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RA00-NDEMANDE                                                    
               PIC X(0005).                                                     
           02  RA00-CTYPDEMANDE                                                 
               PIC X(0005).                                                     
           02  RA00-NLIEU                                                       
               PIC X(0003).                                                     
           02  RA00-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  RA00-LNOM                                                        
               PIC X(0025).                                                     
           02  RA00-LPRENOM                                                     
               PIC X(0015).                                                     
           02  RA00-LBATIMENT                                                   
               PIC X(0003).                                                     
           02  RA00-LESCALIER                                                   
               PIC X(0003).                                                     
           02  RA00-LETAGE                                                      
               PIC X(0003).                                                     
           02  RA00-LPORTE                                                      
               PIC X(0003).                                                     
           02  RA00-LCMPAD1                                                     
               PIC X(0032).                                                     
           02  RA00-CVOIE                                                       
               PIC X(0005).                                                     
           02  RA00-CTVOIE                                                      
               PIC X(0004).                                                     
           02  RA00-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  RA00-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  RA00-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  RA00-LBUREAU                                                     
               PIC X(0026).                                                     
           02  RA00-TELDOM                                                      
               PIC X(0010).                                                     
           02  RA00-TELBUR                                                      
               PIC X(0010).                                                     
           02  RA00-LPOSTEBUR                                                   
               PIC X(0005).                                                     
           02  RA00-DEVN                                                        
               PIC X(0008).                                                     
           02  RA00-DDIFF                                                       
               PIC X(0008).                                                     
           02  RA00-NAVOIRCLIENT                                                
               PIC X(0006).                                                     
           02  RA00-CMOTIFAV                                                    
               PIC X(0002).                                                     
           02  RA00-NVENTE                                                      
               PIC X(0007).                                                     
           02  RA00-NLETTRAGE                                                   
               PIC X(0006).                                                     
           02  RA00-MTREMBTOT                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RA00-DECHEANCE                                                   
               PIC X(0008).                                                     
           02  RA00-CBANQ                                                       
               PIC X(0005).                                                     
           02  RA00-CTYPREGL                                                    
               PIC X(0001).                                                     
           02  RA00-CLETTRE                                                     
               PIC X(0002).                                                     
           02  RA00-DCREATION                                                   
               PIC X(0008).                                                     
           02  RA00-DMAJDEM                                                     
               PIC X(0008).                                                     
           02  RA00-DAUTOR                                                      
               PIC X(0008).                                                     
           02  RA00-NREGL                                                       
               PIC X(0007).                                                     
           02  RA00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RA00-CTYPAVOIR                                                   
               PIC X(0001).                                                     
           02  RA00-NETAB                                                       
               PIC X(0003).                                                     
           02  RA00-WTLMELA                                                     
               PIC X(0001).                                                     
           02  RA00-WBLBR                                                       
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRA0002                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRA0002-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CTYPDEMANDE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CTYPDEMANDE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LBATIMENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LESCALIER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LETAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LPORTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LCMPAD1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-TELBUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-LPOSTEBUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-LPOSTEBUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DEVN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DEVN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DDIFF-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DDIFF-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NAVOIRCLIENT-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NAVOIRCLIENT-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CMOTIFAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CMOTIFAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-MTREMBTOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-MTREMBTOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DECHEANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DECHEANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CBANQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CBANQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CTYPREGL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CTYPREGL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DMAJDEM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DMAJDEM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DAUTOR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NREGL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NREGL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-CTYPAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-CTYPAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-WTLMELA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA00-WBLBR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA00-WBLBR-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
