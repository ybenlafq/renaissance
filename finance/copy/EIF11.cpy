      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE MODES DE COMPTA.                                    00000020
      ***************************************************************** 00000030
       01   EIF11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODPAIL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCMODPAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMODPAIF      PIC X.                                     00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MCMODPAII      PIC X(3).                                  00000270
           02 FILLER  OCCURS   12 TIMES .                               00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANNULL      COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MANNULL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MANNULF      PIC X.                                     00000300
             03 FILLER  PIC X(2).                                       00000310
             03 MANNULI      PIC X.                                     00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPCTAL     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MTYPCTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPCTAF     PIC X.                                     00000340
             03 FILLER  PIC X(2).                                       00000350
             03 MTYPCTAI     PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPMTL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MCTYPMTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTYPMTF     PIC X.                                     00000380
             03 FILLER  PIC X(2).                                       00000390
             03 MCTYPMTI     PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMODCTAL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MMODCTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMODCTAF     PIC X.                                     00000420
             03 FILLER  PIC X(2).                                       00000430
             03 MMODCTAI     PIC X.                                     00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MNAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNAUXF  PIC X.                                          00000460
             03 FILLER  PIC X(2).                                       00000470
             03 MNAUXI  PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLETTRAGEL   COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MLETTRAGEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLETTRAGEF   PIC X.                                     00000500
             03 FILLER  PIC X(2).                                       00000510
             03 MLETTRAGEI   PIC X.                                     00000520
      * MESSAGE ERREUR                                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MLIBERRI  PIC X(79).                                      00000570
      * CODE TRANSACTION                                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      * ZONE CMD AIDA                                                   00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MZONCMDI  PIC X(15).                                      00000670
      * CICS DE TRAVAIL                                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MCICSI    PIC X(5).                                       00000720
      * NETNAME                                                         00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MNETNAMI  PIC X(8).                                       00000770
      * CODE TERMINAL                                                   00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MSCREENI  PIC X(5).                                       00000820
      ***************************************************************** 00000830
      * PARAMETRAGE MODES DE COMPTA.                                    00000840
      ***************************************************************** 00000850
       01   EIF11O REDEFINES EIF11I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
      * DATE DU JOUR                                                    00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MDATJOUA  PIC X.                                          00000900
           02 MDATJOUC  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
      * HEURE                                                           00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUH  PIC X.                                          00000980
           02 MTIMJOUO  PIC X(5).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNPAGEA   PIC X.                                          00001010
           02 MNPAGEC   PIC X.                                          00001020
           02 MNPAGEH   PIC X.                                          00001030
           02 MNPAGEO   PIC ZZ.                                         00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MNBPAGESA      PIC X.                                     00001060
           02 MNBPAGESC PIC X.                                          00001070
           02 MNBPAGESH PIC X.                                          00001080
           02 MNBPAGESO      PIC ZZ.                                    00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MCMODPAIA      PIC X.                                     00001110
           02 MCMODPAIC PIC X.                                          00001120
           02 MCMODPAIH PIC X.                                          00001130
           02 MCMODPAIO      PIC X(3).                                  00001140
           02 FILLER  OCCURS   12 TIMES .                               00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MANNULA      PIC X.                                     00001170
             03 MANNULC PIC X.                                          00001180
             03 MANNULH PIC X.                                          00001190
             03 MANNULO      PIC X.                                     00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MTYPCTAA     PIC X.                                     00001220
             03 MTYPCTAC     PIC X.                                     00001230
             03 MTYPCTAH     PIC X.                                     00001240
             03 MTYPCTAO     PIC X(3).                                  00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MCTYPMTA     PIC X.                                     00001270
             03 MCTYPMTC     PIC X.                                     00001280
             03 MCTYPMTH     PIC X.                                     00001290
             03 MCTYPMTO     PIC X(5).                                  00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MMODCTAA     PIC X.                                     00001320
             03 MMODCTAC     PIC X.                                     00001330
             03 MMODCTAH     PIC X.                                     00001340
             03 MMODCTAO     PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MNAUXA  PIC X.                                          00001370
             03 MNAUXC  PIC X.                                          00001380
             03 MNAUXH  PIC X.                                          00001390
             03 MNAUXO  PIC X(3).                                       00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MLETTRAGEA   PIC X.                                     00001420
             03 MLETTRAGEC   PIC X.                                     00001430
             03 MLETTRAGEH   PIC X.                                     00001440
             03 MLETTRAGEO   PIC X.                                     00001450
      * MESSAGE ERREUR                                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLIBERRA  PIC X.                                          00001480
           02 MLIBERRC  PIC X.                                          00001490
           02 MLIBERRH  PIC X.                                          00001500
           02 MLIBERRO  PIC X(79).                                      00001510
      * CODE TRANSACTION                                                00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCODTRAA  PIC X.                                          00001540
           02 MCODTRAC  PIC X.                                          00001550
           02 MCODTRAH  PIC X.                                          00001560
           02 MCODTRAO  PIC X(4).                                       00001570
      * ZONE CMD AIDA                                                   00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MZONCMDA  PIC X.                                          00001600
           02 MZONCMDC  PIC X.                                          00001610
           02 MZONCMDH  PIC X.                                          00001620
           02 MZONCMDO  PIC X(15).                                      00001630
      * CICS DE TRAVAIL                                                 00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCICSA    PIC X.                                          00001660
           02 MCICSC    PIC X.                                          00001670
           02 MCICSH    PIC X.                                          00001680
           02 MCICSO    PIC X(5).                                       00001690
      * NETNAME                                                         00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNETNAMA  PIC X.                                          00001720
           02 MNETNAMC  PIC X.                                          00001730
           02 MNETNAMH  PIC X.                                          00001740
           02 MNETNAMO  PIC X(8).                                       00001750
      * CODE TERMINAL                                                   00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSCREENA  PIC X.                                          00001780
           02 MSCREENC  PIC X.                                          00001790
           02 MSCREENH  PIC X.                                          00001800
           02 MSCREENO  PIC X(5).                                       00001810
                                                                                
