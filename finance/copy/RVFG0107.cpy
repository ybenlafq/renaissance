           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG0107                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG0107                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0107.                                                            
           02  FG01-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG01-CCHRONO                                                     
               PIC X(0002).                                                     
           02  FG01-NUMFACT                                                     
               PIC X(0007).                                                     
           02  FG01-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FG01-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  FG01-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  FG01-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  FG01-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  FG01-CTYPFACT                                                    
               PIC X(0002).                                                     
           02  FG01-NATFACT                                                     
               PIC X(0005).                                                     
           02  FG01-CVENT                                                       
               PIC X(0005).                                                     
           02  FG01-NUMORIG                                                     
               PIC X(0007).                                                     
           02  FG01-DMOUV                                                       
               PIC X(0008).                                                     
           02  FG01-MONTHT                                                      
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG01-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  FG01-TXESCPT                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FG01-CFRAIS                                                      
               PIC X(0006).                                                     
           02  FG01-MPCF                                                        
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG01-MFRMG                                                       
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG01-QNBPIECES                                                   
               PIC S9(5) COMP-3.                                                
           02  FG01-DCREAT                                                      
               PIC X(0008).                                                     
           02  FG01-DPIECE                                                      
               PIC X(0008).                                                     
           02  FG01-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FG01-DMVT                                                        
               PIC X(0008).                                                     
           02  FG01-DANNUL                                                      
               PIC X(0008).                                                     
           02  FG01-DECHEANC                                                    
               PIC X(0008).                                                     
           02  FG01-DREGL                                                       
               PIC X(0008).                                                     
           02  FG01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FG01-SERVORIG                                                    
               PIC X(0005).                                                     
           02  FG01-SERVDEST                                                    
               PIC X(0005).                                                     
           02  FG01-DVALEUR                                                     
               PIC X(0008).                                                     
           02  FG01-CACID                                                       
               PIC X(0008).                                                     
           02  FG01-DEXCPT                                                      
               PIC X(0004).                                                     
           02  FG01-MPRMP                                                       
               PIC S9(11)V9(0002) COMP-3.                                       
           02  FG01-SECORIG                                                     
               PIC X(0006).                                                     
           02  FG01-SECDEST                                                     
               PIC X(0006).                                                     
           02  FG01-NTIERSE                                                     
               PIC X(0008).                                                     
           02  FG01-NLETTRE                                                     
               PIC X(0010).                                                     
           02  FG01-NTIERSR                                                     
               PIC X(0008).                                                     
           02  FG01-NLETTRR                                                     
               PIC X(0010).                                                     
           02  FG01-TYPECLI                                                     
               PIC X(0003).                                                     
           02  FG01-MFRTP                                                       
               PIC S9(11)V9(0002) COMP-3.                                       
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG0107                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG0107-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NUMFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NUMFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CTYPFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CTYPFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NATFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NATFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CVENT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CVENT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NUMORIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NUMORIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DMOUV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DMOUV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-MONTHT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-MONTHT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-TXESCPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-TXESCPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CFRAIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CFRAIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-MPCF-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-MPCF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-MFRMG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-MFRMG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-QNBPIECES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-QNBPIECES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DECHEANC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DECHEANC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DREGL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DREGL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-SERVORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-SERVORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-SERVDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-SERVDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DVALEUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DVALEUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-DEXCPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-DEXCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-MPRMP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-MPRMP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-SECORIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-SECORIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-SECDEST-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-SECDEST-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NTIERSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NTIERSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NTIERSR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NTIERSR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-NLETTRR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-NLETTRR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-TYPECLI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-TYPECLI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG01-MFRTP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG01-MFRTP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
