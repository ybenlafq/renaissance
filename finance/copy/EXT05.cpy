      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT05   EXT05                                              00000020
      ***************************************************************** 00000030
       01   EXT05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXTRL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MEXTRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEXTRF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MEXTRI    PIC X(5).                                       00000190
           02 M93I OCCURS   11 TIMES .                                  00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN1-1L      COMP PIC S9(4).                            00000210
      *--                                                                       
             03 MDN1-1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN1-1F      PIC X.                                     00000220
             03 FILLER  PIC X(4).                                       00000230
             03 MDN1-1I      PIC X(2).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN2-1L      COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MDN2-1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN2-1F      PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MDN2-1I      PIC X.                                     00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN3-1L      COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MDN3-1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN3-1F      PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MDN3-1I      PIC X(2).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN4-1L      COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MDN4-1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN4-1F      PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MDN4-1I      PIC X.                                     00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN5-1L      COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MDN5-1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN5-1F      PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MDN5-1I      PIC X(2).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN1-2L      COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MDN1-2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN1-2F      PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MDN1-2I      PIC X(2).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN2-2L      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MDN2-2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN2-2F      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MDN2-2I      PIC X.                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN3-2L      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MDN3-2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN3-2F      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MDN3-2I      PIC X(2).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN4-2L      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MDN4-2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN4-2F      PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MDN4-2I      PIC X.                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN5-2L      COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MDN5-2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN5-2F      PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MDN5-2I      PIC X(2).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN1-3L      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MDN1-3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN1-3F      PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MDN1-3I      PIC X(2).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN2-3L      COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MDN2-3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN2-3F      PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MDN2-3I      PIC X.                                     00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN3-3L      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MDN3-3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN3-3F      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MDN3-3I      PIC X(2).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN4-3L      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MDN4-3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN4-3F      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MDN4-3I      PIC X.                                     00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN5-3L      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MDN5-3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN5-3F      PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MDN5-3I      PIC X(2).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN1-4L      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MDN1-4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN1-4F      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MDN1-4I      PIC X(2).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN2-4L      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDN2-4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN2-4F      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDN2-4I      PIC X.                                     00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN3-4L      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MDN3-4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN3-4F      PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MDN3-4I      PIC X(2).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN4-4L      COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MDN4-4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN4-4F      PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MDN4-4I      PIC X.                                     00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDN5-4L      COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MDN5-4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDN5-4F      PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MDN5-4I      PIC X(2).                                  00001000
      * MESSAGE ERREUR                                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(78).                                      00001050
      * CODE TRANSACTION                                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      * CICS DE TRAVAIL                                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MCICSI    PIC X(5).                                       00001190
      * NETNAME                                                         00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      * CODE TERMINAL                                                   00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(5).                                       00001290
      ***************************************************************** 00001300
      * SDF: EXT05   EXT05                                              00001310
      ***************************************************************** 00001320
       01   EXT05O REDEFINES EXT05I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
      * DATE DU JOUR                                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
      * HEURE                                                           00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MTIMJOUA  PIC X.                                          00001450
           02 MTIMJOUC  PIC X.                                          00001460
           02 MTIMJOUP  PIC X.                                          00001470
           02 MTIMJOUH  PIC X.                                          00001480
           02 MTIMJOUV  PIC X.                                          00001490
           02 MTIMJOUO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MEXTRA    PIC X.                                          00001520
           02 MEXTRC    PIC X.                                          00001530
           02 MEXTRP    PIC X.                                          00001540
           02 MEXTRH    PIC X.                                          00001550
           02 MEXTRV    PIC X.                                          00001560
           02 MEXTRO    PIC X(5).                                       00001570
           02 M93O OCCURS   11 TIMES .                                  00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MDN1-1A      PIC X.                                     00001600
             03 MDN1-1C PIC X.                                          00001610
             03 MDN1-1P PIC X.                                          00001620
             03 MDN1-1H PIC X.                                          00001630
             03 MDN1-1V PIC X.                                          00001640
             03 MDN1-1O      PIC X(2).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MDN2-1A      PIC X.                                     00001670
             03 MDN2-1C PIC X.                                          00001680
             03 MDN2-1P PIC X.                                          00001690
             03 MDN2-1H PIC X.                                          00001700
             03 MDN2-1V PIC X.                                          00001710
             03 MDN2-1O      PIC X.                                     00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MDN3-1A      PIC X.                                     00001740
             03 MDN3-1C PIC X.                                          00001750
             03 MDN3-1P PIC X.                                          00001760
             03 MDN3-1H PIC X.                                          00001770
             03 MDN3-1V PIC X.                                          00001780
             03 MDN3-1O      PIC X(2).                                  00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MDN4-1A      PIC X.                                     00001810
             03 MDN4-1C PIC X.                                          00001820
             03 MDN4-1P PIC X.                                          00001830
             03 MDN4-1H PIC X.                                          00001840
             03 MDN4-1V PIC X.                                          00001850
             03 MDN4-1O      PIC X.                                     00001860
             03 FILLER       PIC X(2).                                  00001870
             03 MDN5-1A      PIC X.                                     00001880
             03 MDN5-1C PIC X.                                          00001890
             03 MDN5-1P PIC X.                                          00001900
             03 MDN5-1H PIC X.                                          00001910
             03 MDN5-1V PIC X.                                          00001920
             03 MDN5-1O      PIC X(2).                                  00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MDN1-2A      PIC X.                                     00001950
             03 MDN1-2C PIC X.                                          00001960
             03 MDN1-2P PIC X.                                          00001970
             03 MDN1-2H PIC X.                                          00001980
             03 MDN1-2V PIC X.                                          00001990
             03 MDN1-2O      PIC X(2).                                  00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MDN2-2A      PIC X.                                     00002020
             03 MDN2-2C PIC X.                                          00002030
             03 MDN2-2P PIC X.                                          00002040
             03 MDN2-2H PIC X.                                          00002050
             03 MDN2-2V PIC X.                                          00002060
             03 MDN2-2O      PIC X.                                     00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MDN3-2A      PIC X.                                     00002090
             03 MDN3-2C PIC X.                                          00002100
             03 MDN3-2P PIC X.                                          00002110
             03 MDN3-2H PIC X.                                          00002120
             03 MDN3-2V PIC X.                                          00002130
             03 MDN3-2O      PIC X(2).                                  00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MDN4-2A      PIC X.                                     00002160
             03 MDN4-2C PIC X.                                          00002170
             03 MDN4-2P PIC X.                                          00002180
             03 MDN4-2H PIC X.                                          00002190
             03 MDN4-2V PIC X.                                          00002200
             03 MDN4-2O      PIC X.                                     00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MDN5-2A      PIC X.                                     00002230
             03 MDN5-2C PIC X.                                          00002240
             03 MDN5-2P PIC X.                                          00002250
             03 MDN5-2H PIC X.                                          00002260
             03 MDN5-2V PIC X.                                          00002270
             03 MDN5-2O      PIC X(2).                                  00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MDN1-3A      PIC X.                                     00002300
             03 MDN1-3C PIC X.                                          00002310
             03 MDN1-3P PIC X.                                          00002320
             03 MDN1-3H PIC X.                                          00002330
             03 MDN1-3V PIC X.                                          00002340
             03 MDN1-3O      PIC X(2).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MDN2-3A      PIC X.                                     00002370
             03 MDN2-3C PIC X.                                          00002380
             03 MDN2-3P PIC X.                                          00002390
             03 MDN2-3H PIC X.                                          00002400
             03 MDN2-3V PIC X.                                          00002410
             03 MDN2-3O      PIC X.                                     00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MDN3-3A      PIC X.                                     00002440
             03 MDN3-3C PIC X.                                          00002450
             03 MDN3-3P PIC X.                                          00002460
             03 MDN3-3H PIC X.                                          00002470
             03 MDN3-3V PIC X.                                          00002480
             03 MDN3-3O      PIC X(2).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MDN4-3A      PIC X.                                     00002510
             03 MDN4-3C PIC X.                                          00002520
             03 MDN4-3P PIC X.                                          00002530
             03 MDN4-3H PIC X.                                          00002540
             03 MDN4-3V PIC X.                                          00002550
             03 MDN4-3O      PIC X.                                     00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MDN5-3A      PIC X.                                     00002580
             03 MDN5-3C PIC X.                                          00002590
             03 MDN5-3P PIC X.                                          00002600
             03 MDN5-3H PIC X.                                          00002610
             03 MDN5-3V PIC X.                                          00002620
             03 MDN5-3O      PIC X(2).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MDN1-4A      PIC X.                                     00002650
             03 MDN1-4C PIC X.                                          00002660
             03 MDN1-4P PIC X.                                          00002670
             03 MDN1-4H PIC X.                                          00002680
             03 MDN1-4V PIC X.                                          00002690
             03 MDN1-4O      PIC X(2).                                  00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MDN2-4A      PIC X.                                     00002720
             03 MDN2-4C PIC X.                                          00002730
             03 MDN2-4P PIC X.                                          00002740
             03 MDN2-4H PIC X.                                          00002750
             03 MDN2-4V PIC X.                                          00002760
             03 MDN2-4O      PIC X.                                     00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MDN3-4A      PIC X.                                     00002790
             03 MDN3-4C PIC X.                                          00002800
             03 MDN3-4P PIC X.                                          00002810
             03 MDN3-4H PIC X.                                          00002820
             03 MDN3-4V PIC X.                                          00002830
             03 MDN3-4O      PIC X(2).                                  00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MDN4-4A      PIC X.                                     00002860
             03 MDN4-4C PIC X.                                          00002870
             03 MDN4-4P PIC X.                                          00002880
             03 MDN4-4H PIC X.                                          00002890
             03 MDN4-4V PIC X.                                          00002900
             03 MDN4-4O      PIC X.                                     00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MDN5-4A      PIC X.                                     00002930
             03 MDN5-4C PIC X.                                          00002940
             03 MDN5-4P PIC X.                                          00002950
             03 MDN5-4H PIC X.                                          00002960
             03 MDN5-4V PIC X.                                          00002970
             03 MDN5-4O      PIC X(2).                                  00002980
      * MESSAGE ERREUR                                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MLIBERRA  PIC X.                                          00003010
           02 MLIBERRC  PIC X.                                          00003020
           02 MLIBERRP  PIC X.                                          00003030
           02 MLIBERRH  PIC X.                                          00003040
           02 MLIBERRV  PIC X.                                          00003050
           02 MLIBERRO  PIC X(78).                                      00003060
      * CODE TRANSACTION                                                00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MCODTRAA  PIC X.                                          00003090
           02 MCODTRAC  PIC X.                                          00003100
           02 MCODTRAP  PIC X.                                          00003110
           02 MCODTRAH  PIC X.                                          00003120
           02 MCODTRAV  PIC X.                                          00003130
           02 MCODTRAO  PIC X(4).                                       00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MZONCMDA  PIC X.                                          00003160
           02 MZONCMDC  PIC X.                                          00003170
           02 MZONCMDP  PIC X.                                          00003180
           02 MZONCMDH  PIC X.                                          00003190
           02 MZONCMDV  PIC X.                                          00003200
           02 MZONCMDO  PIC X(15).                                      00003210
      * CICS DE TRAVAIL                                                 00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MCICSA    PIC X.                                          00003240
           02 MCICSC    PIC X.                                          00003250
           02 MCICSP    PIC X.                                          00003260
           02 MCICSH    PIC X.                                          00003270
           02 MCICSV    PIC X.                                          00003280
           02 MCICSO    PIC X(5).                                       00003290
      * NETNAME                                                         00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNETNAMA  PIC X.                                          00003320
           02 MNETNAMC  PIC X.                                          00003330
           02 MNETNAMP  PIC X.                                          00003340
           02 MNETNAMH  PIC X.                                          00003350
           02 MNETNAMV  PIC X.                                          00003360
           02 MNETNAMO  PIC X(8).                                       00003370
      * CODE TERMINAL                                                   00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MSCREENA  PIC X.                                          00003400
           02 MSCREENC  PIC X.                                          00003410
           02 MSCREENP  PIC X.                                          00003420
           02 MSCREENH  PIC X.                                          00003430
           02 MSCREENV  PIC X.                                          00003440
           02 MSCREENO  PIC X(5).                                       00003450
                                                                                
