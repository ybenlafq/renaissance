      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS090 AU 19/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,08,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS090.                                                        
            05 NOMETAT-IPS090           PIC X(6) VALUE 'IPS090'.                
            05 RUPTURES-IPS090.                                                 
           10 IPS090-CGRP               PIC X(05).                      007  005
           10 IPS090-DREMB              PIC X(08).                      012  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS090-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IPS090-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS090.                                                   
           10 IPS090-LGRP               PIC X(20).                      022  020
           10 IPS090-NGCPLT             PIC X(08).                      042  008
           10 IPS090-NOMCLI             PIC X(15).                      050  015
           10 IPS090-CREDIT             PIC S9(06)V9(2) COMP-3.         065  005
           10 IPS090-CT                 PIC S9(06)V9(2) COMP-3.         070  005
           10 IPS090-DEBIT              PIC S9(06)V9(2) COMP-3.         075  005
           10 IPS090-LT                 PIC S9(06)V9(2) COMP-3.         080  005
           10 IPS090-TVA                PIC S9(06)V9(2) COMP-3.         085  005
           10 IPS090-DVENTE             PIC X(08).                      090  008
           10 IPS090-DPERCPT1           PIC X(06).                      098  006
           10 IPS090-DPERCPT2           PIC X(06).                      104  006
            05 FILLER                      PIC X(403).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS090-LONG           PIC S9(4)   COMP  VALUE +109.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS090-LONG           PIC S9(4) COMP-5  VALUE +109.           
                                                                                
      *}                                                                        
