      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:05 >
      
      * ----------------------------------------------------------- *
      * - ZONE DE COMMUNICATION APPEL MODULE MGV78.               - *
      * -                                                         - *
      * ----------------------------------------------------------- *
        01 MESSGV78-APPLI.
           05 MESSGV78-CODRET                      PIC X(01).
           05 MESSGV78-LIBERR                      PIC X(50).
           05 MESSGV78-CTITRENOM                   PIC X(05).
           05 MESSGV78-LNOM                        PIC X(25).
           05 MESSGV78-LPRENOM                     PIC X(15).
           05 MESSGV78-CVOIE                       PIC X(05).
           05 MESSGV78-CTVOIE                      PIC X(04).
           05 MESSGV78-LNOMVOIE                    PIC X(21).
           05 MESSGV78-CPOSTAL                     PIC X(05).
           05 MESSGV78-LCOMMUNE                    PIC X(32).
           05 MESSGV78-NSOC                        PIC X(03).
           05 MESSGV78-NLIEU                       PIC X(03).
           05 MESSGV78-NVENTE                      PIC X(07).
           05 MESSGV78-VTE                        OCCURS 40.
                 15 MESSGV78-NCODIC-VTE            PIC X(07).
                 15 MESSGV78-NSEQNQ-VTE            PIC S9(5) COMP-3.
                 15 MESSGV78-NEAN-VTE              PIC X(13).
                 15 MESSGV78-NSERIE-VTE            PIC X(15).
                 15 MESSGV78-CODRET-VTE            PIC X(01).
                 15 MESSGV78-LIBERR-VTE            PIC X(50).
           05 MESSGV78-RB10                       OCCURS 40.
                 15 MESSGV78-NCODICREM             PIC X(07).
                 15 MESSGV78-NEANREM               PIC X(13).
                 15 MESSGV78-NSERIEREM             PIC X(15).
                 15 MESSGV78-NCODICREP             PIC X(07).
                 15 MESSGV78-NEANREP               PIC X(13).
                 15 MESSGV78-NSERIEREP             PIC X(15).
                 15 MESSGV78-WCOMPLET              PIC X(01).
                 15 MESSGV78-WHS                   PIC X(01).
                 15 MESSGV78-WACTIVATION           PIC X(01).
                 15 MESSGV78-DMVT                  PIC X(08).
                 15 MESSGV78-WTYPMVT               PIC X(01).
                 15 MESSGV78-LCOMMENT              PIC X(70).
           05 MESSGV78-NUMCONTRAT                  PIC X(18).
           05 MESSGV78-FILLER                      PIC X(200).
      
