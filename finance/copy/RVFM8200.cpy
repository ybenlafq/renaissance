      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM8200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8200                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM8200.                                                            
           02  FM82-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  FM82-LINTERFACE                                                  
               PIC X(0025).                                                     
           02  FM82-CPCG                                                        
               PIC X(0004).                                                     
           02  FM82-NJRN                                                        
               PIC X(0003).                                                     
           02  FM82-CNATURE                                                     
               PIC X(0003).                                                     
           02  FM82-WNATSEC                                                     
               PIC X(0001).                                                     
           02  FM82-WTYPSEC                                                     
               PIC X(0001).                                                     
           02  FM82-WNATRUB                                                     
               PIC X(0001).                                                     
           02  FM82-WTYPRUB                                                     
               PIC X(0001).                                                     
           02  FM82-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM82-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM8200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-CINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-LINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-LINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-CNATURE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-CNATURE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-WNATSEC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-WNATSEC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-WTYPSEC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-WTYPSEC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-WNATRUB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-WNATRUB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-WTYPRUB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-WTYPRUB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM82-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM82-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
