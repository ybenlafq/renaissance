      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG14   EFG14                                              00000020
      ***************************************************************** 00000030
       01   EFG14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTITREI   PIC X(24).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECORIGL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MSECORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECORIGF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSECORIGI      PIC X(6).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPNATL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTYPNATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPNATF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYPNATI  PIC X(30).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MTIERSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTIERSEI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSOCCI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSERVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSERVF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSERVI    PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLSOCCI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMFACTL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNUMFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMFACTF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNUMFACTI      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDCREATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCREATF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDCREATI  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDESTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSOCDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCDESTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSOCDESTI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDESTL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLIEUDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIEUDESTF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIEUDESTI     PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECDESTL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MSECDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECDESTF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSECDESTI      PIC X(6).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDESTL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBDESTF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBDESTI      PIC X(20).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTREL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLETTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTREF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLETTREI  PIC X(10).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSRL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MTIERSRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSRF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTIERSRI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVDESTL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MSERVDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSERVDESTF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSERVDESTI     PIC X(5).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTRRL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLETTRRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTRRF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLETTRRI  PIC X(10).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAL1L      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MLIBVAL1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAL1F      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBVAL1I      PIC X(12).                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMORIGL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MNUMORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMORIGF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNUMORIGI      PIC X(7).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOMPTAL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MDCOMPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCOMPTAF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MDCOMPTAI      PIC X(10).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDECHEANI      PIC X(10).                                 00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAL2L      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MLIBVAL2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAL2F      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBVAL2I      PIC X(9).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDORIGL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MDORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDORIGF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MDORIGI   PIC X(10).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXERCICEL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MEXERCICEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MEXERCICEF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MEXERCICEI     PIC X(4).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPERIODEL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MNPERIODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPERIODEF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNPERIODEI     PIC X(3).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPERIODEL     COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MLPERIODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLPERIODEF     PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MLPERIODEI     PIC X(14).                                 00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPASSAGEL     COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MDPASSAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDPASSAGEF     PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MDPASSAGEI     PIC X(10).                                 00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCRITL      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MLIBCRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBCRITF      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MLIBCRITI      PIC X(15).                                 00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITEREL      COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MCRITEREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCRITEREF      PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCRITEREI      PIC X(58).                                 00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MENTETEI  PIC X(31).                                      00001490
           02 MTABFACI OCCURS   8 TIMES .                               00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MCOMMENTI    PIC X(70).                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MZONCMDI  PIC X(15).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MLIBERRI  PIC X(58).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MNETNAMI  PIC X(8).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MSCREENI  PIC X(4).                                       00001780
      ***************************************************************** 00001790
      * SDF: EFG14   EFG14                                              00001800
      ***************************************************************** 00001810
       01   EFG14O REDEFINES EFG14I.                                    00001820
           02 FILLER    PIC X(12).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTIMJOUA  PIC X.                                          00001920
           02 MTIMJOUC  PIC X.                                          00001930
           02 MTIMJOUP  PIC X.                                          00001940
           02 MTIMJOUH  PIC X.                                          00001950
           02 MTIMJOUV  PIC X.                                          00001960
           02 MTIMJOUO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MFONCA    PIC X.                                          00001990
           02 MFONCC    PIC X.                                          00002000
           02 MFONCP    PIC X.                                          00002010
           02 MFONCH    PIC X.                                          00002020
           02 MFONCV    PIC X.                                          00002030
           02 MFONCO    PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MTITREA   PIC X.                                          00002060
           02 MTITREC   PIC X.                                          00002070
           02 MTITREP   PIC X.                                          00002080
           02 MTITREH   PIC X.                                          00002090
           02 MTITREV   PIC X.                                          00002100
           02 MTITREO   PIC X(24).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNSOCA    PIC X.                                          00002130
           02 MNSOCC    PIC X.                                          00002140
           02 MNSOCP    PIC X.                                          00002150
           02 MNSOCH    PIC X.                                          00002160
           02 MNSOCV    PIC X.                                          00002170
           02 MNSOCO    PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNLIEUA   PIC X.                                          00002200
           02 MNLIEUC   PIC X.                                          00002210
           02 MNLIEUP   PIC X.                                          00002220
           02 MNLIEUH   PIC X.                                          00002230
           02 MNLIEUV   PIC X.                                          00002240
           02 MNLIEUO   PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSECORIGA      PIC X.                                     00002270
           02 MSECORIGC PIC X.                                          00002280
           02 MSECORIGP PIC X.                                          00002290
           02 MSECORIGH PIC X.                                          00002300
           02 MSECORIGV PIC X.                                          00002310
           02 MSECORIGO      PIC X(6).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLLIEUA   PIC X.                                          00002340
           02 MLLIEUC   PIC X.                                          00002350
           02 MLLIEUP   PIC X.                                          00002360
           02 MLLIEUH   PIC X.                                          00002370
           02 MLLIEUV   PIC X.                                          00002380
           02 MLLIEUO   PIC X(20).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MTYPNATA  PIC X.                                          00002410
           02 MTYPNATC  PIC X.                                          00002420
           02 MTYPNATP  PIC X.                                          00002430
           02 MTYPNATH  PIC X.                                          00002440
           02 MTYPNATV  PIC X.                                          00002450
           02 MTYPNATO  PIC X(30).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MTIERSEA  PIC X.                                          00002480
           02 MTIERSEC  PIC X.                                          00002490
           02 MTIERSEP  PIC X.                                          00002500
           02 MTIERSEH  PIC X.                                          00002510
           02 MTIERSEV  PIC X.                                          00002520
           02 MTIERSEO  PIC X(8).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSOCCA    PIC X.                                          00002550
           02 MSOCCC    PIC X.                                          00002560
           02 MSOCCP    PIC X.                                          00002570
           02 MSOCCH    PIC X.                                          00002580
           02 MSOCCV    PIC X.                                          00002590
           02 MSOCCO    PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MSERVA    PIC X.                                          00002620
           02 MSERVC    PIC X.                                          00002630
           02 MSERVP    PIC X.                                          00002640
           02 MSERVH    PIC X.                                          00002650
           02 MSERVV    PIC X.                                          00002660
           02 MSERVO    PIC X(5).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLSOCCA   PIC X.                                          00002690
           02 MLSOCCC   PIC X.                                          00002700
           02 MLSOCCP   PIC X.                                          00002710
           02 MLSOCCH   PIC X.                                          00002720
           02 MLSOCCV   PIC X.                                          00002730
           02 MLSOCCO   PIC X(20).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNUMFACTA      PIC X.                                     00002760
           02 MNUMFACTC PIC X.                                          00002770
           02 MNUMFACTP PIC X.                                          00002780
           02 MNUMFACTH PIC X.                                          00002790
           02 MNUMFACTV PIC X.                                          00002800
           02 MNUMFACTO      PIC X(7).                                  00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MDCREATA  PIC X.                                          00002830
           02 MDCREATC  PIC X.                                          00002840
           02 MDCREATP  PIC X.                                          00002850
           02 MDCREATH  PIC X.                                          00002860
           02 MDCREATV  PIC X.                                          00002870
           02 MDCREATO  PIC X(10).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSOCDESTA      PIC X.                                     00002900
           02 MSOCDESTC PIC X.                                          00002910
           02 MSOCDESTP PIC X.                                          00002920
           02 MSOCDESTH PIC X.                                          00002930
           02 MSOCDESTV PIC X.                                          00002940
           02 MSOCDESTO      PIC X(3).                                  00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLIEUDESTA     PIC X.                                     00002970
           02 MLIEUDESTC     PIC X.                                     00002980
           02 MLIEUDESTP     PIC X.                                     00002990
           02 MLIEUDESTH     PIC X.                                     00003000
           02 MLIEUDESTV     PIC X.                                     00003010
           02 MLIEUDESTO     PIC X(3).                                  00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSECDESTA      PIC X.                                     00003040
           02 MSECDESTC PIC X.                                          00003050
           02 MSECDESTP PIC X.                                          00003060
           02 MSECDESTH PIC X.                                          00003070
           02 MSECDESTV PIC X.                                          00003080
           02 MSECDESTO      PIC X(6).                                  00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MLIBDESTA      PIC X.                                     00003110
           02 MLIBDESTC PIC X.                                          00003120
           02 MLIBDESTP PIC X.                                          00003130
           02 MLIBDESTH PIC X.                                          00003140
           02 MLIBDESTV PIC X.                                          00003150
           02 MLIBDESTO      PIC X(20).                                 00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLETTREA  PIC X.                                          00003180
           02 MLETTREC  PIC X.                                          00003190
           02 MLETTREP  PIC X.                                          00003200
           02 MLETTREH  PIC X.                                          00003210
           02 MLETTREV  PIC X.                                          00003220
           02 MLETTREO  PIC X(10).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MTIERSRA  PIC X.                                          00003250
           02 MTIERSRC  PIC X.                                          00003260
           02 MTIERSRP  PIC X.                                          00003270
           02 MTIERSRH  PIC X.                                          00003280
           02 MTIERSRV  PIC X.                                          00003290
           02 MTIERSRO  PIC X(8).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MSERVDESTA     PIC X.                                     00003320
           02 MSERVDESTC     PIC X.                                     00003330
           02 MSERVDESTP     PIC X.                                     00003340
           02 MSERVDESTH     PIC X.                                     00003350
           02 MSERVDESTV     PIC X.                                     00003360
           02 MSERVDESTO     PIC X(5).                                  00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLETTRRA  PIC X.                                          00003390
           02 MLETTRRC  PIC X.                                          00003400
           02 MLETTRRP  PIC X.                                          00003410
           02 MLETTRRH  PIC X.                                          00003420
           02 MLETTRRV  PIC X.                                          00003430
           02 MLETTRRO  PIC X(10).                                      00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MLIBVAL1A      PIC X.                                     00003460
           02 MLIBVAL1C PIC X.                                          00003470
           02 MLIBVAL1P PIC X.                                          00003480
           02 MLIBVAL1H PIC X.                                          00003490
           02 MLIBVAL1V PIC X.                                          00003500
           02 MLIBVAL1O      PIC X(12).                                 00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MNUMORIGA      PIC X.                                     00003530
           02 MNUMORIGC PIC X.                                          00003540
           02 MNUMORIGP PIC X.                                          00003550
           02 MNUMORIGH PIC X.                                          00003560
           02 MNUMORIGV PIC X.                                          00003570
           02 MNUMORIGO      PIC X(7).                                  00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MDCOMPTAA      PIC X.                                     00003600
           02 MDCOMPTAC PIC X.                                          00003610
           02 MDCOMPTAP PIC X.                                          00003620
           02 MDCOMPTAH PIC X.                                          00003630
           02 MDCOMPTAV PIC X.                                          00003640
           02 MDCOMPTAO      PIC X(10).                                 00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MDECHEANA      PIC X.                                     00003670
           02 MDECHEANC PIC X.                                          00003680
           02 MDECHEANP PIC X.                                          00003690
           02 MDECHEANH PIC X.                                          00003700
           02 MDECHEANV PIC X.                                          00003710
           02 MDECHEANO      PIC X(10).                                 00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MLIBVAL2A      PIC X.                                     00003740
           02 MLIBVAL2C PIC X.                                          00003750
           02 MLIBVAL2P PIC X.                                          00003760
           02 MLIBVAL2H PIC X.                                          00003770
           02 MLIBVAL2V PIC X.                                          00003780
           02 MLIBVAL2O      PIC X(9).                                  00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MDORIGA   PIC X.                                          00003810
           02 MDORIGC   PIC X.                                          00003820
           02 MDORIGP   PIC X.                                          00003830
           02 MDORIGH   PIC X.                                          00003840
           02 MDORIGV   PIC X.                                          00003850
           02 MDORIGO   PIC X(10).                                      00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MEXERCICEA     PIC X.                                     00003880
           02 MEXERCICEC     PIC X.                                     00003890
           02 MEXERCICEP     PIC X.                                     00003900
           02 MEXERCICEH     PIC X.                                     00003910
           02 MEXERCICEV     PIC X.                                     00003920
           02 MEXERCICEO     PIC X(4).                                  00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MNPERIODEA     PIC X.                                     00003950
           02 MNPERIODEC     PIC X.                                     00003960
           02 MNPERIODEP     PIC X.                                     00003970
           02 MNPERIODEH     PIC X.                                     00003980
           02 MNPERIODEV     PIC X.                                     00003990
           02 MNPERIODEO     PIC X(3).                                  00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MLPERIODEA     PIC X.                                     00004020
           02 MLPERIODEC     PIC X.                                     00004030
           02 MLPERIODEP     PIC X.                                     00004040
           02 MLPERIODEH     PIC X.                                     00004050
           02 MLPERIODEV     PIC X.                                     00004060
           02 MLPERIODEO     PIC X(14).                                 00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MDPASSAGEA     PIC X.                                     00004090
           02 MDPASSAGEC     PIC X.                                     00004100
           02 MDPASSAGEP     PIC X.                                     00004110
           02 MDPASSAGEH     PIC X.                                     00004120
           02 MDPASSAGEV     PIC X.                                     00004130
           02 MDPASSAGEO     PIC X(10).                                 00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MLIBCRITA      PIC X.                                     00004160
           02 MLIBCRITC PIC X.                                          00004170
           02 MLIBCRITP PIC X.                                          00004180
           02 MLIBCRITH PIC X.                                          00004190
           02 MLIBCRITV PIC X.                                          00004200
           02 MLIBCRITO      PIC X(15).                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MCRITEREA      PIC X.                                     00004230
           02 MCRITEREC PIC X.                                          00004240
           02 MCRITEREP PIC X.                                          00004250
           02 MCRITEREH PIC X.                                          00004260
           02 MCRITEREV PIC X.                                          00004270
           02 MCRITEREO      PIC X(58).                                 00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MENTETEA  PIC X.                                          00004300
           02 MENTETEC  PIC X.                                          00004310
           02 MENTETEP  PIC X.                                          00004320
           02 MENTETEH  PIC X.                                          00004330
           02 MENTETEV  PIC X.                                          00004340
           02 MENTETEO  PIC X(31).                                      00004350
           02 MTABFACO OCCURS   8 TIMES .                               00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MCOMMENTA    PIC X.                                     00004380
             03 MCOMMENTC    PIC X.                                     00004390
             03 MCOMMENTP    PIC X.                                     00004400
             03 MCOMMENTH    PIC X.                                     00004410
             03 MCOMMENTV    PIC X.                                     00004420
             03 MCOMMENTO    PIC X(70).                                 00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MZONCMDA  PIC X.                                          00004450
           02 MZONCMDC  PIC X.                                          00004460
           02 MZONCMDP  PIC X.                                          00004470
           02 MZONCMDH  PIC X.                                          00004480
           02 MZONCMDV  PIC X.                                          00004490
           02 MZONCMDO  PIC X(15).                                      00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MLIBERRA  PIC X.                                          00004520
           02 MLIBERRC  PIC X.                                          00004530
           02 MLIBERRP  PIC X.                                          00004540
           02 MLIBERRH  PIC X.                                          00004550
           02 MLIBERRV  PIC X.                                          00004560
           02 MLIBERRO  PIC X(58).                                      00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCODTRAA  PIC X.                                          00004590
           02 MCODTRAC  PIC X.                                          00004600
           02 MCODTRAP  PIC X.                                          00004610
           02 MCODTRAH  PIC X.                                          00004620
           02 MCODTRAV  PIC X.                                          00004630
           02 MCODTRAO  PIC X(4).                                       00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MCICSA    PIC X.                                          00004660
           02 MCICSC    PIC X.                                          00004670
           02 MCICSP    PIC X.                                          00004680
           02 MCICSH    PIC X.                                          00004690
           02 MCICSV    PIC X.                                          00004700
           02 MCICSO    PIC X(5).                                       00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MNETNAMA  PIC X.                                          00004730
           02 MNETNAMC  PIC X.                                          00004740
           02 MNETNAMP  PIC X.                                          00004750
           02 MNETNAMH  PIC X.                                          00004760
           02 MNETNAMV  PIC X.                                          00004770
           02 MNETNAMO  PIC X(8).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MSCREENA  PIC X.                                          00004800
           02 MSCREENC  PIC X.                                          00004810
           02 MSCREENP  PIC X.                                          00004820
           02 MSCREENH  PIC X.                                          00004830
           02 MSCREENV  PIC X.                                          00004840
           02 MSCREENO  PIC X(4).                                       00004850
                                                                                
