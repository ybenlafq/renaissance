      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE DES LIBELLES                                        00000020
      ***************************************************************** 00000030
       01   EIF14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODPAIL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MMODPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODPAIF  PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MMODPAII  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPMTL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MCTYPMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPMTF  PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MCTYPMTI  PIC X(5).                                       00000230
           02 FILLER  OCCURS   10 TIMES .                               00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRANGL  COMP PIC S9(4).                                 00000250
      *--                                                                       
             03 MRANGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRANGF  PIC X.                                          00000260
             03 FILLER  PIC X(2).                                       00000270
             03 MRANGI  PIC X(2).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000300
             03 FILLER  PIC X(2).                                       00000310
             03 MLIBELLEI    PIC X(30).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCHAMPL   COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNOMCHAMPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNOMCHAMPF   PIC X.                                     00000340
             03 FILLER  PIC X(2).                                       00000350
             03 MNOMCHAMPI   PIC X(10).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000380
             03 FILLER  PIC X(2).                                       00000390
             03 MMASQUEI     PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEPFINL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MSEPFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSEPFINF     PIC X.                                     00000420
             03 FILLER  PIC X(2).                                       00000430
             03 MSEPFINI     PIC X.                                     00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRESULTL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MRESULTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRESULTF  PIC X.                                          00000460
           02 FILLER    PIC X(2).                                       00000470
           02 MRESULTI  PIC X(50).                                      00000480
      * MESSAGE ERREUR                                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(2).                                       00000520
           02 MLIBERRI  PIC X(79).                                      00000530
      * CODE TRANSACTION                                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      * ZONE CMD AIDA                                                   00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MZONCMDI  PIC X(15).                                      00000630
      * CICS DE TRAVAIL                                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MCICSI    PIC X(5).                                       00000680
      * NETNAME                                                         00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MNETNAMI  PIC X(8).                                       00000730
      * CODE TERMINAL                                                   00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MSCREENI  PIC X(5).                                       00000780
      ***************************************************************** 00000790
      * PARAMETRAGE DES LIBELLES                                        00000800
      ***************************************************************** 00000810
       01   EIF14O REDEFINES EIF14I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
      * DATE DU JOUR                                                    00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MDATJOUA  PIC X.                                          00000860
           02 MDATJOUC  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
      * HEURE                                                           00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUO  PIC X(5).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MMODPAIA  PIC X.                                          00000970
           02 MMODPAIC  PIC X.                                          00000980
           02 MMODPAIH  PIC X.                                          00000990
           02 MMODPAIO  PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MCTYPMTA  PIC X.                                          00001020
           02 MCTYPMTC  PIC X.                                          00001030
           02 MCTYPMTH  PIC X.                                          00001040
           02 MCTYPMTO  PIC X(5).                                       00001050
           02 FILLER  OCCURS   10 TIMES .                               00001060
             03 FILLER       PIC X(2).                                  00001070
             03 MRANGA  PIC X.                                          00001080
             03 MRANGC  PIC X.                                          00001090
             03 MRANGH  PIC X.                                          00001100
             03 MRANGO  PIC ZZ.                                         00001110
             03 FILLER       PIC X(2).                                  00001120
             03 MLIBELLEA    PIC X.                                     00001130
             03 MLIBELLEC    PIC X.                                     00001140
             03 MLIBELLEH    PIC X.                                     00001150
             03 MLIBELLEO    PIC X(30).                                 00001160
             03 FILLER       PIC X(2).                                  00001170
             03 MNOMCHAMPA   PIC X.                                     00001180
             03 MNOMCHAMPC   PIC X.                                     00001190
             03 MNOMCHAMPH   PIC X.                                     00001200
             03 MNOMCHAMPO   PIC X(10).                                 00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MMASQUEA     PIC X.                                     00001230
             03 MMASQUEC     PIC X.                                     00001240
             03 MMASQUEH     PIC X.                                     00001250
             03 MMASQUEO     PIC X(5).                                  00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MSEPFINA     PIC X.                                     00001280
             03 MSEPFINC     PIC X.                                     00001290
             03 MSEPFINH     PIC X.                                     00001300
             03 MSEPFINO     PIC X.                                     00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MRESULTA  PIC X.                                          00001330
           02 MRESULTC  PIC X.                                          00001340
           02 MRESULTH  PIC X.                                          00001350
           02 MRESULTO  PIC X(50).                                      00001360
      * MESSAGE ERREUR                                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLIBERRA  PIC X.                                          00001390
           02 MLIBERRC  PIC X.                                          00001400
           02 MLIBERRH  PIC X.                                          00001410
           02 MLIBERRO  PIC X(79).                                      00001420
      * CODE TRANSACTION                                                00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCODTRAA  PIC X.                                          00001450
           02 MCODTRAC  PIC X.                                          00001460
           02 MCODTRAH  PIC X.                                          00001470
           02 MCODTRAO  PIC X(4).                                       00001480
      * ZONE CMD AIDA                                                   00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MZONCMDA  PIC X.                                          00001510
           02 MZONCMDC  PIC X.                                          00001520
           02 MZONCMDH  PIC X.                                          00001530
           02 MZONCMDO  PIC X(15).                                      00001540
      * CICS DE TRAVAIL                                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCICSA    PIC X.                                          00001570
           02 MCICSC    PIC X.                                          00001580
           02 MCICSH    PIC X.                                          00001590
           02 MCICSO    PIC X(5).                                       00001600
      * NETNAME                                                         00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNETNAMA  PIC X.                                          00001630
           02 MNETNAMC  PIC X.                                          00001640
           02 MNETNAMH  PIC X.                                          00001650
           02 MNETNAMO  PIC X(8).                                       00001660
      * CODE TERMINAL                                                   00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSCREENA  PIC X.                                          00001690
           02 MSCREENC  PIC X.                                          00001700
           02 MSCREENH  PIC X.                                          00001710
           02 MSCREENO  PIC X(5).                                       00001720
                                                                                
