      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  PROJET RAPPROCHEMENT DARTY.COM                                *        
      *  FCG200 : FICHIER DETAIL DES ENCAISSEMENTS / FINANCEMENTS      *        
      *                                                                *        
      *  LONGUEUR = 200                                                *        
      *----------------------------------------------------------------*        
       01 :FCG200:-LIGNE.                                                       
      * CODE APPLICATION                                                        
         03 :FCG200:-APPLI            PIC X(10) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * ORIGINE = IDENTIFIANT DE L'EMETTEUR                                     
         03 :FCG200:-ORIGINE          PIC X(20) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * DATE D'INTERFACE = DATE DE CREATION DU FICHIER                          
         03 :FCG200:-DINTERFACE       PIC X(08) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * NUMERO DE CONTRAT                                                       
         03 :FCG200:-CONTRAT          PIC X(10) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * DATE D OPERATION                                                        
         03 :FCG200:-DOPERATION       PIC X(08) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * REFERENCE DE L'OPERATION                                                
         03 :FCG200:-REFERENCE        PIC X(40) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * MONTANT (SANS SIGNE, SANS SEPARATEUR DECIMAL)                           
         03 :FCG200:-MONTANT          PIC X(14) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * SIGNE DU MONTANT ('-' SI MONTANT < 0  SINON '+')                        
         03 :FCG200:-SENS             PIC X(01) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * CRITERE DE RAPPROCHEMENT / LCOMLIV1 - RVGV0299                          
         03 :FCG200:-CRITERE          PIC X(20) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * IDENTIFIANT DU FICHIER : 'BCG210_' + DATE DU JOUR + '.CSV'              
         03 :FCG200:-FICHIER          PIC X(50) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * MOTIF DE REJET                                                          
         03 :FCG200:-MOTIF            PIC X(01) VALUE SPACES.                   
         03 FILLER                    PIC X(01) VALUE ';'.                      
      * FILLER DE FIN                                                           
         03 FILLER                    PIC X(07) VALUE SPACES.                   
       01 :FCG200:-TITRE.                                                       
         03 FILLER              PIC X(11) VALUE 'APPLICATION'.                  
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(15) VALUE 'DATE INTERFACE'.               
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(07) VALUE 'ORIGINE'.                      
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(07) VALUE 'CONTRAT'.                      
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(15) VALUE 'DATE OPERATION'.               
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(09) VALUE 'REFERENCE'.                    
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(07) VALUE 'MONTANT'.                      
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(04) VALUE 'SENS'.                         
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(07) VALUE 'CRITERE'.                      
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(07) VALUE 'FICHIER'.                      
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(05) VALUE 'MOTIF'.                        
         03 FILLER              PIC X(01) VALUE ';'.                            
         03 FILLER              PIC X(95) VALUE SPACES.                         
                                                                                
