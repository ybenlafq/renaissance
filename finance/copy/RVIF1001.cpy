      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIF1001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIF1001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIF1001.                                                            
           02  IF10-CORGANISME                                                  
               PIC X(0003).                                                     
           02  IF10-DRECEPT                                                     
               PIC X(0008).                                                     
           02  IF10-HRECEPT                                                     
               PIC X(0004).                                                     
           02  IF10-DDTRAIT                                                     
               PIC X(0008).                                                     
           02  IF10-NBRECUS                                                     
               PIC S9(5) COMP-3.                                                
           02  IF10-NBREJETS                                                    
               PIC S9(5) COMP-3.                                                
           02  IF10-NBANOS                                                      
               PIC S9(5) COMP-3.                                                
           02  IF10-DFICORG                                                     
               PIC X(8).                                                        
           02  IF10-NFICORG                                                     
               PIC S9(7) COMP-3.                                                
           02  IF10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIF1001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIF1001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-CORGANISME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-CORGANISME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-DRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-DRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-HRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-HRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-DDTRAIT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-DDTRAIT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-NBRECUS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-NBRECUS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-NBREJETS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-NBREJETS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-NBANOS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-NBANOS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-DFICORG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-DFICORG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-NFICORG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-NFICORG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
