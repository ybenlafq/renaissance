      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01 messgv76-donnees.                                                     
          10 messgv76-codret               pic x(02).                           
          10 messgv76-libret               pic x(50).                           
          10 messgv76-ZIN-TAB               OCCURS 40.                  00460000
            15 messgv76-NSERIE             PIC X(15).                   00470000
            15 messgv76-CODRET-VENTE       PIC X(02).                   00480001
            15 messgv76-NSOCIETE           PIC X(03).                   00480001
            15 messgv76-NLIEU              PIC X(03).                   00481001
            15 messgv76-NVENTE             PIC X(07).                   00482001
            15 messgv76-NCODIC             PIC X(07).                   00483001
            15 messgv76-nseqnq             pic s9(05) comp-3.                   
            15 messgv76-CTITRENOM          PIC X(05).                   00484001
            15 messgv76-LNOM               PIC X(25).                   00485001
            15 messgv76-LPRENOM            PIC X(15).                   00486001
            15 messgv76-CVOIE              PIC X(05).                   00487001
            15 messgv76-CTVOIE             PIC X(04).                   00488001
            15 messgv76-LNOMVOIE           PIC X(21).                   00489001
            15 messgv76-CPOSTAL            PIC X(05).                   00489101
            15 messgv76-LCOMMUNE           PIC X(32).                   00489201
      * -> DONNEES DU LIEU ECHANGEUR SI TROUV�ES                        00489301
            15 messgv76-CODRET-ECH         PIC X(02).                   00480001
            15 messgv76-NSOCECH            PIC X(03).                   00489401
            15 messgv76-NLIEUECH           PIC X(03).                   00489501
            15 messgv76-NCODICECH          PIC X(07).                   00489501
            15 messgv76-CUSERECH           PIC X(20).                   00489601
                                                                                
