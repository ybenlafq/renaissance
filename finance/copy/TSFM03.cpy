      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM03-LONG             PIC S9(4) COMP-3 VALUE +630.                
       01  TS-FM03-RECORD.                                                      
           05 TS-FM03-LIGNE         OCCURS 14.                                  
              10 TS-FM03-WACTIF     PIC X.                                      
              10 TS-FM03-CMETHODE   PIC X(3).                                   
              10 TS-FM03-LMETHODE   PIC X(30).                                  
              10 TS-FM03-WRIB       PIC X.                                      
              10 TS-FM03-WNUMAUTO   PIC X.                                      
              10 TS-FM03-WEAP       PIC X.                                      
              10 TS-FM03-DELAI      PIC X(2).                                   
              10 TS-FM03-CDOC       PIC X(6).                                   
                                                                                
