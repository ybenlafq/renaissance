      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IFEXT DESIGNATION CARTE PAR BIN        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIFEXT.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIFEXT.                                                             
      *}                                                                        
           05  IFEXT-CTABLEG2    PIC X(15).                                     
           05  IFEXT-CTABLEG2-REDEF REDEFINES IFEXT-CTABLEG2.                   
               10  IFEXT-BIN             PIC X(12).                             
           05  IFEXT-WTABLEG     PIC X(80).                                     
           05  IFEXT-WTABLEG-REDEF  REDEFINES IFEXT-WTABLEG.                    
               10  IFEXT-FLAG            PIC X(01).                             
               10  IFEXT-TCARTE          PIC X(15).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIFEXT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIFEXT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFEXT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IFEXT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IFEXT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IFEXT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
