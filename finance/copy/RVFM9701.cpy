      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM9701                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9701                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFM9701.                                                    00090001
           05  FM97-CINTERFACE                                          00091000
               PIC X(0005).                                             00092000
           05  FM97-CNATOPER                                            00093000
               PIC X(0005).                                             00094000
           05  FM97-CTYPOPER                                            00095000
               PIC X(0005).                                             00096000
           05  FM97-CTVA                                                00097000
               PIC X(0005).                                             00098000
           05  FM97-CGEO                                                00099000
               PIC X(0001).                                             00099100
           05  FM97-WGROUPE                                             00099200
               PIC X(0001).                                             00099300
           05  FM97-CPTSAP                                              00099600
               PIC X(0008).                                             00099700
           05  FM97-CSENS                                               00099800
               PIC X(0001).                                             00099900
           05  FM97-WCUMUL                                              00100000
               PIC X(0001).                                             00100100
           05  FM97-NLETTRAGE                                           00100200
               PIC X(0001).                                             00100300
           05  FM97-NTIERSSAP                                           00100400
               PIC X(0001).                                             00100500
           05  FM97-CCGS                                                00100600
               PIC X(0001).                                             00100700
           05  FM97-WANAL                                               00101000
               PIC X(0001).                                             00101100
           05  FM97-TYPMT                                               00101200
               PIC X(0002).                                             00101300
           05  FM97-CONTREPSAP                                          00101500
               PIC X(0008).                                             00101600
           05  FM97-WCUMULC                                             00101900
               PIC X(0001).                                             00102000
           05  FM97-NLETTRAGEC                                          00102100
               PIC X(0001).                                             00102200
           05  FM97-NTIERSSAPC                                          00102300
               PIC X(0001).                                             00102400
           05  FM97-CCGSC                                               00102500
               PIC X(0001).                                             00102600
           05  FM97-WANALC                                              00102900
               PIC X(0001).                                             00103000
           05  FM97-TYPMTC                                              00103200
               PIC X(0002).                                             00103300
           05  FM97-DEFFET                                              00103601
               PIC X(0008).                                             00103701
           05  FM97-DMAJ                                                00103801
               PIC X(0008).                                             00103901
           05  FM97-DSYST                                               00104001
               PIC S9(0013) COMP-3.                                     00105001
           05  FM97-WECHEANC                                            00106001
               PIC X(0001).                                             00107001
      *                                                                 00500000
      *                                                                 00510000
      *---------------------------------------------------------        00520000
      *   LISTE DES FLAGS DE LA TABLE RVFM9900                          00530000
      *---------------------------------------------------------        00540000
      *                                                                 00550000
       01  RVFM9700-FLAGS.                                              00560000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CINTERFACE-F                                        00570000
      *        PIC S9(4) COMP.                                          00571000
      *--                                                                       
           05  FM97-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CNATOPER-F                                          00572000
      *        PIC S9(4) COMP.                                          00573000
      *--                                                                       
           05  FM97-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CTYPOPER-F                                          00574000
      *        PIC S9(4) COMP.                                          00575000
      *--                                                                       
           05  FM97-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CTVA-F                                              00576000
      *        PIC S9(4) COMP.                                          00577000
      *--                                                                       
           05  FM97-CTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CGEO-F                                              00578000
      *        PIC S9(4) COMP.                                          00579000
      *--                                                                       
           05  FM97-CGEO-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WGROUPE-F                                           00579100
      *        PIC S9(4) COMP.                                          00579200
      *--                                                                       
           05  FM97-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CPTSAP-F                                            00579500
      *        PIC S9(4) COMP.                                          00579600
      *--                                                                       
           05  FM97-CPTSAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CSENS-F                                             00579700
      *        PIC S9(4) COMP.                                          00579800
      *--                                                                       
           05  FM97-CSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WCUMUL-F                                            00579900
      *        PIC S9(4) COMP.                                          00580000
      *--                                                                       
           05  FM97-WCUMUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NLETTRAGE-F                                         00580100
      *        PIC S9(4) COMP.                                          00580200
      *--                                                                       
           05  FM97-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NTIERSSAP-F                                         00580300
      *        PIC S9(4) COMP.                                          00580400
      *--                                                                       
           05  FM97-NTIERSSAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CCGS-F                                              00580500
      *        PIC S9(4) COMP.                                          00580600
      *--                                                                       
           05  FM97-CCGS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WANAL-F                                             00581000
      *        PIC S9(4) COMP.                                          00582000
      *--                                                                       
           05  FM97-WANAL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-TYPMT-F                                             00582100
      *        PIC S9(4) COMP.                                          00582200
      *--                                                                       
           05  FM97-TYPMT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CONTREPSAP-F                                        00589200
      *        PIC S9(4) COMP.                                          00589300
      *--                                                                       
           05  FM97-CONTREPSAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WCUMULC-F                                           00589400
      *        PIC S9(4) COMP.                                          00589500
      *--                                                                       
           05  FM97-WCUMULC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NLETTRAGEC-F                                        00589600
      *        PIC S9(4) COMP.                                          00589700
      *--                                                                       
           05  FM97-NLETTRAGEC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NTIERSSAPC-F                                        00589800
      *        PIC S9(4) COMP.                                          00589900
      *--                                                                       
           05  FM97-NTIERSSAPC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CCGSC-F                                             00590000
      *        PIC S9(4) COMP.                                          00590100
      *--                                                                       
           05  FM97-CCGSC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WANALC-F                                            00590400
      *        PIC S9(4) COMP.                                          00590500
      *--                                                                       
           05  FM97-WANALC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-TYPMTC-F                                            00595100
      *        PIC S9(4) COMP.                                          00595200
      *--                                                                       
           05  FM97-TYPMTC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DEFFET-F                                            00596000
      *        PIC S9(4) COMP.                                          00597000
      *--                                                                       
           05  FM97-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DMAJ-F                                              00598000
      *        PIC S9(4) COMP.                                          00599000
      *--                                                                       
           05  FM97-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DSYST-F                                             00599100
      *        PIC S9(4) COMP.                                          00600000
      *--                                                                       
           05  FM97-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WECHEANC-F                                          00610001
      *        PIC S9(4) COMP.                                          00620001
      *--                                                                       
           05  FM97-WECHEANC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00960000
                                                                                
