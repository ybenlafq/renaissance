      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM12-LONG             PIC S9(4) COMP-3 VALUE +98.                 
       01  TS-FM12-RECORD.                                                      
           10 TS-FM12-NDEMANDE      PIC X(2).                                   
           10 TS-FM12-DCREATION     PIC X(10).                                  
           10 TS-FM12-CACID         PIC X(8).                                   
           10 TS-FM12-USER          PIC X(25).                                  
           10 TS-FM12-WMFICHE       PIC X.                                      
           10 TS-FM12-WPAPIER       PIC X.                                      
           10 TS-FM12-NETAB         PIC X(3).                                   
AM         10 TS-FM12-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM12-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM12-WTYPAUX       PIC X.                                      
           10 TS-FM12-NTIERSMIN     PIC X(8).                                   
           10 TS-FM12-NTIERSMAX     PIC X(8).                                   
           10 TS-FM12-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM12-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM12-WTOTAL        PIC X OCCURS 4.                             
           10 TS-FM12-WNTRI         PIC X OCCURS 4.                             
           10 TS-FM12-WTTRITIERS    PIC X.                                      
           10 TS-FM12-WTSELTIERS    PIC X.                                      
           10 TS-FM12-CDEVISE       PIC X(3).                                   
                                                                                
