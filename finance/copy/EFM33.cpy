      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - LISTE JOURNAUX                                            00000020
      ***************************************************************** 00000030
       01   EFM33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MTABI OCCURS   14 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCMDL     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MZONCMDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONCMDF     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MZONCMDI     PIC X.                                     00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNJRNL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MNJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNJRNF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MNJRNI  PIC X(3).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLJRNL  COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 MLJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLJRNF  PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLJRNI  PIC X(29).                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTYPAUXL    COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MWTYPAUXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWTYPAUXF    PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MWTYPAUXI    PIC X.                                     00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MNAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNAUXF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MNAUXI  PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRPARTL    COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCTRPARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTRPARTF    PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCTRPARTI    PIC X.                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRPASSL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCTRPASSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTRPASSF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCTRPASSI    PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJRNREGL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MJRNREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MJRNREGF     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MJRNREGI     PIC X.                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMAUTOL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MNUMAUTOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNUMAUTOF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MNUMAUTOI    PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWBATCHTPL   COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MWBATCHTPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWBATCHTPF   PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MWBATCHTPI   PIC X.                                     00000640
      * ZONE CMD AIDA                                                   00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBERRI  PIC X(79).                                      00000690
      * CODE TRANSACTION                                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      * CICS DE TRAVAIL                                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCICSI    PIC X(5).                                       00000790
      * NETNAME                                                         00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MNETNAMI  PIC X(8).                                       00000840
      * CODE TERMINAL                                                   00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * GCT - LISTE JOURNAUX                                            00000910
      ***************************************************************** 00000920
       01   EFM33O REDEFINES EFM33I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
      * DATE DU JOUR                                                    00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
      * HEURE                                                           00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MTIMJOUA  PIC X.                                          00001050
           02 MTIMJOUC  PIC X.                                          00001060
           02 MTIMJOUP  PIC X.                                          00001070
           02 MTIMJOUH  PIC X.                                          00001080
           02 MTIMJOUV  PIC X.                                          00001090
           02 MTIMJOUO  PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MPAGEA    PIC X.                                          00001120
           02 MPAGEC    PIC X.                                          00001130
           02 MPAGEP    PIC X.                                          00001140
           02 MPAGEH    PIC X.                                          00001150
           02 MPAGEV    PIC X.                                          00001160
           02 MPAGEO    PIC Z9.                                         00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNBPA     PIC X.                                          00001190
           02 MNBPC     PIC X.                                          00001200
           02 MNBPP     PIC X.                                          00001210
           02 MNBPH     PIC X.                                          00001220
           02 MNBPV     PIC X.                                          00001230
           02 MNBPO     PIC Z9.                                         00001240
           02 MTABO OCCURS   14 TIMES .                                 00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MZONCMDA     PIC X.                                     00001270
             03 MZONCMDC     PIC X.                                     00001280
             03 MZONCMDP     PIC X.                                     00001290
             03 MZONCMDH     PIC X.                                     00001300
             03 MZONCMDV     PIC X.                                     00001310
             03 MZONCMDO     PIC X.                                     00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MNJRNA  PIC X.                                          00001340
             03 MNJRNC  PIC X.                                          00001350
             03 MNJRNP  PIC X.                                          00001360
             03 MNJRNH  PIC X.                                          00001370
             03 MNJRNV  PIC X.                                          00001380
             03 MNJRNO  PIC X(3).                                       00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MLJRNA  PIC X.                                          00001410
             03 MLJRNC  PIC X.                                          00001420
             03 MLJRNP  PIC X.                                          00001430
             03 MLJRNH  PIC X.                                          00001440
             03 MLJRNV  PIC X.                                          00001450
             03 MLJRNO  PIC X(29).                                      00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MWTYPAUXA    PIC X.                                     00001480
             03 MWTYPAUXC    PIC X.                                     00001490
             03 MWTYPAUXP    PIC X.                                     00001500
             03 MWTYPAUXH    PIC X.                                     00001510
             03 MWTYPAUXV    PIC X.                                     00001520
             03 MWTYPAUXO    PIC X.                                     00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MNAUXA  PIC X.                                          00001550
             03 MNAUXC  PIC X.                                          00001560
             03 MNAUXP  PIC X.                                          00001570
             03 MNAUXH  PIC X.                                          00001580
             03 MNAUXV  PIC X.                                          00001590
             03 MNAUXO  PIC X(3).                                       00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MCTRPARTA    PIC X.                                     00001620
             03 MCTRPARTC    PIC X.                                     00001630
             03 MCTRPARTP    PIC X.                                     00001640
             03 MCTRPARTH    PIC X.                                     00001650
             03 MCTRPARTV    PIC X.                                     00001660
             03 MCTRPARTO    PIC X.                                     00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MCTRPASSA    PIC X.                                     00001690
             03 MCTRPASSC    PIC X.                                     00001700
             03 MCTRPASSP    PIC X.                                     00001710
             03 MCTRPASSH    PIC X.                                     00001720
             03 MCTRPASSV    PIC X.                                     00001730
             03 MCTRPASSO    PIC X.                                     00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MJRNREGA     PIC X.                                     00001760
             03 MJRNREGC     PIC X.                                     00001770
             03 MJRNREGP     PIC X.                                     00001780
             03 MJRNREGH     PIC X.                                     00001790
             03 MJRNREGV     PIC X.                                     00001800
             03 MJRNREGO     PIC X.                                     00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MNUMAUTOA    PIC X.                                     00001830
             03 MNUMAUTOC    PIC X.                                     00001840
             03 MNUMAUTOP    PIC X.                                     00001850
             03 MNUMAUTOH    PIC X.                                     00001860
             03 MNUMAUTOV    PIC X.                                     00001870
             03 MNUMAUTOO    PIC X.                                     00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MWBATCHTPA   PIC X.                                     00001900
             03 MWBATCHTPC   PIC X.                                     00001910
             03 MWBATCHTPP   PIC X.                                     00001920
             03 MWBATCHTPH   PIC X.                                     00001930
             03 MWBATCHTPV   PIC X.                                     00001940
             03 MWBATCHTPO   PIC X.                                     00001950
      * ZONE CMD AIDA                                                   00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLIBERRA  PIC X.                                          00001980
           02 MLIBERRC  PIC X.                                          00001990
           02 MLIBERRP  PIC X.                                          00002000
           02 MLIBERRH  PIC X.                                          00002010
           02 MLIBERRV  PIC X.                                          00002020
           02 MLIBERRO  PIC X(79).                                      00002030
      * CODE TRANSACTION                                                00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
      * CICS DE TRAVAIL                                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCICSA    PIC X.                                          00002140
           02 MCICSC    PIC X.                                          00002150
           02 MCICSP    PIC X.                                          00002160
           02 MCICSH    PIC X.                                          00002170
           02 MCICSV    PIC X.                                          00002180
           02 MCICSO    PIC X(5).                                       00002190
      * NETNAME                                                         00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNETNAMA  PIC X.                                          00002220
           02 MNETNAMC  PIC X.                                          00002230
           02 MNETNAMP  PIC X.                                          00002240
           02 MNETNAMH  PIC X.                                          00002250
           02 MNETNAMV  PIC X.                                          00002260
           02 MNETNAMO  PIC X(8).                                       00002270
      * CODE TERMINAL                                                   00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MSCREENA  PIC X.                                          00002300
           02 MSCREENC  PIC X.                                          00002310
           02 MSCREENP  PIC X.                                          00002320
           02 MSCREENH  PIC X.                                          00002330
           02 MSCREENV  PIC X.                                          00002340
           02 MSCREENO  PIC X(4).                                       00002350
                                                                                
