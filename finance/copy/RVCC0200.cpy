      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCC0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCC0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0200.                                                            
           02  CC02-NSOCIETE             PIC X(0003).                           
           02  CC02-NLIEU                PIC X(0003).                           
           02  CC02-DCAISSE              PIC X(0008).                           
           02  CC02-DRECATT              PIC X(0008).                           
           02  CC02-DRECEPT              PIC X(0008).                           
           02  CC02-LCOMMENT             PIC X(0025).                           
           02  CC02-DSYST                PIC S9(13) COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCC0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-NSOCIETE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-NSOCIETE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-NLIEU-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-NLIEU-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-DCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-DCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-DRECATT-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-DRECATT-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-DRECEPT-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-DRECEPT-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-LCOMMENT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC02-LCOMMENT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC02-DSYST-F              PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
           02  CC02-DSYST-F              PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
