      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------         
      *    VUE DE LA SOUS-TABLE PSEHY CORRESP. PSE NCG HYBRYS                   
      *----------------------------------------------------------------         
       01  RVPSEHY.                                                             
           05  PSEHY-CTABLEG2    PIC X(15).                                     
           05  PSEHY-CTABLEG2-REDEF REDEFINES PSEHY-CTABLEG2.                   
               10  PSEHY-CGARANT         PIC X(05).                             
               10  PSEHY-CFAM            PIC X(05).                             
               10  PSEHY-CTARIF          PIC X(05).                             
           05  PSEHY-WTABLEG     PIC X(80).                                     
           05  PSEHY-WTABLEG-REDEF  REDEFINES PSEHY-WTABLEG.                    
               10  PSEHY-CSERVHY         PIC X(05).                             
      *----------------------------------------------------------------         
      *    FLAGS DE LA VUE                                                      
      *----------------------------------------------------------------         
       01  RVPSEHY-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEHY-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSEHY-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEHY-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSEHY-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
