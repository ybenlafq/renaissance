      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CCPA2 CTRL CAISSES - LIEN NEM/COMPTA   *        
      *----------------------------------------------------------------*        
       01  RVCCPA2.                                                             
           05  CCPA2-CTABLEG2    PIC X(15).                                     
           05  CCPA2-CTABLEG2-REDEF REDEFINES CCPA2-CTABLEG2.                   
               10  CCPA2-TYPTRANS        PIC X(03).                             
               10  CCPA2-MOPAI           PIC X(05).                             
           05  CCPA2-WTABLEG     PIC X(80).                                     
           05  CCPA2-WTABLEG-REDEF  REDEFINES CCPA2-WTABLEG.                    
               10  CCPA2-WACTIF          PIC X(01).                             
               10  CCPA2-CPAICPT         PIC X(03).                             
               10  CCPA2-FDCN            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCCPA2-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCPA2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CCPA2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCPA2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CCPA2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
