      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:08 >
      
      * ----------------------------------------------------------- *
      * - ZONE DE MESSUNICATION APPEL MODULE MGV77.               - *
      * ----------------------------------------------------------- *
        05 MESSGV77-APPLI.
          10 MESSGV77-GESTION-MSG.
             15 MESSGV77-CMSG                 PIC X(10).
      * -----RETOUR  >> 'DBOXRB010'   ------------------------------ *
      * -----ECHANGE >> 'DBOXRB020'   ------------------------------ *
             15 MESSGV77-LONG                 PIC 9(05).
      * -----LONGUEUR DONNEES DU MESSAGE --------------------------- *
             15 MESSGV77-CODMAJ               PIC X(01).
      * -----CODE MISE A JOUR CONSTANTE = 'C' ---------------------- *
          10 MESSGV77-CAPPEL                  PIC X(01).
             88 MESSGV77-CRE-RB10                        VALUE 'C'.
             88 MESSGV77-VALIDE-ECHANGE-SAV              VALUE 'E'.
             88 MESSGV77-VALIDE-ECHANGE-MAG              VALUE 'F'.
             88 MESSGV77-VALIDE-RETOUR                   VALUE 'R'.
             88 MESSGV77-ANNULE-ECHANGE                  VALUE 'A'.
          10 MESSGV77-NSOCVTE                 PIC X(03).
          10 MESSGV77-NLIEUVTE                PIC X(03).
          10 MESSGV77-NVENTE                  PIC X(07).
          10 MESSGV77-LNOM                    PIC X(25).
          10 MESSGV77-LPRENOM                 PIC X(20).
          10 MESSGV77-CVOIE                   PIC X(05).
          10 MESSGV77-CTVOIE                  PIC X(04).
          10 MESSGV77-LNOMVOIE                PIC X(21).
          10 MESSGV77-CPOSTAL                 PIC X(05).
          10 MESSGV77-LCOMMUNE                PIC X(32).
          10 MESSGV77-DONNEES       OCCURS 40.
           15 MESSGV77-NCODICREPRIS           PIC X(07).
           15 MESSGV77-NEANREPRIS             PIC X(13).
           15 MESSGV77-NSERIEREPRIS           PIC X(15).
           15 MESSGV77-WTYPMVT                PIC X(01).
           15 MESSGV77-WCOMPLET               PIC X(01).
           15 MESSGV77-WHS                    PIC X(01).
           15 MESSGV77-WACTIVATION            PIC X(01).
           15 MESSGV77-NCODICREMIS            PIC X(07).
           15 MESSGV77-NEANREMIS              PIC X(13).
           15 MESSGV77-NSERIEREMIS            PIC X(15).
           15 MESSGV77-LCOMMENT               PIC X(70).
           15 MESSGV77-CODE-RETOUR            PIC X(01).
              88 MESSGV77-ECHANGE-KO                     VALUE '1'.
      *{ remove-comma-in-dde 1.5
      *       88 MESSGV77-ECHANGE-OK                     VALUE ' ', '0'.
      *--
              88 MESSGV77-ECHANGE-OK                     VALUE ' '  '0'.
      *}
          10 MESSGV77-CODRET-VTE              PIC X(04).
          10 MESSGV77-CPROGRAMME              PIC X(10).
          10 MESSGV77-LIBRET-VTE              PIC X(59).
          10 MESSGV77-WUSER                   PIC X(10).
          10 MESSGV77-FILLER                  PIC X(200).
      
