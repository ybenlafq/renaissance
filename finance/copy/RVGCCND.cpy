      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GCCND TYPE DE COMMISSION               *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGCCND.                                                             
           05  GCCND-CTABLEG2    PIC X(15).                                     
           05  GCCND-CTABLEG2-REDEF REDEFINES GCCND-CTABLEG2.                   
               10  GCCND-CTYPCND         PIC X(05).                             
           05  GCCND-WTABLEG     PIC X(80).                                     
           05  GCCND-WTABLEG-REDEF  REDEFINES GCCND-WTABLEG.                    
               10  GCCND-LIBELLE         PIC X(40).                             
               10  GCCND-WINCCOND        PIC X(01).                             
               10  GCCND-CRGLT           PIC X(03).                             
               10  GCCND-CFACT           PIC X(03).                             
               10  GCCND-WMTVENTE        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGCCND-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCCND-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GCCND-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GCCND-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GCCND-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
