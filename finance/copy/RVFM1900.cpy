      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM1900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM1900                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1900.                                                            
           02  FM19-CPDR                                                        
               PIC X(0004).                                                     
           02  FM19-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FM19-NSEQ                                                        
               PIC X(0002).                                                     
           02  FM19-CCATEGORIE                                                  
               PIC X(0006).                                                     
           02  FM19-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM1900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM19-CPDR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM19-CPDR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM19-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM19-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM19-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM19-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM19-CCATEGORIE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM19-CCATEGORIE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM19-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM19-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
