      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu COMMISSIONS SUR SERVICE                                    00000020
      ***************************************************************** 00000030
       01   EGC20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATTRTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATTRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATTRTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATTRTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPPRESTL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPPRESTF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPPRESTI    PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MENTETEI  PIC X(78).                                      00000450
           02 MTABLISTI OCCURS   13 TIMES .                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSELI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCODEI  PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLIBELLEI    PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBL    COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNBL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MNBF    PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNBI    PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMONTANTL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MTMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTMONTANTF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MTMONTANTI   PIC X(9).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * menu COMMISSIONS SUR SERVICE                                    00000920
      ***************************************************************** 00000930
       01   EGC20O REDEFINES EGC20I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEP    PIC X.                                          00001130
           02 MPAGEH    PIC X.                                          00001140
           02 MPAGEV    PIC X.                                          00001150
           02 MPAGEO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEMAXA      PIC X.                                     00001180
           02 MPAGEMAXC PIC X.                                          00001190
           02 MPAGEMAXP PIC X.                                          00001200
           02 MPAGEMAXH PIC X.                                          00001210
           02 MPAGEMAXV PIC X.                                          00001220
           02 MPAGEMAXO      PIC X(3).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATTRTA  PIC X.                                          00001250
           02 MDATTRTC  PIC X.                                          00001260
           02 MDATTRTP  PIC X.                                          00001270
           02 MDATTRTH  PIC X.                                          00001280
           02 MDATTRTV  PIC X.                                          00001290
           02 MDATTRTO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNCODICA  PIC X.                                          00001320
           02 MNCODICC  PIC X.                                          00001330
           02 MNCODICP  PIC X.                                          00001340
           02 MNCODICH  PIC X.                                          00001350
           02 MNCODICV  PIC X.                                          00001360
           02 MNCODICO  PIC X(7).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCTYPPRESTA    PIC X.                                     00001390
           02 MCTYPPRESTC    PIC X.                                     00001400
           02 MCTYPPRESTP    PIC X.                                     00001410
           02 MCTYPPRESTH    PIC X.                                     00001420
           02 MCTYPPRESTV    PIC X.                                     00001430
           02 MCTYPPRESTO    PIC X(5).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCFAMA    PIC X.                                          00001460
           02 MCFAMC    PIC X.                                          00001470
           02 MCFAMP    PIC X.                                          00001480
           02 MCFAMH    PIC X.                                          00001490
           02 MCFAMV    PIC X.                                          00001500
           02 MCFAMO    PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCMARQA   PIC X.                                          00001530
           02 MCMARQC   PIC X.                                          00001540
           02 MCMARQP   PIC X.                                          00001550
           02 MCMARQH   PIC X.                                          00001560
           02 MCMARQV   PIC X.                                          00001570
           02 MCMARQO   PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MENTETEA  PIC X.                                          00001600
           02 MENTETEC  PIC X.                                          00001610
           02 MENTETEP  PIC X.                                          00001620
           02 MENTETEH  PIC X.                                          00001630
           02 MENTETEV  PIC X.                                          00001640
           02 MENTETEO  PIC X(78).                                      00001650
           02 MTABLISTO OCCURS   13 TIMES .                             00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MSELA   PIC X.                                          00001680
             03 MSELC   PIC X.                                          00001690
             03 MSELP   PIC X.                                          00001700
             03 MSELH   PIC X.                                          00001710
             03 MSELV   PIC X.                                          00001720
             03 MSELO   PIC X.                                          00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCODEA  PIC X.                                          00001750
             03 MCODEC  PIC X.                                          00001760
             03 MCODEP  PIC X.                                          00001770
             03 MCODEH  PIC X.                                          00001780
             03 MCODEV  PIC X.                                          00001790
             03 MCODEO  PIC X(7).                                       00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MLIBELLEA    PIC X.                                     00001820
             03 MLIBELLEC    PIC X.                                     00001830
             03 MLIBELLEP    PIC X.                                     00001840
             03 MLIBELLEH    PIC X.                                     00001850
             03 MLIBELLEV    PIC X.                                     00001860
             03 MLIBELLEO    PIC X(20).                                 00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNBA    PIC X.                                          00001890
             03 MNBC    PIC X.                                          00001900
             03 MNBP    PIC X.                                          00001910
             03 MNBH    PIC X.                                          00001920
             03 MNBV    PIC X.                                          00001930
             03 MNBO    PIC X(3).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MTMONTANTA   PIC X.                                     00001960
             03 MTMONTANTC   PIC X.                                     00001970
             03 MTMONTANTP   PIC X.                                     00001980
             03 MTMONTANTH   PIC X.                                     00001990
             03 MTMONTANTV   PIC X.                                     00002000
             03 MTMONTANTO   PIC X(9).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
