      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVFM0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM0100.                                                            
      *}                                                                        
           02  FM01-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM01-NAUX                                                        
               PIC X(0003).                                                     
           02  FM01-LAUX                                                        
               PIC X(0030).                                                     
           02  FM01-WTYPAUX                                                     
               PIC X(0001).                                                     
           02  FM01-WSAISIE                                                     
               PIC X(0001).                                                     
           02  FM01-WAUXNCG                                                     
               PIC X(0001).                                                     
           02  FM01-WLETTRE                                                     
               PIC X(0001).                                                     
           02  FM01-WRELANCE                                                    
               PIC X(0001).                                                     
           02  FM01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-LAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-LAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WTYPAUX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WTYPAUX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WAUXNCG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WAUXNCG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-WRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-WRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
