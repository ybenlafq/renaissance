      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GRAND LIVRE GENERAL MODELE                                00000020
      ***************************************************************** 00000030
       01   EFM18I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNENTITEI      PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLENTITEI      PIC X(34).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMANDEL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDEMANDEF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNDEMANDEI     PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDCREATIONI    PIC X(10).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSERL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MUSERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MUSERF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MUSERI    PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMFICHEL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MWMFICHEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMFICHEF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MWMFICHEI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAPIERL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MWPAPIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPAPIERF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MWPAPIERI      PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETABI   PIC X(3).                                       00000470
           02 MWNTRID OCCURS   3 TIMES .                                00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWNTRIL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MWNTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWNTRIF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MWNTRII      PIC X.                                     00000520
           02 MWTOTALD OCCURS   3 TIMES .                               00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOTALL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MWTOTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTOTALF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MWTOTALI     PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPAUXL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MWTYPAUXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPAUXF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWTYPAUXI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMINL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNAUXMINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMINF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNAUXMINI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMAXL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNAUXMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMAXF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNAUXMAXI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEMINL   COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNCOMPTEMINL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNCOMPTEMINF   PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNCOMPTEMINI   PIC X(6).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEMAXL   COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNCOMPTEMAXL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNCOMPTEMAXF   PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNCOMPTEMAXI   PIC X(6).                                  00000770
           02 MCNATD OCCURS   10 TIMES .                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCNATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCNATF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCNATI  PIC X(3).                                       00000820
           02 MCNATED OCCURS   10 TIMES .                               00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATEL      COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MCNATEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCNATEF      PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MCNATEI      PIC X(3).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTTRIECRITL   COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MWTTRIECRITL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWTTRIECRITF   PIC X.                                     00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MWTTRIECRITI   PIC X.                                     00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCOMPTESOLDEL      COMP PIC S9(4).                       00000920
      *--                                                                       
           02 MWCOMPTESOLDEL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MWCOMPTESOLDEF      PIC X.                                00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MWCOMPTESOLDEI      PIC X.                                00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPIECELETTREL      COMP PIC S9(4).                       00000960
      *--                                                                       
           02 MWPIECELETTREL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MWPIECELETTREF      PIC X.                                00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MWPIECELETTREI      PIC X.                                00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00001000
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCDEVISEI      PIC X(3).                                  00001030
      * ZONE CMD AIDA                                                   00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MLIBERRI  PIC X(78).                                      00001080
      * CODE TRANSACTION                                                00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      * CICS DE TRAVAIL                                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      * NETNAME                                                         00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MNETNAMI  PIC X(8).                                       00001230
      * CODE TERMINAL                                                   00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MSCREENI  PIC X(5).                                       00001280
      ***************************************************************** 00001290
      * GCT - GRAND LIVRE GENERAL MODELE                                00001300
      ***************************************************************** 00001310
       01   EFM18O REDEFINES EFM18I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
      * DATE DU JOUR                                                    00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUP  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUV  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
      * HEURE                                                           00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNENTITEA      PIC X.                                     00001510
           02 MNENTITEC PIC X.                                          00001520
           02 MNENTITEP PIC X.                                          00001530
           02 MNENTITEH PIC X.                                          00001540
           02 MNENTITEV PIC X.                                          00001550
           02 MNENTITEO      PIC X(5).                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLENTITEA      PIC X.                                     00001580
           02 MLENTITEC PIC X.                                          00001590
           02 MLENTITEP PIC X.                                          00001600
           02 MLENTITEH PIC X.                                          00001610
           02 MLENTITEV PIC X.                                          00001620
           02 MLENTITEO      PIC X(34).                                 00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNDEMANDEA     PIC X.                                     00001650
           02 MNDEMANDEC     PIC X.                                     00001660
           02 MNDEMANDEP     PIC X.                                     00001670
           02 MNDEMANDEH     PIC X.                                     00001680
           02 MNDEMANDEV     PIC X.                                     00001690
           02 MNDEMANDEO     PIC X(2).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MDCREATIONA    PIC X.                                     00001720
           02 MDCREATIONC    PIC X.                                     00001730
           02 MDCREATIONP    PIC X.                                     00001740
           02 MDCREATIONH    PIC X.                                     00001750
           02 MDCREATIONV    PIC X.                                     00001760
           02 MDCREATIONO    PIC X(10).                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MUSERA    PIC X.                                          00001790
           02 MUSERC    PIC X.                                          00001800
           02 MUSERP    PIC X.                                          00001810
           02 MUSERH    PIC X.                                          00001820
           02 MUSERV    PIC X.                                          00001830
           02 MUSERO    PIC X(25).                                      00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MWMFICHEA      PIC X.                                     00001860
           02 MWMFICHEC PIC X.                                          00001870
           02 MWMFICHEP PIC X.                                          00001880
           02 MWMFICHEH PIC X.                                          00001890
           02 MWMFICHEV PIC X.                                          00001900
           02 MWMFICHEO      PIC X.                                     00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MWPAPIERA      PIC X.                                     00001930
           02 MWPAPIERC PIC X.                                          00001940
           02 MWPAPIERP PIC X.                                          00001950
           02 MWPAPIERH PIC X.                                          00001960
           02 MWPAPIERV PIC X.                                          00001970
           02 MWPAPIERO      PIC X.                                     00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNETABA   PIC X.                                          00002000
           02 MNETABC   PIC X.                                          00002010
           02 MNETABP   PIC X.                                          00002020
           02 MNETABH   PIC X.                                          00002030
           02 MNETABV   PIC X.                                          00002040
           02 MNETABO   PIC X(3).                                       00002050
           02 DFHMS1 OCCURS   3 TIMES .                                 00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MWNTRIA      PIC X.                                     00002080
             03 MWNTRIC PIC X.                                          00002090
             03 MWNTRIP PIC X.                                          00002100
             03 MWNTRIH PIC X.                                          00002110
             03 MWNTRIV PIC X.                                          00002120
             03 MWNTRIO      PIC X.                                     00002130
           02 DFHMS2 OCCURS   3 TIMES .                                 00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MWTOTALA     PIC X.                                     00002160
             03 MWTOTALC     PIC X.                                     00002170
             03 MWTOTALP     PIC X.                                     00002180
             03 MWTOTALH     PIC X.                                     00002190
             03 MWTOTALV     PIC X.                                     00002200
             03 MWTOTALO     PIC X.                                     00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MWTYPAUXA      PIC X.                                     00002230
           02 MWTYPAUXC PIC X.                                          00002240
           02 MWTYPAUXP PIC X.                                          00002250
           02 MWTYPAUXH PIC X.                                          00002260
           02 MWTYPAUXV PIC X.                                          00002270
           02 MWTYPAUXO      PIC X.                                     00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MNAUXMINA      PIC X.                                     00002300
           02 MNAUXMINC PIC X.                                          00002310
           02 MNAUXMINP PIC X.                                          00002320
           02 MNAUXMINH PIC X.                                          00002330
           02 MNAUXMINV PIC X.                                          00002340
           02 MNAUXMINO      PIC X(3).                                  00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MNAUXMAXA      PIC X.                                     00002370
           02 MNAUXMAXC PIC X.                                          00002380
           02 MNAUXMAXP PIC X.                                          00002390
           02 MNAUXMAXH PIC X.                                          00002400
           02 MNAUXMAXV PIC X.                                          00002410
           02 MNAUXMAXO      PIC X(3).                                  00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNCOMPTEMINA   PIC X.                                     00002440
           02 MNCOMPTEMINC   PIC X.                                     00002450
           02 MNCOMPTEMINP   PIC X.                                     00002460
           02 MNCOMPTEMINH   PIC X.                                     00002470
           02 MNCOMPTEMINV   PIC X.                                     00002480
           02 MNCOMPTEMINO   PIC X(6).                                  00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNCOMPTEMAXA   PIC X.                                     00002510
           02 MNCOMPTEMAXC   PIC X.                                     00002520
           02 MNCOMPTEMAXP   PIC X.                                     00002530
           02 MNCOMPTEMAXH   PIC X.                                     00002540
           02 MNCOMPTEMAXV   PIC X.                                     00002550
           02 MNCOMPTEMAXO   PIC X(6).                                  00002560
           02 DFHMS3 OCCURS   10 TIMES .                                00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MCNATA  PIC X.                                          00002590
             03 MCNATC  PIC X.                                          00002600
             03 MCNATP  PIC X.                                          00002610
             03 MCNATH  PIC X.                                          00002620
             03 MCNATV  PIC X.                                          00002630
             03 MCNATO  PIC X(3).                                       00002640
           02 DFHMS4 OCCURS   10 TIMES .                                00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MCNATEA      PIC X.                                     00002670
             03 MCNATEC PIC X.                                          00002680
             03 MCNATEP PIC X.                                          00002690
             03 MCNATEH PIC X.                                          00002700
             03 MCNATEV PIC X.                                          00002710
             03 MCNATEO      PIC X(3).                                  00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MWTTRIECRITA   PIC X.                                     00002740
           02 MWTTRIECRITC   PIC X.                                     00002750
           02 MWTTRIECRITP   PIC X.                                     00002760
           02 MWTTRIECRITH   PIC X.                                     00002770
           02 MWTTRIECRITV   PIC X.                                     00002780
           02 MWTTRIECRITO   PIC X.                                     00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MWCOMPTESOLDEA      PIC X.                                00002810
           02 MWCOMPTESOLDEC PIC X.                                     00002820
           02 MWCOMPTESOLDEP PIC X.                                     00002830
           02 MWCOMPTESOLDEH PIC X.                                     00002840
           02 MWCOMPTESOLDEV PIC X.                                     00002850
           02 MWCOMPTESOLDEO      PIC X.                                00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MWPIECELETTREA      PIC X.                                00002880
           02 MWPIECELETTREC PIC X.                                     00002890
           02 MWPIECELETTREP PIC X.                                     00002900
           02 MWPIECELETTREH PIC X.                                     00002910
           02 MWPIECELETTREV PIC X.                                     00002920
           02 MWPIECELETTREO      PIC X.                                00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MCDEVISEA      PIC X.                                     00002950
           02 MCDEVISEC PIC X.                                          00002960
           02 MCDEVISEP PIC X.                                          00002970
           02 MCDEVISEH PIC X.                                          00002980
           02 MCDEVISEV PIC X.                                          00002990
           02 MCDEVISEO      PIC X(3).                                  00003000
      * ZONE CMD AIDA                                                   00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MLIBERRA  PIC X.                                          00003030
           02 MLIBERRC  PIC X.                                          00003040
           02 MLIBERRP  PIC X.                                          00003050
           02 MLIBERRH  PIC X.                                          00003060
           02 MLIBERRV  PIC X.                                          00003070
           02 MLIBERRO  PIC X(78).                                      00003080
      * CODE TRANSACTION                                                00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCODTRAA  PIC X.                                          00003110
           02 MCODTRAC  PIC X.                                          00003120
           02 MCODTRAP  PIC X.                                          00003130
           02 MCODTRAH  PIC X.                                          00003140
           02 MCODTRAV  PIC X.                                          00003150
           02 MCODTRAO  PIC X(4).                                       00003160
      * CICS DE TRAVAIL                                                 00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCICSA    PIC X.                                          00003190
           02 MCICSC    PIC X.                                          00003200
           02 MCICSP    PIC X.                                          00003210
           02 MCICSH    PIC X.                                          00003220
           02 MCICSV    PIC X.                                          00003230
           02 MCICSO    PIC X(5).                                       00003240
      * NETNAME                                                         00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MNETNAMA  PIC X.                                          00003270
           02 MNETNAMC  PIC X.                                          00003280
           02 MNETNAMP  PIC X.                                          00003290
           02 MNETNAMH  PIC X.                                          00003300
           02 MNETNAMV  PIC X.                                          00003310
           02 MNETNAMO  PIC X(8).                                       00003320
      * CODE TERMINAL                                                   00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MSCREENA  PIC X.                                          00003350
           02 MSCREENC  PIC X.                                          00003360
           02 MSCREENP  PIC X.                                          00003370
           02 MSCREENH  PIC X.                                          00003380
           02 MSCREENV  PIC X.                                          00003390
           02 MSCREENO  PIC X(5).                                       00003400
                                                                                
