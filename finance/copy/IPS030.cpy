      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS030 AU 19/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS030.                                                        
            05 NOMETAT-IPS030           PIC X(6) VALUE 'IPS030'.                
            05 RUPTURES-IPS030.                                                 
           10 IPS030-CGRP               PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS030-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 IPS030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS030.                                                   
           10 IPS030-CDEVISE            PIC X(06).                      014  006
           10 IPS030-LBGRP              PIC X(20).                      020  020
           10 IPS030-NSOC               PIC X(03).                      040  003
           10 IPS030-TTC                PIC S9(10)V9(2) COMP-3.         043  007
           10 IPS030-TVA                PIC S9(09)V9(2) COMP-3.         050  006
           10 IPS030-15PHT              PIC S9(09)V9(2) COMP-3.         056  006
           10 IPS030-85PHT              PIC S9(10)V9(2) COMP-3.         062  007
           10 IPS030-MOIS               PIC X(08).                      069  008
           10 IPS030-DPERCPT1           PIC X(06).                      077  006
           10 IPS030-DPERCPT2           PIC X(06).                      083  006
            05 FILLER                      PIC X(424).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS030-LONG           PIC S9(4)   COMP  VALUE +088.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS030-LONG           PIC S9(4) COMP-5  VALUE +088.           
                                                                                
      *}                                                                        
