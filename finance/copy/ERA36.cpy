      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA36   ERA36                                              00000020
      ***************************************************************** 00000030
       01   ERA36I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *                                                                 00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MPAGEI    PIC X(4).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGMAXL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MPAGMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGMAXF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MPAGMAXI  PIC X(4).                                       00000280
           02 MDEMI OCCURS   15 TIMES .                                 00000290
      * DETAIL DE demande                                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-DEML  COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLIGNE-DEML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLIGNE-DEMF  PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLIGNE-DEMI  PIC X(78).                                 00000340
      * ZONE CMD AIDA                                                   00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MZONCMDI  PIC X(15).                                      00000390
      * MESSAGE ERREUR                                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MLIBERRI  PIC X(58).                                      00000440
      * CODE TRANSACTION                                                00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODTRAI  PIC X(4).                                       00000490
      * CICS DE TRAVAIL                                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      * NETNAME                                                         00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNETNAMI  PIC X(8).                                       00000590
      * CODE TERMINAL                                                   00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MSCREENI  PIC X(5).                                       00000640
      ***************************************************************** 00000650
      * SDF: ERA36   ERA36                                              00000660
      ***************************************************************** 00000670
       01   ERA36O REDEFINES ERA36I.                                    00000680
           02 FILLER    PIC X(12).                                      00000690
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MDATJOUA  PIC X.                                          00000720
           02 MDATJOUC  PIC X.                                          00000730
           02 MDATJOUP  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUV  PIC X.                                          00000760
           02 MDATJOUO  PIC X(10).                                      00000770
      * HEURE                                                           00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MNSOCA    PIC X.                                          00000870
           02 MNSOCC    PIC X.                                          00000880
           02 MNSOCP    PIC X.                                          00000890
           02 MNSOCH    PIC X.                                          00000900
           02 MNSOCV    PIC X.                                          00000910
           02 MNSOCO    PIC X(3).                                       00000920
      *                                                                 00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MPAGEA    PIC X.                                          00000950
           02 MPAGEC    PIC X.                                          00000960
           02 MPAGEP    PIC X.                                          00000970
           02 MPAGEH    PIC X.                                          00000980
           02 MPAGEV    PIC X.                                          00000990
           02 MPAGEO    PIC 9(4).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MPAGMAXA  PIC X.                                          00001020
           02 MPAGMAXC  PIC X.                                          00001030
           02 MPAGMAXP  PIC X.                                          00001040
           02 MPAGMAXH  PIC X.                                          00001050
           02 MPAGMAXV  PIC X.                                          00001060
           02 MPAGMAXO  PIC 9(4).                                       00001070
           02 MDEMO OCCURS   15 TIMES .                                 00001080
      * DETAIL DE demande                                               00001090
             03 FILLER       PIC X(2).                                  00001100
             03 MLIGNE-DEMA  PIC X.                                     00001110
             03 MLIGNE-DEMC  PIC X.                                     00001120
             03 MLIGNE-DEMP  PIC X.                                     00001130
             03 MLIGNE-DEMH  PIC X.                                     00001140
             03 MLIGNE-DEMV  PIC X.                                     00001150
             03 MLIGNE-DEMO  PIC X(78).                                 00001160
      * ZONE CMD AIDA                                                   00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MZONCMDA  PIC X.                                          00001190
           02 MZONCMDC  PIC X.                                          00001200
           02 MZONCMDP  PIC X.                                          00001210
           02 MZONCMDH  PIC X.                                          00001220
           02 MZONCMDV  PIC X.                                          00001230
           02 MZONCMDO  PIC X(15).                                      00001240
      * MESSAGE ERREUR                                                  00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MLIBERRA  PIC X.                                          00001270
           02 MLIBERRC  PIC X.                                          00001280
           02 MLIBERRP  PIC X.                                          00001290
           02 MLIBERRH  PIC X.                                          00001300
           02 MLIBERRV  PIC X.                                          00001310
           02 MLIBERRO  PIC X(58).                                      00001320
      * CODE TRANSACTION                                                00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCODTRAA  PIC X.                                          00001350
           02 MCODTRAC  PIC X.                                          00001360
           02 MCODTRAP  PIC X.                                          00001370
           02 MCODTRAH  PIC X.                                          00001380
           02 MCODTRAV  PIC X.                                          00001390
           02 MCODTRAO  PIC X(4).                                       00001400
      * CICS DE TRAVAIL                                                 00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCICSA    PIC X.                                          00001430
           02 MCICSC    PIC X.                                          00001440
           02 MCICSP    PIC X.                                          00001450
           02 MCICSH    PIC X.                                          00001460
           02 MCICSV    PIC X.                                          00001470
           02 MCICSO    PIC X(5).                                       00001480
      * NETNAME                                                         00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNETNAMA  PIC X.                                          00001510
           02 MNETNAMC  PIC X.                                          00001520
           02 MNETNAMP  PIC X.                                          00001530
           02 MNETNAMH  PIC X.                                          00001540
           02 MNETNAMV  PIC X.                                          00001550
           02 MNETNAMO  PIC X(8).                                       00001560
      * CODE TERMINAL                                                   00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MSCREENA  PIC X.                                          00001590
           02 MSCREENC  PIC X.                                          00001600
           02 MSCREENP  PIC X.                                          00001610
           02 MSCREENH  PIC X.                                          00001620
           02 MSCREENV  PIC X.                                          00001630
           02 MSCREENO  PIC X(5).                                       00001640
                                                                                
