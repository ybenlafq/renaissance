      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS110 AU 19/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,01,BI,A,                          *        
      *                           08,05,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS110.                                                        
            05 NOMETAT-IPS110           PIC X(6) VALUE 'IPS110'.                
            05 RUPTURES-IPS110.                                                 
           10 IPS110-NUMETAT            PIC X(01).                      007  001
           10 IPS110-CGRP               PIC X(05).                      008  005
           10 IPS110-CSECTEUR           PIC X(05).                      013  005
           10 IPS110-NOSAV              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS110-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IPS110-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS110.                                                   
           10 IPS110-DEB                PIC X(07).                      023  007
           10 IPS110-FIN                PIC X(07).                      030  007
           10 IPS110-LGRP               PIC X(20).                      037  020
           10 IPS110-LIBETAT            PIC X(21).                      057  021
           10 IPS110-MOIS1              PIC X(03).                      078  003
           10 IPS110-MOIS10             PIC X(03).                      081  003
           10 IPS110-MOIS11             PIC X(03).                      084  003
           10 IPS110-MOIS12             PIC X(03).                      087  003
           10 IPS110-MOIS2              PIC X(03).                      090  003
           10 IPS110-MOIS3              PIC X(03).                      093  003
           10 IPS110-MOIS4              PIC X(03).                      096  003
           10 IPS110-MOIS5              PIC X(03).                      099  003
           10 IPS110-MOIS6              PIC X(03).                      102  003
           10 IPS110-MOIS7              PIC X(03).                      105  003
           10 IPS110-MOIS8              PIC X(03).                      108  003
           10 IPS110-MOIS9              PIC X(03).                      111  003
           10 IPS110-MT1                PIC S9(08)      COMP-3.         114  005
           10 IPS110-MT10               PIC S9(08)      COMP-3.         119  005
           10 IPS110-MT11               PIC S9(08)      COMP-3.         124  005
           10 IPS110-MT12               PIC S9(08)      COMP-3.         129  005
           10 IPS110-MT2                PIC S9(08)      COMP-3.         134  005
           10 IPS110-MT3                PIC S9(08)      COMP-3.         139  005
           10 IPS110-MT4                PIC S9(08)      COMP-3.         144  005
           10 IPS110-MT5                PIC S9(08)      COMP-3.         149  005
           10 IPS110-MT6                PIC S9(08)      COMP-3.         154  005
           10 IPS110-MT7                PIC S9(08)      COMP-3.         159  005
           10 IPS110-MT8                PIC S9(08)      COMP-3.         164  005
           10 IPS110-MT9                PIC S9(08)      COMP-3.         169  005
            05 FILLER                      PIC X(339).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS110-LONG           PIC S9(4)   COMP  VALUE +173.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS110-LONG           PIC S9(4) COMP-5  VALUE +173.           
                                                                                
      *}                                                                        
