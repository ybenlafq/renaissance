      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA31   ERA31                                              00000020
      ***************************************************************** 00000030
       01   ERA31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTYPREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPREGF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTYPREGI  PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNSOCI    PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPGLOBL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MZPGLOBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZPGLOBF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MZPGLOBI  PIC X.                                          00000270
      * MESSAGE DE CONFI                                                00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MPAGEI    PIC X(4).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGMAXL  COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MPAGMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGMAXF  PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MPAGMAXI  PIC X(4).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPDEML      COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MCTYPDEML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPDEMF      PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCTYPDEMI      PIC X(5).                                  00000400
           02 MDEMI OCCURS   15 TIMES .                                 00000410
      * DETAIL DE BE                                                    00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-DEML  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIGNE-DEML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLIGNE-DEMF  PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIGNE-DEMI  PIC X(76).                                 00000460
      * POINTAGE LIGNE                                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPLIGNEL    COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MZPLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZPLIGNEF    PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MZPLIGNEI    PIC X.                                     00000510
      * ZONE CMD AIDA                                                   00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MZONCMDI  PIC X(15).                                      00000560
      * MESSAGE ERREUR                                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(58).                                      00000610
      * CODE TRANSACTION                                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      * CICS DE TRAVAIL                                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCICSI    PIC X(5).                                       00000710
      * NETNAME                                                         00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MNETNAMI  PIC X(8).                                       00000760
      * CODE TERMINAL                                                   00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSCREENI  PIC X(5).                                       00000810
      ***************************************************************** 00000820
      * SDF: ERA31   ERA31                                              00000830
      ***************************************************************** 00000840
       01   ERA31O REDEFINES ERA31I.                                    00000850
           02 FILLER    PIC X(12).                                      00000860
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
      * HEURE                                                           00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MTIMJOUA  PIC X.                                          00000970
           02 MTIMJOUC  PIC X.                                          00000980
           02 MTIMJOUP  PIC X.                                          00000990
           02 MTIMJOUH  PIC X.                                          00001000
           02 MTIMJOUV  PIC X.                                          00001010
           02 MTIMJOUO  PIC X(5).                                       00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTYPREGA  PIC X.                                          00001040
           02 MTYPREGC  PIC X.                                          00001050
           02 MTYPREGP  PIC X.                                          00001060
           02 MTYPREGH  PIC X.                                          00001070
           02 MTYPREGV  PIC X.                                          00001080
           02 MTYPREGO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNSOCA    PIC X.                                          00001110
           02 MNSOCC    PIC X.                                          00001120
           02 MNSOCP    PIC X.                                          00001130
           02 MNSOCH    PIC X.                                          00001140
           02 MNSOCV    PIC X.                                          00001150
           02 MNSOCO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MZPGLOBA  PIC X.                                          00001180
           02 MZPGLOBC  PIC X.                                          00001190
           02 MZPGLOBP  PIC X.                                          00001200
           02 MZPGLOBH  PIC X.                                          00001210
           02 MZPGLOBV  PIC X.                                          00001220
           02 MZPGLOBO  PIC X.                                          00001230
      * MESSAGE DE CONFI                                                00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEA    PIC X.                                          00001260
           02 MPAGEC    PIC X.                                          00001270
           02 MPAGEP    PIC X.                                          00001280
           02 MPAGEH    PIC X.                                          00001290
           02 MPAGEV    PIC X.                                          00001300
           02 MPAGEO    PIC 9(4).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MPAGMAXA  PIC X.                                          00001330
           02 MPAGMAXC  PIC X.                                          00001340
           02 MPAGMAXP  PIC X.                                          00001350
           02 MPAGMAXH  PIC X.                                          00001360
           02 MPAGMAXV  PIC X.                                          00001370
           02 MPAGMAXO  PIC 9(4).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCTYPDEMA      PIC X.                                     00001400
           02 MCTYPDEMC PIC X.                                          00001410
           02 MCTYPDEMP PIC X.                                          00001420
           02 MCTYPDEMH PIC X.                                          00001430
           02 MCTYPDEMV PIC X.                                          00001440
           02 MCTYPDEMO      PIC X(5).                                  00001450
           02 MDEMO OCCURS   15 TIMES .                                 00001460
      * DETAIL DE BE                                                    00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MLIGNE-DEMA  PIC X.                                     00001490
             03 MLIGNE-DEMC  PIC X.                                     00001500
             03 MLIGNE-DEMP  PIC X.                                     00001510
             03 MLIGNE-DEMH  PIC X.                                     00001520
             03 MLIGNE-DEMV  PIC X.                                     00001530
             03 MLIGNE-DEMO  PIC X(76).                                 00001540
      * POINTAGE LIGNE                                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MZPLIGNEA    PIC X.                                     00001570
             03 MZPLIGNEC    PIC X.                                     00001580
             03 MZPLIGNEP    PIC X.                                     00001590
             03 MZPLIGNEH    PIC X.                                     00001600
             03 MZPLIGNEV    PIC X.                                     00001610
             03 MZPLIGNEO    PIC X.                                     00001620
      * ZONE CMD AIDA                                                   00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MZONCMDA  PIC X.                                          00001650
           02 MZONCMDC  PIC X.                                          00001660
           02 MZONCMDP  PIC X.                                          00001670
           02 MZONCMDH  PIC X.                                          00001680
           02 MZONCMDV  PIC X.                                          00001690
           02 MZONCMDO  PIC X(15).                                      00001700
      * MESSAGE ERREUR                                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLIBERRA  PIC X.                                          00001730
           02 MLIBERRC  PIC X.                                          00001740
           02 MLIBERRP  PIC X.                                          00001750
           02 MLIBERRH  PIC X.                                          00001760
           02 MLIBERRV  PIC X.                                          00001770
           02 MLIBERRO  PIC X(58).                                      00001780
      * CODE TRANSACTION                                                00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCODTRAA  PIC X.                                          00001810
           02 MCODTRAC  PIC X.                                          00001820
           02 MCODTRAP  PIC X.                                          00001830
           02 MCODTRAH  PIC X.                                          00001840
           02 MCODTRAV  PIC X.                                          00001850
           02 MCODTRAO  PIC X(4).                                       00001860
      * CICS DE TRAVAIL                                                 00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCICSA    PIC X.                                          00001890
           02 MCICSC    PIC X.                                          00001900
           02 MCICSP    PIC X.                                          00001910
           02 MCICSH    PIC X.                                          00001920
           02 MCICSV    PIC X.                                          00001930
           02 MCICSO    PIC X(5).                                       00001940
      * NETNAME                                                         00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNETNAMA  PIC X.                                          00001970
           02 MNETNAMC  PIC X.                                          00001980
           02 MNETNAMP  PIC X.                                          00001990
           02 MNETNAMH  PIC X.                                          00002000
           02 MNETNAMV  PIC X.                                          00002010
           02 MNETNAMO  PIC X(8).                                       00002020
      * CODE TERMINAL                                                   00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(5).                                       00002100
                                                                                
