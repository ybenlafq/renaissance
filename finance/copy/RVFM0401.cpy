      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVFM0401                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM0401                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM0401.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM0401.                                                            
      *}                                                                        
           02  FM04-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM04-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FM04-LDEVISE                                                     
               PIC X(0025).                                                     
           02  FM04-NBDECIM                                                     
               PIC S9(1) COMP-3.                                                
           02  FM04-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FM04-LDEV                                                        
               PIC X(0005).                                                     
           02  FM04-LSUB                                                        
               PIC X(0005).                                                     
           02  FM04-LSUBDIVIS                                                   
               PIC X(0025).                                                     
           02  FM04-CCAISSE                                                     
               PIC X(0001).                                                     
           02  FM04-WEURO                                                       
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM0401                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM0401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM0401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-LDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-LDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-NBDECIM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-NBDECIM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-LDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-LDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-LSUB-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-LSUB-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-LSUBDIVIS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-LSUBDIVIS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-CCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-CCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM04-WEURO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM04-WEURO-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
