      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFM64                      *        
      ******************************************************************        
       01  RVFM6401.                                                            
           10 FM64-STEAPP               PIC X(5).                               
           10 FM64-LSTEAPPC             PIC X(15).                              
           10 FM64-LSTEAPPL             PIC X(30).                              
           10 FM64-CMASQUE              PIC X(5).                               
           10 FM64-DCLOTURE             PIC X(8).                               
           10 FM64-DMAJ                 PIC X(8).                               
           10 FM64-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 FM64-NSIRET               PIC X(14).                              
                                                                                
