      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:16 >
      
      *****************************************************************
      *
      *   COPY MESCONSO STRICTEMENT EGALE A CCBMDCONSO SUR AS400
      *
      *   COPY MESSAGES MQ ENVOYE PAR L'AS400 POUR ALIMENTATION RTSL07
      *
      *****************************************************************
      *
      *    Description du message.
       05  MESS-CONSO-MSG.
      *    Code fonction.
           15 MESS-CONSO-CFONC            PIC X.
      *    Table des enregistrements contenus.
           15 MESS-CONSO-TENREGS          PIC X(480).
      *    Donn�es.
           15 MESS-CONSO-DONNEES          PIC X(25000).
      *
      *    TABLE DES ENREGISTREMENTS CONTENUS. Version 1.
       01  MESS-CONSO-TABLE.
           05 MESS-CONSO-ZONES.
      *       Nombre de postes de la table.
              10 MESS-CONSO-NBP        PIC 9(5).
      *       Description des postes de la table.
              10 MESS-CONSO-POSTE OCCURS 25  INDEXED ICONSO.
      *          Nom de l'enregistrement.
                 25 MESS-CONSO-NOM-ENR PIC X(10).
      *          Nombre d'enregistrements.
                 25 MESS-CONSO-NBRE    PIC 9(05) COMP-3.
      *          Position de d�part.
                 25 MESS-CONSO-POS     PIC 9(05) COMP-3.
      *          Longueur de l'enregistrement.
                 25 MESS-CONSO-LGE     PIC 9(05) COMP-3.
      *
      *    TABLE DES ENREGISTREMENTS CONTENUS. Version 2.
       01  MESS-CONSO-TABLE-V2.
           05 MESS-CONSO-ZONES-V2.
      *       Nombre de postes de la table.
              10 MESS-CONSO-NBP-V2        PIC 9(5).
      *       Description des postes de la table.
              10 MESS-CONSO-POSTE-V2   OCCURS 19
                                      INDEXED IC-V2.
      *          Nom de l'enregistrement.
                 25 MESS-CONSO-NOM-ENR-V2 PIC X(10).
      *          Nombre d'enregistrements.
                 25 MESS-CONSO-NBRE-V2    PIC 9(05).
      *          Position de d�part.
                 25 MESS-CONSO-POS-V2     PIC 9(05).
      *          Longueur de l'enregistrement.
                 25 MESS-CONSO-LGE-V2     PIC 9(05).
      *    TABLE DES ENREGISTREMENTS CONTENUS. Version 3.
       01  MESS-CONSO-TABLE-V3.
           05 MESS-CONSO-ZONES-V3.
      *       Nombre de postes de la table.
              10 MESS-CONSO-NBP-V3        PIC 9(5).
      *       Description des postes de la table.
              10 MESS-CONSO-POSTE-V3   OCCURS 19
                                      INDEXED IC-V3.
      *          Nom de l'enregistrement.
                 25 MESS-CONSO-NOM-ENR-V3 PIC X(10).
      *          Nombre d'enregistrements.
                 25 MESS-CONSO-NBRE-V3    PIC 9(05).
      *          Position de d�part.
                 25 MESS-CONSO-POS-V3     PIC 9(05).
      *          Longueur de l'enregistrement.
                 25 MESS-CONSO-LGE-V3     PIC 9(05).
      
