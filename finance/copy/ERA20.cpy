      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA20   ERA20                                              00000020
      ***************************************************************** 00000030
       01   ERA20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * MAGASIN CEDANT                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNSOCI    PIC X(3).                                       00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MNLIEUI   PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MZONCMDI  PIC X(2).                                       00000280
      * MAGASIN CEDANT                                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNDEML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNDEMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNDEMI    PIC X(5).                                       00000330
      * MAGASIN CEDANT                                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MNOMI     PIC X(25).                                      00000380
      * MAGASIN CEDANT                                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDECHEANI      PIC X(10).                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MTYPEI    PIC X.                                          00000470
      * MESSAGE ERREUR                                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MLIBERRI  PIC X(78).                                      00000520
      * CODE TRANSACTION                                                00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      * CICS DE TRAVAIL                                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      * NETNAME                                                         00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNETNAMI  PIC X(8).                                       00000670
      * CODE TERMINAL                                                   00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MSCREENI  PIC X(5).                                       00000720
      ***************************************************************** 00000730
      * SDF: ERA20   ERA20                                              00000740
      ***************************************************************** 00000750
       01   ERA20O REDEFINES ERA20I.                                    00000760
           02 FILLER    PIC X(12).                                      00000770
      * DATE DU JOUR                                                    00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
      * HEURE                                                           00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
      * MAGASIN CEDANT                                                  00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MNSOCA    PIC X.                                          00000960
           02 MNSOCC    PIC X.                                          00000970
           02 MNSOCP    PIC X.                                          00000980
           02 MNSOCH    PIC X.                                          00000990
           02 MNSOCV    PIC X.                                          00001000
           02 MNSOCO    PIC X(3).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNLIEUA   PIC X.                                          00001030
           02 MNLIEUC   PIC X.                                          00001040
           02 MNLIEUP   PIC X.                                          00001050
           02 MNLIEUH   PIC X.                                          00001060
           02 MNLIEUV   PIC X.                                          00001070
           02 MNLIEUO   PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MZONCMDA  PIC X.                                          00001100
           02 MZONCMDC  PIC X.                                          00001110
           02 MZONCMDP  PIC X.                                          00001120
           02 MZONCMDH  PIC X.                                          00001130
           02 MZONCMDV  PIC X.                                          00001140
           02 MZONCMDO  PIC X(2).                                       00001150
      * MAGASIN CEDANT                                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MNDEMA    PIC X.                                          00001180
           02 MNDEMC    PIC X.                                          00001190
           02 MNDEMP    PIC X.                                          00001200
           02 MNDEMH    PIC X.                                          00001210
           02 MNDEMV    PIC X.                                          00001220
           02 MNDEMO    PIC X(5).                                       00001230
      * MAGASIN CEDANT                                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNOMA     PIC X.                                          00001260
           02 MNOMC     PIC X.                                          00001270
           02 MNOMP     PIC X.                                          00001280
           02 MNOMH     PIC X.                                          00001290
           02 MNOMV     PIC X.                                          00001300
           02 MNOMO     PIC X(25).                                      00001310
      * MAGASIN CEDANT                                                  00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDECHEANA      PIC X.                                     00001340
           02 MDECHEANC PIC X.                                          00001350
           02 MDECHEANP PIC X.                                          00001360
           02 MDECHEANH PIC X.                                          00001370
           02 MDECHEANV PIC X.                                          00001380
           02 MDECHEANO      PIC X(10).                                 00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MTYPEA    PIC X.                                          00001410
           02 MTYPEC    PIC X.                                          00001420
           02 MTYPEP    PIC X.                                          00001430
           02 MTYPEH    PIC X.                                          00001440
           02 MTYPEV    PIC X.                                          00001450
           02 MTYPEO    PIC X.                                          00001460
      * MESSAGE ERREUR                                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLIBERRA  PIC X.                                          00001490
           02 MLIBERRC  PIC X.                                          00001500
           02 MLIBERRP  PIC X.                                          00001510
           02 MLIBERRH  PIC X.                                          00001520
           02 MLIBERRV  PIC X.                                          00001530
           02 MLIBERRO  PIC X(78).                                      00001540
      * CODE TRANSACTION                                                00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCODTRAA  PIC X.                                          00001570
           02 MCODTRAC  PIC X.                                          00001580
           02 MCODTRAP  PIC X.                                          00001590
           02 MCODTRAH  PIC X.                                          00001600
           02 MCODTRAV  PIC X.                                          00001610
           02 MCODTRAO  PIC X(4).                                       00001620
      * CICS DE TRAVAIL                                                 00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCICSA    PIC X.                                          00001650
           02 MCICSC    PIC X.                                          00001660
           02 MCICSP    PIC X.                                          00001670
           02 MCICSH    PIC X.                                          00001680
           02 MCICSV    PIC X.                                          00001690
           02 MCICSO    PIC X(5).                                       00001700
      * NETNAME                                                         00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNETNAMA  PIC X.                                          00001730
           02 MNETNAMC  PIC X.                                          00001740
           02 MNETNAMP  PIC X.                                          00001750
           02 MNETNAMH  PIC X.                                          00001760
           02 MNETNAMV  PIC X.                                          00001770
           02 MNETNAMO  PIC X(8).                                       00001780
      * CODE TERMINAL                                                   00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MSCREENA  PIC X.                                          00001810
           02 MSCREENC  PIC X.                                          00001820
           02 MSCREENP  PIC X.                                          00001830
           02 MSCREENH  PIC X.                                          00001840
           02 MSCREENV  PIC X.                                          00001850
           02 MSCREENO  PIC X(5).                                       00001860
                                                                                
