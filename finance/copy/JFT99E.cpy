      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JFT99E AU 25/06/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JFT99E.                                                        
            05 NOMETAT-JFT99E           PIC X(6) VALUE 'JFT99E'.                
            05 RUPTURES-JFT99E.                                                 
           10 JFT99E-CODE               PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JFT99E-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 JFT99E-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JFT99E.                                                   
           10 JFT99E-LIBELLE            PIC X(25).                      014  025
            05 FILLER                      PIC X(474).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JFT99E-LONG           PIC S9(4)   COMP  VALUE +038.           
      *                                                                         
      *--                                                                       
        01  DSECT-JFT99E-LONG           PIC S9(4) COMP-5  VALUE +038.           
                                                                                
      *}                                                                        
