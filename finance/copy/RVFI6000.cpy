      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RVFI6000)                                  *        
      *        LIBRARY(DSA016.DEVL.COPY.COBOL(RVFI6000))               *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(FI60-)                                            *        
      *        STRUCTURE(RVFI6000)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVFI6000 TABLE                               
           ( CPROG                          CHAR(5) NOT NULL,                   
             NSOCORIG                       CHAR(3) NOT NULL,                   
             NLIEUORIG                      CHAR(3) NOT NULL,                   
             NSOCDEST                       CHAR(3) NOT NULL,                   
             NLIEUDEST                      CHAR(3) NOT NULL,                   
             NSOCVTE                        CHAR(3) NOT NULL,                   
             NLIEUVTE                       CHAR(3) NOT NULL,                   
             COPCO                          CHAR(6) NOT NULL,                   
             TYPMVT                         CHAR(15) NOT NULL,                  
             CFLAG                          CHAR(1) NOT NULL                    
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFI6000                    *        
      ******************************************************************        
       01  RVFI6000.                                                            
      *                       CPROG                                             
           10 FI60-CPROG           PIC X(5).                                    
      *                       NSOCORIG                                          
           10 FI60-NSOCORIG        PIC X(3).                                    
      *                       NLIEUORIG                                         
           10 FI60-NLIEUORIG       PIC X(3).                                    
      *                       NSOCDEST                                          
           10 FI60-NSOCDEST        PIC X(3).                                    
      *                       NLIEUDEST                                         
           10 FI60-NLIEUDEST       PIC X(3).                                    
      *                       NSOCVTE                                           
           10 FI60-NSOCVTE         PIC X(3).                                    
      *                       NLIEUVTE                                          
           10 FI60-NLIEUVTE        PIC X(3).                                    
      *                       COPCO                                             
           10 FI60-COPCO           PIC X(6).                                    
      *                       TYPMVT                                            
           10 FI60-TYPMVT          PIC X(15).                                   
      *                       CFLAG                                             
           10 FI60-CFLAG           PIC X(1).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
