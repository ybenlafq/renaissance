      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - liste des plans de comptes                                00000020
      ***************************************************************** 00000030
       01   EFM21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MTITREI   PIC X(9).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MPAGETOTI      PIC X(2).                                  00000250
           02 LIGNEI OCCURS   12 TIMES .                                00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTIONL  COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MSELECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSELECTIONF  PIC X.                                     00000280
             03 FILLER  PIC X(2).                                       00000290
             03 MSELECTIONI  PIC X.                                     00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPLANL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCPLANL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPLANF      PIC X.                                     00000320
             03 FILLER  PIC X(2).                                       00000330
             03 MCPLANI      PIC X(4).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPLANL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLPLANL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLPLANF      PIC X.                                     00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MLPLANI      PIC X(20).                                 00000380
             03 MNENTITED OCCURS   4 TIMES .                            00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNENTITEL  COMP PIC S9(4).                            00000400
      *--                                                                       
               04 MNENTITEL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MNENTITEF  PIC X.                                     00000410
               04 FILLER     PIC X(2).                                  00000420
               04 MNENTITEI  PIC X(5).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MLIBERRI  PIC X(79).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MCODTRAI  PIC X(4).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MSCREENI  PIC X(4).                                       00000590
      ***************************************************************** 00000600
      * GCT - liste des plans de comptes                                00000610
      ***************************************************************** 00000620
       01   EFM21O REDEFINES EFM21I.                                    00000630
           02 FILLER    PIC X(12).                                      00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MDATJOUA  PIC X.                                          00000660
           02 MDATJOUC  PIC X.                                          00000670
           02 MDATJOUH  PIC X.                                          00000680
           02 MDATJOUO  PIC X(10).                                      00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MTIMJOUA  PIC X.                                          00000710
           02 MTIMJOUC  PIC X.                                          00000720
           02 MTIMJOUH  PIC X.                                          00000730
           02 MTIMJOUO  PIC X(5).                                       00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTITREA   PIC X.                                          00000760
           02 MTITREC   PIC X.                                          00000770
           02 MTITREH   PIC X.                                          00000780
           02 MTITREO   PIC X(9).                                       00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MPAGEA    PIC X.                                          00000810
           02 MPAGEC    PIC X.                                          00000820
           02 MPAGEH    PIC X.                                          00000830
           02 MPAGEO    PIC Z9.                                         00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MPAGETOTA      PIC X.                                     00000860
           02 MPAGETOTC PIC X.                                          00000870
           02 MPAGETOTH PIC X.                                          00000880
           02 MPAGETOTO      PIC Z9.                                    00000890
           02 LIGNEO OCCURS   12 TIMES .                                00000900
             03 FILLER       PIC X(2).                                  00000910
             03 MSELECTIONA  PIC X.                                     00000920
             03 MSELECTIONC  PIC X.                                     00000930
             03 MSELECTIONH  PIC X.                                     00000940
             03 MSELECTIONO  PIC X.                                     00000950
             03 FILLER       PIC X(2).                                  00000960
             03 MCPLANA      PIC X.                                     00000970
             03 MCPLANC PIC X.                                          00000980
             03 MCPLANH PIC X.                                          00000990
             03 MCPLANO      PIC X(4).                                  00001000
             03 FILLER       PIC X(2).                                  00001010
             03 MLPLANA      PIC X.                                     00001020
             03 MLPLANC PIC X.                                          00001030
             03 MLPLANH PIC X.                                          00001040
             03 MLPLANO      PIC X(20).                                 00001050
             03 DFHMS1 OCCURS   4 TIMES .                               00001060
               04 FILLER     PIC X(2).                                  00001070
               04 MNENTITEA  PIC X.                                     00001080
               04 MNENTITEC  PIC X.                                     00001090
               04 MNENTITEH  PIC X.                                     00001100
               04 MNENTITEO  PIC X(5).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLIBERRA  PIC X.                                          00001130
           02 MLIBERRC  PIC X.                                          00001140
           02 MLIBERRH  PIC X.                                          00001150
           02 MLIBERRO  PIC X(79).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCODTRAA  PIC X.                                          00001180
           02 MCODTRAC  PIC X.                                          00001190
           02 MCODTRAH  PIC X.                                          00001200
           02 MCODTRAO  PIC X(4).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCICSA    PIC X.                                          00001230
           02 MCICSC    PIC X.                                          00001240
           02 MCICSH    PIC X.                                          00001250
           02 MCICSO    PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MSCREENA  PIC X.                                          00001280
           02 MSCREENC  PIC X.                                          00001290
           02 MSCREENH  PIC X.                                          00001300
           02 MSCREENO  PIC X(4).                                       00001310
                                                                                
