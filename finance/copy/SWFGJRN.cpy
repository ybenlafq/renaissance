      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    FICHIER DU TABLEAU DE REMUNEARTION DE COMPTE COURANT                 
      *                                                                         
      *-------------------------------------------------------------*           
      *   NB : EN CAS DE MODIFICATION, ALLER MODIFIER EGALEMENT LE  *           
      *        PROGRAMME BFG261 QUI UTILISE UNE ZONE WFGJRN, COPIE  *           
      *        EXACTE DE SWFGJRN                                    *           
      *-------------------------------------------------------------*           
      *                                                                         
       01       FFGJRN-DSECT.                                                   
           05   FFGJRN-IDENT.                                                   
001          10 FFGJRN-NSOCCOMPT         PIC X(03).                             
004          10 FFGJRN-DVALEUR           PIC X(08).                             
           05   FFGJRN-SUITE.                                                   
012          10 FFGJRN-NSOC              PIC X(3).                              
015          10 FFGJRN-NLIEU             PIC X(3).                              
018          10 FFGJRN-DPIECE            PIC X(8).                              
026          10 FFGJRN-TYPPIECE          PIC X(2).                              
028          10 FFGJRN-MONTANT           PIC S9(13)V9(2) COMP-3.                
036          10 FFGJRN-LIBELLE           PIC X(20).                             
056          10 FFGJRN-TYPE              PIC X(1).                              
057          10 FFGJRN-SOLDE-PREC        PIC S9(13)V9(2) COMP-3.                
065          10 FFGJRN-DATE-PREC         PIC X(08).                             
073          10 FFGJRN-FILLER            PIC X(08).                             
->080 *                                                                         
                                                                                
