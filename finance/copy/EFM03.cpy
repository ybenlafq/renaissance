      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - MODES DE PAIEMENT                                         00000020
      ***************************************************************** 00000030
       01   EFM03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MTABI OCCURS   14 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MWACTIFI     PIC X.                                     00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMETHODEL   COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MCMETHODEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMETHODEF   PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MCMETHODEI   PIC X(3).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMETHODEL   COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLMETHODEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLMETHODEF   PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLMETHODEI   PIC X(30).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRIBL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 MWRIBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWRIBF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MWRIBI  PIC X.                                          00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWNUMAUTOL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MWNUMAUTOL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWNUMAUTOF   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MWNUMAUTOI   PIC X.                                     00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWEAPL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MWEAPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWEAPF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MWEAPI  PIC X.                                          00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDELAIL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MDELAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDELAIF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MDELAII      PIC X(2).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDOCL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 MCDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCDOCF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCDOCI  PIC X(6).                                       00000560
      * ZONE CMD AIDA                                                   00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(79).                                      00000610
      * CODE TRANSACTION                                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      * CICS DE TRAVAIL                                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCICSI    PIC X(5).                                       00000710
      * NETNAME                                                         00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MNETNAMI  PIC X(8).                                       00000760
      * CODE TERMINAL                                                   00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSCREENI  PIC X(4).                                       00000810
      ***************************************************************** 00000820
      * GCT - MODES DE PAIEMENT                                         00000830
      ***************************************************************** 00000840
       01   EFM03O REDEFINES EFM03I.                                    00000850
           02 FILLER    PIC X(12).                                      00000860
      * DATE DU JOUR                                                    00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
      * HEURE                                                           00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MTIMJOUA  PIC X.                                          00000970
           02 MTIMJOUC  PIC X.                                          00000980
           02 MTIMJOUP  PIC X.                                          00000990
           02 MTIMJOUH  PIC X.                                          00001000
           02 MTIMJOUV  PIC X.                                          00001010
           02 MTIMJOUO  PIC X(5).                                       00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MPAGEA    PIC X.                                          00001040
           02 MPAGEC    PIC X.                                          00001050
           02 MPAGEP    PIC X.                                          00001060
           02 MPAGEH    PIC X.                                          00001070
           02 MPAGEV    PIC X.                                          00001080
           02 MPAGEO    PIC Z9.                                         00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNBPA     PIC X.                                          00001110
           02 MNBPC     PIC X.                                          00001120
           02 MNBPP     PIC X.                                          00001130
           02 MNBPH     PIC X.                                          00001140
           02 MNBPV     PIC X.                                          00001150
           02 MNBPO     PIC Z9.                                         00001160
           02 MTABO OCCURS   14 TIMES .                                 00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MWACTIFA     PIC X.                                     00001190
             03 MWACTIFC     PIC X.                                     00001200
             03 MWACTIFP     PIC X.                                     00001210
             03 MWACTIFH     PIC X.                                     00001220
             03 MWACTIFV     PIC X.                                     00001230
             03 MWACTIFO     PIC X.                                     00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MCMETHODEA   PIC X.                                     00001260
             03 MCMETHODEC   PIC X.                                     00001270
             03 MCMETHODEP   PIC X.                                     00001280
             03 MCMETHODEH   PIC X.                                     00001290
             03 MCMETHODEV   PIC X.                                     00001300
             03 MCMETHODEO   PIC X(3).                                  00001310
             03 FILLER       PIC X(2).                                  00001320
             03 MLMETHODEA   PIC X.                                     00001330
             03 MLMETHODEC   PIC X.                                     00001340
             03 MLMETHODEP   PIC X.                                     00001350
             03 MLMETHODEH   PIC X.                                     00001360
             03 MLMETHODEV   PIC X.                                     00001370
             03 MLMETHODEO   PIC X(30).                                 00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MWRIBA  PIC X.                                          00001400
             03 MWRIBC  PIC X.                                          00001410
             03 MWRIBP  PIC X.                                          00001420
             03 MWRIBH  PIC X.                                          00001430
             03 MWRIBV  PIC X.                                          00001440
             03 MWRIBO  PIC X.                                          00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MWNUMAUTOA   PIC X.                                     00001470
             03 MWNUMAUTOC   PIC X.                                     00001480
             03 MWNUMAUTOP   PIC X.                                     00001490
             03 MWNUMAUTOH   PIC X.                                     00001500
             03 MWNUMAUTOV   PIC X.                                     00001510
             03 MWNUMAUTOO   PIC X.                                     00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MWEAPA  PIC X.                                          00001540
             03 MWEAPC  PIC X.                                          00001550
             03 MWEAPP  PIC X.                                          00001560
             03 MWEAPH  PIC X.                                          00001570
             03 MWEAPV  PIC X.                                          00001580
             03 MWEAPO  PIC X.                                          00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MDELAIA      PIC X.                                     00001610
             03 MDELAIC PIC X.                                          00001620
             03 MDELAIP PIC X.                                          00001630
             03 MDELAIH PIC X.                                          00001640
             03 MDELAIV PIC X.                                          00001650
             03 MDELAIO      PIC 9(2).                                  00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MCDOCA  PIC X.                                          00001680
             03 MCDOCC  PIC X.                                          00001690
             03 MCDOCP  PIC X.                                          00001700
             03 MCDOCH  PIC X.                                          00001710
             03 MCDOCV  PIC X.                                          00001720
             03 MCDOCO  PIC X(6).                                       00001730
      * ZONE CMD AIDA                                                   00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLIBERRA  PIC X.                                          00001760
           02 MLIBERRC  PIC X.                                          00001770
           02 MLIBERRP  PIC X.                                          00001780
           02 MLIBERRH  PIC X.                                          00001790
           02 MLIBERRV  PIC X.                                          00001800
           02 MLIBERRO  PIC X(79).                                      00001810
      * CODE TRANSACTION                                                00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
      * CICS DE TRAVAIL                                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCICSA    PIC X.                                          00001920
           02 MCICSC    PIC X.                                          00001930
           02 MCICSP    PIC X.                                          00001940
           02 MCICSH    PIC X.                                          00001950
           02 MCICSV    PIC X.                                          00001960
           02 MCICSO    PIC X(5).                                       00001970
      * NETNAME                                                         00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNETNAMA  PIC X.                                          00002000
           02 MNETNAMC  PIC X.                                          00002010
           02 MNETNAMP  PIC X.                                          00002020
           02 MNETNAMH  PIC X.                                          00002030
           02 MNETNAMV  PIC X.                                          00002040
           02 MNETNAMO  PIC X(8).                                       00002050
      * CODE TERMINAL                                                   00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MSCREENA  PIC X.                                          00002080
           02 MSCREENC  PIC X.                                          00002090
           02 MSCREENP  PIC X.                                          00002100
           02 MSCREENH  PIC X.                                          00002110
           02 MSCREENV  PIC X.                                          00002120
           02 MSCREENO  PIC X(4).                                       00002130
                                                                                
