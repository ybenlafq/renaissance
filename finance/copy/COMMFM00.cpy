      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00750000
      *    COMMAREA GCT - PARAMETRAGE ENTITE MODELE                *    00760000
      **************************************************************    00770000
                                                                        00780000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-FM00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00790000
      *                                                                         
      *--                                                                       
       01  COM-FM00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
                                                                        00800000
      *}                                                                        
       01  Z-COMMAREA.                                                  00810000
                                                                        00820000
      * ZONES RESERVEES AIDA ------------------------------------- 100  00830000
     1    02 FILLER-COM-AIDA                  PIC  X(100).              00840000
                                                                        00850000
      * ZONES RESERVEES CICS ------------------------------------- 020  00860000
   101    02 COMM-CICS-APPLID                 PIC  X(008).              00870000
   109    02 COMM-CICS-NETNAM                 PIC  X(008).              00880000
   117    02 COMM-CICS-TRANSA                 PIC  X(004).              00890000
                                                                        00900000
      * DATE DU JOUR --------------------------------------------- 100  00910000
   121    02 COMM-DATE-SIECLE                 PIC  9(002).              00920000
   123    02 COMM-DATE-ANNEE                  PIC  9(002).              00930000
   125    02 COMM-DATE-MOIS                   PIC  9(002).              00940000
   127    02 COMM-DATE-JOUR                   PIC  9(002).              00950000
      * QUANTIEMES CALENDAIRE ET STANDARD                               00960000
   129    02 COMM-DATE-QNTA                   PIC  9(003).              00970000
   132    02 COMM-DATE-QNT0                   PIC  9(005).              00980000
      * ANNEE BISSEXTILE 1=OUI 0=NON                                    00990000
   137    02 COMM-DATE-BISX                   PIC  9(001).              01000000
      * JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                             01010000
   138    02 COMM-DATE-JSM                    PIC  9(001).              01020000
      * LIBELLES DU JOUR COURT - LONG                                   01030000
   139    02 COMM-DATE-JSM-LC                 PIC  X(003).              01040000
   142    02 COMM-DATE-JSM-LL                 PIC  X(008).              01050000
      * LIBELLES DU MOIS COURT - LONG                                   01060000
   150    02 COMM-DATE-MOIS-LC                PIC  X(003).              01070000
   153    02 COMM-DATE-MOIS-LL                PIC  X(008).              01080000
      * DIFFERENTES FORMES DE DATE                                      01090000
   161    02 COMM-DATE-SSAAMMJJ               PIC  X(008).              01100000
   169    02 COMM-DATE-AAMMJJ                 PIC  X(006).              01110000
   175    02 COMM-DATE-JJMMSSAA               PIC  X(008).              01120000
   183    02 COMM-DATE-JJMMAA                 PIC  X(006).              01130000
   189    02 COMM-DATE-JJ-MM-AA               PIC  X(008).              01140000
   197    02 COMM-DATE-JJ-MM-SSAA             PIC  X(010).              01150000
      * TRAITEMENT NUMERO DE SEMAINE                                    01160000
          02 COMM-DATE-WEEK.                                            01170000
   207       05 COMM-DATE-SEMSS               PIC  9(002).              01180000
   209       05 COMM-DATE-SEMAA               PIC  9(002).              01190000
   211       05 COMM-DATE-SEMNU               PIC  9(002).              01200000
   213    02 COMM-DATE-FILLER                 PIC  X(008).              01210000
                                                                        01220000
      * ATTRIBUTS BMS                                                   01230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   221*   02 COMM-SWAP-CURS                   PIC S9(4) COMP VALUE -1.  01240000
      *--                                                                       
          02 COMM-SWAP-CURS                   PIC S9(4) COMP-5 VALUE -1.        
      *}                                                                        
   223    02 COMM-FM00-ZMAP                   PIC  X(001).              01250000
      *                                                                 01260000
      ******************************************************************01270000
      ************** ZONES APPLICATIVES FM00 GENERALES *****************01270000
      ******************************************************************01270000
      *                                                                 01260000
          02 COMM-FM00.                                                 01270100
   224       03 COMM-FM00-NENTITE             PIC  X(005).              01270300
   229       03 COMM-FM00-LENTITE             PIC  X(030).              01270400
   259       03 COMM-FM00-NOM-PROG            PIC  X(005).              01272000
   264       03 COMM-FM00-CACID.                                        01290210
                05 COMM-FM00-CSERVICE         PIC  X(004).              01290210
                05 FILLER                     PIC  X(004).              01290210
   272       03 COMM-FM00-PF21                PIC  X(001).              01271300
   273       03 COMM-FM00-CODE                PIC  X(008).              01271300
PCA   ******* PAGINATION ***********************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   281*      03 COMM-FM00-PAGE                PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-PAGE                PIC S9(004) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   283*      03 COMM-FM00-NBP                 PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-NBP                 PIC S9(004) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   285*      03 COMM-FM00-PAGE-B              PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-PAGE-B              PIC S9(004) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   287*      03 COMM-FM00-NBP-B               PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-NBP-B               PIC S9(004) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   289*      03 COMM-FM00-PAGE-T              PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-PAGE-T              PIC S9(004) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   291*      03 COMM-FM00-NBP-T               PIC S9(004) COMP.                 
      *--                                                                       
             03 COMM-FM00-NBP-T               PIC S9(004) COMP-5.               
      *}                                                                        
PCA   ******* PLANS DE COMPTES/SECTIONS/RUBRIQUES **********************        
   293       03 COMM-FM00-CPCG                PIC  X(004).                      
   297       03 COMM-FM00-CPDS                PIC  X(004).                      
   301       03 COMM-FM00-CPDR                PIC  X(004).                      
   305       03 COMM-FM00-LPCG                PIC  X(020).                      
   325       03 COMM-FM00-LPDS                PIC  X(020).                      
   345       03 COMM-FM00-LPDR                PIC  X(020).                      
PCA   ******* PROGRAMME DE RETOUR (PF3) ********************************        
   365       03 COMM-FM00-RPROG               PIC  X(005).                      
      ******************************************************************        
   370*      03 FILLER                        PIC  X(123).              01271300
   370       03 COMM-FM00-CSYSTEME            PIC  X(003).              01290210
   373       03 FILLER                        PIC  X(120).              01271300
      ******* ZONES APPLICATIVES ICS  **********************************01270000
          02 COMM-ICS.                                                  01271300
             03 COMM-GEN-ICS.                                                   
   493          05 COMM-ICS-CINTERFACE        PIC  X(005).                      
   498          05 COMM-ICS-LINTERFACE        PIC  X(025).                      
                05 COMM-ICS-PAGIN.                                              
   523             07 COMM-ICS-PAGE           PIC  9(002).                      
   525             07 COMM-ICS-PAGEMAX        PIC  9(002).                      
             03 COMM-DET-ICS.                                                   
   527          05 COMM-ICS-CRITERE           PIC  X(008) OCCURS 3.             
   551          05 COMM-ICS-CNAT              PIC  X(005).                      
   556          05 COMM-ICS-CTYP              PIC  X(005).                      
   561          05 COMM-ICS-CSEC              PIC  X(006).                      
   567          05 COMM-ICS-CSECR             PIC  X(006).                      
   573          05 COMM-ICS-CRUB              PIC  X(006).                      
   579          05 COMM-ICS-CRUBR             PIC  X(006).                      
   585          05 COMM-ICS-LCRITERE          PIC  X(005) OCCURS 3.             
   600          05 COMM-ICS-WNATSEC           PIC  X(001).                      
   601          05 COMM-ICS-WTYPSEC           PIC  X(001).                      
   602          05 COMM-ICS-WNATRUB           PIC  X(001).                      
   603          05 COMM-ICS-WTYPRUB           PIC  X(001).                      
   604          05 COMM-ICS-CRITERER          PIC  X(008) OCCURS 3.             
   628*         05 COMM-ICS-FILLER            PIC  X(171).                      
   628          05 COMM-ICS-CPTSAP            PIC  X(008).                      
   636          05 COMM-ICS-CONTREPSAP        PIC  X(008).                      
   644          05 COMM-ICS-FILLER            PIC  X(155).                      
      *      03 COMM-ICS-FILLER PIC X(248).                                     
   789    02 COMM-FM00-REDEFINES              PIC X(3000).              01272200
      ******* ZONES APPLICATIVES FM02 **********************************01270000
          02 COMM-FM02 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM02-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM02-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM02-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM02-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
      ******* ZONES APPLICATIVES FM03 **********************************01270000
          02 COMM-FM03 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM03-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM03-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM03-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM03-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
      ******* ZONES APPLICATIVES FM04/05 *******************************01270000
          02 COMM-FM04 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM04-PAGE             PIC S9(04) COMP.             01290210
      *--                                                                       
             03 COMM-FM04-PAGE             PIC S9(04) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM04-NBP              PIC S9(04) COMP.             01290210
      *--                                                                       
             03 COMM-FM04-NBP              PIC S9(04) COMP-5.                   
      *}                                                                        
             03 COMM-FM04-CDEVISE          PIC  X(03).                  01290210
             03 COMM-FM04-LDEVISE          PIC  X(30).                  01290210
             03 FILLER                     PIC  X(99).                          
             03 COMM-FM05-CDEVDEST         PIC  X(03).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM05-PAGE             PIC S9(04) COMP.                     
      *--                                                                       
             03 COMM-FM05-PAGE             PIC S9(04) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM05-NBP              PIC S9(04) COMP.                     
      *--                                                                       
             03 COMM-FM05-NBP              PIC S9(04) COMP-5.                   
      *}                                                                        
             03 COMM-FM05-CPTEG            PIC  X(06).                          
             03 COMM-FM05-SECTG            PIC  X(06).                          
             03 COMM-FM05-RUBRG            PIC  X(06).                          
             03 COMM-FM05-CPTEP            PIC  X(06).                          
             03 COMM-FM05-SECTP            PIC  X(06).                          
             03 COMM-FM05-RUBRP            PIC  X(06).                          
             03 COMM-FM05-CPT-OBLI         PIC  X(01).                          
      ******* ZONES APPLICATIVES FM06 **********************************01270000
          02 COMM-FM06 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM06-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM06-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM06-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM06-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
      ******* ZONES APPLICATIVES FM07 **********************************01270000
          02 COMM-FM07 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM07-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM07-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM07-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM07-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
      ******* ZONES APPLICATIVES FM08 **********************************01270000
          02 COMM-FM08 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM08-CETAT            PIC X(10).                   01290210
             03 COMM-FM08-LETAT            PIC X(25).                   01290210
             03 COMM-FM08-NOM-PROG         PIC X(05).                   01290210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM10-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM10-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM10-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM10-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-FM10-NDEMANDE         PIC X(2).                    01290210
             03 COMM-FM10-FONCTION         PIC X(3).                    01290210
      ******* ZONES APPLICATIVES * COMPTE / SECTION / RUBRIQUE ********         
      ******* FM21 - FM23 - FM24 - FM25 - FM26 - FM36 *****************         
          02 COMM-CSR  REDEFINES COMM-FM00-REDEFINES.                           
      *                                                                         
             03 COMM-CSR-CCODE                PIC X(06).                        
             03 COMM-CSR-CNSOCSEC             PIC X(05).                        
             03 COMM-CSR-SNAUX                PIC X(03).                        
             03 COMM-CSR-FONCTION             PIC X(03).                        
             03 COMM-CSR-POS                  PIC 9(02).                        
      *                                                                         
      *** PARAMETRAGE DES MASQUES *************************************         
             03 COMM-CSR-MASQUE               PIC X(06).                        
             03 COMM-CSR-MCOMPTE              PIC X(06).                        
             03 COMM-CSR-MSECTION             PIC X(06).                        
             03 COMM-CSR-MRUBRIQUE            PIC X(06).                        
             03 COMM-CSR-MNETAB               PIC X(03).                        
             03 COMM-CSR-MSTEAPP              PIC X(05).                        
      *                                                                         
             03 COMM-CSR-FILLER               PIC X(900).                       
             03 COMM-CSR-REDEFINES            PIC X(2000).                      
      ******* ZONES APPLICATIVES FM21 **********************************        
             03 COMM-FM21 REDEFINES COMM-CSR-REDEFINES.                         
                05 COMM-FM21-ITEM             PIC S9(3) COMP-3.                 
                05 COMM-FM21-ITEM-MAX         PIC S9(3) COMP-3.                 
      ******* ZONES APPLICATIVES FM24 **********************************        
             03 COMM-FM24 REDEFINES COMM-CSR-REDEFINES.                         
                05 COMM-FM24-DONNEES.                                           
                   07 COMM-FM24-CODE          PIC X(06).                        
                   07 COMM-FM24-LCODEC        PIC X(15).                        
                   07 COMM-FM24-LCODEL        PIC X(30).                        
                   07 COMM-FM24-CCONSOC       PIC X(06).                        
                   07 COMM-FM24-CCONSOD       PIC X(06).                        
                   07 COMM-FM24-NAUX          PIC X(03).                        
                   07 COMM-FM24-WTYPCOMPTE    PIC X(01).                        
                   07 COMM-FM24-WAUXILIARISE  PIC X(01).                        
                   07 COMM-FM24-WCOLLECTIF    PIC X(01).                        
                   07 COMM-FM24-WABONNE       PIC X(01).                        
                   07 COMM-FM24-WREGAUTO      PIC X(01).                        
                   07 COMM-FM24-WREGAUTOC     PIC X(01).                        
                   07 COMM-FM24-WCATNATCH     PIC X(01).                        
                   07 COMM-FM24-WLETTRAGE     PIC X(01).                        
                   07 COMM-FM24-WPOINTAGE     PIC X(01).                        
                   07 COMM-FM24-WEDITION      PIC X(01).                        
                   07 COMM-FM24-WCLOTURE      PIC X(01).                        
                   07 COMM-FM24-CEPURATION    PIC X(01).                        
                   07 COMM-FM24-LEPURATION    PIC X(11).                        
                   07 COMM-FM24-WINTERFACE    PIC X(01).                        
                   07 COMM-FM24-MASQUE        PIC X(06).                        
                   07 COMM-FM24-DCLOTURE      PIC X(08).                        
                   07 COMM-FM24-DMAJ          PIC X(08).                        
                   07 COMM-FM24-LIGNE         OCCURS 6.                         
                      10 COMM-FM24-CATEG      PIC X(06).                        
      ******* ZONES APPLICATIVES FM26 **********************************        
             03 COMM-FM26 REDEFINES COMM-CSR-REDEFINES.                         
                05 COMM-FM26-DONNEES.                                           
                   07 COMM-FM26-CODE          PIC X(06).                        
                   07 COMM-FM26-LCODEC        PIC X(15).                        
                   07 COMM-FM26-LCODEL        PIC X(30).                        
                   07 COMM-FM26-MASQUE        PIC X(06).                        
                   07 COMM-FM26-DCLOTURE      PIC X(08).                        
                   07 COMM-FM26-DMAJ          PIC X(08).                        
                   07 COMM-FM26-LIGNE         OCCURS 30.                        
                      10 COMM-FM26-CATEG      PIC X(06).                        
                   07 COMM-FM26-WFIR          PIC X(01).                        
                   07 COMM-FM26-NSOCSEC       PIC X(03).                        
                   07 COMM-FM26-NSOCSECLIB    PIC X(30).                        
                   07 COMM-FM26-DSECTCRE      PIC X(03).                        
                   07 COMM-FM26-DSECTCREL     PIC X(60).                        
                   07 COMM-FM26-FSECTCRE      PIC X(03).                        
                   07 COMM-FM26-FSECTCREL     PIC X(60).                        
                   07 COMM-FM26-DSECTORIG     PIC X(03).                        
                   07 COMM-FM26-DSECTORIGL    PIC X(60).                        
                   07 COMM-FM26-FSECTORIG     PIC X(03).                        
                   07 COMM-FM26-FSECTORIGL    PIC X(60).                        
                   07 COMM-FM26-DUPLSOC       PIC X(03).                        
      ******* ZONES APPLICATIVES FM32 **********************************01270000
          02 COMM-FM32 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM32-PAGE             PIC S9(3) COMP-3.            01290210
             03 COMM-FM32-PAGETOT          PIC S9(3) COMP-3.            01290210
      ******* ZONES APPLICATIVES FM40 **********************************01270000
          02 COMM-FM40 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM40-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM40-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM40-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM40-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-FM40-FONCTION         PIC X(3).                    01290210
             03 COMM-FM40-NEXER            PIC X(4).                            
             03 COMM-FM40-NANCIV           PIC X(4).                            
             03 COMM-FM40-NMOISDEB         PIC X(2).                            
             03 COMM-FM40-NANCIVFUT        PIC X(4).                            
             03 COMM-FM40-NMOISDEBFUT      PIC X(2).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM39-PAGE             PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM39-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FM39-NBP              PIC S9(4) COMP.              01290210
      *--                                                                       
             03 COMM-FM39-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
      ******* ZONES APPLICATIVES FM27 **********************************01270000
          02 COMM-FM27 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM27-PAGE             PIC S9(3) COMP-3.            01290210
             03 COMM-FM27-PAGETOT          PIC S9(3) COMP-3.            01290210
      ******* ZONES APPLICATIVES FM28 **********************************01270000
          02 COMM-FM28 REDEFINES COMM-FM00-REDEFINES.                   01272200
   811       03 COMM-FM28-PAGE             PIC S9(3) COMP-3.            01290210
             03 COMM-FM28-PAGETOT          PIC S9(3) COMP-3.            01290210
             03 COMM-FM28-CPT              PIC X(06).                   01290210
      ******* ZONES APPLICATIVES FM33 ** LISTE DES CODES JOURNAUX ******01270000
          02 COMM-FM33 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM33-PAGE             PIC S9(4) COMP-3.            01290210
             03 COMM-FM33-NBP              PIC S9(4) COMP-3.            01290210
             03 COMM-FM33-FONCTION         PIC X(3).                    01290210
             03 COMM-FM33-NJRN             PIC X(3).                    01290210
      ******* ZONES APPLICATIVES FM34 ** DETAIL DES CODES JOURNAUX *****01270000
          02 COMM-FM34 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM34-SUPPR            PIC X.                       01290210
      ******* ZONES APPLICATIVES FM35 ** LISTE DES NATURES DE PIECES ***01270000
          02 COMM-FM35 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM35-PAGE             PIC 9(2).                    01290210
             03 COMM-FM35-PAGEMAX          PIC 9(2).                    01290210
             03 COMM-FM35-NLIGNE           PIC 9(2).                    01290210
             03 COMM-FM35-NATURE           PIC X(3).                    01290210
             03 COMM-FM35-LNATURE          PIC X(20).                   01290210
             03 COMM-FM35-SENS             PIC X(1).                    01290210
             03 COMM-FM35-DMAJ             PIC X(8).                    01290210
      ******* ZONES APPLICATIVES FM41 **********************************01270000
          02 COMM-FM41 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM41-PAGE             PIC 9(2).                    01290210
             03 COMM-FM41-PAGEMAX          PIC 9(2).                    01290210
      ******* ZONES APPLICATIVES FM51 **********************************01270000
          02 COMM-FM51 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM51-CHOIX            PIC X(5).                    01290210
      ******* ZONES APPLICATIVES FM52 **********************************01270000
          02 COMM-FM52 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM52-CHOIX            PIC X(5).                    01290210
      ******* ZONES APPLICATIVES FM55 **********************************01270000
          02 COMM-FM55 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM55-CPT              PIC X(6).                    01290210
             03 COMM-FM55-CTRP             PIC X(6).                    01290210
             03 COMM-FM55-TVA              PIC X(5).                    01290210
             03 COMM-FM55-GEO              PIC X(1).                    01290210
             03 COMM-FM55-GROUPE           PIC X(1).                    01290210
      ******* ZONES APPLICATIVES FM58 **********************************01270000
          02 COMM-FM58 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM58-LIB              PIC X(01) OCCURS 5.          01290210
      ******* ZONES APPLICATIVES FM80 **********************************01270000
          02 COMM-FM80 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM80-PAGE             PIC 9(2).                    01290210
             03 COMM-FM80-PAGEMAX          PIC 9(2).                    01290210
             03 COMM-FM80-CNOMPROG         PIC X(8).                    01290210
             03 COMM-FM80-LNOMPROG         PIC X(30).                   01290210
             03 COMM-FM80-FONCTION         PIC X(3).                    01290210
      ******* ZONES APPLICATIVES FM61 **********************************01270000
      *   02 COMM-FM61 REDEFINES COMM-FM00-REDEFINES.                   01272200
      *      03 COMM-FM61-PAGE             PIC 9(2).                    01290210
      ******* ZONES APPLICATIVES FM70 **********************************01270000
          02 COMM-FM70 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM70-NOECS            PIC X(5).                    01290210
             03 COMM-FM70-FILLER           PIC X(495).                          
             03 COMM-FM70-REDEFINES        PIC X(2500).                         
      ******* ZONES APPLICATIVES FM71 **********************************01270000
             03 COMM-FM71 REDEFINES COMM-FM70-REDEFINES.                        
                04 COMM-FM71-CRIT          PIC X(5) OCCURS 3.                   
                04 COMM-FM71-LCRIT         PIC X(13) OCCURS 3.                  
                04 COMM-FM71-SENS          PIC X(1).                            
                04 COMM-FM71-DJP1          PIC X(8).                            
                04 COMM-FM71-LIBECS        PIC X(20).                           
                04 COMM-FM71-FILLER        PIC X(417).                          
                04 COMM-FM71-REDEFINES     PIC X(2000).                         
      ******* ZONES APPLICATIVES FM71 **********************************01270000
                04 COMM-FM72 REDEFINES COMM-FM71-REDEFINES.                     
                   05 COMM-FM72-CRIT       PIC X(5) OCCURS 3.                   
                   05 COMM-FM72-LIGNE   OCCURS 12.                              
                      06 COMM-FM72-COMPTE  PIC X(6) OCCURS 2.                   
                      06 COMM-FM72-SECTION PIC X(6) OCCURS 2.                   
                      06 COMM-FM72-RUBRIQUE PIC X(6) OCCURS 2.                  
                      06 COMM-FM72-DATEF   PIC X(8) OCCURS 2.                   
      ******* ZONES APPLICATIVES FM73 **********************************01270000
          02 COMM-FM73 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM73-CRIT             PIC X(3) OCCURS 2.           01290210
             03 COMM-FM73-LPCA             PIC X(3).                            
      ******* ZONES APPLICATIVES FM74 **********************************01270000
          02 COMM-FM74 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM74-PAGE             PIC 9(02).                   01290210
             03 COMM-FM74-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM74-SUP              PIC X(01).                   01290210
          02 COMM-FM75 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM75-PAGE             PIC 9(02).                   01290210
             03 COMM-FM75-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM75-SUP              PIC X(01).                   01290210
          02 COMM-FM76 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM76-PAGE             PIC 9(02).                   01290210
             03 COMM-FM76-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM76-SUP              PIC X(01).                   01290210
      ******* ZONES APPLICATIVES FM82 **********************************01270000
          02 COMM-FM82 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM82-CPT              PIC X(8).                    01290210
             03 COMM-FM82-CTRP             PIC X(8).                    01290210
             03 COMM-FM82-TVA              PIC X(5).                    01290210
             03 COMM-FM82-GEO              PIC X(1).                    01290210
             03 COMM-FM82-GROUPE           PIC X(1).                    01290210
      ******* ZONES APPLICATIVES FM83 **********************************01270000
          02 COMM-FM83 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM83-PAGE             PIC 9(02).                   01290210
             03 COMM-FM83-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM83-SUP              PIC X(01).                   01290210
          02 COMM-FM85 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM85-PAGE             PIC 9(02).                   01290210
             03 COMM-FM85-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM85-SUP              PIC X(01).                   01290210
          02 COMM-FM92 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM92-PAGE             PIC 9(02).                   01290210
             03 COMM-FM92-PAGEMAX          PIC 9(02).                   01290210
             03 COMM-FM92-SUP              PIC X(01).                   01290210
      ******* ZONES APPLICATIVES FM95 **********************************01270000
          02 COMM-FM95 REDEFINES COMM-FM00-REDEFINES.                   01272200
             03 COMM-FM95-INTCOR           PIC X(5).                    01290210
             03 COMM-FM95-TYPCOR           PIC X(5).                    01290210
             03 COMM-FM95-NATCOR           PIC X(5).                    01290210
             03 COMM-FM95-WINTER           PIC X(1).                    01290210
                                                                                
