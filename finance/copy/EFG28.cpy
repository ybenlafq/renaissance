      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG28   EFG28                                              00000020
      ***************************************************************** 00000030
       01   EFG28I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBELLEI      PIC X(20).                                 00000230
      * N� DE PAGE                                                      00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MPAGEI    PIC X.                                          00000280
      * NOMBRE DE PAGES                                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTOTPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPAGEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTOTPAGEI      PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSENSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSENSF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSENSI    PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTVALEURL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MMTVALEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MMTVALEURF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MMTVALEURI     PIC X(14).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALEURL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDVALEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALEURF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDVALEURI      PIC X(10).                                 00000450
           02 MTABLEAUI OCCURS   12 TIMES .                             00000460
             03 MDSOLDED OCCURS   2 TIMES .                             00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDSOLDEL   COMP PIC S9(4).                            00000480
      *--                                                                       
               04 MDSOLDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDSOLDEF   PIC X.                                     00000490
               04 FILLER     PIC X(4).                                  00000500
               04 MDSOLDEI   PIC X(10).                                 00000510
             03 MDT-CRD OCCURS   2 TIMES .                              00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDT-CRL    COMP PIC S9(4).                            00000530
      *--                                                                       
               04 MDT-CRL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MDT-CRF    PIC X.                                     00000540
               04 FILLER     PIC X(4).                                  00000550
               04 MDT-CRI    PIC X.                                     00000560
             03 MMTSOLDED OCCURS   2 TIMES .                            00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MMTSOLDEL  COMP PIC S9(4).                            00000580
      *--                                                                       
               04 MMTSOLDEL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MMTSOLDEF  PIC X.                                     00000590
               04 FILLER     PIC X(4).                                  00000600
               04 MMTSOLDEI  PIC X(14).                                 00000610
      * MESSAGE ERREUR                                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      * CODE TRANSACTION                                                00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCODTRAI  PIC X(4).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MZONCMDI  PIC X(15).                                      00000750
      * CICS DE TRAVAIL                                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCICSI    PIC X(5).                                       00000800
      * NETNAME                                                         00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNETNAMI  PIC X(8).                                       00000850
      * CODE TERMINAL                                                   00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(5).                                       00000900
      ***************************************************************** 00000910
      * SDF: EFG28   EFG28                                              00000920
      ***************************************************************** 00000930
       01   EFG28O REDEFINES EFG28I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
      * DATE DU JOUR                                                    00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MDATJOUA  PIC X.                                          00000980
           02 MDATJOUC  PIC X.                                          00000990
           02 MDATJOUP  PIC X.                                          00001000
           02 MDATJOUH  PIC X.                                          00001010
           02 MDATJOUV  PIC X.                                          00001020
           02 MDATJOUO  PIC X(10).                                      00001030
      * HEURE                                                           00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUP  PIC X.                                          00001080
           02 MTIMJOUH  PIC X.                                          00001090
           02 MTIMJOUV  PIC X.                                          00001100
           02 MTIMJOUO  PIC X(5).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNSOCA    PIC X.                                          00001130
           02 MNSOCC    PIC X.                                          00001140
           02 MNSOCP    PIC X.                                          00001150
           02 MNSOCH    PIC X.                                          00001160
           02 MNSOCV    PIC X.                                          00001170
           02 MNSOCO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLIBELLEA      PIC X.                                     00001200
           02 MLIBELLEC PIC X.                                          00001210
           02 MLIBELLEP PIC X.                                          00001220
           02 MLIBELLEH PIC X.                                          00001230
           02 MLIBELLEV PIC X.                                          00001240
           02 MLIBELLEO      PIC X(20).                                 00001250
      * N� DE PAGE                                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MPAGEA    PIC X.                                          00001280
           02 MPAGEC    PIC X.                                          00001290
           02 MPAGEP    PIC X.                                          00001300
           02 MPAGEH    PIC X.                                          00001310
           02 MPAGEV    PIC X.                                          00001320
           02 MPAGEO    PIC 9.                                          00001330
      * NOMBRE DE PAGES                                                 00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTOTPAGEA      PIC X.                                     00001360
           02 MTOTPAGEC PIC X.                                          00001370
           02 MTOTPAGEP PIC X.                                          00001380
           02 MTOTPAGEH PIC X.                                          00001390
           02 MTOTPAGEV PIC X.                                          00001400
           02 MTOTPAGEO      PIC 9.                                     00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MSENSA    PIC X.                                          00001430
           02 MSENSC    PIC X.                                          00001440
           02 MSENSP    PIC X.                                          00001450
           02 MSENSH    PIC X.                                          00001460
           02 MSENSV    PIC X.                                          00001470
           02 MSENSO    PIC X.                                          00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MMTVALEURA     PIC X.                                     00001500
           02 MMTVALEURC     PIC X.                                     00001510
           02 MMTVALEURP     PIC X.                                     00001520
           02 MMTVALEURH     PIC X.                                     00001530
           02 MMTVALEURV     PIC X.                                     00001540
           02 MMTVALEURO     PIC Z(10)9,99.                             00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDVALEURA      PIC X.                                     00001570
           02 MDVALEURC PIC X.                                          00001580
           02 MDVALEURP PIC X.                                          00001590
           02 MDVALEURH PIC X.                                          00001600
           02 MDVALEURV PIC X.                                          00001610
           02 MDVALEURO      PIC X(10).                                 00001620
           02 MTABLEAUO OCCURS   12 TIMES .                             00001630
             03 DFHMS1 OCCURS   2 TIMES .                               00001640
               04 FILLER     PIC X(2).                                  00001650
               04 MDSOLDEA   PIC X.                                     00001660
               04 MDSOLDEC   PIC X.                                     00001670
               04 MDSOLDEP   PIC X.                                     00001680
               04 MDSOLDEH   PIC X.                                     00001690
               04 MDSOLDEV   PIC X.                                     00001700
               04 MDSOLDEO   PIC X(10).                                 00001710
             03 DFHMS2 OCCURS   2 TIMES .                               00001720
               04 FILLER     PIC X(2).                                  00001730
               04 MDT-CRA    PIC X.                                     00001740
               04 MDT-CRC    PIC X.                                     00001750
               04 MDT-CRP    PIC X.                                     00001760
               04 MDT-CRH    PIC X.                                     00001770
               04 MDT-CRV    PIC X.                                     00001780
               04 MDT-CRO    PIC X.                                     00001790
             03 DFHMS3 OCCURS   2 TIMES .                               00001800
               04 FILLER     PIC X(2).                                  00001810
               04 MMTSOLDEA  PIC X.                                     00001820
               04 MMTSOLDEC  PIC X.                                     00001830
               04 MMTSOLDEP  PIC X.                                     00001840
               04 MMTSOLDEH  PIC X.                                     00001850
               04 MMTSOLDEV  PIC X.                                     00001860
               04 MMTSOLDEO  PIC Z(10)9,99.                             00001870
      * MESSAGE ERREUR                                                  00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLIBERRA  PIC X.                                          00001900
           02 MLIBERRC  PIC X.                                          00001910
           02 MLIBERRP  PIC X.                                          00001920
           02 MLIBERRH  PIC X.                                          00001930
           02 MLIBERRV  PIC X.                                          00001940
           02 MLIBERRO  PIC X(78).                                      00001950
      * CODE TRANSACTION                                                00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCODTRAA  PIC X.                                          00001980
           02 MCODTRAC  PIC X.                                          00001990
           02 MCODTRAP  PIC X.                                          00002000
           02 MCODTRAH  PIC X.                                          00002010
           02 MCODTRAV  PIC X.                                          00002020
           02 MCODTRAO  PIC X(4).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MZONCMDA  PIC X.                                          00002050
           02 MZONCMDC  PIC X.                                          00002060
           02 MZONCMDP  PIC X.                                          00002070
           02 MZONCMDH  PIC X.                                          00002080
           02 MZONCMDV  PIC X.                                          00002090
           02 MZONCMDO  PIC X(15).                                      00002100
      * CICS DE TRAVAIL                                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
      * NETNAME                                                         00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MNETNAMA  PIC X.                                          00002210
           02 MNETNAMC  PIC X.                                          00002220
           02 MNETNAMP  PIC X.                                          00002230
           02 MNETNAMH  PIC X.                                          00002240
           02 MNETNAMV  PIC X.                                          00002250
           02 MNETNAMO  PIC X(8).                                       00002260
      * CODE TERMINAL                                                   00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MSCREENA  PIC X.                                          00002290
           02 MSCREENC  PIC X.                                          00002300
           02 MSCREENP  PIC X.                                          00002310
           02 MSCREENH  PIC X.                                          00002320
           02 MSCREENV  PIC X.                                          00002330
           02 MSCREENO  PIC X(5).                                       00002340
                                                                                
