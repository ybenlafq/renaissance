      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA109 AU 26/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA109.                                                        
            05 NOMETAT-IRA109           PIC X(6) VALUE 'IRA109'.                
            05 RUPTURES-IRA109.                                                 
           10 IRA109-NSOCIETE           PIC X(03).                      007  003
           10 IRA109-TYPREGL            PIC X(12).                      010  012
           10 IRA109-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA109-NDEMANDE           PIC X(05).                      027  005
           10 IRA109-NREGL              PIC X(07).                      032  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA109-SEQUENCE           PIC S9(04) COMP.                039  002
      *--                                                                       
           10 IRA109-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA109.                                                   
           10 IRA109-BANQUE             PIC X(15).                      041  015
           10 IRA109-NOM                PIC X(22).                      056  022
           10 IRA109-MTREMBTOT          PIC 9(09)V9(2).                 078  011
           10 IRA109-QTE                PIC 9(01)     .                 089  001
           10 IRA109-DATEPARAM          PIC X(08).                      090  008
           10 IRA109-DEDITLET           PIC X(08).                      098  008
           10 IRA109-DREGL              PIC X(08).                      106  008
            05 FILLER                      PIC X(399).                          
                                                                                
