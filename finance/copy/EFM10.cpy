      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - LISTE ETAT MENSUEL MODELE                                 00000020
      ***************************************************************** 00000030
       01   EFM10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                         
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                                  
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                              
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNENTITEI      PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLENTITEI      PIC X(22).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETATL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETATF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLETATI   PIC X(25).                                      00000350
           02 MTABI OCCURS   13 TIMES .                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCMDL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MZONCMDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONCMDF     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MZONCMDI     PIC X.                                     00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEMANDEL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MNDEMANDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNDEMANDEF   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MNDEMANDEI   PIC X(2).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MUSERL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MUSERL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MUSERF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MUSERI  PIC X(25).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTYPAUXL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MWTYPAUXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWTYPAUXF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MWTYPAUXI    PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXMINL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MNAUXMINL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNAUXMINF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MNAUXMINI    PIC X(3).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONLIBL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MZONLIBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONLIBF     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MZONLIBI     PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXMAXL    COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MNAUXMAXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNAUXMAXF    PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MNAUXMAXI    PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATIONL  COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MDCREATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDCREATIONF  PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MDCREATIONI  PIC X(10).                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMAJL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MDMAJL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMAJF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MDMAJI  PIC X(10).                                      00000720
      * ZONE CMD AIDA                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(79).                                      00000770
      * CODE TRANSACTION                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      * CICS DE TRAVAIL                                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      * NETNAME                                                         00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      * CODE TERMINAL                                                   00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(5).                                       00000970
      ***************************************************************** 00000980
      * GCT - LISTE ETAT MENSUEL MODELE                                 00000990
      ***************************************************************** 00001000
       01   EFM10O REDEFINES EFM10I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
      * DATE DU JOUR                                                    00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                                  
           02 MDATJOUC  PIC X.                                                  
           02 MDATJOUP  PIC X.                                                  
           02 MDATJOUH  PIC X.                                                  
           02 MDATJOUV  PIC X.                                                  
           02 MDATJOUO  PIC X(10).                                              
      * HEURE                                                           00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEA    PIC X.                                          00001200
           02 MPAGEC    PIC X.                                          00001210
           02 MPAGEP    PIC X.                                          00001220
           02 MPAGEH    PIC X.                                          00001230
           02 MPAGEV    PIC X.                                          00001240
           02 MPAGEO    PIC Z9.                                         00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNBPA     PIC X.                                          00001270
           02 MNBPC     PIC X.                                          00001280
           02 MNBPP     PIC X.                                          00001290
           02 MNBPH     PIC X.                                          00001300
           02 MNBPV     PIC X.                                          00001310
           02 MNBPO     PIC Z9.                                         00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNENTITEA      PIC X.                                     00001340
           02 MNENTITEC PIC X.                                          00001350
           02 MNENTITEP PIC X.                                          00001360
           02 MNENTITEH PIC X.                                          00001370
           02 MNENTITEV PIC X.                                          00001380
           02 MNENTITEO      PIC X(5).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLENTITEA      PIC X.                                     00001410
           02 MLENTITEC PIC X.                                          00001420
           02 MLENTITEP PIC X.                                          00001430
           02 MLENTITEH PIC X.                                          00001440
           02 MLENTITEV PIC X.                                          00001450
           02 MLENTITEO      PIC X(22).                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLETATA   PIC X.                                          00001480
           02 MLETATC   PIC X.                                          00001490
           02 MLETATP   PIC X.                                          00001500
           02 MLETATH   PIC X.                                          00001510
           02 MLETATV   PIC X.                                          00001520
           02 MLETATO   PIC X(25).                                      00001530
           02 MTABO OCCURS   13 TIMES .                                 00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MZONCMDA     PIC X.                                     00001560
             03 MZONCMDC     PIC X.                                     00001570
             03 MZONCMDP     PIC X.                                     00001580
             03 MZONCMDH     PIC X.                                     00001590
             03 MZONCMDV     PIC X.                                     00001600
             03 MZONCMDO     PIC X.                                     00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNDEMANDEA   PIC X.                                     00001630
             03 MNDEMANDEC   PIC X.                                     00001640
             03 MNDEMANDEP   PIC X.                                     00001650
             03 MNDEMANDEH   PIC X.                                     00001660
             03 MNDEMANDEV   PIC X.                                     00001670
             03 MNDEMANDEO   PIC X(2).                                  00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MUSERA  PIC X.                                          00001700
             03 MUSERC  PIC X.                                          00001710
             03 MUSERP  PIC X.                                          00001720
             03 MUSERH  PIC X.                                          00001730
             03 MUSERV  PIC X.                                          00001740
             03 MUSERO  PIC X(25).                                      00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MWTYPAUXA    PIC X.                                     00001770
             03 MWTYPAUXC    PIC X.                                     00001780
             03 MWTYPAUXP    PIC X.                                     00001790
             03 MWTYPAUXH    PIC X.                                     00001800
             03 MWTYPAUXV    PIC X.                                     00001810
             03 MWTYPAUXO    PIC X.                                     00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MNAUXMINA    PIC X.                                     00001840
             03 MNAUXMINC    PIC X.                                     00001850
             03 MNAUXMINP    PIC X.                                     00001860
             03 MNAUXMINH    PIC X.                                     00001870
             03 MNAUXMINV    PIC X.                                     00001880
             03 MNAUXMINO    PIC X(3).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MZONLIBA     PIC X.                                     00001910
             03 MZONLIBC     PIC X.                                     00001920
             03 MZONLIBP     PIC X.                                     00001930
             03 MZONLIBH     PIC X.                                     00001940
             03 MZONLIBV     PIC X.                                     00001950
             03 MZONLIBO     PIC X.                                     00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MNAUXMAXA    PIC X.                                     00001980
             03 MNAUXMAXC    PIC X.                                     00001990
             03 MNAUXMAXP    PIC X.                                     00002000
             03 MNAUXMAXH    PIC X.                                     00002010
             03 MNAUXMAXV    PIC X.                                     00002020
             03 MNAUXMAXO    PIC X(3).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MDCREATIONA  PIC X.                                     00002050
             03 MDCREATIONC  PIC X.                                     00002060
             03 MDCREATIONP  PIC X.                                     00002070
             03 MDCREATIONH  PIC X.                                     00002080
             03 MDCREATIONV  PIC X.                                     00002090
             03 MDCREATIONO  PIC X(10).                                 00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MDMAJA  PIC X.                                          00002120
             03 MDMAJC  PIC X.                                          00002130
             03 MDMAJP  PIC X.                                          00002140
             03 MDMAJH  PIC X.                                          00002150
             03 MDMAJV  PIC X.                                          00002160
             03 MDMAJO  PIC X(10).                                      00002170
      * ZONE CMD AIDA                                                   00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBERRA  PIC X.                                          00002200
           02 MLIBERRC  PIC X.                                          00002210
           02 MLIBERRP  PIC X.                                          00002220
           02 MLIBERRH  PIC X.                                          00002230
           02 MLIBERRV  PIC X.                                          00002240
           02 MLIBERRO  PIC X(79).                                      00002250
      * CODE TRANSACTION                                                00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
      * CICS DE TRAVAIL                                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCICSA    PIC X.                                          00002360
           02 MCICSC    PIC X.                                          00002370
           02 MCICSP    PIC X.                                          00002380
           02 MCICSH    PIC X.                                          00002390
           02 MCICSV    PIC X.                                          00002400
           02 MCICSO    PIC X(5).                                       00002410
      * NETNAME                                                         00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
      * CODE TERMINAL                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MSCREENA  PIC X.                                          00002520
           02 MSCREENC  PIC X.                                          00002530
           02 MSCREENP  PIC X.                                          00002540
           02 MSCREENH  PIC X.                                          00002550
           02 MSCREENV  PIC X.                                          00002560
           02 MSCREENO  PIC X(5).                                       00002570
                                                                                
