      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE IF1000                       
      ******************************************************************        
      *                                                                         
       CLEF-IF1000             SECTION.                                         
      *                                                                         
           MOVE 'RVIF1000          '       TO   TABLE-NAME.                     
           MOVE 'IF1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-IF1000. EXIT.                                                   
                EJECT                                                           
                                                                                
