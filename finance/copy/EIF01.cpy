      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE SOCIETES COMPTABLES                                 00000020
      ***************************************************************** 00000030
       01   EIF01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCOMPTL    COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNSOCCOMPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCCOMPTF    PIC X.                                     00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MNSOCCOMPTI    PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MNETABI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCOMPTL    COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLSOCCOMPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLSOCCOMPTF    PIC X.                                     00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MLSOCCOMPTI    PIC X(20).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCTAL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MCTYPCTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPCTAF      PIC X.                                     00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MCTYPCTAI      PIC X(3).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODIMPL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MCMODIMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMODIMPF      PIC X.                                     00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MCMODIMPI      PIC X.                                     00000430
           02 FILLER  OCCURS   14 TIMES .                               00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORGANL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCORGANL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCORGANF     PIC X.                                     00000460
             03 FILLER  PIC X(2).                                       00000470
             03 MCORGANI     PIC X(3).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLORGANL     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MLORGANL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLORGANF     PIC X.                                     00000500
             03 FILLER  PIC X(2).                                       00000510
             03 MLORGANI     PIC X(20).                                 00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTIFL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MWACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTIFF     PIC X.                                     00000540
             03 FILLER  PIC X(2).                                       00000550
             03 MWACTIFI     PIC X.                                     00000560
      * MESSAGE ERREUR                                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MLIBERRI  PIC X(79).                                      00000610
      * CODE TRANSACTION                                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      * ZONE CMD AIDA                                                   00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MZONCMDI  PIC X(15).                                      00000710
      * CICS DE TRAVAIL                                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MCICSI    PIC X(5).                                       00000760
      * NETNAME                                                         00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MNETNAMI  PIC X(8).                                       00000810
      * CODE TERMINAL                                                   00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MSCREENI  PIC X(5).                                       00000860
      ***************************************************************** 00000870
      * PARAMETRAGE SOCIETES COMPTABLES                                 00000880
      ***************************************************************** 00000890
       01   EIF01O REDEFINES EIF01I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
      * DATE DU JOUR                                                    00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MDATJOUA  PIC X.                                          00000940
           02 MDATJOUC  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUO  PIC X(10).                                      00000970
      * HEURE                                                           00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUH  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MNPAGEA   PIC X.                                          00001050
           02 MNPAGEC   PIC X.                                          00001060
           02 MNPAGEH   PIC X.                                          00001070
           02 MNPAGEO   PIC ZZ.                                         00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNBPAGESA      PIC X.                                     00001100
           02 MNBPAGESC PIC X.                                          00001110
           02 MNBPAGESH PIC X.                                          00001120
           02 MNBPAGESO      PIC ZZ.                                    00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNSOCCOMPTA    PIC X.                                     00001150
           02 MNSOCCOMPTC    PIC X.                                     00001160
           02 MNSOCCOMPTH    PIC X.                                     00001170
           02 MNSOCCOMPTO    PIC X(3).                                  00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MNETABA   PIC X.                                          00001200
           02 MNETABC   PIC X.                                          00001210
           02 MNETABH   PIC X.                                          00001220
           02 MNETABO   PIC X(3).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MLSOCCOMPTA    PIC X.                                     00001250
           02 MLSOCCOMPTC    PIC X.                                     00001260
           02 MLSOCCOMPTH    PIC X.                                     00001270
           02 MLSOCCOMPTO    PIC X(20).                                 00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MCTYPCTAA      PIC X.                                     00001300
           02 MCTYPCTAC PIC X.                                          00001310
           02 MCTYPCTAH PIC X.                                          00001320
           02 MCTYPCTAO      PIC X(3).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCMODIMPA      PIC X.                                     00001350
           02 MCMODIMPC PIC X.                                          00001360
           02 MCMODIMPH PIC X.                                          00001370
           02 MCMODIMPO      PIC X.                                     00001380
           02 FILLER  OCCURS   14 TIMES .                               00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MCORGANA     PIC X.                                     00001410
             03 MCORGANC     PIC X.                                     00001420
             03 MCORGANH     PIC X.                                     00001430
             03 MCORGANO     PIC X(3).                                  00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MLORGANA     PIC X.                                     00001460
             03 MLORGANC     PIC X.                                     00001470
             03 MLORGANH     PIC X.                                     00001480
             03 MLORGANO     PIC X(20).                                 00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MWACTIFA     PIC X.                                     00001510
             03 MWACTIFC     PIC X.                                     00001520
             03 MWACTIFH     PIC X.                                     00001530
             03 MWACTIFO     PIC X.                                     00001540
      * MESSAGE ERREUR                                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRH  PIC X.                                          00001590
           02 MLIBERRO  PIC X(79).                                      00001600
      * CODE TRANSACTION                                                00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCODTRAA  PIC X.                                          00001630
           02 MCODTRAC  PIC X.                                          00001640
           02 MCODTRAH  PIC X.                                          00001650
           02 MCODTRAO  PIC X(4).                                       00001660
      * ZONE CMD AIDA                                                   00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MZONCMDA  PIC X.                                          00001690
           02 MZONCMDC  PIC X.                                          00001700
           02 MZONCMDH  PIC X.                                          00001710
           02 MZONCMDO  PIC X(15).                                      00001720
      * CICS DE TRAVAIL                                                 00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCICSA    PIC X.                                          00001750
           02 MCICSC    PIC X.                                          00001760
           02 MCICSH    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
      * NETNAME                                                         00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNETNAMA  PIC X.                                          00001810
           02 MNETNAMC  PIC X.                                          00001820
           02 MNETNAMH  PIC X.                                          00001830
           02 MNETNAMO  PIC X(8).                                       00001840
      * CODE TERMINAL                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSCREENA  PIC X.                                          00001870
           02 MSCREENC  PIC X.                                          00001880
           02 MSCREENH  PIC X.                                          00001890
           02 MSCREENO  PIC X(5).                                       00001900
                                                                                
