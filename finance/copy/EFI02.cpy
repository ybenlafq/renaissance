      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFI02   EFI02                                              00000020
      ***************************************************************** 00000030
       01   EFI02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLFONCI   PIC X(13).                                      00000210
           02 MPCFI OCCURS   10 TIMES .                                 00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNENTCDEI    PIC X(5).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCFAMI  PIC X(5).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCTYPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPEF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCTYPEI      PIC X(4).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDEFFETI     PIC X(10).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMVTL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCMVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMVTF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCMVTI  PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MZONCMDI  PIC X(15).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLIBERRI  PIC X(58).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSCREENI  PIC X(5).                                       00000660
      ***************************************************************** 00000670
      * SDF: EFI02   EFI02                                              00000680
      ***************************************************************** 00000690
       01   EFI02O REDEFINES EFI02I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MPAGEA    PIC X.                                          00000870
           02 MPAGEC    PIC X.                                          00000880
           02 MPAGEP    PIC X.                                          00000890
           02 MPAGEH    PIC X.                                          00000900
           02 MPAGEV    PIC X.                                          00000910
           02 MPAGEO    PIC X(3).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MLFONCA   PIC X.                                          00000940
           02 MLFONCC   PIC X.                                          00000950
           02 MLFONCP   PIC X.                                          00000960
           02 MLFONCH   PIC X.                                          00000970
           02 MLFONCV   PIC X.                                          00000980
           02 MLFONCO   PIC X(13).                                      00000990
           02 MPCFO OCCURS   10 TIMES .                                 00001000
             03 FILLER       PIC X(2).                                  00001010
             03 MNENTCDEA    PIC X.                                     00001020
             03 MNENTCDEC    PIC X.                                     00001030
             03 MNENTCDEP    PIC X.                                     00001040
             03 MNENTCDEH    PIC X.                                     00001050
             03 MNENTCDEV    PIC X.                                     00001060
             03 MNENTCDEO    PIC X(5).                                  00001070
             03 FILLER       PIC X(2).                                  00001080
             03 MCFAMA  PIC X.                                          00001090
             03 MCFAMC  PIC X.                                          00001100
             03 MCFAMP  PIC X.                                          00001110
             03 MCFAMH  PIC X.                                          00001120
             03 MCFAMV  PIC X.                                          00001130
             03 MCFAMO  PIC X(5).                                       00001140
             03 FILLER       PIC X(2).                                  00001150
             03 MCTYPEA      PIC X.                                     00001160
             03 MCTYPEC PIC X.                                          00001170
             03 MCTYPEP PIC X.                                          00001180
             03 MCTYPEH PIC X.                                          00001190
             03 MCTYPEV PIC X.                                          00001200
             03 MCTYPEO      PIC X(4).                                  00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MDEFFETA     PIC X.                                     00001230
             03 MDEFFETC     PIC X.                                     00001240
             03 MDEFFETP     PIC X.                                     00001250
             03 MDEFFETH     PIC X.                                     00001260
             03 MDEFFETV     PIC X.                                     00001270
             03 MDEFFETO     PIC X(10).                                 00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MCMVTA  PIC X.                                          00001300
             03 MCMVTC  PIC X.                                          00001310
             03 MCMVTP  PIC X.                                          00001320
             03 MCMVTH  PIC X.                                          00001330
             03 MCMVTV  PIC X.                                          00001340
             03 MCMVTO  PIC X.                                          00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MZONCMDA  PIC X.                                          00001370
           02 MZONCMDC  PIC X.                                          00001380
           02 MZONCMDP  PIC X.                                          00001390
           02 MZONCMDH  PIC X.                                          00001400
           02 MZONCMDV  PIC X.                                          00001410
           02 MZONCMDO  PIC X(15).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLIBERRA  PIC X.                                          00001440
           02 MLIBERRC  PIC X.                                          00001450
           02 MLIBERRP  PIC X.                                          00001460
           02 MLIBERRH  PIC X.                                          00001470
           02 MLIBERRV  PIC X.                                          00001480
           02 MLIBERRO  PIC X(58).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCODTRAA  PIC X.                                          00001510
           02 MCODTRAC  PIC X.                                          00001520
           02 MCODTRAP  PIC X.                                          00001530
           02 MCODTRAH  PIC X.                                          00001540
           02 MCODTRAV  PIC X.                                          00001550
           02 MCODTRAO  PIC X(4).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCICSA    PIC X.                                          00001580
           02 MCICSC    PIC X.                                          00001590
           02 MCICSP    PIC X.                                          00001600
           02 MCICSH    PIC X.                                          00001610
           02 MCICSV    PIC X.                                          00001620
           02 MCICSO    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNETNAMA  PIC X.                                          00001650
           02 MNETNAMC  PIC X.                                          00001660
           02 MNETNAMP  PIC X.                                          00001670
           02 MNETNAMH  PIC X.                                          00001680
           02 MNETNAMV  PIC X.                                          00001690
           02 MNETNAMO  PIC X(8).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSCREENA  PIC X.                                          00001720
           02 MSCREENC  PIC X.                                          00001730
           02 MSCREENP  PIC X.                                          00001740
           02 MSCREENH  PIC X.                                          00001750
           02 MSCREENV  PIC X.                                          00001760
           02 MSCREENO  PIC X(5).                                       00001770
                                                                                
