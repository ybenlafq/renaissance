      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - DATE DE CLOTURE MENSUELLE                                 00000020
      ***************************************************************** 00000030
       01   EFM81I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNOMPROGL     COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MCNOMPROGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCNOMPROGF     PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCNOMPROGI     PIC X(8).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMPROGL     COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLNOMPROGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNOMPROGF     PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLNOMPROGI     PIC X(30).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCTYPEI   PIC X.                                          00000270
           02 MJOURD OCCURS   7 TIMES .                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJOURL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MJOURF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MJOURI  PIC X.                                          00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTRT1L   COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MWTRT1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWTRT1F   PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MWTRT1I   PIC X.                                          00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTRT2L   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MWTRT2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWTRT2F   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MWTRT2I   PIC X.                                          00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJRNL    COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MNJRNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNJRNF    PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MNJRNI    PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MNATUREI  PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCETATI   PIC X(8).                                       00000520
      * ZONE CMD AIDA                                                   00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(79).                                      00000570
      * CODE TRANSACTION                                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      * CICS DE TRAVAIL                                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      * NETNAME                                                         00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MNETNAMI  PIC X(8).                                       00000720
      * CODE TERMINAL                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCREENI  PIC X(4).                                       00000770
      ***************************************************************** 00000780
      * GCT - DATE DE CLOTURE MENSUELLE                                 00000790
      ***************************************************************** 00000800
       01   EFM81O REDEFINES EFM81I.                                    00000810
           02 FILLER    PIC X(12).                                      00000820
      * DATE DU JOUR                                                    00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
      * HEURE                                                           00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MTIMJOUA  PIC X.                                          00000930
           02 MTIMJOUC  PIC X.                                          00000940
           02 MTIMJOUP  PIC X.                                          00000950
           02 MTIMJOUH  PIC X.                                          00000960
           02 MTIMJOUV  PIC X.                                          00000970
           02 MTIMJOUO  PIC X(5).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MCNOMPROGA     PIC X.                                     00001000
           02 MCNOMPROGC     PIC X.                                     00001010
           02 MCNOMPROGP     PIC X.                                     00001020
           02 MCNOMPROGH     PIC X.                                     00001030
           02 MCNOMPROGV     PIC X.                                     00001040
           02 MCNOMPROGO     PIC X(8).                                  00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MLNOMPROGA     PIC X.                                     00001070
           02 MLNOMPROGC     PIC X.                                     00001080
           02 MLNOMPROGP     PIC X.                                     00001090
           02 MLNOMPROGH     PIC X.                                     00001100
           02 MLNOMPROGV     PIC X.                                     00001110
           02 MLNOMPROGO     PIC X(30).                                 00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCTYPEA   PIC X.                                          00001140
           02 MCTYPEC   PIC X.                                          00001150
           02 MCTYPEP   PIC X.                                          00001160
           02 MCTYPEH   PIC X.                                          00001170
           02 MCTYPEV   PIC X.                                          00001180
           02 MCTYPEO   PIC X.                                          00001190
           02 DFHMS1 OCCURS   7 TIMES .                                 00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MJOURA  PIC X.                                          00001220
             03 MJOURC  PIC X.                                          00001230
             03 MJOURP  PIC X.                                          00001240
             03 MJOURH  PIC X.                                          00001250
             03 MJOURV  PIC X.                                          00001260
             03 MJOURO  PIC X.                                          00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MWTRT1A   PIC X.                                          00001290
           02 MWTRT1C   PIC X.                                          00001300
           02 MWTRT1P   PIC X.                                          00001310
           02 MWTRT1H   PIC X.                                          00001320
           02 MWTRT1V   PIC X.                                          00001330
           02 MWTRT1O   PIC X.                                          00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MWTRT2A   PIC X.                                          00001360
           02 MWTRT2C   PIC X.                                          00001370
           02 MWTRT2P   PIC X.                                          00001380
           02 MWTRT2H   PIC X.                                          00001390
           02 MWTRT2V   PIC X.                                          00001400
           02 MWTRT2O   PIC X.                                          00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNJRNA    PIC X.                                          00001430
           02 MNJRNC    PIC X.                                          00001440
           02 MNJRNP    PIC X.                                          00001450
           02 MNJRNH    PIC X.                                          00001460
           02 MNJRNV    PIC X.                                          00001470
           02 MNJRNO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNATUREA  PIC X.                                          00001500
           02 MNATUREC  PIC X.                                          00001510
           02 MNATUREP  PIC X.                                          00001520
           02 MNATUREH  PIC X.                                          00001530
           02 MNATUREV  PIC X.                                          00001540
           02 MNATUREO  PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCETATA   PIC X.                                          00001570
           02 MCETATC   PIC X.                                          00001580
           02 MCETATP   PIC X.                                          00001590
           02 MCETATH   PIC X.                                          00001600
           02 MCETATV   PIC X.                                          00001610
           02 MCETATO   PIC X(8).                                       00001620
      * ZONE CMD AIDA                                                   00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBERRA  PIC X.                                          00001650
           02 MLIBERRC  PIC X.                                          00001660
           02 MLIBERRP  PIC X.                                          00001670
           02 MLIBERRH  PIC X.                                          00001680
           02 MLIBERRV  PIC X.                                          00001690
           02 MLIBERRO  PIC X(79).                                      00001700
      * CODE TRANSACTION                                                00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
      * CICS DE TRAVAIL                                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCICSA    PIC X.                                          00001810
           02 MCICSC    PIC X.                                          00001820
           02 MCICSP    PIC X.                                          00001830
           02 MCICSH    PIC X.                                          00001840
           02 MCICSV    PIC X.                                          00001850
           02 MCICSO    PIC X(5).                                       00001860
      * NETNAME                                                         00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
      * CODE TERMINAL                                                   00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MSCREENA  PIC X.                                          00001970
           02 MSCREENC  PIC X.                                          00001980
           02 MSCREENP  PIC X.                                          00001990
           02 MSCREENH  PIC X.                                          00002000
           02 MSCREENV  PIC X.                                          00002010
           02 MSCREENO  PIC X(4).                                       00002020
                                                                                
