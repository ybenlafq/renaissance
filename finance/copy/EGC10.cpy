      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu COMMISSIONS SUR SERVICE                                    00000020
      ***************************************************************** 00000030
       01   EGC10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATTRTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATTRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATTRTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATTRTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPRESTATIONL  COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCPRESTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCPRESTATIONF  PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPRESTATIONI  PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPPRESTL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPPRESTF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPPRESTI    PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MENTETEI  PIC X(78).                                      00000410
           02 MTABLISTI OCCURS   13 TIMES .                             00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MSELI   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPCNDL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCTYPCNDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTYPCNDF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCTYPCNDI    PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRESTATIONL     COMP PIC S9(4).                       00000510
      *--                                                                       
             03 MLPRESTATIONL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MLPRESTATIONF     PIC X.                                00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLPRESTATIONI     PIC X(20).                            00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBL    COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MNBL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MNBF    PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNBI    PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMONTANTL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MTMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTMONTANTF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTMONTANTI   PIC X(9).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * menu COMMISSIONS SUR SERVICE                                    00000880
      ***************************************************************** 00000890
       01   EGC10O REDEFINES EGC10I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEMAXA      PIC X.                                     00001140
           02 MPAGEMAXC PIC X.                                          00001150
           02 MPAGEMAXP PIC X.                                          00001160
           02 MPAGEMAXH PIC X.                                          00001170
           02 MPAGEMAXV PIC X.                                          00001180
           02 MPAGEMAXO      PIC X(3).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATTRTA  PIC X.                                          00001210
           02 MDATTRTC  PIC X.                                          00001220
           02 MDATTRTP  PIC X.                                          00001230
           02 MDATTRTH  PIC X.                                          00001240
           02 MDATTRTV  PIC X.                                          00001250
           02 MDATTRTO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCPRESTATIONA  PIC X.                                     00001280
           02 MCPRESTATIONC  PIC X.                                     00001290
           02 MCPRESTATIONP  PIC X.                                     00001300
           02 MCPRESTATIONH  PIC X.                                     00001310
           02 MCPRESTATIONV  PIC X.                                     00001320
           02 MCPRESTATIONO  PIC X(5).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCTYPPRESTA    PIC X.                                     00001350
           02 MCTYPPRESTC    PIC X.                                     00001360
           02 MCTYPPRESTP    PIC X.                                     00001370
           02 MCTYPPRESTH    PIC X.                                     00001380
           02 MCTYPPRESTV    PIC X.                                     00001390
           02 MCTYPPRESTO    PIC X(5).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCMARQA   PIC X.                                          00001420
           02 MCMARQC   PIC X.                                          00001430
           02 MCMARQP   PIC X.                                          00001440
           02 MCMARQH   PIC X.                                          00001450
           02 MCMARQV   PIC X.                                          00001460
           02 MCMARQO   PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MENTETEA  PIC X.                                          00001490
           02 MENTETEC  PIC X.                                          00001500
           02 MENTETEP  PIC X.                                          00001510
           02 MENTETEH  PIC X.                                          00001520
           02 MENTETEV  PIC X.                                          00001530
           02 MENTETEO  PIC X(78).                                      00001540
           02 MTABLISTO OCCURS   13 TIMES .                             00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MSELA   PIC X.                                          00001570
             03 MSELC   PIC X.                                          00001580
             03 MSELP   PIC X.                                          00001590
             03 MSELH   PIC X.                                          00001600
             03 MSELV   PIC X.                                          00001610
             03 MSELO   PIC X.                                          00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MCTYPCNDA    PIC X.                                     00001640
             03 MCTYPCNDC    PIC X.                                     00001650
             03 MCTYPCNDP    PIC X.                                     00001660
             03 MCTYPCNDH    PIC X.                                     00001670
             03 MCTYPCNDV    PIC X.                                     00001680
             03 MCTYPCNDO    PIC X(5).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MLPRESTATIONA     PIC X.                                00001710
             03 MLPRESTATIONC     PIC X.                                00001720
             03 MLPRESTATIONP     PIC X.                                00001730
             03 MLPRESTATIONH     PIC X.                                00001740
             03 MLPRESTATIONV     PIC X.                                00001750
             03 MLPRESTATIONO     PIC X(20).                            00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNBA    PIC X.                                          00001780
             03 MNBC    PIC X.                                          00001790
             03 MNBP    PIC X.                                          00001800
             03 MNBH    PIC X.                                          00001810
             03 MNBV    PIC X.                                          00001820
             03 MNBO    PIC X(3).                                       00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MTMONTANTA   PIC X.                                     00001850
             03 MTMONTANTC   PIC X.                                     00001860
             03 MTMONTANTP   PIC X.                                     00001870
             03 MTMONTANTH   PIC X.                                     00001880
             03 MTMONTANTV   PIC X.                                     00001890
             03 MTMONTANTO   PIC X(9).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
