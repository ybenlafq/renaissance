      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PSEPX GESTION AUTOMATIQUE TARIF PSE    *        
      *----------------------------------------------------------------*        
       01  RVPSEPX.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  PSEPX-CTABLEG2    PIC X(15).                                     
           05  PSEPX-CTABLEG2-REDEF REDEFINES PSEPX-CTABLEG2.                   
               10  PSEPX-CFAM            PIC X(05).                             
               10  PSEPX-CGCPLT          PIC X(05).                             
               10  PSEPX-CTARIF          PIC X(05).                             
           05  PSEPX-WTABLEG     PIC X(80).                                     
           05  PSEPX-WTABLEG-REDEF  REDEFINES PSEPX-WTABLEG.                    
               10  PSEPX-WACTIF          PIC X(01).                             
               10  PSEPX-PXMIN           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  PSEPX-PXMIN-N        REDEFINES PSEPX-PXMIN                   
                                         PIC 9(05).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  PSEPX-PXMAX           PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  PSEPX-PXMAX-N        REDEFINES PSEPX-PXMAX                   
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPSEPX-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEPX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSEPX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEPX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSEPX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
