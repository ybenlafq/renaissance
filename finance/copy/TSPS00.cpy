      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *  PROJET : PSE                                                           
      *  TS POUR APPLICATION PS00                                               
      ****************************************************************          
       01  STRUCT-TS.                                                           
           05 TS-LIGNE         OCCURS 16.                                       
              10  TS-CGCPLT    PIC  X(5).                                       
              10  TS-NGCPLT    PIC  X(8).                                       
              10  TS-NLIEU     PIC  X(3).                                       
              10  TS-NVENTE    PIC  X(7).                                       
              10  TS-DCOMPTA   PIC  X(8).                                       
              10  TS-NCODIC    PIC  X(7).                                       
              10  TS-MTGCPLT   PIC  9(5)V99.                                    
              10  TS-DANNUL    PIC  X(8).                                       
              10  TS-MTREMB    PIC  9(5)V99.                                    
              10  TS-DREMB     PIC  X(8).                                       
              10  TS-MTRACHAT  PIC  9(5)V99.                                    
              10  TS-DRACHAT   PIC  X(8).                                       
              10  TS-NOMCLI    PIC  X(15).                                      
              10  TS-CINSEE    PIC  X(05).                                      
              10  TS-DTRAITR   PIC  X(8).                                       
              10  TS-LREF      PIC  X(20).                                      
              10  TS-CFAM      PIC  X(5).                                       
              10  TS-CMARQ     PIC  X(5).                                       
                                                                                
