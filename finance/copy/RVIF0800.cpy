      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVIF0800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIF0800                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF0800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF0800.                                                            
      *}                                                                        
           02  IF08-CMODPAI                                                     
               PIC X(0003).                                                     
           02  IF08-CTYPCTA                                                     
               PIC X(0003).                                                     
           02  IF08-CTYPMONT                                                    
               PIC X(0005).                                                     
           02  IF08-NORDRE                                                      
               PIC X(0002).                                                     
           02  IF08-CLIBFIXE                                                    
               PIC X(0006).                                                     
           02  IF08-CMOTCLE                                                     
               PIC X(0010).                                                     
           02  IF08-CMASQUE                                                     
               PIC X(0005).                                                     
           02  IF08-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIF0800                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF0800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF0800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CMODPAI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CMODPAI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CTYPCTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CTYPCTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CTYPMONT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CTYPMONT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CLIBFIXE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CLIBFIXE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CMOTCLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CMOTCLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF08-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
