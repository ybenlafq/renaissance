      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMRA00.                                              00000101
      *                                                                 00000200
      **************************************************************    00000300
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000400
      **************************************************************    00000500
      *                                                                 00000600
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000700
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000800
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000900
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00001000
      *                                                                 00001100
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00001200
      * COMPRENANT :                                                    00001300
      * 1 - LES ZONES RESERVEES A AIDA                                  00001400
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00001500
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00001600
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00001700
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00001800
      *                                                                 00001900
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00002000
      * PAR AIDA                                                        00002100
      *                                                                 00002200
      *-------------------------------------------------------------    00002300
      *                                                                 00002400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RA00-LONG-COMMAREA PIC S9(5) COMP VALUE +4096.           00002501
      *--                                                                       
       01  COM-RA00-LONG-COMMAREA PIC S9(5) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00002600
       01  Z-COMMAREA.                                                  00002700
      *                                                                 00002800
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00002900
           02 FILLER-COM-AIDA      PIC X(100).                          00003000
      *                                                                 00003100
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00003200
           02 COMM-CICS-APPLID     PIC X(8).                            00003300
           02 COMM-CICS-NETNAM     PIC X(8).                            00003400
           02 COMM-CICS-TRANSA     PIC X(4).                            00003500
      *                                                                 00003600
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00003700
           02 COMM-DATE-SIECLE     PIC XX.                              00003800
           02 COMM-DATE-ANNEE      PIC XX.                              00003900
           02 COMM-DATE-MOIS       PIC XX.                              00004000
           02 COMM-DATE-JOUR       PIC 99.                              00004100
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00004200
           02 COMM-DATE-QNTA       PIC 999.                             00004300
           02 COMM-DATE-QNT0       PIC 99999.                           00004400
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00004500
           02 COMM-DATE-BISX       PIC 9.                               00004600
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00004700
           02 COMM-DATE-JSM        PIC 9.                               00004800
      *   LIBELLES DU JOUR COURT - LONG                                 00004900
           02 COMM-DATE-JSM-LC     PIC XXX.                             00005000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00005100
      *   LIBELLES DU MOIS COURT - LONG                                 00005200
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00005300
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00005400
      *   DIFFERENTES FORMES DE DATE                                    00005500
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00005600
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00005700
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00005800
           02 COMM-DATE-JJMMAA     PIC X(6).                            00005900
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00006000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00006100
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00006200
           02 COMM-DATE-WEEK.                                           00006300
              05 COMM-DATE-SEMSS   PIC 99.                              00006400
              05 COMM-DATE-SEMAA   PIC 99.                              00006500
              05 COMM-DATE-SEMNU   PIC 99.                              00006600
      *                                                                 00006700
           02 COMM-DATE-FILLER     PIC X(08).                           00006800
      *                                                                 00006900
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------152   00007000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00007100
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR       OCCURS 150 PIC X(01).                00007200
      *                                                                 00007300
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00007400
           02 FILLER   PIC X(22) VALUE ' *** COMMAREA RA00 ***'.        00007501
           02 COMM-RA00-APPLI.                                          00007601
      *                                                                 00007700
      *--------INCHANGEEES                                              00007800
              03 COMM-RA00-NSOC           PIC X(3).                     00008001
              03 FILLER                   PIC X(2).                     00008001
              03 FILLER                   PIC X(308).                   00008200
      *                                                                 00008300
      *--------INITIALISER A CHAQUE OPTION                              00008400
              03 COMM-RA00-ZONES.                                       00008501
                 05 COMM-RA00-NOPT           PIC X.                     00009101
                 05 COMM-RA00-CODRET         PIC X.                     00009101
                    88  COMM-RA00-PAS-MESS   VALUE ' '.                 00009201
                    88  COMM-RA00-MESS-RET   VALUE '1'.                 00009301
                 05 COMM-RA00-MESSAGE        PIC X(50).                 00009401
                 05 COMM-RA00-CODIMP         PIC X(04).                 00009500
                 05 COMM-RA00-DECHEAN        PIC X(08).                 00009500
GCT              05 COMM-RA00-ITEM           PIC 9(02).                 00011200
                 05 FILLER                   PIC X(345).                00009500
                 05 COMM-RA00-SSPRGS         PIC X(2978).               00009601
      *                                                                 00009700
      *--------ZONES PROPRES A TRA1X                                    00009801
                 05 COMM-RA00-SSPRG-1X    REDEFINES COMM-RA00-SSPRGS .  00009901
                    10  COMM-RA10-NDEM          PIC  X(05).             00011200
                    10  COMM-RA10-NOM           PIC  X(25).             00011200
                    10  COMM-RA10-DECHEAN       PIC  X(08).             00011200
                    10  COMM-RA10-FONC          PIC  X(03).             00011200
                    10  COMM-RA10-CBANQ         PIC  X(05).             00011200
                    10  COMM-RA10-LBANQ         PIC  X(30).             00011200
                    10  COMM-RA10-ITEM          PIC  9(02).             00011200
AP                  10  COMM-RA10-NLIEU         PIC  X(03).             00011200
AP                  10  FILLER                  PIC  X(17).             00011200
                    10  COMM-RA10-FILLER        PIC  X(2880).           00011201
                    10  COMM-RA01-FILLER    REDEFINES COMM-RA10-FILLER. 00011301
                     12  COMM-RA01-ZONES.                               00011900
                       15  COMM-RA01-NMAG          PIC  X(3).           00011401
                       15  COMM-RA01-NVENTE        PIC  X(7).           00011501
                       15  COMM-RA01-DEVN          PIC  X(8).           00011601
                       15  COMM-RA01-DDIFF         PIC  X(8).           00011801
                       15  COMM-RA01-NOMC          PIC  X(5).           00011401
                       15  COMM-RA01-LNOMC         PIC  X(25).          00011401
                       15  COMM-RA01-LPNOMC        PIC  X(15).          00011501
                       15  COMM-RA01-LADDRC        PIC  X(32).          00011601
                       15  COMM-RA01-NVOIEC        PIC  X(5).           00011801
                       15  COMM-RA01-NTPVOIEC      PIC  X(4).           00011801
                       15  COMM-RA01-NLVOIEC       PIC  X(21).          00011801
                       15  COMM-RA01-CCOMNC        PIC  X(32).          00011401
                       15  COMM-RA01-CPOSTC        PIC  X(5).           00011501
                       15  COMM-RA01-LPOSTC        PIC  X(26).          00011601
                       15  COMM-RA01-BAT           PIC  X(3).           00011801
                       15  COMM-RA01-ESC           PIC  X(3).           00011401
                       15  COMM-RA01-ETAGE         PIC  X(3).           00011501
                       15  COMM-RA01-PORTE         PIC  X(3).           00011601
                       15  COMM-RA01-TELD11        PIC  X(2).           00011801
                       15  COMM-RA01-TELD12        PIC  X(2).           00011801
                       15  COMM-RA01-TELD13        PIC  X(2).           00011801
                       15  COMM-RA01-TELD14        PIC  X(2).           00011801
                       15  COMM-RA01-TELB11        PIC  X(2).           00011801
                       15  COMM-RA01-TELB12        PIC  X(2).           00011801
                       15  COMM-RA01-TELB13        PIC  X(2).           00011801
                       15  COMM-RA01-TELB14        PIC  X(2).           00011801
                       15  COMM-RA01-POSTE         PIC  X(5).           00011801
                       15  COMM-RA01-PRODUIT       OCCURS 4.                    
                           20  COMM-RA01-MARQUE    PIC  X(5).               0001
                           20  COMM-RA01-REF       PIC  X(15).              0001
                           20  COMM-RA01-CODIC     PIC  X(7).               0001
                           20  COMM-RA01-PVDARTY   PIC  9(5)V9(2).          0001
                           20  COMM-RA01-PVCONC    PIC  9(5)V9(2).          0001
                           20  COMM-RA01-NCONC     PIC  X(4).               0001
                           20  COMM-RA01-TYPCONC   PIC  X(1).               0001
                           20  COMM-RA01-QTE       PIC  9(3).               0001
                           20  COMM-RA01-REMB      PIC  9(5)V9(2).          0001
                           20  COMM-RA01-DIFF      PIC  9(5)V9(2).          0001
                       15  COMM-RA01-TOTDIFF       PIC  9(5)V9(2).      00011801
                       15  COMM-RA01-TOTREMB       PIC  9(5)V9(2).      00011801
                       15  COMM-RA01-DECHEANC      PIC  X(8).           00011801
                       15  COMM-RA01-TYPREMB       PIC  X(1).           00011801
                       15  COMM-RA01-CBANQUE       PIC  X(5).           00011801
                       15  COMM-RA01-LBANQUE       PIC  X(30).          00011801
                       15  COMM-RA01-CLETTRE       PIC  X(2).           00011801
                       15  COMM-RA01-NB-RENSEIGNE  PIC  9(1).           00011801
                       15  COMM-RA01-DCREATION     PIC  X(8).           00011801
                     12  COMM-RA01-LIBERR          PIC  X(58).          00011900
                     12  COMM-RA01-TACHE-PREC      PIC  X(5).           00011900
      *                                                                         
1096                 12  COMM-RA01-TEL10.                               00011900
1096                   15  COMM-RA01-TELD10        PIC  X(2).           00011801
1096                   15  COMM-RA01-TELB10        PIC  X(2).           00011801
1096                 12  FILLER                    PIC  X(2245).        00011900
                    10  COMM-RA03-FILLER    REDEFINES COMM-RA10-FILLER. 00012001
                       15  COMM-RA03-ITEM          PIC  9(2).           00012101
                       15  COMM-RA03-POS-MAX       PIC  9(2).           00012201
                       15  COMM-RA03-CPT-LIGNE     PIC  9(2).           00012501
                       15  COMM-RA03-TACHE-PREC    PIC  X(5).           00012501
                       15  COMM-RA03-TYPDEM        PIC  X(20).          00012501
                       15  COMM-RA03-CASCURSEUR    PIC  X(1).           00012501
                       15  FILLER                  PIC  X(2848).        00012600
                    10  COMM-RA04-FILLER    REDEFINES COMM-RA10-FILLER. 00012701
                       15  COMM-RA04-ITEM          PIC  9(2).           00012801
                       15  COMM-RA04-POS-MAX       PIC  9(2).           00012901
                       15  COMM-RA04-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA04-TACHE-PREC    PIC  X(5).           00012501
                       15  COMM-RA04-TYPDEM        PIC  X(20).          00012501
                       15  COMM-RA04-POINTGLOBAL   PIC  X(1).           00012501
                       15  FILLER                  PIC  X(2848).        00013300
      *                                                                 00014100
      *--------ZONES PROPRES A TRA2X                                    00014201
                 05 COMM-RA00-SSPRG-2X    REDEFINES COMM-RA00-SSPRGS .  00014301
                    10  COMM-RA20-NDEM          PIC  X(05).             00011200
                    10  COMM-RA20-NOM           PIC  X(25).             00011200
                    10  COMM-RA20-DECHEAN       PIC  X(08).             00011200
                    10  COMM-RA20-FONC          PIC  X(03).             00011200
                    10  COMM-RA20-CBANQ         PIC  X(05).             00011200
                    10  COMM-RA20-LBANQ         PIC  X(30).             00011200
AM                  10  COMM-RA20-TYPE-AVOIR    PIC  X(01).                     
AP                  10  COMM-RA20-NLIEU         PIC  X(03).             00011200
AP                  10  FILLER                  PIC  X(18).                     
                    10  COMM-RA20-FILLER        PIC  X(2880).           00011201
                    10  COMM-RA02-FILLER    REDEFINES COMM-RA20-FILLER. 00015601
                     12  COMM-RA02-ZONES.                               00011900
                       15  COMM-RA02-NMAG          PIC  X(3).           00011401
                       15  COMM-RA02-NVENTE        PIC  X(7).           00011501
                       15  COMM-RA02-DEVN          PIC  X(8).           00011601
                       15  COMM-RA02-NAVOIR        PIC  X(6).           00011801
                       15  COMM-RA02-LAVOIR        PIC  X(23).          00011801
                       15  COMM-RA02-MOTIF         PIC  X(2).           00011801
                       15  COMM-RA02-NOMC          PIC  X(5).           00011401
                       15  COMM-RA02-LNOMC         PIC  X(25).          00011401
                       15  COMM-RA02-LPNOMC        PIC  X(15).          00011501
                       15  COMM-RA02-LADDRC        PIC  X(32).          00011601
                       15  COMM-RA02-NVOIEC        PIC  X(5).           00011801
                       15  COMM-RA02-NTPVOIEC      PIC  X(4).           00011801
                       15  COMM-RA02-NLVOIEC       PIC  X(21).          00011801
                       15  COMM-RA02-CCOMNC        PIC  X(32).          00011401
                       15  COMM-RA02-CPOSTC        PIC  X(5).           00011501
                       15  COMM-RA02-LPOSTC        PIC  X(26).          00011601
                       15  COMM-RA02-BAT           PIC  X(3).           00011801
                       15  COMM-RA02-ESC           PIC  X(3).           00011401
                       15  COMM-RA02-ETAGE         PIC  X(3).           00011501
                       15  COMM-RA02-PORTE         PIC  X(3).           00011601
                       15  COMM-RA02-TELD11        PIC  X(2).           00011801
                       15  COMM-RA02-TELD12        PIC  X(2).           00011801
                       15  COMM-RA02-TELD13        PIC  X(2).           00011801
                       15  COMM-RA02-TELD14        PIC  X(2).           00011801
                       15  COMM-RA02-TELB11        PIC  X(2).           00011801
                       15  COMM-RA02-TELB12        PIC  X(2).           00011801
                       15  COMM-RA02-TELB13        PIC  X(2).           00011801
                       15  COMM-RA02-TELB14        PIC  X(2).           00011801
                       15  COMM-RA02-POSTE         PIC  X(5).           00011801
                       15  COMM-RA02-PRODUIT       OCCURS 4.                    
                           20  COMM-RA02-MARQUE    PIC  X(5).               0001
                           20  COMM-RA02-REF       PIC  X(15).              0001
                           20  COMM-RA02-CODIC     PIC  X(7).               0001
                           20  COMM-RA02-REMB      PIC  9(6)V9(2).          0001
                       15  COMM-RA02-TOTREMB       PIC  9(6)V9(2).      00011801
                       15  COMM-RA02-DECHEANC      PIC  X(8).           00011801
                       15  COMM-RA02-TYPREMB       PIC  X(1).           00011801
                       15  COMM-RA02-CBANQUE       PIC  X(5).           00011801
                       15  COMM-RA02-LBANQUE       PIC  X(30).          00011801
                       15  COMM-RA02-CLETTRE       PIC  X(2).           00011801
                       15  COMM-RA02-NB-RENSEIGNE  PIC  9(1).           00011801
                       15  COMM-RA02-DCREATION     PIC  X(8).           00011801
1099                   15  COMM-RA02-NLETTRAGE     PIC  X(10).          00011801
                     12  COMM-RA02-LIBERR          PIC  X(58).          00011900
                     12  COMM-RA02-TACHE-PREC      PIC  X(5).           00011900
                     12  COMM-RA02-WTLMELA         PIC  X(1).                   
                     12  COMM-RA02-WBLBR           PIC  X(1).                   
      *                                                                         
1096                 12  COMM-RA02-TEL10.                               00011900
1096                   15  COMM-RA02-TELD10        PIC  X(2).           00011801
1096                   15  COMM-RA02-TELB10        PIC  X(2).           00011801
1099                 12  FILLER                    PIC  X(2341).        00011900
EC                  10  COMM-RA22-FILLER    REDEFINES COMM-RA20-FILLER. 00015601
EC                   12  COMM-RA22-ZONES.                               00011900
EC                     15  COMM-RA22-NMAG          PIC  X(3).           00011501
EC                     15  COMM-RA22-NVENTE        PIC  X(7).           00011501
EC                     15  COMM-RA22-DEVN          PIC  X(8).           00011601
EC                     15  COMM-RA22-LAVOIR        PIC  X(23).          00011801
EC                     15  COMM-RA22-MOTIF         PIC  X(2).           00011801
EC                     15  COMM-RA22-NOMC          PIC  X(5).           00011401
EC                     15  COMM-RA22-LNOMC         PIC  X(25).          00011401
EC                     15  COMM-RA22-LPNOMC        PIC  X(15).          00011501
EC                     15  COMM-RA22-LADDRC        PIC  X(32).          00011601
EC                     15  COMM-RA22-NVOIEC        PIC  X(5).           00011801
EC                     15  COMM-RA22-NTPVOIEC      PIC  X(4).           00011801
EC                     15  COMM-RA22-NLVOIEC       PIC  X(21).          00011801
EC                     15  COMM-RA22-CCOMNC        PIC  X(32).          00011401
EC                     15  COMM-RA22-CPOSTC        PIC  X(5).           00011501
EC                     15  COMM-RA22-LPOSTC        PIC  X(26).          00011601
EC                     15  COMM-RA22-BAT           PIC  X(3).           00011801
EC                     15  COMM-RA22-ESC           PIC  X(3).           00011401
EC                     15  COMM-RA22-ETAGE         PIC  X(3).           00011501
EC                     15  COMM-RA22-PORTE         PIC  X(3).           00011601
EC                     15  COMM-RA22-TELD11        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELD12        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELD13        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELD14        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELB11        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELB12        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELB13        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELB14        PIC  X(2).           00011801
EC                     15  COMM-RA22-POSTE         PIC  X(5).           00011801
EC                     15  COMM-RA22-REMB          PIC  9(6)V9(2).          0001
EC                     15  COMM-RA22-DECHEANC      PIC  X(8).           00011801
EC                     15  COMM-RA22-TYPREMB       PIC  X(1).           00011801
EC                     15  COMM-RA22-CBANQUE       PIC  X(5).           00011801
EC                     15  COMM-RA22-LBANQUE       PIC  X(30).          00011801
EC                     15  COMM-RA22-CLETTRE       PIC  X(2).           00011801
EC                     15  COMM-RA22-NB-RENSEIGNE  PIC  9(1).           00011801
EC                     15  COMM-RA22-DCREATION     PIC  X(8).           00011801
EC                   12  COMM-RA22-LIBERR          PIC  X(58).          00011900
EC                   12  COMM-RA22-TACHE-PREC      PIC  X(5).           00011900
EC                   12  COMM-RA22-WTLMELA         PIC  X(1).                   
EC                   12  COMM-RA22-WBLBR           PIC  X(1).                   
EC                   12  COMM-RA22-TEL10.                               00011900
EC                     15  COMM-RA22-TELD10        PIC  X(2).           00011801
EC                     15  COMM-RA22-TELB10        PIC  X(2).           00011801
EC                   12  FILLER                    PIC  X(2501).        00011900
      *                                                                 00019700
      *--------ZONES PROPRES A TRA3X                                    00019801
                 05 COMM-RA00-SSPRG-3X    REDEFINES COMM-RA00-SSPRGS .  00019901
                    10  COMM-RA30-TYPREGL       PIC  X(1).                      
                    10  COMM-RA30-CHQ1          PIC  X(7).                      
                    10  COMM-RA30-CHQ2          PIC  X(7).                      
                    10  COMM-RA30-AVOIR1        PIC  X(7).                      
                    10  COMM-RA30-AVOIR2        PIC  X(7).                      
                    10  COMM-RA30-ESP1          PIC  X(7).                      
                    10  COMM-RA30-ESP2          PIC  X(7).                      
                    10  COMM-RA30-DECHEAN       PIC  X(8).                      
                    10  COMM-RA30-FLAG-DETAIL   PIC  X(1).              00012501
                        88  COMM-RA30-DETAIL-OK     VALUE '0'.              0001
                        88  COMM-RA30-DETAIL-NONOK  VALUE '1'.              0001
AP    ************  10  FILLER                  PIC  X(3).                      
AP                  10  COMM-RA30-NLIEU         PIC  X(3).                      
                    10  COMM-RA30-DREGLT        PIC  X(8).                      
                    10  COMM-RA30-FILLER        PIC  X(2915).           00011201
                    10  COMM-RA31-FILLER    REDEFINES COMM-RA30-FILLER. 00012701
                       15  COMM-RA31-ITEM          PIC  9(2).           00012801
                       15  COMM-RA31-POS-MAX       PIC  9(2).           00012901
                       15  COMM-RA31-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA31-POINTGLOBAL   PIC  X(1).           00012501
                       15  COMM-RA31-NREGL.                             00012501
                           17  COMM-RA31-NREGL0    PIC  X(7).               0001
                           17  COMM-RA31-NREGL1    PIC  X(7).               0001
                           17  COMM-RA31-NREGL2    PIC  X(7).               0001
                       15  COMM-RA31-FLAG          PIC  9(1).           00012501
                           88  COMM-RA31-FLAG-NOK  VALUE 0.                 0001
                           88  COMM-RA31-FLAG-OK   VALUE 1.                 0001
                       15  COMM-RA31-CTYPDEM       PIC  X(5).           00012501
                       15  COMM-RA31-SUITE         PIC  X(1).           00012501
                           88  COMM-RA31-FINTRT    VALUE '1'.               0001
                           88  COMM-RA31-SUITRT    VALUE '0'.               0001
                       15  COMM-RA31-NB-MAJ        PIC  9(7).           00012501
AD04                   15  COMM-RA30-TRILIEU       PIC  X(1).                   
AD04                   15  FILLER                  PIC  X(2872).        00013300
AD04  *                15  FILLER                  PIC  X(2873).        00013300
                    10  COMM-RA33-FILLER    REDEFINES COMM-RA30-FILLER. 00012701
                       15  COMM-RA33-ITEM          PIC  9(2).           00012801
                       15  COMM-RA33-POS-MAX       PIC  9(2).           00012901
                       15  COMM-RA33-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA33-TACHE-PREC    PIC  X(5).           00012501
                       15  COMM-RA33-POINTGLOBAL   PIC  X(1).           00012501
                       15  COMM-RA33-ESP1          PIC  X(7).           00012501
                       15  COMM-RA33-ESP2          PIC  X(7).           00012501
                       15  COMM-RA33-CHQ1          PIC  X(7).           00012501
                       15  COMM-RA33-CHQ2          PIC  X(7).           00012501
                       15  COMM-RA33-AVOIR1        PIC  X(7).           00012501
                       15  COMM-RA33-AVOIR2        PIC  X(7).           00012501
                       15  COMM-RA33-NB-MAJ        PIC  9(7).           00012501
                       15  FILLER                  PIC  X(2854).        00013300
                    10  COMM-RA34-FILLER    REDEFINES COMM-RA30-FILLER. 00012701
                       15  COMM-RA34-ITEM          PIC  9(2).           00012801
                       15  COMM-RA34-POS-MAX       PIC  9(2).           00012901
                       15  COMM-RA34-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA34-TACHE-PREC    PIC  X(5).           00012501
                       15  COMM-RA34-POINTGLOBAL   PIC  X(1).           00012501
                       15  COMM-RA34-ESP1          PIC  X(7).           00012501
                       15  COMM-RA34-ESP2          PIC  X(7).           00012501
                       15  COMM-RA34-CHQ1          PIC  X(7).           00012501
                       15  COMM-RA34-CHQ2          PIC  X(7).           00012501
                       15  COMM-RA34-AVOIR1        PIC  X(7).           00012501
                       15  COMM-RA34-AVOIR2        PIC  X(7).           00012501
                       15  FILLER                  PIC  X(2861).        00013300
                    10  COMM-RA35-FILLER    REDEFINES COMM-RA30-FILLER. 00012701
AD04  *                15  COMM-RA35-ITEM          PIC  9(2).           00012801
AD04  *                15  COMM-RA35-POS-MAX       PIC  9(2).           00012901
AD04                   15  COMM-RA35-ITEM          PIC  9(4).           00012801
AD04                   15  COMM-RA35-POS-MAX       PIC  9(4).           00012901
                       15  COMM-RA35-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA35-TACHE-PREC    PIC  X(5).           00012501
AD04                   15  FILLER                  PIC  X(2900).        00013300
AD04  *                15  FILLER                  PIC  X(2904).        00013300
                    10  COMM-RA36-FILLER    REDEFINES COMM-RA30-FILLER. 00012701
                       15  COMM-RA36-ITEM          PIC  9(4).           00012801
                       15  COMM-RA36-POS-MAX       PIC  9(4).           00012901
                       15  COMM-RA36-CPT-LIGNE     PIC  9(2).           00013201
                       15  COMM-RA36-TACHE-PREC    PIC  X(5).           00012501
AD0204*                15  COMM-RA30-TRILIEU       PIC  X(1).           00013300
AD04                   15  FILLER                  PIC  X(1).           00013300
AD04                   15  COMM-RA30-DATES         PIC  X(8).                   
AD04                   15  FILLER                  PIC  X(2891).                
AD02  *                15  FILLER                  PIC  X(2899).        00013300
AD02  *                15  FILLER                  PIC  X(2903).        00013300
AD02  *                15  FILLER                  PIC  X(2904).        00013300
      *--------ZONES PROPRES A TRA4X                                    00019801
                 05 COMM-RA00-SSPRG-4X    REDEFINES COMM-RA00-SSPRGS .  00019901
                    10  COMM-RA40-ZONCMD        PIC  X(1).                      
                    10  COMM-RA40-FONC          PIC  X(7).                      
                    10  COMM-RA40-FILLER        PIC  X(2970).           00011201
                                                                                
