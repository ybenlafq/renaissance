      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM18-LONG             PIC S9(4) COMP-3 VALUE +141.                
       01  TS-FM18-RECORD.                                                      
           10 TS-FM18-NDEMANDE      PIC X(2).                                   
           10 TS-FM18-DCREATION     PIC X(10).                                  
           10 TS-FM18-CACID         PIC X(8).                                   
           10 TS-FM18-USER          PIC X(25).                                  
           10 TS-FM18-WMFICHE       PIC X.                                      
           10 TS-FM18-WPAPIER       PIC X.                                      
           10 TS-FM18-NETAB         PIC X(3).                                   
AM         10 TS-FM18-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM18-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM18-WTYPAUX       PIC X.                                      
           10 TS-FM18-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM18-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM18-WTOTAL        PIC X    OCCURS 3.                          
           10 TS-FM18-WNTRI         PIC X    OCCURS 3.                          
           10 TS-FM18-CNAT          PIC X(3) OCCURS 10.                         
           10 TS-FM18-CNATE         PIC X(3) OCCURS 10.                         
           10 TS-FM18-WTTRIECRIT    PIC X.                                      
           10 TS-FM18-WCOMPTESOLDE  PIC X.                                      
           10 TS-FM18-WPIECELETTRE  PIC X.                                      
           10 TS-FM18-CDEVISE       PIC X(3).                                   
                                                                                
