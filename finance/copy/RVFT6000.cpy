      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFT60                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6000.                                                            
      *}                                                                        
           10 FT60-CPCG            PIC X(4).                                    
           10 FT60-COMPTE          PIC X(6).                                    
           10 FT60-LCOMPTEC        PIC X(15).                                   
           10 FT60-LCOMPTEL        PIC X(30).                                   
           10 FT60-CMASQUE         PIC X(6).                                    
           10 FT60-CCONSOD         PIC X(6).                                    
           10 FT60-CCONSOC         PIC X(6).                                    
           10 FT60-NAUX            PIC X(3).                                    
           10 FT60-WTYPCOMPTE      PIC X(1).                                    
           10 FT60-WAUXILIARISE    PIC X(1).                                    
           10 FT60-WCOLLECTIF      PIC X(1).                                    
           10 FT60-WABONNE         PIC X(1).                                    
           10 FT60-WREGAUTO        PIC X(1).                                    
           10 FT60-WCATNATCH       PIC X(1).                                    
           10 FT60-WLETTRAGE       PIC X(1).                                    
           10 FT60-WPOINTAGE       PIC X(1).                                    
           10 FT60-WEDITION        PIC X(1).                                    
           10 FT60-WCLOTURE        PIC X(1).                                    
           10 FT60-WINTERFACE      PIC X(1).                                    
           10 FT60-CEPURATION      PIC X(1).                                    
           10 FT60-DCLOTURE        PIC X(8).                                    
           10 FT60-DMAJ            PIC X(8).                                    
           10 FT60-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
