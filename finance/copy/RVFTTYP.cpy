      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTTYP TYPE DE COMPTES GCT  GROUPE      *        
      *----------------------------------------------------------------*        
       01  RVFTTYP.                                                             
           05  FTTYP-CTABLEG2    PIC X(15).                                     
           05  FTTYP-CTABLEG2-REDEF REDEFINES FTTYP-CTABLEG2.                   
               10  FTTYP-CTYPE           PIC X(01).                             
           05  FTTYP-WTABLEG     PIC X(80).                                     
           05  FTTYP-WTABLEG-REDEF  REDEFINES FTTYP-WTABLEG.                    
               10  FTTYP-WACTIF          PIC X(01).                             
               10  FTTYP-LTYPE           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFTTYP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
