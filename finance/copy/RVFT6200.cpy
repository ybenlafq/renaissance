      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFT62                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6200.                                                            
      *}                                                                        
           10 FT62-CPDR            PIC X(4).                                    
           10 FT62-RUBRIQUE        PIC X(6).                                    
           10 FT62-LRUBRIQUEC      PIC X(15).                                   
           10 FT62-LRUBRIQUEL      PIC X(30).                                   
           10 FT62-CMASQUE         PIC X(6).                                    
           10 FT62-WINTERFACE      PIC X(1).                                    
           10 FT62-DCLOTURE        PIC X(8).                                    
           10 FT62-DMAJ            PIC X(8).                                    
           10 FT62-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
