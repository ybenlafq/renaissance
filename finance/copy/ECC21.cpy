      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CTRL CAISSE - CAI NON DEVERSSES                                 00000020
      ***************************************************************** 00000030
       01   ECC21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCSF   PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MNSOCSI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCSL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLSOCSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCSF   PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLSOCSI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUSF  PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MNLIEUSI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUSF  PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MLLIEUSI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MNCAISI   PIC X(3).                                       00000410
           02 LIGNEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCAILL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDCAILL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCAILF      PIC X.                                     00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MDCAILI      PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCLL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSOCLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCLF      PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MNSOCLI      PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEULL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNLIEULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEULF     PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MNLIEULI     PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEULL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLLIEULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLLIEULF     PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MLLIEULI     PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCAILL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCAILL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCAILF      PIC X.                                     00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MNCAILI      PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREMLL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MREMLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MREMLF  PIC X.                                          00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MREMLI  PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNEMLL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MNEMLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNEMLF  PIC X.                                          00000680
             03 FILLER  PIC X(2).                                       00000690
             03 MNEMLI  PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MICSLL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MICSLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MICSLF  PIC X.                                          00000720
             03 FILLER  PIC X(2).                                       00000730
             03 MICSLI  PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MLIBERRI  PIC X(79).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * CTRL CAISSE - CAI NON DEVERSSES                                 00000920
      ***************************************************************** 00000930
       01   ECC21O REDEFINES ECC21I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUO  PIC X(10).                                      00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MTIMJOUA  PIC X.                                          00001020
           02 MTIMJOUC  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEH    PIC X.                                          00001090
           02 MPAGEO    PIC Z9.                                         00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MPAGETOTA      PIC X.                                     00001120
           02 MPAGETOTC PIC X.                                          00001130
           02 MPAGETOTH PIC X.                                          00001140
           02 MPAGETOTO      PIC Z9.                                    00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNSOCSA   PIC X.                                          00001170
           02 MNSOCSC   PIC X.                                          00001180
           02 MNSOCSH   PIC X.                                          00001190
           02 MNSOCSO   PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MLSOCSA   PIC X.                                          00001220
           02 MLSOCSC   PIC X.                                          00001230
           02 MLSOCSH   PIC X.                                          00001240
           02 MLSOCSO   PIC X(20).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNLIEUSA  PIC X.                                          00001270
           02 MNLIEUSC  PIC X.                                          00001280
           02 MNLIEUSH  PIC X.                                          00001290
           02 MNLIEUSO  PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLLIEUSA  PIC X.                                          00001320
           02 MLLIEUSC  PIC X.                                          00001330
           02 MLLIEUSH  PIC X.                                          00001340
           02 MLLIEUSO  PIC X(20).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNCAISA   PIC X.                                          00001370
           02 MNCAISC   PIC X.                                          00001380
           02 MNCAISH   PIC X.                                          00001390
           02 MNCAISO   PIC X(3).                                       00001400
           02 LIGNEO OCCURS   12 TIMES .                                00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MDCAILA      PIC X.                                     00001430
             03 MDCAILC PIC X.                                          00001440
             03 MDCAILH PIC X.                                          00001450
             03 MDCAILO      PIC X(8).                                  00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MNSOCLA      PIC X.                                     00001480
             03 MNSOCLC PIC X.                                          00001490
             03 MNSOCLH PIC X.                                          00001500
             03 MNSOCLO      PIC X(3).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MNLIEULA     PIC X.                                     00001530
             03 MNLIEULC     PIC X.                                     00001540
             03 MNLIEULH     PIC X.                                     00001550
             03 MNLIEULO     PIC X(3).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLLIEULA     PIC X.                                     00001580
             03 MLLIEULC     PIC X.                                     00001590
             03 MLLIEULH     PIC X.                                     00001600
             03 MLLIEULO     PIC X(20).                                 00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNCAILA      PIC X.                                     00001630
             03 MNCAILC PIC X.                                          00001640
             03 MNCAILH PIC X.                                          00001650
             03 MNCAILO      PIC X(3).                                  00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MREMLA  PIC X.                                          00001680
             03 MREMLC  PIC X.                                          00001690
             03 MREMLH  PIC X.                                          00001700
             03 MREMLO  PIC X.                                          00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MNEMLA  PIC X.                                          00001730
             03 MNEMLC  PIC X.                                          00001740
             03 MNEMLH  PIC X.                                          00001750
             03 MNEMLO  PIC X.                                          00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MICSLA  PIC X.                                          00001780
             03 MICSLC  PIC X.                                          00001790
             03 MICSLH  PIC X.                                          00001800
             03 MICSLO  PIC X.                                          00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBERRA  PIC X.                                          00001830
           02 MLIBERRC  PIC X.                                          00001840
           02 MLIBERRH  PIC X.                                          00001850
           02 MLIBERRO  PIC X(79).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAH  PIC X.                                          00001900
           02 MCODTRAO  PIC X(4).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCICSA    PIC X.                                          00001930
           02 MCICSC    PIC X.                                          00001940
           02 MCICSH    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MSCREENA  PIC X.                                          00001980
           02 MSCREENC  PIC X.                                          00001990
           02 MSCREENH  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
