      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT1301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT1301                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1301.                                                            
      *}                                                                        
           02  FT13-NSOC                                                        
               PIC X(0005).                                                     
           02  FT13-NETAB                                                       
               PIC X(0003).                                                     
           02  FT13-NJRN                                                        
               PIC X(0003).                                                     
           02  FT13-NEXER                                                       
               PIC X(0004).                                                     
           02  FT13-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT13-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FT13-DECH                                                        
               PIC X(0008).                                                     
           02  FT13-MONTANT                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT13-MTDEVBASE                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT13-NTBENEF                                                     
               PIC X(0008).                                                     
           02  FT13-METHREG                                                     
               PIC X(0003).                                                     
           02  FT13-WBAP                                                        
               PIC X(0001).                                                     
           02  FT13-WSENS                                                       
               PIC X(0001).                                                     
           02  FT13-NPROPO                                                      
               PIC X(0004).                                                     
           02  FT13-NORDRE                                                      
               PIC X(0004).                                                     
           02  FT13-NREG                                                        
               PIC X(0007).                                                     
           02  FT13-CBANQUE                                                     
               PIC X(0005).                                                     
           02  FT13-NEXERREG                                                    
               PIC X(0005).                                                     
           02  FT13-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT13-NAUX                                                        
               PIC X(0003).                                                     
           02  FT13-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT13-LETTRAGE                                                    
               PIC X(0010).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT1301                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1301-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1301-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-DECH-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-DECH-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-MTDEVBASE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-MTDEVBASE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NTBENEF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NTBENEF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-METHREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-METHREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-WBAP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-WBAP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-WSENS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-WSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NPROPO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NPROPO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NREG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NREG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-CBANQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-CBANQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NEXERREG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NEXERREG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT13-LETTRAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT13-LETTRAGE-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
