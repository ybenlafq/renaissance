      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SIGA - CORRESPONDANCE COMPTABLE                                 00000020
      ***************************************************************** 00000030
       01   ESG01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPSOCL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTYPSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPSOCF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTYPSOCI  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEI    PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MPAGEMAXI      PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRUBL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLRUBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRUBF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLRUBI    PIC X(8).                                       00000310
           02 MLIGNEI OCCURS   12 TIMES .                               00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBPAIEL    COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MRUBPAIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MRUBPAIEF    PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MRUBPAIEI    PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUBPAIEL   COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLRUBPAIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLRUBPAIEF   PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLRUBPAIEI   PIC X(25).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTRATL    COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MCONTRATL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCONTRATF    PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MCONTRATI    PIC X(2).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTEBILL    COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCPTEBILL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPTEBILF    PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCPTEBILI    PIC X(8).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSBILL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MSENSBILL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSENSBILF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MSENSBILI    PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTECHGL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCPTECHGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPTECHGF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCPTECHGI    PIC X(8).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSCHGL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MSENSCHGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSENSCHGF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MSENSCHGI    PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTIONL    COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MSECTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECTIONF    PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MSECTIONI    PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUEL   COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MRUBRIQUEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MRUBRIQUEF   PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MRUBRIQUEI   PIC X(6).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPLL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCOMPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOMPLF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCOMPLI      PIC X.                                     00000720
      * ZONE CMD AIDA                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(79).                                      00000770
      * CODE TRANSACTION                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      * CICS DE TRAVAIL                                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      * NETNAME                                                         00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      * CODE TERMINAL                                                   00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SIGA - CORRESPONDANCE COMPTABLE                                 00000990
      ***************************************************************** 00001000
       01   ESG01O REDEFINES ESG01I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
      * DATE DU JOUR                                                    00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
      * HEURE                                                           00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTYPSOCA  PIC X.                                          00001200
           02 MTYPSOCC  PIC X.                                          00001210
           02 MTYPSOCP  PIC X.                                          00001220
           02 MTYPSOCH  PIC X.                                          00001230
           02 MTYPSOCV  PIC X.                                          00001240
           02 MTYPSOCO  PIC X(3).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC ZZ9.                                        00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMAXA      PIC X.                                     00001340
           02 MPAGEMAXC PIC X.                                          00001350
           02 MPAGEMAXP PIC X.                                          00001360
           02 MPAGEMAXH PIC X.                                          00001370
           02 MPAGEMAXV PIC X.                                          00001380
           02 MPAGEMAXO      PIC ZZ9.                                   00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLRUBA    PIC X.                                          00001410
           02 MLRUBC    PIC X.                                          00001420
           02 MLRUBP    PIC X.                                          00001430
           02 MLRUBH    PIC X.                                          00001440
           02 MLRUBV    PIC X.                                          00001450
           02 MLRUBO    PIC X(8).                                       00001460
           02 MLIGNEO OCCURS   12 TIMES .                               00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MRUBPAIEA    PIC X.                                     00001490
             03 MRUBPAIEC    PIC X.                                     00001500
             03 MRUBPAIEP    PIC X.                                     00001510
             03 MRUBPAIEH    PIC X.                                     00001520
             03 MRUBPAIEV    PIC X.                                     00001530
             03 MRUBPAIEO    PIC X(3).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLRUBPAIEA   PIC X.                                     00001560
             03 MLRUBPAIEC   PIC X.                                     00001570
             03 MLRUBPAIEP   PIC X.                                     00001580
             03 MLRUBPAIEH   PIC X.                                     00001590
             03 MLRUBPAIEV   PIC X.                                     00001600
             03 MLRUBPAIEO   PIC X(25).                                 00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MCONTRATA    PIC X.                                     00001630
             03 MCONTRATC    PIC X.                                     00001640
             03 MCONTRATP    PIC X.                                     00001650
             03 MCONTRATH    PIC X.                                     00001660
             03 MCONTRATV    PIC X.                                     00001670
             03 MCONTRATO    PIC X(2).                                  00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MCPTEBILA    PIC X.                                     00001700
             03 MCPTEBILC    PIC X.                                     00001710
             03 MCPTEBILP    PIC X.                                     00001720
             03 MCPTEBILH    PIC X.                                     00001730
             03 MCPTEBILV    PIC X.                                     00001740
             03 MCPTEBILO    PIC X(8).                                  00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MSENSBILA    PIC X.                                     00001770
             03 MSENSBILC    PIC X.                                     00001780
             03 MSENSBILP    PIC X.                                     00001790
             03 MSENSBILH    PIC X.                                     00001800
             03 MSENSBILV    PIC X.                                     00001810
             03 MSENSBILO    PIC X.                                     00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MCPTECHGA    PIC X.                                     00001840
             03 MCPTECHGC    PIC X.                                     00001850
             03 MCPTECHGP    PIC X.                                     00001860
             03 MCPTECHGH    PIC X.                                     00001870
             03 MCPTECHGV    PIC X.                                     00001880
             03 MCPTECHGO    PIC X(8).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MSENSCHGA    PIC X.                                     00001910
             03 MSENSCHGC    PIC X.                                     00001920
             03 MSENSCHGP    PIC X.                                     00001930
             03 MSENSCHGH    PIC X.                                     00001940
             03 MSENSCHGV    PIC X.                                     00001950
             03 MSENSCHGO    PIC X.                                     00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MSECTIONA    PIC X.                                     00001980
             03 MSECTIONC    PIC X.                                     00001990
             03 MSECTIONP    PIC X.                                     00002000
             03 MSECTIONH    PIC X.                                     00002010
             03 MSECTIONV    PIC X.                                     00002020
             03 MSECTIONO    PIC X(3).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MRUBRIQUEA   PIC X.                                     00002050
             03 MRUBRIQUEC   PIC X.                                     00002060
             03 MRUBRIQUEP   PIC X.                                     00002070
             03 MRUBRIQUEH   PIC X.                                     00002080
             03 MRUBRIQUEV   PIC X.                                     00002090
             03 MRUBRIQUEO   PIC X(6).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MCOMPLA      PIC X.                                     00002120
             03 MCOMPLC PIC X.                                          00002130
             03 MCOMPLP PIC X.                                          00002140
             03 MCOMPLH PIC X.                                          00002150
             03 MCOMPLV PIC X.                                          00002160
             03 MCOMPLO      PIC X.                                     00002170
      * ZONE CMD AIDA                                                   00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBERRA  PIC X.                                          00002200
           02 MLIBERRC  PIC X.                                          00002210
           02 MLIBERRP  PIC X.                                          00002220
           02 MLIBERRH  PIC X.                                          00002230
           02 MLIBERRV  PIC X.                                          00002240
           02 MLIBERRO  PIC X(79).                                      00002250
      * CODE TRANSACTION                                                00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
      * CICS DE TRAVAIL                                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCICSA    PIC X.                                          00002360
           02 MCICSC    PIC X.                                          00002370
           02 MCICSP    PIC X.                                          00002380
           02 MCICSH    PIC X.                                          00002390
           02 MCICSV    PIC X.                                          00002400
           02 MCICSO    PIC X(5).                                       00002410
      * NETNAME                                                         00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
      * CODE TERMINAL                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MSCREENA  PIC X.                                          00002520
           02 MSCREENC  PIC X.                                          00002530
           02 MSCREENP  PIC X.                                          00002540
           02 MSCREENH  PIC X.                                          00002550
           02 MSCREENV  PIC X.                                          00002560
           02 MSCREENO  PIC X(4).                                       00002570
                                                                                
