      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTDIV CONTROLE COMPTES GCT             *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFTDIV.                                                             
           05  FTDIV-CTABLEG2    PIC X(15).                                     
           05  FTDIV-CTABLEG2-REDEF REDEFINES FTDIV-CTABLEG2.                   
               10  FTDIV-ELTCTL          PIC X(12).                             
               10  FTDIV-TYPCTL          PIC X(03).                             
           05  FTDIV-WTABLEG     PIC X(80).                                     
           05  FTDIV-WTABLEG-REDEF  REDEFINES FTDIV-WTABLEG.                    
               10  FTDIV-WACTIF          PIC X(01).                             
               10  FTDIV-LIBELLE         PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFTDIV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTDIV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTDIV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTDIV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTDIV-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
