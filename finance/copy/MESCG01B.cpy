      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * LONGUEUR 105 DEFINITION ENTETE                                 *      13
      *           43 DEFINITION DATA                                   *        
      * COPY POUR LE PROGRAMME MCG01B                                  *      13
      ******************************************************************        
       01 WS-MQ10B.                                                             
          02 WS-QUEUEB.                                                         
             10 MQ10B-MSGID              PIC X(24).                             
             10 MQ10B-CORRELID           PIC X(24).                             
          02 WS-CODRETB                  PIC X(02).                             
          02 WS-MESSAGEB.                                                       
             05 MCG01B-ENTETE.                                                  
                10 MCG01B-TYPE           PIC X(03).                             
                10 MCG01B-NSOCMSG        PIC X(03).                             
                10 MCG01B-NLIEUMSG       PIC X(03).                             
                10 MCG01B-NSOCDST        PIC X(03).                             
                10 MCG01B-NLIEUDST       PIC X(03).                             
                10 MCG01B-NORD           PIC 9(08).                             
                10 MCG01B-LPROG          PIC X(10).                             
                10 MCG01B-DJOUR          PIC X(08).                             
                10 MCG01B-WSID           PIC X(10).                             
                10 MCG01B-USER           PIC X(10).                             
                10 MCG01B-CHRONO         PIC 9(07).                             
                10 MCG01B-NBRMSG         PIC 9(07).                             
                10 MCG01B-NBRENR         PIC 9(05).                             
                10 MCG01B-TAILLE         PIC 9(05).                             
                10 MCG01B-FILLER         PIC X(20).                             
             05 MCG01B-DATA.                                                    
                10 MCG01B-RETOUR1        PIC X(01).                             
                10 MCG01B-HDR            PIC X(03).                             
                10 MCG01B-NCDE           PIC X(07).                             
                10 MCG01B-NSOCDEPOT      PIC X(06).                             
                10 MCG01B-NCODIC         PIC X(07).                             
                10 MCG01B-PBF            PIC 9(07)V99.                          
                10 MCG01B-PRA            PIC 9(07)V99.                          
                10 MCG01B-RETOUR2        PIC X(01).                             
                                                                                
