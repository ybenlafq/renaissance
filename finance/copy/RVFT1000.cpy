      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFT1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT1000.                                                            
           02  FT10-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT10-NAUX                                                        
               PIC X(0003).                                                     
           02  FT10-LIBFOUR                                                     
               PIC X(0030).                                                     
           02  FT10-NTBENEF                                                     
               PIC X(0008).                                                     
           02  FT10-CSIRET                                                      
               PIC X(0014).                                                     
           02  FT10-CAPE                                                        
               PIC X(0004).                                                     
           02  FT10-PROFESSION                                                  
               PIC X(0020).                                                     
           02  FT10-CREGTVA                                                     
               PIC X(0001).                                                     
           02  FT10-CTRESO                                                      
               PIC X(0002).                                                     
           02  FT10-NBJREG                                                      
               PIC S9(3) COMP-3.                                                
           02  FT10-INDICE                                                      
               PIC X(0001).                                                     
           02  FT10-JPAIE                                                       
               PIC X(0002).                                                     
           02  FT10-WREGECH                                                     
               PIC X(0001).                                                     
           02  FT10-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT10-SECTION                                                     
               PIC X(0006).                                                     
           02  FT10-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FT10-DEVISEREG                                                   
               PIC X(0003).                                                     
           02  FT10-RETENREG                                                    
               PIC X(0001).                                                     
           02  FT10-METHREG                                                     
               PIC X(0003).                                                     
           02  FT10-DCREAT                                                      
               PIC X(0008).                                                     
           02  FT10-ACIDCRE                                                     
               PIC X(0008).                                                     
           02  FT10-DMAJ                                                        
               PIC X(0008).                                                     
           02  FT10-ACIDMAJ                                                     
               PIC X(0008).                                                     
           02  FT10-WACTIF                                                      
               PIC X(0001).                                                     
           02  FT10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-LIBFOUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-LIBFOUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-NTBENEF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-NTBENEF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-CSIRET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-CSIRET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-CAPE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-CAPE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-PROFESSION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-PROFESSION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-CREGTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-CREGTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-CTRESO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-CTRESO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-NBJREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-NBJREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-INDICE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-INDICE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-JPAIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-JPAIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-WREGECH-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-WREGECH-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-DEVISEREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-DEVISEREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-RETENREG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-RETENREG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-METHREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-METHREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-ACIDCRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-ACIDCRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-ACIDMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-ACIDMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-WACTIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
