      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  DSECT-IF010.                                                         
      ******************************************************************        
      *                                                                         
      *   DESCRIPTION DU FICHIER DES MOUVEMENTS A COMPTABILISER                 
      *                                                                         
      ******************************************************************        
      *  SOCIETE COMPTABLE                                                      
           05  IF010-NSOCCOMPT  PIC X(3).                               001 003 
      *  CODE ETABLISSEMENT                                                     
           05  IF010-NETAB      PIC X(3).                               004 003 
      *  COMPTE COMPTABLE                                                       
           05  IF010-COMPTE     PIC X(6).                               007 006 
      *  SECTION ANALYTIQUE                                                     
           05  IF010-SECTION    PIC X(6).                               013 006 
      *  RUBRIQUE ANALYTIQUE                                                    
           05  IF010-RUBRIQUE   PIC X(6).                               019 006 
      *  RUPTURE DE CUMUL                                                       
           05  IF010-ZONECUMUL  PIC X(20).                              025 020 
      *  NUMERO D'ECS                                                           
           05  IF010-NOECS      PIC X(5).                               045 005 
      *  CODE ORGANISME                                                         
           05  IF010-CORGANISME PIC X(3).                               050 003 
      *  MODE DE PAIEMENT                                                       
           05  IF010-CMODPAI    PIC X(3).                               053 003 
      *  SOCIETE DE GESTION                                                     
           05  IF010-NSOCIETE   PIC X(3).                               056 003 
      *  LIEU DE GESTION                                                        
           05  IF010-NLIEU      PIC X(3).                               059 003 
      *  IDENTIFIANT POUR L'ORGANISME                                           
           05  IF010-NIDENT     PIC X(20).                              062 020 
      *  DATE DU MOUVEMENT                                                      
           05  IF010-DOPER      PIC X(8).                               082 008 
      *  DATE DE FINANCEMENT                                                    
           05  IF010-DFINANCE   PIC X(8).                               090 008 
      *  DATE DE REMISE                                                         
           05  IF010-DREMISE    PIC X(8).                               098 008 
      *  NUMERO DE REMISE                                                       
           05  IF010-NREMISE    PIC X(6).                               106 006 
      *  NUMERO DE DOSSIER                                                      
           05  IF010-NDOSSIER   PIC X(8).                               112 008 
      *  NOM DU CLIENT                                                          
           05  IF010-NOMCLIENT  PIC X(15).                              120 015 
      *  REFERENCE INTERNE (BON DE COMMANDE)                                    
           05  IF010-REFINTERNE PIC X(9).                               135 009 
      *  NOMBRES DE FACTURETTES                                                 
           05  IF010-NFACTURES  PIC S9(7)        COMP-3.                144 004 
      *  NUMERO DE CARTE DU PORTEUR                                             
           05  IF010-NPORTEUR   PIC X(19).                              148 019 
      *  MONTANT DE LA LIGNE                                                    
           05  IF010-MONTANT    PIC S9(9)V9(2)   COMP-3.                167 006 
      *  TYPE DE COMPTABILITE                                                   
           05  IF010-CTYPCTA    PIC X(3).                               173 003 
      *  TYPE DE MONTANT                                                        
           05  IF010-CTYPMONT   PIC X(5).                               176 005 
      *  MODE DE COMPTABILISATION                                               
           05  IF010-CMODCTA    PIC X(1).                               181 001 
      *  CODE AUXILIAIRE                                                        
           05  IF010-NAUX       PIC X(3).                               182 003 
      *  LIGNE A LETTRER (O/N)                                                  
           05  IF010-WLETTRAGE  PIC X(1).                               185 001 
      *  CODE BANQUE DE FINANCEMENT                                             
           05  IF010-CBANQUE    PIC X(2).                               186 002 
      *  CODE JOURNAL                                                           
           05  IF010-NJRN       PIC X(3).                               188 003 
      *  DEVISE ( 1 CARACTERE )                                                 
           05  IF010-CDEVISE    PIC X(1).                                       
           05  IF010-FILLER     PIC X(1).                               191 002 
      *******************************************************************       
      * DESCRIPTION DYNAMIQUE DU FICHIER IF010                                  
      *******************************************************************       
       01  IF010-CHAMPS.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FILLER      PIC S9(4)    VALUE +29    COMP.                      
      *--                                                                       
           05  FILLER      PIC S9(4)    VALUE +29 COMP-5.                       
      *}                                                                        
           05  FILLER      PIC X(10)   VALUE 'NSOCCOMPT '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NETAB     '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'COMPTE    '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +6   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'SECTION   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +6   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'RUBRIQUE  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +6   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'ZONECUMUL '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +20  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NOECS     '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +5   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CORGANISME'.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CMODPAI   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NSOCIETE  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NLIEU     '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NIDENT    '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +20  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DOPER     '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DFINANCE  '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'DREMISE   '.                      
           05  FILLER      PIC X       VALUE 'S'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NREMISE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +6   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NDOSSIER  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +8   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NOMCLIENT '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +15  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'REFINTERNE'.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +9   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NFACTURES '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +7   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NPORTEUR  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +19  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'MONTANT   '.                      
           05  FILLER      PIC X       VALUE 'P'.                               
           05  FILLER      PIC S9(3)   VALUE +11  COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CTYPCTA   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CTYPMONT  '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +5   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CMODCTA   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +1   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NAUX      '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'WLETTRAGE '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +1   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CBANQUE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +2   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'NJRN      '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +3   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
           05  FILLER      PIC X(10)   VALUE 'CDEVISE   '.                      
           05  FILLER      PIC X       VALUE 'C'.                               
           05  FILLER      PIC S9(3)   VALUE +1   COMP-3.                       
           05  FILLER      PIC S9(3)   VALUE +0   COMP-3.                       
                                                                                
