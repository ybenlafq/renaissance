      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SGRUB RUB DE PAIE CONCERNES PAR TVA    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSGRUB.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSGRUB.                                                             
      *}                                                                        
           05  SGRUB-CTABLEG2    PIC X(15).                                     
           05  SGRUB-CTABLEG2-REDEF REDEFINES SGRUB-CTABLEG2.                   
               10  SGRUB-RUBPAIE         PIC X(03).                             
           05  SGRUB-WTABLEG     PIC X(80).                                     
           05  SGRUB-WTABLEG-REDEF  REDEFINES SGRUB-WTABLEG.                    
               10  SGRUB-TYPMNT          PIC X(02).                             
               10  SGRUB-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSGRUB-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSGRUB-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGRUB-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SGRUB-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SGRUB-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SGRUB-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
