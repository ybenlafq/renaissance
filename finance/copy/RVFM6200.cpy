      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM6200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM6200                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM6200.                                                            
           02  FM62-CPDR                                                        
               PIC X(0004).                                                     
           02  FM62-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FM62-LRUBRIQUEC                                                  
               PIC X(0015).                                                     
           02  FM62-LRUBRIQUEL                                                  
               PIC X(0030).                                                     
           02  FM62-CMASQUE                                                     
               PIC X(0006).                                                     
           02  FM62-WINTERFACE                                                  
               PIC X(0001).                                                     
           02  FM62-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FM62-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM62-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM6200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-CPDR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-CPDR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-LRUBRIQUEC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-LRUBRIQUEC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-LRUBRIQUEL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-LRUBRIQUEL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-WINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-WINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM62-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM62-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
