      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA105 AU 26/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA105.                                                        
            05 NOMETAT-IRA105           PIC X(6) VALUE 'IRA105'.                
            05 RUPTURES-IRA105.                                                 
           10 IRA105-NSOCIETE           PIC X(03).                      007  003
           10 IRA105-TYPREGL            PIC X(12).                      010  012
           10 IRA105-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA105-NDEMANDE           PIC X(05).                      027  005
           10 IRA105-NREGL              PIC X(07).                      032  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA105-SEQUENCE           PIC S9(04) COMP.                039  002
      *--                                                                       
           10 IRA105-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA105.                                                   
           10 IRA105-BANQUE             PIC X(20).                      041  020
           10 IRA105-FLAGANN            PIC X(06).                      061  006
           10 IRA105-NOM                PIC X(27).                      067  027
           10 IRA105-MTREMBTOT          PIC 9(09)V9(2).                 094  011
           10 IRA105-QTE                PIC 9(01)     .                 105  001
           10 IRA105-DATEPARAM          PIC X(08).                      106  008
           10 IRA105-DREGL              PIC X(08).                      114  008
            05 FILLER                      PIC X(391).                          
                                                                                
