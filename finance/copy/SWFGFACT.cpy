      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DU FICHIER DES FACTURES A MICRO-FICHER                    
      *                                                                         
      *                                                                         
       01         FFGFACT-DSECT.                                                
      *                                                                         
           03     FFGFACT-IDENT.                                                
1            05   FFGFACT-NSEQ         PIC S9(9) COMP-3.                        
6            05   FFGFACT-NSOCCOMPT    PIC X(03).                               
9            05   FFGFACT-CCHRONO      PIC X(02).                               
11           05   FFGFACT-NSOCCOR      PIC X(03).                               
14           05   FFGFACT-DFACT        PIC X(08).                               
22           05   FFGFACT-NSOC-E       PIC X(03).                               
25           05   FFGFACT-NLIEU-E      PIC X(03).                               
28           05   FFGFACT-NSOC-R       PIC X(03).                               
31           05   FFGFACT-NLIEU-R      PIC X(03).                               
34           05   FFGFACT-NUMFACT      PIC X(07).                               
41           05   FFGFACT-NATFACT      PIC X(05).                               
46           05   FFGFACT-EMIS-RECU    PIC X(01).                               
47         03     FFGFACT-ASA          PIC X(01).                               
48         03     FFGFACT-LIGNE        PIC X(132).                              
LG=179*                                                                         
                                                                                
