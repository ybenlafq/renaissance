      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - COMPOSITION LIBELLE                                       00000020
      ***************************************************************** 00000030
       01   EFM61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCINTERFACEI   PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLINTERFACEI   PIC X(30).                                 00000230
           02 MTABRI OCCURS   6 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITEREL    COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MCRITEREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRITEREF    PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MCRITEREI    PIC X(8).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCRITEREL   COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLCRITEREL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCRITEREF   PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLCRITEREI   PIC X(25).                                 00000320
           02 MSEQSD OCCURS   3 TIMES .                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEQSL  COMP PIC S9(4).                                 00000340
      *--                                                                       
             03 MSEQSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEQSF  PIC X.                                          00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MSEQSI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPSECL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MWTYPSECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPSECF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWTYPSECI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNATSECL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MWNATSECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWNATSECF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWNATSECI      PIC X.                                     00000450
           02 MSEQRD OCCURS   3 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEQRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSEQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEQRF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSEQRI  PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPRUBL      COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MWTYPRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPRUBF      PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MWTYPRUBI      PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNATRUBL      COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MWNATRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWNATRUBF      PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MWNATRUBI      PIC X.                                     00000580
      * ZONE CMD AIDA                                                   00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLIBERRI  PIC X(79).                                      00000630
      * CODE TRANSACTION                                                00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCODTRAI  PIC X(4).                                       00000680
      * CICS DE TRAVAIL                                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCICSI    PIC X(5).                                       00000730
      * NETNAME                                                         00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      * CODE TERMINAL                                                   00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MSCREENI  PIC X(4).                                       00000830
      ***************************************************************** 00000840
      * GCT - COMPOSITION LIBELLE                                       00000850
      ***************************************************************** 00000860
       01   EFM61O REDEFINES EFM61I.                                    00000870
           02 FILLER    PIC X(12).                                      00000880
      * DATE DU JOUR                                                    00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDATJOUA  PIC X.                                          00000910
           02 MDATJOUC  PIC X.                                          00000920
           02 MDATJOUP  PIC X.                                          00000930
           02 MDATJOUH  PIC X.                                          00000940
           02 MDATJOUV  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
      * HEURE                                                           00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTIMJOUA  PIC X.                                          00000990
           02 MTIMJOUC  PIC X.                                          00001000
           02 MTIMJOUP  PIC X.                                          00001010
           02 MTIMJOUH  PIC X.                                          00001020
           02 MTIMJOUV  PIC X.                                          00001030
           02 MTIMJOUO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MCINTERFACEA   PIC X.                                     00001060
           02 MCINTERFACEC   PIC X.                                     00001070
           02 MCINTERFACEP   PIC X.                                     00001080
           02 MCINTERFACEH   PIC X.                                     00001090
           02 MCINTERFACEV   PIC X.                                     00001100
           02 MCINTERFACEO   PIC X(5).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLINTERFACEA   PIC X.                                     00001130
           02 MLINTERFACEC   PIC X.                                     00001140
           02 MLINTERFACEP   PIC X.                                     00001150
           02 MLINTERFACEH   PIC X.                                     00001160
           02 MLINTERFACEV   PIC X.                                     00001170
           02 MLINTERFACEO   PIC X(30).                                 00001180
           02 MTABRO OCCURS   6 TIMES .                                 00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MCRITEREA    PIC X.                                     00001210
             03 MCRITEREC    PIC X.                                     00001220
             03 MCRITEREP    PIC X.                                     00001230
             03 MCRITEREH    PIC X.                                     00001240
             03 MCRITEREV    PIC X.                                     00001250
             03 MCRITEREO    PIC X(8).                                  00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MLCRITEREA   PIC X.                                     00001280
             03 MLCRITEREC   PIC X.                                     00001290
             03 MLCRITEREP   PIC X.                                     00001300
             03 MLCRITEREH   PIC X.                                     00001310
             03 MLCRITEREV   PIC X.                                     00001320
             03 MLCRITEREO   PIC X(25).                                 00001330
           02 DFHMS1 OCCURS   3 TIMES .                                 00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MSEQSA  PIC X.                                          00001360
             03 MSEQSC  PIC X.                                          00001370
             03 MSEQSP  PIC X.                                          00001380
             03 MSEQSH  PIC X.                                          00001390
             03 MSEQSV  PIC X.                                          00001400
             03 MSEQSO  PIC X.                                          00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MWTYPSECA      PIC X.                                     00001430
           02 MWTYPSECC PIC X.                                          00001440
           02 MWTYPSECP PIC X.                                          00001450
           02 MWTYPSECH PIC X.                                          00001460
           02 MWTYPSECV PIC X.                                          00001470
           02 MWTYPSECO      PIC X.                                     00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MWNATSECA      PIC X.                                     00001500
           02 MWNATSECC PIC X.                                          00001510
           02 MWNATSECP PIC X.                                          00001520
           02 MWNATSECH PIC X.                                          00001530
           02 MWNATSECV PIC X.                                          00001540
           02 MWNATSECO      PIC X.                                     00001550
           02 DFHMS2 OCCURS   3 TIMES .                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MSEQRA  PIC X.                                          00001580
             03 MSEQRC  PIC X.                                          00001590
             03 MSEQRP  PIC X.                                          00001600
             03 MSEQRH  PIC X.                                          00001610
             03 MSEQRV  PIC X.                                          00001620
             03 MSEQRO  PIC X.                                          00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MWTYPRUBA      PIC X.                                     00001650
           02 MWTYPRUBC PIC X.                                          00001660
           02 MWTYPRUBP PIC X.                                          00001670
           02 MWTYPRUBH PIC X.                                          00001680
           02 MWTYPRUBV PIC X.                                          00001690
           02 MWTYPRUBO      PIC X.                                     00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MWNATRUBA      PIC X.                                     00001720
           02 MWNATRUBC PIC X.                                          00001730
           02 MWNATRUBP PIC X.                                          00001740
           02 MWNATRUBH PIC X.                                          00001750
           02 MWNATRUBV PIC X.                                          00001760
           02 MWNATRUBO      PIC X.                                     00001770
      * ZONE CMD AIDA                                                   00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MLIBERRA  PIC X.                                          00001800
           02 MLIBERRC  PIC X.                                          00001810
           02 MLIBERRP  PIC X.                                          00001820
           02 MLIBERRH  PIC X.                                          00001830
           02 MLIBERRV  PIC X.                                          00001840
           02 MLIBERRO  PIC X(79).                                      00001850
      * CODE TRANSACTION                                                00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
      * CICS DE TRAVAIL                                                 00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCICSA    PIC X.                                          00001960
           02 MCICSC    PIC X.                                          00001970
           02 MCICSP    PIC X.                                          00001980
           02 MCICSH    PIC X.                                          00001990
           02 MCICSV    PIC X.                                          00002000
           02 MCICSO    PIC X(5).                                       00002010
      * NETNAME                                                         00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MNETNAMA  PIC X.                                          00002040
           02 MNETNAMC  PIC X.                                          00002050
           02 MNETNAMP  PIC X.                                          00002060
           02 MNETNAMH  PIC X.                                          00002070
           02 MNETNAMV  PIC X.                                          00002080
           02 MNETNAMO  PIC X(8).                                       00002090
      * CODE TERMINAL                                                   00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MSCREENA  PIC X.                                          00002120
           02 MSCREENC  PIC X.                                          00002130
           02 MSCREENP  PIC X.                                          00002140
           02 MSCREENH  PIC X.                                          00002150
           02 MSCREENV  PIC X.                                          00002160
           02 MSCREENO  PIC X(4).                                       00002170
                                                                                
