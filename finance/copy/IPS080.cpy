      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS080 AU 29/01/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS080.                                                        
            05 NOMETAT-IPS080           PIC X(6) VALUE 'IPS080'.                
            05 RUPTURES-IPS080.                                                 
           10 IPS080-NOSAV              PIC X(03).                      007  003
           10 IPS080-CGCPLT             PIC X(05).                      010  005
           10 IPS080-CSECTEUR           PIC X(05).                      015  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS080-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IPS080-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS080.                                                   
           10 IPS080-CDEVISE            PIC X(06).                      022  006
           10 IPS080-CPOSTALSAV         PIC X(05).                      028  005
           10 IPS080-CPOSTALSOC         PIC X(05).                      033  005
           10 IPS080-LADRSAV1           PIC X(30).                      038  030
           10 IPS080-LADRSAV2           PIC X(30).                      068  030
           10 IPS080-LADRSAV3           PIC X(30).                      098  030
           10 IPS080-LADRSOC1           PIC X(30).                      128  030
           10 IPS080-LADRSOC2           PIC X(30).                      158  030
           10 IPS080-LADRSOC3           PIC X(30).                      188  030
           10 IPS080-LIBAN              PIC X(02).                      218  002
           10 IPS080-LIBMOIS            PIC X(09).                      220  009
           10 IPS080-LLIEUSAV           PIC X(20).                      229  020
           10 IPS080-LLIEUSOC           PIC X(20).                      249  020
           10 IPS080-VILLESAV           PIC X(29).                      269  029
           10 IPS080-VILLESOC           PIC X(29).                      298  029
           10 IPS080-HTSECT             PIC S9(06)V9(2) COMP-3.         327  005
           10 IPS080-TTCSECT            PIC S9(06)V9(2) COMP-3.         332  005
           10 IPS080-TVASECT            PIC S9(06)V9(2) COMP-3.         337  005
            05 FILLER                      PIC X(171).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS080-LONG           PIC S9(4)   COMP  VALUE +341.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS080-LONG           PIC S9(4) COMP-5  VALUE +341.           
                                                                                
      *}                                                                        
