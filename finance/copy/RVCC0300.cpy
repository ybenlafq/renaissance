      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCC0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCC0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0300.                                                            
           02  CC03-NSOCIETE             PIC X(0003).                           
           02  CC03-NLIEU                PIC X(0003).                           
           02  CC03-DCAISSE              PIC X(0008).                           
           02  CC03-NCAISSE              PIC X(0003).                           
           02  CC03-TYPTRANS             PIC X(0003).                           
           02  CC03-MOPAI                PIC X(0003).                           
           02  CC03-DTRAIT               PIC X(0008).                           
           02  CC03-MONTANT              PIC S9(13)V9(0002) COMP-3.             
           02  CC03-DSYST                PIC S9(13)         COMP-3.             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCC0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-NSOCIETE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-NSOCIETE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-NLIEU-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-NLIEU-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-DCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-DCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-NCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-NCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-TYPTRANS-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-TYPTRANS-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-MOPAI-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-MOPAI-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-DTRAIT-F             PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-DTRAIT-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-MONTANT-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC03-MONTANT-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC03-DSYST-F              PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
           02  CC03-DSYST-F              PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
