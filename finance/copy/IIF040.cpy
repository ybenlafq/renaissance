      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIF040 AU 22/06/1998  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,08,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIF040.                                                        
            05 NOMETAT-IIF040           PIC X(6) VALUE 'IIF040'.                
            05 RUPTURES-IIF040.                                                 
           10 IIF040-NSOCIETE           PIC X(03).                      007  003
           10 IIF040-CMODPAI            PIC X(03).                      010  003
           10 IIF040-NLIEU              PIC X(03).                      013  003
           10 IIF040-DOPER              PIC X(08).                      016  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIF040-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 IIF040-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIF040.                                                   
           10 IIF040-LLIEU              PIC X(20).                      026  020
           10 IIF040-NOMCLIENT          PIC X(15).                      046  015
           10 IIF040-REFINTERNE         PIC X(09).                      061  009
           10 IIF040-MTBRUT             PIC S9(09)V9(2) COMP-3.         070  006
           10 IIF040-MTCOM              PIC S9(07)V9(2) COMP-3.         076  005
           10 IIF040-MTNET              PIC S9(09)V9(2) COMP-3.         081  006
            05 FILLER                      PIC X(426).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIF040-LONG           PIC S9(4)   COMP  VALUE +086.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIF040-LONG           PIC S9(4) COMP-5  VALUE +086.           
                                                                                
      *}                                                                        
