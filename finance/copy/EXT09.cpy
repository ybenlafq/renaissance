      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * SDF: EXT09   EXT09                                                      
      *****************************************************************         
       01   EXT09I.                                                     00010000
           02 FILLER    PIC X(12).                                      00020000
      * DATE DU JOUR                                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00030000
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00040000
           02 FILLER    PIC X(4).                                       00070000
           02 MDATJOUI  PIC X(10).                                      00090000
      * HEURE                                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00100000
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00110000
           02 FILLER    PIC X(4).                                       00140000
           02 MTIMJOUI  PIC X(5).                                       00160000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00170000
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00180000
           02 FILLER    PIC X(4).                                       00210000
           02 MCETATI   PIC X(10).                                      00220000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MONL      COMP PIC S9(4).                                 00230000
      *--                                                                       
           02 MONL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MONF      PIC X.                                          00240000
           02 FILLER    PIC X(4).                                       00270000
           02 MONI      PIC X(3).                                       00280000
      * MESSAGE ERREUR                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00290000
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00300000
           02 FILLER    PIC X(4).                                       00330000
           02 MLIBERRI  PIC X(78).                                      00350000
      * CODE TRANSACTION                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00360000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00370000
           02 FILLER    PIC X(4).                                       00400000
           02 MCODTRAI  PIC X(4).                                       00420000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00430000
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00440000
           02 FILLER    PIC X(4).                                       00470000
           02 MZONCMDI  PIC X(15).                                      00480000
      * CICS DE TRAVAIL                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00490000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00500000
           02 FILLER    PIC X(4).                                       00530000
           02 MCICSI    PIC X(5).                                       00550000
      * NETNAME                                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00560000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00570000
           02 FILLER    PIC X(4).                                       00600000
           02 MNETNAMI  PIC X(8).                                       00620000
      * CODE TERMINAL                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00630000
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00640000
           02 FILLER    PIC X(4).                                       00670000
           02 MSCREENI  PIC X(5).                                       00690000
      *****************************************************************         
      * SDF: EXT09   EXT09                                                      
      *****************************************************************         
       01   EXT09O REDEFINES EXT09I.                                    00700000
           02 FILLER    PIC X(12).                                      00710000
      * DATE DU JOUR                                                            
           02 FILLER    PIC X(2).                                               
           02 MDATJOUA  PIC X.                                                  
           02 MDATJOUC  PIC X.                                          00730000
           02 MDATJOUP  PIC X.                                          00740000
           02 MDATJOUH  PIC X.                                          00750000
           02 MDATJOUV  PIC X.                                          00760000
           02 MDATJOUO  PIC X(10).                                      00780000
      * HEURE                                                                   
           02 FILLER    PIC X(2).                                               
           02 MTIMJOUA  PIC X.                                                  
           02 MTIMJOUC  PIC X.                                          00800000
           02 MTIMJOUP  PIC X.                                          00810000
           02 MTIMJOUH  PIC X.                                          00820000
           02 MTIMJOUV  PIC X.                                          00830000
           02 MTIMJOUO  PIC X(5).                                       00850000
           02 FILLER    PIC X(2).                                               
           02 MCETATA   PIC X.                                                  
           02 MCETATC   PIC X.                                          00870000
           02 MCETATP   PIC X.                                          00880000
           02 MCETATH   PIC X.                                          00890000
           02 MCETATV   PIC X.                                          00900000
           02 MCETATO   PIC X(10).                                      00910000
           02 FILLER    PIC X(2).                                               
           02 MONA      PIC X.                                                  
           02 MONC      PIC X.                                          00930000
           02 MONP      PIC X.                                          00940000
           02 MONH      PIC X.                                          00950000
           02 MONV      PIC X.                                          00960000
           02 MONO      PIC X(3).                                       00970000
      * MESSAGE ERREUR                                                          
           02 FILLER    PIC X(2).                                               
           02 MLIBERRA  PIC X.                                                  
           02 MLIBERRC  PIC X.                                          00990000
           02 MLIBERRP  PIC X.                                          01000000
           02 MLIBERRH  PIC X.                                          01010000
           02 MLIBERRV  PIC X.                                          01020000
           02 MLIBERRO  PIC X(78).                                      01040000
      * CODE TRANSACTION                                                        
           02 FILLER    PIC X(2).                                               
           02 MCODTRAA  PIC X.                                                  
           02 MCODTRAC  PIC X.                                          01060000
           02 MCODTRAP  PIC X.                                          01070000
           02 MCODTRAH  PIC X.                                          01080000
           02 MCODTRAV  PIC X.                                          01090000
           02 MCODTRAO  PIC X(4).                                       01110000
           02 FILLER    PIC X(2).                                               
           02 MZONCMDA  PIC X.                                                  
           02 MZONCMDC  PIC X.                                          01130000
           02 MZONCMDP  PIC X.                                          01140000
           02 MZONCMDH  PIC X.                                          01150000
           02 MZONCMDV  PIC X.                                          01160000
           02 MZONCMDO  PIC X(15).                                      01170000
      * CICS DE TRAVAIL                                                         
           02 FILLER    PIC X(2).                                               
           02 MCICSA    PIC X.                                                  
           02 MCICSC    PIC X.                                          01190000
           02 MCICSP    PIC X.                                          01200000
           02 MCICSH    PIC X.                                          01210000
           02 MCICSV    PIC X.                                          01220000
           02 MCICSO    PIC X(5).                                       01240000
      * NETNAME                                                                 
           02 FILLER    PIC X(2).                                               
           02 MNETNAMA  PIC X.                                                  
           02 MNETNAMC  PIC X.                                          01260000
           02 MNETNAMP  PIC X.                                          01270000
           02 MNETNAMH  PIC X.                                          01280000
           02 MNETNAMV  PIC X.                                          01290000
           02 MNETNAMO  PIC X(8).                                       01310000
      * CODE TERMINAL                                                           
           02 FILLER    PIC X(2).                                               
           02 MSCREENA  PIC X.                                                  
           02 MSCREENC  PIC X.                                          01330000
           02 MSCREENP  PIC X.                                          01340000
           02 MSCREENH  PIC X.                                          01350000
           02 MSCREENV  PIC X.                                          01360000
           02 MSCREENO  PIC X(5).                                       01380000
                                                                                
