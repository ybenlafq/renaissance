      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
       01  TS-FG28-LONG                PIC S9(5) COMP-3 VALUE +384.             
       01  TS-FG28-RECORD.                                                      
           05 TS-FG28-LIGNE            OCCURS 12.                               
              10 TS-FG28-COLONNE       OCCURS 2.                                
                 15 TS-FG28-DSOLDE     PIC X(8).                                
                 15 TS-FG28-DT-CR      PIC X.                                   
                 15 TS-FG28-MTSOLDE    PIC S9(11)V99 COMP-3.                    
                                                                                
