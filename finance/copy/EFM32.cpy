      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - entites modeles                                           00000020
      ***************************************************************** 00000030
       01   EFM32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
           02 LIGNEI OCCURS   12 TIMES .                                00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTIONL  COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MSELECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSELECTIONF  PIC X.                                     00000240
             03 FILLER  PIC X(2).                                       00000250
             03 MSELECTIONI  PIC X.                                     00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITEL    COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MNENTITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTITEF    PIC X.                                     00000280
             03 FILLER  PIC X(2).                                       00000290
             03 MNENTITEI    PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTITEL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLENTITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTITEF    PIC X.                                     00000320
             03 FILLER  PIC X(2).                                       00000330
             03 MLENTITEI    PIC X(20).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPCGL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MCPCGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPCGF  PIC X.                                          00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MCPCGI  PIC X(4).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPDSL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCPDSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPDSF  PIC X.                                          00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MCPDSI  PIC X(4).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANCCPDSL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MANCCPDSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MANCCPDSF    PIC X.                                     00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MANCCPDSI    PIC X(4).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPDRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCPDRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPDRF  PIC X.                                          00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MCPDRI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANCCPDRL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MANCCPDRL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MANCCPDRF    PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MANCCPDRI    PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEVISEL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDEVISEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEVISEF     PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MDEVISEI     PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MLIBERRI  PIC X(79).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * GCT - entites modeles                                           00000760
      ***************************************************************** 00000770
       01   EFM32O REDEFINES EFM32I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUO  PIC X(10).                                      00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MTIMJOUA  PIC X.                                          00000860
           02 MTIMJOUC  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MPAGEA    PIC X.                                          00000910
           02 MPAGEC    PIC X.                                          00000920
           02 MPAGEH    PIC X.                                          00000930
           02 MPAGEO    PIC Z9.                                         00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MPAGETOTA      PIC X.                                     00000960
           02 MPAGETOTC PIC X.                                          00000970
           02 MPAGETOTH PIC X.                                          00000980
           02 MPAGETOTO      PIC Z9.                                    00000990
           02 LIGNEO OCCURS   12 TIMES .                                00001000
             03 FILLER       PIC X(2).                                  00001010
             03 MSELECTIONA  PIC X.                                     00001020
             03 MSELECTIONC  PIC X.                                     00001030
             03 MSELECTIONH  PIC X.                                     00001040
             03 MSELECTIONO  PIC X.                                     00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MNENTITEA    PIC X.                                     00001070
             03 MNENTITEC    PIC X.                                     00001080
             03 MNENTITEH    PIC X.                                     00001090
             03 MNENTITEO    PIC X(5).                                  00001100
             03 FILLER       PIC X(2).                                  00001110
             03 MLENTITEA    PIC X.                                     00001120
             03 MLENTITEC    PIC X.                                     00001130
             03 MLENTITEH    PIC X.                                     00001140
             03 MLENTITEO    PIC X(20).                                 00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MCPCGA  PIC X.                                          00001170
             03 MCPCGC  PIC X.                                          00001180
             03 MCPCGH  PIC X.                                          00001190
             03 MCPCGO  PIC X(4).                                       00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MCPDSA  PIC X.                                          00001220
             03 MCPDSC  PIC X.                                          00001230
             03 MCPDSH  PIC X.                                          00001240
             03 MCPDSO  PIC X(4).                                       00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MANCCPDSA    PIC X.                                     00001270
             03 MANCCPDSC    PIC X.                                     00001280
             03 MANCCPDSH    PIC X.                                     00001290
             03 MANCCPDSO    PIC X(4).                                  00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MCPDRA  PIC X.                                          00001320
             03 MCPDRC  PIC X.                                          00001330
             03 MCPDRH  PIC X.                                          00001340
             03 MCPDRO  PIC X(4).                                       00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MANCCPDRA    PIC X.                                     00001370
             03 MANCCPDRC    PIC X.                                     00001380
             03 MANCCPDRH    PIC X.                                     00001390
             03 MANCCPDRO    PIC X(4).                                  00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MDEVISEA     PIC X.                                     00001420
             03 MDEVISEC     PIC X.                                     00001430
             03 MDEVISEH     PIC X.                                     00001440
             03 MDEVISEO     PIC X(3).                                  00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLIBERRA  PIC X.                                          00001470
           02 MLIBERRC  PIC X.                                          00001480
           02 MLIBERRH  PIC X.                                          00001490
           02 MLIBERRO  PIC X(79).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MCODTRAA  PIC X.                                          00001520
           02 MCODTRAC  PIC X.                                          00001530
           02 MCODTRAH  PIC X.                                          00001540
           02 MCODTRAO  PIC X(4).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCICSA    PIC X.                                          00001570
           02 MCICSC    PIC X.                                          00001580
           02 MCICSH    PIC X.                                          00001590
           02 MCICSO    PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSCREENA  PIC X.                                          00001620
           02 MSCREENC  PIC X.                                          00001630
           02 MSCREENH  PIC X.                                          00001640
           02 MSCREENO  PIC X(4).                                       00001650
                                                                                
