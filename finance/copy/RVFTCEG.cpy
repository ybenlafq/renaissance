      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTCEG LISTE DES CEGA                   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFTCEG.                                                             
           05  FTCEG-CTABLEG2    PIC X(15).                                     
           05  FTCEG-CTABLEG2-REDEF REDEFINES FTCEG-CTABLEG2.                   
               10  FTCEG-CEGA            PIC X(03).                             
           05  FTCEG-WTABLEG     PIC X(80).                                     
           05  FTCEG-WTABLEG-REDEF  REDEFINES FTCEG-WTABLEG.                    
               10  FTCEG-LIBCEGA         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFTCEG-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTCEG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTCEG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTCEG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTCEG-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
