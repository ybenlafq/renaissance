      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTCEN GCT SOCIETES CENTRALISEES        *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFTCEN.                                                             
           05  FTCEN-CTABLEG2    PIC X(15).                                     
           05  FTCEN-CTABLEG2-REDEF REDEFINES FTCEN-CTABLEG2.                   
               10  FTCEN-NSOC            PIC X(03).                             
           05  FTCEN-WTABLEG     PIC X(80).                                     
           05  FTCEN-WTABLEG-REDEF  REDEFINES FTCEN-WTABLEG.                    
               10  FTCEN-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFTCEN-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTCEN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTCEN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTCEN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTCEN-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
