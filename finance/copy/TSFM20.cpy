      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM20-LONG             PIC S9(4) COMP-3 VALUE +78.                 
       01  TS-FM20-RECORD.                                                      
           10 TS-FM20-NDEMANDE      PIC X(2).                                   
           10 TS-FM20-DCREATION     PIC X(10).                                  
           10 TS-FM20-CACID         PIC X(8).                                   
           10 TS-FM20-USER          PIC X(25).                                  
           10 TS-FM20-WMFICHE       PIC X.                                      
           10 TS-FM20-WPAPIER       PIC X.                                      
           10 TS-FM20-NETAB         PIC X(3).                                   
AM         10 TS-FM20-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM20-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM20-WTYPAUX       PIC X.                                      
           10 TS-FM20-NBANQUEMIN    PIC X(5).                                   
           10 TS-FM20-NBANQUEMAX    PIC X(5).                                   
           10 TS-FM20-CMETHODE      PIC X(3).                                   
           10 TS-FM20-WTOTAL        PIC X OCCURS 4.                             
           10 TS-FM20-WNTRI         PIC X OCCURS 4.                             
                                                                                
