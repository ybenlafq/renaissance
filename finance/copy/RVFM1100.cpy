      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM1100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM1100.                                                            
           02  FM11-CPCG                                                        
               PIC X(0004).                                                     
           02  FM11-LPCG                                                        
               PIC X(0020).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM11-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM11-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM11-LPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM11-LPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
