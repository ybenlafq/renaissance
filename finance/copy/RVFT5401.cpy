      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT5401   ECHEANCES CLIENTS                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT5401.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT5401.                                                            
      *}                                                                        
           02  FT54-NSOC                                                        
               PIC X(0005).                                                     
           02  FT54-NETAB                                                       
               PIC X(0003).                                                     
           02  FT54-NJRN                                                        
               PIC X(0003).                                                     
           02  FT54-NEXER                                                       
               PIC X(0004).                                                     
           02  FT54-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT54-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FT54-DECH                                                        
               PIC X(0008).                                                     
           02  FT54-MONTANT                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT54-MTDEVBASE                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT54-NTBENEFCV                                                   
               PIC X(0006).                                                     
           02  FT54-METHREG                                                     
               PIC X(0003).                                                     
           02  FT54-WSENS                                                       
               PIC X(0001).                                                     
           02  FT54-DREG                                                        
               PIC X(0008).                                                     
           02  FT54-NPIECEREG                                                   
               PIC X(0010).                                                     
           02  FT54-NJRNREG                                                     
               PIC X(0003).                                                     
           02  FT54-NEXERREG                                                    
               PIC X(0004).                                                     
           02  FT54-CBANQUE                                                     
               PIC X(0006).                                                     
           02  FT54-DMAJ                                                        
               PIC X(08).                                                       
           02  FT54-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT54-NAUX                                                        
               PIC X(03).                                                       
           02  FT54-NTIERSCV                                                    
               PIC X(06).                                                       
           02  FT54-LETTRAGE                                                    
               PIC X(10).                                                       
           02  FT54-DRELANCE                                                    
               PIC X(08).                                                       
           02  FT54-BAR                                                         
               PIC X(01).                                                       
           02  FT54-NBRELANCE                                                   
               PIC S9(2) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT5401                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT5401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT5401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-DECH-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-DECH-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-MTDEVBASE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-MTDEVBASE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NTBENEFCV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NTBENEFCV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-METHREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-METHREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-WSENS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-WSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-DREG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-DREG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NPIECEREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NPIECEREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NJRNREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NJRNREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NEXERREG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NEXERREG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-CBANQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-CBANQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-LETTRAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-LETTRAGE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-DRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-DRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-BAR-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT54-BAR-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT54-NBRELANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  FT54-NBRELANCE-F                                                 
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
