      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DES ANOMALIES POUR COMPTA FACTURATION GROUPE              
      *                                                                         
      *                                                                         
       01       FFGANO-DSECT.                                                   
           05   FFGANO-IDENT.                                                   
001          10 FFGANO-NSOCCOMPT         PIC X(03).                             
004          10 FFGANO-NEXERCICE         PIC X(04).                             
008          10 FFGANO-NPERIODE          PIC X(03).                             
011          10 FFGANO-DCOMPTA           PIC X(08).                             
019          10 FFGANO-NETAB             PIC X(03).                             
022          10 FFGANO-NSOCCOR           PIC X(03).                             
025          10 FFGANO-TSOCFACT          PIC X(01).                             
026          10 FFGANO-CCHRONO           PIC X(02).                             
028          10 FFGANO-NUMFACT           PIC X(07).                             
035          10 FFGANO-NLIGNE            PIC X(03).                             
           05   FFGANO-SUITE.                                                   
038          10 FFGANO-NSOCORIG          PIC X(03).                             
041          10 FFGANO-NLIEUORIG         PIC X(03).                             
044          10 FFGANO-NSOCDEST          PIC X(03).                             
047          10 FFGANO-NLIEUDEST         PIC X(03).                             
050          10 FFGANO-NATFACT           PIC X(05).                             
055          10 FFGANO-CVENT             PIC X(05).                             
060          10 FFGANO-NOECS             PIC X(05).                             
065          10 FFGANO-CRIT1             PIC X(05).                             
070          10 FFGANO-CRIT2             PIC X(05).                             
075          10 FFGANO-CRIT3             PIC X(05).                             
080          10 FFGANO-DPIECE            PIC X(08).                             
088          10 FFGANO-MONTANT           PIC S9(11)V99 COMP-3.                  
095          10 FFGANO-NJRN              PIC X(10).                             
105          10 FFGANO-SENS              PIC X(01).                             
106          10 FFGANO-CTAUXTVA          PIC X(05).                             
111          10 FFGANO-COMPTE            PIC X(06).                             
117          10 FFGANO-SECTION           PIC X(06).                             
123          10 FFGANO-RUBRIQUE          PIC X(06).                             
129          10 FFGANO-LIBELLE           PIC X(20).                             
149          10 FFGANO-CNOMPGRM          PIC X(06).                             
155          10 FFGANO-DECHEANC          PIC X(08).                             
163          10 FFGANO-CTYPPIECE         PIC X(03).                             
166          10 FFGANO-NTIERS            PIC X(06).                             
172          10 FFGANO-ERREUR            PIC 9(04).                             
176          10 FFGANO-CTYPERR           PIC X(01).                             
177          10 FFGANO-FILLER            PIC X(04).                             
->180 *                                                                         
      *                                                                         
                                                                                
