      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM9700                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9700                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM9700.                                                    00090001
           05  FM97-CINTERFACE                                          00091001
               PIC X(0005).                                             00092001
           05  FM97-CNATOPER                                            00093001
               PIC X(0005).                                             00094001
           05  FM97-CTYPOPER                                            00095001
               PIC X(0005).                                             00096001
           05  FM97-CTVA                                                00097001
               PIC X(0005).                                             00098001
           05  FM97-CGEO                                                00099001
               PIC X(0001).                                             00099101
           05  FM97-WGROUPE                                             00099201
               PIC X(0001).                                             00099301
           05  FM97-CPTSAP                                              00099601
               PIC X(0008).                                             00099701
           05  FM97-CSENS                                               00099804
               PIC X(0001).                                             00099904
           05  FM97-WCUMUL                                              00100004
               PIC X(0001).                                             00100104
           05  FM97-NLETTRAGE                                           00100204
               PIC X(0001).                                             00100304
           05  FM97-NTIERSSAP                                           00100401
               PIC X(0001).                                             00100501
           05  FM97-CCGS                                                00100601
               PIC X(0001).                                             00100701
           05  FM97-WANAL                                               00101001
               PIC X(0001).                                             00101101
           05  FM97-TYPMT                                               00101204
               PIC X(0002).                                             00101306
           05  FM97-CONTREPSAP                                          00101501
               PIC X(0008).                                             00101603
           05  FM97-WCUMULC                                             00101904
               PIC X(0001).                                             00102004
           05  FM97-NLETTRAGEC                                          00102104
               PIC X(0001).                                             00102204
           05  FM97-NTIERSSAPC                                          00102304
               PIC X(0001).                                             00102404
           05  FM97-CCGSC                                               00102501
               PIC X(0001).                                             00102601
           05  FM97-WANALC                                              00102901
               PIC X(0001).                                             00103001
           05  FM97-TYPMTC                                              00103204
               PIC X(0002).                                             00103306
           05  FM97-DEFFET                                              00103401
               PIC X(0008).                                             00103502
           05  FM97-DMAJ                                                00103601
               PIC X(0008).                                             00103701
           05  FM97-DSYST                                               00103801
               PIC S9(0013) COMP-3.                                     00104001
      *                                                                 00500000
      *                                                                 00510001
      *---------------------------------------------------------        00520001
      *   LISTE DES FLAGS DE LA TABLE RVFM9900                          00530001
      *---------------------------------------------------------        00540001
      *                                                                 00550001
       01  RVFM9700-FLAGS.                                              00560001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CINTERFACE-F                                        00570001
      *        PIC S9(4) COMP.                                          00571001
      *--                                                                       
           05  FM97-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CNATOPER-F                                          00572001
      *        PIC S9(4) COMP.                                          00573001
      *--                                                                       
           05  FM97-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CTYPOPER-F                                          00574001
      *        PIC S9(4) COMP.                                          00575001
      *--                                                                       
           05  FM97-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CTVA-F                                              00576001
      *        PIC S9(4) COMP.                                          00577001
      *--                                                                       
           05  FM97-CTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CGEO-F                                              00578001
      *        PIC S9(4) COMP.                                          00579001
      *--                                                                       
           05  FM97-CGEO-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WGROUPE-F                                           00579101
      *        PIC S9(4) COMP.                                          00579201
      *--                                                                       
           05  FM97-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CPTSAP-F                                            00579501
      *        PIC S9(4) COMP.                                          00579601
      *--                                                                       
           05  FM97-CPTSAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CSENS-F                                             00579704
      *        PIC S9(4) COMP.                                          00579804
      *--                                                                       
           05  FM97-CSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WCUMUL-F                                            00579904
      *        PIC S9(4) COMP.                                          00580004
      *--                                                                       
           05  FM97-WCUMUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NLETTRAGE-F                                         00580104
      *        PIC S9(4) COMP.                                          00580204
      *--                                                                       
           05  FM97-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NTIERSSAP-F                                         00580301
      *        PIC S9(4) COMP.                                          00580401
      *--                                                                       
           05  FM97-NTIERSSAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CCGS-F                                              00580501
      *        PIC S9(4) COMP.                                          00580601
      *--                                                                       
           05  FM97-CCGS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WANAL-F                                             00581001
      *        PIC S9(4) COMP.                                          00582001
      *--                                                                       
           05  FM97-WANAL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-TYPMT-F                                             00582104
      *        PIC S9(4) COMP.                                          00582204
      *--                                                                       
           05  FM97-TYPMT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CONTREPSAP-F                                        00589201
      *        PIC S9(4) COMP.                                          00589301
      *--                                                                       
           05  FM97-CONTREPSAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WCUMULC-F                                           00589404
      *        PIC S9(4) COMP.                                          00589504
      *--                                                                       
           05  FM97-WCUMULC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NLETTRAGEC-F                                        00589604
      *        PIC S9(4) COMP.                                          00589704
      *--                                                                       
           05  FM97-NLETTRAGEC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-NTIERSSAPC-F                                        00589801
      *        PIC S9(4) COMP.                                          00589901
      *--                                                                       
           05  FM97-NTIERSSAPC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-CCGSC-F                                             00590001
      *        PIC S9(4) COMP.                                          00590101
      *--                                                                       
           05  FM97-CCGSC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-WANALC-F                                            00590401
      *        PIC S9(4) COMP.                                          00590501
      *--                                                                       
           05  FM97-WANALC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-TYPMTC-F                                            00595104
      *        PIC S9(4) COMP.                                          00595204
      *--                                                                       
           05  FM97-TYPMTC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DEFFET-F                                            00596001
      *        PIC S9(4) COMP.                                          00597001
      *--                                                                       
           05  FM97-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DMAJ-F                                              00598001
      *        PIC S9(4) COMP.                                          00599001
      *--                                                                       
           05  FM97-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FM97-DSYST-F                                             00599101
      *        PIC S9(4) COMP.                                          00600001
      *--                                                                       
           05  FM97-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                            00960000
                                                                                
