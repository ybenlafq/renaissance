      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu FICHE DE COMMISSION                                        00000020
      ***************************************************************** 00000030
       01   EGC51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMARQI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLMARQI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPTXPL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPTXPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPTXPF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPTXPI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCTYPTXPL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLCTYPTXPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCTYPTXPF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLCTYPTXPI     PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPPRESTL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCTYPPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPPRESTF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTYPPRESTI    PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTPRESTL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLTPRESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTPRESTF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLTPRESTI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCND2L     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTYPCND2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPCND2F     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTYPCND2I     PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLE2L     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLIBELLE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELLE2F     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBELLE2I     PIC X(26).                                 00000530
           02 MTABI OCCURS   11 TIMES .                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MXI     PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCODEI  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODELL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCODELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODELF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCODELI      PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTAUXL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MTAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTAUXF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MTAUXI  PIC X(9).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDEFFETI     PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFETL  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDFINEFFETL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDFINEFFETF  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDFINEFFETI  PIC X(8).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * menu FICHE DE COMMISSION                                        00001040
      ***************************************************************** 00001050
       01   EGC51O REDEFINES EGC51I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEMAXA      PIC X.                                     00001300
           02 MPAGEMAXC PIC X.                                          00001310
           02 MPAGEMAXP PIC X.                                          00001320
           02 MPAGEMAXH PIC X.                                          00001330
           02 MPAGEMAXV PIC X.                                          00001340
           02 MPAGEMAXO      PIC X(3).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCMARQA   PIC X.                                          00001370
           02 MCMARQC   PIC X.                                          00001380
           02 MCMARQP   PIC X.                                          00001390
           02 MCMARQH   PIC X.                                          00001400
           02 MCMARQV   PIC X.                                          00001410
           02 MCMARQO   PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLMARQA   PIC X.                                          00001440
           02 MLMARQC   PIC X.                                          00001450
           02 MLMARQP   PIC X.                                          00001460
           02 MLMARQH   PIC X.                                          00001470
           02 MLMARQV   PIC X.                                          00001480
           02 MLMARQO   PIC X(20).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCTYPTXPA      PIC X.                                     00001510
           02 MCTYPTXPC PIC X.                                          00001520
           02 MCTYPTXPP PIC X.                                          00001530
           02 MCTYPTXPH PIC X.                                          00001540
           02 MCTYPTXPV PIC X.                                          00001550
           02 MCTYPTXPO      PIC X(5).                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLCTYPTXPA     PIC X.                                     00001580
           02 MLCTYPTXPC     PIC X.                                     00001590
           02 MLCTYPTXPP     PIC X.                                     00001600
           02 MLCTYPTXPH     PIC X.                                     00001610
           02 MLCTYPTXPV     PIC X.                                     00001620
           02 MLCTYPTXPO     PIC X(20).                                 00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCTYPPRESTA    PIC X.                                     00001650
           02 MCTYPPRESTC    PIC X.                                     00001660
           02 MCTYPPRESTP    PIC X.                                     00001670
           02 MCTYPPRESTH    PIC X.                                     00001680
           02 MCTYPPRESTV    PIC X.                                     00001690
           02 MCTYPPRESTO    PIC X(5).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLTPRESTA      PIC X.                                     00001720
           02 MLTPRESTC PIC X.                                          00001730
           02 MLTPRESTP PIC X.                                          00001740
           02 MLTPRESTH PIC X.                                          00001750
           02 MLTPRESTV PIC X.                                          00001760
           02 MLTPRESTO      PIC X(20).                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCTYPCND2A     PIC X.                                     00001790
           02 MCTYPCND2C     PIC X.                                     00001800
           02 MCTYPCND2P     PIC X.                                     00001810
           02 MCTYPCND2H     PIC X.                                     00001820
           02 MCTYPCND2V     PIC X.                                     00001830
           02 MCTYPCND2O     PIC X(5).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLIBELLE2A     PIC X.                                     00001860
           02 MLIBELLE2C     PIC X.                                     00001870
           02 MLIBELLE2P     PIC X.                                     00001880
           02 MLIBELLE2H     PIC X.                                     00001890
           02 MLIBELLE2V     PIC X.                                     00001900
           02 MLIBELLE2O     PIC X(26).                                 00001910
           02 MTABO OCCURS   11 TIMES .                                 00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MXA     PIC X.                                          00001940
             03 MXC     PIC X.                                          00001950
             03 MXP     PIC X.                                          00001960
             03 MXH     PIC X.                                          00001970
             03 MXV     PIC X.                                          00001980
             03 MXO     PIC X.                                          00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCODEA  PIC X.                                          00002010
             03 MCODEC  PIC X.                                          00002020
             03 MCODEP  PIC X.                                          00002030
             03 MCODEH  PIC X.                                          00002040
             03 MCODEV  PIC X.                                          00002050
             03 MCODEO  PIC X(3).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MCODELA      PIC X.                                     00002080
             03 MCODELC PIC X.                                          00002090
             03 MCODELP PIC X.                                          00002100
             03 MCODELH PIC X.                                          00002110
             03 MCODELV PIC X.                                          00002120
             03 MCODELO      PIC X(3).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MTAUXA  PIC X.                                          00002150
             03 MTAUXC  PIC X.                                          00002160
             03 MTAUXP  PIC X.                                          00002170
             03 MTAUXH  PIC X.                                          00002180
             03 MTAUXV  PIC X.                                          00002190
             03 MTAUXO  PIC -----9,99.                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MDEFFETA     PIC X.                                     00002220
             03 MDEFFETC     PIC X.                                     00002230
             03 MDEFFETP     PIC X.                                     00002240
             03 MDEFFETH     PIC X.                                     00002250
             03 MDEFFETV     PIC X.                                     00002260
             03 MDEFFETO     PIC X(8).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MDFINEFFETA  PIC X.                                     00002290
             03 MDFINEFFETC  PIC X.                                     00002300
             03 MDFINEFFETP  PIC X.                                     00002310
             03 MDFINEFFETH  PIC X.                                     00002320
             03 MDFINEFFETV  PIC X.                                     00002330
             03 MDFINEFFETO  PIC X(8).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
