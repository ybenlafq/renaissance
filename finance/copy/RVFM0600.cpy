      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM0600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0600.                                                            
           02  FM06-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM06-CETAT                                                       
               PIC X(0010).                                                     
           02  FM06-NDEMANDE                                                    
               PIC X(0002).                                                     
           02  FM06-WMFICHE                                                     
               PIC X(0001).                                                     
           02  FM06-WPAPIER                                                     
               PIC X(0001).                                                     
           02  FM06-NETAB                                                       
               PIC X(0003).                                                     
           02  FM06-WTRIETAB                                                    
               PIC X(0001).                                                     
           02  FM06-WNTRIETAB                                                   
               PIC X(0001).                                                     
           02  FM06-NAUXMIN                                                     
               PIC X(0003).                                                     
           02  FM06-NAUXMAX                                                     
               PIC X(0003).                                                     
           02  FM06-WTYPAUX                                                     
               PIC X(0001).                                                     
           02  FM06-WTRIAUX                                                     
               PIC X(0001).                                                     
           02  FM06-WNTRIAUX                                                    
               PIC X(0001).                                                     
           02  FM06-NTIERSMIN                                                   
               PIC X(0008).                                                     
           02  FM06-NTIERSMAX                                                   
               PIC X(0008).                                                     
           02  FM06-WTRITIERS                                                   
               PIC X(0001).                                                     
           02  FM06-WNTRITIERS                                                  
               PIC X(0001).                                                     
           02  FM06-CMETHODE                                                    
               PIC X(0003).                                                     
           02  FM06-WTRIMETHODE                                                 
               PIC X(0001).                                                     
           02  FM06-WNTRIMETHODE                                                
               PIC X(0001).                                                     
           02  FM06-NCOMPTEMIN                                                  
               PIC X(0006).                                                     
           02  FM06-NCOMPTEMAX                                                  
               PIC X(0006).                                                     
           02  FM06-WTRICOMPTE                                                  
               PIC X(0001).                                                     
           02  FM06-WNTRICOMPTE                                                 
               PIC X(0001).                                                     
           02  FM06-NCPTEAUXMIN                                                 
               PIC X(0006).                                                     
           02  FM06-NCPTEAUXMAX                                                 
               PIC X(0006).                                                     
           02  FM06-WTRICPTEAUX                                                 
               PIC X(0001).                                                     
           02  FM06-WNTRICPTEAUX                                                
               PIC X(0001).                                                     
           02  FM06-NSECTIONMIN                                                 
               PIC X(0006).                                                     
           02  FM06-NSECTIONMAX                                                 
               PIC X(0006).                                                     
           02  FM06-WTRISECTION                                                 
               PIC X(0001).                                                     
           02  FM06-WNTRISECTION                                                
               PIC X(0001).                                                     
           02  FM06-NRUBRIQUEMIN                                                
               PIC X(0006).                                                     
           02  FM06-NRUBRIQUEMAX                                                
               PIC X(0006).                                                     
           02  FM06-WTRIRUBRIQUE                                                
               PIC X(0001).                                                     
           02  FM06-WNTRIRUBRIQUE                                               
               PIC X(0001).                                                     
           02  FM06-NBANQUEMIN                                                  
               PIC X(0005).                                                     
           02  FM06-NBANQUEMAX                                                  
               PIC X(0005).                                                     
           02  FM06-WTRIBANQUE                                                  
               PIC X(0001).                                                     
           02  FM06-WNTRIBANQUE                                                 
               PIC X(0001).                                                     
           02  FM06-DECHEANCEMIN                                                
               PIC X(0008).                                                     
           02  FM06-DECHEANCEMAX                                                
               PIC X(0008).                                                     
           02  FM06-WTRIECHEANCE                                                
               PIC X(0001).                                                     
           02  FM06-WNTRIECHEANCE                                               
               PIC X(0001).                                                     
           02  FM06-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FM06-WPTOTDEVISE                                                 
               PIC X(0001).                                                     
           02  FM06-WTTRIECRIT                                                  
               PIC X(0001).                                                     
           02  FM06-WTTRITIERS                                                  
               PIC X(0001).                                                     
           02  FM06-WEDTTIERS                                                   
               PIC X(0001).                                                     
           02  FM06-WTSELTIERS                                                  
               PIC X(0001).                                                     
           02  FM06-WCOMPTESOLDE                                                
               PIC X(0001).                                                     
           02  FM06-WPIECELETTRE                                                
               PIC X(0001).                                                     
           02  FM06-CNAT1                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT2                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT3                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT4                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT5                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT6                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT7                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT8                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT9                                                       
               PIC X(0003).                                                     
           02  FM06-CNAT10                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE1                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE2                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE3                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE4                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE5                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE6                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE7                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE8                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE9                                                      
               PIC X(0003).                                                     
           02  FM06-CNATE10                                                     
               PIC X(0003).                                                     
           02  FM06-NJOURNALMIN                                                 
               PIC X(0003).                                                     
           02  FM06-NJOURNALMAX                                                 
               PIC X(0003).                                                     
           02  FM06-WJOURNALGAL                                                 
               PIC X(0001).                                                     
           02  FM06-DCREATION                                                   
               PIC X(0008).                                                     
           02  FM06-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM06-CACID                                                       
               PIC X(0008).                                                     
           02  FM06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM0600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WMFICHE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WMFICHE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WPAPIER-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WPAPIER-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIETAB-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIETAB-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIETAB-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIETAB-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NAUXMIN-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NAUXMIN-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NAUXMAX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NAUXMAX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTYPAUX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTYPAUX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIAUX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIAUX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIAUX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIAUX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NTIERSMIN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NTIERSMIN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NTIERSMAX-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NTIERSMAX-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRITIERS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRITIERS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRITIERS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRITIERS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CMETHODE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CMETHODE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIMETHODE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIMETHODE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIMETHODE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIMETHODE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NCOMPTEMIN-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NCOMPTEMIN-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NCOMPTEMAX-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NCOMPTEMAX-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRICOMPTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRICOMPTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRICOMPTE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRICOMPTE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NCPTEAUXMIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NCPTEAUXMIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NCPTEAUXMAX-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NCPTEAUXMAX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRICPTEAUX-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRICPTEAUX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRICPTEAUX-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRICPTEAUX-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NSECTIONMIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NSECTIONMIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NSECTIONMAX-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NSECTIONMAX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRISECTION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRISECTION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRISECTION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRISECTION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NRUBRIQUEMIN-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NRUBRIQUEMIN-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NRUBRIQUEMAX-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NRUBRIQUEMAX-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIRUBRIQUE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIRUBRIQUE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIRUBRIQUE-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIRUBRIQUE-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NBANQUEMIN-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NBANQUEMIN-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NBANQUEMAX-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NBANQUEMAX-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIBANQUE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIBANQUE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIBANQUE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIBANQUE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-DECHEANCEMIN-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-DECHEANCEMIN-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-DECHEANCEMAX-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-DECHEANCEMAX-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTRIECHEANCE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTRIECHEANCE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WNTRIECHEANCE-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WNTRIECHEANCE-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WPTOTDEVISE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WPTOTDEVISE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTTRIECRIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTTRIECRIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTTRITIERS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTTRITIERS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WEDTTIERS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WEDTTIERS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WTSELTIERS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WTSELTIERS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WCOMPTESOLDE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WCOMPTESOLDE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WPIECELETTRE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WPIECELETTRE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT4-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT4-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT5-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT5-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT6-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT6-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT7-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT7-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT8-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT8-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT9-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT9-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNAT10-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNAT10-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE7-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE7-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE8-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE8-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE9-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE9-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CNATE10-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CNATE10-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NJOURNALMIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NJOURNALMIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-NJOURNALMAX-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-NJOURNALMAX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-WJOURNALGAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-WJOURNALGAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
