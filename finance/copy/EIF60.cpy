      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INTERRO SUIVI DES REJETS                                        00000020
      ***************************************************************** 00000030
       01   EIF60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNPAGEI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MNSOCIETEI     PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MNLIEUI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MLLIEUI   PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCTAL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MCTYPCTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPCTAF      PIC X.                                     00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MCTYPCTAI      PIC X(3).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGANL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCORGANF  PIC X.                                          00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MCORGANI  PIC X(3).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTDATEL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MTDATEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTDATEF   PIC X.                                          00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MTDATEI   PIC X.                                          00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNIDENTL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MDNIDENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDNIDENTF      PIC X.                                     00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MDNIDENTI      PIC X.                                     00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORGANL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLORGANF  PIC X.                                          00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MLORGANI  PIC X(6).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIDENTL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIDENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIDENTF  PIC X.                                          00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MLIDENTI  PIC X(11).                                      00000590
           02 FILLER  OCCURS   10 TIMES .                               00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORGANTL    COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MCORGANTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCORGANTF    PIC X.                                     00000620
             03 FILLER  PIC X(2).                                       00000630
             03 MCORGANTI    PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODPAIL    COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MCMODPAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCMODPAIF    PIC X.                                     00000660
             03 FILLER  PIC X(2).                                       00000670
             03 MCMODPAII    PIC X(5).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNJRNL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MNJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNJRNF  PIC X.                                          00000700
             03 FILLER  PIC X(2).                                       00000710
             03 MNJRNI  PIC X(3).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIDENTL     COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MNIDENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNIDENTF     PIC X.                                     00000740
             03 FILLER  PIC X(2).                                       00000750
             03 MNIDENTI     PIC X(15).                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTRAITL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MDTRAITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDTRAITF     PIC X.                                     00000780
             03 FILLER  PIC X(2).                                       00000790
             03 MDTRAITI     PIC X(10).                                 00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINANCEL   COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MDFINANCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDFINANCEF   PIC X.                                     00000820
             03 FILLER  PIC X(2).                                       00000830
             03 MDFINANCEI   PIC X(10).                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDREMISEL    COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDREMISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDREMISEF    PIC X.                                     00000860
             03 FILLER  PIC X(2).                                       00000870
             03 MDREMISEI    PIC X(10).                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREMISEL    COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MNREMISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNREMISEF    PIC X.                                     00000900
             03 FILLER  PIC X(2).                                       00000910
             03 MNREMISEI    PIC X(6).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBMVTSL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MNBMVTSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNBMVTSF     PIC X.                                     00000940
             03 FILLER  PIC X(2).                                       00000950
             03 MNBMVTSI     PIC X(5).                                  00000960
      * MESSAGE ERREUR                                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MLIBERRI  PIC X(79).                                      00001010
      * CODE TRANSACTION                                                00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      * ZONE CMD AIDA                                                   00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MZONCMDI  PIC X(15).                                      00001110
      * CICS DE TRAVAIL                                                 00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MCICSI    PIC X(5).                                       00001160
      * NETNAME                                                         00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      * CODE TERMINAL                                                   00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MSCREENI  PIC X(5).                                       00001260
      ***************************************************************** 00001270
      * INTERRO SUIVI DES REJETS                                        00001280
      ***************************************************************** 00001290
       01   EIF60O REDEFINES EIF60I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
      * DATE DU JOUR                                                    00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDATJOUA  PIC X.                                          00001340
           02 MDATJOUC  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUO  PIC X(10).                                      00001370
      * HEURE                                                           00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUH  PIC X.                                          00001420
           02 MTIMJOUO  PIC X(5).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNPAGEA   PIC X.                                          00001450
           02 MNPAGEC   PIC X.                                          00001460
           02 MNPAGEH   PIC X.                                          00001470
           02 MNPAGEO   PIC ZZ.                                         00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNBPAGESA      PIC X.                                     00001500
           02 MNBPAGESC PIC X.                                          00001510
           02 MNBPAGESH PIC X.                                          00001520
           02 MNBPAGESO      PIC ZZ.                                    00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNSOCIETEA     PIC X.                                     00001550
           02 MNSOCIETEC     PIC X.                                     00001560
           02 MNSOCIETEH     PIC X.                                     00001570
           02 MNSOCIETEO     PIC X(3).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNLIEUA   PIC X.                                          00001600
           02 MNLIEUC   PIC X.                                          00001610
           02 MNLIEUH   PIC X.                                          00001620
           02 MNLIEUO   PIC X(3).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLLIEUA   PIC X.                                          00001650
           02 MLLIEUC   PIC X.                                          00001660
           02 MLLIEUH   PIC X.                                          00001670
           02 MLLIEUO   PIC X(25).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCTYPCTAA      PIC X.                                     00001700
           02 MCTYPCTAC PIC X.                                          00001710
           02 MCTYPCTAH PIC X.                                          00001720
           02 MCTYPCTAO      PIC X(3).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCORGANA  PIC X.                                          00001750
           02 MCORGANC  PIC X.                                          00001760
           02 MCORGANH  PIC X.                                          00001770
           02 MCORGANO  PIC X(3).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MTDATEA   PIC X.                                          00001800
           02 MTDATEC   PIC X.                                          00001810
           02 MTDATEH   PIC X.                                          00001820
           02 MTDATEO   PIC X.                                          00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDNIDENTA      PIC X.                                     00001850
           02 MDNIDENTC PIC X.                                          00001860
           02 MDNIDENTH PIC X.                                          00001870
           02 MDNIDENTO      PIC X.                                     00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLORGANA  PIC X.                                          00001900
           02 MLORGANC  PIC X.                                          00001910
           02 MLORGANH  PIC X.                                          00001920
           02 MLORGANO  PIC X(6).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIDENTA  PIC X.                                          00001950
           02 MLIDENTC  PIC X.                                          00001960
           02 MLIDENTH  PIC X.                                          00001970
           02 MLIDENTO  PIC X(11).                                      00001980
           02 FILLER  OCCURS   10 TIMES .                               00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCORGANTA    PIC X.                                     00002010
             03 MCORGANTC    PIC X.                                     00002020
             03 MCORGANTH    PIC X.                                     00002030
             03 MCORGANTO    PIC X(3).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCMODPAIA    PIC X.                                     00002060
             03 MCMODPAIC    PIC X.                                     00002070
             03 MCMODPAIH    PIC X.                                     00002080
             03 MCMODPAIO    PIC X(5).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MNJRNA  PIC X.                                          00002110
             03 MNJRNC  PIC X.                                          00002120
             03 MNJRNH  PIC X.                                          00002130
             03 MNJRNO  PIC X(3).                                       00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MNIDENTA     PIC X.                                     00002160
             03 MNIDENTC     PIC X.                                     00002170
             03 MNIDENTH     PIC X.                                     00002180
             03 MNIDENTO     PIC X(15).                                 00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MDTRAITA     PIC X.                                     00002210
             03 MDTRAITC     PIC X.                                     00002220
             03 MDTRAITH     PIC X.                                     00002230
             03 MDTRAITO     PIC X(10).                                 00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MDFINANCEA   PIC X.                                     00002260
             03 MDFINANCEC   PIC X.                                     00002270
             03 MDFINANCEH   PIC X.                                     00002280
             03 MDFINANCEO   PIC X(10).                                 00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MDREMISEA    PIC X.                                     00002310
             03 MDREMISEC    PIC X.                                     00002320
             03 MDREMISEH    PIC X.                                     00002330
             03 MDREMISEO    PIC X(10).                                 00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MNREMISEA    PIC X.                                     00002360
             03 MNREMISEC    PIC X.                                     00002370
             03 MNREMISEH    PIC X.                                     00002380
             03 MNREMISEO    PIC X(6).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MNBMVTSA     PIC X.                                     00002410
             03 MNBMVTSC     PIC X.                                     00002420
             03 MNBMVTSH     PIC X.                                     00002430
             03 MNBMVTSO     PIC ZZZZZ.                                 00002440
      * MESSAGE ERREUR                                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLIBERRA  PIC X.                                          00002470
           02 MLIBERRC  PIC X.                                          00002480
           02 MLIBERRH  PIC X.                                          00002490
           02 MLIBERRO  PIC X(79).                                      00002500
      * CODE TRANSACTION                                                00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MCODTRAA  PIC X.                                          00002530
           02 MCODTRAC  PIC X.                                          00002540
           02 MCODTRAH  PIC X.                                          00002550
           02 MCODTRAO  PIC X(4).                                       00002560
      * ZONE CMD AIDA                                                   00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MZONCMDA  PIC X.                                          00002590
           02 MZONCMDC  PIC X.                                          00002600
           02 MZONCMDH  PIC X.                                          00002610
           02 MZONCMDO  PIC X(15).                                      00002620
      * CICS DE TRAVAIL                                                 00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCICSA    PIC X.                                          00002650
           02 MCICSC    PIC X.                                          00002660
           02 MCICSH    PIC X.                                          00002670
           02 MCICSO    PIC X(5).                                       00002680
      * NETNAME                                                         00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MNETNAMA  PIC X.                                          00002710
           02 MNETNAMC  PIC X.                                          00002720
           02 MNETNAMH  PIC X.                                          00002730
           02 MNETNAMO  PIC X(8).                                       00002740
      * CODE TERMINAL                                                   00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MSCREENA  PIC X.                                          00002770
           02 MSCREENC  PIC X.                                          00002780
           02 MSCREENH  PIC X.                                          00002790
           02 MSCREENO  PIC X(5).                                       00002800
                                                                                
