      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SIBMQ CODE FONCTION MQ / TYPE VTE      *        
      *----------------------------------------------------------------*        
       01  RVSIBMQ .                                                            
           05  SIBMQ-CTABLEG2    PIC X(15).                                     
           05  SIBMQ-CTABLEG2-REDEF REDEFINES SIBMQ-CTABLEG2.                   
               10  SIBMQ-TYPVTE          PIC X(05).                             
           05  SIBMQ-WTABLEG     PIC X(80).                                     
           05  SIBMQ-WTABLEG-REDEF  REDEFINES SIBMQ-WTABLEG.                    
               10  SIBMQ-WACTIF          PIC X(01).                             
               10  SIBMQ-CFONC           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSIBMQ-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SIBMQ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SIBMQ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SIBMQ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SIBMQ-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
