      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCC0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCC0100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCC0100.                                                            
           02  CC01-NSOCIETE             PIC X(0003).                           
           02  CC01-NLIEU                PIC X(0003).                           
           02  CC01-DCAISSE              PIC X(0008).                           
           02  CC01-NCAISSE              PIC X(0003).                           
           02  CC01-DTRAIT               PIC X(0008).                           
           02  CC01-DVALID               PIC X(0008).                           
           02  CC01-DSAISIE              PIC X(0008).                           
           02  CC01-DSYST                PIC S9(13) COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCC0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-NSOCIETE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-NSOCIETE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-NLIEU-F              PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-NLIEU-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-DCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-DCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-NCAISSE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-NCAISSE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-DTRAIT-F             PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-DTRAIT-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-DVALID-F             PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-DVALID-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-DSAISIE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02  CC01-DSAISIE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC01-DSYST-F              PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
           02  CC01-DSYST-F              PIC S9(4) COMP-5.                      
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
