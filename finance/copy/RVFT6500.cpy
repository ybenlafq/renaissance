      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT6500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT6500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6500.                                                            
      *}                                                                        
           02  FT65-NENTITE                                                     
               PIC X(0005).                                                     
           02  FT65-CMASQUEC                                                    
               PIC X(0006).                                                     
           02  FT65-CMASQUES                                                    
               PIC X(0006).                                                     
           02  FT65-CMASQUER                                                    
               PIC X(0006).                                                     
           02  FT65-CMASQUEE                                                    
               PIC X(0003).                                                     
           02  FT65-CMASQUEA                                                    
               PIC X(0005).                                                     
           02  FT65-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FT65-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT6500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-CMASQUEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-CMASQUEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-CMASQUES-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-CMASQUES-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-CMASQUER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-CMASQUER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-CMASQUEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-CMASQUEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-CMASQUEA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-CMASQUEA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT65-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT65-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
