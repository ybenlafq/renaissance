      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS040 AU 19/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS040.                                                        
            05 NOMETAT-IPS040           PIC X(6) VALUE 'IPS040'.                
            05 RUPTURES-IPS040.                                                 
           10 IPS040-CGRP               PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS040-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 IPS040-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS040.                                                   
           10 IPS040-CDEVISE            PIC X(06).                      014  006
           10 IPS040-LBGRP              PIC X(20).                      020  020
           10 IPS040-NSOCIETE           PIC X(03).                      040  003
           10 IPS040-MTDJFC             PIC S9(10)V9(2) COMP-3.         043  007
           10 IPS040-MTFACM             PIC S9(08)V9(2) COMP-3.         050  006
           10 IPS040-MTRAFC             PIC S9(10)V9(2) COMP-3.         056  007
           10 IPS040-MTTC               PIC S9(08)V9(2) COMP-3.         063  006
           10 IPS040-MOIS               PIC X(08).                      069  008
            05 FILLER                      PIC X(436).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS040-LONG           PIC S9(4)   COMP  VALUE +076.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS040-LONG           PIC S9(4) COMP-5  VALUE +076.           
                                                                                
      *}                                                                        
