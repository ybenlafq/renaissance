      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
       01  RVCC0500.                                                            
      *                       NSOC                                              
           10 CC05-NSOC            PIC X(5).                                    
      *                       DJOUR                                             
           10 CC05-DJOUR           PIC X(8).                                    
      *                       NSEQ                                              
           10 CC05-NSEQ            PIC X(4).                                    
      *                       NLIGNE                                            
           10 CC05-NLIGNE          PIC X(2).                                    
      *                       NSECTION                                          
           10 CC05-NSECTION        PIC X(6).                                    
      *                       NLETTRAGE                                         
           10 CC05-NLETTRAGE       PIC X(10).                                   
      *                       LMVT                                              
           10 CC05-LMVT            PIC X(10).                                   
      *                       MONTANT                                           
           10 CC05-MONTANT         PIC S9(11)V9(2) USAGE COMP-3.                
      *                       SENS                                              
           10 CC05-SENS            PIC X(1).                                    
      *                       DMVT                                              
           10 CC05-DMVT            PIC X(8).                                    
      *                       DDOC                                              
           10 CC05-DDOC            PIC X(8).                                    
      *                       CDEVISE                                           
           10 CC05-CDEVISE         PIC X(3).                                    
      *                       CPTSAP                                            
           10 CC05-CPTSAP          PIC X(8).                                    
      *                       CPROFIT                                           
           10 CC05-CPROFIT         PIC X(10).                                   
      *                       DTRAIT                                            
           10 CC05-DTRAIT          PIC X(8).                                    
      *                       CAISSE                                            
           10 CC05-CAISSE          PIC X(3).                                    
      *                       DCAISSE                                           
           10 CC05-DCAISSE         PIC X(8).                                    
      *                       LIEU                                              
           10 CC05-LIEU            PIC X(3).                                    
      *                       CPAITCPT                                          
           10 CC05-CPAITCPT        PIC X(3).                                    
      *                       DSYST                                             
           10 CC05-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
