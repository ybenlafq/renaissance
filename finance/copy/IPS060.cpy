      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS060 AU 19/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS060.                                                        
            05 NOMETAT-IPS060           PIC X(6) VALUE 'IPS060'.                
            05 RUPTURES-IPS060.                                                 
           10 IPS060-CGRP               PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS060-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 IPS060-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS060.                                                   
           10 IPS060-LBGRP              PIC X(20).                      014  020
           10 IPS060-NSOC               PIC X(03).                      034  003
           10 IPS060-ANCSCT             PIC S9(10)V9(2) COMP-3.         037  007
           10 IPS060-ANCSLT             PIC S9(10)V9(2) COMP-3.         044  007
           10 IPS060-CAMOIS             PIC S9(08)V9(2) COMP-3.         051  006
           10 IPS060-NVSCT              PIC S9(10)V9(2) COMP-3.         057  007
           10 IPS060-NVSLT              PIC S9(10)V9(2) COMP-3.         064  007
           10 IPS060-PASSCT             PIC S9(08)V9(2) COMP-3.         071  006
           10 IPS060-PASSLT             PIC S9(08)V9(2) COMP-3.         077  006
           10 IPS060-PSECHU             PIC S9(08)V9(2) COMP-3.         083  006
           10 IPS060-REMBCT             PIC S9(06)V9(2) COMP-3.         089  005
           10 IPS060-REMBLT             PIC S9(06)V9(2) COMP-3.         094  005
           10 IPS060-DPERCPT1           PIC X(06).                      099  006
           10 IPS060-DPERCPT2           PIC X(06).                      105  006
            05 FILLER                      PIC X(402).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS060-LONG           PIC S9(4)   COMP  VALUE +110.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS060-LONG           PIC S9(4) COMP-5  VALUE +110.           
                                                                                
      *}                                                                        
