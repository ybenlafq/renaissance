      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVSG0301                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSG0301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSG0301.                                                            
      *}                                                                        
      *                       NSOC                                              
           10 SG03-NSOC            PIC X(3).                                    
      *                       NETAB                                             
           10 SG03-NETAB           PIC X(3).                                    
      *                       NETABADM                                          
           10 SG03-NETABADM        PIC X(3).                                    
      *                       COMPTE                                            
           10 SG03-COMPTE          PIC X(6).                                    
      *                       NTIERS                                            
           10 SG03-NTIERS          PIC X(8).                                    
      *                       DSYST                                             
           10 SG03-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       NTIERSSAP                                         
           10 SG03-NTIERSSAP       PIC X(10).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
                                                                                
