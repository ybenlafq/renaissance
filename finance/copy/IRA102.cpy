      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA102 AU 17/03/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA102.                                                        
            05 NOMETAT-IRA102           PIC X(6) VALUE 'IRA102'.                
            05 RUPTURES-IRA102.                                                 
           10 IRA102-NLIEU              PIC X(03).                      007  003
           10 IRA102-NSOCIETE           PIC X(03).                      010  003
           10 IRA102-NDEMANDE           PIC X(05).                      013  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA102-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IRA102-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA102.                                                   
           10 IRA102-BANQUE             PIC X(20).                      020  020
           10 IRA102-LNOM               PIC X(25).                      040  025
           10 IRA102-LPRENOM            PIC X(15).                      065  015
           10 IRA102-MOTIF              PIC X(20).                      080  020
           10 IRA102-NAVOIRCLIENT       PIC X(06).                      100  006
           10 IRA102-MTREMBTOT          PIC 9(09)V9(2).                 106  011
           10 IRA102-DATEPARAM          PIC X(08).                      117  008
           10 IRA102-DAVOIR             PIC X(08).                      125  008
           10 IRA102-DECHEANCE          PIC X(08).                      133  008
            05 FILLER                      PIC X(372).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRA102-LONG           PIC S9(4)   COMP  VALUE +140.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRA102-LONG           PIC S9(4) COMP-5  VALUE +140.           
                                                                                
      *}                                                                        
