      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFG10 (MENU)             TR: FG10  *    00002200
      *                                                            *    00002900
      *           POUR L'ADMINISTATION DES DONNEES                 *    00003000
      **************************************************************    00003100
      * MODIF AP DSA005 - LE 01/02/2000                                         
      * AUGMENTER NOMBRE DE CRITERES PAR NATURES                                
      * --> PASSE DE 50 � 55                                                    
      * C'EST LA LIMITE : APRES, IL FAUT CREER UNE NOUVELLE NATURE *            
      * SI PLUS DE CRITERES                                                     
      **************************************************************    00003100
      * MODIF MB DSA044 - LE 27/04/2000                                         
      * RAJOUT DES ZONES RELATIVES A LA SAISIE DU TIERS                         
      *     ET DU LETTRAGE (FG10)                                               
      **************************************************************    00003100
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00003200
      **************************************************************    00003300
      *                                                                 00003400
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00003500
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00003600
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00003700
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00003800
      *                                                                 00003900
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00004000
      * COMPRENANT :                                                    00004100
      * 1 - LES ZONES RESERVEES A AIDA                                  00004200
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00004300
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00004400
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00004500
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00004600
      *                                                                 00004700
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00004800
      * PAR AIDA                                                        00004900
      *                                                                 00005000
      *-------------------------------------------------------------    00005100
      *                                                                 00005200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-FG10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00005300
      *--                                                                       
       01  COM-FG10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00005400
       01  Z-COMMAREA.                                                  00005500
      *                                                                 00005600
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00005700
          02 FILLER-COM-AIDA                  PIC X(100).               00005800
      *                                                                 00005900
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00006000
          02 COMM-CICS-APPLID                 PIC X(8).                 00006100
          02 COMM-CICS-NETNAM                 PIC X(8).                 00006200
          02 COMM-CICS-TRANSA                 PIC X(4).                 00006300
      *                                                                 00006400
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00006500
          02 COMM-DATE-SIECLE                 PIC XX.                   00006600
          02 COMM-DATE-ANNEE                  PIC XX.                   00006700
          02 COMM-DATE-MOIS                   PIC XX.                   00006800
          02 COMM-DATE-JOUR                   PIC XX.                   00006900
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00007000
          02 COMM-DATE-QNTA                   PIC 999.                  00007100
          02 COMM-DATE-QNT0                   PIC 99999.                00007200
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00007300
          02 COMM-DATE-BISX                   PIC 9.                    00007400
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00007500
          02 COMM-DATE-JSM                    PIC 9.                    00007600
      *   LIBELLES DU JOUR COURT - LONG                                 00007700
          02 COMM-DATE-JSM-LC                 PIC XXX.                  00007800
          02 COMM-DATE-JSM-LL                 PIC XXXXXXXX.             00007900
      *   LIBELLES DU MOIS COURT - LONG                                 00008000
          02 COMM-DATE-MOIS-LC                PIC XXX.                  00008100
          02 COMM-DATE-MOIS-LL                PIC XXXXXXXX.             00008200
      *   DIFFERENTES FORMES DE DATE                                    00008300
          02 COMM-DATE-SSAAMMJJ               PIC X(8).                 00008400
          02 COMM-DATE-AAMMJJ                 PIC X(6).                 00008500
          02 COMM-DATE-JJMMSSAA               PIC X(8).                 00008600
          02 COMM-DATE-JJMMAA                 PIC X(6).                 00008700
          02 COMM-DATE-JJ-MM-AA               PIC X(8).                 00008800
          02 COMM-DATE-JJ-MM-SSAA             PIC X(10).                00008900
      *   DIFFERENTES FORMES DE DATE                                    00009000
          02 COMM-DATE-SEMSS                  PIC X(02).                00009100
          02 COMM-DATE-SEMAA                  PIC X(02).                00009100
          02 COMM-DATE-SEMNU                  PIC X(02).                00009100
          02 COMM-DATE-FILLER                 PIC X(08).                00009100
      *                                                                 00009200
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00009300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS                   PIC S9(4) COMP VALUE -1.  00009400
      *--                                                                       
          02 COMM-SWAP-CURS                   PIC S9(4) COMP-5 VALUE -1.        
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150        PIC X(1).                 00009500
      *                                                                 00009600
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00010000
      *                                                                 00410100
      *            TRANSACTION FG10 : ADMINISTRATION DES DONNEES      * 00411000
      *                                                                         
3724      02 COMM-FG10-APPLI.                                           00420000
      *                                                                 00412000
      *                                                                 00412000
      * DONNEES OBTENUES A L'INIT TRANSACTION                           00412000
             03 COMM-FG10-NIVEAU-SUP.                                           
                05 COMM-FG10-NSOCCOMPT-ACID   PIC X(03).                        
                05 COMM-FG10-NLIEUCOMPT-ACID  PIC X(03).                        
                05 COMM-FG10-LSOCCOMPT-ACID   PIC X(20).                        
                05 COMM-FG10-NSOC-ACID        PIC X(03).                        
                05 COMM-FG10-NLIEU-ACID       PIC X(03).                        
                05 COMM-FG10-LLIEU-ACID       PIC X(20).                        
                05 COMM-FG10-SERVICE-ACID     PIC X(05).                        
                05 COMM-FG10-LSERVICE-ACID    PIC X(20).                        
                05 COMM-FG10-NSOCFISC-ACID    PIC X(03).                        
                05 COMM-FG10-WETAB-ACID       PIC X(01).                        
                05 COMM-FG10-MULTI-LIEUX      PIC X(01).                        
                05 COMM-FG10-AUT-EMET         PIC X(01).                        
                05 COMM-FG10-AUT-CONS         PIC X(01).                        
                05 COMM-FG10-TYPE-FACTURE     PIC X(02).                        
                05 COMM-FG10-TYPE-AVOIR       PIC X(02).                        
                05 COMM-FG10-CHRONO-FACTURE   PIC X(02).                        
                05 COMM-FG10-CHRONO-AVOIR     PIC X(02).                        
                05 COMM-FG10-GROUPE           PIC X(01).                        
                05 COMM-FG10-SGP              PIC X(01).                        
                05 COMM-FG10-DCTA1-INI        PIC X(08).                        
0795            05 COMM-FG10-DCTA2-INI        PIC X(08).                        
      *                                                                         
                05 COMM-FG10-TABLE-TVA.                                         
                   07 COMM-FG10-LIGNE-TVA    OCCURS 5.                          
                      09 COMM-FG10-CTVA       PIC X(05).                        
                      09 COMM-FG10-DTVA       PIC X(08).                        
                      09 COMM-FG10-TTVA       PIC S9(3)V9(2) COMP-3.            
                      09 COMM-FG10-ANC-TVA    PIC S9(3)V9(2) COMP-3.            
                05 COMM-FG10-CODE-TAUX        PIC X(05).                        
      *                                                                         
      *                                                                         
             03 COMM-FG10-NIVEAU-SOC.                                           
                05 COMM-FG10-NSOCCOMPT        PIC X(03).                        
                05 COMM-FG10-NLIEUCOMPT       PIC X(03).                        
                05 COMM-FG10-LSOCCOMPT        PIC X(20).                        
                05 COMM-FG10-SERVICE          PIC X(05).                        
                05 COMM-FG10-LSERVICE         PIC X(20).                        
                05 COMM-FG10-NSOCFISC         PIC X(03).                        
                05 COMM-FG10-WETAB            PIC X(01).                        
                05 COMM-FG10-LIBERR           PIC X(58).                        
      *                                                                         
             03 COMM-FG10-NIVEAU-INF.                                           
                05 COMM-FG10-NSOC             PIC X(03).                        
                05 COMM-FG10-NLIEU            PIC X(03).                        
                05 COMM-FG10-LLIEU            PIC X(20).                        
                05 COMM-FG10-FONCTION         PIC X(03).                        
                05 COMM-FG10-CTYPE            PIC X(02).                        
                05 COMM-FG10-LTYPE            PIC X(25).                        
                05 COMM-FG10-NATURE           PIC X(05).                        
                05 COMM-FG10-LNATURE          PIC X(30).                        
                05 COMM-FG10-VENTIL           PIC X(01).                        
                05 COMM-FG10-NUMFACT          PIC X(07).                        
                05 COMM-FG10-CODE-ECHEAN      PIC X(01).                        
                05 COMM-FG10-JOUR-ECHEAN      PIC X(02).                        
                05 COMM-FG10-FREQ             PIC X(02).                        
                05 COMM-FG10-OPTION           PIC X(01).                        
                05 COMM-FG10-WAVOIR           PIC X(01).                        
                05 COMM-FG10-NSOCC-DEST       PIC X(03).                        
                05 COMM-FG10-NSOCDEST         PIC X(03).                        
                05 COMM-FG10-NLIEUDEST        PIC X(03).                        
                05 COMM-FG10-LLIEUDEST        PIC X(25).                        
                05 COMM-FG10-NUMORIG          PIC X(07).                        
                05 COMM-FG10-DORIG            PIC X(08).                        
                05 COMM-FG10-DPIECE           PIC X(08).                        
                05 COMM-FG10-DDEBUT           PIC X(08).                        
                05 COMM-FG10-CODE-BATCH       PIC X(01).                        
                05 COMM-FG10-CCHRONO          PIC X(02).                        
                05 COMM-FG10-CIMP             PIC X(04).                        
                05 COMM-FG10-AUTORISE-NAT     PIC X(01).                        
                05 COMM-FG10-IMP-PTC          PIC X(01).                        
                05 COMM-FG10-DCTA1            PIC X(08).                        
                05 COMM-FG10-DCTA2            PIC X(08).                        
                05 COMM-FG10-SOCCOR           PIC X(03).                        
-178            05 COMM-FG10-LSOCCOR          PIC X(20).                        
MB              05 COMM-FG10-CTIERS           PIC X(03).                        
MB              05 COMM-FG10-CLETTRAGE        PIC X(02).                        
AM0595       03 COMM-FG10-CACID               PIC X(08).                        
AP0597       03 COMM-FG10-DEST-ETAT           PIC X(03).                        
0795         03 COMM-FG10-FILLER              PIC X(66).                        
      *                                                                         
      *------------------------------ ZONE COMMUNE AUX SOUS-PROGRAMMES  00510000
             03 COMM-FG10-SSPRGS              PIC X(3172).              00520000
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE TFG12             00510000
             03 COMM-FG12-SSPRG-12       REDEFINES COMM-FG10-SSPRGS.    00520000
                05 COMM-FG12-PAGEMAX          PIC 9(3).                         
                05 COMM-FG12-MAX-TS           PIC 9(3).                         
                05 COMM-FG12-ITEM             PIC 9(3).                         
                05 COMM-FG12-LIGNE            PIC 9(2).                         
                05 COMM-FG12-MODE             PIC X(01).                        
                   88 COMM-FG12-MODE-MAJ           VALUE '0'.                   
                   88 COMM-FG12-MODE-CRE           VALUE '1'.                   
                05 COMM-FG12-MESSAGE          PIC X(01).                        
                05 COMM-FG12-SENSCD           PIC X(01).                        
                05 COMM-FG12-FLAG-TYPE.                                         
                   07 COMM-FG12-FLAG-INTERNE  PIC X(01).                        
                   07 COMM-FG12-FLAG-EXTERNE  PIC X(01).                        
                   07 COMM-FG12-FLAG-TVA      PIC X(01).                        
                   07 COMM-FG12-FLAG-TESC     PIC X(01).                        
                   07 COMM-FG12-FLAG-FAC      PIC X(01).                        
                   07 COMM-FG12-FLAG-REG      PIC X(01).                        
                05 COMM-FG12-NSOCCRE          PIC X(03).                        
      *         05 COMM-FG12-FILLER           PIC X(3077).                      
MB              05 COMM-FG12-FILLER           PIC X(3149).                      
      *------------------------------ ZONE SPECIFIQUE TFG13             00510000
             03 COMM-FG13-SSPRG-13       REDEFINES COMM-FG10-SSPRGS.    00520000
                05 COMM-FG13-PAGEMAX          PIC 9(3).                         
                05 COMM-FG13-ITEM             PIC 9(3).                         
                05 COMM-FG13-LIGNE            PIC 9(2).                         
                05 COMM-FG13-RETOUR-FG11      PIC X(01).                        
      *         05 COMM-FG13-FILLER           PIC X(3091).                      
      * MODIF AD - LE 13/09/2004                                                
MB  AD*         05 COMM-FG13-FILLER           PIC X(3163).                      
    AD          05 COMM-FG13-COUNT            PIC S9(4) COMP-3.                 
    AD          05 COMM-FG13-FILLER           PIC X(3160).                      
                05 COMM-FG11-SSPRG-11    REDEFINES COMM-FG13-FILLER.            
                   07 COMM-FG11-RETOUR.                                         
                      09 COMM-FG11-T          PIC X(01).                        
                      09 COMM-FG11-TACHE      PIC X(04).                        
                   07 COMM-FG11-PF20          PIC X(01).                        
                   07 COMM-FG11-TOUCHE-SUITE  PIC X(04).                        
                   07 COMM-FG11-DETAIL        PIC X(58).                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            07 COMM-FG11-LIGNE-PF12    PIC S9(4) COMP.                   
      *--                                                                       
                   07 COMM-FG11-LIGNE-PF12    PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            07 COMM-FG11-ITEM-FG14     PIC S9(4) COMP.                   
      *--                                                                       
                   07 COMM-FG11-ITEM-FG14     PIC S9(4) COMP-5.                 
      *}                                                                        
                   07 COMM-FG11-MONT-SAISIS   PIC X(01).                        
                   07 COMM-FG11-MODE-CRE      PIC X(01).                        
                   07 COMM-FG11-NB-CRITERES   PIC 9(02).                        
                   07 COMM-FG11-NUMFACT       PIC X(07).                        
                   07 COMM-FG11-EXERCICE      PIC X(04).                        
                   07 COMM-FG11-NPERIODE      PIC X(03).                        
                   07 COMM-FG11-LPERIODE      PIC X(20).                        
                   07 COMM-FG11-DPIECE        PIC X(08).                        
                   07 COMM-FG11-DECHEAN       PIC X(08).                        
                   07 COMM-FG11-DCOMPTA       PIC X(08).                        
                   07 COMM-FG11-NUMORIG-I     PIC X(07).                        
                   07 COMM-FG11-DORIG-I       PIC X(08).                        
                   07 COMM-FG11-NUMORIG-F     PIC X(07).                        
                   07 COMM-FG11-DORIG-F       PIC X(08).                        
                   07 COMM-FG11-TESC-I        PIC S9(3)V9(2) COMP-3.            
                   07 COMM-FG11-TESC-F        PIC S9(3)V9(2) COMP-3.            
      *                                                                         
                   07 COMM-FG11-TAB-CRITERES  OCCURS 55.                        
                      09 COMM-FG11-CRITERE    PIC X(05).                        
                      09 COMM-FG11-DESIGN     PIC X(25).                        
                      09 COMM-FG11-CRIT-PRIM  PIC X(01).                        
                      09 COMM-FG11-CRIT-SERV  PIC X(05).                        
      *                                                                         
                   07 COMM-FG11-PAGEMAX       PIC 9(03).                        
                   07 COMM-FG11-MAX-TS        PIC 9(03).                        
                   07 COMM-FG11-ITEM          PIC 9(03).                        
                   07 COMM-FG11-LIGNE         PIC 9(02).                        
                   07 COMM-FG11-TOTAL-TTC     PIC S9(13)V9(2) COMP-3.           
                   07 COMM-FG11-TOTAL-HT      PIC S9(13)V9(2) COMP-3.           
                   07 COMM-FG11-TOTAL-TVA     PIC S9(13)V9(2) COMP-3.           
                   07 COMM-FG11-TOTAL-ESC     PIC S9(13)V9(2) COMP-3.           
                   07 COMM-FG11-SENSCD        PIC X(01).                        
                   07 COMM-FG11-FLAG-TYPE.                                      
                      09 COMM-FG11-FLAG-INTERNE  PIC X(01).                     
                      09 COMM-FG11-FLAG-EXTERNE  PIC X(01).                     
                      09 COMM-FG11-FLAG-TVA      PIC X(01).                     
                      09 COMM-FG11-FLAG-TESC     PIC X(01).                     
                      09 COMM-FG11-FLAG-FAC      PIC X(01).                     
                      09 COMM-FG11-FLAG-REG      PIC X(01).                     
      *                                                                         
                   07 COMM-FG11-NSOCCOMPT      PIC X(03).                       
                   07 COMM-FG11-LSOCCOMPT      PIC X(20).                       
                   07 COMM-FG11-SERVICE        PIC X(05).                       
                   07 COMM-FG11-LSERVICE       PIC X(20).                       
      *                                                                         
                   07 COMM-FG11-DMVT           PIC X(08).                       
                   07 COMM-FG11-MESSAGE        PIC X(01).                       
                   07 COMM-FG11-TYPNAT         PIC X(32).                       
                   07 COMM-FG11-OK-PF20        PIC X(01).                       
                   07 COMM-FG11-SUBDIV-SERV    PIC X(1).                        
                   07 COMM-FG11-SERVDEST       PIC X(5).                        
                   07 COMM-FG11-LSERVDEST      PIC X(20).                       
                   07 COMM-FG11-ZONE-DISPO    PIC X(60).                        
AM                 07 COMM-FG11-DECHEAN-F     PIC X(08).                        
0795               07 COMM-FG11-VIREMENT      PIC X(01).                        
0795               07 COMM-FG11-DVALEUR       PIC X(08).                        
0795               07 COMM-FG11-CONFIRM-DVAL  PIC X(01).                        
AM                 07 COMM-FG11-DREF-ECH      PIC X(08).                        
OA                 07 COMM-FG11-SECORIG       PIC X(06).                        
0398               07 COMM-FG11-SECDEST       PIC X(06).                        
MB                 07 COMM-FG11-NTIERSE-I     PIC X(8).                         
MB                 07 COMM-FG11-NTIERSR-I     PIC X(8).                         
MB                 07 COMM-FG11-NLETTRE-I     PIC X(10).                        
MB                 07 COMM-FG11-NLETTRR-I     PIC X(10).                        
MB                 07 COMM-FG11-NTIERSE-F     PIC X(8).                         
MB                 07 COMM-FG11-NTIERSR-F     PIC X(8).                         
MB                 07 COMM-FG11-NLETTRE-F     PIC X(10).                        
MB                 07 COMM-FG11-NLETTRR-F     PIC X(10).                        
OAAD  *            07 COMM-FG11-FILLER        PIC X(677).                       
AD                 07 COMM-FG11-FILLER        PIC X(674).                       
                   07 COMM-FG14-SSPRG-14    REDEFINES COMM-FG11-FILLER.         
                      09 COMM-FG14-TAB-LIGNE    OCCURS 8.                       
                         11 COMM-FG14-ECRIT-I     PIC X(01).                    
                         11 COMM-FG14-ECRIT-F     PIC X(01).                    
                         11 COMM-FG14-MODIF       PIC X(01).                    
                         11 COMM-FG14-NSEQ        PIC X(02).                    
                         11 COMM-FG14-COMMENT     PIC X(70).                    
                      09 COMM-FG14-LIGNES-SAISIES PIC X(01).                    
                      09 COMM-FG14-TOUCHE-PF5     PIC X(01).                    
                      09 COMM-FG14-PAGEMAX        PIC 9(03).                    
                      09 COMM-FG14-MESSAGE        PIC X(01).                    
AD    *               09 COMM-FG14-FILLER         PIC X(71).                    
AD                    09 COMM-FG14-FILLER         PIC X(68).                    
      ***************************************************************** 00740000
                                                                                
