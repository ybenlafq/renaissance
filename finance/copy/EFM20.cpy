      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - ECHEANCIER TRESORERIE MOD                                 00000020
      ***************************************************************** 00000030
       01   EFM20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNENTITEI      PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLENTITEI      PIC X(34).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMANDEL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDEMANDEF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNDEMANDEI     PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDCREATIONI    PIC X(10).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSERL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MUSERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MUSERF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MUSERI    PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMFICHEL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MWMFICHEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMFICHEF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MWMFICHEI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAPIERL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MWPAPIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPAPIERF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MWPAPIERI      PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETABI   PIC X(3).                                       00000470
           02 MWNTRID OCCURS   4 TIMES .                                00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWNTRIL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MWNTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWNTRIF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MWNTRII      PIC X.                                     00000520
           02 MWTOTALD OCCURS   4 TIMES .                               00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOTALL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MWTOTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTOTALF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MWTOTALI     PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPAUXL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MWTYPAUXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPAUXF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWTYPAUXI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMINL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNAUXMINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMINF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNAUXMINI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMAXL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNAUXMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMAXF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNAUXMAXI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMETHODEL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCMETHODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMETHODEF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCMETHODEI     PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBANQMINL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNBANQMINL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBANQMINF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNBANQMINI     PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBANQMAXL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNBANQMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBANQMAXF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNBANQMAXI     PIC X(5).                                  00000810
      * ZONE CMD AIDA                                                   00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      * CODE TRANSACTION                                                00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCODTRAI  PIC X(4).                                       00000910
      * CICS DE TRAVAIL                                                 00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MCICSI    PIC X(5).                                       00000960
      * NETNAME                                                         00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNETNAMI  PIC X(8).                                       00001010
      * CODE TERMINAL                                                   00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(5).                                       00001060
      ***************************************************************** 00001070
      * GCT - ECHEANCIER TRESORERIE MOD                                 00001080
      ***************************************************************** 00001090
       01   EFM20O REDEFINES EFM20I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
      * DATE DU JOUR                                                    00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUP  PIC X.                                          00001160
           02 MDATJOUH  PIC X.                                          00001170
           02 MDATJOUV  PIC X.                                          00001180
           02 MDATJOUO  PIC X(10).                                      00001190
      * HEURE                                                           00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MTIMJOUA  PIC X.                                          00001220
           02 MTIMJOUC  PIC X.                                          00001230
           02 MTIMJOUP  PIC X.                                          00001240
           02 MTIMJOUH  PIC X.                                          00001250
           02 MTIMJOUV  PIC X.                                          00001260
           02 MTIMJOUO  PIC X(5).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNENTITEA      PIC X.                                     00001290
           02 MNENTITEC PIC X.                                          00001300
           02 MNENTITEP PIC X.                                          00001310
           02 MNENTITEH PIC X.                                          00001320
           02 MNENTITEV PIC X.                                          00001330
           02 MNENTITEO      PIC X(5).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLENTITEA      PIC X.                                     00001360
           02 MLENTITEC PIC X.                                          00001370
           02 MLENTITEP PIC X.                                          00001380
           02 MLENTITEH PIC X.                                          00001390
           02 MLENTITEV PIC X.                                          00001400
           02 MLENTITEO      PIC X(34).                                 00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNDEMANDEA     PIC X.                                     00001430
           02 MNDEMANDEC     PIC X.                                     00001440
           02 MNDEMANDEP     PIC X.                                     00001450
           02 MNDEMANDEH     PIC X.                                     00001460
           02 MNDEMANDEV     PIC X.                                     00001470
           02 MNDEMANDEO     PIC X(2).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDCREATIONA    PIC X.                                     00001500
           02 MDCREATIONC    PIC X.                                     00001510
           02 MDCREATIONP    PIC X.                                     00001520
           02 MDCREATIONH    PIC X.                                     00001530
           02 MDCREATIONV    PIC X.                                     00001540
           02 MDCREATIONO    PIC X(10).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MUSERA    PIC X.                                          00001570
           02 MUSERC    PIC X.                                          00001580
           02 MUSERP    PIC X.                                          00001590
           02 MUSERH    PIC X.                                          00001600
           02 MUSERV    PIC X.                                          00001610
           02 MUSERO    PIC X(25).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MWMFICHEA      PIC X.                                     00001640
           02 MWMFICHEC PIC X.                                          00001650
           02 MWMFICHEP PIC X.                                          00001660
           02 MWMFICHEH PIC X.                                          00001670
           02 MWMFICHEV PIC X.                                          00001680
           02 MWMFICHEO      PIC X.                                     00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MWPAPIERA      PIC X.                                     00001710
           02 MWPAPIERC PIC X.                                          00001720
           02 MWPAPIERP PIC X.                                          00001730
           02 MWPAPIERH PIC X.                                          00001740
           02 MWPAPIERV PIC X.                                          00001750
           02 MWPAPIERO      PIC X.                                     00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNETABA   PIC X.                                          00001780
           02 MNETABC   PIC X.                                          00001790
           02 MNETABP   PIC X.                                          00001800
           02 MNETABH   PIC X.                                          00001810
           02 MNETABV   PIC X.                                          00001820
           02 MNETABO   PIC X(3).                                       00001830
           02 DFHMS1 OCCURS   4 TIMES .                                 00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MWNTRIA      PIC X.                                     00001860
             03 MWNTRIC PIC X.                                          00001870
             03 MWNTRIP PIC X.                                          00001880
             03 MWNTRIH PIC X.                                          00001890
             03 MWNTRIV PIC X.                                          00001900
             03 MWNTRIO      PIC X.                                     00001910
           02 DFHMS2 OCCURS   4 TIMES .                                 00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MWTOTALA     PIC X.                                     00001940
             03 MWTOTALC     PIC X.                                     00001950
             03 MWTOTALP     PIC X.                                     00001960
             03 MWTOTALH     PIC X.                                     00001970
             03 MWTOTALV     PIC X.                                     00001980
             03 MWTOTALO     PIC X.                                     00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MWTYPAUXA      PIC X.                                     00002010
           02 MWTYPAUXC PIC X.                                          00002020
           02 MWTYPAUXP PIC X.                                          00002030
           02 MWTYPAUXH PIC X.                                          00002040
           02 MWTYPAUXV PIC X.                                          00002050
           02 MWTYPAUXO      PIC X.                                     00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNAUXMINA      PIC X.                                     00002080
           02 MNAUXMINC PIC X.                                          00002090
           02 MNAUXMINP PIC X.                                          00002100
           02 MNAUXMINH PIC X.                                          00002110
           02 MNAUXMINV PIC X.                                          00002120
           02 MNAUXMINO      PIC X(3).                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNAUXMAXA      PIC X.                                     00002150
           02 MNAUXMAXC PIC X.                                          00002160
           02 MNAUXMAXP PIC X.                                          00002170
           02 MNAUXMAXH PIC X.                                          00002180
           02 MNAUXMAXV PIC X.                                          00002190
           02 MNAUXMAXO      PIC X(3).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCMETHODEA     PIC X.                                     00002220
           02 MCMETHODEC     PIC X.                                     00002230
           02 MCMETHODEP     PIC X.                                     00002240
           02 MCMETHODEH     PIC X.                                     00002250
           02 MCMETHODEV     PIC X.                                     00002260
           02 MCMETHODEO     PIC X(3).                                  00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNBANQMINA     PIC X.                                     00002290
           02 MNBANQMINC     PIC X.                                     00002300
           02 MNBANQMINP     PIC X.                                     00002310
           02 MNBANQMINH     PIC X.                                     00002320
           02 MNBANQMINV     PIC X.                                     00002330
           02 MNBANQMINO     PIC X(5).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MNBANQMAXA     PIC X.                                     00002360
           02 MNBANQMAXC     PIC X.                                     00002370
           02 MNBANQMAXP     PIC X.                                     00002380
           02 MNBANQMAXH     PIC X.                                     00002390
           02 MNBANQMAXV     PIC X.                                     00002400
           02 MNBANQMAXO     PIC X(5).                                  00002410
      * ZONE CMD AIDA                                                   00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLIBERRA  PIC X.                                          00002440
           02 MLIBERRC  PIC X.                                          00002450
           02 MLIBERRP  PIC X.                                          00002460
           02 MLIBERRH  PIC X.                                          00002470
           02 MLIBERRV  PIC X.                                          00002480
           02 MLIBERRO  PIC X(78).                                      00002490
      * CODE TRANSACTION                                                00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MCODTRAA  PIC X.                                          00002520
           02 MCODTRAC  PIC X.                                          00002530
           02 MCODTRAP  PIC X.                                          00002540
           02 MCODTRAH  PIC X.                                          00002550
           02 MCODTRAV  PIC X.                                          00002560
           02 MCODTRAO  PIC X(4).                                       00002570
      * CICS DE TRAVAIL                                                 00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MCICSA    PIC X.                                          00002600
           02 MCICSC    PIC X.                                          00002610
           02 MCICSP    PIC X.                                          00002620
           02 MCICSH    PIC X.                                          00002630
           02 MCICSV    PIC X.                                          00002640
           02 MCICSO    PIC X(5).                                       00002650
      * NETNAME                                                         00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MNETNAMA  PIC X.                                          00002680
           02 MNETNAMC  PIC X.                                          00002690
           02 MNETNAMP  PIC X.                                          00002700
           02 MNETNAMH  PIC X.                                          00002710
           02 MNETNAMV  PIC X.                                          00002720
           02 MNETNAMO  PIC X(8).                                       00002730
      * CODE TERMINAL                                                   00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MSCREENA  PIC X.                                          00002760
           02 MSCREENC  PIC X.                                          00002770
           02 MSCREENP  PIC X.                                          00002780
           02 MSCREENH  PIC X.                                          00002790
           02 MSCREENV  PIC X.                                          00002800
           02 MSCREENO  PIC X(5).                                       00002810
                                                                                
