      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTSAP PARAMETRAGE EXTRACTION GCT       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFTSAP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFTSAP.                                                             
      *}                                                                        
           05  FTSAP-CTABLEG2    PIC X(15).                                     
           05  FTSAP-CTABLEG2-REDEF REDEFINES FTSAP-CTABLEG2.                   
               10  FTSAP-CAT             PIC X(03).                             
               10  FTSAP-CC1             PIC X(08).                             
               10  FTSAP-CC2             PIC X(03).                             
           05  FTSAP-WTABLEG     PIC X(80).                                     
           05  FTSAP-WTABLEG-REDEF  REDEFINES FTSAP-WTABLEG.                    
               10  FTSAP-WFLAG           PIC X(01).                             
               10  FTSAP-VALEUR          PIC X(49).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFTSAP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFTSAP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTSAP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTSAP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTSAP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTSAP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
