      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      *    INTERROGATION FICHIER COMPTABLES - PGM CICS TFFAP          *         
      *****************************************************************         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-FFAP-LONG-COMMAREA PIC S9(4) COMP VALUE +500.                   
      *--                                                                       
       01  COMM-FFAP-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +500.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA-FFAP.                                                     
      *** DEMANDE ET ENTITE COMPTABLE A RENSEIGNER                              
          03 COMM-FFAP-DEMANDE              PIC 99.                             
          03 COMM-FFAP-ENTCPT.                                                  
             05 COMM-FFAP-ENTCPT-SOC        PIC X(02).                          
             05 COMM-FFAP-ENTCPT-TLMELA     PIC X(03).                          
      *                                                                         
      * REPONSE  OK = 0   SINON = 1 (NON TROUVE) 9 (FATAL)                      
      *                                                                         
          03 COMM-FFAP-REPONSE              PIC 9.                              
          03 COMM-FFAP-MESS-ERR             PIC X(60).                          
      *                                                                         
          03 COMM-FFAP-INFOS PIC X(432).                                        
      *                                                                         
      * DEMANDE 01 - ANNEE, PERIODE FISCALE  NE RIEN RENSEIGNER                 
      *                                                                         
          03 COMM-FFAP-INFOS-PU0001 REDEFINES COMM-FFAP-INFOS.                  
                05 COMM-FFAP-PERIODNBR      PIC 99.                             
                05 COMM-FFAP-FISCAYY        PIC 99.                             
                05 COMM-FFAP-INTPEDTE       PIC S9(5).                          
      *                                                                         
      * DEMANDE 02 - GENERALITES TIERS  RENSEIGNER COMM-FFAP-TIERS              
      *                                                                         
      *                                                                         
      * DEMANDE 03 - BANQUE             RENSEIGNER COMM-FFAP-BANK               
      *                                                                         
          03 COMM-FFAP-INFOS-AP0001 REDEFINES COMM-FFAP-INFOS.                  
                05 COMM-FFAP-TIERS.                                             
                   07 COMM-FFAP-CTIERS-1    PIC X(2).                           
                   07 COMM-FFAP-CTIERS-2    PIC X(5).                           
                   07 COMM-FFAP-CTIERS-3    PIC X(2).                           
                05 COMM-FFAP-REMITNAME      PIC X(31).                          
                05 COMM-FFAP-REMITADDR2     PIC X(31).                          
                05 COMM-FFAP-REMITADDR3     PIC X(31).                          
                05 COMM-FFAP-REMITADDR4     PIC X(31).                          
                05 COMM-FFAP-REMITADDR5     PIC X(22).                          
                05 COMM-FFAP-ZIPCODE        PIC X(09).                          
                05 COMM-FFAP-ALPHASORT      PIC X(20).                          
                05 COMM-FFAP-CONTACT        PIC X(20).                          
                05 COMM-FFAP-PHONE          PIC X(20).                          
                05 COMM-FFAP-TERMSCODE      PIC X(01).                          
                05 COMM-FFAP-DISCDAYS       PIC 9(03).                          
                05 COMM-FFAP-PAYDAYS        PIC 9(03).                          
                05 COMM-FFAP-MISCCODE       PIC X(01).                          
                05 COMM-FFAP-HOLD           PIC X(01).                          
                05 COMM-FFAP-BANK           PIC X(02).                          
                05 COMM-FFAP-BANKDESC       PIC X(25).                          
                05 COMM-FFAP-ALTVENDOR      PIC X(15).                          
                05 COMM-FFAP-REGIME         PIC X(01).                          
                05 COMM-FFAP-NATURE         PIC X(01).                          
                05 COMM-FFAP-VATCODE        PIC X(02).                          
                05 COMM-FFAP-PAYMETHOD      PIC X(02).                          
                05 COMM-FFAP-SHORTDESC      PIC X(14).                          
                05 COMM-FFAP-GROUPE-HG      PIC X(01).                          
                05 COMM-FFAP-LMISCCODE      PIC X(03).                          
                05 COMM-FFAP-ACCOUNT        PIC X(35).                          
                05 COMM-FFAP-IDENTIFIER     PIC X(05).                          
                05 COMM-FFAP-SORTCODE       PIC X(10).                          
                05 COMM-FFAP-CHECKDGTS      PIC 9(02).                          
      *                                                                         
      * ZONES GCT                                                               
      *                                                                         
          03 COMM-FFAP-INFOS-GCT    REDEFINES COMM-FFAP-INFOS.                  
                05 COMM-GCT-NAUX            PIC X.                              
                05 COMM-GCT-NFOUR           PIC X(08).                          
                05 COMM-GCT-LIBFOUR         PIC X(31).                          
                05 COMM-GCT-ADRESSE         PIC X(31).                          
                05 COMM-GCT-LCMPAD1         PIC X(31).                          
                05 COMM-GCT-LCOMMUNE        PIC X(31).                          
                05 COMM-GCT-LBUREAU         PIC X(22).                          
                05 COMM-GCT-CPOSTAL         PIC X(09).                          
                05 FILLER                   PIC X(20).                          
                05 COMM-GCT-INTERLOC        PIC X(20).                          
                05 COMM-GCT-NUMTEL          PIC X(20).                          
                05 COMM-GCT-INDICE          PIC X.                              
                05 COMM-GCT-JPAIE           PIC X(02).                          
                05 FILLER                   PIC 9.                              
                05 COMM-GCT-NBJREG          PIC 9(03).                          
                05 COMM-GCT-CODEGEO         PIC X.                              
                05 FILLER                   PIC X.                              
                05 COMM-GCT-METHREG         PIC X(03).                          
                05 COMM-GCT-LMETHREG        PIC X(30).                          
                05 COMM-GCT-NTBENEF         PIC X(08).                          
                05 FILLER                   PIC X(21).                          
                05 COMM-GCT-GROUPE-HG       PIC X.                              
                05 COMM-GCT-LCODEGEO        PIC X(03).                          
PBL             05 COMM-GCT-DEVISEREG       PIC X(03).                          
PBL             05 FILLER                   PIC X(47).                          
                05 FILLER                   PIC 9(02).                          
      *                                                                         
                                                                                
