      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT7600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT7600                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT7600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT7600.                                                            
      *}                                                                        
           02  FT76-NSOC                                                        
               PIC X(0005).                                                     
           02  FT76-NETAB                                                       
               PIC X(0003).                                                     
           02  FT76-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT76-NAUX                                                        
               PIC X(0003).                                                     
           02  FT76-NFACTURE                                                    
               PIC X(0020).                                                     
           02  FT76-DFACTURE                                                    
               PIC X(0008).                                                     
           02  FT76-NJRN                                                        
               PIC X(0003).                                                     
           02  FT76-NEXER                                                       
               PIC X(0004).                                                     
           02  FT76-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT76-DFACTW4                                                     
               PIC X(0008).                                                     
           02  FT76-DCREAT                                                      
               PIC X(0008).                                                     
           02  FT76-DMAJBAP                                                     
               PIC X(0008).                                                     
           02  FT76-DFACTMAJ                                                    
               PIC X(0008).                                                     
           02  FT76-TYPECR                                                      
               PIC X(0003).                                                     
           02  FT76-ORIGINE                                                     
               PIC X(0005).                                                     
           02  FT76-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT7600                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT7600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT7600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DFACTURE-FF                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DFACTURE-FF                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DFACTW4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DFACTW4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DMAJBAP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DMAJBAP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DFACTMAJ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DFACTMAJ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-TYPECR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-TYPECR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT76-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT76-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
