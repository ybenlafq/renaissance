      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM5800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM5800                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5800.                                                            
           02  FM58-CODE                                                        
               PIC X(0001).                                                     
           02  FM58-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM58-NBMOIS                                                      
               PIC S9(2) COMP-3.                                                
           02  FM58-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM5800                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM58-CODE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM58-CODE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM58-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM58-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM58-NBMOIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM58-NBMOIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM58-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM58-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
