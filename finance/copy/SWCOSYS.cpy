      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    DEFINITION DES ECRITURES A COMPTABILISER                             
      *        DANS LES COMPTA. COSYS CIBLES DE LA F.I.S.                       
      *                                                                         
      *    MODIF LE 21/12/95 : AJOUT DECHEANCE + FILLER DE 20                   
      *    INTEGRATION DANS BFG140 : 05/96                                      
      *    LRECL = 128                                                          
      *                                                                         
       01       FCOSYS-DSECT.                                                   
           05   FCOSYS-IDENT.                                                   
001          10 FCOSYS-NSOCCOMPT         PIC X(03).                             
004          10 FCOSYS-NETAB             PIC X(02).                             
006          10 FCOSYS-NEXERCICE         PIC X(02).                             
008          10 FCOSYS-NPERIODE          PIC X(03).                             
011          10 FCOSYS-DCOMPTA           PIC X(06).                             
017          10 FCOSYS-NJRN              PIC X(03).                             
020          10 FCOSYS-NPIECE            PIC 9(05).                             
025          10 FCOSYS-NLIGNE            PIC 9(03).                             
           05   FCOSYS-SUITE.                                                   
028          10 FCOSYS-COMPTE            PIC X(06).                             
034          10 FCOSYS-SENS              PIC X(01).                             
035          10 FCOSYS-MONTANT           PIC S9(11)V99 COMP-3.                  
042          10 FCOSYS-LIBELLE           PIC X(20).                             
062          10 FCOSYS-SECTION           PIC X(06).                             
068          10 FCOSYS-RUBRIQUE          PIC X(04).                             
072          10 FCOSYS-NAUX              PIC X(01).                             
073          10 FCOSYS-NSOCCOR           PIC X(03).                             
076          10 FCOSYS-TSOCFACT          PIC X(01).                             
077          10 FCOSYS-NSOCGEST          PIC X(03).                             
080          10 FCOSYS-NLIEUGEST         PIC X(03).                             
083          10 FCOSYS-NATFACT           PIC X(05).                             
088          10 FCOSYS-CVENT             PIC X(05).                             
093          10 FCOSYS-CTYPPIECE         PIC X(03).                             
096          10 FCOSYS-CTYPLIGNE         PIC X(03).                             
099          10 FCOSYS-DECHEANCE         PIC X(08).                             
107          10 FILLER                   PIC X(22).                             
->128 *                                                                         
                                                                                
