      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFM32                                          *  00020000
      *       TR : FM32  GESTION PARAMETRAGE MODELE GCT              *  00030000
      *       PG : TFM32 CREATION/MODIFICATION DES ENTITE MODELES    *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
      *01  TS-FM32-LONG         PIC S9(5) COMP-3 VALUE 492.             00080006
       01  TS-FM32-LONG         PIC S9(5) COMP-3 VALUE 600.             00080006
       01  TS-FM32-DONNEES.                                             00090000
           05 TS-FM32-LIGNE OCCURS 12.                                          
              10 TS-FM32-NENTITE   PIC X(05).                           00150000
              10 TS-FM32-LENTITE   PIC X(20).                           00150000
              10 TS-FM32-CPCG      PIC X(04).                           00150000
              10 TS-FM32-CPDS      PIC X(04).                           00150000
              10 TS-FM32-CPDR      PIC X(04).                           00150000
              10 TS-FM32-ANCCPDS   PIC X(04).                           00150000
              10 TS-FM32-ANCCPDR   PIC X(04).                           00150000
              10 TS-FM32-CDEVISE   PIC X(03).                           00150000
      *-------FLAG -----------------------------------                  00070000
              10 TS-FM32-FLAG      PIC X.                               00150000
              10 TS-FM32-SUP       PIC X.                               00150000
                                                                                
