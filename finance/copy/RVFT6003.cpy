      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RTFT60                                               
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6003.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6003.                                                            
      *}                                                                        
           02  FT60-CPCG                                                        
               PIC X(0004).                                                     
           02  FT60-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT60-LCOMPTEC                                                    
               PIC X(0015).                                                     
           02  FT60-LCOMPTEL                                                    
               PIC X(0030).                                                     
           02  FT60-CMASQUE                                                     
               PIC X(0006).                                                     
           02  FT60-CCONSOD                                                     
               PIC X(0006).                                                     
           02  FT60-CCONSOC                                                     
               PIC X(0006).                                                     
           02  FT60-NAUX                                                        
               PIC X(0003).                                                     
           02  FT60-WTYPCOMPTE                                                  
               PIC X(0001).                                                     
           02  FT60-WAUXILIARISE                                                
               PIC X(0001).                                                     
           02  FT60-WCOLLECTIF                                                  
               PIC X(0001).                                                     
           02  FT60-WABONNE                                                     
               PIC X(0001).                                                     
           02  FT60-WREGAUTO                                                    
               PIC X(0001).                                                     
           02  FT60-WCATNATCH                                                   
               PIC X(0001).                                                     
           02  FT60-WLETTRAGE                                                   
               PIC X(0001).                                                     
           02  FT60-WPOINTAGE                                                   
               PIC X(0001).                                                     
           02  FT60-WEDITION                                                    
               PIC X(0001).                                                     
           02  FT60-WCLOTURE                                                    
               PIC X(0001).                                                     
           02  FT60-WINTERFACE                                                  
               PIC X(0001).                                                     
           02  FT60-CEPURATION                                                  
               PIC X(0001).                                                     
           02  FT60-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FT60-DMAJ                                                        
               PIC X(0008).                                                     
           02  FT60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT60-WDETAX                                                      
               PIC X(0001).                                                     
           02  FT60-TRANSPO                                                     
               PIC X(0006).                                                     
           02  FT60-WREGAUTOC                                                   
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RTFT60                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT6003-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT6003-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-LCOMPTEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-LCOMPTEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-LCOMPTEL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-LCOMPTEL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-CCONSOD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-CCONSOD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-CCONSOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-CCONSOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WTYPCOMPTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WTYPCOMPTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WAUXILIARISE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WAUXILIARISE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WCOLLECTIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WCOLLECTIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WABONNE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WABONNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WREGAUTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WREGAUTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WCATNATCH-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WCATNATCH-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WPOINTAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WPOINTAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WEDITION-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WEDITION-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-CEPURATION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-CEPURATION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WDETAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WDETAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-TRANSPO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-TRANSPO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT60-WREGAUTOC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT60-WREGAUTOC-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
