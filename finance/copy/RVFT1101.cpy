      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVFT1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT1100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1101.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1101.                                                            
      *}                                                                        
           02  FT11-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT11-NAUX                                                        
               PIC X(0003).                                                     
           02  FT11-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  FT11-ADRESSE                                                     
               PIC X(0032).                                                     
           02  FT11-LCMPAD1                                                     
               PIC X(0032).                                                     
           02  FT11-LCMPAD2                                                     
               PIC X(0032).                                                     
           02  FT11-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  FT11-CPOSTAL                                                     
               PIC X(0008).                                                     
           02  FT11-LBUREAU                                                     
               PIC X(0032).                                                     
           02  FT11-CPAYS                                                       
               PIC X(0003).                                                     
           02  FT11-NUMTEL                                                      
               PIC X(0015).                                                     
           02  FT11-NUMFAX                                                      
               PIC X(0015).                                                     
           02  FT11-INTERLOC                                                    
               PIC X(0012).                                                     
           02  FT11-RAISONSLE                                                   
               PIC X(0032).                                                     
           02  FT11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT11-NSOC                                                        
               PIC X(0005).                                                     
           02  FT11-NETAB                                                       
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT1100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1101-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1101-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-LCMPAD1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-LCMPAD2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-LCMPAD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-CPAYS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-CPAYS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NUMTEL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NUMTEL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NUMFAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NUMFAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-INTERLOC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-INTERLOC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-RAISONSLE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-RAISONSLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT11-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT11-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
