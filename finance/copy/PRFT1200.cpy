      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE FT1200                       
      ******************************************************************        
      *                                                                         
       CLEF-FT1200             SECTION.                                         
      *                                                                         
           MOVE 'RVFT1200          '       TO   TABLE-NAME.                     
           MOVE 'FT1200'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-FT1200. EXIT.                                                   
                EJECT                                                           
                                                                                
