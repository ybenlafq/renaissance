      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS100 AU 28/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS100.                                                        
            05 NOMETAT-IPS100           PIC X(6) VALUE 'IPS100'.                
            05 RUPTURES-IPS100.                                                 
           10 IPS100-CGCPLT             PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS100-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 IPS100-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS100.                                                   
           10 IPS100-LGCPLT             PIC X(20).                      014  020
           10 IPS100-NSOC               PIC X(03).                      034  003
           10 IPS100-NBRXT              PIC S9(05)      COMP-3.         037  003
           10 IPS100-NBR1               PIC S9(03)      COMP-3.         040  002
           10 IPS100-NBR10              PIC S9(03)      COMP-3.         042  002
           10 IPS100-NBR11              PIC S9(03)      COMP-3.         044  002
           10 IPS100-NBR12              PIC S9(03)      COMP-3.         046  002
           10 IPS100-NBR13              PIC S9(03)      COMP-3.         048  002
           10 IPS100-NBR14              PIC S9(03)      COMP-3.         050  002
           10 IPS100-NBR15              PIC S9(03)      COMP-3.         052  002
           10 IPS100-NBR16              PIC S9(03)      COMP-3.         054  002
           10 IPS100-NBR17              PIC S9(03)      COMP-3.         056  002
           10 IPS100-NBR18              PIC S9(03)      COMP-3.         058  002
           10 IPS100-NBR19              PIC S9(03)      COMP-3.         060  002
           10 IPS100-NBR2               PIC S9(03)      COMP-3.         062  002
           10 IPS100-NBR3               PIC S9(03)      COMP-3.         064  002
           10 IPS100-NBR4               PIC S9(03)      COMP-3.         066  002
           10 IPS100-NBR5               PIC S9(03)      COMP-3.         068  002
           10 IPS100-NBR6               PIC S9(03)      COMP-3.         070  002
           10 IPS100-NBR7               PIC S9(03)      COMP-3.         072  002
           10 IPS100-NBR8               PIC S9(03)      COMP-3.         074  002
           10 IPS100-NBR9               PIC S9(03)      COMP-3.         076  002
           10 IPS100-VTEXT              PIC S9(07)      COMP-3.         078  004
           10 IPS100-VTE1               PIC S9(06)      COMP-3.         082  004
           10 IPS100-VTE10              PIC S9(06)      COMP-3.         086  004
           10 IPS100-VTE11              PIC S9(06)      COMP-3.         090  004
           10 IPS100-VTE12              PIC S9(06)      COMP-3.         094  004
           10 IPS100-VTE13              PIC S9(06)      COMP-3.         098  004
           10 IPS100-VTE14              PIC S9(06)      COMP-3.         102  004
           10 IPS100-VTE15              PIC S9(06)      COMP-3.         106  004
           10 IPS100-VTE16              PIC S9(06)      COMP-3.         110  004
           10 IPS100-VTE17              PIC S9(06)      COMP-3.         114  004
           10 IPS100-VTE18              PIC S9(06)      COMP-3.         118  004
           10 IPS100-VTE19              PIC S9(06)      COMP-3.         122  004
           10 IPS100-VTE2               PIC S9(06)      COMP-3.         126  004
           10 IPS100-VTE3               PIC S9(06)      COMP-3.         130  004
           10 IPS100-VTE4               PIC S9(06)      COMP-3.         134  004
           10 IPS100-VTE5               PIC S9(06)      COMP-3.         138  004
           10 IPS100-VTE6               PIC S9(06)      COMP-3.         142  004
           10 IPS100-VTE7               PIC S9(06)      COMP-3.         146  004
           10 IPS100-VTE8               PIC S9(06)      COMP-3.         150  004
           10 IPS100-VTE9               PIC S9(06)      COMP-3.         154  004
           10 IPS100-MOISRACH           PIC X(08).                      158  008
           10 IPS100-MOIS1              PIC X(08).                      166  008
           10 IPS100-MOIS10             PIC X(08).                      174  008
           10 IPS100-MOIS11             PIC X(08).                      182  008
           10 IPS100-MOIS12             PIC X(08).                      190  008
           10 IPS100-MOIS13             PIC X(08).                      198  008
           10 IPS100-MOIS14             PIC X(08).                      206  008
           10 IPS100-MOIS15             PIC X(08).                      214  008
           10 IPS100-MOIS16             PIC X(08).                      222  008
           10 IPS100-MOIS17             PIC X(08).                      230  008
           10 IPS100-MOIS18             PIC X(08).                      238  008
           10 IPS100-MOIS19             PIC X(08).                      246  008
           10 IPS100-MOIS2              PIC X(08).                      254  008
           10 IPS100-MOIS3              PIC X(08).                      262  008
           10 IPS100-MOIS4              PIC X(08).                      270  008
           10 IPS100-MOIS5              PIC X(08).                      278  008
           10 IPS100-MOIS6              PIC X(08).                      286  008
           10 IPS100-MOIS7              PIC X(08).                      294  008
           10 IPS100-MOIS8              PIC X(08).                      302  008
           10 IPS100-MOIS9              PIC X(08).                      310  008
            05 FILLER                      PIC X(195).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS100-LONG           PIC S9(4)   COMP  VALUE +317.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS100-LONG           PIC S9(4) COMP-5  VALUE +317.           
                                                                                
      *}                                                                        
