      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFT6600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT6600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6600.                                                            
           02  FT66-CPCG                                                        
               PIC X(0004).                                                     
           02  FT66-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT66-NSEQ                                                        
               PIC X(0002).                                                     
           02  FT66-CCATEGORIE                                                  
               PIC X(0006).                                                     
           02  FT66-DSYST                                                       
               PIC S9(13)  COMP-3.                                              
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT6600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT66-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT66-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT66-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT66-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT66-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT66-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT66-CCATEGORIE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT66-CCATEGORIE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT66-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT66-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
