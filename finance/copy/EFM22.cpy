      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GESTION DEVISE                                            00000020
      ***************************************************************** 00000030
       01   EFM22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCDEVISEI      PIC X(3).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVNUML      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MCDEVNUML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVNUMF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCDEVNUMI      PIC X(3).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLDEVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLDEVF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLDEVI    PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLDEVISEI      PIC X(25).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBDECIML      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MNBDECIML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBDECIMF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNBDECIMI      PIC X.                                     00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUBL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLSUBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSUBF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLSUBI    PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUBDIVL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLSUBDIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSUBDIVF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLSUBDIVI      PIC X(25).                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCAISSEL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MCCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCCAISSEF      PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCCAISSEI      PIC X.                                     00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEUROL   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MWEUROL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWEUROF   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MWEUROI   PIC X.                                          00000510
      * ZONE CMD AIDA                                                   00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(79).                                      00000560
      * CODE TRANSACTION                                                00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      * CICS DE TRAVAIL                                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      * NETNAME                                                         00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      * CODE TERMINAL                                                   00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MSCREENI  PIC X(4).                                       00000760
      ***************************************************************** 00000770
      * GCT - GESTION DEVISE                                            00000780
      ***************************************************************** 00000790
       01   EFM22O REDEFINES EFM22I.                                    00000800
           02 FILLER    PIC X(12).                                      00000810
      * DATE DU JOUR                                                    00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
      * HEURE                                                           00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MCDEVISEA      PIC X.                                     00000990
           02 MCDEVISEC PIC X.                                          00001000
           02 MCDEVISEP PIC X.                                          00001010
           02 MCDEVISEH PIC X.                                          00001020
           02 MCDEVISEV PIC X.                                          00001030
           02 MCDEVISEO      PIC X(3).                                  00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MCDEVNUMA      PIC X.                                     00001060
           02 MCDEVNUMC PIC X.                                          00001070
           02 MCDEVNUMP PIC X.                                          00001080
           02 MCDEVNUMH PIC X.                                          00001090
           02 MCDEVNUMV PIC X.                                          00001100
           02 MCDEVNUMO      PIC X(3).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLDEVA    PIC X.                                          00001130
           02 MLDEVC    PIC X.                                          00001140
           02 MLDEVP    PIC X.                                          00001150
           02 MLDEVH    PIC X.                                          00001160
           02 MLDEVV    PIC X.                                          00001170
           02 MLDEVO    PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLDEVISEA      PIC X.                                     00001200
           02 MLDEVISEC PIC X.                                          00001210
           02 MLDEVISEP PIC X.                                          00001220
           02 MLDEVISEH PIC X.                                          00001230
           02 MLDEVISEV PIC X.                                          00001240
           02 MLDEVISEO      PIC X(25).                                 00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNBDECIMA      PIC X.                                     00001270
           02 MNBDECIMC PIC X.                                          00001280
           02 MNBDECIMP PIC X.                                          00001290
           02 MNBDECIMH PIC X.                                          00001300
           02 MNBDECIMV PIC X.                                          00001310
           02 MNBDECIMO      PIC X.                                     00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLSUBA    PIC X.                                          00001340
           02 MLSUBC    PIC X.                                          00001350
           02 MLSUBP    PIC X.                                          00001360
           02 MLSUBH    PIC X.                                          00001370
           02 MLSUBV    PIC X.                                          00001380
           02 MLSUBO    PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLSUBDIVA      PIC X.                                     00001410
           02 MLSUBDIVC PIC X.                                          00001420
           02 MLSUBDIVP PIC X.                                          00001430
           02 MLSUBDIVH PIC X.                                          00001440
           02 MLSUBDIVV PIC X.                                          00001450
           02 MLSUBDIVO      PIC X(25).                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCCAISSEA      PIC X.                                     00001480
           02 MCCAISSEC PIC X.                                          00001490
           02 MCCAISSEP PIC X.                                          00001500
           02 MCCAISSEH PIC X.                                          00001510
           02 MCCAISSEV PIC X.                                          00001520
           02 MCCAISSEO      PIC X.                                     00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MWEUROA   PIC X.                                          00001550
           02 MWEUROC   PIC X.                                          00001560
           02 MWEUROP   PIC X.                                          00001570
           02 MWEUROH   PIC X.                                          00001580
           02 MWEUROV   PIC X.                                          00001590
           02 MWEUROO   PIC X.                                          00001600
      * ZONE CMD AIDA                                                   00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLIBERRA  PIC X.                                          00001630
           02 MLIBERRC  PIC X.                                          00001640
           02 MLIBERRP  PIC X.                                          00001650
           02 MLIBERRH  PIC X.                                          00001660
           02 MLIBERRV  PIC X.                                          00001670
           02 MLIBERRO  PIC X(79).                                      00001680
      * CODE TRANSACTION                                                00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
      * CICS DE TRAVAIL                                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCICSA    PIC X.                                          00001790
           02 MCICSC    PIC X.                                          00001800
           02 MCICSP    PIC X.                                          00001810
           02 MCICSH    PIC X.                                          00001820
           02 MCICSV    PIC X.                                          00001830
           02 MCICSO    PIC X(5).                                       00001840
      * NETNAME                                                         00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
      * CODE TERMINAL                                                   00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MSCREENA  PIC X.                                          00001950
           02 MSCREENC  PIC X.                                          00001960
           02 MSCREENP  PIC X.                                          00001970
           02 MSCREENH  PIC X.                                          00001980
           02 MSCREENV  PIC X.                                          00001990
           02 MSCREENO  PIC X(4).                                       00002000
                                                                                
