      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM8500                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8500                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFM8500.                                                    00090001
           02  FM85-SOCREF                                              00100001
               PIC X(0005).                                             00110000
           02  FM85-CINTERFACE                                          00120001
               PIC X(0005).                                             00130000
           02  FM85-CNATOPER                                            00140001
               PIC X(0005).                                             00150000
           02  FM85-CTYPOPER                                            00160001
               PIC X(0005).                                             00170000
           02  FM85-CRITERE1                                            00180001
               PIC X(0005).                                             00190000
           02  FM85-CRITERE2                                            00200001
               PIC X(0005).                                             00210000
           02  FM85-CRITERE3                                            00220001
               PIC X(0005).                                             00230000
           02  FM85-NSOCIETE                                            00240001
               PIC X(0003).                                             00250000
           02  FM85-NLIEU                                               00260001
               PIC X(0003).                                             00270000
           02  FM85-SECTION                                             00280001
               PIC X(0006).                                             00290000
           02  FM85-SECTIONC                                            00300001
               PIC X(0006).                                             00310001
           02  FM85-DEFFET                                              00320001
               PIC X(0008).                                             00330001
           02  FM85-DMAJ                                                00340001
               PIC X(0008).                                             00350001
           02  FM85-DSYST                                               00360001
               PIC S9(13) COMP-3.                                       00370001
      *                                                                 00380001
      *---------------------------------------------------------        00390001
      *   LISTE DES FLAGS DE LA TABLE RVFM8500                          00400001
      *---------------------------------------------------------        00410001
      *                                                                 00420001
       01  RVFM8500-FLAGS.                                              00430001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-SOCREF-F                                            00440001
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  FM85-SOCREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CINTERFACE-F                                        00460001
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  FM85-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CNATOPER-F                                          00480001
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  FM85-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CTYPOPER-F                                          00500001
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  FM85-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CRITERE1-F                                          00520001
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  FM85-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CRITERE2-F                                          00540001
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  FM85-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-CRITERE3-F                                          00560001
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  FM85-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-NSOCIETE-F                                          00580001
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  FM85-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-NLIEU-F                                             00600001
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  FM85-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-SECTION-F                                           00620001
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  FM85-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-SECTIONC-F                                          00640001
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  FM85-SECTIONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-DEFFET-F                                            00660001
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  FM85-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-DMAJ-F                                              00680001
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  FM85-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM85-DSYST-F                                             00700001
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  FM85-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00720000
                                                                                
