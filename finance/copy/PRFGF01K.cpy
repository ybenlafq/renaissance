      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *                                                                         
      ******************************************************************        
      * PREPARATION DE L'ACCES AU FICHIER FGF01C PAR LE CHEMIN FGF01K           
      ******************************************************************        
      *                                                                         
       CLEF-FGF01K             SECTION.                                         
      *                                                                         
           MOVE '907'        TO                                                 
                FGF01-FGF01K-GFNSOC.                                            
      *                                                                         
           MOVE FGF01-FGF01K TO VSAM-KEY.                                       
      *                                                                         
           MOVE 'FGF01K' TO PATH-NAME.                                          
           MOVE 'CD0001A' TO FILE-NAME.                                         
           MOVE +100  TO FILE-LONG.                                             
           MOVE FGF01    TO Z-INOUT.                                            
      *                                                                         
       FIN-CLEF-FGF01K.  EXIT.                                                  
                EJECT                                                           
                                                                                
EMOD                                                                            
