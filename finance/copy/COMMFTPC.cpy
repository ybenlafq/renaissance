      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA PROGRAMMES TFTPCA ET BFTPCA                            00020001
      *     COMPLEMENT DE LA SECTION                                    00030006
      * ============================================================            
      **************************************************************    00050000
       01 COMMFTPC.                                                     00060001
          02 COMM-FTPC-SECTION        PIC  X(6).                        00080001
          02 COMM-FTPC-CRITLIEU1      PIC  X(3).                        00120001
          02 COMM-FTPC-CRITLIEU2      PIC  X(3).                        00140001
          02 COMM-FTPC-RCODE          PIC  X(1).                        00280001
             88 COMM-FTPC-OK          VALUE '0'.                        00290001
             88 COMM-FTPC-KO          VALUE '1'.                        00300001
          02 COMM-FTPC-SQLCODE        PIC  ---9.                        00280001
          02 COMM-FTPC-ERREUR         PIC X(80).                        00320007
                                                                                
