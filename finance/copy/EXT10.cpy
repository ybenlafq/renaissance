      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT10   EXT10                                              00000020
      ***************************************************************** 00000030
       01   EXT10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCETATI   PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNPAGEI   PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNBPAGESI      PIC X(2).                                  00000270
           02 M65I OCCURS   15 TIMES .                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQCORRL   COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MWSEQCORRL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWSEQCORRF   PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MWSEQCORRI   PIC X(7).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRODCORRL  COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLPRODCORRL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLPRODCORRF  PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLPRODCORRI  PIC X(25).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUITEL      COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MSUITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSUITEF      PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MSUITEI      PIC X(8).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQPROL    COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MWSEQPROL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSEQPROF    PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MWSEQPROI    PIC X(7).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRODL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MLPRODL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLPRODF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MLPRODI      PIC X(25).                                 00000480
      * MESSAGE ERREUR                                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(78).                                      00000530
      * CODE TRANSACTION                                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      * CICS DE TRAVAIL                                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      * NETNAME                                                         00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MNETNAMI  PIC X(8).                                       00000720
      * CODE TERMINAL                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCREENI  PIC X(5).                                       00000770
      ***************************************************************** 00000780
      * SDF: EXT10   EXT10                                              00000790
      ***************************************************************** 00000800
       01   EXT10O REDEFINES EXT10I.                                    00000810
           02 FILLER    PIC X(12).                                      00000820
      * DATE DU JOUR                                                    00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
      * HEURE                                                           00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MTIMJOUA  PIC X.                                          00000930
           02 MTIMJOUC  PIC X.                                          00000940
           02 MTIMJOUP  PIC X.                                          00000950
           02 MTIMJOUH  PIC X.                                          00000960
           02 MTIMJOUV  PIC X.                                          00000970
           02 MTIMJOUO  PIC X(5).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MCETATA   PIC X.                                          00001000
           02 MCETATC   PIC X.                                          00001010
           02 MCETATP   PIC X.                                          00001020
           02 MCETATH   PIC X.                                          00001030
           02 MCETATV   PIC X.                                          00001040
           02 MCETATO   PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNPAGEA   PIC X.                                          00001070
           02 MNPAGEC   PIC X.                                          00001080
           02 MNPAGEP   PIC X.                                          00001090
           02 MNPAGEH   PIC X.                                          00001100
           02 MNPAGEV   PIC X.                                          00001110
           02 MNPAGEO   PIC ZZ.                                         00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MNBPAGESA      PIC X.                                     00001140
           02 MNBPAGESC PIC X.                                          00001150
           02 MNBPAGESP PIC X.                                          00001160
           02 MNBPAGESH PIC X.                                          00001170
           02 MNBPAGESV PIC X.                                          00001180
           02 MNBPAGESO      PIC ZZ.                                    00001190
           02 M65O OCCURS   15 TIMES .                                  00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MWSEQCORRA   PIC X.                                     00001220
             03 MWSEQCORRC   PIC X.                                     00001230
             03 MWSEQCORRP   PIC X.                                     00001240
             03 MWSEQCORRH   PIC X.                                     00001250
             03 MWSEQCORRV   PIC X.                                     00001260
             03 MWSEQCORRO   PIC ZZZZZZZ.                               00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MLPRODCORRA  PIC X.                                     00001290
             03 MLPRODCORRC  PIC X.                                     00001300
             03 MLPRODCORRP  PIC X.                                     00001310
             03 MLPRODCORRH  PIC X.                                     00001320
             03 MLPRODCORRV  PIC X.                                     00001330
             03 MLPRODCORRO  PIC X(25).                                 00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MSUITEA      PIC X.                                     00001360
             03 MSUITEC PIC X.                                          00001370
             03 MSUITEP PIC X.                                          00001380
             03 MSUITEH PIC X.                                          00001390
             03 MSUITEV PIC X.                                          00001400
             03 MSUITEO      PIC X(8).                                  00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MWSEQPROA    PIC X.                                     00001430
             03 MWSEQPROC    PIC X.                                     00001440
             03 MWSEQPROP    PIC X.                                     00001450
             03 MWSEQPROH    PIC X.                                     00001460
             03 MWSEQPROV    PIC X.                                     00001470
             03 MWSEQPROO    PIC ZZZZZZZ.                               00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MLPRODA      PIC X.                                     00001500
             03 MLPRODC PIC X.                                          00001510
             03 MLPRODP PIC X.                                          00001520
             03 MLPRODH PIC X.                                          00001530
             03 MLPRODV PIC X.                                          00001540
             03 MLPRODO      PIC X(25).                                 00001550
      * MESSAGE ERREUR                                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLIBERRA  PIC X.                                          00001580
           02 MLIBERRC  PIC X.                                          00001590
           02 MLIBERRP  PIC X.                                          00001600
           02 MLIBERRH  PIC X.                                          00001610
           02 MLIBERRV  PIC X.                                          00001620
           02 MLIBERRO  PIC X(78).                                      00001630
      * CODE TRANSACTION                                                00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCODTRAA  PIC X.                                          00001660
           02 MCODTRAC  PIC X.                                          00001670
           02 MCODTRAP  PIC X.                                          00001680
           02 MCODTRAH  PIC X.                                          00001690
           02 MCODTRAV  PIC X.                                          00001700
           02 MCODTRAO  PIC X(4).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MZONCMDA  PIC X.                                          00001730
           02 MZONCMDC  PIC X.                                          00001740
           02 MZONCMDP  PIC X.                                          00001750
           02 MZONCMDH  PIC X.                                          00001760
           02 MZONCMDV  PIC X.                                          00001770
           02 MZONCMDO  PIC X(15).                                      00001780
      * CICS DE TRAVAIL                                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCICSA    PIC X.                                          00001810
           02 MCICSC    PIC X.                                          00001820
           02 MCICSP    PIC X.                                          00001830
           02 MCICSH    PIC X.                                          00001840
           02 MCICSV    PIC X.                                          00001850
           02 MCICSO    PIC X(5).                                       00001860
      * NETNAME                                                         00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
      * CODE TERMINAL                                                   00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MSCREENA  PIC X.                                          00001970
           02 MSCREENC  PIC X.                                          00001980
           02 MSCREENP  PIC X.                                          00001990
           02 MSCREENH  PIC X.                                          00002000
           02 MSCREENV  PIC X.                                          00002010
           02 MSCREENO  PIC X(5).                                       00002020
                                                                                
