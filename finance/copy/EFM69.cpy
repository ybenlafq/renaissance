      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - COMBINAISONS CLE COMPTABLE                                00000020
      ***************************************************************** 00000030
       01   EFM69I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSECTION1L    COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MCSECTION1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCSECTION1F    PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCSECTION1I    PIC X(8).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSECTION2L    COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MCSECTION2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCSECTION2F    PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCSECTION2I    PIC X(8).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSECTION3L    COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MCSECTION3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCSECTION3F    PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCSECTION3I    PIC X(8).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRUB1L   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCRUB1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRUB1F   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCRUB1I   PIC X(8).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRUB2L   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCRUB2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRUB2F   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCRUB2I   PIC X(8).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRUB3L   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCRUB3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRUB3F   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCRUB3I   PIC X(8).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCTYPI    PIC X(5).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCNATI    PIC X(5).                                       00000630
           02 MTABI OCCURS   2 TIMES .                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MCTYPOPERI   PIC X(5).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCNATOPERI   PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTYPOPERL   COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MLTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLTYPOPERF   PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MLTYPOPERI   PIC X(15).                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNATOPERL   COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MLNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLNATOPERF   PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MLNATOPERI   PIC X(15).                                 00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSECTION1L  COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MLSECTION1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSECTION1F  PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MLSECTION1I  PIC X(5).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSECTION2L  COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MLSECTION2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSECTION2F  PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MLSECTION2I  PIC X(5).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSECTION3L  COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MLSECTION3L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSECTION3F  PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MLSECTION3I  PIC X(5).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUB1L      COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MLRUB1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLRUB1F      PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MLRUB1I      PIC X(5).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUB2L      COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MLRUB2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLRUB2F      PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MLRUB2I      PIC X(5).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUB3L      COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MLRUB3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLRUB3F      PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MLRUB3I      PIC X(5).                                  00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MCOMPTEI     PIC X(6).                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTEL    COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MLCOMPTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMPTEF    PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MLCOMPTEI    PIC X(15).                                 00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTIONL    COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MSECTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECTIONF    PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MSECTIONI    PIC X(6).                                  00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSECTIONL   COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MLSECTIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLSECTIONF   PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MLSECTIONI   PIC X(15).                                 00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUEL   COMP PIC S9(4).                            00001210
      *--                                                                       
             03 MRUBRIQUEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MRUBRIQUEF   PIC X.                                     00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MRUBRIQUEI   PIC X(6).                                  00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUBRIQUEL  COMP PIC S9(4).                            00001250
      *--                                                                       
             03 MLRUBRIQUEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLRUBRIQUEF  PIC X.                                     00001260
             03 FILLER  PIC X(4).                                       00001270
             03 MLRUBRIQUEI  PIC X(15).                                 00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTREPL    COMP PIC S9(4).                            00001290
      *--                                                                       
             03 MCONTREPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCONTREPF    PIC X.                                     00001300
             03 FILLER  PIC X(4).                                       00001310
             03 MCONTREPI    PIC X(6).                                  00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONTREPL   COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MLCONTREPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCONTREPF   PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MLCONTREPI   PIC X(15).                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTIONCL   COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MSECTIONCL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MSECTIONCF   PIC X.                                     00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MSECTIONCI   PIC X(6).                                  00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSECTIONCL  COMP PIC S9(4).                            00001410
      *--                                                                       
             03 MLSECTIONCL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSECTIONCF  PIC X.                                     00001420
             03 FILLER  PIC X(4).                                       00001430
             03 MLSECTIONCI  PIC X(15).                                 00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUECL  COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MRUBRIQUECL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MRUBRIQUECF  PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MRUBRIQUECI  PIC X(6).                                  00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRUBRIQUECL      COMP PIC S9(4).                       00001490
      *--                                                                       
             03 MLRUBRIQUECL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MLRUBRIQUECF      PIC X.                                00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MLRUBRIQUECI      PIC X(15).                            00001520
      * ZONE CMD AIDA                                                   00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MLIBERRI  PIC X(79).                                      00001570
      * CODE TRANSACTION                                                00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCODTRAI  PIC X(4).                                       00001620
      * CICS DE TRAVAIL                                                 00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MCICSI    PIC X(5).                                       00001670
      * NETNAME                                                         00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001700
           02 FILLER    PIC X(4).                                       00001710
           02 MNETNAMI  PIC X(8).                                       00001720
      * CODE TERMINAL                                                   00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MSCREENI  PIC X(4).                                       00001770
      ***************************************************************** 00001780
      * GCT - COMBINAISONS CLE COMPTABLE                                00001790
      ***************************************************************** 00001800
       01   EFM69O REDEFINES EFM69I.                                    00001810
           02 FILLER    PIC X(12).                                      00001820
      * DATE DU JOUR                                                    00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
      * HEURE                                                           00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MTIMJOUA  PIC X.                                          00001930
           02 MTIMJOUC  PIC X.                                          00001940
           02 MTIMJOUP  PIC X.                                          00001950
           02 MTIMJOUH  PIC X.                                          00001960
           02 MTIMJOUV  PIC X.                                          00001970
           02 MTIMJOUO  PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MPAGEA    PIC X.                                          00002000
           02 MPAGEC    PIC X.                                          00002010
           02 MPAGEP    PIC X.                                          00002020
           02 MPAGEH    PIC X.                                          00002030
           02 MPAGEV    PIC X.                                          00002040
           02 MPAGEO    PIC ZZ9.                                        00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNBPA     PIC X.                                          00002070
           02 MNBPC     PIC X.                                          00002080
           02 MNBPP     PIC X.                                          00002090
           02 MNBPH     PIC X.                                          00002100
           02 MNBPV     PIC X.                                          00002110
           02 MNBPO     PIC ZZ9.                                        00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCINTERFACEA   PIC X.                                     00002140
           02 MCINTERFACEC   PIC X.                                     00002150
           02 MCINTERFACEP   PIC X.                                     00002160
           02 MCINTERFACEH   PIC X.                                     00002170
           02 MCINTERFACEV   PIC X.                                     00002180
           02 MCINTERFACEO   PIC X(5).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLINTERFACEA   PIC X.                                     00002210
           02 MLINTERFACEC   PIC X.                                     00002220
           02 MLINTERFACEP   PIC X.                                     00002230
           02 MLINTERFACEH   PIC X.                                     00002240
           02 MLINTERFACEV   PIC X.                                     00002250
           02 MLINTERFACEO   PIC X(30).                                 00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCSECTION1A    PIC X.                                     00002280
           02 MCSECTION1C    PIC X.                                     00002290
           02 MCSECTION1P    PIC X.                                     00002300
           02 MCSECTION1H    PIC X.                                     00002310
           02 MCSECTION1V    PIC X.                                     00002320
           02 MCSECTION1O    PIC X(8).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCSECTION2A    PIC X.                                     00002350
           02 MCSECTION2C    PIC X.                                     00002360
           02 MCSECTION2P    PIC X.                                     00002370
           02 MCSECTION2H    PIC X.                                     00002380
           02 MCSECTION2V    PIC X.                                     00002390
           02 MCSECTION2O    PIC X(8).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCSECTION3A    PIC X.                                     00002420
           02 MCSECTION3C    PIC X.                                     00002430
           02 MCSECTION3P    PIC X.                                     00002440
           02 MCSECTION3H    PIC X.                                     00002450
           02 MCSECTION3V    PIC X.                                     00002460
           02 MCSECTION3O    PIC X(8).                                  00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MCRUB1A   PIC X.                                          00002490
           02 MCRUB1C   PIC X.                                          00002500
           02 MCRUB1P   PIC X.                                          00002510
           02 MCRUB1H   PIC X.                                          00002520
           02 MCRUB1V   PIC X.                                          00002530
           02 MCRUB1O   PIC X(8).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MCRUB2A   PIC X.                                          00002560
           02 MCRUB2C   PIC X.                                          00002570
           02 MCRUB2P   PIC X.                                          00002580
           02 MCRUB2H   PIC X.                                          00002590
           02 MCRUB2V   PIC X.                                          00002600
           02 MCRUB2O   PIC X(8).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCRUB3A   PIC X.                                          00002630
           02 MCRUB3C   PIC X.                                          00002640
           02 MCRUB3P   PIC X.                                          00002650
           02 MCRUB3H   PIC X.                                          00002660
           02 MCRUB3V   PIC X.                                          00002670
           02 MCRUB3O   PIC X(8).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCTYPA    PIC X.                                          00002700
           02 MCTYPC    PIC X.                                          00002710
           02 MCTYPP    PIC X.                                          00002720
           02 MCTYPH    PIC X.                                          00002730
           02 MCTYPV    PIC X.                                          00002740
           02 MCTYPO    PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MCNATA    PIC X.                                          00002770
           02 MCNATC    PIC X.                                          00002780
           02 MCNATP    PIC X.                                          00002790
           02 MCNATH    PIC X.                                          00002800
           02 MCNATV    PIC X.                                          00002810
           02 MCNATO    PIC X(5).                                       00002820
           02 MTABO OCCURS   2 TIMES .                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MCTYPOPERA   PIC X.                                     00002850
             03 MCTYPOPERC   PIC X.                                     00002860
             03 MCTYPOPERP   PIC X.                                     00002870
             03 MCTYPOPERH   PIC X.                                     00002880
             03 MCTYPOPERV   PIC X.                                     00002890
             03 MCTYPOPERO   PIC X(5).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MCNATOPERA   PIC X.                                     00002920
             03 MCNATOPERC   PIC X.                                     00002930
             03 MCNATOPERP   PIC X.                                     00002940
             03 MCNATOPERH   PIC X.                                     00002950
             03 MCNATOPERV   PIC X.                                     00002960
             03 MCNATOPERO   PIC X(5).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MLTYPOPERA   PIC X.                                     00002990
             03 MLTYPOPERC   PIC X.                                     00003000
             03 MLTYPOPERP   PIC X.                                     00003010
             03 MLTYPOPERH   PIC X.                                     00003020
             03 MLTYPOPERV   PIC X.                                     00003030
             03 MLTYPOPERO   PIC X(15).                                 00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MLNATOPERA   PIC X.                                     00003060
             03 MLNATOPERC   PIC X.                                     00003070
             03 MLNATOPERP   PIC X.                                     00003080
             03 MLNATOPERH   PIC X.                                     00003090
             03 MLNATOPERV   PIC X.                                     00003100
             03 MLNATOPERO   PIC X(15).                                 00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MLSECTION1A  PIC X.                                     00003130
             03 MLSECTION1C  PIC X.                                     00003140
             03 MLSECTION1P  PIC X.                                     00003150
             03 MLSECTION1H  PIC X.                                     00003160
             03 MLSECTION1V  PIC X.                                     00003170
             03 MLSECTION1O  PIC X(5).                                  00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MLSECTION2A  PIC X.                                     00003200
             03 MLSECTION2C  PIC X.                                     00003210
             03 MLSECTION2P  PIC X.                                     00003220
             03 MLSECTION2H  PIC X.                                     00003230
             03 MLSECTION2V  PIC X.                                     00003240
             03 MLSECTION2O  PIC X(5).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MLSECTION3A  PIC X.                                     00003270
             03 MLSECTION3C  PIC X.                                     00003280
             03 MLSECTION3P  PIC X.                                     00003290
             03 MLSECTION3H  PIC X.                                     00003300
             03 MLSECTION3V  PIC X.                                     00003310
             03 MLSECTION3O  PIC X(5).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MLRUB1A      PIC X.                                     00003340
             03 MLRUB1C PIC X.                                          00003350
             03 MLRUB1P PIC X.                                          00003360
             03 MLRUB1H PIC X.                                          00003370
             03 MLRUB1V PIC X.                                          00003380
             03 MLRUB1O      PIC X(5).                                  00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MLRUB2A      PIC X.                                     00003410
             03 MLRUB2C PIC X.                                          00003420
             03 MLRUB2P PIC X.                                          00003430
             03 MLRUB2H PIC X.                                          00003440
             03 MLRUB2V PIC X.                                          00003450
             03 MLRUB2O      PIC X(5).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MLRUB3A      PIC X.                                     00003480
             03 MLRUB3C PIC X.                                          00003490
             03 MLRUB3P PIC X.                                          00003500
             03 MLRUB3H PIC X.                                          00003510
             03 MLRUB3V PIC X.                                          00003520
             03 MLRUB3O      PIC X(5).                                  00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MCOMPTEA     PIC X.                                     00003550
             03 MCOMPTEC     PIC X.                                     00003560
             03 MCOMPTEP     PIC X.                                     00003570
             03 MCOMPTEH     PIC X.                                     00003580
             03 MCOMPTEV     PIC X.                                     00003590
             03 MCOMPTEO     PIC X(6).                                  00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MLCOMPTEA    PIC X.                                     00003620
             03 MLCOMPTEC    PIC X.                                     00003630
             03 MLCOMPTEP    PIC X.                                     00003640
             03 MLCOMPTEH    PIC X.                                     00003650
             03 MLCOMPTEV    PIC X.                                     00003660
             03 MLCOMPTEO    PIC X(15).                                 00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MSECTIONA    PIC X.                                     00003690
             03 MSECTIONC    PIC X.                                     00003700
             03 MSECTIONP    PIC X.                                     00003710
             03 MSECTIONH    PIC X.                                     00003720
             03 MSECTIONV    PIC X.                                     00003730
             03 MSECTIONO    PIC X(6).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MLSECTIONA   PIC X.                                     00003760
             03 MLSECTIONC   PIC X.                                     00003770
             03 MLSECTIONP   PIC X.                                     00003780
             03 MLSECTIONH   PIC X.                                     00003790
             03 MLSECTIONV   PIC X.                                     00003800
             03 MLSECTIONO   PIC X(15).                                 00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MRUBRIQUEA   PIC X.                                     00003830
             03 MRUBRIQUEC   PIC X.                                     00003840
             03 MRUBRIQUEP   PIC X.                                     00003850
             03 MRUBRIQUEH   PIC X.                                     00003860
             03 MRUBRIQUEV   PIC X.                                     00003870
             03 MRUBRIQUEO   PIC X(6).                                  00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MLRUBRIQUEA  PIC X.                                     00003900
             03 MLRUBRIQUEC  PIC X.                                     00003910
             03 MLRUBRIQUEP  PIC X.                                     00003920
             03 MLRUBRIQUEH  PIC X.                                     00003930
             03 MLRUBRIQUEV  PIC X.                                     00003940
             03 MLRUBRIQUEO  PIC X(15).                                 00003950
             03 FILLER       PIC X(2).                                  00003960
             03 MCONTREPA    PIC X.                                     00003970
             03 MCONTREPC    PIC X.                                     00003980
             03 MCONTREPP    PIC X.                                     00003990
             03 MCONTREPH    PIC X.                                     00004000
             03 MCONTREPV    PIC X.                                     00004010
             03 MCONTREPO    PIC X(6).                                  00004020
             03 FILLER       PIC X(2).                                  00004030
             03 MLCONTREPA   PIC X.                                     00004040
             03 MLCONTREPC   PIC X.                                     00004050
             03 MLCONTREPP   PIC X.                                     00004060
             03 MLCONTREPH   PIC X.                                     00004070
             03 MLCONTREPV   PIC X.                                     00004080
             03 MLCONTREPO   PIC X(15).                                 00004090
             03 FILLER       PIC X(2).                                  00004100
             03 MSECTIONCA   PIC X.                                     00004110
             03 MSECTIONCC   PIC X.                                     00004120
             03 MSECTIONCP   PIC X.                                     00004130
             03 MSECTIONCH   PIC X.                                     00004140
             03 MSECTIONCV   PIC X.                                     00004150
             03 MSECTIONCO   PIC X(6).                                  00004160
             03 FILLER       PIC X(2).                                  00004170
             03 MLSECTIONCA  PIC X.                                     00004180
             03 MLSECTIONCC  PIC X.                                     00004190
             03 MLSECTIONCP  PIC X.                                     00004200
             03 MLSECTIONCH  PIC X.                                     00004210
             03 MLSECTIONCV  PIC X.                                     00004220
             03 MLSECTIONCO  PIC X(15).                                 00004230
             03 FILLER       PIC X(2).                                  00004240
             03 MRUBRIQUECA  PIC X.                                     00004250
             03 MRUBRIQUECC  PIC X.                                     00004260
             03 MRUBRIQUECP  PIC X.                                     00004270
             03 MRUBRIQUECH  PIC X.                                     00004280
             03 MRUBRIQUECV  PIC X.                                     00004290
             03 MRUBRIQUECO  PIC X(6).                                  00004300
             03 FILLER       PIC X(2).                                  00004310
             03 MLRUBRIQUECA      PIC X.                                00004320
             03 MLRUBRIQUECC PIC X.                                     00004330
             03 MLRUBRIQUECP PIC X.                                     00004340
             03 MLRUBRIQUECH PIC X.                                     00004350
             03 MLRUBRIQUECV PIC X.                                     00004360
             03 MLRUBRIQUECO      PIC X(15).                            00004370
      * ZONE CMD AIDA                                                   00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MLIBERRA  PIC X.                                          00004400
           02 MLIBERRC  PIC X.                                          00004410
           02 MLIBERRP  PIC X.                                          00004420
           02 MLIBERRH  PIC X.                                          00004430
           02 MLIBERRV  PIC X.                                          00004440
           02 MLIBERRO  PIC X(79).                                      00004450
      * CODE TRANSACTION                                                00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MCODTRAA  PIC X.                                          00004480
           02 MCODTRAC  PIC X.                                          00004490
           02 MCODTRAP  PIC X.                                          00004500
           02 MCODTRAH  PIC X.                                          00004510
           02 MCODTRAV  PIC X.                                          00004520
           02 MCODTRAO  PIC X(4).                                       00004530
      * CICS DE TRAVAIL                                                 00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MCICSA    PIC X.                                          00004560
           02 MCICSC    PIC X.                                          00004570
           02 MCICSP    PIC X.                                          00004580
           02 MCICSH    PIC X.                                          00004590
           02 MCICSV    PIC X.                                          00004600
           02 MCICSO    PIC X(5).                                       00004610
      * NETNAME                                                         00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MNETNAMA  PIC X.                                          00004640
           02 MNETNAMC  PIC X.                                          00004650
           02 MNETNAMP  PIC X.                                          00004660
           02 MNETNAMH  PIC X.                                          00004670
           02 MNETNAMV  PIC X.                                          00004680
           02 MNETNAMO  PIC X(8).                                       00004690
      * CODE TERMINAL                                                   00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MSCREENA  PIC X.                                          00004720
           02 MSCREENC  PIC X.                                          00004730
           02 MSCREENP  PIC X.                                          00004740
           02 MSCREENH  PIC X.                                          00004750
           02 MSCREENV  PIC X.                                          00004760
           02 MSCREENO  PIC X(4).                                       00004770
                                                                                
