      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GD LIVRE AUXILIARISE MODEL                                00000020
      ***************************************************************** 00000030
       01   EFM15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNENTITEI      PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLENTITEI      PIC X(34).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMANDEL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDEMANDEF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNDEMANDEI     PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDCREATIONI    PIC X(10).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSERL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MUSERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MUSERF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MUSERI    PIC X(25).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMFICHEL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MWMFICHEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWMFICHEF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MWMFICHEI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAPIERL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MWPAPIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPAPIERF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MWPAPIERI      PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETABI   PIC X(3).                                       00000470
           02 MWNTRID OCCURS   6 TIMES .                                00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWNTRIL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MWNTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWNTRIF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MWNTRII      PIC X.                                     00000520
           02 MWTOTALD OCCURS   6 TIMES .                               00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOTALL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MWTOTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTOTALF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MWTOTALI     PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPAUXL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MWTYPAUXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPAUXF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWTYPAUXI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMINL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNAUXMINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMINF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNAUXMINI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXMAXL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNAUXMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUXMAXF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNAUXMAXI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSMINL    COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNTIERSMINL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNTIERSMINF    PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNTIERSMINI    PIC X(8).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSMAXL    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNTIERSMAXL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNTIERSMAXF    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNTIERSMAXI    PIC X(8).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCPTEAUXMINL  COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNCPTEAUXMINL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNCPTEAUXMINF  PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNCPTEAUXMINI  PIC X(6).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCPTEAUXMAXL  COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MNCPTEAUXMAXL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNCPTEAUXMAXF  PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNCPTEAUXMAXI  PIC X(6).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSECTIONMINL  COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MNSECTIONMINL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSECTIONMINF  PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNSECTIONMINI  PIC X(6).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSECTIONMAXL  COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MNSECTIONMAXL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSECTIONMAXF  PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNSECTIONMAXI  PIC X(6).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRUBRIQUEMINL      COMP PIC S9(4).                       00000940
      *--                                                                       
           02 MNRUBRIQUEMINL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MNRUBRIQUEMINF      PIC X.                                00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNRUBRIQUEMINI      PIC X(6).                             00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRUBRIQUEMAXL      COMP PIC S9(4).                       00000980
      *--                                                                       
           02 MNRUBRIQUEMAXL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MNRUBRIQUEMAXF      PIC X.                                00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNRUBRIQUEMAXI      PIC X(6).                             00001010
           02 MCNATD OCCURS   10 TIMES .                                00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MCNATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCNATF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCNATI  PIC X(3).                                       00001060
           02 MCNATED OCCURS   10 TIMES .                               00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATEL      COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MCNATEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCNATEF      PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MCNATEI      PIC X(3).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTTRITIERSL   COMP PIC S9(4).                            00001120
      *--                                                                       
           02 MWTTRITIERSL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWTTRITIERSF   PIC X.                                     00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MWTTRITIERSI   PIC X.                                     00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTTRIECRITL   COMP PIC S9(4).                            00001160
      *--                                                                       
           02 MWTTRIECRITL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MWTTRIECRITF   PIC X.                                     00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MWTTRIECRITI   PIC X.                                     00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCOMPTESOLDEL      COMP PIC S9(4).                       00001200
      *--                                                                       
           02 MWCOMPTESOLDEL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MWCOMPTESOLDEF      PIC X.                                00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MWCOMPTESOLDEI      PIC X.                                00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00001240
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MCDEVISEI      PIC X(3).                                  00001270
      * ZONE CMD AIDA                                                   00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MLIBERRI  PIC X(78).                                      00001320
      * CODE TRANSACTION                                                00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCODTRAI  PIC X(4).                                       00001370
      * CICS DE TRAVAIL                                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      * NETNAME                                                         00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MNETNAMI  PIC X(8).                                       00001470
      * CODE TERMINAL                                                   00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001490
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001500
           02 FILLER    PIC X(4).                                       00001510
           02 MSCREENI  PIC X(5).                                       00001520
      ***************************************************************** 00001530
      * GCT - GD LIVRE AUXILIARISE MODEL                                00001540
      ***************************************************************** 00001550
       01   EFM15O REDEFINES EFM15I.                                    00001560
           02 FILLER    PIC X(12).                                      00001570
      * DATE DU JOUR                                                    00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MDATJOUA  PIC X.                                          00001600
           02 MDATJOUC  PIC X.                                          00001610
           02 MDATJOUP  PIC X.                                          00001620
           02 MDATJOUH  PIC X.                                          00001630
           02 MDATJOUV  PIC X.                                          00001640
           02 MDATJOUO  PIC X(10).                                      00001650
      * HEURE                                                           00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MTIMJOUA  PIC X.                                          00001680
           02 MTIMJOUC  PIC X.                                          00001690
           02 MTIMJOUP  PIC X.                                          00001700
           02 MTIMJOUH  PIC X.                                          00001710
           02 MTIMJOUV  PIC X.                                          00001720
           02 MTIMJOUO  PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNENTITEA      PIC X.                                     00001750
           02 MNENTITEC PIC X.                                          00001760
           02 MNENTITEP PIC X.                                          00001770
           02 MNENTITEH PIC X.                                          00001780
           02 MNENTITEV PIC X.                                          00001790
           02 MNENTITEO      PIC X(5).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLENTITEA      PIC X.                                     00001820
           02 MLENTITEC PIC X.                                          00001830
           02 MLENTITEP PIC X.                                          00001840
           02 MLENTITEH PIC X.                                          00001850
           02 MLENTITEV PIC X.                                          00001860
           02 MLENTITEO      PIC X(34).                                 00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNDEMANDEA     PIC X.                                     00001890
           02 MNDEMANDEC     PIC X.                                     00001900
           02 MNDEMANDEP     PIC X.                                     00001910
           02 MNDEMANDEH     PIC X.                                     00001920
           02 MNDEMANDEV     PIC X.                                     00001930
           02 MNDEMANDEO     PIC X(2).                                  00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDCREATIONA    PIC X.                                     00001960
           02 MDCREATIONC    PIC X.                                     00001970
           02 MDCREATIONP    PIC X.                                     00001980
           02 MDCREATIONH    PIC X.                                     00001990
           02 MDCREATIONV    PIC X.                                     00002000
           02 MDCREATIONO    PIC X(10).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MUSERA    PIC X.                                          00002030
           02 MUSERC    PIC X.                                          00002040
           02 MUSERP    PIC X.                                          00002050
           02 MUSERH    PIC X.                                          00002060
           02 MUSERV    PIC X.                                          00002070
           02 MUSERO    PIC X(25).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MWMFICHEA      PIC X.                                     00002100
           02 MWMFICHEC PIC X.                                          00002110
           02 MWMFICHEP PIC X.                                          00002120
           02 MWMFICHEH PIC X.                                          00002130
           02 MWMFICHEV PIC X.                                          00002140
           02 MWMFICHEO      PIC X.                                     00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MWPAPIERA      PIC X.                                     00002170
           02 MWPAPIERC PIC X.                                          00002180
           02 MWPAPIERP PIC X.                                          00002190
           02 MWPAPIERH PIC X.                                          00002200
           02 MWPAPIERV PIC X.                                          00002210
           02 MWPAPIERO      PIC X.                                     00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNETABA   PIC X.                                          00002240
           02 MNETABC   PIC X.                                          00002250
           02 MNETABP   PIC X.                                          00002260
           02 MNETABH   PIC X.                                          00002270
           02 MNETABV   PIC X.                                          00002280
           02 MNETABO   PIC X(3).                                       00002290
           02 DFHMS1 OCCURS   6 TIMES .                                 00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MWNTRIA      PIC X.                                     00002320
             03 MWNTRIC PIC X.                                          00002330
             03 MWNTRIP PIC X.                                          00002340
             03 MWNTRIH PIC X.                                          00002350
             03 MWNTRIV PIC X.                                          00002360
             03 MWNTRIO      PIC X.                                     00002370
           02 DFHMS2 OCCURS   6 TIMES .                                 00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MWTOTALA     PIC X.                                     00002400
             03 MWTOTALC     PIC X.                                     00002410
             03 MWTOTALP     PIC X.                                     00002420
             03 MWTOTALH     PIC X.                                     00002430
             03 MWTOTALV     PIC X.                                     00002440
             03 MWTOTALO     PIC X.                                     00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MWTYPAUXA      PIC X.                                     00002470
           02 MWTYPAUXC PIC X.                                          00002480
           02 MWTYPAUXP PIC X.                                          00002490
           02 MWTYPAUXH PIC X.                                          00002500
           02 MWTYPAUXV PIC X.                                          00002510
           02 MWTYPAUXO      PIC X.                                     00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MNAUXMINA      PIC X.                                     00002540
           02 MNAUXMINC PIC X.                                          00002550
           02 MNAUXMINP PIC X.                                          00002560
           02 MNAUXMINH PIC X.                                          00002570
           02 MNAUXMINV PIC X.                                          00002580
           02 MNAUXMINO      PIC X(3).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNAUXMAXA      PIC X.                                     00002610
           02 MNAUXMAXC PIC X.                                          00002620
           02 MNAUXMAXP PIC X.                                          00002630
           02 MNAUXMAXH PIC X.                                          00002640
           02 MNAUXMAXV PIC X.                                          00002650
           02 MNAUXMAXO      PIC X(3).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MNTIERSMINA    PIC X.                                     00002680
           02 MNTIERSMINC    PIC X.                                     00002690
           02 MNTIERSMINP    PIC X.                                     00002700
           02 MNTIERSMINH    PIC X.                                     00002710
           02 MNTIERSMINV    PIC X.                                     00002720
           02 MNTIERSMINO    PIC X(8).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNTIERSMAXA    PIC X.                                     00002750
           02 MNTIERSMAXC    PIC X.                                     00002760
           02 MNTIERSMAXP    PIC X.                                     00002770
           02 MNTIERSMAXH    PIC X.                                     00002780
           02 MNTIERSMAXV    PIC X.                                     00002790
           02 MNTIERSMAXO    PIC X(8).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MNCPTEAUXMINA  PIC X.                                     00002820
           02 MNCPTEAUXMINC  PIC X.                                     00002830
           02 MNCPTEAUXMINP  PIC X.                                     00002840
           02 MNCPTEAUXMINH  PIC X.                                     00002850
           02 MNCPTEAUXMINV  PIC X.                                     00002860
           02 MNCPTEAUXMINO  PIC X(6).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MNCPTEAUXMAXA  PIC X.                                     00002890
           02 MNCPTEAUXMAXC  PIC X.                                     00002900
           02 MNCPTEAUXMAXP  PIC X.                                     00002910
           02 MNCPTEAUXMAXH  PIC X.                                     00002920
           02 MNCPTEAUXMAXV  PIC X.                                     00002930
           02 MNCPTEAUXMAXO  PIC X(6).                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MNSECTIONMINA  PIC X.                                     00002960
           02 MNSECTIONMINC  PIC X.                                     00002970
           02 MNSECTIONMINP  PIC X.                                     00002980
           02 MNSECTIONMINH  PIC X.                                     00002990
           02 MNSECTIONMINV  PIC X.                                     00003000
           02 MNSECTIONMINO  PIC X(6).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MNSECTIONMAXA  PIC X.                                     00003030
           02 MNSECTIONMAXC  PIC X.                                     00003040
           02 MNSECTIONMAXP  PIC X.                                     00003050
           02 MNSECTIONMAXH  PIC X.                                     00003060
           02 MNSECTIONMAXV  PIC X.                                     00003070
           02 MNSECTIONMAXO  PIC X(6).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MNRUBRIQUEMINA      PIC X.                                00003100
           02 MNRUBRIQUEMINC PIC X.                                     00003110
           02 MNRUBRIQUEMINP PIC X.                                     00003120
           02 MNRUBRIQUEMINH PIC X.                                     00003130
           02 MNRUBRIQUEMINV PIC X.                                     00003140
           02 MNRUBRIQUEMINO      PIC X(6).                             00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNRUBRIQUEMAXA      PIC X.                                00003170
           02 MNRUBRIQUEMAXC PIC X.                                     00003180
           02 MNRUBRIQUEMAXP PIC X.                                     00003190
           02 MNRUBRIQUEMAXH PIC X.                                     00003200
           02 MNRUBRIQUEMAXV PIC X.                                     00003210
           02 MNRUBRIQUEMAXO      PIC X(6).                             00003220
           02 DFHMS3 OCCURS   10 TIMES .                                00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MCNATA  PIC X.                                          00003250
             03 MCNATC  PIC X.                                          00003260
             03 MCNATP  PIC X.                                          00003270
             03 MCNATH  PIC X.                                          00003280
             03 MCNATV  PIC X.                                          00003290
             03 MCNATO  PIC X(3).                                       00003300
           02 DFHMS4 OCCURS   10 TIMES .                                00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MCNATEA      PIC X.                                     00003330
             03 MCNATEC PIC X.                                          00003340
             03 MCNATEP PIC X.                                          00003350
             03 MCNATEH PIC X.                                          00003360
             03 MCNATEV PIC X.                                          00003370
             03 MCNATEO      PIC X(3).                                  00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MWTTRITIERSA   PIC X.                                     00003400
           02 MWTTRITIERSC   PIC X.                                     00003410
           02 MWTTRITIERSP   PIC X.                                     00003420
           02 MWTTRITIERSH   PIC X.                                     00003430
           02 MWTTRITIERSV   PIC X.                                     00003440
           02 MWTTRITIERSO   PIC X.                                     00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MWTTRIECRITA   PIC X.                                     00003470
           02 MWTTRIECRITC   PIC X.                                     00003480
           02 MWTTRIECRITP   PIC X.                                     00003490
           02 MWTTRIECRITH   PIC X.                                     00003500
           02 MWTTRIECRITV   PIC X.                                     00003510
           02 MWTTRIECRITO   PIC X.                                     00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MWCOMPTESOLDEA      PIC X.                                00003540
           02 MWCOMPTESOLDEC PIC X.                                     00003550
           02 MWCOMPTESOLDEP PIC X.                                     00003560
           02 MWCOMPTESOLDEH PIC X.                                     00003570
           02 MWCOMPTESOLDEV PIC X.                                     00003580
           02 MWCOMPTESOLDEO      PIC X.                                00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MCDEVISEA      PIC X.                                     00003610
           02 MCDEVISEC PIC X.                                          00003620
           02 MCDEVISEP PIC X.                                          00003630
           02 MCDEVISEH PIC X.                                          00003640
           02 MCDEVISEV PIC X.                                          00003650
           02 MCDEVISEO      PIC X(3).                                  00003660
      * ZONE CMD AIDA                                                   00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MLIBERRA  PIC X.                                          00003690
           02 MLIBERRC  PIC X.                                          00003700
           02 MLIBERRP  PIC X.                                          00003710
           02 MLIBERRH  PIC X.                                          00003720
           02 MLIBERRV  PIC X.                                          00003730
           02 MLIBERRO  PIC X(78).                                      00003740
      * CODE TRANSACTION                                                00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCODTRAA  PIC X.                                          00003770
           02 MCODTRAC  PIC X.                                          00003780
           02 MCODTRAP  PIC X.                                          00003790
           02 MCODTRAH  PIC X.                                          00003800
           02 MCODTRAV  PIC X.                                          00003810
           02 MCODTRAO  PIC X(4).                                       00003820
      * CICS DE TRAVAIL                                                 00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MCICSA    PIC X.                                          00003850
           02 MCICSC    PIC X.                                          00003860
           02 MCICSP    PIC X.                                          00003870
           02 MCICSH    PIC X.                                          00003880
           02 MCICSV    PIC X.                                          00003890
           02 MCICSO    PIC X(5).                                       00003900
      * NETNAME                                                         00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MNETNAMA  PIC X.                                          00003930
           02 MNETNAMC  PIC X.                                          00003940
           02 MNETNAMP  PIC X.                                          00003950
           02 MNETNAMH  PIC X.                                          00003960
           02 MNETNAMV  PIC X.                                          00003970
           02 MNETNAMO  PIC X(8).                                       00003980
      * CODE TERMINAL                                                   00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MSCREENA  PIC X.                                          00004010
           02 MSCREENC  PIC X.                                          00004020
           02 MSCREENP  PIC X.                                          00004030
           02 MSCREENH  PIC X.                                          00004040
           02 MSCREENV  PIC X.                                          00004050
           02 MSCREENO  PIC X(5).                                       00004060
                                                                                
