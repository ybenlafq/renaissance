      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSG0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSG0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSG0300.                                                            
           02  SG03-NSOC                                                        
               PIC X(0003).                                                     
           02  SG03-NETAB                                                       
               PIC X(0003).                                                     
           02  SG03-NETABADM                                                    
               PIC X(0003).                                                     
           02  SG03-COMPTE                                                      
               PIC X(0006).                                                     
           02  SG03-NTIERS                                                      
               PIC X(0008).                                                     
           02  SG03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSG0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSG0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-NETABADM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-NETABADM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-NTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-NTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SG03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SG03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
