      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM13-LONG             PIC S9(4) COMP-3 VALUE +160.                
       01  TS-FM13-RECORD.                                                      
           10 TS-FM13-NDEMANDE      PIC X(2).                                   
           10 TS-FM13-DCREATION     PIC X(10).                                  
           10 TS-FM13-CACID         PIC X(8).                                   
           10 TS-FM13-USER          PIC X(25).                                  
           10 TS-FM13-WMFICHE       PIC X.                                      
           10 TS-FM13-WPAPIER       PIC X.                                      
           10 TS-FM13-NETAB         PIC X(3).                                   
AM         10 TS-FM13-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM13-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM13-WTYPAUX       PIC X.                                      
           10 TS-FM13-NTIERSMIN     PIC X(8).                                   
           10 TS-FM13-NTIERSMAX     PIC X(8).                                   
           10 TS-FM13-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM13-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM13-WTOTAL        PIC X    OCCURS 4.                          
           10 TS-FM13-WNTRI         PIC X    OCCURS 4.                          
           10 TS-FM13-CNAT          PIC X(3) OCCURS 10.                         
           10 TS-FM13-CNATE         PIC X(3) OCCURS 10.                         
           10 TS-FM13-WTTRITIERS    PIC X.                                      
           10 TS-FM13-WTTRIECRIT    PIC X.                                      
           10 TS-FM13-WEDTTIERS     PIC X.                                      
           10 TS-FM13-WPIECELETTRE  PIC X.                                      
           10 TS-FM13-CDEVISE       PIC X(3).                                   
                                                                                
