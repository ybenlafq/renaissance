      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - liste des comptes                                         00000020
      ***************************************************************** 00000030
       01   EFM23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPCGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCPCGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCPCGF    PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MCPCGI    PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPCGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLPCGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLPCGF    PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLPCGI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCPTF     PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MCPTI     PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCMASQUEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSCMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSCMASQUEF     PIC X.                                     00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MSCMASQUEI     PIC X(6).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNAUXL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSNAUXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNAUXF   PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MSNAUXI   PIC X(3).                                       00000410
           02 LIGNEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTIONL  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MSELECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSELECTIONF  PIC X.                                     00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MSELECTIONI  PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MCOMPTEI     PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTEL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLCOMPTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMPTEF    PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MLCOMPTEI    PIC X(30).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MMASQUEI     PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNAUXF  PIC X.                                          00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MNAUXI  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWAUXILIARISEL    COMP PIC S9(4).                       00000630
      *--                                                                       
             03 MWAUXILIARISEL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MWAUXILIARISEF    PIC X.                                00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MWAUXILIARISEI    PIC X.                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCOLLECTIFL      COMP PIC S9(4).                       00000670
      *--                                                                       
             03 MWCOLLECTIFL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MWCOLLECTIFF      PIC X.                                00000680
             03 FILLER  PIC X(2).                                       00000690
             03 MWCOLLECTIFI      PIC X.                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCLOTUREL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MWCLOTUREL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWCLOTUREF   PIC X.                                     00000720
             03 FILLER  PIC X(2).                                       00000730
             03 MWCLOTUREI   PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MLIBERRI  PIC X(79).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * GCT - liste des comptes                                         00000920
      ***************************************************************** 00000930
       01   EFM23O REDEFINES EFM23I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUO  PIC X(10).                                      00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MTIMJOUA  PIC X.                                          00001020
           02 MTIMJOUC  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEH    PIC X.                                          00001090
           02 MPAGEO    PIC ZZ9.                                        00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MPAGETOTA      PIC X.                                     00001120
           02 MPAGETOTC PIC X.                                          00001130
           02 MPAGETOTH PIC X.                                          00001140
           02 MPAGETOTO      PIC ZZ9.                                   00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCPCGA    PIC X.                                          00001170
           02 MCPCGC    PIC X.                                          00001180
           02 MCPCGH    PIC X.                                          00001190
           02 MCPCGO    PIC X(4).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MLPCGA    PIC X.                                          00001220
           02 MLPCGC    PIC X.                                          00001230
           02 MLPCGH    PIC X.                                          00001240
           02 MLPCGO    PIC X(20).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCPTA     PIC X.                                          00001270
           02 MCPTC     PIC X.                                          00001280
           02 MCPTH     PIC X.                                          00001290
           02 MCPTO     PIC X(6).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MSCMASQUEA     PIC X.                                     00001320
           02 MSCMASQUEC     PIC X.                                     00001330
           02 MSCMASQUEH     PIC X.                                     00001340
           02 MSCMASQUEO     PIC X(6).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MSNAUXA   PIC X.                                          00001370
           02 MSNAUXC   PIC X.                                          00001380
           02 MSNAUXH   PIC X.                                          00001390
           02 MSNAUXO   PIC X(3).                                       00001400
           02 LIGNEO OCCURS   12 TIMES .                                00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MSELECTIONA  PIC X.                                     00001430
             03 MSELECTIONC  PIC X.                                     00001440
             03 MSELECTIONH  PIC X.                                     00001450
             03 MSELECTIONO  PIC X.                                     00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MCOMPTEA     PIC X.                                     00001480
             03 MCOMPTEC     PIC X.                                     00001490
             03 MCOMPTEH     PIC X.                                     00001500
             03 MCOMPTEO     PIC X(6).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MLCOMPTEA    PIC X.                                     00001530
             03 MLCOMPTEC    PIC X.                                     00001540
             03 MLCOMPTEH    PIC X.                                     00001550
             03 MLCOMPTEO    PIC X(30).                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MMASQUEA     PIC X.                                     00001580
             03 MMASQUEC     PIC X.                                     00001590
             03 MMASQUEH     PIC X.                                     00001600
             03 MMASQUEO     PIC X(6).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNAUXA  PIC X.                                          00001630
             03 MNAUXC  PIC X.                                          00001640
             03 MNAUXH  PIC X.                                          00001650
             03 MNAUXO  PIC X(3).                                       00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MWAUXILIARISEA    PIC X.                                00001680
             03 MWAUXILIARISEC    PIC X.                                00001690
             03 MWAUXILIARISEH    PIC X.                                00001700
             03 MWAUXILIARISEO    PIC X.                                00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MWCOLLECTIFA      PIC X.                                00001730
             03 MWCOLLECTIFC PIC X.                                     00001740
             03 MWCOLLECTIFH PIC X.                                     00001750
             03 MWCOLLECTIFO      PIC X.                                00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MWCLOTUREA   PIC X.                                     00001780
             03 MWCLOTUREC   PIC X.                                     00001790
             03 MWCLOTUREH   PIC X.                                     00001800
             03 MWCLOTUREO   PIC X.                                     00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBERRA  PIC X.                                          00001830
           02 MLIBERRC  PIC X.                                          00001840
           02 MLIBERRH  PIC X.                                          00001850
           02 MLIBERRO  PIC X(79).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAH  PIC X.                                          00001900
           02 MCODTRAO  PIC X(4).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCICSA    PIC X.                                          00001930
           02 MCICSC    PIC X.                                          00001940
           02 MCICSH    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MSCREENA  PIC X.                                          00001980
           02 MSCREENC  PIC X.                                          00001990
           02 MSCREENH  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
