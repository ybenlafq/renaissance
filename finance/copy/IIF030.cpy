      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIF030 AU 02/09/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,01,BI,A,                          *        
      *                           08,03,BI,A,                          *        
      *                           11,03,BI,A,                          *        
      *                           14,03,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIF030.                                                        
            05 NOMETAT-IIF030           PIC X(6) VALUE 'IIF030'.                
            05 RUPTURES-IIF030.                                                 
           10 IIF030-L0                 PIC X(01).                      007  001
           10 IIF030-CORGANISME         PIC X(03).                      008  003
           10 IIF030-NSOCIETE           PIC X(03).                      011  003
           10 IIF030-NLIEU              PIC X(03).                      014  003
           10 IIF030-CMODPAI            PIC X(03).                      017  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIF030-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IIF030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIF030.                                                   
           10 IIF030-LLIEU              PIC X(25).                      022  025
           10 IIF030-LMODPAI            PIC X(25).                      047  025
           10 IIF030-LORGANISME         PIC X(25).                      072  025
           10 IIF030-MTBRUT             PIC S9(09)V9(2) COMP-3.         097  006
           10 IIF030-MTCOM              PIC S9(07)V9(2) COMP-3.         103  005
           10 IIF030-MTNET              PIC S9(09)V9(2) COMP-3.         108  006
           10 IIF030-NBFAC              PIC S9(05)      COMP-3.         114  003
            05 FILLER                      PIC X(396).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIF030-LONG           PIC S9(4)   COMP  VALUE +116.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIF030-LONG           PIC S9(4) COMP-5  VALUE +116.           
                                                                                
      *}                                                                        
