      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE IF2000                       
      ******************************************************************        
      *                                                                         
       CLEF-IF2000             SECTION.                                         
      *                                                                         
           MOVE 'RVIF2000          '       TO   TABLE-NAME.                     
           MOVE 'IF2000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-IF2000. EXIT.                                                   
                EJECT                                                           
                                                                                
