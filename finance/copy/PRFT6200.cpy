      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE FT6200                       
      ******************************************************************        
      *                                                                         
       CLEF-FT6200             SECTION.                                         
      *                                                                         
           MOVE 'RVFT6200          '       TO   TABLE-NAME.                     
           MOVE 'FT6200'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-FT6200. EXIT.                                                   
                EJECT                                                           
                                                                                
