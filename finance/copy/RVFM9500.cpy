      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RVFM9500)                                  *        
      *        LIBRARY(DSA016.DEVL.COPY.COBOL(RVFM9500))               *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(FM95-)                                            *        
      *        STRUCTURE(RVFM9500)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVFM9500 TABLE                               
           ( CINTERFACE                     CHAR(5) NOT NULL,                   
             CTYPOPER                       CHAR(5) NOT NULL,                   
             CNATOPER                       CHAR(5) NOT NULL,                   
             CINTERFACE_CORR                CHAR(5) NOT NULL,                   
             CTYPOPER_CORR                  CHAR(5) NOT NULL,                   
             CNATOPER_CORR                  CHAR(5) NOT NULL,                   
             CDTIERS                        CHAR(4) NOT NULL,                   
             NSOCCPTA                       CHAR(3) NOT NULL,                   
             NLIEUCPTA                      CHAR(3) NOT NULL,                   
             NPIECE                         CHAR(2) NOT NULL,                   
             DMAJ                           CHAR(8) NOT NULL,                   
             DSYST                          DECIMAL(13, 0) NOT NULL             
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFM9500                    *        
      ******************************************************************        
       01  RVFM9500.                                                            
      *                       CINTERFACE                                        
           10 FM95-CINTERFACE      PIC X(5).                                    
      *                       CTYPOPER                                          
           10 FM95-CTYPOPER        PIC X(5).                                    
      *                       CNATOPER                                          
           10 FM95-CNATOPER        PIC X(5).                                    
      *                       CINTERFACE_CORR                                   
           10 FM95-CINTERFACE-CORR                                              
              PIC X(5).                                                         
      *                       CTYPOPER_CORR                                     
           10 FM95-CTYPOPER-CORR   PIC X(5).                                    
      *                       CNATOPER_CORR                                     
           10 FM95-CNATOPER-CORR   PIC X(5).                                    
      *                       CDTIERS                                           
           10 FM95-CDTIERS         PIC X(4).                                    
      *                       NSOCCPTA                                          
           10 FM95-NSOCCPTA        PIC X(3).                                    
      *                       NLIEUCPTA                                         
           10 FM95-NLIEUCPTA       PIC X(3).                                    
      *                       NPIECE                                            
           10 FM95-NPIECE          PIC X(2).                                    
      *                       DMAJ                                              
           10 FM95-DMAJ            PIC X(8).                                    
      *                       DSYST                                             
           10 FM95-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *        
      ******************************************************************        
                                                                                
