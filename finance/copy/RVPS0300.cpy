      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPS0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPS0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPS0300.                                                            
           02  PS03-NSOC                                                        
               PIC X(0003).                                                     
           02  PS03-CGCPLT                                                      
               PIC X(0005).                                                     
           02  PS03-NGCPLT                                                      
               PIC X(0008).                                                     
           02  PS03-NLIEU                                                       
               PIC X(0003).                                                     
           02  PS03-NVENTE                                                      
               PIC X(0007).                                                     
           02  PS03-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  PS03-CINSEE                                                      
               PIC X(0005).                                                     
           02  PS03-NCODIC                                                      
               PIC X(0007).                                                     
           02  PS03-MTGCPLT                                                     
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS03-MTREMB                                                      
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS03-DREMB                                                       
               PIC X(0008).                                                     
           02  PS03-NOMCLI                                                      
               PIC X(0015).                                                     
           02  PS03-NCHEQUE                                                     
               PIC X(0007).                                                     
           02  PS03-MTCT                                                        
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS03-MTLT                                                        
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS03-MTTVA                                                       
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PS03-DSAISIE                                                     
               PIC X(0008).                                                     
           02  PS03-DTRAITR                                                     
               PIC X(0008).                                                     
           02  PS03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPS0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPS0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-MTGCPLT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-MTGCPLT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-MTREMB-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-MTREMB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-DREMB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-DREMB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NOMCLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NOMCLI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-NCHEQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-NCHEQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-MTCT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-MTCT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-MTLT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-MTLT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-MTTVA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-MTTVA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-DTRAITR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-DTRAITR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
