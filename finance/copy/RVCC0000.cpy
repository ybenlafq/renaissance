      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **************************************************************            
      *   COPY DE LA VUE RVCC0000 : LISTE DES CAISSES NON DEVERSSES             
      **************************************************************            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCC0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0000.                                                            
           02  CC00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  CC00-NLIEU                                                       
               PIC X(0003).                                                     
           02  CC00-DCAISSE                                                     
               PIC X(0008).                                                     
           02  CC00-NCAISSE                                                     
               PIC X(0003).                                                     
           02  CC00-CREJET                                                      
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCC0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCC0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CC00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CC00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC00-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CC00-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC00-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CC00-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CC00-CREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CC00-CREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
