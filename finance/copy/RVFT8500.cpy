      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFT8500                                     00020000
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT8500                 00060000
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFT8500.                                                    00090000
           02  FT85-SOCREF                                              00100000
               PIC X(0005).                                             00110000
           02  FT85-CINTERFACE                                          00120000
               PIC X(0005).                                             00130000
           02  FT85-CNATOPER                                            00140000
               PIC X(0005).                                             00150000
           02  FT85-CTYPOPER                                            00160000
               PIC X(0005).                                             00170000
           02  FT85-CRITERE1                                            00180000
               PIC X(0005).                                             00190000
           02  FT85-CRITERE2                                            00200000
               PIC X(0005).                                             00210000
           02  FT85-CRITERE3                                            00220000
               PIC X(0005).                                             00230000
           02  FT85-NSOCIETE                                            00240000
               PIC X(0003).                                             00250000
           02  FT85-NLIEU                                               00260000
               PIC X(0003).                                             00270000
           02  FT85-SECTION                                             00280000
               PIC X(0006).                                             00290000
           02  FT85-SECTIONC                                            00300002
               PIC X(0006).                                             00310002
           02  FT85-DEFFET                                              00320002
               PIC X(0008).                                             00330002
           02  FT85-DMAJ                                                00340002
               PIC X(0008).                                             00350002
           02  FT85-DSYST                                               00360002
               PIC S9(13) COMP-3.                                       00370002
      *                                                                 00380002
      *---------------------------------------------------------        00390002
      *   LISTE DES FLAGS DE LA TABLE RVFT8500                          00400002
      *---------------------------------------------------------        00410002
      *                                                                 00420002
       01  RVFT8500-FLAGS.                                              00430002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-SOCREF-F                                            00440002
      *        PIC S9(4) COMP.                                          00450002
      *--                                                                       
           02  FT85-SOCREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CINTERFACE-F                                        00460000
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  FT85-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CNATOPER-F                                          00480000
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  FT85-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CTYPOPER-F                                          00500000
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  FT85-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CRITERE1-F                                          00520000
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  FT85-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CRITERE2-F                                          00540000
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  FT85-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-CRITERE3-F                                          00560000
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  FT85-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-NSOCIETE-F                                          00580000
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  FT85-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-NLIEU-F                                             00600000
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  FT85-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-SECTION-F                                           00620000
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  FT85-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-SECTIONC-F                                          00640002
      *        PIC S9(4) COMP.                                          00650002
      *--                                                                       
           02  FT85-SECTIONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-DEFFET-F                                            00660002
      *        PIC S9(4) COMP.                                          00670002
      *--                                                                       
           02  FT85-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-DMAJ-F                                              00680002
      *        PIC S9(4) COMP.                                          00690002
      *--                                                                       
           02  FT85-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT85-DSYST-F                                             00700002
      *        PIC S9(4) COMP.                                          00710002
      *--                                                                       
           02  FT85-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00720000
                                                                                
