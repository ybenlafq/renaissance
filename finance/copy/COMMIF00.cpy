      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-IF00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-IF00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02  COMM-IF00-NOM-PROG       PIC X(8).                               
           02  COMM-IF00-STABLES.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-AI                 PIC S9(4)    COMP.                   
      *--                                                                       
               05  COMM-AI                 PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-IFORG-NOCCURS      PIC S9(4)    COMP.                   
      *--                                                                       
               05  COMM-IFORG-NOCCURS      PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-IFTMT-NOCCURS      PIC S9(4)    COMP.                   
      *--                                                                       
               05  COMM-IFTMT-NOCCURS      PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-IFTCT-NOCCURS      PIC S9(4)    COMP.                   
      *--                                                                       
               05  COMM-IFTCT-NOCCURS      PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-IFMPA-NOCCURS      PIC S9(4)    COMP.                   
      *--                                                                       
               05  COMM-IFMPA-NOCCURS      PIC S9(4) COMP-5.                    
      *}                                                                        
               05  COMM-IF00-IFORG      OCCURS 15.                              
                   10  COMM-IFORG-CORGAN     PIC XXX.                           
                   10  COMM-IFORG-LORGAN     PIC X(20).                         
                   10  COMM-IFORG-LGIDENT    PIC 99.                            
               05  COMM-IF00-IFTMT      OCCURS 20.                              
                   10  COMM-IFTMT-CTYPMONT   PIC X(5).                          
                   10  COMM-IFTMT-LTYPMONT   PIC X(20).                         
               05  COMM-IF00-IFTCT      OCCURS 5.                               
                   10  COMM-IFTCT-CTYPCTA   PIC XXX.                            
                   10  COMM-IFTCT-LTYPCTA   PIC X(20).                          
               05  COMM-IF00-IFMPA      OCCURS 20.                              
                   10  COMM-IFMPA-CMODPAI   PIC X(3).                           
                   10  COMM-IFMPA-LMODPAI   PIC X(20).                          
           02 COMM-IF00-APPLI.                                          00740000
              03 COMM-IF00-LGIDENT        PIC S999     COMP-3.          00970003
              03 COMM-IF00-NPAGE          PIC S999     COMP-3.          00970003
              03 COMM-IF00-NBPAGES        PIC S999     COMP-3.          00970003
              03 COMM-IF00-NSOCCOMPT      PIC X(3).                     00741002
              03 COMM-IF00-NETAB          PIC X(3).                     00741002
              03 COMM-IF00-LSOCCOMPT      PIC X(20).                    00741002
              03 COMM-IF00-CORGANISME     PIC X(3).                     00741002
              03 COMM-IF00-LORGANISME     PIC X(20).                    00741002
              03 COMM-IF00-PRESENT        PIC X.                        00742002
              03 COMM-IF00-CTYPMONT       PIC X(5).                     00742002
              03 COMM-IF00-CTYPCTA        PIC X(3).                     00742002
              03 COMM-IF00-CMODPAI        PIC X(3).                     00742002
              03 COMM-IF00-CMODIMP        PIC X.                        00742002
              03 COMM-IF00-FILLER         PIC X(2000).                          
              03 COMM-IF01-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF01-FLAG       PIC X.                                
                     88  COMM-IF01-SUPP          VALUE 'S'.                     
              03 COMM-IF02-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF02-FLAG       PIC X.                                
                     88  COMM-IF02-SUPP          VALUE 'S'.                     
              03 COMM-IF50-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF50-DRECEPT    PIC X(8).                             
              03 COMM-IF12-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF12-POSTE      OCCURS 11.                            
                     10  COMM-IF12-RANGA.                                       
                         15  COMM-IF12-RANG       PIC 99.                       
                     10  COMM-IF12-NOMCHAMP   PIC X(10).                        
                     10  COMM-IF12-MASQUE     PIC X(5).                         
              03 COMM-IF13-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF13-POSTE      OCCURS 7.                             
                     10  COMM-IF13-RANGA.                                       
                         15  COMM-IF13-RANG       PIC 99.                       
                     10  COMM-IF13-LIBELLE    PIC X(6).                         
                     10  COMM-IF13-NOMCHAMP   PIC X(10).                        
                     10  COMM-IF13-MASQUE     PIC X(5).                         
              03 COMM-IF14-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF14-POSTE      OCCURS 11.                            
                     10  COMM-IF14-RANGA.                                       
                         15  COMM-IF14-RANG       PIC 99.                       
                     10  COMM-IF14-LIBELLE    PIC X(30).                        
                     10  COMM-IF14-NOMCHAMP   PIC X(10).                        
                     10  COMM-IF14-MASQUE     PIC X(5).                         
                     10  COMM-IF14-SEPFIN     PIC X.                            
              03 COMM-IF51-APPLI   REDEFINES COMM-IF00-FILLER.                  
                 05  COMM-IF51-NPAGE          PIC S999     COMP-3.      00970003
                 05  COMM-IF51-NBPAGES        PIC S999     COMP-3.      00970003
                 05  COMM-IF51-CORGAN         PIC XXX.                          
                 05  COMM-IF51-DRECEPT        PIC X(8).                         
                 05  COMM-IF51-HRECEPT        PIC X(4).                         
                 05  COMM-IF51-DDTRAIT        PIC X(8).                         
              03 COMM-IF60-APPLI   REDEFINES COMM-IF00-FILLER.          00740000
                 05  COMM-IF60-NPAGE          PIC S999     COMP-3.      00970003
                 05  COMM-IF60-NBPAGES        PIC S999     COMP-3.      00970003
                 05  COMM-IF60-NSOCIETE       PIC X(3).                 00741002
                 05  COMM-IF60-NLIEU          PIC X(3).                 00741002
                 05  COMM-IF60-CORGANISME     PIC X(3).                 00741002
                 05  COMM-IF60-TDATE          PIC X.                    00742002
                 05  COMM-IF60-DNIDENT        PIC X.                    00742002
                                                                                
