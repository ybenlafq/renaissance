      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIF010 AU 31/03/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,20,BI,A,                          *        
      *                           30,03,BI,A,                          *        
      *                           33,03,BI,A,                          *        
      *                           36,03,BI,A,                          *        
      *                           39,03,BI,A,                          *        
      *                           42,03,BI,A,                          *        
      *                           45,05,BI,A,                          *        
      *                           50,02,BI,A,                          *        
      *                           52,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIF010.                                                        
            05 NOMETAT-IIF010           PIC X(6) VALUE 'IIF010'.                
            05 RUPTURES-IIF010.                                                 
           10 IIF010-CORGANISME         PIC X(03).                      007  003
           10 IIF010-NIDENT             PIC X(20).                      010  020
           10 IIF010-NSOCCOMPT          PIC X(03).                      030  003
           10 IIF010-NLIEUCOMPT         PIC X(03).                      033  003
           10 IIF010-CTYPCTA            PIC X(03).                      036  003
           10 IIF010-NSOCIETE           PIC X(03).                      039  003
           10 IIF010-NLIEU              PIC X(03).                      042  003
           10 IIF010-CTYPMVT            PIC X(05).                      045  005
           10 IIF010-CODANO             PIC X(02).                      050  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIF010-SEQUENCE           PIC S9(04) COMP.                052  002
      *--                                                                       
           10 IIF010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIF010.                                                   
           10 IIF010-CMODPAI            PIC X(03).                      054  003
           10 IIF010-CRITERE1           PIC X(05).                      057  005
           10 IIF010-CRITERE2           PIC X(05).                      062  005
           10 IIF010-CRITERE3           PIC X(05).                      067  005
           10 IIF010-CTYPMONT           PIC X(05).                      072  005
           10 IIF010-NOECS              PIC X(05).                      077  005
            05 FILLER                      PIC X(431).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIF010-LONG           PIC S9(4)   COMP  VALUE +081.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIF010-LONG           PIC S9(4) COMP-5  VALUE +081.           
                                                                                
      *}                                                                        
