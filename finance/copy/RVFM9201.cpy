      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM9201   POUR MISE EN PLACE PCA            00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9201                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVFM9201.                                                    00090001
           02  FM92-CINTERFACE                                          00100000
               PIC X(0005).                                             00110000
           02  FM92-CZONE                                               00120000
               PIC X(0002).                                             00130000
           02  FM92-CRITERE                                             00140000
               PIC X(0008).                                             00150001
           02  FM92-LCRITERE                                            00160000
               PIC X(0025).                                             00170000
           02  FM92-DMAJ                                                00180000
               PIC X(0008).                                             00190000
           02  FM92-DSYST                                               00200000
               PIC S9(13) COMP-3.                                       00210000
           02  FM92-SEQS                                                00211001
               PIC X(0001).                                             00212001
           02  FM92-SEQR                                                00213001
               PIC X(0001).                                             00214001
      *                                                                 00220000
      *---------------------------------------------------------        00230000
      *   LISTE DES FLAGS DE LA TABLE RVFM9200                          00240000
      *---------------------------------------------------------        00250000
      *                                                                 00260000
       01  RVFM9200-FLAGS.                                              00270000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-CINTERFACE-F                                        00280000
      *        PIC S9(4) COMP.                                          00290000
      *--                                                                       
           02  FM92-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-CZONE-F                                             00300000
      *        PIC S9(4) COMP.                                          00310000
      *--                                                                       
           02  FM92-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-CRITERE-F                                           00320000
      *        PIC S9(4) COMP.                                          00330000
      *--                                                                       
           02  FM92-CRITERE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-LCRITERE-F                                          00340000
      *        PIC S9(4) COMP.                                          00350000
      *--                                                                       
           02  FM92-LCRITERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-DMAJ-F                                              00360000
      *        PIC S9(4) COMP.                                          00370000
      *--                                                                       
           02  FM92-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM92-DSYST-F                                             00380000
      *        PIC S9(4) COMP.                                          00390000
      *--                                                                       
           02  FM92-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00400000
                                                                                
