      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRA0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRA0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRA0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRA0100.                                                            
      *}                                                                        
           02  RA01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RA01-NDEMANDE                                                    
               PIC X(0005).                                                     
           02  RA01-NSEQCODIC                                                   
               PIC X(0001).                                                     
           02  RA01-NCODIC                                                      
               PIC X(0007).                                                     
           02  RA01-MTREMB                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RA01-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RA01-PCONC                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  RA01-CCONC                                                       
               PIC X(0004).                                                     
           02  RA01-CDARTYCONC                                                  
               PIC X(0001).                                                     
           02  RA01-QTE                                                         
               PIC S9(5) COMP-3.                                                
           02  RA01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRA0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRA0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRA0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-NDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-NDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-NSEQCODIC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-NSEQCODIC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-MTREMB-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-MTREMB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-PCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-PCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-CCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-CCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-CDARTYCONC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-CDARTYCONC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-QTE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-QTE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
