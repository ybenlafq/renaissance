      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM1600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1600.                                                            
           02  FM16-CPDS                                                        
               PIC X(0004).                                                     
           02  FM16-SECTION                                                     
               PIC X(0006).                                                     
           02  FM16-NSEQ                                                        
               PIC X(0002).                                                     
           02  FM16-CCATEGORIE                                                  
               PIC X(0006).                                                     
           02  FM16-DSYST                                                       
               PIC S9(13)  COMP-3.                                              
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM1600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM16-CPDS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM16-CPDS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM16-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM16-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM16-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM16-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM16-CCATEGORIE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM16-CCATEGORIE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM16-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM16-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
