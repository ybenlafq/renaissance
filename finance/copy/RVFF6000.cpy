      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFF6000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFF6000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF6000.                                                            
           02  FF60-PREFT1                                                      
               PIC X(0001).                                                     
           02  FF60-DEXCPT                                                      
               PIC X(0004).                                                     
           02  FF60-NPIECE                                                      
               PIC X(0006).                                                     
           02  FF60-WTYPEPIECE                                                  
               PIC X(0001).                                                     
           02  FF60-WTYPDOC                                                     
               PIC X(0002).                                                     
           02  FF60-NFACT                                                       
               PIC X(0016).                                                     
           02  FF60-NTIERS                                                      
               PIC X(0009).                                                     
           02  FF60-ALTVENDOR                                                   
               PIC X(0009).                                                     
           02  FF60-DFACT                                                       
               PIC X(0008).                                                     
           02  FF60-DRECFACT                                                    
               PIC X(0008).                                                     
           02  FF60-DEVN                                                        
               PIC X(0008).                                                     
           02  FF60-DCPT                                                        
               PIC X(0008).                                                     
           02  FF60-DPERCPT                                                     
               PIC X(0002).                                                     
           02  FF60-NLITIGE                                                     
               PIC X(0006).                                                     
           02  FF60-PHTMSES                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-PTVAMSES                                                    
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-PHTFRAIS                                                    
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-PTVAFRAIS                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-PTTC                                                        
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-PRETROFIL                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FF60-QTAUXESCPT                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FF60-WESCDEDUIT                                                  
               PIC X(0001).                                                     
           02  FF60-WRETROFIL                                                   
               PIC X(0001).                                                     
           02  FF60-WAPAYER                                                     
               PIC X(0001).                                                     
           02  FF60-DELAIRGLT                                                   
               PIC S9(3) COMP-3.                                                
           02  FF60-DRGLT                                                       
               PIC X(0008).                                                     
           02  FF60-CBANQUE                                                     
               PIC X(0002).                                                     
           02  FF60-CACID                                                       
               PIC X(0008).                                                     
           02  FF60-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FF60-WANN                                                        
               PIC X(0001).                                                     
           02  FF60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFF6000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF6000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PREFT1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PREFT1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DEXCPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DEXCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WTYPEPIECE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WTYPEPIECE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WTYPDOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WTYPDOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-NFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-NFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-NTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-NTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-ALTVENDOR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-ALTVENDOR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DRECFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DRECFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DEVN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DEVN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DCPT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DCPT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DPERCPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DPERCPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-NLITIGE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-NLITIGE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PHTMSES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PHTMSES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PTVAMSES-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PTVAMSES-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PHTFRAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PHTFRAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PTVAFRAIS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PTVAFRAIS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PTTC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PTTC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-PRETROFIL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-PRETROFIL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-QTAUXESCPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-QTAUXESCPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WESCDEDUIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WESCDEDUIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WRETROFIL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WRETROFIL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WAPAYER-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WAPAYER-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DELAIRGLT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DELAIRGLT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DRGLT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DRGLT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-CBANQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-CBANQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-WANN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-WANN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FF60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
