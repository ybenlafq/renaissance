      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONNEES DE NCG VERS SAP *            
      * DONNEES RECEPTION                                          *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MCG02                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MCG02-LONG-COMMAREA       PIC S9(4) COMP VALUE +323.             
      *--                                                                       
       01 COMM-MCG02-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +323.           
      *}                                                                        
       01 COMM-MCG02-APPLI.                                                     
          02 COMM-MCG02-ENTREE.                                                 
             03 COMM-MCG02-PGRM          PIC X(05).                             
             03 COMM-MCG02-NTERMID       PIC X(04).                             
             03 COMM-MCG02-CFONC         PIC X(03).                             
             03 COMM-MCG02-NREC          PIC X(07).                             
             03 COMM-MCG02-NRECQUAI      PIC X(07).                             
             03 COMM-MCG02-DSAISREC      PIC X(08).                             
             03 COMM-MCG02-DJRECQUAI     PIC X(08).                             
             03 COMM-MCG02-NSOCLIVR      PIC X(03).                             
             03 COMM-MCG02-NDEPOT        PIC X(03).                             
             03 COMM-MCG02-NCODIC        PIC X(07).                             
             03 FILLER                   PIC X(59).                             
          02 COMM-MCG02-SORTIE.                                                 
             03 COMM-MCG02-MESSAGE.                                             
                04 COMM-MCG02-CODRET     PIC X(01).                             
                   88 COMM-MCG02-OK          VALUE ' '.                         
                   88 COMM-MCG02-ERR         VALUE '1'.                         
                04 COMM-MCG02-LIBERR     PIC X(58).                             
                04 FILLER                PIC X(41).                             
          02 FILLER                      PIC X(100).                            
                                                                                
