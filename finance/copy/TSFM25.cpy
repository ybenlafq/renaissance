      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFM25                                          *  00020000
      *       TR : FM25  GESTION PARAMETRAGE MODELE GCT              *  00030000
      *       PG : TFM25 CREATION/MODIFICATION DES ENTITE MODELES    *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-FM25-LONG         PIC S9(5) COMP-3 VALUE 564.             00080006
       01  TS-FM25-DONNEES.                                             00090000
           05 TS-FM25-LIGNE OCCURS 12.                                          
              10 TS-FM25-CODE         PIC X(06).                        00150000
              10 TS-FM25-LCODE        PIC X(30).                        00150000
              10 TS-FM25-CMASQUE      PIC X(06).                        00150000
              10 TS-FM25-NSOCSEC      PIC X(03).                                
      *-------FLAG -----------------------------------                  00070000
              10 TS-FM25-FLAG         PIC X.                            00150000
              10 TS-FM25-SEL          PIC X.                            00150000
                                                                                
