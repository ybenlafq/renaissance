      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFT6700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT6700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6700.                                                            
           02  FT67-CPDS                                                        
               PIC X(0004).                                                     
           02  FT67-SECTION                                                     
               PIC X(0006).                                                     
           02  FT67-NSEQ                                                        
               PIC X(0002).                                                     
           02  FT67-CCATEGORIE                                                  
               PIC X(0006).                                                     
           02  FT67-DSYST                                                       
               PIC S9(13)  COMP-3.                                              
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT6700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT67-CPDS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT67-CPDS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT67-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT67-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT67-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT67-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT67-CCATEGORIE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT67-CCATEGORIE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT67-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT67-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
