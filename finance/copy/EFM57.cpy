      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - DEFINITION TIERS                                          00000020
      ***************************************************************** 00000030
       01   EFM57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCINTERFACEI   PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLINTERFACEI   PIC X(30).                                 00000230
           02 MTABI OCCURS   5 TIMES .                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONEL  COMP PIC S9(4).                                 00000250
      *--                                                                       
             03 MZONEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MZONEF  PIC X.                                          00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MZONEI  PIC X(2).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLIBELLEI    PIC X(30).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPTIERSL   COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MTYPTIERSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTYPTIERSF   PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MTYPTIERSI   PIC X.                                     00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSOCL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 MWSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSOCF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MWSOCI  PIC X.                                          00000400
      * ZONE CMD AIDA                                                   00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBERRI  PIC X(79).                                      00000450
      * CODE TRANSACTION                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      * CICS DE TRAVAIL                                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      * NETNAME                                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNETNAMI  PIC X(8).                                       00000600
      * CODE TERMINAL                                                   00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCREENI  PIC X(4).                                       00000650
      ***************************************************************** 00000660
      * GCT - DEFINITION TIERS                                          00000670
      ***************************************************************** 00000680
       01   EFM57O REDEFINES EFM57I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
      * DATE DU JOUR                                                    00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
      * HEURE                                                           00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MTIMJOUA  PIC X.                                          00000810
           02 MTIMJOUC  PIC X.                                          00000820
           02 MTIMJOUP  PIC X.                                          00000830
           02 MTIMJOUH  PIC X.                                          00000840
           02 MTIMJOUV  PIC X.                                          00000850
           02 MTIMJOUO  PIC X(5).                                       00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MCINTERFACEA   PIC X.                                     00000880
           02 MCINTERFACEC   PIC X.                                     00000890
           02 MCINTERFACEP   PIC X.                                     00000900
           02 MCINTERFACEH   PIC X.                                     00000910
           02 MCINTERFACEV   PIC X.                                     00000920
           02 MCINTERFACEO   PIC X(5).                                  00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MLINTERFACEA   PIC X.                                     00000950
           02 MLINTERFACEC   PIC X.                                     00000960
           02 MLINTERFACEP   PIC X.                                     00000970
           02 MLINTERFACEH   PIC X.                                     00000980
           02 MLINTERFACEV   PIC X.                                     00000990
           02 MLINTERFACEO   PIC X(30).                                 00001000
           02 MTABO OCCURS   5 TIMES .                                  00001010
             03 FILLER       PIC X(2).                                  00001020
             03 MZONEA  PIC X.                                          00001030
             03 MZONEC  PIC X.                                          00001040
             03 MZONEP  PIC X.                                          00001050
             03 MZONEH  PIC X.                                          00001060
             03 MZONEV  PIC X.                                          00001070
             03 MZONEO  PIC X(2).                                       00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MLIBELLEA    PIC X.                                     00001100
             03 MLIBELLEC    PIC X.                                     00001110
             03 MLIBELLEP    PIC X.                                     00001120
             03 MLIBELLEH    PIC X.                                     00001130
             03 MLIBELLEV    PIC X.                                     00001140
             03 MLIBELLEO    PIC X(30).                                 00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MTYPTIERSA   PIC X.                                     00001170
             03 MTYPTIERSC   PIC X.                                     00001180
             03 MTYPTIERSP   PIC X.                                     00001190
             03 MTYPTIERSH   PIC X.                                     00001200
             03 MTYPTIERSV   PIC X.                                     00001210
             03 MTYPTIERSO   PIC X.                                     00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MWSOCA  PIC X.                                          00001240
             03 MWSOCC  PIC X.                                          00001250
             03 MWSOCP  PIC X.                                          00001260
             03 MWSOCH  PIC X.                                          00001270
             03 MWSOCV  PIC X.                                          00001280
             03 MWSOCO  PIC X.                                          00001290
      * ZONE CMD AIDA                                                   00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLIBERRA  PIC X.                                          00001320
           02 MLIBERRC  PIC X.                                          00001330
           02 MLIBERRP  PIC X.                                          00001340
           02 MLIBERRH  PIC X.                                          00001350
           02 MLIBERRV  PIC X.                                          00001360
           02 MLIBERRO  PIC X(79).                                      00001370
      * CODE TRANSACTION                                                00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
      * CICS DE TRAVAIL                                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCICSA    PIC X.                                          00001480
           02 MCICSC    PIC X.                                          00001490
           02 MCICSP    PIC X.                                          00001500
           02 MCICSH    PIC X.                                          00001510
           02 MCICSV    PIC X.                                          00001520
           02 MCICSO    PIC X(5).                                       00001530
      * NETNAME                                                         00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNETNAMA  PIC X.                                          00001560
           02 MNETNAMC  PIC X.                                          00001570
           02 MNETNAMP  PIC X.                                          00001580
           02 MNETNAMH  PIC X.                                          00001590
           02 MNETNAMV  PIC X.                                          00001600
           02 MNETNAMO  PIC X(8).                                       00001610
      * CODE TERMINAL                                                   00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MSCREENA  PIC X.                                          00001640
           02 MSCREENC  PIC X.                                          00001650
           02 MSCREENP  PIC X.                                          00001660
           02 MSCREENH  PIC X.                                          00001670
           02 MSCREENV  PIC X.                                          00001680
           02 MSCREENO  PIC X(4).                                       00001690
                                                                                
