      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - liste de compte de conso                                  00000020
      ***************************************************************** 00000030
       01   EFM28I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCPTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCPTF     PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MCPTI     PIC X(6).                                       00000250
           02 LIGNEI OCCURS   12 TIMES .                                00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00000280
             03 FILLER  PIC X(2).                                       00000290
             03 MCOMPTEI     PIC X(6).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTECL   COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLCOMPTECL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMPTECF   PIC X.                                     00000320
             03 FILLER  PIC X(2).                                       00000330
             03 MLCOMPTECI   PIC X(15).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTELL   COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLCOMPTELL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMPTELF   PIC X.                                     00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MLCOMPTELI   PIC X(30).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCLOTUREL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDCLOTUREL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDCLOTUREF   PIC X.                                     00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MDCLOTUREI   PIC X(10).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMAJL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MDMAJL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMAJF  PIC X.                                          00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MDMAJI  PIC X(10).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000480
           02 FILLER    PIC X(2).                                       00000490
           02 MLIBERRI  PIC X(79).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MSCREENI  PIC X(4).                                       00000620
      ***************************************************************** 00000630
      * GCT - liste de compte de conso                                  00000640
      ***************************************************************** 00000650
       01   EFM28O REDEFINES EFM28I.                                    00000660
           02 FILLER    PIC X(12).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUH  PIC X.                                          00000710
           02 MDATJOUO  PIC X(10).                                      00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MTIMJOUA  PIC X.                                          00000740
           02 MTIMJOUC  PIC X.                                          00000750
           02 MTIMJOUH  PIC X.                                          00000760
           02 MTIMJOUO  PIC X(5).                                       00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MPAGEA    PIC X.                                          00000790
           02 MPAGEC    PIC X.                                          00000800
           02 MPAGEH    PIC X.                                          00000810
           02 MPAGEO    PIC Z9.                                         00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MPAGETOTA      PIC X.                                     00000840
           02 MPAGETOTC PIC X.                                          00000850
           02 MPAGETOTH PIC X.                                          00000860
           02 MPAGETOTO      PIC Z9.                                    00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MCPTA     PIC X.                                          00000890
           02 MCPTC     PIC X.                                          00000900
           02 MCPTH     PIC X.                                          00000910
           02 MCPTO     PIC X(6).                                       00000920
           02 LIGNEO OCCURS   12 TIMES .                                00000930
             03 FILLER       PIC X(2).                                  00000940
             03 MCOMPTEA     PIC X.                                     00000950
             03 MCOMPTEC     PIC X.                                     00000960
             03 MCOMPTEH     PIC X.                                     00000970
             03 MCOMPTEO     PIC X(6).                                  00000980
             03 FILLER       PIC X(2).                                  00000990
             03 MLCOMPTECA   PIC X.                                     00001000
             03 MLCOMPTECC   PIC X.                                     00001010
             03 MLCOMPTECH   PIC X.                                     00001020
             03 MLCOMPTECO   PIC X(15).                                 00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MLCOMPTELA   PIC X.                                     00001050
             03 MLCOMPTELC   PIC X.                                     00001060
             03 MLCOMPTELH   PIC X.                                     00001070
             03 MLCOMPTELO   PIC X(30).                                 00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MDCLOTUREA   PIC X.                                     00001100
             03 MDCLOTUREC   PIC X.                                     00001110
             03 MDCLOTUREH   PIC X.                                     00001120
             03 MDCLOTUREO   PIC X(10).                                 00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MDMAJA  PIC X.                                          00001150
             03 MDMAJC  PIC X.                                          00001160
             03 MDMAJH  PIC X.                                          00001170
             03 MDMAJO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLIBERRA  PIC X.                                          00001200
           02 MLIBERRC  PIC X.                                          00001210
           02 MLIBERRH  PIC X.                                          00001220
           02 MLIBERRO  PIC X(79).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCODTRAA  PIC X.                                          00001250
           02 MCODTRAC  PIC X.                                          00001260
           02 MCODTRAH  PIC X.                                          00001270
           02 MCODTRAO  PIC X(4).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MCICSA    PIC X.                                          00001300
           02 MCICSC    PIC X.                                          00001310
           02 MCICSH    PIC X.                                          00001320
           02 MCICSO    PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MSCREENA  PIC X.                                          00001350
           02 MSCREENC  PIC X.                                          00001360
           02 MSCREENH  PIC X.                                          00001370
           02 MSCREENO  PIC X(4).                                       00001380
                                                                                
