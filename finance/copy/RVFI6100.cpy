      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RVFI6100)                                  *        
      *        LIBRARY(DSA016.DEVL.COPY.COBOL(RVFI6100))               *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(FI61-)                                            *        
      *        STRUCTURE(RVFI6100)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVFI6100 TABLE                               
           ( CEXTR                          CHAR(5) NOT NULL,                   
             COPCO                          CHAR(6) NOT NULL,                   
             CPROG                          CHAR(5) NOT NULL,                   
             NSOCORIG                       CHAR(3) NOT NULL,                   
             NLIEUORIG                      CHAR(3) NOT NULL,                   
             NSOCDEST                       CHAR(3) NOT NULL,                   
             NLIEUDEST                      CHAR(3) NOT NULL,                   
             NSOCVTE                        CHAR(3) NOT NULL,                   
             NLIEUVTE                       CHAR(3) NOT NULL,                   
             ESFIL                          CHAR(1) NOT NULL,                   
             LIBEL                          CHAR(50) NOT NULL,                  
             NSEQ                           DECIMAL(3, 0) NOT NULL,             
             CVALO                          CHAR(1) NOT NULL,                   
             CIMPUT                         CHAR(1) NOT NULL                    
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFI6100                    *        
      ******************************************************************        
       01  RVFI6100.                                                            
      *                       CEXTR                                             
           10 FI61-CEXTR           PIC X(5).                                    
      *                       COPCO                                             
           10 FI61-COPCO           PIC X(6).                                    
      *                       CPROG                                             
           10 FI61-CPROG           PIC X(5).                                    
      *                       NSOCORIG                                          
           10 FI61-NSOCORIG        PIC X(3).                                    
      *                       NLIEUORIG                                         
           10 FI61-NLIEUORIG       PIC X(3).                                    
      *                       NSOCDEST                                          
           10 FI61-NSOCDEST        PIC X(3).                                    
      *                       NLIEUDEST                                         
           10 FI61-NLIEUDEST       PIC X(3).                                    
      *                       NSOCVTE                                           
           10 FI61-NSOCVTE         PIC X(3).                                    
      *                       NLIEUVTE                                          
           10 FI61-NLIEUVTE        PIC X(3).                                    
      *                       ESFIL                                             
           10 FI61-ESFIL           PIC X(1).                                    
      *                       LIBEL                                             
           10 FI61-LIBEL           PIC X(50).                                   
      *                       NSEQ                                              
           10 FI61-NSEQ            PIC S9(3)V USAGE COMP-3.                     
      *                       CVALO                                             
           10 FI61-CVALO           PIC X(1).                                    
      *                       CIMPUT                                            
           10 FI61-CIMPUT          PIC X(1).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *        
      ******************************************************************        
                                                                                
