      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GESTION TAUX DE CHANGE                                    00000020
      ***************************************************************** 00000030
       01   EFM05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCDEVISEI      PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLDEVISEI      PIC X(24).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTEPL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCPTEPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPTEPF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCPTEPI   PIC X(6).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTPL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MSECTPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSECTPF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MSECTPI   PIC X(6).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRPL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MRUBRPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRUBRPF   PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MRUBRPI   PIC X(6).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTEGL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCPTEGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPTEGF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCPTEGI   PIC X(6).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTGL   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MSECTGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSECTGF   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MSECTGI   PIC X(6).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRGL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MRUBRGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRUBRGF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MRUBRGI   PIC X(6).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEVSELL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MDEVSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEVSELF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MDEVSELI  PIC X(3).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEVSAIL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MDEVSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEVSAIF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MDEVSAII  PIC X(3).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPSAIL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCOPSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOPSAIF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCOPSAII  PIC X.                                          00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTASAIL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MPTASAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPTASAIF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MPTASAII  PIC X(11).                                      00000710
           02 MTABI OCCURS   11 TIMES .                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEVDESL     COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MDEVDESL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEVDESF     PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MDEVDESI     PIC X(3).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPERAL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MCOPERAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOPERAF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCOPERAI     PIC X.                                     00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTAUXL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MPTAUXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPTAUXF      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MPTAUXI      PIC X(11).                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDEFFETI     PIC X(10).                                 00000880
      * ZONE CMD AIDA                                                   00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(79).                                      00000930
      * CODE TRANSACTION                                                00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      * CICS DE TRAVAIL                                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCICSI    PIC X(5).                                       00001030
      * NETNAME                                                         00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MNETNAMI  PIC X(8).                                       00001080
      * CODE TERMINAL                                                   00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MSCREENI  PIC X(4).                                       00001130
      ***************************************************************** 00001140
      * GCT - GESTION TAUX DE CHANGE                                    00001150
      ***************************************************************** 00001160
       01   EFM05O REDEFINES EFM05I.                                    00001170
           02 FILLER    PIC X(12).                                      00001180
      * DATE DU JOUR                                                    00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
      * HEURE                                                           00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPAGEA    PIC X.                                          00001360
           02 MPAGEC    PIC X.                                          00001370
           02 MPAGEP    PIC X.                                          00001380
           02 MPAGEH    PIC X.                                          00001390
           02 MPAGEV    PIC X.                                          00001400
           02 MPAGEO    PIC Z9.                                         00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNBPA     PIC X.                                          00001430
           02 MNBPC     PIC X.                                          00001440
           02 MNBPP     PIC X.                                          00001450
           02 MNBPH     PIC X.                                          00001460
           02 MNBPV     PIC X.                                          00001470
           02 MNBPO     PIC Z9.                                         00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCDEVISEA      PIC X.                                     00001500
           02 MCDEVISEC PIC X.                                          00001510
           02 MCDEVISEP PIC X.                                          00001520
           02 MCDEVISEH PIC X.                                          00001530
           02 MCDEVISEV PIC X.                                          00001540
           02 MCDEVISEO      PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLDEVISEA      PIC X.                                     00001570
           02 MLDEVISEC PIC X.                                          00001580
           02 MLDEVISEP PIC X.                                          00001590
           02 MLDEVISEH PIC X.                                          00001600
           02 MLDEVISEV PIC X.                                          00001610
           02 MLDEVISEO      PIC X(24).                                 00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCPTEPA   PIC X.                                          00001640
           02 MCPTEPC   PIC X.                                          00001650
           02 MCPTEPP   PIC X.                                          00001660
           02 MCPTEPH   PIC X.                                          00001670
           02 MCPTEPV   PIC X.                                          00001680
           02 MCPTEPO   PIC X(6).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MSECTPA   PIC X.                                          00001710
           02 MSECTPC   PIC X.                                          00001720
           02 MSECTPP   PIC X.                                          00001730
           02 MSECTPH   PIC X.                                          00001740
           02 MSECTPV   PIC X.                                          00001750
           02 MSECTPO   PIC X(6).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MRUBRPA   PIC X.                                          00001780
           02 MRUBRPC   PIC X.                                          00001790
           02 MRUBRPP   PIC X.                                          00001800
           02 MRUBRPH   PIC X.                                          00001810
           02 MRUBRPV   PIC X.                                          00001820
           02 MRUBRPO   PIC X(6).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCPTEGA   PIC X.                                          00001850
           02 MCPTEGC   PIC X.                                          00001860
           02 MCPTEGP   PIC X.                                          00001870
           02 MCPTEGH   PIC X.                                          00001880
           02 MCPTEGV   PIC X.                                          00001890
           02 MCPTEGO   PIC X(6).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSECTGA   PIC X.                                          00001920
           02 MSECTGC   PIC X.                                          00001930
           02 MSECTGP   PIC X.                                          00001940
           02 MSECTGH   PIC X.                                          00001950
           02 MSECTGV   PIC X.                                          00001960
           02 MSECTGO   PIC X(6).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MRUBRGA   PIC X.                                          00001990
           02 MRUBRGC   PIC X.                                          00002000
           02 MRUBRGP   PIC X.                                          00002010
           02 MRUBRGH   PIC X.                                          00002020
           02 MRUBRGV   PIC X.                                          00002030
           02 MRUBRGO   PIC X(6).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDEVSELA  PIC X.                                          00002060
           02 MDEVSELC  PIC X.                                          00002070
           02 MDEVSELP  PIC X.                                          00002080
           02 MDEVSELH  PIC X.                                          00002090
           02 MDEVSELV  PIC X.                                          00002100
           02 MDEVSELO  PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDEVSAIA  PIC X.                                          00002130
           02 MDEVSAIC  PIC X.                                          00002140
           02 MDEVSAIP  PIC X.                                          00002150
           02 MDEVSAIH  PIC X.                                          00002160
           02 MDEVSAIV  PIC X.                                          00002170
           02 MDEVSAIO  PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCOPSAIA  PIC X.                                          00002200
           02 MCOPSAIC  PIC X.                                          00002210
           02 MCOPSAIP  PIC X.                                          00002220
           02 MCOPSAIH  PIC X.                                          00002230
           02 MCOPSAIV  PIC X.                                          00002240
           02 MCOPSAIO  PIC X.                                          00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MPTASAIA  PIC X.                                          00002270
           02 MPTASAIC  PIC X.                                          00002280
           02 MPTASAIP  PIC X.                                          00002290
           02 MPTASAIH  PIC X.                                          00002300
           02 MPTASAIV  PIC X.                                          00002310
           02 MPTASAIO  PIC Z(4)9,9(5).                                 00002320
           02 MTABO OCCURS   11 TIMES .                                 00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MDEVDESA     PIC X.                                     00002350
             03 MDEVDESC     PIC X.                                     00002360
             03 MDEVDESP     PIC X.                                     00002370
             03 MDEVDESH     PIC X.                                     00002380
             03 MDEVDESV     PIC X.                                     00002390
             03 MDEVDESO     PIC X(3).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MCOPERAA     PIC X.                                     00002420
             03 MCOPERAC     PIC X.                                     00002430
             03 MCOPERAP     PIC X.                                     00002440
             03 MCOPERAH     PIC X.                                     00002450
             03 MCOPERAV     PIC X.                                     00002460
             03 MCOPERAO     PIC X.                                     00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MPTAUXA      PIC X.                                     00002490
             03 MPTAUXC PIC X.                                          00002500
             03 MPTAUXP PIC X.                                          00002510
             03 MPTAUXH PIC X.                                          00002520
             03 MPTAUXV PIC X.                                          00002530
             03 MPTAUXO      PIC Z(4)9,9(5).                            00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MDEFFETA     PIC X.                                     00002560
             03 MDEFFETC     PIC X.                                     00002570
             03 MDEFFETP     PIC X.                                     00002580
             03 MDEFFETH     PIC X.                                     00002590
             03 MDEFFETV     PIC X.                                     00002600
             03 MDEFFETO     PIC X(10).                                 00002610
      * ZONE CMD AIDA                                                   00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLIBERRA  PIC X.                                          00002640
           02 MLIBERRC  PIC X.                                          00002650
           02 MLIBERRP  PIC X.                                          00002660
           02 MLIBERRH  PIC X.                                          00002670
           02 MLIBERRV  PIC X.                                          00002680
           02 MLIBERRO  PIC X(79).                                      00002690
      * CODE TRANSACTION                                                00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
      * CICS DE TRAVAIL                                                 00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCICSA    PIC X.                                          00002800
           02 MCICSC    PIC X.                                          00002810
           02 MCICSP    PIC X.                                          00002820
           02 MCICSH    PIC X.                                          00002830
           02 MCICSV    PIC X.                                          00002840
           02 MCICSO    PIC X(5).                                       00002850
      * NETNAME                                                         00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNETNAMA  PIC X.                                          00002880
           02 MNETNAMC  PIC X.                                          00002890
           02 MNETNAMP  PIC X.                                          00002900
           02 MNETNAMH  PIC X.                                          00002910
           02 MNETNAMV  PIC X.                                          00002920
           02 MNETNAMO  PIC X(8).                                       00002930
      * CODE TERMINAL                                                   00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MSCREENA  PIC X.                                          00002960
           02 MSCREENC  PIC X.                                          00002970
           02 MSCREENP  PIC X.                                          00002980
           02 MSCREENH  PIC X.                                          00002990
           02 MSCREENV  PIC X.                                          00003000
           02 MSCREENO  PIC X(4).                                       00003010
                                                                                
