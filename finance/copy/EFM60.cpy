      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MENU PROVISIONS                                                 00000020
      ***************************************************************** 00000030
       01   EFM60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCINTERFACEI   PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPOPERL     COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MCTYPOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPOPERF     PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCTYPOPERI     PIC X(5).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATOPERL     COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCNATOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCNATOPERF     PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCNATOPERI     PIC X(5).                                  00000270
      * ZONE CMD AIDA                                                   00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MLIBERRI  PIC X(79).                                      00000320
      * CODE TRANSACTION                                                00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODTRAI  PIC X(4).                                       00000370
      * CICS DE TRAVAIL                                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCICSI    PIC X(5).                                       00000420
      * NETNAME                                                         00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETNAMI  PIC X(8).                                       00000470
      * CODE TERMINAL                                                   00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MSCREENI  PIC X(4).                                       00000520
      ***************************************************************** 00000530
      * MENU PROVISIONS                                                 00000540
      ***************************************************************** 00000550
       01   EFM60O REDEFINES EFM60I.                                    00000560
           02 FILLER    PIC X(12).                                      00000570
      * DATE DU JOUR                                                    00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MDATJOUA  PIC X.                                          00000600
           02 MDATJOUC  PIC X.                                          00000610
           02 MDATJOUP  PIC X.                                          00000620
           02 MDATJOUH  PIC X.                                          00000630
           02 MDATJOUV  PIC X.                                          00000640
           02 MDATJOUO  PIC X(10).                                      00000650
      * HEURE                                                           00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MTIMJOUA  PIC X.                                          00000680
           02 MTIMJOUC  PIC X.                                          00000690
           02 MTIMJOUP  PIC X.                                          00000700
           02 MTIMJOUH  PIC X.                                          00000710
           02 MTIMJOUV  PIC X.                                          00000720
           02 MTIMJOUO  PIC X(5).                                       00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MCINTERFACEA   PIC X.                                     00000750
           02 MCINTERFACEC   PIC X.                                     00000760
           02 MCINTERFACEP   PIC X.                                     00000770
           02 MCINTERFACEH   PIC X.                                     00000780
           02 MCINTERFACEV   PIC X.                                     00000790
           02 MCINTERFACEO   PIC X(5).                                  00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCTYPOPERA     PIC X.                                     00000820
           02 MCTYPOPERC     PIC X.                                     00000830
           02 MCTYPOPERP     PIC X.                                     00000840
           02 MCTYPOPERH     PIC X.                                     00000850
           02 MCTYPOPERV     PIC X.                                     00000860
           02 MCTYPOPERO     PIC X(5).                                  00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MCNATOPERA     PIC X.                                     00000890
           02 MCNATOPERC     PIC X.                                     00000900
           02 MCNATOPERP     PIC X.                                     00000910
           02 MCNATOPERH     PIC X.                                     00000920
           02 MCNATOPERV     PIC X.                                     00000930
           02 MCNATOPERO     PIC X(5).                                  00000940
      * ZONE CMD AIDA                                                   00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MLIBERRA  PIC X.                                          00000970
           02 MLIBERRC  PIC X.                                          00000980
           02 MLIBERRP  PIC X.                                          00000990
           02 MLIBERRH  PIC X.                                          00001000
           02 MLIBERRV  PIC X.                                          00001010
           02 MLIBERRO  PIC X(79).                                      00001020
      * CODE TRANSACTION                                                00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCODTRAA  PIC X.                                          00001050
           02 MCODTRAC  PIC X.                                          00001060
           02 MCODTRAP  PIC X.                                          00001070
           02 MCODTRAH  PIC X.                                          00001080
           02 MCODTRAV  PIC X.                                          00001090
           02 MCODTRAO  PIC X(4).                                       00001100
      * CICS DE TRAVAIL                                                 00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MCICSA    PIC X.                                          00001130
           02 MCICSC    PIC X.                                          00001140
           02 MCICSP    PIC X.                                          00001150
           02 MCICSH    PIC X.                                          00001160
           02 MCICSV    PIC X.                                          00001170
           02 MCICSO    PIC X(5).                                       00001180
      * NETNAME                                                         00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNETNAMA  PIC X.                                          00001210
           02 MNETNAMC  PIC X.                                          00001220
           02 MNETNAMP  PIC X.                                          00001230
           02 MNETNAMH  PIC X.                                          00001240
           02 MNETNAMV  PIC X.                                          00001250
           02 MNETNAMO  PIC X(8).                                       00001260
      * CODE TERMINAL                                                   00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MSCREENA  PIC X.                                          00001290
           02 MSCREENC  PIC X.                                          00001300
           02 MSCREENP  PIC X.                                          00001310
           02 MSCREENH  PIC X.                                          00001320
           02 MSCREENV  PIC X.                                          00001330
           02 MSCREENO  PIC X(4).                                       00001340
                                                                                
