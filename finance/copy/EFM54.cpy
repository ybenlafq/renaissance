      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - exception CODES INTERFACES                                00000020
      ***************************************************************** 00000030
       01   EFM54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
           02 MTABI OCCURS   11 TIMES .                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUPL   COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 MSUPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSUPF   PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MSUPI   PIC X.                                          00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MCTYPOPERI   PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTYPOPERL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MLTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLTYPOPERF   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MLTYPOPERI   PIC X(29).                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNJRNL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MNJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNJRNF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MNJRNI  PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLJRNL  COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MLJRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLJRNF  PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MLJRNI  PIC X(13).                                      00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATUREL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCNATUREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCNATUREF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCNATUREI    PIC X(3).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNATUREL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MLNATUREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLNATUREF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MLNATUREI    PIC X(13).                                 00000600
      * ZONE CMD AIDA                                                   00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(79).                                      00000650
      * CODE TRANSACTION                                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      * CICS DE TRAVAIL                                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCICSI    PIC X(5).                                       00000750
      * NETNAME                                                         00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      * CODE TERMINAL                                                   00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(4).                                       00000850
      ***************************************************************** 00000860
      * GCT - exception CODES INTERFACES                                00000870
      ***************************************************************** 00000880
       01   EFM54O REDEFINES EFM54I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
      * DATE DU JOUR                                                    00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
      * HEURE                                                           00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MTIMJOUA  PIC X.                                          00001010
           02 MTIMJOUC  PIC X.                                          00001020
           02 MTIMJOUP  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUV  PIC X.                                          00001050
           02 MTIMJOUO  PIC X(5).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MPAGEA    PIC X.                                          00001080
           02 MPAGEC    PIC X.                                          00001090
           02 MPAGEP    PIC X.                                          00001100
           02 MPAGEH    PIC X.                                          00001110
           02 MPAGEV    PIC X.                                          00001120
           02 MPAGEO    PIC Z9.                                         00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNBPA     PIC X.                                          00001150
           02 MNBPC     PIC X.                                          00001160
           02 MNBPP     PIC X.                                          00001170
           02 MNBPH     PIC X.                                          00001180
           02 MNBPV     PIC X.                                          00001190
           02 MNBPO     PIC Z9.                                         00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCINTERFACEA   PIC X.                                     00001220
           02 MCINTERFACEC   PIC X.                                     00001230
           02 MCINTERFACEP   PIC X.                                     00001240
           02 MCINTERFACEH   PIC X.                                     00001250
           02 MCINTERFACEV   PIC X.                                     00001260
           02 MCINTERFACEO   PIC X(5).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLINTERFACEA   PIC X.                                     00001290
           02 MLINTERFACEC   PIC X.                                     00001300
           02 MLINTERFACEP   PIC X.                                     00001310
           02 MLINTERFACEH   PIC X.                                     00001320
           02 MLINTERFACEV   PIC X.                                     00001330
           02 MLINTERFACEO   PIC X(30).                                 00001340
           02 MTABO OCCURS   11 TIMES .                                 00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MSUPA   PIC X.                                          00001370
             03 MSUPC   PIC X.                                          00001380
             03 MSUPP   PIC X.                                          00001390
             03 MSUPH   PIC X.                                          00001400
             03 MSUPV   PIC X.                                          00001410
             03 MSUPO   PIC X.                                          00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MCTYPOPERA   PIC X.                                     00001440
             03 MCTYPOPERC   PIC X.                                     00001450
             03 MCTYPOPERP   PIC X.                                     00001460
             03 MCTYPOPERH   PIC X.                                     00001470
             03 MCTYPOPERV   PIC X.                                     00001480
             03 MCTYPOPERO   PIC X(5).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MLTYPOPERA   PIC X.                                     00001510
             03 MLTYPOPERC   PIC X.                                     00001520
             03 MLTYPOPERP   PIC X.                                     00001530
             03 MLTYPOPERH   PIC X.                                     00001540
             03 MLTYPOPERV   PIC X.                                     00001550
             03 MLTYPOPERO   PIC X(29).                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MNJRNA  PIC X.                                          00001580
             03 MNJRNC  PIC X.                                          00001590
             03 MNJRNP  PIC X.                                          00001600
             03 MNJRNH  PIC X.                                          00001610
             03 MNJRNV  PIC X.                                          00001620
             03 MNJRNO  PIC X(3).                                       00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MLJRNA  PIC X.                                          00001650
             03 MLJRNC  PIC X.                                          00001660
             03 MLJRNP  PIC X.                                          00001670
             03 MLJRNH  PIC X.                                          00001680
             03 MLJRNV  PIC X.                                          00001690
             03 MLJRNO  PIC X(13).                                      00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MCNATUREA    PIC X.                                     00001720
             03 MCNATUREC    PIC X.                                     00001730
             03 MCNATUREP    PIC X.                                     00001740
             03 MCNATUREH    PIC X.                                     00001750
             03 MCNATUREV    PIC X.                                     00001760
             03 MCNATUREO    PIC X(3).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MLNATUREA    PIC X.                                     00001790
             03 MLNATUREC    PIC X.                                     00001800
             03 MLNATUREP    PIC X.                                     00001810
             03 MLNATUREH    PIC X.                                     00001820
             03 MLNATUREV    PIC X.                                     00001830
             03 MLNATUREO    PIC X(13).                                 00001840
      * ZONE CMD AIDA                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLIBERRA  PIC X.                                          00001870
           02 MLIBERRC  PIC X.                                          00001880
           02 MLIBERRP  PIC X.                                          00001890
           02 MLIBERRH  PIC X.                                          00001900
           02 MLIBERRV  PIC X.                                          00001910
           02 MLIBERRO  PIC X(79).                                      00001920
      * CODE TRANSACTION                                                00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
      * CICS DE TRAVAIL                                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCICSA    PIC X.                                          00002030
           02 MCICSC    PIC X.                                          00002040
           02 MCICSP    PIC X.                                          00002050
           02 MCICSH    PIC X.                                          00002060
           02 MCICSV    PIC X.                                          00002070
           02 MCICSO    PIC X(5).                                       00002080
      * NETNAME                                                         00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNETNAMA  PIC X.                                          00002110
           02 MNETNAMC  PIC X.                                          00002120
           02 MNETNAMP  PIC X.                                          00002130
           02 MNETNAMH  PIC X.                                          00002140
           02 MNETNAMV  PIC X.                                          00002150
           02 MNETNAMO  PIC X(8).                                       00002160
      * CODE TERMINAL                                                   00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MSCREENA  PIC X.                                          00002190
           02 MSCREENC  PIC X.                                          00002200
           02 MSCREENP  PIC X.                                          00002210
           02 MSCREENH  PIC X.                                          00002220
           02 MSCREENV  PIC X.                                          00002230
           02 MSCREENO  PIC X(4).                                       00002240
                                                                                
