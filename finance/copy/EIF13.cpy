      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE REGLES DE LETTRAGE                                  00000020
      ***************************************************************** 00000030
       01   EIF13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODPAIL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MMODPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODPAIF  PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MMODPAII  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCTAL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MTYPCTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPCTAF  PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MTYPCTAI  PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPMTL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTYPMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPMTF  PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MCTYPMTI  PIC X(5).                                       00000270
           02 FILLER  OCCURS   6 TIMES .                                00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRANGL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MRANGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRANGF  PIC X.                                          00000300
             03 FILLER  PIC X(2).                                       00000310
             03 MRANGI  PIC X(2).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000340
             03 FILLER  PIC X(2).                                       00000350
             03 MLIBELLEI    PIC X(6).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCHAMPL   COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MNOMCHAMPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNOMCHAMPF   PIC X.                                     00000380
             03 FILLER  PIC X(2).                                       00000390
             03 MNOMCHAMPI   PIC X(10).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000420
             03 FILLER  PIC X(2).                                       00000430
             03 MMASQUEI     PIC X(5).                                  00000440
      * MESSAGE ERREUR                                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000470
           02 FILLER    PIC X(2).                                       00000480
           02 MLIBERRI  PIC X(79).                                      00000490
      * CODE TRANSACTION                                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      * ZONE CMD AIDA                                                   00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MZONCMDI  PIC X(15).                                      00000590
      * CICS DE TRAVAIL                                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000620
           02 FILLER    PIC X(2).                                       00000630
           02 MCICSI    PIC X(5).                                       00000640
      * NETNAME                                                         00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      * CODE TERMINAL                                                   00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MSCREENI  PIC X(5).                                       00000740
      ***************************************************************** 00000750
      * PARAMETRAGE REGLES DE LETTRAGE                                  00000760
      ***************************************************************** 00000770
       01   EIF13O REDEFINES EIF13I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
      * DATE DU JOUR                                                    00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
      * HEURE                                                           00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUO  PIC X(5).                                       00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MMODPAIA  PIC X.                                          00000930
           02 MMODPAIC  PIC X.                                          00000940
           02 MMODPAIH  PIC X.                                          00000950
           02 MMODPAIO  PIC X(3).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTYPCTAA  PIC X.                                          00000980
           02 MTYPCTAC  PIC X.                                          00000990
           02 MTYPCTAH  PIC X.                                          00001000
           02 MTYPCTAO  PIC X(3).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MCTYPMTA  PIC X.                                          00001030
           02 MCTYPMTC  PIC X.                                          00001040
           02 MCTYPMTH  PIC X.                                          00001050
           02 MCTYPMTO  PIC X(5).                                       00001060
           02 FILLER  OCCURS   6 TIMES .                                00001070
             03 FILLER       PIC X(2).                                  00001080
             03 MRANGA  PIC X.                                          00001090
             03 MRANGC  PIC X.                                          00001100
             03 MRANGH  PIC X.                                          00001110
             03 MRANGO  PIC ZZ.                                         00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MLIBELLEA    PIC X.                                     00001140
             03 MLIBELLEC    PIC X.                                     00001150
             03 MLIBELLEH    PIC X.                                     00001160
             03 MLIBELLEO    PIC X(6).                                  00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MNOMCHAMPA   PIC X.                                     00001190
             03 MNOMCHAMPC   PIC X.                                     00001200
             03 MNOMCHAMPH   PIC X.                                     00001210
             03 MNOMCHAMPO   PIC X(10).                                 00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MMASQUEA     PIC X.                                     00001240
             03 MMASQUEC     PIC X.                                     00001250
             03 MMASQUEH     PIC X.                                     00001260
             03 MMASQUEO     PIC X(5).                                  00001270
      * MESSAGE ERREUR                                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLIBERRA  PIC X.                                          00001300
           02 MLIBERRC  PIC X.                                          00001310
           02 MLIBERRH  PIC X.                                          00001320
           02 MLIBERRO  PIC X(79).                                      00001330
      * CODE TRANSACTION                                                00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCODTRAA  PIC X.                                          00001360
           02 MCODTRAC  PIC X.                                          00001370
           02 MCODTRAH  PIC X.                                          00001380
           02 MCODTRAO  PIC X(4).                                       00001390
      * ZONE CMD AIDA                                                   00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MZONCMDA  PIC X.                                          00001420
           02 MZONCMDC  PIC X.                                          00001430
           02 MZONCMDH  PIC X.                                          00001440
           02 MZONCMDO  PIC X(15).                                      00001450
      * CICS DE TRAVAIL                                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCICSA    PIC X.                                          00001480
           02 MCICSC    PIC X.                                          00001490
           02 MCICSH    PIC X.                                          00001500
           02 MCICSO    PIC X(5).                                       00001510
      * NETNAME                                                         00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMH  PIC X.                                          00001560
           02 MNETNAMO  PIC X(8).                                       00001570
      * CODE TERMINAL                                                   00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MSCREENA  PIC X.                                          00001600
           02 MSCREENC  PIC X.                                          00001610
           02 MSCREENH  PIC X.                                          00001620
           02 MSCREENO  PIC X(5).                                       00001630
                                                                                
