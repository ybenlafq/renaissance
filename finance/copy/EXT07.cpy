      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT07   EXT07                                              00000020
      ***************************************************************** 00000030
       01   EXT07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAFFL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MPAGEAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEAFFF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEAFFI      PIC X(2).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
           02 M86I OCCURS   15 TIMES .                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-1L     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MCAGR-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-1F     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MCAGR-1I     PIC X(5).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-1L     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLAGR-1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-1F     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLAGR-1I     PIC X(10).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-2L     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MCAGR-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-2F     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MCAGR-2I     PIC X(5).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-2L     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLAGR-2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-2F     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLAGR-2I     PIC X(10).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-3L     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MCAGR-3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-3F     PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MCAGR-3I     PIC X(5).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-3L     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MLAGR-3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-3F     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MLAGR-3I     PIC X(10).                                 00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAGR-4L     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCAGR-4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAGR-4F     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCAGR-4I     PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGR-4L     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MLAGR-4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGR-4F     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MLAGR-4I     PIC X(10).                                 00000560
      * MESSAGE ERREUR                                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(78).                                      00000610
      * CODE TRANSACTION                                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      * CICS DE TRAVAIL                                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCICSI    PIC X(5).                                       00000750
      * NETNAME                                                         00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      * CODE TERMINAL                                                   00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(5).                                       00000850
      ***************************************************************** 00000860
      * SDF: EXT07   EXT07                                              00000870
      ***************************************************************** 00000880
       01   EXT07O REDEFINES EXT07I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
      * DATE DU JOUR                                                    00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
      * HEURE                                                           00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MTIMJOUA  PIC X.                                          00001010
           02 MTIMJOUC  PIC X.                                          00001020
           02 MTIMJOUP  PIC X.                                          00001030
           02 MTIMJOUH  PIC X.                                          00001040
           02 MTIMJOUV  PIC X.                                          00001050
           02 MTIMJOUO  PIC X(5).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MPAGEAFFA      PIC X.                                     00001080
           02 MPAGEAFFC PIC X.                                          00001090
           02 MPAGEAFFP PIC X.                                          00001100
           02 MPAGEAFFH PIC X.                                          00001110
           02 MPAGEAFFV PIC X.                                          00001120
           02 MPAGEAFFO      PIC 99.                                    00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNBPAGESA      PIC X.                                     00001150
           02 MNBPAGESC PIC X.                                          00001160
           02 MNBPAGESP PIC X.                                          00001170
           02 MNBPAGESH PIC X.                                          00001180
           02 MNBPAGESV PIC X.                                          00001190
           02 MNBPAGESO      PIC 99.                                    00001200
           02 M86O OCCURS   15 TIMES .                                  00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MCAGR-1A     PIC X.                                     00001230
             03 MCAGR-1C     PIC X.                                     00001240
             03 MCAGR-1P     PIC X.                                     00001250
             03 MCAGR-1H     PIC X.                                     00001260
             03 MCAGR-1V     PIC X.                                     00001270
             03 MCAGR-1O     PIC X(5).                                  00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MLAGR-1A     PIC X.                                     00001300
             03 MLAGR-1C     PIC X.                                     00001310
             03 MLAGR-1P     PIC X.                                     00001320
             03 MLAGR-1H     PIC X.                                     00001330
             03 MLAGR-1V     PIC X.                                     00001340
             03 MLAGR-1O     PIC X(10).                                 00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MCAGR-2A     PIC X.                                     00001370
             03 MCAGR-2C     PIC X.                                     00001380
             03 MCAGR-2P     PIC X.                                     00001390
             03 MCAGR-2H     PIC X.                                     00001400
             03 MCAGR-2V     PIC X.                                     00001410
             03 MCAGR-2O     PIC X(5).                                  00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MLAGR-2A     PIC X.                                     00001440
             03 MLAGR-2C     PIC X.                                     00001450
             03 MLAGR-2P     PIC X.                                     00001460
             03 MLAGR-2H     PIC X.                                     00001470
             03 MLAGR-2V     PIC X.                                     00001480
             03 MLAGR-2O     PIC X(10).                                 00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MCAGR-3A     PIC X.                                     00001510
             03 MCAGR-3C     PIC X.                                     00001520
             03 MCAGR-3P     PIC X.                                     00001530
             03 MCAGR-3H     PIC X.                                     00001540
             03 MCAGR-3V     PIC X.                                     00001550
             03 MCAGR-3O     PIC X(5).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLAGR-3A     PIC X.                                     00001580
             03 MLAGR-3C     PIC X.                                     00001590
             03 MLAGR-3P     PIC X.                                     00001600
             03 MLAGR-3H     PIC X.                                     00001610
             03 MLAGR-3V     PIC X.                                     00001620
             03 MLAGR-3O     PIC X(10).                                 00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCAGR-4A     PIC X.                                     00001650
             03 MCAGR-4C     PIC X.                                     00001660
             03 MCAGR-4P     PIC X.                                     00001670
             03 MCAGR-4H     PIC X.                                     00001680
             03 MCAGR-4V     PIC X.                                     00001690
             03 MCAGR-4O     PIC X(5).                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MLAGR-4A     PIC X.                                     00001720
             03 MLAGR-4C     PIC X.                                     00001730
             03 MLAGR-4P     PIC X.                                     00001740
             03 MLAGR-4H     PIC X.                                     00001750
             03 MLAGR-4V     PIC X.                                     00001760
             03 MLAGR-4O     PIC X(10).                                 00001770
      * MESSAGE ERREUR                                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MLIBERRA  PIC X.                                          00001800
           02 MLIBERRC  PIC X.                                          00001810
           02 MLIBERRP  PIC X.                                          00001820
           02 MLIBERRH  PIC X.                                          00001830
           02 MLIBERRV  PIC X.                                          00001840
           02 MLIBERRO  PIC X(78).                                      00001850
      * CODE TRANSACTION                                                00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MZONCMDA  PIC X.                                          00001950
           02 MZONCMDC  PIC X.                                          00001960
           02 MZONCMDP  PIC X.                                          00001970
           02 MZONCMDH  PIC X.                                          00001980
           02 MZONCMDV  PIC X.                                          00001990
           02 MZONCMDO  PIC X(15).                                      00002000
      * CICS DE TRAVAIL                                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCICSA    PIC X.                                          00002030
           02 MCICSC    PIC X.                                          00002040
           02 MCICSP    PIC X.                                          00002050
           02 MCICSH    PIC X.                                          00002060
           02 MCICSV    PIC X.                                          00002070
           02 MCICSO    PIC X(5).                                       00002080
      * NETNAME                                                         00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNETNAMA  PIC X.                                          00002110
           02 MNETNAMC  PIC X.                                          00002120
           02 MNETNAMP  PIC X.                                          00002130
           02 MNETNAMH  PIC X.                                          00002140
           02 MNETNAMV  PIC X.                                          00002150
           02 MNETNAMO  PIC X(8).                                       00002160
      * CODE TERMINAL                                                   00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MSCREENA  PIC X.                                          00002190
           02 MSCREENC  PIC X.                                          00002200
           02 MSCREENP  PIC X.                                          00002210
           02 MSCREENH  PIC X.                                          00002220
           02 MSCREENV  PIC X.                                          00002230
           02 MSCREENO  PIC X(5).                                       00002240
                                                                                
