      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFF8900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFF8900                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF8900.                                                            
           02  FF89-PREFT1                                                      
               PIC  X(0001).                                                    
           02  FF89-NTIERS                                                      
               PIC  X(0008).                                                    
           02  FF89-TXREMISE                                                    
               PIC  S9(05)V99 COMP-3.                                           
           02  FF89-DATEFF                                                      
               PIC  X(0008).                                                    
           02  FF89-TXREMANC                                                    
               PIC  S9(05)V99 COMP-3.                                           
           02  FF89-DSYST                                                       
               PIC  S9(13) COMP-3.                                              
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFF8900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFF8900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-PREFT1-F                                                    
      *        PIC  S9(4)  COMP.                                                
      *--                                                                       
           02  FF89-PREFT1-F                                                    
               PIC  S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-NTIERS-F                                                    
      *        PIC  S9(4)  COMP.                                                
      *--                                                                       
           02  FF89-NTIERS-F                                                    
               PIC  S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-TXREMISE-F                                                  
      *        PIC  S9(4)  COMP.                                                
      *--                                                                       
           02  FF89-TXREMISE-F                                                  
               PIC  S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-DATEFF-F                                                    
      *        PIC  S9(4)  COMP.                                                
      *--                                                                       
           02  FF89-DATEFF-F                                                    
               PIC  S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-TXREMANC-F                                                  
      *        PIC  S9(4)  COMP.                                                
      *--                                                                       
           02  FF89-TXREMANC-F                                                  
               PIC  S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FF89-DSYST-F                                                     
      *        PIC  S9(4)  COMP.                                                
      *                                                                         
      *--                                                                       
           02  FF89-DSYST-F                                                     
               PIC  S9(4) COMP-5.                                               
                                                                                
      *}                                                                        
