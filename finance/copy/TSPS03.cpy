      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *  PROJET : PSE                                                           
      *  TS POUR APPLICATION PS00 : PROGRAMME PS03                              
      ****************************************************************          
      *------------------------------ LONGUEUR                                  
       01  TS-LONG              PIC S9(4) COMP-3 VALUE 898.                     
       01  TS-DONNEES.                                                          
           05 TS-LIGNE         OCCURS 16.                                       
              10  TS-CGCPLT    PIC  X(5).                                       
              10  TS-NGCPLT    PIC  X(8).                                       
              10  TS-NLIEU     PIC  X(3).                                       
              10  TS-NVENTE    PIC  X(7).                                       
              10  TS-MTGCPLT   PIC  S9(5)V99 COMP-3.                            
              10  TS-MTREMB    PIC  S9(5)V99 COMP-3.                            
              10  TS-NOMCLI    PIC  X(15).                                      
              10  TS-NCHEQUE   PIC  X(07).                                      
                                                                                
