      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - liste sections/rubriques                                  00000020
      ***************************************************************** 00000030
       01   EFM25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MTITREI   PIC X(9).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGEI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MPAGETOTI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLANL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPLANF   PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MCPLANI   PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLANL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLPLANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPLANF   PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MLPLANI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTSOCSECL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTSOCSECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTSOCSECF      PIC X.                                     00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MTSOCSECI      PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCODEL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCCODEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCODEF   PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MCCODEI   PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMASQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMASQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMASQF   PIC X.                                          00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MCMASQI   PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNSOCSECL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCNSOCSECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCNSOCSECF     PIC X.                                     00000470
           02 FILLER    PIC X(2).                                       00000480
           02 MCNSOCSECI     PIC X(3).                                  00000490
           02 LIGNEI OCCURS   12 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTIONL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MSELECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSELECTIONF  PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MSELECTIONI  PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MCODEI  PIC X(6).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCODEL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLCODEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCODEF      PIC X.                                     00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MLCODEI      PIC X(30).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MMASQUEI     PIC X(6).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCSECL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNSOCSECL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCSECF    PIC X.                                     00000680
             03 FILLER  PIC X(2).                                       00000690
             03 MNSOCSECI    PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MLIBERRI  PIC X(79).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * GCT - liste sections/rubriques                                  00000880
      ***************************************************************** 00000890
       01   EFM25O REDEFINES EFM25I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUH  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUH  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTITREA   PIC X.                                          00001030
           02 MTITREC   PIC X.                                          00001040
           02 MTITREH   PIC X.                                          00001050
           02 MTITREO   PIC X(9).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MPAGEA    PIC X.                                          00001080
           02 MPAGEC    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEO    PIC ZZ9.                                        00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MPAGETOTA      PIC X.                                     00001130
           02 MPAGETOTC PIC X.                                          00001140
           02 MPAGETOTH PIC X.                                          00001150
           02 MPAGETOTO      PIC ZZ9.                                   00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCPLANA   PIC X.                                          00001180
           02 MCPLANC   PIC X.                                          00001190
           02 MCPLANH   PIC X.                                          00001200
           02 MCPLANO   PIC X(4).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MLPLANA   PIC X.                                          00001230
           02 MLPLANC   PIC X.                                          00001240
           02 MLPLANH   PIC X.                                          00001250
           02 MLPLANO   PIC X(20).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTSOCSECA      PIC X.                                     00001280
           02 MTSOCSECC PIC X.                                          00001290
           02 MTSOCSECH PIC X.                                          00001300
           02 MTSOCSECO      PIC X(7).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCCODEA   PIC X.                                          00001330
           02 MCCODEC   PIC X.                                          00001340
           02 MCCODEH   PIC X.                                          00001350
           02 MCCODEO   PIC X(6).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCMASQA   PIC X.                                          00001380
           02 MCMASQC   PIC X.                                          00001390
           02 MCMASQH   PIC X.                                          00001400
           02 MCMASQO   PIC X(6).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCNSOCSECA     PIC X.                                     00001430
           02 MCNSOCSECC     PIC X.                                     00001440
           02 MCNSOCSECH     PIC X.                                     00001450
           02 MCNSOCSECO     PIC X(3).                                  00001460
           02 LIGNEO OCCURS   12 TIMES .                                00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MSELECTIONA  PIC X.                                     00001490
             03 MSELECTIONC  PIC X.                                     00001500
             03 MSELECTIONH  PIC X.                                     00001510
             03 MSELECTIONO  PIC X.                                     00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MCODEA  PIC X.                                          00001540
             03 MCODEC  PIC X.                                          00001550
             03 MCODEH  PIC X.                                          00001560
             03 MCODEO  PIC X(6).                                       00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MLCODEA      PIC X.                                     00001590
             03 MLCODEC PIC X.                                          00001600
             03 MLCODEH PIC X.                                          00001610
             03 MLCODEO      PIC X(30).                                 00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MMASQUEA     PIC X.                                     00001640
             03 MMASQUEC     PIC X.                                     00001650
             03 MMASQUEH     PIC X.                                     00001660
             03 MMASQUEO     PIC X(6).                                  00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MNSOCSECA    PIC X.                                     00001690
             03 MNSOCSECC    PIC X.                                     00001700
             03 MNSOCSECH    PIC X.                                     00001710
             03 MNSOCSECO    PIC X(3).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLIBERRA  PIC X.                                          00001740
           02 MLIBERRC  PIC X.                                          00001750
           02 MLIBERRH  PIC X.                                          00001760
           02 MLIBERRO  PIC X(79).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCODTRAA  PIC X.                                          00001790
           02 MCODTRAC  PIC X.                                          00001800
           02 MCODTRAH  PIC X.                                          00001810
           02 MCODTRAO  PIC X(4).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCICSA    PIC X.                                          00001840
           02 MCICSC    PIC X.                                          00001850
           02 MCICSH    PIC X.                                          00001860
           02 MCICSO    PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSCREENA  PIC X.                                          00001890
           02 MSCREENC  PIC X.                                          00001900
           02 MSCREENH  PIC X.                                          00001910
           02 MSCREENO  PIC X(4).                                       00001920
                                                                                
