      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRA0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRA0200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRA0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRA0200.                                                            
      *}                                                                        
           02  RA02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RA02-CTYPREGL                                                    
               PIC X(0001).                                                     
           02  RA02-NREGL                                                       
               PIC X(0007).                                                     
           02  RA02-DREGL                                                       
               PIC X(0008).                                                     
           02  RA02-DEDITLET                                                    
               PIC X(0008).                                                     
           02  RA02-DVALIDATION                                                 
               PIC X(0008).                                                     
           02  RA02-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  RA02-DANNUL                                                      
               PIC X(0008).                                                     
           02  RA02-NDEMANDE                                                    
               PIC X(0005).                                                     
           02  RA02-CTYPDEMANDE                                                 
               PIC X(0005).                                                     
           02  RA02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRA0200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRA0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRA0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-CTYPREGL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-CTYPREGL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-NREGL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-NREGL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DREGL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DREGL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DEDITLET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DEDITLET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DVALIDATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DVALIDATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-NDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-NDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-CTYPDEMANDE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-CTYPDEMANDE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RA02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RA02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
