      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM1300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM1300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1300.                                                            
           02  FM13-CPCG                                                        
               PIC X(0004).                                                     
           02  FM13-COMPTE                                                      
               PIC X(0006).                                                     
           02  FM13-NSEQ                                                        
               PIC X(0002).                                                     
           02  FM13-CCATEGORIE                                                  
               PIC X(0006).                                                     
           02  FM13-DSYST                                                       
               PIC S9(13)  COMP-3.                                              
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM1300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM1300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM13-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM13-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM13-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM13-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM13-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM13-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM13-CCATEGORIE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM13-CCATEGORIE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM13-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM13-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
