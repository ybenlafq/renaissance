      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - Codification rubrique                                     00000020
      ***************************************************************** 00000030
       01   EFM76I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPCODEL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MTYPCODEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPCODEF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTYPCODEI      PIC X(5).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPCODEL     COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLTYPCODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPCODEF     PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLTYPCODEI     PIC X(25).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATCODEL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MNATCODEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNATCODEF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNATCODEI      PIC X(5).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNATCODEL     COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLNATCODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNATCODEF     PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLNATCODEI     PIC X(25).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITERE1L     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MCRITERE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRITERE1F     PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCRITERE1I     PIC X(8).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITERE2L     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MCRITERE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRITERE2F     PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCRITERE2I     PIC X(8).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITERE3L     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MCRITERE3L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRITERE3F     PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCRITERE3I     PIC X(8).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCRITERE1L    COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MLCRITERE1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCRITERE1F    PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLCRITERE1I    PIC X(5).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCRITERE2L    COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MLCRITERE2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCRITERE2F    PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLCRITERE2I    PIC X(5).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCRITERE3L    COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MLCRITERE3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCRITERE3F    PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLCRITERE3I    PIC X(5).                                  00000710
           02 MTABI OCCURS   7 TIMES .                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELGL  COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MSELGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSELGF  PIC X.                                          00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MSELGI  PIC X.                                          00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUGL   COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MRUBRIQUGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MRUBRIQUGF   PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MRUBRIQUGI   PIC X(6).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUCGL  COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MRUBRIQUCGL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MRUBRIQUCGF  PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MRUBRIQUCGI  PIC X(6).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETGL    COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDEFFETGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDEFFETGF    PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDEFFETGI    PIC X(10).                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELDL  COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MSELDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSELDF  PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MSELDI  PIC X.                                          00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUDL   COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MRUBRIQUDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MRUBRIQUDF   PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MRUBRIQUDI   PIC X(6).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRIQUCDL  COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MRUBRIQUCDL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MRUBRIQUCDF  PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MRUBRIQUCDI  PIC X(6).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETDL    COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MDEFFETDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDEFFETDF    PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MDEFFETDI    PIC X(10).                                 00001040
      * ZONE CMD AIDA                                                   00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(79).                                      00001090
      * CODE TRANSACTION                                                00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      * CICS DE TRAVAIL                                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MCICSI    PIC X(5).                                       00001190
      * NETNAME                                                         00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      * CODE TERMINAL                                                   00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * GCT - Codification rubrique                                     00001310
      ***************************************************************** 00001320
       01   EFM76O REDEFINES EFM76I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
      * DATE DU JOUR                                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
      * HEURE                                                           00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MTIMJOUA  PIC X.                                          00001450
           02 MTIMJOUC  PIC X.                                          00001460
           02 MTIMJOUP  PIC X.                                          00001470
           02 MTIMJOUH  PIC X.                                          00001480
           02 MTIMJOUV  PIC X.                                          00001490
           02 MTIMJOUO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MPAGEA    PIC X.                                          00001520
           02 MPAGEC    PIC X.                                          00001530
           02 MPAGEP    PIC X.                                          00001540
           02 MPAGEH    PIC X.                                          00001550
           02 MPAGEV    PIC X.                                          00001560
           02 MPAGEO    PIC X(2).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNBPA     PIC X.                                          00001590
           02 MNBPC     PIC X.                                          00001600
           02 MNBPP     PIC X.                                          00001610
           02 MNBPH     PIC X.                                          00001620
           02 MNBPV     PIC X.                                          00001630
           02 MNBPO     PIC X(2).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCINTERFACEA   PIC X.                                     00001660
           02 MCINTERFACEC   PIC X.                                     00001670
           02 MCINTERFACEP   PIC X.                                     00001680
           02 MCINTERFACEH   PIC X.                                     00001690
           02 MCINTERFACEV   PIC X.                                     00001700
           02 MCINTERFACEO   PIC X(5).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLINTERFACEA   PIC X.                                     00001730
           02 MLINTERFACEC   PIC X.                                     00001740
           02 MLINTERFACEP   PIC X.                                     00001750
           02 MLINTERFACEH   PIC X.                                     00001760
           02 MLINTERFACEV   PIC X.                                     00001770
           02 MLINTERFACEO   PIC X(30).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MTYPCODEA      PIC X.                                     00001800
           02 MTYPCODEC PIC X.                                          00001810
           02 MTYPCODEP PIC X.                                          00001820
           02 MTYPCODEH PIC X.                                          00001830
           02 MTYPCODEV PIC X.                                          00001840
           02 MTYPCODEO      PIC X(5).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLTYPCODEA     PIC X.                                     00001870
           02 MLTYPCODEC     PIC X.                                     00001880
           02 MLTYPCODEP     PIC X.                                     00001890
           02 MLTYPCODEH     PIC X.                                     00001900
           02 MLTYPCODEV     PIC X.                                     00001910
           02 MLTYPCODEO     PIC X(25).                                 00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNATCODEA      PIC X.                                     00001940
           02 MNATCODEC PIC X.                                          00001950
           02 MNATCODEP PIC X.                                          00001960
           02 MNATCODEH PIC X.                                          00001970
           02 MNATCODEV PIC X.                                          00001980
           02 MNATCODEO      PIC X(5).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLNATCODEA     PIC X.                                     00002010
           02 MLNATCODEC     PIC X.                                     00002020
           02 MLNATCODEP     PIC X.                                     00002030
           02 MLNATCODEH     PIC X.                                     00002040
           02 MLNATCODEV     PIC X.                                     00002050
           02 MLNATCODEO     PIC X(25).                                 00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCRITERE1A     PIC X.                                     00002080
           02 MCRITERE1C     PIC X.                                     00002090
           02 MCRITERE1P     PIC X.                                     00002100
           02 MCRITERE1H     PIC X.                                     00002110
           02 MCRITERE1V     PIC X.                                     00002120
           02 MCRITERE1O     PIC X(8).                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCRITERE2A     PIC X.                                     00002150
           02 MCRITERE2C     PIC X.                                     00002160
           02 MCRITERE2P     PIC X.                                     00002170
           02 MCRITERE2H     PIC X.                                     00002180
           02 MCRITERE2V     PIC X.                                     00002190
           02 MCRITERE2O     PIC X(8).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCRITERE3A     PIC X.                                     00002220
           02 MCRITERE3C     PIC X.                                     00002230
           02 MCRITERE3P     PIC X.                                     00002240
           02 MCRITERE3H     PIC X.                                     00002250
           02 MCRITERE3V     PIC X.                                     00002260
           02 MCRITERE3O     PIC X(8).                                  00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MLCRITERE1A    PIC X.                                     00002290
           02 MLCRITERE1C    PIC X.                                     00002300
           02 MLCRITERE1P    PIC X.                                     00002310
           02 MLCRITERE1H    PIC X.                                     00002320
           02 MLCRITERE1V    PIC X.                                     00002330
           02 MLCRITERE1O    PIC X(5).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLCRITERE2A    PIC X.                                     00002360
           02 MLCRITERE2C    PIC X.                                     00002370
           02 MLCRITERE2P    PIC X.                                     00002380
           02 MLCRITERE2H    PIC X.                                     00002390
           02 MLCRITERE2V    PIC X.                                     00002400
           02 MLCRITERE2O    PIC X(5).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLCRITERE3A    PIC X.                                     00002430
           02 MLCRITERE3C    PIC X.                                     00002440
           02 MLCRITERE3P    PIC X.                                     00002450
           02 MLCRITERE3H    PIC X.                                     00002460
           02 MLCRITERE3V    PIC X.                                     00002470
           02 MLCRITERE3O    PIC X(5).                                  00002480
           02 MTABO OCCURS   7 TIMES .                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MSELGA  PIC X.                                          00002510
             03 MSELGC  PIC X.                                          00002520
             03 MSELGP  PIC X.                                          00002530
             03 MSELGH  PIC X.                                          00002540
             03 MSELGV  PIC X.                                          00002550
             03 MSELGO  PIC X.                                          00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MRUBRIQUGA   PIC X.                                     00002580
             03 MRUBRIQUGC   PIC X.                                     00002590
             03 MRUBRIQUGP   PIC X.                                     00002600
             03 MRUBRIQUGH   PIC X.                                     00002610
             03 MRUBRIQUGV   PIC X.                                     00002620
             03 MRUBRIQUGO   PIC X(6).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MRUBRIQUCGA  PIC X.                                     00002650
             03 MRUBRIQUCGC  PIC X.                                     00002660
             03 MRUBRIQUCGP  PIC X.                                     00002670
             03 MRUBRIQUCGH  PIC X.                                     00002680
             03 MRUBRIQUCGV  PIC X.                                     00002690
             03 MRUBRIQUCGO  PIC X(6).                                  00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MDEFFETGA    PIC X.                                     00002720
             03 MDEFFETGC    PIC X.                                     00002730
             03 MDEFFETGP    PIC X.                                     00002740
             03 MDEFFETGH    PIC X.                                     00002750
             03 MDEFFETGV    PIC X.                                     00002760
             03 MDEFFETGO    PIC X(10).                                 00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MSELDA  PIC X.                                          00002790
             03 MSELDC  PIC X.                                          00002800
             03 MSELDP  PIC X.                                          00002810
             03 MSELDH  PIC X.                                          00002820
             03 MSELDV  PIC X.                                          00002830
             03 MSELDO  PIC X.                                          00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MRUBRIQUDA   PIC X.                                     00002860
             03 MRUBRIQUDC   PIC X.                                     00002870
             03 MRUBRIQUDP   PIC X.                                     00002880
             03 MRUBRIQUDH   PIC X.                                     00002890
             03 MRUBRIQUDV   PIC X.                                     00002900
             03 MRUBRIQUDO   PIC X(6).                                  00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MRUBRIQUCDA  PIC X.                                     00002930
             03 MRUBRIQUCDC  PIC X.                                     00002940
             03 MRUBRIQUCDP  PIC X.                                     00002950
             03 MRUBRIQUCDH  PIC X.                                     00002960
             03 MRUBRIQUCDV  PIC X.                                     00002970
             03 MRUBRIQUCDO  PIC X(6).                                  00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MDEFFETDA    PIC X.                                     00003000
             03 MDEFFETDC    PIC X.                                     00003010
             03 MDEFFETDP    PIC X.                                     00003020
             03 MDEFFETDH    PIC X.                                     00003030
             03 MDEFFETDV    PIC X.                                     00003040
             03 MDEFFETDO    PIC X(10).                                 00003050
      * ZONE CMD AIDA                                                   00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MLIBERRA  PIC X.                                          00003080
           02 MLIBERRC  PIC X.                                          00003090
           02 MLIBERRP  PIC X.                                          00003100
           02 MLIBERRH  PIC X.                                          00003110
           02 MLIBERRV  PIC X.                                          00003120
           02 MLIBERRO  PIC X(79).                                      00003130
      * CODE TRANSACTION                                                00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
      * CICS DE TRAVAIL                                                 00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MCICSA    PIC X.                                          00003240
           02 MCICSC    PIC X.                                          00003250
           02 MCICSP    PIC X.                                          00003260
           02 MCICSH    PIC X.                                          00003270
           02 MCICSV    PIC X.                                          00003280
           02 MCICSO    PIC X(5).                                       00003290
      * NETNAME                                                         00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNETNAMA  PIC X.                                          00003320
           02 MNETNAMC  PIC X.                                          00003330
           02 MNETNAMP  PIC X.                                          00003340
           02 MNETNAMH  PIC X.                                          00003350
           02 MNETNAMV  PIC X.                                          00003360
           02 MNETNAMO  PIC X(8).                                       00003370
      * CODE TERMINAL                                                   00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MSCREENA  PIC X.                                          00003400
           02 MSCREENC  PIC X.                                          00003410
           02 MSCREENP  PIC X.                                          00003420
           02 MSCREENH  PIC X.                                          00003430
           02 MSCREENV  PIC X.                                          00003440
           02 MSCREENO  PIC X(4).                                       00003450
                                                                                
