      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - LISTE JOURNAUX                                            00000020
      ***************************************************************** 00000030
       01   EFM35I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEMAXI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSUPL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLIBSUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBSUPF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLIBSUPI  PIC X.                                          00000270
           02 LIGNEI OCCURS   14 TIMES .                                00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCMDL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MZONCMDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONCMDF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MZONCMDI     PIC X.                                     00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNATUREL     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNATUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNATUREF     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNATUREI     PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNATURL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLNATURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLNATURF     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLNATURI     PIC X(30).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MSENSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSENSF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MSENSI  PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMAJL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MDMAJL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMAJF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MDMAJI  PIC X(10).                                      00000480
      * ZONE CMD AIDA                                                   00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(79).                                      00000530
      * CODE TRANSACTION                                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      * CICS DE TRAVAIL                                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCICSI    PIC X(5).                                       00000630
      * NETNAME                                                         00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNETNAMI  PIC X(8).                                       00000680
      * CODE TERMINAL                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * GCT - LISTE JOURNAUX                                            00000750
      ***************************************************************** 00000760
       01   EFM35O REDEFINES EFM35I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
      * DATE DU JOUR                                                    00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
      * HEURE                                                           00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MPAGEA    PIC X.                                          00000960
           02 MPAGEC    PIC X.                                          00000970
           02 MPAGEP    PIC X.                                          00000980
           02 MPAGEH    PIC X.                                          00000990
           02 MPAGEV    PIC X.                                          00001000
           02 MPAGEO    PIC Z9.                                         00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGEMAXA      PIC X.                                     00001030
           02 MPAGEMAXC PIC X.                                          00001040
           02 MPAGEMAXP PIC X.                                          00001050
           02 MPAGEMAXH PIC X.                                          00001060
           02 MPAGEMAXV PIC X.                                          00001070
           02 MPAGEMAXO      PIC Z9.                                    00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLIBSUPA  PIC X.                                          00001100
           02 MLIBSUPC  PIC X.                                          00001110
           02 MLIBSUPP  PIC X.                                          00001120
           02 MLIBSUPH  PIC X.                                          00001130
           02 MLIBSUPV  PIC X.                                          00001140
           02 MLIBSUPO  PIC X.                                          00001150
           02 LIGNEO OCCURS   14 TIMES .                                00001160
             03 FILLER       PIC X(2).                                  00001170
             03 MZONCMDA     PIC X.                                     00001180
             03 MZONCMDC     PIC X.                                     00001190
             03 MZONCMDP     PIC X.                                     00001200
             03 MZONCMDH     PIC X.                                     00001210
             03 MZONCMDV     PIC X.                                     00001220
             03 MZONCMDO     PIC X.                                     00001230
             03 FILLER       PIC X(2).                                  00001240
             03 MNATUREA     PIC X.                                     00001250
             03 MNATUREC     PIC X.                                     00001260
             03 MNATUREP     PIC X.                                     00001270
             03 MNATUREH     PIC X.                                     00001280
             03 MNATUREV     PIC X.                                     00001290
             03 MNATUREO     PIC X(3).                                  00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MLNATURA     PIC X.                                     00001320
             03 MLNATURC     PIC X.                                     00001330
             03 MLNATURP     PIC X.                                     00001340
             03 MLNATURH     PIC X.                                     00001350
             03 MLNATURV     PIC X.                                     00001360
             03 MLNATURO     PIC X(30).                                 00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MSENSA  PIC X.                                          00001390
             03 MSENSC  PIC X.                                          00001400
             03 MSENSP  PIC X.                                          00001410
             03 MSENSH  PIC X.                                          00001420
             03 MSENSV  PIC X.                                          00001430
             03 MSENSO  PIC X.                                          00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MDMAJA  PIC X.                                          00001460
             03 MDMAJC  PIC X.                                          00001470
             03 MDMAJP  PIC X.                                          00001480
             03 MDMAJH  PIC X.                                          00001490
             03 MDMAJV  PIC X.                                          00001500
             03 MDMAJO  PIC X(10).                                      00001510
      * ZONE CMD AIDA                                                   00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLIBERRA  PIC X.                                          00001540
           02 MLIBERRC  PIC X.                                          00001550
           02 MLIBERRP  PIC X.                                          00001560
           02 MLIBERRH  PIC X.                                          00001570
           02 MLIBERRV  PIC X.                                          00001580
           02 MLIBERRO  PIC X(79).                                      00001590
      * CODE TRANSACTION                                                00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCODTRAA  PIC X.                                          00001620
           02 MCODTRAC  PIC X.                                          00001630
           02 MCODTRAP  PIC X.                                          00001640
           02 MCODTRAH  PIC X.                                          00001650
           02 MCODTRAV  PIC X.                                          00001660
           02 MCODTRAO  PIC X(4).                                       00001670
      * CICS DE TRAVAIL                                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCICSA    PIC X.                                          00001700
           02 MCICSC    PIC X.                                          00001710
           02 MCICSP    PIC X.                                          00001720
           02 MCICSH    PIC X.                                          00001730
           02 MCICSV    PIC X.                                          00001740
           02 MCICSO    PIC X(5).                                       00001750
      * NETNAME                                                         00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNETNAMA  PIC X.                                          00001780
           02 MNETNAMC  PIC X.                                          00001790
           02 MNETNAMP  PIC X.                                          00001800
           02 MNETNAMH  PIC X.                                          00001810
           02 MNETNAMV  PIC X.                                          00001820
           02 MNETNAMO  PIC X(8).                                       00001830
      * CODE TERMINAL                                                   00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MSCREENA  PIC X.                                          00001860
           02 MSCREENC  PIC X.                                          00001870
           02 MSCREENP  PIC X.                                          00001880
           02 MSCREENH  PIC X.                                          00001890
           02 MSCREENV  PIC X.                                          00001900
           02 MSCREENO  PIC X(4).                                       00001910
                                                                                
