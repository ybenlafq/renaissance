      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FTPCA LIEUX ANALYTIQUES                *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFTPCA.                                                             
           05  FTPCA-CTABLEG2    PIC X(15).                                     
           05  FTPCA-CTABLEG2-REDEF REDEFINES FTPCA-CTABLEG2.                   
               10  FTPCA-CODLIEU         PIC X(03).                             
           05  FTPCA-WTABLEG     PIC X(80).                                     
           05  FTPCA-WTABLEG-REDEF  REDEFINES FTPCA-WTABLEG.                    
               10  FTPCA-LIBLIEU         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFTPCA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTPCA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FTPCA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTPCA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FTPCA-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
