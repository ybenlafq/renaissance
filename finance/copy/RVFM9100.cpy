      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM9100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM9100.                                                            
           02  FM91-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  FM91-CNATOPER                                                    
               PIC X(0005).                                                     
           02  FM91-CTYPOPER                                                    
               PIC X(0005).                                                     
           02  FM91-CRUB1                                                       
               PIC X(0001).                                                     
           02  FM91-CRUB2                                                       
               PIC X(0001).                                                     
           02  FM91-CRUB3                                                       
               PIC X(0001).                                                     
           02  FM91-CSEC1                                                       
               PIC X(0001).                                                     
           02  FM91-CSEC2                                                       
               PIC X(0001).                                                     
           02  FM91-CSEC3                                                       
               PIC X(0001).                                                     
           02  FM91-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM91-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM9100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM9100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CNATOPER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CNATOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CTYPOPER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CRUB1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CRUB1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CRUB2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CRUB2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CRUB3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CRUB3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CSEC1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CSEC1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CSEC2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CSEC2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-CSEC3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-CSEC3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM91-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM91-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
