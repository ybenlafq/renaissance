      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM15-LONG             PIC S9(4) COMP-3 VALUE +187.                
       01  TS-FM15-RECORD.                                                      
           10 TS-FM15-NDEMANDE      PIC X(2).                                   
           10 TS-FM15-DCREATION     PIC X(10).                                  
           10 TS-FM15-CACID         PIC X(8).                                   
           10 TS-FM15-USER          PIC X(25).                                  
           10 TS-FM15-WMFICHE       PIC X.                                      
           10 TS-FM15-WPAPIER       PIC X.                                      
           10 TS-FM15-NETAB         PIC X(3).                                   
AM         10 TS-FM15-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM15-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM15-WTYPAUX       PIC X.                                      
           10 TS-FM15-NTIERSMIN     PIC X(8).                                   
           10 TS-FM15-NTIERSMAX     PIC X(8).                                   
           10 TS-FM15-NCPTEAUXMIN   PIC X(6).                                   
           10 TS-FM15-NCPTEAUXMAX   PIC X(6).                                   
           10 TS-FM15-NSECTIONMIN   PIC X(6).                                   
           10 TS-FM15-NSECTIONMAX   PIC X(6).                                   
           10 TS-FM15-NRUBRIQUEMIN  PIC X(6).                                   
           10 TS-FM15-NRUBRIQUEMAX  PIC X(6).                                   
           10 TS-FM15-WTOTAL        PIC X    OCCURS 6.                          
           10 TS-FM15-WNTRI         PIC X    OCCURS 6.                          
           10 TS-FM15-CNAT          PIC X(3) OCCURS 10.                         
           10 TS-FM15-CNATE         PIC X(3) OCCURS 10.                         
           10 TS-FM15-WTTRITIERS    PIC X.                                      
           10 TS-FM15-WTTRIECRIT    PIC X.                                      
           10 TS-FM15-WCOMPTESOLDE  PIC X.                                      
           10 TS-FM15-CDEVISE       PIC X(3).                                   
                                                                                
