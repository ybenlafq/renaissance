      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA33   ERA33                                              00000020
      ***************************************************************** 00000030
       01   ERA33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNSOCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPGLOBL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MZPGLOBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZPGLOBF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MZPGLOBI  PIC X.                                          00000230
      * MESSAGE DE CONFI                                                00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MPAGEI    PIC X(4).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGMAXL  COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MPAGMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGMAXF  PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MPAGMAXI  PIC X(4).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEQ1L   COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MCHEQ1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEQ1F   PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MCHEQ1I   PIC X(7).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEQ2L   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCHEQ2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEQ2F   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCHEQ2I   PIC X(7).                                       00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAVOIR1L  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MAVOIR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAVOIR1F  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MAVOIR1I  PIC X(7).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAVOIR2L  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MAVOIR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAVOIR2F  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MAVOIR2I  PIC X(7).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESP1L    COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MESP1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MESP1F    PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MESP1I    PIC X(7).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESP2L    COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MESP2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MESP2F    PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MESP2I    PIC X(7).                                       00000560
           02 MDEMI OCCURS   15 TIMES .                                 00000570
      * DETAIL DE BE                                                    00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-DEML  COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLIGNE-DEML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLIGNE-DEMF  PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLIGNE-DEMI  PIC X(76).                                 00000620
      * POINTAGE LIGNE                                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPLIGNEL    COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MZPLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZPLIGNEF    PIC X.                                     00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MZPLIGNEI    PIC X.                                     00000670
      * ZONE CMD AIDA                                                   00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MZONCMDI  PIC X(15).                                      00000720
      * MESSAGE ERREUR                                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(58).                                      00000770
      * CODE TRANSACTION                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      * CICS DE TRAVAIL                                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      * NETNAME                                                         00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      * CODE TERMINAL                                                   00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(5).                                       00000970
      ***************************************************************** 00000980
      * SDF: ERA33   ERA33                                              00000990
      ***************************************************************** 00001000
       01   ERA33O REDEFINES ERA33I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
      * DATE DU JOUR(JJ/MM/AAAA)                                        00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
      * HEURE                                                           00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MNSOCA    PIC X.                                          00001200
           02 MNSOCC    PIC X.                                          00001210
           02 MNSOCP    PIC X.                                          00001220
           02 MNSOCH    PIC X.                                          00001230
           02 MNSOCV    PIC X.                                          00001240
           02 MNSOCO    PIC X(3).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MZPGLOBA  PIC X.                                          00001270
           02 MZPGLOBC  PIC X.                                          00001280
           02 MZPGLOBP  PIC X.                                          00001290
           02 MZPGLOBH  PIC X.                                          00001300
           02 MZPGLOBV  PIC X.                                          00001310
           02 MZPGLOBO  PIC X.                                          00001320
      * MESSAGE DE CONFI                                                00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC 9(4).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGMAXA  PIC X.                                          00001420
           02 MPAGMAXC  PIC X.                                          00001430
           02 MPAGMAXP  PIC X.                                          00001440
           02 MPAGMAXH  PIC X.                                          00001450
           02 MPAGMAXV  PIC X.                                          00001460
           02 MPAGMAXO  PIC 9(4).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCHEQ1A   PIC X.                                          00001490
           02 MCHEQ1C   PIC X.                                          00001500
           02 MCHEQ1P   PIC X.                                          00001510
           02 MCHEQ1H   PIC X.                                          00001520
           02 MCHEQ1V   PIC X.                                          00001530
           02 MCHEQ1O   PIC X(7).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCHEQ2A   PIC X.                                          00001560
           02 MCHEQ2C   PIC X.                                          00001570
           02 MCHEQ2P   PIC X.                                          00001580
           02 MCHEQ2H   PIC X.                                          00001590
           02 MCHEQ2V   PIC X.                                          00001600
           02 MCHEQ2O   PIC X(7).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MAVOIR1A  PIC X.                                          00001630
           02 MAVOIR1C  PIC X.                                          00001640
           02 MAVOIR1P  PIC X.                                          00001650
           02 MAVOIR1H  PIC X.                                          00001660
           02 MAVOIR1V  PIC X.                                          00001670
           02 MAVOIR1O  PIC X(7).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MAVOIR2A  PIC X.                                          00001700
           02 MAVOIR2C  PIC X.                                          00001710
           02 MAVOIR2P  PIC X.                                          00001720
           02 MAVOIR2H  PIC X.                                          00001730
           02 MAVOIR2V  PIC X.                                          00001740
           02 MAVOIR2O  PIC X(7).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MESP1A    PIC X.                                          00001770
           02 MESP1C    PIC X.                                          00001780
           02 MESP1P    PIC X.                                          00001790
           02 MESP1H    PIC X.                                          00001800
           02 MESP1V    PIC X.                                          00001810
           02 MESP1O    PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MESP2A    PIC X.                                          00001840
           02 MESP2C    PIC X.                                          00001850
           02 MESP2P    PIC X.                                          00001860
           02 MESP2H    PIC X.                                          00001870
           02 MESP2V    PIC X.                                          00001880
           02 MESP2O    PIC X(7).                                       00001890
           02 MDEMO OCCURS   15 TIMES .                                 00001900
      * DETAIL DE BE                                                    00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MLIGNE-DEMA  PIC X.                                     00001930
             03 MLIGNE-DEMC  PIC X.                                     00001940
             03 MLIGNE-DEMP  PIC X.                                     00001950
             03 MLIGNE-DEMH  PIC X.                                     00001960
             03 MLIGNE-DEMV  PIC X.                                     00001970
             03 MLIGNE-DEMO  PIC X(76).                                 00001980
      * POINTAGE LIGNE                                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MZPLIGNEA    PIC X.                                     00002010
             03 MZPLIGNEC    PIC X.                                     00002020
             03 MZPLIGNEP    PIC X.                                     00002030
             03 MZPLIGNEH    PIC X.                                     00002040
             03 MZPLIGNEV    PIC X.                                     00002050
             03 MZPLIGNEO    PIC X.                                     00002060
      * ZONE CMD AIDA                                                   00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MZONCMDA  PIC X.                                          00002090
           02 MZONCMDC  PIC X.                                          00002100
           02 MZONCMDP  PIC X.                                          00002110
           02 MZONCMDH  PIC X.                                          00002120
           02 MZONCMDV  PIC X.                                          00002130
           02 MZONCMDO  PIC X(15).                                      00002140
      * MESSAGE ERREUR                                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLIBERRA  PIC X.                                          00002170
           02 MLIBERRC  PIC X.                                          00002180
           02 MLIBERRP  PIC X.                                          00002190
           02 MLIBERRH  PIC X.                                          00002200
           02 MLIBERRV  PIC X.                                          00002210
           02 MLIBERRO  PIC X(58).                                      00002220
      * CODE TRANSACTION                                                00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCODTRAA  PIC X.                                          00002250
           02 MCODTRAC  PIC X.                                          00002260
           02 MCODTRAP  PIC X.                                          00002270
           02 MCODTRAH  PIC X.                                          00002280
           02 MCODTRAV  PIC X.                                          00002290
           02 MCODTRAO  PIC X(4).                                       00002300
      * CICS DE TRAVAIL                                                 00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
      * NETNAME                                                         00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNETNAMA  PIC X.                                          00002410
           02 MNETNAMC  PIC X.                                          00002420
           02 MNETNAMP  PIC X.                                          00002430
           02 MNETNAMH  PIC X.                                          00002440
           02 MNETNAMV  PIC X.                                          00002450
           02 MNETNAMO  PIC X(8).                                       00002460
      * CODE TERMINAL                                                   00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(5).                                       00002540
                                                                                
