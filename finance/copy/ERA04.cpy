      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA04   ERA04                                              00000020
      ***************************************************************** 00000030
       01   ERA04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * type dem avoir/diff                                             00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDEML  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MTYPDEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPDEMF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MTYPDEMI  PIC X(20).                                      00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MNSOCI    PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPGLOBL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MZPGLOBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZPGLOBF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MZPGLOBI  PIC X.                                          00000280
      * MESSAGE DE CONFI                                                00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEI    PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGMAXL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MPAGMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGMAXF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPAGMAXI  PIC X(4).                                       00000370
           02 MDEMI OCCURS   15 TIMES .                                 00000380
      * DETAIL DE BE                                                    00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-DEML  COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MLIGNE-DEML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLIGNE-DEMF  PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MLIGNE-DEMI  PIC X(76).                                 00000430
      * POINTAGE LIGNE                                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPLIGNEL    COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MZPLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZPLIGNEF    PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MZPLIGNEI    PIC X.                                     00000480
      * ZONE CMD AIDA                                                   00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MZONCMDI  PIC X(15).                                      00000530
      * MESSAGE ERREUR                                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(58).                                      00000580
      * CODE TRANSACTION                                                00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      * CICS DE TRAVAIL                                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCICSI    PIC X(5).                                       00000680
      * NETNAME                                                         00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNETNAMI  PIC X(8).                                       00000730
      * CODE TERMINAL                                                   00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(5).                                       00000780
      ***************************************************************** 00000790
      * SDF: ERA04   ERA04                                              00000800
      ***************************************************************** 00000810
       01   ERA04O REDEFINES ERA04I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MDATJOUA  PIC X.                                          00000860
           02 MDATJOUC  PIC X.                                          00000870
           02 MDATJOUP  PIC X.                                          00000880
           02 MDATJOUH  PIC X.                                          00000890
           02 MDATJOUV  PIC X.                                          00000900
           02 MDATJOUO  PIC X(10).                                      00000910
      * HEURE                                                           00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUP  PIC X.                                          00000960
           02 MTIMJOUH  PIC X.                                          00000970
           02 MTIMJOUV  PIC X.                                          00000980
           02 MTIMJOUO  PIC X(5).                                       00000990
      * type dem avoir/diff                                             00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MTYPDEMA  PIC X.                                          00001020
           02 MTYPDEMC  PIC X.                                          00001030
           02 MTYPDEMP  PIC X.                                          00001040
           02 MTYPDEMH  PIC X.                                          00001050
           02 MTYPDEMV  PIC X.                                          00001060
           02 MTYPDEMO  PIC X(20).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNSOCA    PIC X.                                          00001090
           02 MNSOCC    PIC X.                                          00001100
           02 MNSOCP    PIC X.                                          00001110
           02 MNSOCH    PIC X.                                          00001120
           02 MNSOCV    PIC X.                                          00001130
           02 MNSOCO    PIC X(3).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MZPGLOBA  PIC X.                                          00001160
           02 MZPGLOBC  PIC X.                                          00001170
           02 MZPGLOBP  PIC X.                                          00001180
           02 MZPGLOBH  PIC X.                                          00001190
           02 MZPGLOBV  PIC X.                                          00001200
           02 MZPGLOBO  PIC X.                                          00001210
      * MESSAGE DE CONFI                                                00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MPAGEA    PIC X.                                          00001240
           02 MPAGEC    PIC X.                                          00001250
           02 MPAGEP    PIC X.                                          00001260
           02 MPAGEH    PIC X.                                          00001270
           02 MPAGEV    PIC X.                                          00001280
           02 MPAGEO    PIC 9(4).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGMAXA  PIC X.                                          00001310
           02 MPAGMAXC  PIC X.                                          00001320
           02 MPAGMAXP  PIC X.                                          00001330
           02 MPAGMAXH  PIC X.                                          00001340
           02 MPAGMAXV  PIC X.                                          00001350
           02 MPAGMAXO  PIC 9(4).                                       00001360
           02 MDEMO OCCURS   15 TIMES .                                 00001370
      * DETAIL DE BE                                                    00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MLIGNE-DEMA  PIC X.                                     00001400
             03 MLIGNE-DEMC  PIC X.                                     00001410
             03 MLIGNE-DEMP  PIC X.                                     00001420
             03 MLIGNE-DEMH  PIC X.                                     00001430
             03 MLIGNE-DEMV  PIC X.                                     00001440
             03 MLIGNE-DEMO  PIC X(76).                                 00001450
      * POINTAGE LIGNE                                                  00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MZPLIGNEA    PIC X.                                     00001480
             03 MZPLIGNEC    PIC X.                                     00001490
             03 MZPLIGNEP    PIC X.                                     00001500
             03 MZPLIGNEH    PIC X.                                     00001510
             03 MZPLIGNEV    PIC X.                                     00001520
             03 MZPLIGNEO    PIC X.                                     00001530
      * ZONE CMD AIDA                                                   00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MZONCMDA  PIC X.                                          00001560
           02 MZONCMDC  PIC X.                                          00001570
           02 MZONCMDP  PIC X.                                          00001580
           02 MZONCMDH  PIC X.                                          00001590
           02 MZONCMDV  PIC X.                                          00001600
           02 MZONCMDO  PIC X(15).                                      00001610
      * MESSAGE ERREUR                                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBERRA  PIC X.                                          00001640
           02 MLIBERRC  PIC X.                                          00001650
           02 MLIBERRP  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRV  PIC X.                                          00001680
           02 MLIBERRO  PIC X(58).                                      00001690
      * CODE TRANSACTION                                                00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCODTRAA  PIC X.                                          00001720
           02 MCODTRAC  PIC X.                                          00001730
           02 MCODTRAP  PIC X.                                          00001740
           02 MCODTRAH  PIC X.                                          00001750
           02 MCODTRAV  PIC X.                                          00001760
           02 MCODTRAO  PIC X(4).                                       00001770
      * CICS DE TRAVAIL                                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
      * NETNAME                                                         00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNETNAMA  PIC X.                                          00001880
           02 MNETNAMC  PIC X.                                          00001890
           02 MNETNAMP  PIC X.                                          00001900
           02 MNETNAMH  PIC X.                                          00001910
           02 MNETNAMV  PIC X.                                          00001920
           02 MNETNAMO  PIC X(8).                                       00001930
      * CODE TERMINAL                                                   00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSCREENA  PIC X.                                          00001960
           02 MSCREENC  PIC X.                                          00001970
           02 MSCREENP  PIC X.                                          00001980
           02 MSCREENH  PIC X.                                          00001990
           02 MSCREENV  PIC X.                                          00002000
           02 MSCREENO  PIC X(5).                                       00002010
                                                                                
