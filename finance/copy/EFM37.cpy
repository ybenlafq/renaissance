      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - Aide � la saisie - section                                00000020
      ***************************************************************** 00000030
       01   EFM37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTCREL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSECTCREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTCREF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MSECTCREI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSECTCREL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLSECTCREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSECTCREF     PIC X.                                     00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLSECTCREI     PIC X(60).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTORIGL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MSECTORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSECTORIGF     PIC X.                                     00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MSECTORIGI     PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSECTORIGL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLSECTORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLSECTORIGF    PIC X.                                     00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MLSECTORIGI    PIC X(60).                                 00000370
           02 LIGNEI OCCURS   12 TIMES .                                00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MSELI   PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MCODEI  PIC X(6).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBCOURTL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLIBCOURTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIBCOURTF   PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MLIBCOURTI   PIC X(15).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBLONGL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLIBLONGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBLONGF    PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MLIBLONGI    PIC X(30).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMASQUEL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MMASQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMASQUEF     PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MMASQUEI     PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWFIRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MWFIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWFIRF  PIC X.                                          00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MWFIRI  PIC X.                                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCSECTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSOCSECTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCSECTF    PIC X.                                     00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MSOCSECTI    PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MLIBERRI  PIC X(79).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * GCT - Aide � la saisie - section                                00000840
      ***************************************************************** 00000850
       01   EFM37O REDEFINES EFM37I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUO  PIC X(10).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUH  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEH    PIC X.                                          00001010
           02 MPAGEO    PIC ZZ9.                                        00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MPAGETOTA      PIC X.                                     00001040
           02 MPAGETOTC PIC X.                                          00001050
           02 MPAGETOTH PIC X.                                          00001060
           02 MPAGETOTO      PIC ZZ9.                                   00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MSECTCREA      PIC X.                                     00001090
           02 MSECTCREC PIC X.                                          00001100
           02 MSECTCREH PIC X.                                          00001110
           02 MSECTCREO      PIC X(3).                                  00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MLSECTCREA     PIC X.                                     00001140
           02 MLSECTCREC     PIC X.                                     00001150
           02 MLSECTCREH     PIC X.                                     00001160
           02 MLSECTCREO     PIC X(60).                                 00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MSECTORIGA     PIC X.                                     00001190
           02 MSECTORIGC     PIC X.                                     00001200
           02 MSECTORIGH     PIC X.                                     00001210
           02 MSECTORIGO     PIC X(3).                                  00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLSECTORIGA    PIC X.                                     00001240
           02 MLSECTORIGC    PIC X.                                     00001250
           02 MLSECTORIGH    PIC X.                                     00001260
           02 MLSECTORIGO    PIC X(60).                                 00001270
           02 LIGNEO OCCURS   12 TIMES .                                00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MSELA   PIC X.                                          00001300
             03 MSELC   PIC X.                                          00001310
             03 MSELH   PIC X.                                          00001320
             03 MSELO   PIC X.                                          00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MCODEA  PIC X.                                          00001350
             03 MCODEC  PIC X.                                          00001360
             03 MCODEH  PIC X.                                          00001370
             03 MCODEO  PIC X(6).                                       00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MLIBCOURTA   PIC X.                                     00001400
             03 MLIBCOURTC   PIC X.                                     00001410
             03 MLIBCOURTH   PIC X.                                     00001420
             03 MLIBCOURTO   PIC X(15).                                 00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MLIBLONGA    PIC X.                                     00001450
             03 MLIBLONGC    PIC X.                                     00001460
             03 MLIBLONGH    PIC X.                                     00001470
             03 MLIBLONGO    PIC X(30).                                 00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MMASQUEA     PIC X.                                     00001500
             03 MMASQUEC     PIC X.                                     00001510
             03 MMASQUEH     PIC X.                                     00001520
             03 MMASQUEO     PIC X(6).                                  00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MWFIRA  PIC X.                                          00001550
             03 MWFIRC  PIC X.                                          00001560
             03 MWFIRH  PIC X.                                          00001570
             03 MWFIRO  PIC X.                                          00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MSOCSECTA    PIC X.                                     00001600
             03 MSOCSECTC    PIC X.                                     00001610
             03 MSOCSECTH    PIC X.                                     00001620
             03 MSOCSECTO    PIC X(3).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBERRA  PIC X.                                          00001650
           02 MLIBERRC  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRO  PIC X(79).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCODTRAA  PIC X.                                          00001700
           02 MCODTRAC  PIC X.                                          00001710
           02 MCODTRAH  PIC X.                                          00001720
           02 MCODTRAO  PIC X(4).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCICSA    PIC X.                                          00001750
           02 MCICSC    PIC X.                                          00001760
           02 MCICSH    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MSCREENA  PIC X.                                          00001800
           02 MSCREENC  PIC X.                                          00001810
           02 MSCREENH  PIC X.                                          00001820
           02 MSCREENO  PIC X(4).                                       00001830
                                                                                
