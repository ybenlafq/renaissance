      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RAINT PARAM POUR INTERFACE RA00        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRAINT.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRAINT.                                                             
      *}                                                                        
           05  RAINT-CTABLEG2    PIC X(15).                                     
           05  RAINT-CTABLEG2-REDEF REDEFINES RAINT-CTABLEG2.                   
               10  RAINT-NSOC            PIC X(03).                             
               10  RAINT-CINT            PIC X(04).                             
               10  RAINT-CRIT1           PIC X(05).                             
               10  RAINT-CRIT2           PIC X(03).                             
           05  RAINT-WTABLEG     PIC X(80).                                     
           05  RAINT-WTABLEG-REDEF  REDEFINES RAINT-WTABLEG.                    
               10  RAINT-WACTIF          PIC X(01).                             
               10  RAINT-VALEUR          PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRAINT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRAINT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RAINT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RAINT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RAINT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RAINT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
