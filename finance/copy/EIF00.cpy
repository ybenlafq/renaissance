      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MENU GENERAL INTERFACE FINANCEMT                                00000020
      ***************************************************************** 00000030
       01   EIF00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * ZONE CMD AIDA                                                   00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(2).                                       00000190
           02 MZONCMDI  PIC X(15).                                      00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCTAL      COMP PIC S9(4).                            00000210
      *--                                                                       
           02 MNSOCCTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCCTAF      PIC X.                                     00000220
           02 FILLER    PIC X(2).                                       00000230
           02 MNSOCCTAI      PIC X(3).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000260
           02 FILLER    PIC X(2).                                       00000270
           02 MNETABI   PIC X(3).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGAN2L      COMP PIC S9(4).                            00000290
      *--                                                                       
           02 MCORGAN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCORGAN2F      PIC X.                                     00000300
           02 FILLER    PIC X(2).                                       00000310
           02 MCORGAN2I      PIC X(3).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGAN3L      COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MCORGAN3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCORGAN3F      PIC X.                                     00000340
           02 FILLER    PIC X(2).                                       00000350
           02 MCORGAN3I      PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRESENTL      COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MPRESENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRESENTF      PIC X.                                     00000380
           02 FILLER    PIC X(2).                                       00000390
           02 MPRESENTI      PIC X.                                     00000400
      * MESSAGE ERREUR                                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MLIBERRI  PIC X(79).                                      00000450
      * CODE TRANSACTION                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(2).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      * CICS DE TRAVAIL                                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      * NETNAME                                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MNETNAMI  PIC X(8).                                       00000600
      * CODE TERMINAL                                                   00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MSCREENI  PIC X(5).                                       00000650
      ***************************************************************** 00000660
      * MENU GENERAL INTERFACE FINANCEMT                                00000670
      ***************************************************************** 00000680
       01   EIF00O REDEFINES EIF00I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
      * DATE DU JOUR                                                    00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUO  PIC X(10).                                      00000760
      * HEURE                                                           00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MTIMJOUA  PIC X.                                          00000790
           02 MTIMJOUC  PIC X.                                          00000800
           02 MTIMJOUH  PIC X.                                          00000810
           02 MTIMJOUO  PIC X(5).                                       00000820
      * ZONE CMD AIDA                                                   00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MZONCMDA  PIC X.                                          00000850
           02 MZONCMDC  PIC X.                                          00000860
           02 MZONCMDH  PIC X.                                          00000870
           02 MZONCMDO  PIC X(15).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MNSOCCTAA      PIC X.                                     00000900
           02 MNSOCCTAC PIC X.                                          00000910
           02 MNSOCCTAH PIC X.                                          00000920
           02 MNSOCCTAO      PIC X(3).                                  00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNETABA   PIC X.                                          00000950
           02 MNETABC   PIC X.                                          00000960
           02 MNETABH   PIC X.                                          00000970
           02 MNETABO   PIC X(3).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MCORGAN2A      PIC X.                                     00001000
           02 MCORGAN2C PIC X.                                          00001010
           02 MCORGAN2H PIC X.                                          00001020
           02 MCORGAN2O      PIC X(3).                                  00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCORGAN3A      PIC X.                                     00001050
           02 MCORGAN3C PIC X.                                          00001060
           02 MCORGAN3H PIC X.                                          00001070
           02 MCORGAN3O      PIC X(3).                                  00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MPRESENTA      PIC X.                                     00001100
           02 MPRESENTC PIC X.                                          00001110
           02 MPRESENTH PIC X.                                          00001120
           02 MPRESENTO      PIC X.                                     00001130
      * MESSAGE ERREUR                                                  00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLIBERRA  PIC X.                                          00001160
           02 MLIBERRC  PIC X.                                          00001170
           02 MLIBERRH  PIC X.                                          00001180
           02 MLIBERRO  PIC X(79).                                      00001190
      * CODE TRANSACTION                                                00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCODTRAA  PIC X.                                          00001220
           02 MCODTRAC  PIC X.                                          00001230
           02 MCODTRAH  PIC X.                                          00001240
           02 MCODTRAO  PIC X(4).                                       00001250
      * CICS DE TRAVAIL                                                 00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCICSA    PIC X.                                          00001280
           02 MCICSC    PIC X.                                          00001290
           02 MCICSH    PIC X.                                          00001300
           02 MCICSO    PIC X(5).                                       00001310
      * NETNAME                                                         00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNETNAMA  PIC X.                                          00001340
           02 MNETNAMC  PIC X.                                          00001350
           02 MNETNAMH  PIC X.                                          00001360
           02 MNETNAMO  PIC X(8).                                       00001370
      * CODE TERMINAL                                                   00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MSCREENA  PIC X.                                          00001400
           02 MSCREENC  PIC X.                                          00001410
           02 MSCREENH  PIC X.                                          00001420
           02 MSCREENO  PIC X(5).                                       00001430
                                                                                
