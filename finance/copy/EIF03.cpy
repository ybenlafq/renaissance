      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * PARAMETRAGE DES IDENTIFIANTS                                    00000020
      ***************************************************************** 00000030
       01   EIF03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGANL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCORGANF  PIC X.                                          00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MCORGANI  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORGANL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLORGANL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLORGANF  PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MLORGANI  PIC X(20).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRESENTL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MPRESENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPRESENTF      PIC X.                                     00000250
           02 FILLER    PIC X(2).                                       00000260
           02 MPRESENTI      PIC X.                                     00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MNPAGEI   PIC X(2).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MNBPAGESI      PIC X(2).                                  00000350
           02 FILLER  OCCURS   14 TIMES .                               00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANNULL      COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MANNULL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MANNULF      PIC X.                                     00000380
             03 FILLER  PIC X(2).                                       00000390
             03 MANNULI      PIC X.                                     00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIDENTL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MNIDENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNIDENTF     PIC X.                                     00000420
             03 FILLER  PIC X(2).                                       00000430
             03 MNIDENTI     PIC X(22).                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000460
             03 FILLER  PIC X(2).                                       00000470
             03 MNSOCIETEI   PIC X(3).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000500
             03 FILLER  PIC X(2).                                       00000510
             03 MNLIEUI      PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00000540
             03 FILLER  PIC X(2).                                       00000550
             03 MLLIEUI      PIC X(20).                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCCTAL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MNSOCCTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCCTAF    PIC X.                                     00000580
             03 FILLER  PIC X(2).                                       00000590
             03 MNSOCCTAI    PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNETABL      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MNETABL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNETABF      PIC X.                                     00000620
             03 FILLER  PIC X(2).                                       00000630
             03 MNETABI      PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCBANQUEL    COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MCBANQUEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCBANQUEF    PIC X.                                     00000660
             03 FILLER  PIC X(2).                                       00000670
             03 MCBANQUEI    PIC X(2).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLBANQUEL    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MLBANQUEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLBANQUEF    PIC X.                                     00000700
             03 FILLER  PIC X(2).                                       00000710
             03 MLBANQUEI    PIC X(10).                                 00000720
      * MESSAGE ERREUR                                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MLIBERRI  PIC X(79).                                      00000770
      * CODE TRANSACTION                                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      * ZONE CMD AIDA                                                   00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MZONCMDI  PIC X(15).                                      00000870
      * CICS DE TRAVAIL                                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MCICSI    PIC X(5).                                       00000920
      * NETNAME                                                         00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      * CODE TERMINAL                                                   00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MSCREENI  PIC X(5).                                       00001020
      ***************************************************************** 00001030
      * PARAMETRAGE DES IDENTIFIANTS                                    00001040
      ***************************************************************** 00001050
       01   EIF03O REDEFINES EIF03I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
      * DATE DU JOUR                                                    00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MDATJOUA  PIC X.                                          00001100
           02 MDATJOUC  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
      * HEURE                                                           00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUO  PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MCORGANA  PIC X.                                          00001210
           02 MCORGANC  PIC X.                                          00001220
           02 MCORGANH  PIC X.                                          00001230
           02 MCORGANO  PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MLORGANA  PIC X.                                          00001260
           02 MLORGANC  PIC X.                                          00001270
           02 MLORGANH  PIC X.                                          00001280
           02 MLORGANO  PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPRESENTA      PIC X.                                     00001310
           02 MPRESENTC PIC X.                                          00001320
           02 MPRESENTH PIC X.                                          00001330
           02 MPRESENTO      PIC X.                                     00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNPAGEA   PIC X.                                          00001360
           02 MNPAGEC   PIC X.                                          00001370
           02 MNPAGEH   PIC X.                                          00001380
           02 MNPAGEO   PIC ZZ.                                         00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNBPAGESA      PIC X.                                     00001410
           02 MNBPAGESC PIC X.                                          00001420
           02 MNBPAGESH PIC X.                                          00001430
           02 MNBPAGESO      PIC ZZ.                                    00001440
           02 FILLER  OCCURS   14 TIMES .                               00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MANNULA      PIC X.                                     00001470
             03 MANNULC PIC X.                                          00001480
             03 MANNULH PIC X.                                          00001490
             03 MANNULO      PIC X.                                     00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MNIDENTA     PIC X.                                     00001520
             03 MNIDENTC     PIC X.                                     00001530
             03 MNIDENTH     PIC X.                                     00001540
             03 MNIDENTO     PIC X(22).                                 00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MNSOCIETEA   PIC X.                                     00001570
             03 MNSOCIETEC   PIC X.                                     00001580
             03 MNSOCIETEH   PIC X.                                     00001590
             03 MNSOCIETEO   PIC X(3).                                  00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MNLIEUA      PIC X.                                     00001620
             03 MNLIEUC PIC X.                                          00001630
             03 MNLIEUH PIC X.                                          00001640
             03 MNLIEUO      PIC X(3).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MLLIEUA      PIC X.                                     00001670
             03 MLLIEUC PIC X.                                          00001680
             03 MLLIEUH PIC X.                                          00001690
             03 MLLIEUO      PIC X(20).                                 00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MNSOCCTAA    PIC X.                                     00001720
             03 MNSOCCTAC    PIC X.                                     00001730
             03 MNSOCCTAH    PIC X.                                     00001740
             03 MNSOCCTAO    PIC X(3).                                  00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MNETABA      PIC X.                                     00001770
             03 MNETABC PIC X.                                          00001780
             03 MNETABH PIC X.                                          00001790
             03 MNETABO      PIC X(3).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MCBANQUEA    PIC X.                                     00001820
             03 MCBANQUEC    PIC X.                                     00001830
             03 MCBANQUEH    PIC X.                                     00001840
             03 MCBANQUEO    PIC X(2).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MLBANQUEA    PIC X.                                     00001870
             03 MLBANQUEC    PIC X.                                     00001880
             03 MLBANQUEH    PIC X.                                     00001890
             03 MLBANQUEO    PIC X(10).                                 00001900
      * MESSAGE ERREUR                                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLIBERRA  PIC X.                                          00001930
           02 MLIBERRC  PIC X.                                          00001940
           02 MLIBERRH  PIC X.                                          00001950
           02 MLIBERRO  PIC X(79).                                      00001960
      * CODE TRANSACTION                                                00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCODTRAA  PIC X.                                          00001990
           02 MCODTRAC  PIC X.                                          00002000
           02 MCODTRAH  PIC X.                                          00002010
           02 MCODTRAO  PIC X(4).                                       00002020
      * ZONE CMD AIDA                                                   00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MZONCMDA  PIC X.                                          00002050
           02 MZONCMDC  PIC X.                                          00002060
           02 MZONCMDH  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
      * CICS DE TRAVAIL                                                 00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSH    PIC X.                                          00002130
           02 MCICSO    PIC X(5).                                       00002140
      * NETNAME                                                         00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNETNAMA  PIC X.                                          00002170
           02 MNETNAMC  PIC X.                                          00002180
           02 MNETNAMH  PIC X.                                          00002190
           02 MNETNAMO  PIC X(8).                                       00002200
      * CODE TERMINAL                                                   00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MSCREENA  PIC X.                                          00002230
           02 MSCREENC  PIC X.                                          00002240
           02 MSCREENH  PIC X.                                          00002250
           02 MSCREENO  PIC X(5).                                       00002260
                                                                                
