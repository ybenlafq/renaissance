      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA PROGRAMMES TFTCCC ET BFTCCC                            00020001
      *     CONTROLE DE LA CLE COMPTABLE GCT2                           00030006
      * ============================================================            
      * MODIF AP DSA005 - LE 12/04/00                                           
      * AJOUT DECOMPOSITION COMM-FTCC-KO, POUR SAVOIR QUAND IL Y A              
      * REJET POUR CAUSE DE COMPTE DEJA DETAXE SUR LA PERIODE                   
      * MODIF REFERENCEE PAR 'AP' EN COL 1                                      
      **************************************************************    00050000
       01 COMMFTCC.                                                     00060001
 1        02 COMM-FTCC-NENTITE        PIC  X(5).                        00080001
 6        02 COMM-FTCC-NEXER          PIC  X(4).                        00120001
10        02 COMM-FTCC-NPER           PIC  X(2).                        00140001
12        02 COMM-FTCC-DCOMPTA        PIC  X(8).                        00160008
20        02 COMM-FTCC-NSOC           PIC  X(5).                        00190001
25        02 COMM-FTCC-NETAB          PIC  X(3).                        00260001
28        02 COMM-FTCC-COMPTE         PIC  X(6).                        00200001
34        02 COMM-FTCC-SECTION        PIC  X(6).                        00220001
40        02 COMM-FTCC-RUBRIQUE       PIC  X(6).                        00230001
46        02 COMM-FTCC-STEAPP         PIC  X(5).                        00250001
51        02 COMM-FTCC-RCODE          PIC  X(1).                        00280001
             88 COMM-FTCC-OK          VALUE '0'.                        00290001
AP           88 COMM-FTCC-KO          VALUE '1' THRU '2'.               00300001
AP           88 COMM-FTCC-KO-STD      VALUE '1'.                        00300001
AP           88 COMM-FTCC-KO-DETAXE   VALUE '2'.                        00300001
52        02 COMM-FTCC-SQLCODE        PIC  ---9.                        00280001
56        02 COMM-FTCC-ERREUR         PIC X(80).                        00320007
                                                                                
