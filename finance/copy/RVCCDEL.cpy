      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CCDEL CTRL CAISSES- DELAI SACOCHES     *        
      *----------------------------------------------------------------*        
       01  RVCCDEL.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  CCDEL-CTABLEG2    PIC X(15).                                     
           05  CCDEL-CTABLEG2-REDEF REDEFINES CCDEL-CTABLEG2.                   
               10  CCDEL-NSOC            PIC X(03).                             
               10  CCDEL-NLIEU           PIC X(03).                             
               10  CCDEL-JOUR            PIC X(03).                             
           05  CCDEL-WTABLEG     PIC X(80).                                     
           05  CCDEL-WTABLEG-REDEF  REDEFINES CCDEL-WTABLEG.                    
               10  CCDEL-WACTIF          PIC X(01).                             
               10  CCDEL-DELAI           PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CCDEL-DELAI-N        REDEFINES CCDEL-DELAI                   
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCCDEL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCDEL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CCDEL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCDEL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CCDEL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
