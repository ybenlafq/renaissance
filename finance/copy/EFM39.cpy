      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - TABLES GENERALISEES                                       00000020
      ***************************************************************** 00000030
       01   EFM39I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MNENTITEI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MLENTITEI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTIONL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MFONCTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFONCTIONF     PIC X.                                     00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MFONCTIONI     PIC X(15).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MTITREI   PIC X(33).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMOISL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MMOISL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMOISF    PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MMOISI    PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEEL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MANNEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MANNEEF   PIC X.                                          00000430
           02 FILLER    PIC X(2).                                       00000440
           02 MANNEEI   PIC X(4).                                       00000450
           02 TABLEAUI OCCURS   12 TIMES .                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MSELI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNEXERL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNEXERL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNEXERF      PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MNEXERI      PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMOISDEBL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNMOISDEBL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNMOISDEBF   PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MNMOISDEBI   PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMOISDEBL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLMOISDEBL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLMOISDEBF   PIC X.                                     00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MLMOISDEBI   PIC X(10).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNANCIVL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNANCIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNANCIVF     PIC X.                                     00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MNANCIVI     PIC X(4).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MLIBERRI  PIC X(79).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * GCT - TABLES GENERALISEES                                       00000840
      ***************************************************************** 00000850
       01   EFM39O REDEFINES EFM39I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUO  PIC X(10).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUH  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEH    PIC X.                                          00001010
           02 MPAGEO    PIC Z9.                                         00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MNBPA     PIC X.                                          00001040
           02 MNBPC     PIC X.                                          00001050
           02 MNBPH     PIC X.                                          00001060
           02 MNBPO     PIC Z9.                                         00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNENTITEA      PIC X.                                     00001090
           02 MNENTITEC PIC X.                                          00001100
           02 MNENTITEH PIC X.                                          00001110
           02 MNENTITEO      PIC X(5).                                  00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MLENTITEA      PIC X.                                     00001140
           02 MLENTITEC PIC X.                                          00001150
           02 MLENTITEH PIC X.                                          00001160
           02 MLENTITEO      PIC X(20).                                 00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MFONCTIONA     PIC X.                                     00001190
           02 MFONCTIONC     PIC X.                                     00001200
           02 MFONCTIONH     PIC X.                                     00001210
           02 MFONCTIONO     PIC X(15).                                 00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTITREA   PIC X.                                          00001240
           02 MTITREC   PIC X.                                          00001250
           02 MTITREH   PIC X.                                          00001260
           02 MTITREO   PIC X(33).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MMOISA    PIC X.                                          00001290
           02 MMOISC    PIC X.                                          00001300
           02 MMOISH    PIC X.                                          00001310
           02 MMOISO    PIC X(2).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MANNEEA   PIC X.                                          00001340
           02 MANNEEC   PIC X.                                          00001350
           02 MANNEEH   PIC X.                                          00001360
           02 MANNEEO   PIC X(4).                                       00001370
           02 TABLEAUO OCCURS   12 TIMES .                              00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MSELA   PIC X.                                          00001400
             03 MSELC   PIC X.                                          00001410
             03 MSELH   PIC X.                                          00001420
             03 MSELO   PIC X.                                          00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MNEXERA      PIC X.                                     00001450
             03 MNEXERC PIC X.                                          00001460
             03 MNEXERH PIC X.                                          00001470
             03 MNEXERO      PIC X(4).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNMOISDEBA   PIC X.                                     00001500
             03 MNMOISDEBC   PIC X.                                     00001510
             03 MNMOISDEBH   PIC X.                                     00001520
             03 MNMOISDEBO   PIC X(2).                                  00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MLMOISDEBA   PIC X.                                     00001550
             03 MLMOISDEBC   PIC X.                                     00001560
             03 MLMOISDEBH   PIC X.                                     00001570
             03 MLMOISDEBO   PIC X(10).                                 00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MNANCIVA     PIC X.                                     00001600
             03 MNANCIVC     PIC X.                                     00001610
             03 MNANCIVH     PIC X.                                     00001620
             03 MNANCIVO     PIC X(4).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBERRA  PIC X.                                          00001650
           02 MLIBERRC  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRO  PIC X(79).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCODTRAA  PIC X.                                          00001700
           02 MCODTRAC  PIC X.                                          00001710
           02 MCODTRAH  PIC X.                                          00001720
           02 MCODTRAO  PIC X(4).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCICSA    PIC X.                                          00001750
           02 MCICSC    PIC X.                                          00001760
           02 MCICSH    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MSCREENA  PIC X.                                          00001800
           02 MSCREENC  PIC X.                                          00001810
           02 MSCREENH  PIC X.                                          00001820
           02 MSCREENO  PIC X(4).                                       00001830
                                                                                
