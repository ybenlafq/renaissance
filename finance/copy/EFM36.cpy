      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - Combinaisons valides gr.                                  00000020
      ***************************************************************** 00000030
       01   EFM36I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MPAGETOTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCOMPTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPTEF  PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MCOMPTEI  PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTIONL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MSECTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTIONF      PIC X.                                     00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MSECTIONI      PIC X(6).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRIQUEL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MRUBRIQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MRUBRIQUEF     PIC X.                                     00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MRUBRIQUEI     PIC X(6).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTEAPPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSTEAPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTEAPPF  PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MSTEAPPI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MNETABI   PIC X(3).                                       00000410
           02 LIGNEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MSELI   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXMASQUECL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MXMASQUECL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MXMASQUECF   PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MXMASQUECI   PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMASQUECL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCMASQUECL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMASQUECF   PIC X.                                     00000520
             03 FILLER  PIC X(2).                                       00000530
             03 MCMASQUECI   PIC X(6).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXMASQUESL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MXMASQUESL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MXMASQUESF   PIC X.                                     00000560
             03 FILLER  PIC X(2).                                       00000570
             03 MXMASQUESI   PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMASQUESL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCMASQUESL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMASQUESF   PIC X.                                     00000600
             03 FILLER  PIC X(2).                                       00000610
             03 MCMASQUESI   PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXMASQUERL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MXMASQUERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MXMASQUERF   PIC X.                                     00000640
             03 FILLER  PIC X(2).                                       00000650
             03 MXMASQUERI   PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMASQUERL   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMASQUERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMASQUERF   PIC X.                                     00000680
             03 FILLER  PIC X(2).                                       00000690
             03 MCMASQUERI   PIC X(6).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMASQUEAL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCMASQUEAL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMASQUEAF   PIC X.                                     00000720
             03 FILLER  PIC X(2).                                       00000730
             03 MCMASQUEAI   PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMASQUEEL   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMASQUEEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMASQUEEF   PIC X.                                     00000760
             03 FILLER  PIC X(2).                                       00000770
             03 MCMASQUEEI   PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCLOTUREL   COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDCLOTUREL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDCLOTUREF   PIC X.                                     00000800
             03 FILLER  PIC X(2).                                       00000810
             03 MDCLOTUREI   PIC X(10).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MLIBERRI  PIC X(79).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * GCT - Combinaisons valides gr.                                  00001000
      ***************************************************************** 00001010
       01   EFM36O REDEFINES EFM36I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUO  PIC X(10).                                      00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MTIMJOUA  PIC X.                                          00001100
           02 MTIMJOUC  PIC X.                                          00001110
           02 MTIMJOUH  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEH    PIC X.                                          00001170
           02 MPAGEO    PIC ZZ9.                                        00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGETOTA      PIC X.                                     00001200
           02 MPAGETOTC PIC X.                                          00001210
           02 MPAGETOTH PIC X.                                          00001220
           02 MPAGETOTO      PIC ZZ9.                                   00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCOMPTEA  PIC X.                                          00001250
           02 MCOMPTEC  PIC X.                                          00001260
           02 MCOMPTEH  PIC X.                                          00001270
           02 MCOMPTEO  PIC X(6).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MSECTIONA      PIC X.                                     00001300
           02 MSECTIONC PIC X.                                          00001310
           02 MSECTIONH PIC X.                                          00001320
           02 MSECTIONO      PIC X(6).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MRUBRIQUEA     PIC X.                                     00001350
           02 MRUBRIQUEC     PIC X.                                     00001360
           02 MRUBRIQUEH     PIC X.                                     00001370
           02 MRUBRIQUEO     PIC X(6).                                  00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MSTEAPPA  PIC X.                                          00001400
           02 MSTEAPPC  PIC X.                                          00001410
           02 MSTEAPPH  PIC X.                                          00001420
           02 MSTEAPPO  PIC X(5).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNETABA   PIC X.                                          00001450
           02 MNETABC   PIC X.                                          00001460
           02 MNETABH   PIC X.                                          00001470
           02 MNETABO   PIC X(3).                                       00001480
           02 LIGNEO OCCURS   12 TIMES .                                00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MSELA   PIC X.                                          00001510
             03 MSELC   PIC X.                                          00001520
             03 MSELH   PIC X.                                          00001530
             03 MSELO   PIC X.                                          00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MXMASQUECA   PIC X.                                     00001560
             03 MXMASQUECC   PIC X.                                     00001570
             03 MXMASQUECH   PIC X.                                     00001580
             03 MXMASQUECO   PIC X.                                     00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MCMASQUECA   PIC X.                                     00001610
             03 MCMASQUECC   PIC X.                                     00001620
             03 MCMASQUECH   PIC X.                                     00001630
             03 MCMASQUECO   PIC X(6).                                  00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MXMASQUESA   PIC X.                                     00001660
             03 MXMASQUESC   PIC X.                                     00001670
             03 MXMASQUESH   PIC X.                                     00001680
             03 MXMASQUESO   PIC X.                                     00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MCMASQUESA   PIC X.                                     00001710
             03 MCMASQUESC   PIC X.                                     00001720
             03 MCMASQUESH   PIC X.                                     00001730
             03 MCMASQUESO   PIC X(6).                                  00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MXMASQUERA   PIC X.                                     00001760
             03 MXMASQUERC   PIC X.                                     00001770
             03 MXMASQUERH   PIC X.                                     00001780
             03 MXMASQUERO   PIC X.                                     00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MCMASQUERA   PIC X.                                     00001810
             03 MCMASQUERC   PIC X.                                     00001820
             03 MCMASQUERH   PIC X.                                     00001830
             03 MCMASQUERO   PIC X(6).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MCMASQUEAA   PIC X.                                     00001860
             03 MCMASQUEAC   PIC X.                                     00001870
             03 MCMASQUEAH   PIC X.                                     00001880
             03 MCMASQUEAO   PIC X(5).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MCMASQUEEA   PIC X.                                     00001910
             03 MCMASQUEEC   PIC X.                                     00001920
             03 MCMASQUEEH   PIC X.                                     00001930
             03 MCMASQUEEO   PIC X(3).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MDCLOTUREA   PIC X.                                     00001960
             03 MDCLOTUREC   PIC X.                                     00001970
             03 MDCLOTUREH   PIC X.                                     00001980
             03 MDCLOTUREO   PIC X(10).                                 00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLIBERRA  PIC X.                                          00002010
           02 MLIBERRC  PIC X.                                          00002020
           02 MLIBERRH  PIC X.                                          00002030
           02 MLIBERRO  PIC X(79).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAH  PIC X.                                          00002080
           02 MCODTRAO  PIC X(4).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSH    PIC X.                                          00002130
           02 MCICSO    PIC X(5).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENH  PIC X.                                          00002180
           02 MSCREENO  PIC X(4).                                       00002190
                                                                                
