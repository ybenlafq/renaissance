      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT0100.                                                            
      *}                                                                        
           02  FT01-NSOC                                                        
               PIC X(0005).                                                     
           02  FT01-NAUX                                                        
               PIC X(0003).                                                     
           02  FT01-WTYPAUX                                                     
               PIC X(0001).                                                     
           02  FT01-WSAISIE                                                     
               PIC X(0001).                                                     
           02  FT01-WREGLT                                                      
               PIC X(0001).                                                     
           02  FT01-WBAP                                                        
               PIC X(0001).                                                     
           02  FT01-WAUXNCG                                                     
               PIC X(0001).                                                     
           02  FT01-CEPUR                                                       
               PIC X(0001).                                                     
           02  FT01-WLETTRAGE                                                   
               PIC X(0001).                                                     
           02  FT01-CBANQUE                                                     
               PIC X(0005).                                                     
           02  FT01-WRELANCE                                                    
               PIC X(0001).                                                     
           02  FT01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WTYPAUX-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WTYPAUX-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WREGLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WREGLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WBAP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WBAP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WAUXNCG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WAUXNCG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-CEPUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-CEPUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-CBANQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-CBANQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-WRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-WRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
