      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGC030 AU 23/09/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,01,BI,A,                          *        
      *                           33,07,BI,A,                          *        
      *                           40,03,BI,A,                          *        
      *                           43,05,BI,A,                          *        
      *                           48,05,BI,A,                          *        
      *                           53,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGC030.                                                        
            05 NOMETAT-IGC030           PIC X(6) VALUE 'IGC030'.                
            05 RUPTURES-IGC030.                                                 
           10 IGC030-CMARQSE            PIC X(05).                      007  005
           10 IGC030-NSEQTPREST         PIC X(03).                      012  003
           10 IGC030-CTYPPREST          PIC X(05).                      015  005
           10 IGC030-CTYPE              PIC X(02).                      020  002
           10 IGC030-CFAMCO             PIC X(05).                      022  005
           10 IGC030-CMARQCO            PIC X(05).                      027  005
           10 IGC030-W-TRI-ENTITE       PIC X(01).                      032  001
           10 IGC030-NENTITE            PIC X(07).                      033  007
           10 IGC030-GCCND-NSEQ         PIC X(03).                      040  003
           10 IGC030-CTYPCND            PIC X(05).                      043  005
           10 IGC030-NLIGNE             PIC X(05).                      048  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGC030-SEQUENCE           PIC S9(04) COMP.                053  002
      *--                                                                       
           10 IGC030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGC030.                                                   
           10 IGC030-CACHE-L10-T10      PIC X(01).                      055  001
           10 IGC030-CRITERES           PIC X(11).                      056  011
           10 IGC030-LCOMMENT           PIC X(30).                      067  030
           10 IGC030-LENTITE            PIC X(20).                      097  020
           10 IGC030-LFAMCO             PIC X(20).                      117  020
           10 IGC030-LIBELLE            PIC X(20).                      137  020
           10 IGC030-LMARQCO            PIC X(20).                      157  020
           10 IGC030-LMARQSE            PIC X(20).                      177  020
           10 IGC030-LTYPE              PIC X(20).                      197  020
           10 IGC030-LTYPPREST          PIC X(20).                      217  020
           10 IGC030-MONTANT            PIC S9(08)V9(2) COMP-3.         237  006
           10 IGC030-MONTANT2           PIC S9(08)V9(2) COMP-3.         243  006
           10 IGC030-DEFFET             PIC X(08).                      249  008
           10 IGC030-DFINEFFET          PIC X(08).                      257  008
           10 IGC030-DJOUR              PIC X(08).                      265  008
           10 IGC030-DMAJ               PIC X(08).                      273  008
            05 FILLER                      PIC X(232).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGC030-LONG           PIC S9(4)   COMP  VALUE +280.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGC030-LONG           PIC S9(4) COMP-5  VALUE +280.           
                                                                                
      *}                                                                        
