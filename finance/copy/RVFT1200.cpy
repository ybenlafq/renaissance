      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT1200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1200.                                                            
      *}                                                                        
           02  FT12-NSOC                                                        
               PIC X(0005).                                                     
           02  FT12-NETAB                                                       
               PIC X(0003).                                                     
           02  FT12-NJRN                                                        
               PIC X(0003).                                                     
           02  FT12-NEXER                                                       
               PIC X(0004).                                                     
           02  FT12-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT12-NPER                                                        
               PIC X(0002).                                                     
           02  FT12-TYPECR                                                      
               PIC X(0003).                                                     
           02  FT12-TYPJRN                                                      
               PIC X(0002).                                                     
           02  FT12-ORIGINE                                                     
               PIC X(0005).                                                     
           02  FT12-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT12-NAUX                                                        
               PIC X(0003).                                                     
           02  FT12-NATURE                                                      
               PIC X(0003).                                                     
           02  FT12-DDOC                                                        
               PIC X(0008).                                                     
           02  FT12-REFDOC                                                      
               PIC X(0015).                                                     
           02  FT12-CDAS2                                                       
               PIC X(0002).                                                     
           02  FT12-MTDAS2                                                      
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT12-DDECLAR                                                     
               PIC X(0008).                                                     
           02  FT12-DCPT                                                        
               PIC X(0008).                                                     
           02  FT12-MONTANT                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT12-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FT12-MTDEVBASE                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT12-MTREGLE                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT12-CREGTVA                                                     
               PIC X(0001).                                                     
           02  FT12-WCONTREP                                                    
               PIC X(0001).                                                     
           02  FT12-CACID                                                       
               PIC X(0008).                                                     
           02  FT12-DSAISIE                                                     
               PIC X(0008).                                                     
           02  FT12-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FT12-DECHRGAR                                                    
               PIC X(0008).                                                     
           02  FT12-NLETTRAGE                                                   
               PIC X(0010).                                                     
           02  FT12-DARC                                                        
               PIC X(0008).                                                     
           02  FT12-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT1200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT1200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT1200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NPER-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NPER-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-TYPECR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-TYPECR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-TYPJRN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-TYPJRN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DDOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DDOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-REFDOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-REFDOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-CDAS2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-CDAS2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-MTDAS2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-MTDAS2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DDECLAR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DDECLAR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DCPT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DCPT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-MTDEVBASE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-MTDEVBASE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-MTREGLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-MTREGLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-CREGTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-CREGTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-WCONTREP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-WCONTREP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DECHRGAR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DECHRGAR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-NLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DARC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DARC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT12-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT12-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
