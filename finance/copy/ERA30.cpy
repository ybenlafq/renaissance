      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA30   ERA30                                              00000020
      ***************************************************************** 00000030
       01   ERA30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *                                                                 00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNSOCI    PIC X(3).                                       00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000210
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MNLIEUI   PIC X(3).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBANQL   COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MCBANQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBANQF   PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MCBANQI   PIC X(5).                                       00000280
      * OPTION                                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MZONCMDI  PIC X(2).                                       00000330
      *                                                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGLL      COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MTYPREGLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREGLF      PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MTYPREGLI      PIC X.                                     00000380
      * cheque premier                                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHQ1L    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCHQ1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCHQ1F    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCHQ1I    PIC X(7).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHQ2L    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCHQ2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCHQ2F    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCHQ2I    PIC X(7).                                       00000470
      * avoir premier                                                   00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAVOIR1L  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MAVOIR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAVOIR1F  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MAVOIR1I  PIC X(7).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAVOIR2L  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MAVOIR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAVOIR2F  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MAVOIR2I  PIC X(7).                                       00000560
      * especes premier                                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESP1L    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MESP1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MESP1F    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MESP1I    PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESP2L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MESP2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MESP2F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MESP2I    PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDECHEANI      PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDETAILL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDETAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDETAILF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDETAILI  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRILIEUL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MTRILIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTRILIEUF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MTRILIEUI      PIC X.                                     00000770
      * code imprimante                                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIMPL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODIMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIMPF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODIMPI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDREGLTL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MDREGLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDREGLTF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MDREGLTI  PIC X(10).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATESL   COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MDATESL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATESF   PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MDATESI   PIC X(10).                                      00000900
      * MESSAGE ERREUR                                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLIBERRI  PIC X(78).                                      00000950
      * CODE TRANSACTION                                                00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCODTRAI  PIC X(4).                                       00001000
      * CICS DE TRAVAIL                                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCICSI    PIC X(5).                                       00001050
      * NETNAME                                                         00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      * CODE TERMINAL                                                   00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MSCREENI  PIC X(5).                                       00001150
      ***************************************************************** 00001160
      * SDF: ERA30   ERA30                                              00001170
      ***************************************************************** 00001180
       01   ERA30O REDEFINES ERA30I.                                    00001190
           02 FILLER    PIC X(12).                                      00001200
      * DATE DU JOUR                                                    00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MDATJOUA  PIC X.                                          00001230
           02 MDATJOUC  PIC X.                                          00001240
           02 MDATJOUP  PIC X.                                          00001250
           02 MDATJOUH  PIC X.                                          00001260
           02 MDATJOUV  PIC X.                                          00001270
           02 MDATJOUO  PIC X(10).                                      00001280
      * HEURE                                                           00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
      *                                                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNSOCA    PIC X.                                          00001390
           02 MNSOCC    PIC X.                                          00001400
           02 MNSOCP    PIC X.                                          00001410
           02 MNSOCH    PIC X.                                          00001420
           02 MNSOCV    PIC X.                                          00001430
           02 MNSOCO    PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNLIEUA   PIC X.                                          00001460
           02 MNLIEUC   PIC X.                                          00001470
           02 MNLIEUP   PIC X.                                          00001480
           02 MNLIEUH   PIC X.                                          00001490
           02 MNLIEUV   PIC X.                                          00001500
           02 MNLIEUO   PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCBANQA   PIC X.                                          00001530
           02 MCBANQC   PIC X.                                          00001540
           02 MCBANQP   PIC X.                                          00001550
           02 MCBANQH   PIC X.                                          00001560
           02 MCBANQV   PIC X.                                          00001570
           02 MCBANQO   PIC X(5).                                       00001580
      * OPTION                                                          00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MZONCMDA  PIC X.                                          00001610
           02 MZONCMDC  PIC X.                                          00001620
           02 MZONCMDP  PIC X.                                          00001630
           02 MZONCMDH  PIC X.                                          00001640
           02 MZONCMDV  PIC X.                                          00001650
           02 MZONCMDO  PIC X(2).                                       00001660
      *                                                                 00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MTYPREGLA      PIC X.                                     00001690
           02 MTYPREGLC PIC X.                                          00001700
           02 MTYPREGLP PIC X.                                          00001710
           02 MTYPREGLH PIC X.                                          00001720
           02 MTYPREGLV PIC X.                                          00001730
           02 MTYPREGLO      PIC X.                                     00001740
      * cheque premier                                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCHQ1A    PIC X.                                          00001770
           02 MCHQ1C    PIC X.                                          00001780
           02 MCHQ1P    PIC X.                                          00001790
           02 MCHQ1H    PIC X.                                          00001800
           02 MCHQ1V    PIC X.                                          00001810
           02 MCHQ1O    PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCHQ2A    PIC X.                                          00001840
           02 MCHQ2C    PIC X.                                          00001850
           02 MCHQ2P    PIC X.                                          00001860
           02 MCHQ2H    PIC X.                                          00001870
           02 MCHQ2V    PIC X.                                          00001880
           02 MCHQ2O    PIC X(7).                                       00001890
      * avoir premier                                                   00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MAVOIR1A  PIC X.                                          00001920
           02 MAVOIR1C  PIC X.                                          00001930
           02 MAVOIR1P  PIC X.                                          00001940
           02 MAVOIR1H  PIC X.                                          00001950
           02 MAVOIR1V  PIC X.                                          00001960
           02 MAVOIR1O  PIC X(7).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MAVOIR2A  PIC X.                                          00001990
           02 MAVOIR2C  PIC X.                                          00002000
           02 MAVOIR2P  PIC X.                                          00002010
           02 MAVOIR2H  PIC X.                                          00002020
           02 MAVOIR2V  PIC X.                                          00002030
           02 MAVOIR2O  PIC X(7).                                       00002040
      * especes premier                                                 00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MESP1A    PIC X.                                          00002070
           02 MESP1C    PIC X.                                          00002080
           02 MESP1P    PIC X.                                          00002090
           02 MESP1H    PIC X.                                          00002100
           02 MESP1V    PIC X.                                          00002110
           02 MESP1O    PIC X(7).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MESP2A    PIC X.                                          00002140
           02 MESP2C    PIC X.                                          00002150
           02 MESP2P    PIC X.                                          00002160
           02 MESP2H    PIC X.                                          00002170
           02 MESP2V    PIC X.                                          00002180
           02 MESP2O    PIC X(7).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MDECHEANA      PIC X.                                     00002210
           02 MDECHEANC PIC X.                                          00002220
           02 MDECHEANP PIC X.                                          00002230
           02 MDECHEANH PIC X.                                          00002240
           02 MDECHEANV PIC X.                                          00002250
           02 MDECHEANO      PIC X(10).                                 00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDETAILA  PIC X.                                          00002280
           02 MDETAILC  PIC X.                                          00002290
           02 MDETAILP  PIC X.                                          00002300
           02 MDETAILH  PIC X.                                          00002310
           02 MDETAILV  PIC X.                                          00002320
           02 MDETAILO  PIC X.                                          00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTRILIEUA      PIC X.                                     00002350
           02 MTRILIEUC PIC X.                                          00002360
           02 MTRILIEUP PIC X.                                          00002370
           02 MTRILIEUH PIC X.                                          00002380
           02 MTRILIEUV PIC X.                                          00002390
           02 MTRILIEUO      PIC X.                                     00002400
      * code imprimante                                                 00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCODIMPA  PIC X.                                          00002430
           02 MCODIMPC  PIC X.                                          00002440
           02 MCODIMPP  PIC X.                                          00002450
           02 MCODIMPH  PIC X.                                          00002460
           02 MCODIMPV  PIC X.                                          00002470
           02 MCODIMPO  PIC X(4).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MDREGLTA  PIC X.                                          00002500
           02 MDREGLTC  PIC X.                                          00002510
           02 MDREGLTP  PIC X.                                          00002520
           02 MDREGLTH  PIC X.                                          00002530
           02 MDREGLTV  PIC X.                                          00002540
           02 MDREGLTO  PIC X(10).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MDATESA   PIC X.                                          00002570
           02 MDATESC   PIC X.                                          00002580
           02 MDATESP   PIC X.                                          00002590
           02 MDATESH   PIC X.                                          00002600
           02 MDATESV   PIC X.                                          00002610
           02 MDATESO   PIC X(10).                                      00002620
      * MESSAGE ERREUR                                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(78).                                      00002700
      * CODE TRANSACTION                                                00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCODTRAA  PIC X.                                          00002730
           02 MCODTRAC  PIC X.                                          00002740
           02 MCODTRAP  PIC X.                                          00002750
           02 MCODTRAH  PIC X.                                          00002760
           02 MCODTRAV  PIC X.                                          00002770
           02 MCODTRAO  PIC X(4).                                       00002780
      * CICS DE TRAVAIL                                                 00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MCICSA    PIC X.                                          00002810
           02 MCICSC    PIC X.                                          00002820
           02 MCICSP    PIC X.                                          00002830
           02 MCICSH    PIC X.                                          00002840
           02 MCICSV    PIC X.                                          00002850
           02 MCICSO    PIC X(5).                                       00002860
      * NETNAME                                                         00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MNETNAMA  PIC X.                                          00002890
           02 MNETNAMC  PIC X.                                          00002900
           02 MNETNAMP  PIC X.                                          00002910
           02 MNETNAMH  PIC X.                                          00002920
           02 MNETNAMV  PIC X.                                          00002930
           02 MNETNAMO  PIC X(8).                                       00002940
      * CODE TERMINAL                                                   00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MSCREENA  PIC X.                                          00002970
           02 MSCREENC  PIC X.                                          00002980
           02 MSCREENP  PIC X.                                          00002990
           02 MSCREENH  PIC X.                                          00003000
           02 MSCREENV  PIC X.                                          00003010
           02 MSCREENO  PIC X(5).                                       00003020
                                                                                
