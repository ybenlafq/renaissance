      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM14-LONG             PIC S9(4) COMP-3 VALUE +97.                 
       01  TS-FM14-RECORD.                                                      
           10 TS-FM14-NDEMANDE      PIC X(2).                                   
           10 TS-FM14-DCREATION     PIC X(10).                                  
           10 TS-FM14-CACID         PIC X(8).                                   
           10 TS-FM14-USER          PIC X(25).                                  
           10 TS-FM14-WMFICHE       PIC X.                                      
           10 TS-FM14-WPAPIER       PIC X.                                      
           10 TS-FM14-NETAB         PIC X(3).                                   
AM         10 TS-FM14-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM14-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM14-WTYPAUX       PIC X.                                      
           10 TS-FM14-NTIERSMIN     PIC X(8).                                   
           10 TS-FM14-NTIERSMAX     PIC X(8).                                   
           10 TS-FM14-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM14-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM14-WTOTAL        PIC X OCCURS 4.                             
           10 TS-FM14-WNTRI         PIC X OCCURS 4.                             
           10 TS-FM14-WTTRITIERS    PIC X.                                      
           10 TS-FM14-CDEVISE       PIC X(3).                                   
                                                                                
