      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT00   EXT00                                              00000020
      ***************************************************************** 00000030
       01   EXT00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MZONCMDI  PIC X(15).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXTRL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MEXTRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEXTRF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MEXTRI    PIC X(5).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONSLL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCONSLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCONSLF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCONSLI   PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAGRCLL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MAGRCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MAGRCLF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MAGRCLI   PIC X(5).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAGRPRL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MAGRPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MAGRPRF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MAGRPRI   PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCETATI   PIC X(10).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSEQPROL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MWSEQPROL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWSEQPROF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MWSEQPROI      PIC X(7).                                  00000430
      * MESSAGE ERREUR                                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLIBERRI  PIC X(78).                                      00000480
      * CODE TRANSACTION                                                00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODTRAI  PIC X(4).                                       00000530
      * CICS DE TRAVAIL                                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      * NETNAME                                                         00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNETNAMI  PIC X(8).                                       00000630
      * CODE TERMINAL                                                   00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MSCREENI  PIC X(5).                                       00000680
      ***************************************************************** 00000690
      * SDF: EXT00   EXT00                                              00000700
      ***************************************************************** 00000710
       01   EXT00O REDEFINES EXT00I.                                    00000720
           02 FILLER    PIC X(12).                                      00000730
      * DATE DU JOUR                                                    00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
      * HEURE                                                           00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MZONCMDA  PIC X.                                          00000910
           02 MZONCMDC  PIC X.                                          00000920
           02 MZONCMDP  PIC X.                                          00000930
           02 MZONCMDH  PIC X.                                          00000940
           02 MZONCMDV  PIC X.                                          00000950
           02 MZONCMDO  PIC X(15).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MEXTRA    PIC X.                                          00000980
           02 MEXTRC    PIC X.                                          00000990
           02 MEXTRP    PIC X.                                          00001000
           02 MEXTRH    PIC X.                                          00001010
           02 MEXTRV    PIC X.                                          00001020
           02 MEXTRO    PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCONSLA   PIC X.                                          00001050
           02 MCONSLC   PIC X.                                          00001060
           02 MCONSLP   PIC X.                                          00001070
           02 MCONSLH   PIC X.                                          00001080
           02 MCONSLV   PIC X.                                          00001090
           02 MCONSLO   PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MAGRCLA   PIC X.                                          00001120
           02 MAGRCLC   PIC X.                                          00001130
           02 MAGRCLP   PIC X.                                          00001140
           02 MAGRCLH   PIC X.                                          00001150
           02 MAGRCLV   PIC X.                                          00001160
           02 MAGRCLO   PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MAGRPRA   PIC X.                                          00001190
           02 MAGRPRC   PIC X.                                          00001200
           02 MAGRPRP   PIC X.                                          00001210
           02 MAGRPRH   PIC X.                                          00001220
           02 MAGRPRV   PIC X.                                          00001230
           02 MAGRPRO   PIC X(5).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCETATA   PIC X.                                          00001260
           02 MCETATC   PIC X.                                          00001270
           02 MCETATP   PIC X.                                          00001280
           02 MCETATH   PIC X.                                          00001290
           02 MCETATV   PIC X.                                          00001300
           02 MCETATO   PIC X(10).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MWSEQPROA      PIC X.                                     00001330
           02 MWSEQPROC PIC X.                                          00001340
           02 MWSEQPROP PIC X.                                          00001350
           02 MWSEQPROH PIC X.                                          00001360
           02 MWSEQPROV PIC X.                                          00001370
           02 MWSEQPROO      PIC X(7).                                  00001380
      * MESSAGE ERREUR                                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLIBERRA  PIC X.                                          00001410
           02 MLIBERRC  PIC X.                                          00001420
           02 MLIBERRP  PIC X.                                          00001430
           02 MLIBERRH  PIC X.                                          00001440
           02 MLIBERRV  PIC X.                                          00001450
           02 MLIBERRO  PIC X(78).                                      00001460
      * CODE TRANSACTION                                                00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCODTRAA  PIC X.                                          00001490
           02 MCODTRAC  PIC X.                                          00001500
           02 MCODTRAP  PIC X.                                          00001510
           02 MCODTRAH  PIC X.                                          00001520
           02 MCODTRAV  PIC X.                                          00001530
           02 MCODTRAO  PIC X(4).                                       00001540
      * CICS DE TRAVAIL                                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCICSA    PIC X.                                          00001570
           02 MCICSC    PIC X.                                          00001580
           02 MCICSP    PIC X.                                          00001590
           02 MCICSH    PIC X.                                          00001600
           02 MCICSV    PIC X.                                          00001610
           02 MCICSO    PIC X(5).                                       00001620
      * NETNAME                                                         00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNETNAMA  PIC X.                                          00001650
           02 MNETNAMC  PIC X.                                          00001660
           02 MNETNAMP  PIC X.                                          00001670
           02 MNETNAMH  PIC X.                                          00001680
           02 MNETNAMV  PIC X.                                          00001690
           02 MNETNAMO  PIC X(8).                                       00001700
      * CODE TERMINAL                                                   00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MSCREENA  PIC X.                                          00001730
           02 MSCREENC  PIC X.                                          00001740
           02 MSCREENP  PIC X.                                          00001750
           02 MSCREENH  PIC X.                                          00001760
           02 MSCREENV  PIC X.                                          00001770
           02 MSCREENO  PIC X(5).                                       00001780
                                                                                
