      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM11-LONG             PIC S9(4) COMP-3 VALUE +113.                
       01  TS-FM11-RECORD.                                                      
           10 TS-FM11-NDEMANDE      PIC X(2).                                   
           10 TS-FM11-DCREATION     PIC X(10).                                  
           10 TS-FM11-CACID         PIC X(8).                                   
           10 TS-FM11-USER          PIC X(25).                                  
           10 TS-FM11-WMFICHE       PIC X.                                      
           10 TS-FM11-WPAPIER       PIC X.                                      
           10 TS-FM11-NETAB         PIC X(3).                                   
AM         10 TS-FM11-NAUXMIN       PIC X(3).                                   
AM         10 TS-FM11-NAUXMAX       PIC X(3).                                   
AM         10 TS-FM11-WTYPAUX       PIC X.                                      
           10 TS-FM11-NTIERSMIN     PIC X(8).                                   
           10 TS-FM11-NTIERSMAX     PIC X(8).                                   
           10 TS-FM11-NCOMPTEMIN    PIC X(6).                                   
           10 TS-FM11-NCOMPTEMAX    PIC X(6).                                   
           10 TS-FM11-NCPTEAUXMIN   PIC X(6).                                   
           10 TS-FM11-NCPTEAUXMAX   PIC X(6).                                   
           10 TS-FM11-WTOTAL        PIC X OCCURS 5.                             
           10 TS-FM11-WNTRI         PIC X OCCURS 5.                             
           10 TS-FM11-WTTRITIERS    PIC X.                                      
           10 TS-FM11-WEDTTIERS     PIC X.                                      
           10 TS-FM11-WTSELTIERS    PIC X.                                      
           10 TS-FM11-CDEVISE       PIC X(3).                                   
                                                                                
