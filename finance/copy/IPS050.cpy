      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPS050 AU 19/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPS050.                                                        
            05 NOMETAT-IPS050           PIC X(6) VALUE 'IPS050'.                
            05 RUPTURES-IPS050.                                                 
           10 IPS050-CGRP               PIC X(05).                      007  005
           10 IPS050-NOSAV              PIC X(05).                      012  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPS050-SEQUENCE           PIC S9(04) COMP.                017  002
      *--                                                                       
           10 IPS050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPS050.                                                   
           10 IPS050-LADR               PIC X(24).                      019  024
           10 IPS050-LBGRP              PIC X(20).                      043  020
           10 IPS050-MOIS               PIC X(05).                      063  005
           10 IPS050-NSOC               PIC X(03).                      068  003
           10 IPS050-MTDJFC             PIC S9(07)V9(6) COMP-3.         071  007
           10 IPS050-MTFACM             PIC S9(07)V9(6) COMP-3.         078  007
           10 IPS050-MTHT               PIC S9(07)V9(6) COMP-3.         085  007
           10 IPS050-MTHTR              PIC S9(06)V9(6) COMP-3.         092  007
           10 IPS050-MTRAFC             PIC S9(07)V9(6) COMP-3.         099  007
           10 IPS050-MTTC               PIC S9(07)V9(6) COMP-3.         106  007
           10 IPS050-MTTCR              PIC S9(06)V9(6) COMP-3.         113  007
           10 IPS050-MT15PHT            PIC S9(07)V9(6) COMP-3.         120  007
           10 IPS050-NBPSE              PIC S9(05)      COMP-3.         127  003
           10 IPS050-NBPSER             PIC S9(05)      COMP-3.         130  003
           10 IPS050-DEFFET             PIC X(08).                      133  008
            05 FILLER                      PIC X(372).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPS050-LONG           PIC S9(4)   COMP  VALUE +140.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPS050-LONG           PIC S9(4) COMP-5  VALUE +140.           
                                                                                
      *}                                                                        
