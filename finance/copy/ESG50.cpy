      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESG00   ESG00                                              00000020
      ***************************************************************** 00000030
       01   ESG50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X.                                          00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MRUBRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUBRF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MRUBRI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTYPEI   PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIGNEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIGNEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIGNEI   PIC X(34).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLECHEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MFLECHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFLECHEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFLECHEI  PIC X(3).                                       00000330
           02 MLIGNE1D OCCURS   5 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE1L     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLIGNE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE1F     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLIGNE1I     PIC X(3).                                  00000380
           02 MLIGNE2D OCCURS   5 TIMES .                               00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE2L     COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MLIGNE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE2F     PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MLIGNE2I     PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGRPL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCGRPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCGRPF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCGRPI    PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTIONL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MSECTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTIONF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MSECTIONI      PIC X(6).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRI2L    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MTRI2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTRI2F    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MTRI2I    PIC X.                                          00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONTRATL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MCONTRATL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCONTRATF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCONTRATI      PIC X(2).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCIMPI    PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUB1L    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MRUB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUB1F    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MRUB1I    PIC X(3).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUB2L    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MRUB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRUB2F    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MRUB2I    PIC X(3).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLIBERRI  PIC X(78).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCODTRAI  PIC X(4).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCICSI    PIC X(5).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNETNAMI  PIC X(8).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MSCREENI  PIC X(4).                                       00000910
      ***************************************************************** 00000920
      * SDF: ESG00   ESG00                                              00000930
      ***************************************************************** 00000940
       01   ESG50O REDEFINES ESG50I.                                    00000950
           02 FILLER    PIC X(12).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MDATJOUA  PIC X.                                          00000980
           02 MDATJOUC  PIC X.                                          00000990
           02 MDATJOUP  PIC X.                                          00001000
           02 MDATJOUH  PIC X.                                          00001010
           02 MDATJOUV  PIC X.                                          00001020
           02 MDATJOUO  PIC X(10).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MTIMJOUA  PIC X.                                          00001050
           02 MTIMJOUC  PIC X.                                          00001060
           02 MTIMJOUP  PIC X.                                          00001070
           02 MTIMJOUH  PIC X.                                          00001080
           02 MTIMJOUV  PIC X.                                          00001090
           02 MTIMJOUO  PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MZONCMDA  PIC X.                                          00001120
           02 MZONCMDC  PIC X.                                          00001130
           02 MZONCMDP  PIC X.                                          00001140
           02 MZONCMDH  PIC X.                                          00001150
           02 MZONCMDV  PIC X.                                          00001160
           02 MZONCMDO  PIC X.                                          00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MRUBRA    PIC X.                                          00001190
           02 MRUBRC    PIC X.                                          00001200
           02 MRUBRP    PIC X.                                          00001210
           02 MRUBRH    PIC X.                                          00001220
           02 MRUBRV    PIC X.                                          00001230
           02 MRUBRO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCTYPEA   PIC X.                                          00001260
           02 MCTYPEC   PIC X.                                          00001270
           02 MCTYPEP   PIC X.                                          00001280
           02 MCTYPEH   PIC X.                                          00001290
           02 MCTYPEV   PIC X.                                          00001300
           02 MCTYPEO   PIC X(2).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIGNEA   PIC X.                                          00001330
           02 MLIGNEC   PIC X.                                          00001340
           02 MLIGNEP   PIC X.                                          00001350
           02 MLIGNEH   PIC X.                                          00001360
           02 MLIGNEV   PIC X.                                          00001370
           02 MLIGNEO   PIC X(34).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MFLECHEA  PIC X.                                          00001400
           02 MFLECHEC  PIC X.                                          00001410
           02 MFLECHEP  PIC X.                                          00001420
           02 MFLECHEH  PIC X.                                          00001430
           02 MFLECHEV  PIC X.                                          00001440
           02 MFLECHEO  PIC X(3).                                       00001450
           02 DFHMS1 OCCURS   5 TIMES .                                 00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MLIGNE1A     PIC X.                                     00001480
             03 MLIGNE1C     PIC X.                                     00001490
             03 MLIGNE1P     PIC X.                                     00001500
             03 MLIGNE1H     PIC X.                                     00001510
             03 MLIGNE1V     PIC X.                                     00001520
             03 MLIGNE1O     PIC X(3).                                  00001530
           02 DFHMS2 OCCURS   5 TIMES .                                 00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLIGNE2A     PIC X.                                     00001560
             03 MLIGNE2C     PIC X.                                     00001570
             03 MLIGNE2P     PIC X.                                     00001580
             03 MLIGNE2H     PIC X.                                     00001590
             03 MLIGNE2V     PIC X.                                     00001600
             03 MLIGNE2O     PIC X(3).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCGRPA    PIC X.                                          00001630
           02 MCGRPC    PIC X.                                          00001640
           02 MCGRPP    PIC X.                                          00001650
           02 MCGRPH    PIC X.                                          00001660
           02 MCGRPV    PIC X.                                          00001670
           02 MCGRPO    PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSECTIONA      PIC X.                                     00001700
           02 MSECTIONC PIC X.                                          00001710
           02 MSECTIONP PIC X.                                          00001720
           02 MSECTIONH PIC X.                                          00001730
           02 MSECTIONV PIC X.                                          00001740
           02 MSECTIONO      PIC X(6).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MTRI2A    PIC X.                                          00001770
           02 MTRI2C    PIC X.                                          00001780
           02 MTRI2P    PIC X.                                          00001790
           02 MTRI2H    PIC X.                                          00001800
           02 MTRI2V    PIC X.                                          00001810
           02 MTRI2O    PIC X.                                          00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCONTRATA      PIC X.                                     00001840
           02 MCONTRATC PIC X.                                          00001850
           02 MCONTRATP PIC X.                                          00001860
           02 MCONTRATH PIC X.                                          00001870
           02 MCONTRATV PIC X.                                          00001880
           02 MCONTRATO      PIC X(2).                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCIMPA    PIC X.                                          00001910
           02 MCIMPC    PIC X.                                          00001920
           02 MCIMPP    PIC X.                                          00001930
           02 MCIMPH    PIC X.                                          00001940
           02 MCIMPV    PIC X.                                          00001950
           02 MCIMPO    PIC X(4).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MRUB1A    PIC X.                                          00001980
           02 MRUB1C    PIC X.                                          00001990
           02 MRUB1P    PIC X.                                          00002000
           02 MRUB1H    PIC X.                                          00002010
           02 MRUB1V    PIC X.                                          00002020
           02 MRUB1O    PIC X(3).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MRUB2A    PIC X.                                          00002050
           02 MRUB2C    PIC X.                                          00002060
           02 MRUB2P    PIC X.                                          00002070
           02 MRUB2H    PIC X.                                          00002080
           02 MRUB2V    PIC X.                                          00002090
           02 MRUB2O    PIC X(3).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MLIBERRA  PIC X.                                          00002120
           02 MLIBERRC  PIC X.                                          00002130
           02 MLIBERRP  PIC X.                                          00002140
           02 MLIBERRH  PIC X.                                          00002150
           02 MLIBERRV  PIC X.                                          00002160
           02 MLIBERRO  PIC X(78).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCODTRAA  PIC X.                                          00002190
           02 MCODTRAC  PIC X.                                          00002200
           02 MCODTRAP  PIC X.                                          00002210
           02 MCODTRAH  PIC X.                                          00002220
           02 MCODTRAV  PIC X.                                          00002230
           02 MCODTRAO  PIC X(4).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCICSA    PIC X.                                          00002260
           02 MCICSC    PIC X.                                          00002270
           02 MCICSP    PIC X.                                          00002280
           02 MCICSH    PIC X.                                          00002290
           02 MCICSV    PIC X.                                          00002300
           02 MCICSO    PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNETNAMA  PIC X.                                          00002330
           02 MNETNAMC  PIC X.                                          00002340
           02 MNETNAMP  PIC X.                                          00002350
           02 MNETNAMH  PIC X.                                          00002360
           02 MNETNAMV  PIC X.                                          00002370
           02 MNETNAMO  PIC X(8).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MSCREENA  PIC X.                                          00002400
           02 MSCREENC  PIC X.                                          00002410
           02 MSCREENP  PIC X.                                          00002420
           02 MSCREENH  PIC X.                                          00002430
           02 MSCREENV  PIC X.                                          00002440
           02 MSCREENO  PIC X(4).                                       00002450
                                                                                
