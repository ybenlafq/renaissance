      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM6102                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM6102                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6102.                                                            
           02  FM61-CPDS                                                        
               PIC X(0004).                                                     
           02  FM61-SECTION                                                     
               PIC X(0006).                                                     
           02  FM61-LSECTIONC                                                   
               PIC X(0015).                                                     
           02  FM61-LSECTIONL                                                   
               PIC X(0030).                                                     
           02  FM61-CMASQUE                                                     
               PIC X(0006).                                                     
           02  FM61-WINTERFACE                                                  
               PIC X(0001).                                                     
           02  FM61-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FM61-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM61-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FM61-WFIR                                                        
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM6102                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM6102-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-CPDS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-CPDS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-LSECTIONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-LSECTIONC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-LSECTIONL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-LSECTIONL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-WINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-WINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM61-WFIR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM61-WFIR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
