      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * LISTE DES ECS: ANCIENS NUMEROS                                  00000020
      ***************************************************************** 00000030
       01   EFM72I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOECSL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNOECSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOECSF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNOECSI   PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBECSL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLIBECSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBECSF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBECSI  PIC X(25).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSENSL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSENSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSENSF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSENSI   PIC X.                                          00000270
           02 MCRITD OCCURS   3 TIMES .                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MCRITL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCRITF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MCRITI  PIC X(5).                                       00000320
           02 MLCRITD OCCURS   3 TIMES .                                00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCRITL      COMP PIC S9(4).                            00000340
      *--                                                                       
             03 MLCRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCRITF      PIC X.                                     00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MLCRITI      PIC X(13).                                 00000370
           02 MLIGNEI OCCURS   12 TIMES .                               00000380
             03 MSD OCCURS   2 TIMES .                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSL   COMP PIC S9(4).                                 00000400
      *--                                                                       
               04 MSL COMP-5 PIC S9(4).                                         
      *}                                                                        
               04 MSF   PIC X.                                          00000410
               04 FILLER     PIC X(4).                                  00000420
               04 MSI   PIC X.                                          00000430
             03 MCTECGD OCCURS   2 TIMES .                              00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCTECGL    COMP PIC S9(4).                            00000450
      *--                                                                       
               04 MCTECGL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MCTECGF    PIC X.                                     00000460
               04 FILLER     PIC X(4).                                  00000470
               04 MCTECGI    PIC X(6).                                  00000480
             03 MSECTIOND OCCURS   2 TIMES .                            00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECTIONL  COMP PIC S9(4).                            00000500
      *--                                                                       
               04 MSECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MSECTIONF  PIC X.                                     00000510
               04 FILLER     PIC X(4).                                  00000520
               04 MSECTIONI  PIC X(6).                                  00000530
             03 MRUBRD OCCURS   2 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MRUBRL     COMP PIC S9(4).                            00000550
      *--                                                                       
               04 MRUBRL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MRUBRF     PIC X.                                     00000560
               04 FILLER     PIC X(4).                                  00000570
               04 MRUBRI     PIC X(6).                                  00000580
             03 MDEFFETD OCCURS   2 TIMES .                             00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDEFFETL   COMP PIC S9(4).                            00000600
      *--                                                                       
               04 MDEFFETL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDEFFETF   PIC X.                                     00000610
               04 FILLER     PIC X(4).                                  00000620
               04 MDEFFETI   PIC X(10).                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLIBERRI  PIC X(78).                                      00000670
      * CODE TRANSACTION                                                00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      * NOM DU CICS                                                     00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCICSI    PIC X(5).                                       00000770
      * CODE TERMINAL                                                   00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * LISTE DES ECS: ANCIENS NUMEROS                                  00000840
      ***************************************************************** 00000850
       01   EFM72O REDEFINES EFM72I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
      * DATE JOUR TRAITEMENT                                            00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MDATJOUA  PIC X.                                          00000900
           02 MDATJOUC  PIC X.                                          00000910
           02 MDATJOUP  PIC X.                                          00000920
           02 MDATJOUH  PIC X.                                          00000930
           02 MDATJOUV  PIC X.                                          00000940
           02 MDATJOUO  PIC X(10).                                      00000950
      * HEURE TRAITEMENT                                                00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MNOECSA   PIC X.                                          00001050
           02 MNOECSC   PIC X.                                          00001060
           02 MNOECSP   PIC X.                                          00001070
           02 MNOECSH   PIC X.                                          00001080
           02 MNOECSV   PIC X.                                          00001090
           02 MNOECSO   PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLIBECSA  PIC X.                                          00001120
           02 MLIBECSC  PIC X.                                          00001130
           02 MLIBECSP  PIC X.                                          00001140
           02 MLIBECSH  PIC X.                                          00001150
           02 MLIBECSV  PIC X.                                          00001160
           02 MLIBECSO  PIC X(25).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNSENSA   PIC X.                                          00001190
           02 MNSENSC   PIC X.                                          00001200
           02 MNSENSP   PIC X.                                          00001210
           02 MNSENSH   PIC X.                                          00001220
           02 MNSENSV   PIC X.                                          00001230
           02 MNSENSO   PIC X.                                          00001240
           02 DFHMS1 OCCURS   3 TIMES .                                 00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MCRITA  PIC X.                                          00001270
             03 MCRITC  PIC X.                                          00001280
             03 MCRITP  PIC X.                                          00001290
             03 MCRITH  PIC X.                                          00001300
             03 MCRITV  PIC X.                                          00001310
             03 MCRITO  PIC X(5).                                       00001320
           02 DFHMS2 OCCURS   3 TIMES .                                 00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MLCRITA      PIC X.                                     00001350
             03 MLCRITC PIC X.                                          00001360
             03 MLCRITP PIC X.                                          00001370
             03 MLCRITH PIC X.                                          00001380
             03 MLCRITV PIC X.                                          00001390
             03 MLCRITO      PIC X(13).                                 00001400
           02 MLIGNEO OCCURS   12 TIMES .                               00001410
             03 DFHMS3 OCCURS   2 TIMES .                               00001420
               04 FILLER     PIC X(2).                                  00001430
               04 MSA   PIC X.                                          00001440
               04 MSC   PIC X.                                          00001450
               04 MSP   PIC X.                                          00001460
               04 MSH   PIC X.                                          00001470
               04 MSV   PIC X.                                          00001480
               04 MSO   PIC X.                                          00001490
             03 DFHMS4 OCCURS   2 TIMES .                               00001500
               04 FILLER     PIC X(2).                                  00001510
               04 MCTECGA    PIC X.                                     00001520
               04 MCTECGC    PIC X.                                     00001530
               04 MCTECGP    PIC X.                                     00001540
               04 MCTECGH    PIC X.                                     00001550
               04 MCTECGV    PIC X.                                     00001560
               04 MCTECGO    PIC X(6).                                  00001570
             03 DFHMS5 OCCURS   2 TIMES .                               00001580
               04 FILLER     PIC X(2).                                  00001590
               04 MSECTIONA  PIC X.                                     00001600
               04 MSECTIONC  PIC X.                                     00001610
               04 MSECTIONP  PIC X.                                     00001620
               04 MSECTIONH  PIC X.                                     00001630
               04 MSECTIONV  PIC X.                                     00001640
               04 MSECTIONO  PIC X(6).                                  00001650
             03 DFHMS6 OCCURS   2 TIMES .                               00001660
               04 FILLER     PIC X(2).                                  00001670
               04 MRUBRA     PIC X.                                     00001680
               04 MRUBRC     PIC X.                                     00001690
               04 MRUBRP     PIC X.                                     00001700
               04 MRUBRH     PIC X.                                     00001710
               04 MRUBRV     PIC X.                                     00001720
               04 MRUBRO     PIC X(6).                                  00001730
             03 DFHMS7 OCCURS   2 TIMES .                               00001740
               04 FILLER     PIC X(2).                                  00001750
               04 MDEFFETA   PIC X.                                     00001760
               04 MDEFFETC   PIC X.                                     00001770
               04 MDEFFETP   PIC X.                                     00001780
               04 MDEFFETH   PIC X.                                     00001790
               04 MDEFFETV   PIC X.                                     00001800
               04 MDEFFETO   PIC X(10).                                 00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBERRA  PIC X.                                          00001830
           02 MLIBERRC  PIC X.                                          00001840
           02 MLIBERRP  PIC X.                                          00001850
           02 MLIBERRH  PIC X.                                          00001860
           02 MLIBERRV  PIC X.                                          00001870
           02 MLIBERRO  PIC X(78).                                      00001880
      * CODE TRANSACTION                                                00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCODTRAA  PIC X.                                          00001910
           02 MCODTRAC  PIC X.                                          00001920
           02 MCODTRAP  PIC X.                                          00001930
           02 MCODTRAH  PIC X.                                          00001940
           02 MCODTRAV  PIC X.                                          00001950
           02 MCODTRAO  PIC X(4).                                       00001960
      * NOM DU CICS                                                     00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCICSA    PIC X.                                          00001990
           02 MCICSC    PIC X.                                          00002000
           02 MCICSP    PIC X.                                          00002010
           02 MCICSH    PIC X.                                          00002020
           02 MCICSV    PIC X.                                          00002030
           02 MCICSO    PIC X(5).                                       00002040
      * CODE TERMINAL                                                   00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MSCREENA  PIC X.                                          00002070
           02 MSCREENC  PIC X.                                          00002080
           02 MSCREENP  PIC X.                                          00002090
           02 MSCREENH  PIC X.                                          00002100
           02 MSCREENV  PIC X.                                          00002110
           02 MSCREENO  PIC X(4).                                       00002120
                                                                                
