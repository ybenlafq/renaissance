      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT3400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT3400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT3400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT3400.                                                            
      *}                                                                        
           02  FT34-NSOC                                                        
               PIC X(0005).                                                     
           02  FT34-NETAB                                                       
               PIC X(0003).                                                     
           02  FT34-NJRN                                                        
               PIC X(0003).                                                     
           02  FT34-NEXER                                                       
               PIC X(0004).                                                     
           02  FT34-NPIECE                                                      
               PIC X(0010).                                                     
           02  FT34-NPER                                                        
               PIC X(0002).                                                     
           02  FT34-TYPECR                                                      
               PIC X(0003).                                                     
           02  FT34-TYPJRN                                                      
               PIC X(0002).                                                     
           02  FT34-ORIGINE                                                     
               PIC X(0005).                                                     
           02  FT34-NFOUR                                                       
               PIC X(0008).                                                     
           02  FT34-NAUX                                                        
               PIC X(0003).                                                     
           02  FT34-NATURE                                                      
               PIC X(0003).                                                     
           02  FT34-DDOC                                                        
               PIC X(0008).                                                     
           02  FT34-REFDOC                                                      
               PIC X(0015).                                                     
           02  FT34-CDAS2                                                       
               PIC X(0002).                                                     
           02  FT34-MTDAS2                                                      
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT34-DDECLAR                                                     
               PIC X(0008).                                                     
           02  FT34-DCPT                                                        
               PIC X(0008).                                                     
           02  FT34-MONTANT                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT34-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FT34-MTDEVBASE                                                   
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT34-MTREGLE                                                     
               PIC S9(13)V9(0002) COMP-3.                                       
           02  FT34-CREGTVA                                                     
               PIC X(0001).                                                     
           02  FT34-WCONTREP                                                    
               PIC X(0001).                                                     
           02  FT34-CACID                                                       
               PIC X(0008).                                                     
           02  FT34-DSAISIE                                                     
               PIC X(0008).                                                     
           02  FT34-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  FT34-DECHRGAR                                                    
               PIC X(0008).                                                     
           02  FT34-NLETTRAGE                                                   
               PIC X(0010).                                                     
           02  FT34-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT3400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT3400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT3400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NEXER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NEXER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NPER-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NPER-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-TYPECR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-TYPECR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-TYPJRN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-TYPJRN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NFOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NFOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DDOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DDOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-REFDOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-REFDOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-CDAS2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-CDAS2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-MTDAS2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-MTDAS2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DDECLAR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DDECLAR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DCPT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DCPT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-MONTANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-MONTANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-MTDEVBASE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-MTDEVBASE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-MTREGLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-MTREGLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-CREGTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-CREGTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-WCONTREP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-WCONTREP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DECHRGAR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DECHRGAR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-NLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT34-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT34-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
