      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CCCO2 CTRL CAISSES - CODES PAIEMENT    *        
      *----------------------------------------------------------------*        
       01  RVCCCO2.                                                             
           05  CCCO2-CTABLEG2    PIC X(15).                                     
           05  CCCO2-CTABLEG2-REDEF REDEFINES CCCO2-CTABLEG2.                   
               10  CCCO2-CPAICPT         PIC X(03).                             
           05  CCCO2-WTABLEG     PIC X(80).                                     
           05  CCCO2-WTABLEG-REDEF  REDEFINES CCCO2-WTABLEG.                    
               10  CCCO2-WACTIF          PIC X(01).                             
               10  CCCO2-LPAICPT         PIC X(20).                             
               10  CCCO2-COMPTE          PIC X(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCCCO2-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCCO2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CCCO2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CCCO2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CCCO2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
