      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA106 AU 26/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA106.                                                        
            05 NOMETAT-IRA106           PIC X(6) VALUE 'IRA106'.                
            05 RUPTURES-IRA106.                                                 
           10 IRA106-NSOCIETE           PIC X(03).                      007  003
           10 IRA106-TYPREGL            PIC X(12).                      010  012
           10 IRA106-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA106-NDEMANDE           PIC X(05).                      027  005
           10 IRA106-NREGL              PIC X(07).                      032  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA106-SEQUENCE           PIC S9(04) COMP.                039  002
      *--                                                                       
           10 IRA106-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA106.                                                   
           10 IRA106-BANQUE             PIC X(15).                      041  015
           10 IRA106-MTREMBTOT          PIC 9(09)V9(2).                 056  011
           10 IRA106-QTE                PIC 9(01)     .                 067  001
           10 IRA106-DANNUL             PIC X(08).                      068  008
           10 IRA106-DATEPARAM          PIC X(08).                      076  008
           10 IRA106-DEDITLET           PIC X(08).                      084  008
           10 IRA106-DREGL              PIC X(08).                      092  008
            05 FILLER                      PIC X(413).                          
                                                                                
