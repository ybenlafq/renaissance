      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM5500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM5500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5500.                                                            
           02  FM55-CODE                                                        
               PIC X(0003).                                                     
           02  FM55-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM55-CODEGEO                                                     
               PIC X(0001).                                                     
           02  FM55-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM5500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM5500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-CODE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-CODE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-CODEGEO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-CODEGEO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM55-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
