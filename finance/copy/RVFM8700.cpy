      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM8700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8700.                                                            
           02  FM87-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  FM87-CZONE                                                       
               PIC X(0002).                                                     
           02  FM87-LETTRAGE                                                    
               PIC X(0015).                                                     
           02  FM87-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM87-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM87-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM8700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-CINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-CZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-LETTRAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-LETTRAGE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM87-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM87-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
