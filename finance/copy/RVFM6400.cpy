      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSARO1.RTFM64                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM6400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM6400.                                                            
      *}                                                                        
           10 FM64-STEAPP               PIC X(5).                               
           10 FM64-LSTEAPPC             PIC X(15).                              
           10 FM64-LSTEAPPL             PIC X(30).                              
           10 FM64-CMASQUE              PIC X(5).                               
           10 FM64-DCLOTURE             PIC X(8).                               
           10 FM64-DMAJ                 PIC X(8).                               
           10 FM64-DSYST                PIC S9(13)V USAGE COMP-3.               
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
