      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * LONGUEUR 105 DEFINITION ENTETE                                 *      13
      *           94 DEFINITION DATA                                   *        
      * COPY POUR LE PROGRAMME MCG01                                   *      13
      ******************************************************************        
       01 WS-MQ10.                                                              
          02 WS-QUEUE.                                                          
             10 MQ10-MSGID               PIC X(24).                             
             10 MQ10-CORRELID            PIC X(24).                             
          02 WS-CODRET                   PIC X(02).                             
          02 WS-MESSAGE.                                                        
             05 MCG01-ENTETE.                                                   
                10 MCG01-TYPE            PIC X(03).                             
                10 MCG01-NSOCMSG         PIC X(03).                             
                10 MCG01-NLIEUMSG        PIC X(03).                             
                10 MCG01-NSOCDST         PIC X(03).                             
                10 MCG01-NLIEUDST        PIC X(03).                             
                10 MCG01-NORD            PIC 9(08).                             
                10 MCG01-LPROG           PIC X(10).                             
                10 MCG01-DJOUR           PIC X(08).                             
                10 MCG01-WSID            PIC X(10).                             
                10 MCG01-USER            PIC X(10).                             
                10 MCG01-CHRONO          PIC 9(07).                             
                10 MCG01-NBRMSG          PIC 9(07).                             
                10 MCG01-NBRENR          PIC 9(05).                             
                10 MCG01-TAILLE          PIC 9(05).                             
                10 MCG01-FILLER          PIC X(20).                             
             05 MCG01-DATA.                                                     
                10 MCG01-CFONC           PIC X(03).                             
                10 MCG01-NCDE            PIC X(07).                             
                10 MCG01-CMODRGLT        PIC X(05).                             
                10 MCG01-QDELRGLT        PIC 9(03).                             
                10 MCG01-CDEPRGLT        PIC X(05).                             
                10 MCG01-QTAUXESCPT      PIC 9(03)V99.                          
                10 MCG01-NCODIC          PIC X(07).                             
                10 MCG01-PABASEFACT      PIC 9(07)V99.                          
                10 MCG01-PTAUXESCPT      PIC 9(07)V99.                          
                10 MCG01-PRISTPROMO      PIC 9(07)V99.                          
                10 MCG01-PRISTCONDI      PIC 9(07)V99.                          
                10 MCG01-PRA             PIC 9(07)V99.                          
                10 MCG01-PCF             PIC 9(07)V9(6).                        
                10 MCG01-FFACTURE        PIC X.                                 
                                                                                
