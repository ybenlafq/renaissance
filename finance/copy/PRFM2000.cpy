      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE FM2000                       
      ******************************************************************        
      *                                                                         
       CLEF-FM2000             SECTION.                                         
      *                                                                         
           MOVE 'RVFM2000          '       TO   TABLE-NAME.                     
           MOVE 'FM2000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-FM2000. EXIT.                                                   
                EJECT                                                           
                                                                                
