      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERA02   ERA02                                              00000020
      ***************************************************************** 00000030
       01   ERA02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTITREI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDEML  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLIBDEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBDEMF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBDEMI  PIC X(13).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNDEMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREGLL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNREGLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNREGLF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNREGLI   PIC X(40).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIEUL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIBLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIEUF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBLIEUI      PIC X(9).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMAGI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNVENTEI  PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLETTRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNLETTRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLETTRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNLETTRI  PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDAVOIRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDAVOIRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDAVOIRI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNAVOIRI  PIC X(6).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMOTIFL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MMOTIFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMOTIFF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MMOTIFI   PIC X(2).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAVOIRL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAVOIRF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLAVOIRI  PIC X(23).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNOMCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMCF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNOMCI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMCL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLNOMCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMCF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLNOMCI   PIC X(25).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMCL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLPNOMCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMCF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLPNOMCI  PIC X(15).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATCL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCBATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATCF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCBATCI   PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCESCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCESCF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCESCI    PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGCL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCETAGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGCF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCETAGCI  PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTECL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MCPORTECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTECF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCPORTECI      PIC X(3).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADDRCL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLADDRCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLADDRCF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLADDRCI  PIC X(32).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIECL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIECF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNVOIECI  PIC X(5).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIECL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MTPVOIECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIECF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MTPVOIECI      PIC X(4).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIECL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIECF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLVOIECI  PIC X(21).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD10L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MTELD10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD10F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MTELD10I  PIC X(2).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD11L  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MTELD11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD11F  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MTELD11I  PIC X(2).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD12L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MTELD12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD12F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MTELD12I  PIC X(2).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD13L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MTELD13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD13F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MTELD13I  PIC X(2).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD14L  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MTELD14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD14F  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MTELD14I  PIC X(2).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNCL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCCOMNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNCF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCCOMNCI  PIC X(32).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB10L  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MTELB10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB10F  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MTELB10I  PIC X(2).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB11L  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MTELB11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB11F  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MTELB11I  PIC X(2).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB12L  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MTELB12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB12F  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MTELB12I  PIC X(2).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB13L  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MTELB13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB13F  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MTELB13I  PIC X(2).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB14L  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MTELB14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB14F  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MTELB14I  PIC X(2).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTCL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTCF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCPOSTCI  PIC X(5).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTCL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MLPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTCF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MLPOSTCI  PIC X(26).                                      00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTEL   COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MPOSTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPOSTEF   PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MPOSTEI   PIC X(5).                                       00001690
           02 MPRODUITI OCCURS   4 TIMES .                              00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQUL      COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MMARQUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMARQUF      PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MMARQUI      PIC X(5).                                  00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00001750
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MREFI   PIC X(24).                                      00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00001790
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 MCODICI      PIC X(7).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREMBL  COMP PIC S9(4).                                 00001830
      *--                                                                       
             03 MREMBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MREMBF  PIC X.                                          00001840
             03 FILLER  PIC X(4).                                       00001850
             03 MREMBI  PIC X(9).                                       00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTREMBL      COMP PIC S9(4).                            00001870
      *--                                                                       
           02 MTOTREMBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTREMBF      PIC X.                                     00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MTOTREMBI      PIC X(9).                                  00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00001910
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MDECHEANI      PIC X(10).                                 00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREMBL      COMP PIC S9(4).                            00001950
      *--                                                                       
           02 MTYPREMBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREMBF      PIC X.                                     00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MTYPREMBI      PIC X.                                     00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBANQL   COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MCBANQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBANQF   PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MCBANQI   PIC X(5).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBANQL   COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MLBANQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLBANQF   PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MLBANQI   PIC X(30).                                      00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLETTREL      COMP PIC S9(4).                            00002070
      *--                                                                       
           02 MCLETTREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLETTREF      PIC X.                                     00002080
           02 FILLER    PIC X(4).                                       00002090
           02 MCLETTREI      PIC X(2).                                  00002100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002120
           02 FILLER    PIC X(4).                                       00002130
           02 MZONCMDI  PIC X(15).                                      00002140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002160
           02 FILLER    PIC X(4).                                       00002170
           02 MLIBERRI  PIC X(58).                                      00002180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002200
           02 FILLER    PIC X(4).                                       00002210
           02 MCODTRAI  PIC X(4).                                       00002220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002240
           02 FILLER    PIC X(4).                                       00002250
           02 MCICSI    PIC X(5).                                       00002260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002280
           02 FILLER    PIC X(4).                                       00002290
           02 MNETNAMI  PIC X(8).                                       00002300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002320
           02 FILLER    PIC X(4).                                       00002330
           02 MSCREENI  PIC X(4).                                       00002340
      ***************************************************************** 00002350
      * SDF: ERA02   ERA02                                              00002360
      ***************************************************************** 00002370
       01   ERA02O REDEFINES ERA02I.                                    00002380
           02 FILLER    PIC X(12).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDATJOUA  PIC X.                                          00002410
           02 MDATJOUC  PIC X.                                          00002420
           02 MDATJOUP  PIC X.                                          00002430
           02 MDATJOUH  PIC X.                                          00002440
           02 MDATJOUV  PIC X.                                          00002450
           02 MDATJOUO  PIC X(10).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MTIMJOUA  PIC X.                                          00002480
           02 MTIMJOUC  PIC X.                                          00002490
           02 MTIMJOUP  PIC X.                                          00002500
           02 MTIMJOUH  PIC X.                                          00002510
           02 MTIMJOUV  PIC X.                                          00002520
           02 MTIMJOUO  PIC X(5).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MFONCA    PIC X.                                          00002550
           02 MFONCC    PIC X.                                          00002560
           02 MFONCP    PIC X.                                          00002570
           02 MFONCH    PIC X.                                          00002580
           02 MFONCV    PIC X.                                          00002590
           02 MFONCO    PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MTITREA   PIC X.                                          00002620
           02 MTITREC   PIC X.                                          00002630
           02 MTITREP   PIC X.                                          00002640
           02 MTITREH   PIC X.                                          00002650
           02 MTITREV   PIC X.                                          00002660
           02 MTITREO   PIC X(7).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLIBDEMA  PIC X.                                          00002690
           02 MLIBDEMC  PIC X.                                          00002700
           02 MLIBDEMP  PIC X.                                          00002710
           02 MLIBDEMH  PIC X.                                          00002720
           02 MLIBDEMV  PIC X.                                          00002730
           02 MLIBDEMO  PIC X(13).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNDEMA    PIC X.                                          00002760
           02 MNDEMC    PIC X.                                          00002770
           02 MNDEMP    PIC X.                                          00002780
           02 MNDEMH    PIC X.                                          00002790
           02 MNDEMV    PIC X.                                          00002800
           02 MNDEMO    PIC X(5).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MNREGLA   PIC X.                                          00002830
           02 MNREGLC   PIC X.                                          00002840
           02 MNREGLP   PIC X.                                          00002850
           02 MNREGLH   PIC X.                                          00002860
           02 MNREGLV   PIC X.                                          00002870
           02 MNREGLO   PIC X(40).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNSOCA    PIC X.                                          00002900
           02 MNSOCC    PIC X.                                          00002910
           02 MNSOCP    PIC X.                                          00002920
           02 MNSOCH    PIC X.                                          00002930
           02 MNSOCV    PIC X.                                          00002940
           02 MNSOCO    PIC X(3).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLIBLIEUA      PIC X.                                     00002970
           02 MLIBLIEUC PIC X.                                          00002980
           02 MLIBLIEUP PIC X.                                          00002990
           02 MLIBLIEUH PIC X.                                          00003000
           02 MLIBLIEUV PIC X.                                          00003010
           02 MLIBLIEUO      PIC X(9).                                  00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MNMAGA    PIC X.                                          00003040
           02 MNMAGC    PIC X.                                          00003050
           02 MNMAGP    PIC X.                                          00003060
           02 MNMAGH    PIC X.                                          00003070
           02 MNMAGV    PIC X.                                          00003080
           02 MNMAGO    PIC X(3).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MNVENTEA  PIC X.                                          00003110
           02 MNVENTEC  PIC X.                                          00003120
           02 MNVENTEP  PIC X.                                          00003130
           02 MNVENTEH  PIC X.                                          00003140
           02 MNVENTEV  PIC X.                                          00003150
           02 MNVENTEO  PIC X(7).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MNLETTRA  PIC X.                                          00003180
           02 MNLETTRC  PIC X.                                          00003190
           02 MNLETTRP  PIC X.                                          00003200
           02 MNLETTRH  PIC X.                                          00003210
           02 MNLETTRV  PIC X.                                          00003220
           02 MNLETTRO  PIC X(10).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MDAVOIRA  PIC X.                                          00003250
           02 MDAVOIRC  PIC X.                                          00003260
           02 MDAVOIRP  PIC X.                                          00003270
           02 MDAVOIRH  PIC X.                                          00003280
           02 MDAVOIRV  PIC X.                                          00003290
           02 MDAVOIRO  PIC X(10).                                      00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNAVOIRA  PIC X.                                          00003320
           02 MNAVOIRC  PIC X.                                          00003330
           02 MNAVOIRP  PIC X.                                          00003340
           02 MNAVOIRH  PIC X.                                          00003350
           02 MNAVOIRV  PIC X.                                          00003360
           02 MNAVOIRO  PIC 9(6).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MMOTIFA   PIC X.                                          00003390
           02 MMOTIFC   PIC X.                                          00003400
           02 MMOTIFP   PIC X.                                          00003410
           02 MMOTIFH   PIC X.                                          00003420
           02 MMOTIFV   PIC X.                                          00003430
           02 MMOTIFO   PIC X(2).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MLAVOIRA  PIC X.                                          00003460
           02 MLAVOIRC  PIC X.                                          00003470
           02 MLAVOIRP  PIC X.                                          00003480
           02 MLAVOIRH  PIC X.                                          00003490
           02 MLAVOIRV  PIC X.                                          00003500
           02 MLAVOIRO  PIC X(23).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MNOMCA    PIC X.                                          00003530
           02 MNOMCC    PIC X.                                          00003540
           02 MNOMCP    PIC X.                                          00003550
           02 MNOMCH    PIC X.                                          00003560
           02 MNOMCV    PIC X.                                          00003570
           02 MNOMCO    PIC X(5).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MLNOMCA   PIC X.                                          00003600
           02 MLNOMCC   PIC X.                                          00003610
           02 MLNOMCP   PIC X.                                          00003620
           02 MLNOMCH   PIC X.                                          00003630
           02 MLNOMCV   PIC X.                                          00003640
           02 MLNOMCO   PIC X(25).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MLPNOMCA  PIC X.                                          00003670
           02 MLPNOMCC  PIC X.                                          00003680
           02 MLPNOMCP  PIC X.                                          00003690
           02 MLPNOMCH  PIC X.                                          00003700
           02 MLPNOMCV  PIC X.                                          00003710
           02 MLPNOMCO  PIC X(15).                                      00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MCBATCA   PIC X.                                          00003740
           02 MCBATCC   PIC X.                                          00003750
           02 MCBATCP   PIC X.                                          00003760
           02 MCBATCH   PIC X.                                          00003770
           02 MCBATCV   PIC X.                                          00003780
           02 MCBATCO   PIC X(3).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MCESCA    PIC X.                                          00003810
           02 MCESCC    PIC X.                                          00003820
           02 MCESCP    PIC X.                                          00003830
           02 MCESCH    PIC X.                                          00003840
           02 MCESCV    PIC X.                                          00003850
           02 MCESCO    PIC X(3).                                       00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MCETAGCA  PIC X.                                          00003880
           02 MCETAGCC  PIC X.                                          00003890
           02 MCETAGCP  PIC X.                                          00003900
           02 MCETAGCH  PIC X.                                          00003910
           02 MCETAGCV  PIC X.                                          00003920
           02 MCETAGCO  PIC X(3).                                       00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MCPORTECA      PIC X.                                     00003950
           02 MCPORTECC PIC X.                                          00003960
           02 MCPORTECP PIC X.                                          00003970
           02 MCPORTECH PIC X.                                          00003980
           02 MCPORTECV PIC X.                                          00003990
           02 MCPORTECO      PIC X(3).                                  00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MLADDRCA  PIC X.                                          00004020
           02 MLADDRCC  PIC X.                                          00004030
           02 MLADDRCP  PIC X.                                          00004040
           02 MLADDRCH  PIC X.                                          00004050
           02 MLADDRCV  PIC X.                                          00004060
           02 MLADDRCO  PIC X(32).                                      00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MNVOIECA  PIC X.                                          00004090
           02 MNVOIECC  PIC X.                                          00004100
           02 MNVOIECP  PIC X.                                          00004110
           02 MNVOIECH  PIC X.                                          00004120
           02 MNVOIECV  PIC X.                                          00004130
           02 MNVOIECO  PIC X(5).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MTPVOIECA      PIC X.                                     00004160
           02 MTPVOIECC PIC X.                                          00004170
           02 MTPVOIECP PIC X.                                          00004180
           02 MTPVOIECH PIC X.                                          00004190
           02 MTPVOIECV PIC X.                                          00004200
           02 MTPVOIECO      PIC X(4).                                  00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MLVOIECA  PIC X.                                          00004230
           02 MLVOIECC  PIC X.                                          00004240
           02 MLVOIECP  PIC X.                                          00004250
           02 MLVOIECH  PIC X.                                          00004260
           02 MLVOIECV  PIC X.                                          00004270
           02 MLVOIECO  PIC X(21).                                      00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MTELD10A  PIC X.                                          00004300
           02 MTELD10C  PIC X.                                          00004310
           02 MTELD10P  PIC X.                                          00004320
           02 MTELD10H  PIC X.                                          00004330
           02 MTELD10V  PIC X.                                          00004340
           02 MTELD10O  PIC X(2).                                       00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MTELD11A  PIC X.                                          00004370
           02 MTELD11C  PIC X.                                          00004380
           02 MTELD11P  PIC X.                                          00004390
           02 MTELD11H  PIC X.                                          00004400
           02 MTELD11V  PIC X.                                          00004410
           02 MTELD11O  PIC X(2).                                       00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MTELD12A  PIC X.                                          00004440
           02 MTELD12C  PIC X.                                          00004450
           02 MTELD12P  PIC X.                                          00004460
           02 MTELD12H  PIC X.                                          00004470
           02 MTELD12V  PIC X.                                          00004480
           02 MTELD12O  PIC X(2).                                       00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MTELD13A  PIC X.                                          00004510
           02 MTELD13C  PIC X.                                          00004520
           02 MTELD13P  PIC X.                                          00004530
           02 MTELD13H  PIC X.                                          00004540
           02 MTELD13V  PIC X.                                          00004550
           02 MTELD13O  PIC X(2).                                       00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MTELD14A  PIC X.                                          00004580
           02 MTELD14C  PIC X.                                          00004590
           02 MTELD14P  PIC X.                                          00004600
           02 MTELD14H  PIC X.                                          00004610
           02 MTELD14V  PIC X.                                          00004620
           02 MTELD14O  PIC X(2).                                       00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MCCOMNCA  PIC X.                                          00004650
           02 MCCOMNCC  PIC X.                                          00004660
           02 MCCOMNCP  PIC X.                                          00004670
           02 MCCOMNCH  PIC X.                                          00004680
           02 MCCOMNCV  PIC X.                                          00004690
           02 MCCOMNCO  PIC X(32).                                      00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MTELB10A  PIC X.                                          00004720
           02 MTELB10C  PIC X.                                          00004730
           02 MTELB10P  PIC X.                                          00004740
           02 MTELB10H  PIC X.                                          00004750
           02 MTELB10V  PIC X.                                          00004760
           02 MTELB10O  PIC X(2).                                       00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MTELB11A  PIC X.                                          00004790
           02 MTELB11C  PIC X.                                          00004800
           02 MTELB11P  PIC X.                                          00004810
           02 MTELB11H  PIC X.                                          00004820
           02 MTELB11V  PIC X.                                          00004830
           02 MTELB11O  PIC X(2).                                       00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MTELB12A  PIC X.                                          00004860
           02 MTELB12C  PIC X.                                          00004870
           02 MTELB12P  PIC X.                                          00004880
           02 MTELB12H  PIC X.                                          00004890
           02 MTELB12V  PIC X.                                          00004900
           02 MTELB12O  PIC X(2).                                       00004910
           02 FILLER    PIC X(2).                                       00004920
           02 MTELB13A  PIC X.                                          00004930
           02 MTELB13C  PIC X.                                          00004940
           02 MTELB13P  PIC X.                                          00004950
           02 MTELB13H  PIC X.                                          00004960
           02 MTELB13V  PIC X.                                          00004970
           02 MTELB13O  PIC X(2).                                       00004980
           02 FILLER    PIC X(2).                                       00004990
           02 MTELB14A  PIC X.                                          00005000
           02 MTELB14C  PIC X.                                          00005010
           02 MTELB14P  PIC X.                                          00005020
           02 MTELB14H  PIC X.                                          00005030
           02 MTELB14V  PIC X.                                          00005040
           02 MTELB14O  PIC X(2).                                       00005050
           02 FILLER    PIC X(2).                                       00005060
           02 MCPOSTCA  PIC X.                                          00005070
           02 MCPOSTCC  PIC X.                                          00005080
           02 MCPOSTCP  PIC X.                                          00005090
           02 MCPOSTCH  PIC X.                                          00005100
           02 MCPOSTCV  PIC X.                                          00005110
           02 MCPOSTCO  PIC X(5).                                       00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MLPOSTCA  PIC X.                                          00005140
           02 MLPOSTCC  PIC X.                                          00005150
           02 MLPOSTCP  PIC X.                                          00005160
           02 MLPOSTCH  PIC X.                                          00005170
           02 MLPOSTCV  PIC X.                                          00005180
           02 MLPOSTCO  PIC X(26).                                      00005190
           02 FILLER    PIC X(2).                                       00005200
           02 MPOSTEA   PIC X.                                          00005210
           02 MPOSTEC   PIC X.                                          00005220
           02 MPOSTEP   PIC X.                                          00005230
           02 MPOSTEH   PIC X.                                          00005240
           02 MPOSTEV   PIC X.                                          00005250
           02 MPOSTEO   PIC X(5).                                       00005260
           02 MPRODUITO OCCURS   4 TIMES .                              00005270
             03 FILLER       PIC X(2).                                  00005280
             03 MMARQUA      PIC X.                                     00005290
             03 MMARQUC PIC X.                                          00005300
             03 MMARQUP PIC X.                                          00005310
             03 MMARQUH PIC X.                                          00005320
             03 MMARQUV PIC X.                                          00005330
             03 MMARQUO      PIC X(5).                                  00005340
             03 FILLER       PIC X(2).                                  00005350
             03 MREFA   PIC X.                                          00005360
             03 MREFC   PIC X.                                          00005370
             03 MREFP   PIC X.                                          00005380
             03 MREFH   PIC X.                                          00005390
             03 MREFV   PIC X.                                          00005400
             03 MREFO   PIC X(24).                                      00005410
             03 FILLER       PIC X(2).                                  00005420
             03 MCODICA      PIC X.                                     00005430
             03 MCODICC PIC X.                                          00005440
             03 MCODICP PIC X.                                          00005450
             03 MCODICH PIC X.                                          00005460
             03 MCODICV PIC X.                                          00005470
             03 MCODICO      PIC 9(7).                                  00005480
             03 FILLER       PIC X(2).                                  00005490
             03 MREMBA  PIC X.                                          00005500
             03 MREMBC  PIC X.                                          00005510
             03 MREMBP  PIC X.                                          00005520
             03 MREMBH  PIC X.                                          00005530
             03 MREMBV  PIC X.                                          00005540
             03 MREMBO  PIC ZZZZZ9,99.                                  00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MTOTREMBA      PIC X.                                     00005570
           02 MTOTREMBC PIC X.                                          00005580
           02 MTOTREMBP PIC X.                                          00005590
           02 MTOTREMBH PIC X.                                          00005600
           02 MTOTREMBV PIC X.                                          00005610
           02 MTOTREMBO      PIC ZZZZZ9,99.                             00005620
           02 FILLER    PIC X(2).                                       00005630
           02 MDECHEANA      PIC X.                                     00005640
           02 MDECHEANC PIC X.                                          00005650
           02 MDECHEANP PIC X.                                          00005660
           02 MDECHEANH PIC X.                                          00005670
           02 MDECHEANV PIC X.                                          00005680
           02 MDECHEANO      PIC X(10).                                 00005690
           02 FILLER    PIC X(2).                                       00005700
           02 MTYPREMBA      PIC X.                                     00005710
           02 MTYPREMBC PIC X.                                          00005720
           02 MTYPREMBP PIC X.                                          00005730
           02 MTYPREMBH PIC X.                                          00005740
           02 MTYPREMBV PIC X.                                          00005750
           02 MTYPREMBO      PIC X.                                     00005760
           02 FILLER    PIC X(2).                                       00005770
           02 MCBANQA   PIC X.                                          00005780
           02 MCBANQC   PIC X.                                          00005790
           02 MCBANQP   PIC X.                                          00005800
           02 MCBANQH   PIC X.                                          00005810
           02 MCBANQV   PIC X.                                          00005820
           02 MCBANQO   PIC X(5).                                       00005830
           02 FILLER    PIC X(2).                                       00005840
           02 MLBANQA   PIC X.                                          00005850
           02 MLBANQC   PIC X.                                          00005860
           02 MLBANQP   PIC X.                                          00005870
           02 MLBANQH   PIC X.                                          00005880
           02 MLBANQV   PIC X.                                          00005890
           02 MLBANQO   PIC X(30).                                      00005900
           02 FILLER    PIC X(2).                                       00005910
           02 MCLETTREA      PIC X.                                     00005920
           02 MCLETTREC PIC X.                                          00005930
           02 MCLETTREP PIC X.                                          00005940
           02 MCLETTREH PIC X.                                          00005950
           02 MCLETTREV PIC X.                                          00005960
           02 MCLETTREO      PIC X(2).                                  00005970
           02 FILLER    PIC X(2).                                       00005980
           02 MZONCMDA  PIC X.                                          00005990
           02 MZONCMDC  PIC X.                                          00006000
           02 MZONCMDP  PIC X.                                          00006010
           02 MZONCMDH  PIC X.                                          00006020
           02 MZONCMDV  PIC X.                                          00006030
           02 MZONCMDO  PIC X(15).                                      00006040
           02 FILLER    PIC X(2).                                       00006050
           02 MLIBERRA  PIC X.                                          00006060
           02 MLIBERRC  PIC X.                                          00006070
           02 MLIBERRP  PIC X.                                          00006080
           02 MLIBERRH  PIC X.                                          00006090
           02 MLIBERRV  PIC X.                                          00006100
           02 MLIBERRO  PIC X(58).                                      00006110
           02 FILLER    PIC X(2).                                       00006120
           02 MCODTRAA  PIC X.                                          00006130
           02 MCODTRAC  PIC X.                                          00006140
           02 MCODTRAP  PIC X.                                          00006150
           02 MCODTRAH  PIC X.                                          00006160
           02 MCODTRAV  PIC X.                                          00006170
           02 MCODTRAO  PIC X(4).                                       00006180
           02 FILLER    PIC X(2).                                       00006190
           02 MCICSA    PIC X.                                          00006200
           02 MCICSC    PIC X.                                          00006210
           02 MCICSP    PIC X.                                          00006220
           02 MCICSH    PIC X.                                          00006230
           02 MCICSV    PIC X.                                          00006240
           02 MCICSO    PIC X(5).                                       00006250
           02 FILLER    PIC X(2).                                       00006260
           02 MNETNAMA  PIC X.                                          00006270
           02 MNETNAMC  PIC X.                                          00006280
           02 MNETNAMP  PIC X.                                          00006290
           02 MNETNAMH  PIC X.                                          00006300
           02 MNETNAMV  PIC X.                                          00006310
           02 MNETNAMO  PIC X(8).                                       00006320
           02 FILLER    PIC X(2).                                       00006330
           02 MSCREENA  PIC X.                                          00006340
           02 MSCREENC  PIC X.                                          00006350
           02 MSCREENP  PIC X.                                          00006360
           02 MSCREENH  PIC X.                                          00006370
           02 MSCREENV  PIC X.                                          00006380
           02 MSCREENO  PIC X(4).                                       00006390
                                                                                
