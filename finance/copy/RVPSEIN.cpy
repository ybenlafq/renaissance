      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PSEIN CONVERSION PSE EN CODE NUM       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPSEIN.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPSEIN.                                                             
      *}                                                                        
           05  PSEIN-CTABLEG2    PIC X(15).                                     
           05  PSEIN-CTABLEG2-REDEF REDEFINES PSEIN-CTABLEG2.                   
               10  PSEIN-CGCPLTNU        PIC X(05).                             
           05  PSEIN-WTABLEG     PIC X(80).                                     
           05  PSEIN-WTABLEG-REDEF  REDEFINES PSEIN-WTABLEG.                    
               10  PSEIN-CGCPLT          PIC X(05).                             
               10  PSEIN-WACTIF          PIC X(01).                             
               10  PSEIN-LIBCPLT         PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPSEIN-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPSEIN-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEIN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PSEIN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PSEIN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PSEIN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
