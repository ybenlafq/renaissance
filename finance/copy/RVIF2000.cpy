      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVIF2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIF2000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF2000.                                                            
      *}                                                                        
           02  IF20-CORGANISME                                                  
               PIC X(0003).                                                     
           02  IF20-CMODPAI                                                     
               PIC X(0005).                                                     
           02  IF20-NIDENT                                                      
               PIC X(0020).                                                     
           02  IF20-DFINANCE                                                    
               PIC X(0008).                                                     
           02  IF20-DTRAIT                                                      
               PIC X(0008).                                                     
           02  IF20-DREMISE                                                     
               PIC X(0008).                                                     
           02  IF20-NREMISE                                                     
               PIC X(0006).                                                     
           02  IF20-NBMVTS                                                      
               PIC S9(5)    COMP-3.                                             
           02  IF20-NJRN                                                        
               PIC X(0003).                                                     
           02  IF20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIF2000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIF2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIF2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-CORGANISME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-CORGANISME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-CMODPAI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-CMODPAI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-NIDENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-NIDENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-DFINANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-DFINANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-DREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-DREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-NREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-NREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-NBMVTS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-NBMVTS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IF20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IF20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
