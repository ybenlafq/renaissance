      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DETERMINATION DES CLES COMPTABLE                                00000020
      ***************************************************************** 00000030
       01   EFM73I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MECRITD OCCURS   2 TIMES .                                00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MECRITL      COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MECRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MECRITF      PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MECRITI      PIC X(3).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPCAL    COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MLPCAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLPCAF    PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MLPCAI    PIC X(3).                                       00000320
           02 MLIGNEI OCCURS   14 TIMES .                               00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000340
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MXI     PIC X.                                          00000370
             03 MCRITD OCCURS   2 TIMES .                               00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCRITL     COMP PIC S9(4).                            00000390
      *--                                                                       
               04 MCRITL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCRITF     PIC X.                                     00000400
               04 FILLER     PIC X(4).                                  00000410
               04 MCRITI     PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIBELI      PIC X(30).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPCAL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MPCAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPCAF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MPCAI   PIC X(3).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBPCAL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLIBPCAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBPCAF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLIBPCAI     PIC X(30).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      * CODE TRANSACTION                                                00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      * NOM DU CICS                                                     00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCICSI    PIC X(5).                                       00000680
      * CODE TERMINAL                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * DETERMINATION DES CLES COMPTABLE                                00000750
      ***************************************************************** 00000760
       01   EFM73O REDEFINES EFM73I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
      * DATE JOUR TRAITEMENT                                            00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
      * HEURE TRAITEMENT                                                00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MPAGEA    PIC X.                                          00000960
           02 MPAGEC    PIC X.                                          00000970
           02 MPAGEP    PIC X.                                          00000980
           02 MPAGEH    PIC X.                                          00000990
           02 MPAGEV    PIC X.                                          00001000
           02 MPAGEO    PIC Z9.                                         00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNBPA     PIC X.                                          00001030
           02 MNBPC     PIC X.                                          00001040
           02 MNBPP     PIC X.                                          00001050
           02 MNBPH     PIC X.                                          00001060
           02 MNBPV     PIC X.                                          00001070
           02 MNBPO     PIC Z9.                                         00001080
           02 DFHMS1 OCCURS   2 TIMES .                                 00001090
             03 FILLER       PIC X(2).                                  00001100
             03 MECRITA      PIC X.                                     00001110
             03 MECRITC PIC X.                                          00001120
             03 MECRITP PIC X.                                          00001130
             03 MECRITH PIC X.                                          00001140
             03 MECRITV PIC X.                                          00001150
             03 MECRITO      PIC X(3).                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MLPCAA    PIC X.                                          00001180
           02 MLPCAC    PIC X.                                          00001190
           02 MLPCAP    PIC X.                                          00001200
           02 MLPCAH    PIC X.                                          00001210
           02 MLPCAV    PIC X.                                          00001220
           02 MLPCAO    PIC X(3).                                       00001230
           02 MLIGNEO OCCURS   14 TIMES .                               00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MXA     PIC X.                                          00001260
             03 MXC     PIC X.                                          00001270
             03 MXP     PIC X.                                          00001280
             03 MXH     PIC X.                                          00001290
             03 MXV     PIC X.                                          00001300
             03 MXO     PIC X.                                          00001310
             03 DFHMS2 OCCURS   2 TIMES .                               00001320
               04 FILLER     PIC X(2).                                  00001330
               04 MCRITA     PIC X.                                     00001340
               04 MCRITC     PIC X.                                     00001350
               04 MCRITP     PIC X.                                     00001360
               04 MCRITH     PIC X.                                     00001370
               04 MCRITV     PIC X.                                     00001380
               04 MCRITO     PIC X(3).                                  00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MLIBELA      PIC X.                                     00001410
             03 MLIBELC PIC X.                                          00001420
             03 MLIBELP PIC X.                                          00001430
             03 MLIBELH PIC X.                                          00001440
             03 MLIBELV PIC X.                                          00001450
             03 MLIBELO      PIC X(30).                                 00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MPCAA   PIC X.                                          00001480
             03 MPCAC   PIC X.                                          00001490
             03 MPCAP   PIC X.                                          00001500
             03 MPCAH   PIC X.                                          00001510
             03 MPCAV   PIC X.                                          00001520
             03 MPCAO   PIC X(3).                                       00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MLIBPCAA     PIC X.                                     00001550
             03 MLIBPCAC     PIC X.                                     00001560
             03 MLIBPCAP     PIC X.                                     00001570
             03 MLIBPCAH     PIC X.                                     00001580
             03 MLIBPCAV     PIC X.                                     00001590
             03 MLIBPCAO     PIC X(30).                                 00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLIBERRA  PIC X.                                          00001620
           02 MLIBERRC  PIC X.                                          00001630
           02 MLIBERRP  PIC X.                                          00001640
           02 MLIBERRH  PIC X.                                          00001650
           02 MLIBERRV  PIC X.                                          00001660
           02 MLIBERRO  PIC X(78).                                      00001670
      * CODE TRANSACTION                                                00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCODTRAA  PIC X.                                          00001700
           02 MCODTRAC  PIC X.                                          00001710
           02 MCODTRAP  PIC X.                                          00001720
           02 MCODTRAH  PIC X.                                          00001730
           02 MCODTRAV  PIC X.                                          00001740
           02 MCODTRAO  PIC X(4).                                       00001750
      * NOM DU CICS                                                     00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCICSA    PIC X.                                          00001780
           02 MCICSC    PIC X.                                          00001790
           02 MCICSP    PIC X.                                          00001800
           02 MCICSH    PIC X.                                          00001810
           02 MCICSV    PIC X.                                          00001820
           02 MCICSO    PIC X(5).                                       00001830
      * CODE TERMINAL                                                   00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MSCREENA  PIC X.                                          00001860
           02 MSCREENC  PIC X.                                          00001870
           02 MSCREENP  PIC X.                                          00001880
           02 MSCREENH  PIC X.                                          00001890
           02 MSCREENV  PIC X.                                          00001900
           02 MSCREENO  PIC X(4).                                       00001910
                                                                                
