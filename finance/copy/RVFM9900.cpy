      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVFM9900                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM9900                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM9900.                                                    00090001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM9900.                                                            
      *}                                                                        
           02  FM99-CPTSAP                                              00100001
               PIC X(0008).                                             00110003
           02  FM99-WSTEAPP                                             00111005
               PIC X(0001).                                             00112006
           02  FM99-CPTGCT                                              00120001
               PIC X(0006).                                             00130001
           02  FM99-RUBRGCT                                             00140001
               PIC X(0006).                                             00150002
           02  FM99-TYPCPT                                              00160001
               PIC X(0001).                                             00170002
           02  FM99-CSODEBI                                             00180001
               PIC X(0006).                                             00190002
           02  FM99-CSOCRED                                             00200001
               PIC X(0006).                                             00210002
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00500000
      *---------------------------------------------------------        00510000
      *   LISTE DES FLAGS DE LA TABLE RVFM9900                          00520001
      *---------------------------------------------------------        00530000
      *                                                                 00540000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFM9900-FLAGS.                                              00550001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFM9900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-CPTSAP-F                                            00560001
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  FM99-CPTSAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-WSTEAPP-F                                           00571005
      *        PIC S9(4) COMP.                                          00572005
      *--                                                                       
           02  FM99-WSTEAPP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-CPTGCT-F                                            00580001
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  FM99-CPTGCT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-RUBRGCT-F                                           00600001
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  FM99-RUBRGCT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-TYPCPT-F                                            00620001
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  FM99-TYPCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-CSODEBI-F                                           00640001
      *        PIC S9(4) COMP.                                          00650000
      *--                                                                       
           02  FM99-CSODEBI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM99-CSOCRED-F                                           00660001
      *        PIC S9(4) COMP.                                          00670000
      *--                                                                       
           02  FM99-CSOCRED-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00960000
                                                                                
