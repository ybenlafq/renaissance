      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA INTERFACE PAIE SIGAGIP/CS - GCT         TR: SGXX  *    00002200
      *                                                            *    00002900
      **************************************************************    00003100
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00003200
      **************************************************************    00003300
      *                                                                 00003400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SG00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00005300
      *--                                                                       
       01  COM-SG00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00005400
       01  Z-COMMAREA.                                                  00005500
      *                                                                 00005600
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00005700
          02 FILLER-COM-AIDA                  PIC X(100).               00005800
      *                                                                 00005900
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00006000
          02 COMM-CICS-APPLID                 PIC X(8).                 00006100
          02 COMM-CICS-NETNAM                 PIC X(8).                 00006200
          02 COMM-CICS-TRANSA                 PIC X(4).                 00006300
      *                                                                 00006400
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00006500
          02 COMM-DATE-SIECLE                 PIC XX.                   00006600
          02 COMM-DATE-ANNEE                  PIC XX.                   00006700
          02 COMM-DATE-MOIS                   PIC XX.                   00006800
          02 COMM-DATE-JOUR                   PIC XX.                   00006900
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00007000
          02 COMM-DATE-QNTA                   PIC 999.                  00007100
          02 COMM-DATE-QNT0                   PIC 99999.                00007200
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00007300
          02 COMM-DATE-BISX                   PIC 9.                    00007400
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00007500
          02 COMM-DATE-JSM                    PIC 9.                    00007600
      *   LIBELLES DU JOUR COURT - LONG                                 00007700
          02 COMM-DATE-JSM-LC                 PIC XXX.                  00007800
          02 COMM-DATE-JSM-LL                 PIC XXXXXXXX.             00007900
      *   LIBELLES DU MOIS COURT - LONG                                 00008000
          02 COMM-DATE-MOIS-LC                PIC XXX.                  00008100
          02 COMM-DATE-MOIS-LL                PIC XXXXXXXX.             00008200
      *   DIFFERENTES FORMES DE DATE                                    00008300
          02 COMM-DATE-SSAAMMJJ               PIC X(8).                 00008400
          02 COMM-DATE-AAMMJJ                 PIC X(6).                 00008500
          02 COMM-DATE-JJMMSSAA               PIC X(8).                 00008600
          02 COMM-DATE-JJMMAA                 PIC X(6).                 00008700
          02 COMM-DATE-JJ-MM-AA               PIC X(8).                 00008800
          02 COMM-DATE-JJ-MM-SSAA             PIC X(10).                00008900
      *   DIFFERENTES FORMES DE DATE                                    00009000
          02 COMM-DATE-SEMSS                  PIC X(02).                00009100
          02 COMM-DATE-SEMAA                  PIC X(02).                00009100
          02 COMM-DATE-SEMNU                  PIC X(02).                00009100
          02 COMM-DATE-FILLER                 PIC X(08).                00009100
      *                                                                 00009200
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00009300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS                   PIC S9(4) COMP VALUE -1.  00009400
      *--                                                                       
          02 COMM-SWAP-CURS                   PIC S9(4) COMP-5 VALUE -1.        
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150        PIC X(1).                 00009500
      *                                                                 00009600
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00010000
      *                                                                 00410100
      *            TRANSACTION SG00 : ADMINISTRATION DES DONNEES      * 00411000
      *                                                                         
3724      02 COMM-SG00-APPLI.                                           00420000
      *                                                                 00412000
      *------ZONES MAJEURES DU MENU SG00                                00412000
             03 COMM-SG00-INIT.                                                 
                05 COMM-SG00-CACID            PIC X(08).                        
                05 COMM-SG00-NENTITE          PIC X(05).                        
                05 COMM-SG00-NSOC             PIC X(03).                        
                05 COMM-SG00-LSOC             PIC X(25).                        
                05 COMM-SG00-NETAB            PIC X(03).                        
                05 COMM-SG00-LETAB            PIC X(25).                        
                05 COMM-SG00-COMPTES OCCURS 5.                                  
                   10 COMM-SG00-LIGNE1        PIC X(6).                         
                   10 COMM-SG00-LIGNE2        PIC X(6).                         
      *                                                                         
                05 COMM-SG00-CPCG             PIC X(04).                        
                05 COMM-SG00-CPDS             PIC X(04).                        
                05 COMM-SG00-CPDR             PIC X(04).                        
                05 FILLER                     PIC X(38).                        
      *                                                                         
             03 COMM-SG00-OPTION.                                               
                05 COMM-SG00-RUBRIQUE         PIC X(03).                        
                05 COMM-SG00-TYPCONT          PIC X(02).                        
                05 COMM-SG00-CGROUPE          PIC X(03).                        
                05 COMM-SG00-SECPAIE          PIC X(06).                        
                05 COMM-SG00-TRI2             PIC X(01).                        
                05 COMM-SG00-COMPTE           PIC X(06).                        
                05 COMM-SG00-ETBADM           PIC X(03).                        
                05 COMM-SG00-TRI3             PIC X(01).                        
      *                                                                         
      *------------------------------ ZONE COMMUNE AUX SOUS-PROGRAMMES  00510000
             03 COMM-SG00-SSPRGS              PIC X(3520).              00520000
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE TSG01             00510000
             03 COMM-SG01-APPLI          REDEFINES COMM-SG00-SSPRGS.    00520000
                05 COMM-SG01-PAGE             PIC 9(03).                        
                05 COMM-SG01-PAGEMAX          PIC 9(03).                        
                05 FILLER                     PIC X(48).                        
                05 COMM-SG01-PF12-MODIF       PIC X(01).                        
                05 COMM-SG01-PF12.                                              
                   07 COMM-SG01-NLIGNE        PIC 9(02).                        
                   07 COMM-SG01-RUBPAIE       PIC X(03).                        
                   07 COMM-SG01-CONTRAT       PIC X(02).                        
                   07 COMM-SG01-COMPTE        PIC X(06).                        
                   07 COMM-SG01-SENS          PIC X(01).                        
                   07 COMM-SG01-SECTION       PIC X(03).                        
                   07 COMM-SG01-RUBRIQUE      PIC X(06).                        
                   07 COMM-SG01-COMPLEMENT.                                     
                      10 COMM-SG01-CPTE-COMP  PIC X(06).                        
                      10 COMM-SG01-SENS-COMP  PIC X(01).                        
                      10 COMM-SG01-SEC-COMP   PIC X(03).                        
                      10 COMM-SG01-RUBR-COMP  PIC X(06).                        
                   07 COMM-SG01-LRUBPAIE      PIC X(25).                        
                05 FILLER                     PIC X(3401).                      
      *                                                                         
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE TSG02             00510000
             03 COMM-SG02-APPLI          REDEFINES COMM-SG00-SSPRGS.    00520000
                05 COMM-SG02-PAGE             PIC 9(02).                        
                05 COMM-SG02-PAGEMAX          PIC 9(02).                        
                05 FILLER                     PIC X(3516).                      
      *                                                                         
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE TSG03             00510000
             03 COMM-SG03-APPLI          REDEFINES COMM-SG00-SSPRGS.    00520000
                05 COMM-SG03-PAGE             PIC 9(02).                        
                05 COMM-SG03-PAGEMAX          PIC 9(02).                        
                05 FILLER                     PIC X(3516).                      
      *                                                                         
      *                                                                         
      *------------------------------ ZONE SPECIFIQUE MSG10             00510000
             03 COMM-SG10-APPLI          REDEFINES COMM-SG00-SSPRGS.    00520000
                05 COMM-SG10-RUB1             PIC X(03).                        
                05 COMM-SG10-RUB2             PIC X(03).                        
                05 COMM-SG10-CONTRAT          PIC X(02).                        
                05 COMM-SG10-MESSAGE          PIC X(70).                        
                05 COMM-SG10-CIMP             PIC X(04).                        
                05 FILLER                     PIC X(3438).                      
                                                                                
