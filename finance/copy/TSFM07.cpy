      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM07-LONG             PIC S9(4) COMP-3 VALUE +490.                
       01  TS-FM07-RECORD.                                                      
           05 TS-FM07-LIGNE         OCCURS 14.                                  
              10 TS-FM07-NAUX       PIC X(3).                                   
              10 TS-FM07-LAUX       PIC X(30).                                  
              10 TS-FM07-WLETTRE    PIC X.                                      
              10 TS-FM07-WRELANCE   PIC X.                                      
                                                                                
