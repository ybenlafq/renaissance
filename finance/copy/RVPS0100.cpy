      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPS0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPS0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPS0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPS0100.                                                            
      *}                                                                        
           02  PS01-NSOC                                                        
               PIC X(0003).                                                     
           02  PS01-CGCPLT                                                      
               PIC X(0005).                                                     
           02  PS01-NOSAV                                                       
               PIC X(0003).                                                     
           02  PS01-CSECTEUR                                                    
               PIC X(0005).                                                     
           02  PS01-DEFFET                                                      
               PIC X(0006).                                                     
           02  PS01-NBPSE                                                       
               PIC S9(7) COMP-3.                                                
           02  PS01-MTTC                                                        
               PIC S9(9)V9(0006) COMP-3.                                        
           02  PS01-MTHT                                                        
               PIC S9(9)V9(0006) COMP-3.                                        
           02  PS01-NBPSER                                                      
               PIC S9(7) COMP-3.                                                
           02  PS01-MTTCR                                                       
               PIC S9(9)V9(0006) COMP-3.                                        
           02  PS01-MTHTR                                                       
               PIC S9(9)V9(0006) COMP-3.                                        
           02  PS01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPS0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPS0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPS0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-CGCPLT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-CGCPLT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-NOSAV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-NOSAV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-CSECTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-CSECTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-NBPSE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-NBPSE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-MTTC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-MTTC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-MTHT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-MTHT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-NBPSER-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-NBPSER-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-MTTCR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-MTTCR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-MTHTR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-MTHTR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PS01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PS01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
