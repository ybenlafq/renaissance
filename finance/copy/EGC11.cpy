      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu FICHE DE COMMISSION                                        00000020
      ***************************************************************** 00000030
       01   EGC11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFICHEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFICHEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFICHEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFICHEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFICHEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MFICHEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFICHEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFICHEMAXI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBFICHEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBFICHEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBFICHEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBFICHEI     PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPAGEI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPAGEMAXI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR1L      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLIBVAR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR1F      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBVAR1I      PIC X(11).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODEL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCODEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCODEF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCODEI   PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNCODEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLNCODEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNCODEI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR5L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIBVAR5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR5F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBVAR5I      PIC X(11).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODE2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCODE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODE2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCODE2I  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNCODE2L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLNCODE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNCODE2F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLNCODE2I      PIC X(17).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR2L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLIBVAR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR2F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBVAR2I      PIC X(11).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCFAMI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLFAMI    PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR6L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLIBVAR6L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR6F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBVAR6I      PIC X(11).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM2L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM2F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCFAM2I   PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAM2L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAM2F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLFAM2I   PIC X(17).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR3L      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLIBVAR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR3F      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBVAR3I      PIC X(11).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTYPEI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLTYPEI   PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR7L      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MLIBVAR7L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR7F      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLIBVAR7I      PIC X(11).                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPE2L   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MTYPE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTYPE2F   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTYPE2I   PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPE2L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLTYPE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTYPE2F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLTYPE2I  PIC X(17).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR4L      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MLIBVAR4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR4F      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBVAR4I      PIC X(11).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCMARQI   PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLMARQI   PIC X(20).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAR8L      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MLIBVAR8L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAR8F      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBVAR8I      PIC X(11).                                 00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ2L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCMARQ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ2F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCMARQ2I  PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQ2L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MLMARQ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQ2F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLMARQ2I  PIC X(17).                                      00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCND2L     COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MCTYPCND2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPCND2F     PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MCTYPCND2I     PIC X(5).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMENT2L    COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MLCOMMENT2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCOMMENT2F    PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLCOMMENT2I    PIC X(30).                                 00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMONTANT2L     COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MMONTANT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MMONTANT2F     PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MMONTANT2I     PIC X(9).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFET2L      COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MDEFFET2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFET2F      PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MDEFFET2I      PIC X(8).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFET2L   COMP PIC S9(4).                            00001460
      *--                                                                       
           02 MDFINEFFET2L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MDFINEFFET2F   PIC X.                                     00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MDFINEFFET2I   PIC X(8).                                  00001490
           02 MTABI OCCURS   9 TIMES .                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00001510
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MXI     PIC X.                                          00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEL  COMP PIC S9(4).                                 00001550
      *--                                                                       
             03 MCODEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCODEF  PIC X.                                          00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MCODEI  PIC X(3).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPCNDL    COMP PIC S9(4).                            00001590
      *--                                                                       
             03 MCTYPCNDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTYPCNDF    PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MCTYPCNDI    PIC X(5).                                  00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00001630
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MLIBELLEI    PIC X(30).                                 00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODELL      COMP PIC S9(4).                            00001670
      *--                                                                       
             03 MCODELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODELF      PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MCODELI      PIC X(3).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTANTL    COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MMONTANTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMONTANTF    PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MMONTANTI    PIC X(9).                                  00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001750
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MDEFFETI     PIC X(8).                                  00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFETL  COMP PIC S9(4).                            00001790
      *--                                                                       
             03 MDFINEFFETL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDFINEFFETF  PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 MDFINEFFETI  PIC X(8).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MZONCMDI  PIC X(15).                                      00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MLIBERRI  PIC X(58).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MCODTRAI  PIC X(4).                                       00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MCICSI    PIC X(5).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MNETNAMI  PIC X(8).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MSCREENI  PIC X(4).                                       00002060
      ***************************************************************** 00002070
      * menu FICHE DE COMMISSION                                        00002080
      ***************************************************************** 00002090
       01   EGC11O REDEFINES EGC11I.                                    00002100
           02 FILLER    PIC X(12).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDATJOUA  PIC X.                                          00002130
           02 MDATJOUC  PIC X.                                          00002140
           02 MDATJOUP  PIC X.                                          00002150
           02 MDATJOUH  PIC X.                                          00002160
           02 MDATJOUV  PIC X.                                          00002170
           02 MDATJOUO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MTIMJOUA  PIC X.                                          00002200
           02 MTIMJOUC  PIC X.                                          00002210
           02 MTIMJOUP  PIC X.                                          00002220
           02 MTIMJOUH  PIC X.                                          00002230
           02 MTIMJOUV  PIC X.                                          00002240
           02 MTIMJOUO  PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MFICHEA   PIC X.                                          00002270
           02 MFICHEC   PIC X.                                          00002280
           02 MFICHEP   PIC X.                                          00002290
           02 MFICHEH   PIC X.                                          00002300
           02 MFICHEV   PIC X.                                          00002310
           02 MFICHEO   PIC X(3).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MFICHEMAXA     PIC X.                                     00002340
           02 MFICHEMAXC     PIC X.                                     00002350
           02 MFICHEMAXP     PIC X.                                     00002360
           02 MFICHEMAXH     PIC X.                                     00002370
           02 MFICHEMAXV     PIC X.                                     00002380
           02 MFICHEMAXO     PIC X(3).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIBFICHEA     PIC X.                                     00002410
           02 MLIBFICHEC     PIC X.                                     00002420
           02 MLIBFICHEP     PIC X.                                     00002430
           02 MLIBFICHEH     PIC X.                                     00002440
           02 MLIBFICHEV     PIC X.                                     00002450
           02 MLIBFICHEO     PIC X(20).                                 00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MPAGEA    PIC X.                                          00002480
           02 MPAGEC    PIC X.                                          00002490
           02 MPAGEP    PIC X.                                          00002500
           02 MPAGEH    PIC X.                                          00002510
           02 MPAGEV    PIC X.                                          00002520
           02 MPAGEO    PIC X(3).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MPAGEMAXA      PIC X.                                     00002550
           02 MPAGEMAXC PIC X.                                          00002560
           02 MPAGEMAXP PIC X.                                          00002570
           02 MPAGEMAXH PIC X.                                          00002580
           02 MPAGEMAXV PIC X.                                          00002590
           02 MPAGEMAXO      PIC X(3).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLIBVAR1A      PIC X.                                     00002620
           02 MLIBVAR1C PIC X.                                          00002630
           02 MLIBVAR1P PIC X.                                          00002640
           02 MLIBVAR1H PIC X.                                          00002650
           02 MLIBVAR1V PIC X.                                          00002660
           02 MLIBVAR1O      PIC X(11).                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNCODEA   PIC X.                                          00002690
           02 MNCODEC   PIC X.                                          00002700
           02 MNCODEP   PIC X.                                          00002710
           02 MNCODEH   PIC X.                                          00002720
           02 MNCODEV   PIC X.                                          00002730
           02 MNCODEO   PIC X(7).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLNCODEA  PIC X.                                          00002760
           02 MLNCODEC  PIC X.                                          00002770
           02 MLNCODEP  PIC X.                                          00002780
           02 MLNCODEH  PIC X.                                          00002790
           02 MLNCODEV  PIC X.                                          00002800
           02 MLNCODEO  PIC X(20).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLIBVAR5A      PIC X.                                     00002830
           02 MLIBVAR5C PIC X.                                          00002840
           02 MLIBVAR5P PIC X.                                          00002850
           02 MLIBVAR5H PIC X.                                          00002860
           02 MLIBVAR5V PIC X.                                          00002870
           02 MLIBVAR5O      PIC X(11).                                 00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNCODE2A  PIC X.                                          00002900
           02 MNCODE2C  PIC X.                                          00002910
           02 MNCODE2P  PIC X.                                          00002920
           02 MNCODE2H  PIC X.                                          00002930
           02 MNCODE2V  PIC X.                                          00002940
           02 MNCODE2O  PIC X(7).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLNCODE2A      PIC X.                                     00002970
           02 MLNCODE2C PIC X.                                          00002980
           02 MLNCODE2P PIC X.                                          00002990
           02 MLNCODE2H PIC X.                                          00003000
           02 MLNCODE2V PIC X.                                          00003010
           02 MLNCODE2O      PIC X(17).                                 00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MLIBVAR2A      PIC X.                                     00003040
           02 MLIBVAR2C PIC X.                                          00003050
           02 MLIBVAR2P PIC X.                                          00003060
           02 MLIBVAR2H PIC X.                                          00003070
           02 MLIBVAR2V PIC X.                                          00003080
           02 MLIBVAR2O      PIC X(11).                                 00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCFAMA    PIC X.                                          00003110
           02 MCFAMC    PIC X.                                          00003120
           02 MCFAMP    PIC X.                                          00003130
           02 MCFAMH    PIC X.                                          00003140
           02 MCFAMV    PIC X.                                          00003150
           02 MCFAMO    PIC X(5).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLFAMA    PIC X.                                          00003180
           02 MLFAMC    PIC X.                                          00003190
           02 MLFAMP    PIC X.                                          00003200
           02 MLFAMH    PIC X.                                          00003210
           02 MLFAMV    PIC X.                                          00003220
           02 MLFAMO    PIC X(20).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLIBVAR6A      PIC X.                                     00003250
           02 MLIBVAR6C PIC X.                                          00003260
           02 MLIBVAR6P PIC X.                                          00003270
           02 MLIBVAR6H PIC X.                                          00003280
           02 MLIBVAR6V PIC X.                                          00003290
           02 MLIBVAR6O      PIC X(11).                                 00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCFAM2A   PIC X.                                          00003320
           02 MCFAM2C   PIC X.                                          00003330
           02 MCFAM2P   PIC X.                                          00003340
           02 MCFAM2H   PIC X.                                          00003350
           02 MCFAM2V   PIC X.                                          00003360
           02 MCFAM2O   PIC X(5).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLFAM2A   PIC X.                                          00003390
           02 MLFAM2C   PIC X.                                          00003400
           02 MLFAM2P   PIC X.                                          00003410
           02 MLFAM2H   PIC X.                                          00003420
           02 MLFAM2V   PIC X.                                          00003430
           02 MLFAM2O   PIC X(17).                                      00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MLIBVAR3A      PIC X.                                     00003460
           02 MLIBVAR3C PIC X.                                          00003470
           02 MLIBVAR3P PIC X.                                          00003480
           02 MLIBVAR3H PIC X.                                          00003490
           02 MLIBVAR3V PIC X.                                          00003500
           02 MLIBVAR3O      PIC X(11).                                 00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MTYPEA    PIC X.                                          00003530
           02 MTYPEC    PIC X.                                          00003540
           02 MTYPEP    PIC X.                                          00003550
           02 MTYPEH    PIC X.                                          00003560
           02 MTYPEV    PIC X.                                          00003570
           02 MTYPEO    PIC X(5).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MLTYPEA   PIC X.                                          00003600
           02 MLTYPEC   PIC X.                                          00003610
           02 MLTYPEP   PIC X.                                          00003620
           02 MLTYPEH   PIC X.                                          00003630
           02 MLTYPEV   PIC X.                                          00003640
           02 MLTYPEO   PIC X(20).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MLIBVAR7A      PIC X.                                     00003670
           02 MLIBVAR7C PIC X.                                          00003680
           02 MLIBVAR7P PIC X.                                          00003690
           02 MLIBVAR7H PIC X.                                          00003700
           02 MLIBVAR7V PIC X.                                          00003710
           02 MLIBVAR7O      PIC X(11).                                 00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MTYPE2A   PIC X.                                          00003740
           02 MTYPE2C   PIC X.                                          00003750
           02 MTYPE2P   PIC X.                                          00003760
           02 MTYPE2H   PIC X.                                          00003770
           02 MTYPE2V   PIC X.                                          00003780
           02 MTYPE2O   PIC X(5).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MLTYPE2A  PIC X.                                          00003810
           02 MLTYPE2C  PIC X.                                          00003820
           02 MLTYPE2P  PIC X.                                          00003830
           02 MLTYPE2H  PIC X.                                          00003840
           02 MLTYPE2V  PIC X.                                          00003850
           02 MLTYPE2O  PIC X(17).                                      00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MLIBVAR4A      PIC X.                                     00003880
           02 MLIBVAR4C PIC X.                                          00003890
           02 MLIBVAR4P PIC X.                                          00003900
           02 MLIBVAR4H PIC X.                                          00003910
           02 MLIBVAR4V PIC X.                                          00003920
           02 MLIBVAR4O      PIC X(11).                                 00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MCMARQA   PIC X.                                          00003950
           02 MCMARQC   PIC X.                                          00003960
           02 MCMARQP   PIC X.                                          00003970
           02 MCMARQH   PIC X.                                          00003980
           02 MCMARQV   PIC X.                                          00003990
           02 MCMARQO   PIC X(5).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MLMARQA   PIC X.                                          00004020
           02 MLMARQC   PIC X.                                          00004030
           02 MLMARQP   PIC X.                                          00004040
           02 MLMARQH   PIC X.                                          00004050
           02 MLMARQV   PIC X.                                          00004060
           02 MLMARQO   PIC X(20).                                      00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MLIBVAR8A      PIC X.                                     00004090
           02 MLIBVAR8C PIC X.                                          00004100
           02 MLIBVAR8P PIC X.                                          00004110
           02 MLIBVAR8H PIC X.                                          00004120
           02 MLIBVAR8V PIC X.                                          00004130
           02 MLIBVAR8O      PIC X(11).                                 00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MCMARQ2A  PIC X.                                          00004160
           02 MCMARQ2C  PIC X.                                          00004170
           02 MCMARQ2P  PIC X.                                          00004180
           02 MCMARQ2H  PIC X.                                          00004190
           02 MCMARQ2V  PIC X.                                          00004200
           02 MCMARQ2O  PIC X(5).                                       00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MLMARQ2A  PIC X.                                          00004230
           02 MLMARQ2C  PIC X.                                          00004240
           02 MLMARQ2P  PIC X.                                          00004250
           02 MLMARQ2H  PIC X.                                          00004260
           02 MLMARQ2V  PIC X.                                          00004270
           02 MLMARQ2O  PIC X(17).                                      00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MCTYPCND2A     PIC X.                                     00004300
           02 MCTYPCND2C     PIC X.                                     00004310
           02 MCTYPCND2P     PIC X.                                     00004320
           02 MCTYPCND2H     PIC X.                                     00004330
           02 MCTYPCND2V     PIC X.                                     00004340
           02 MCTYPCND2O     PIC X(5).                                  00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MLCOMMENT2A    PIC X.                                     00004370
           02 MLCOMMENT2C    PIC X.                                     00004380
           02 MLCOMMENT2P    PIC X.                                     00004390
           02 MLCOMMENT2H    PIC X.                                     00004400
           02 MLCOMMENT2V    PIC X.                                     00004410
           02 MLCOMMENT2O    PIC X(30).                                 00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MMONTANT2A     PIC X.                                     00004440
           02 MMONTANT2C     PIC X.                                     00004450
           02 MMONTANT2P     PIC X.                                     00004460
           02 MMONTANT2H     PIC X.                                     00004470
           02 MMONTANT2V     PIC X.                                     00004480
           02 MMONTANT2O     PIC -----9,99.                             00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MDEFFET2A      PIC X.                                     00004510
           02 MDEFFET2C PIC X.                                          00004520
           02 MDEFFET2P PIC X.                                          00004530
           02 MDEFFET2H PIC X.                                          00004540
           02 MDEFFET2V PIC X.                                          00004550
           02 MDEFFET2O      PIC X(8).                                  00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MDFINEFFET2A   PIC X.                                     00004580
           02 MDFINEFFET2C   PIC X.                                     00004590
           02 MDFINEFFET2P   PIC X.                                     00004600
           02 MDFINEFFET2H   PIC X.                                     00004610
           02 MDFINEFFET2V   PIC X.                                     00004620
           02 MDFINEFFET2O   PIC X(8).                                  00004630
           02 MTABO OCCURS   9 TIMES .                                  00004640
             03 FILLER       PIC X(2).                                  00004650
             03 MXA     PIC X.                                          00004660
             03 MXC     PIC X.                                          00004670
             03 MXP     PIC X.                                          00004680
             03 MXH     PIC X.                                          00004690
             03 MXV     PIC X.                                          00004700
             03 MXO     PIC X.                                          00004710
             03 FILLER       PIC X(2).                                  00004720
             03 MCODEA  PIC X.                                          00004730
             03 MCODEC  PIC X.                                          00004740
             03 MCODEP  PIC X.                                          00004750
             03 MCODEH  PIC X.                                          00004760
             03 MCODEV  PIC X.                                          00004770
             03 MCODEO  PIC X(3).                                       00004780
             03 FILLER       PIC X(2).                                  00004790
             03 MCTYPCNDA    PIC X.                                     00004800
             03 MCTYPCNDC    PIC X.                                     00004810
             03 MCTYPCNDP    PIC X.                                     00004820
             03 MCTYPCNDH    PIC X.                                     00004830
             03 MCTYPCNDV    PIC X.                                     00004840
             03 MCTYPCNDO    PIC X(5).                                  00004850
             03 FILLER       PIC X(2).                                  00004860
             03 MLIBELLEA    PIC X.                                     00004870
             03 MLIBELLEC    PIC X.                                     00004880
             03 MLIBELLEP    PIC X.                                     00004890
             03 MLIBELLEH    PIC X.                                     00004900
             03 MLIBELLEV    PIC X.                                     00004910
             03 MLIBELLEO    PIC X(30).                                 00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MCODELA      PIC X.                                     00004940
             03 MCODELC PIC X.                                          00004950
             03 MCODELP PIC X.                                          00004960
             03 MCODELH PIC X.                                          00004970
             03 MCODELV PIC X.                                          00004980
             03 MCODELO      PIC X(3).                                  00004990
             03 FILLER       PIC X(2).                                  00005000
             03 MMONTANTA    PIC X.                                     00005010
             03 MMONTANTC    PIC X.                                     00005020
             03 MMONTANTP    PIC X.                                     00005030
             03 MMONTANTH    PIC X.                                     00005040
             03 MMONTANTV    PIC X.                                     00005050
             03 MMONTANTO    PIC -----9,99.                             00005060
             03 FILLER       PIC X(2).                                  00005070
             03 MDEFFETA     PIC X.                                     00005080
             03 MDEFFETC     PIC X.                                     00005090
             03 MDEFFETP     PIC X.                                     00005100
             03 MDEFFETH     PIC X.                                     00005110
             03 MDEFFETV     PIC X.                                     00005120
             03 MDEFFETO     PIC X(8).                                  00005130
             03 FILLER       PIC X(2).                                  00005140
             03 MDFINEFFETA  PIC X.                                     00005150
             03 MDFINEFFETC  PIC X.                                     00005160
             03 MDFINEFFETP  PIC X.                                     00005170
             03 MDFINEFFETH  PIC X.                                     00005180
             03 MDFINEFFETV  PIC X.                                     00005190
             03 MDFINEFFETO  PIC X(8).                                  00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MZONCMDA  PIC X.                                          00005220
           02 MZONCMDC  PIC X.                                          00005230
           02 MZONCMDP  PIC X.                                          00005240
           02 MZONCMDH  PIC X.                                          00005250
           02 MZONCMDV  PIC X.                                          00005260
           02 MZONCMDO  PIC X(15).                                      00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MLIBERRA  PIC X.                                          00005290
           02 MLIBERRC  PIC X.                                          00005300
           02 MLIBERRP  PIC X.                                          00005310
           02 MLIBERRH  PIC X.                                          00005320
           02 MLIBERRV  PIC X.                                          00005330
           02 MLIBERRO  PIC X(58).                                      00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MCODTRAA  PIC X.                                          00005360
           02 MCODTRAC  PIC X.                                          00005370
           02 MCODTRAP  PIC X.                                          00005380
           02 MCODTRAH  PIC X.                                          00005390
           02 MCODTRAV  PIC X.                                          00005400
           02 MCODTRAO  PIC X(4).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MCICSA    PIC X.                                          00005430
           02 MCICSC    PIC X.                                          00005440
           02 MCICSP    PIC X.                                          00005450
           02 MCICSH    PIC X.                                          00005460
           02 MCICSV    PIC X.                                          00005470
           02 MCICSO    PIC X(5).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MNETNAMA  PIC X.                                          00005500
           02 MNETNAMC  PIC X.                                          00005510
           02 MNETNAMP  PIC X.                                          00005520
           02 MNETNAMH  PIC X.                                          00005530
           02 MNETNAMV  PIC X.                                          00005540
           02 MNETNAMO  PIC X(8).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MSCREENA  PIC X.                                          00005570
           02 MSCREENC  PIC X.                                          00005580
           02 MSCREENP  PIC X.                                          00005590
           02 MSCREENH  PIC X.                                          00005600
           02 MSCREENV  PIC X.                                          00005610
           02 MSCREENO  PIC X(4).                                       00005620
                                                                                
