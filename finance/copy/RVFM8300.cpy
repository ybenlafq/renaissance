      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM8300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8300.                                                            
           02  FM83-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  FM83-CTYPOPER                                                    
               PIC X(0005).                                                     
           02  FM83-NJRN                                                        
               PIC X(0003).                                                     
           02  FM83-CNATURE                                                     
               PIC X(0003).                                                     
           02  FM83-CPCG                                                        
               PIC X(0004).                                                     
           02  FM83-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM83-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM8300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-CINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-CTYPOPER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-CTYPOPER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-CNATURE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-CNATURE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-CPCG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-CPCG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM83-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM83-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
