      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FM05-LONG             PIC S9(4) COMP-3 VALUE +290.                
       01  TS-FM05-RECORD.                                                      
           05 TS-FM05-CPTEG         PIC X(6).                                   
           05 TS-FM05-SECTG         PIC X(6).                                   
           05 TS-FM05-RUBRG         PIC X(6).                                   
           05 TS-FM05-CPTEP         PIC X(6).                                   
           05 TS-FM05-SECTP         PIC X(6).                                   
           05 TS-FM05-RUBRP         PIC X(6).                                   
           05 TS-FM05-CPT-OBLI      PIC X.                                      
           05 TS-FM05-LIGNE         OCCURS 11.                                  
              10 TS-FM05-CDEVDEST PIC X(3).                                     
              10 TS-FM05-COPERAT  PIC X.                                        
              10 TS-FM05-PTAUX    PIC S9(5)V9(5) COMP-3.                        
              10 TS-FM05-DEFFET   PIC X(8).                                     
                                                                                
