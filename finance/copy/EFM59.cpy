      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - COMPOSITION LIBELLE                                       00000020
      ***************************************************************** 00000030
       01   EFM59I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTYPF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTYPI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCNATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCNATF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCNATI    PIC X(5).                                       00000390
           02 MTABI OCCURS   5 TIMES .                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPOPERL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MCTYPOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPOPERF   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MCTYPOPERI   PIC X(5).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNATOPERL   COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCNATOPERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCNATOPERF   PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCNATOPERI   PIC X(5).                                  00000480
             03 MZ1D OCCURS   5 TIMES .                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MZ1L  COMP PIC S9(4).                                 00000500
      *--                                                                       
               04 MZ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
               04 MZ1F  PIC X.                                          00000510
               04 FILLER     PIC X(4).                                  00000520
               04 MZ1I  PIC 99.                                         00000530
             03 MZ2D OCCURS   5 TIMES .                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MZ2L  COMP PIC S9(4).                                 00000550
      *--                                                                       
               04 MZ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
               04 MZ2F  PIC X.                                          00000560
               04 FILLER     PIC X(4).                                  00000570
               04 MZ2I  PIC 99.                                         00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLE1L   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLIBELLE1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIBELLE1F   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLIBELLE1I   PIC X(25).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLE2L   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLIBELLE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIBELLE2F   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLIBELLE2I   PIC X(25).                                 00000660
      * ZONE CMD AIDA                                                   00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLIBERRI  PIC X(79).                                      00000710
      * CODE TRANSACTION                                                00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCODTRAI  PIC X(4).                                       00000760
      * CICS DE TRAVAIL                                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      * NETNAME                                                         00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      * CODE TERMINAL                                                   00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MSCREENI  PIC X(4).                                       00000910
      ***************************************************************** 00000920
      * GCT - COMPOSITION LIBELLE                                       00000930
      ***************************************************************** 00000940
       01   EFM59O REDEFINES EFM59I.                                    00000950
           02 FILLER    PIC X(12).                                      00000960
      * DATE DU JOUR                                                    00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MDATJOUA  PIC X.                                          00000990
           02 MDATJOUC  PIC X.                                          00001000
           02 MDATJOUP  PIC X.                                          00001010
           02 MDATJOUH  PIC X.                                          00001020
           02 MDATJOUV  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
      * HEURE                                                           00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEA    PIC X.                                          00001140
           02 MPAGEC    PIC X.                                          00001150
           02 MPAGEP    PIC X.                                          00001160
           02 MPAGEH    PIC X.                                          00001170
           02 MPAGEV    PIC X.                                          00001180
           02 MPAGEO    PIC X(2).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNBPA     PIC X.                                          00001210
           02 MNBPC     PIC X.                                          00001220
           02 MNBPP     PIC X.                                          00001230
           02 MNBPH     PIC X.                                          00001240
           02 MNBPV     PIC X.                                          00001250
           02 MNBPO     PIC X(2).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCINTERFACEA   PIC X.                                     00001280
           02 MCINTERFACEC   PIC X.                                     00001290
           02 MCINTERFACEP   PIC X.                                     00001300
           02 MCINTERFACEH   PIC X.                                     00001310
           02 MCINTERFACEV   PIC X.                                     00001320
           02 MCINTERFACEO   PIC X(5).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLINTERFACEA   PIC X.                                     00001350
           02 MLINTERFACEC   PIC X.                                     00001360
           02 MLINTERFACEP   PIC X.                                     00001370
           02 MLINTERFACEH   PIC X.                                     00001380
           02 MLINTERFACEV   PIC X.                                     00001390
           02 MLINTERFACEO   PIC X(30).                                 00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCTYPA    PIC X.                                          00001420
           02 MCTYPC    PIC X.                                          00001430
           02 MCTYPP    PIC X.                                          00001440
           02 MCTYPH    PIC X.                                          00001450
           02 MCTYPV    PIC X.                                          00001460
           02 MCTYPO    PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCNATA    PIC X.                                          00001490
           02 MCNATC    PIC X.                                          00001500
           02 MCNATP    PIC X.                                          00001510
           02 MCNATH    PIC X.                                          00001520
           02 MCNATV    PIC X.                                          00001530
           02 MCNATO    PIC X(5).                                       00001540
           02 MTABO OCCURS   5 TIMES .                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MCTYPOPERA   PIC X.                                     00001570
             03 MCTYPOPERC   PIC X.                                     00001580
             03 MCTYPOPERP   PIC X.                                     00001590
             03 MCTYPOPERH   PIC X.                                     00001600
             03 MCTYPOPERV   PIC X.                                     00001610
             03 MCTYPOPERO   PIC X(5).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MCNATOPERA   PIC X.                                     00001640
             03 MCNATOPERC   PIC X.                                     00001650
             03 MCNATOPERP   PIC X.                                     00001660
             03 MCNATOPERH   PIC X.                                     00001670
             03 MCNATOPERV   PIC X.                                     00001680
             03 MCNATOPERO   PIC X(5).                                  00001690
             03 DFHMS1 OCCURS   5 TIMES .                               00001700
               04 FILLER     PIC X(2).                                  00001710
               04 MZ1A  PIC X.                                          00001720
               04 MZ1C  PIC X.                                          00001730
               04 MZ1P  PIC X.                                          00001740
               04 MZ1H  PIC X.                                          00001750
               04 MZ1V  PIC X.                                          00001760
               04 MZ1O  PIC ZZ.                                         00001770
             03 DFHMS2 OCCURS   5 TIMES .                               00001780
               04 FILLER     PIC X(2).                                  00001790
               04 MZ2A  PIC X.                                          00001800
               04 MZ2C  PIC X.                                          00001810
               04 MZ2P  PIC X.                                          00001820
               04 MZ2H  PIC X.                                          00001830
               04 MZ2V  PIC X.                                          00001840
               04 MZ2O  PIC ZZ.                                         00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MLIBELLE1A   PIC X.                                     00001870
             03 MLIBELLE1C   PIC X.                                     00001880
             03 MLIBELLE1P   PIC X.                                     00001890
             03 MLIBELLE1H   PIC X.                                     00001900
             03 MLIBELLE1V   PIC X.                                     00001910
             03 MLIBELLE1O   PIC X(25).                                 00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MLIBELLE2A   PIC X.                                     00001940
             03 MLIBELLE2C   PIC X.                                     00001950
             03 MLIBELLE2P   PIC X.                                     00001960
             03 MLIBELLE2H   PIC X.                                     00001970
             03 MLIBELLE2V   PIC X.                                     00001980
             03 MLIBELLE2O   PIC X(25).                                 00001990
      * ZONE CMD AIDA                                                   00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLIBERRA  PIC X.                                          00002020
           02 MLIBERRC  PIC X.                                          00002030
           02 MLIBERRP  PIC X.                                          00002040
           02 MLIBERRH  PIC X.                                          00002050
           02 MLIBERRV  PIC X.                                          00002060
           02 MLIBERRO  PIC X(79).                                      00002070
      * CODE TRANSACTION                                                00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCODTRAA  PIC X.                                          00002100
           02 MCODTRAC  PIC X.                                          00002110
           02 MCODTRAP  PIC X.                                          00002120
           02 MCODTRAH  PIC X.                                          00002130
           02 MCODTRAV  PIC X.                                          00002140
           02 MCODTRAO  PIC X(4).                                       00002150
      * CICS DE TRAVAIL                                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCICSA    PIC X.                                          00002180
           02 MCICSC    PIC X.                                          00002190
           02 MCICSP    PIC X.                                          00002200
           02 MCICSH    PIC X.                                          00002210
           02 MCICSV    PIC X.                                          00002220
           02 MCICSO    PIC X(5).                                       00002230
      * NETNAME                                                         00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNETNAMA  PIC X.                                          00002260
           02 MNETNAMC  PIC X.                                          00002270
           02 MNETNAMP  PIC X.                                          00002280
           02 MNETNAMH  PIC X.                                          00002290
           02 MNETNAMV  PIC X.                                          00002300
           02 MNETNAMO  PIC X(8).                                       00002310
      * CODE TERMINAL                                                   00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MSCREENA  PIC X.                                          00002340
           02 MSCREENC  PIC X.                                          00002350
           02 MSCREENP  PIC X.                                          00002360
           02 MSCREENH  PIC X.                                          00002370
           02 MSCREENV  PIC X.                                          00002380
           02 MSCREENO  PIC X(4).                                       00002390
                                                                                
