      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
AM     01  TS-FM10-LONG             PIC S9(4) COMP-3 VALUE +806.                
       01  TS-FM10-RECORD.                                                      
           05 TS-FM10-LIGNE         OCCURS 13.                                  
              10 TS-FM10-NDEMANDE   PIC X(2).                                   
              10 TS-FM10-CACID      PIC X(8).                                   
              10 TS-FM10-USER       PIC X(25).                                  
AM            10 TS-FM10-NAUXMIN    PIC X(3).                                   
AM            10 TS-FM10-NAUXMAX    PIC X(3).                                   
AM            10 TS-FM10-WTYPAUX    PIC X.                                      
              10 TS-FM10-DCREATION  PIC X(10).                                  
              10 TS-FM10-DMAJ       PIC X(10).                                  
                                                                                
