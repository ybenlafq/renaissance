      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE L'ACCES AU FICHIER FIG01C PAR LE CHEMIN FIG01K           
      ******************************************************************        
       CLEF-FIG01K             SECTION.                                         
           MOVE FIG01-FIG01K TO VSAM-KEY.                                       
           MOVE 'FIG01K'     TO PATH-NAME.                                      
           MOVE 'FIGPRT'     TO FILE-NAME.                                      
           MOVE +100         TO FILE-LONG.                                      
           MOVE FIG01        TO Z-INOUT.                                        
       FIN-CLEF-FIG01K.  EXIT.                                                  
                EJECT                                                           
                                                                                
EMOD                                                                            
