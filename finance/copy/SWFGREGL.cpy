      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *    FACTURES ARRIVEES A ECHEANCE, PAR COMPTE                             
      *                                                                         
      *                                                                         
       01       FFGREGL-DSECT.                                                  
           05   FFGREGL-IDENT.                                                  
001          10 FFGREGL-NSOCCOMPT        PIC X(03).                             
004          10 FFGREGL-NETAB            PIC X(03).                             
007          10 FFGREGL-NSOCCOR          PIC X(03).                             
010          10 FFGREGL-NETABCOR         PIC X(03).                             
013          10 FFGREGL-CODE-ER          PIC X(01).                             
014          10 FFGREGL-NSOCORIG         PIC X(03).                             
017          10 FFGREGL-NLIEUORIG        PIC X(03).                             
020          10 FFGREGL-NSOCDEST         PIC X(03).                             
023          10 FFGREGL-NLIEUDEST        PIC X(03).                             
026          10 FFGREGL-COMPTE           PIC X(06).                             
           05   FFGREGL-SUITE.                                                  
032          10 FFGREGL-DECHEANC         PIC X(08).                             
040          10 FFGREGL-DCOMPTA          PIC X(08).                             
048          10 FFGREGL-DPIECE           PIC X(08).                             
056          10 FFGREGL-SENS             PIC X(01).                             
057          10 FFGREGL-MONTANT          PIC S9(11)V9(2) COMP-3.                
             10 FFGREGL-LIBELLE.                                                
064             15 FFGREGL-NUMF          PIC X(07).                             
071             15 FILLER                PIC X(02) VALUE '  '.                  
073             15 FFGREGL-TYPE          PIC X(02).                             
075             15 FILLER                PIC X(01) VALUE ' '.                   
076          10 FFGREGL-CNAT             PIC X(05).                             
081          10 FFGREGL-FILLER           PIC X(10).                             
->90  *                                                                         
                                                                                
