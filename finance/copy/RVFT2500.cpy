      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFT2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT2500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT2500.                                                            
      *}                                                                        
           02  FT25-NSOC                                                        
               PIC X(0005).                                                     
           02  FT25-NETAB                                                       
               PIC X(0003).                                                     
           02  FT25-NJRN                                                        
               PIC X(0003).                                                     
           02  FT25-LJRN                                                        
               PIC X(0025).                                                     
           02  FT25-WCTRPASS                                                    
               PIC X(0001).                                                     
           02  FT25-WCTRPART                                                    
               PIC X(0001).                                                     
           02  FT25-WNUMAUTO                                                    
               PIC X(0001).                                                     
           02  FT25-WJRNREG                                                     
               PIC X(0001).                                                     
           02  FT25-WEDTCRS                                                     
               PIC X(0001).                                                     
           02  FT25-COMPTE                                                      
               PIC X(0006).                                                     
           02  FT25-SSCOMPTE                                                    
               PIC X(0006).                                                     
           02  FT25-SECTION                                                     
               PIC X(0006).                                                     
           02  FT25-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FT25-STEAPP                                                      
               PIC X(0005).                                                     
           02  FT25-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FT25-NAUX                                                        
               PIC X(0003).                                                     
           02  FT25-WBATCHTP                                                    
               PIC X(0001).                                                     
           02  FT25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT2500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFT2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFT2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-LJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-LJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WCTRPASS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WCTRPASS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WCTRPART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WCTRPART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WNUMAUTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WNUMAUTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WJRNREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WJRNREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WEDTCRS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WEDTCRS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-SSCOMPTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-SSCOMPTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-STEAPP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-STEAPP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-WBATCHTP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-WBATCHTP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
