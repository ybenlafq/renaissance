      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS SAP *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MCG04                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MCG04-LONG-COMMAREA       PIC S9(4) COMP VALUE +409.             
      *--                                                                       
       01 COMM-MCG04-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +409.           
      *}                                                                        
       01 COMM-MCG04-APPLI.                                                     
      *                                                                         
          02 COMM-MCG04-ENTREE.                                                 
      *------------------------------------------------------------09           
             05 COMM-MCG04-PGRM         PIC X(5).                               
             05 COMM-MCG04-NTERMID      PIC X(4).                               
      *      DONNEES GENERALES ----------------------------- 200                
             05 COMM-MCG04-CFONC        PIC X(03).                              
             05 COMM-MCG04-NCODIC       PIC X(07).                              
             05 COMM-FLAG-EAN           PIC X(01) VALUE SPACE.                  
                88 CHANGE-EAN           VALUE '1'.                              
             05 COMM-FLAG-EAN-2         PIC X(01) VALUE SPACE.                  
                88 SUP-EAN              VALUE '1'.                              
             05 TAB-NEAN-SUP       OCCURS 10.                                   
                10 COMM-MCG04-NEAN-SUP  PIC X(13).                              
             05 IND-EAN-SUP-MAX         PIC 9(03).                              
             05 FILLER                  PIC X(55).                              
      *                                                                         
          02 COMM-MCG04-SORTIE.                                                 
      *            MESSAGE DE SORTIE ----------------------- 100                
             05 COMM-MCG04-MESSAGE.                                             
                  10 COMM-MCG04-CODRET              PIC X(1).                   
                     88 COMM-MCG04-OK              VALUE ' '.                   
                     88 COMM-MCG04-ERR             VALUE '1'.                   
                  10 COMM-MCG04-LIBERR             PIC X(58).                   
                  10 FILLER                        PIC X(41).                   
          02  FILLER                               PIC X(100).                  
      *                                                                         
                                                                                
