      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EXT02   EXT02                                              00000020
      ***************************************************************** 00000030
       01   EXT02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCETATI   PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNPAGEI   PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNBPAGESI      PIC X(2).                                  00000270
           02 M59I OCCURS   15 TIMES .                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQPRO1L   COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MWSEQPRO1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWSEQPRO1F   PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MWSEQPRO1I   PIC X(7).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROD1L     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLPROD1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPROD1F     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLPROD1I     PIC X(20).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQCORR1L  COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MWSEQCORR1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MWSEQCORR1F  PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MWSEQCORR1I  PIC X(7).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFLAGLIB1L   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MFLAGLIB1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MFLAGLIB1F   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MFLAGLIB1I   PIC X.                                     00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQPRO2L   COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MWSEQPRO2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWSEQPRO2F   PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MWSEQPRO2I   PIC X(7).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROD2L     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MLPROD2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPROD2F     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MLPROD2I     PIC X(20).                                 00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSEQCORR2L  COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MWSEQCORR2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MWSEQCORR2F  PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MWSEQCORR2I  PIC X(7).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFLAGLIB2L   COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MFLAGLIB2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MFLAGLIB2F   PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MFLAGLIB2I   PIC X.                                     00000600
      * MESSAGE ERREUR                                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(78).                                      00000650
      * CODE TRANSACTION                                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      * CICS DE TRAVAIL                                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCICSI    PIC X(5).                                       00000790
      * NETNAME                                                         00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MNETNAMI  PIC X(8).                                       00000840
      * CODE TERMINAL                                                   00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(5).                                       00000890
      ***************************************************************** 00000900
      * SDF: EXT02   EXT02                                              00000910
      ***************************************************************** 00000920
       01   EXT02O REDEFINES EXT02I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
      * DATE DU JOUR                                                    00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
      * HEURE                                                           00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MTIMJOUA  PIC X.                                          00001050
           02 MTIMJOUC  PIC X.                                          00001060
           02 MTIMJOUP  PIC X.                                          00001070
           02 MTIMJOUH  PIC X.                                          00001080
           02 MTIMJOUV  PIC X.                                          00001090
           02 MTIMJOUO  PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MCETATA   PIC X.                                          00001120
           02 MCETATC   PIC X.                                          00001130
           02 MCETATP   PIC X.                                          00001140
           02 MCETATH   PIC X.                                          00001150
           02 MCETATV   PIC X.                                          00001160
           02 MCETATO   PIC X(10).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNPAGEA   PIC X.                                          00001190
           02 MNPAGEC   PIC X.                                          00001200
           02 MNPAGEP   PIC X.                                          00001210
           02 MNPAGEH   PIC X.                                          00001220
           02 MNPAGEV   PIC X.                                          00001230
           02 MNPAGEO   PIC ZZ.                                         00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNBPAGESA      PIC X.                                     00001260
           02 MNBPAGESC PIC X.                                          00001270
           02 MNBPAGESP PIC X.                                          00001280
           02 MNBPAGESH PIC X.                                          00001290
           02 MNBPAGESV PIC X.                                          00001300
           02 MNBPAGESO      PIC ZZ.                                    00001310
           02 M59O OCCURS   15 TIMES .                                  00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MWSEQPRO1A   PIC X.                                     00001340
             03 MWSEQPRO1C   PIC X.                                     00001350
             03 MWSEQPRO1P   PIC X.                                     00001360
             03 MWSEQPRO1H   PIC X.                                     00001370
             03 MWSEQPRO1V   PIC X.                                     00001380
             03 MWSEQPRO1O   PIC ZZZZZZZ.                               00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MLPROD1A     PIC X.                                     00001410
             03 MLPROD1C     PIC X.                                     00001420
             03 MLPROD1P     PIC X.                                     00001430
             03 MLPROD1H     PIC X.                                     00001440
             03 MLPROD1V     PIC X.                                     00001450
             03 MLPROD1O     PIC X(20).                                 00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MWSEQCORR1A  PIC X.                                     00001480
             03 MWSEQCORR1C  PIC X.                                     00001490
             03 MWSEQCORR1P  PIC X.                                     00001500
             03 MWSEQCORR1H  PIC X.                                     00001510
             03 MWSEQCORR1V  PIC X.                                     00001520
             03 MWSEQCORR1O  PIC ZZZZZZZ.                               00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MFLAGLIB1A   PIC X.                                     00001550
             03 MFLAGLIB1C   PIC X.                                     00001560
             03 MFLAGLIB1P   PIC X.                                     00001570
             03 MFLAGLIB1H   PIC X.                                     00001580
             03 MFLAGLIB1V   PIC X.                                     00001590
             03 MFLAGLIB1O   PIC X.                                     00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MWSEQPRO2A   PIC X.                                     00001620
             03 MWSEQPRO2C   PIC X.                                     00001630
             03 MWSEQPRO2P   PIC X.                                     00001640
             03 MWSEQPRO2H   PIC X.                                     00001650
             03 MWSEQPRO2V   PIC X.                                     00001660
             03 MWSEQPRO2O   PIC ZZZZZZZ.                               00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MLPROD2A     PIC X.                                     00001690
             03 MLPROD2C     PIC X.                                     00001700
             03 MLPROD2P     PIC X.                                     00001710
             03 MLPROD2H     PIC X.                                     00001720
             03 MLPROD2V     PIC X.                                     00001730
             03 MLPROD2O     PIC X(20).                                 00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MWSEQCORR2A  PIC X.                                     00001760
             03 MWSEQCORR2C  PIC X.                                     00001770
             03 MWSEQCORR2P  PIC X.                                     00001780
             03 MWSEQCORR2H  PIC X.                                     00001790
             03 MWSEQCORR2V  PIC X.                                     00001800
             03 MWSEQCORR2O  PIC ZZZZZZZ.                               00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MFLAGLIB2A   PIC X.                                     00001830
             03 MFLAGLIB2C   PIC X.                                     00001840
             03 MFLAGLIB2P   PIC X.                                     00001850
             03 MFLAGLIB2H   PIC X.                                     00001860
             03 MFLAGLIB2V   PIC X.                                     00001870
             03 MFLAGLIB2O   PIC X.                                     00001880
      * MESSAGE ERREUR                                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLIBERRA  PIC X.                                          00001910
           02 MLIBERRC  PIC X.                                          00001920
           02 MLIBERRP  PIC X.                                          00001930
           02 MLIBERRH  PIC X.                                          00001940
           02 MLIBERRV  PIC X.                                          00001950
           02 MLIBERRO  PIC X(78).                                      00001960
      * CODE TRANSACTION                                                00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCODTRAA  PIC X.                                          00001990
           02 MCODTRAC  PIC X.                                          00002000
           02 MCODTRAP  PIC X.                                          00002010
           02 MCODTRAH  PIC X.                                          00002020
           02 MCODTRAV  PIC X.                                          00002030
           02 MCODTRAO  PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MZONCMDA  PIC X.                                          00002060
           02 MZONCMDC  PIC X.                                          00002070
           02 MZONCMDP  PIC X.                                          00002080
           02 MZONCMDH  PIC X.                                          00002090
           02 MZONCMDV  PIC X.                                          00002100
           02 MZONCMDO  PIC X(15).                                      00002110
      * CICS DE TRAVAIL                                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCICSA    PIC X.                                          00002140
           02 MCICSC    PIC X.                                          00002150
           02 MCICSP    PIC X.                                          00002160
           02 MCICSH    PIC X.                                          00002170
           02 MCICSV    PIC X.                                          00002180
           02 MCICSO    PIC X(5).                                       00002190
      * NETNAME                                                         00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNETNAMA  PIC X.                                          00002220
           02 MNETNAMC  PIC X.                                          00002230
           02 MNETNAMP  PIC X.                                          00002240
           02 MNETNAMH  PIC X.                                          00002250
           02 MNETNAMV  PIC X.                                          00002260
           02 MNETNAMO  PIC X(8).                                       00002270
      * CODE TERMINAL                                                   00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MSCREENA  PIC X.                                          00002300
           02 MSCREENC  PIC X.                                          00002310
           02 MSCREENP  PIC X.                                          00002320
           02 MSCREENH  PIC X.                                          00002330
           02 MSCREENV  PIC X.                                          00002340
           02 MSCREENO  PIC X(5).                                       00002350
                                                                                
