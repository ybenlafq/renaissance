      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * LONGUEUR 105   DEFINITION ENTETE                               *      13
      *          38000 DEFINITION DATA                                 *        
      * COPY POUR LE PROGRAMME MCG02                                   *      13
      ******************************************************************        
       01 WS-MQ10.                                                              
          02 WS-QUEUE.                                                          
             10 MQ10-MSGID               PIC X(24).                             
             10 MQ10-CORRELID            PIC X(24).                             
          02 WS-CODRET                   PIC X(02).                             
          02 WS-MESSAGE.                                                        
             05 MCG02-ENTETE.                                                   
                10 MCG02-TYPE            PIC X(03).                             
                10 MCG02-NSOCMSG         PIC X(03).                             
                10 MCG02-NLIEUMSG        PIC X(03).                             
                10 MCG02-NSOCDST         PIC X(03).                             
                10 MCG02-NLIEUDST        PIC X(03).                             
                10 MCG02-NORD            PIC 9(08).                             
                10 MCG02-LPROG           PIC X(10).                             
                10 MCG02-DJOUR           PIC X(08).                             
                10 MCG02-WSID            PIC X(10).                             
                10 MCG02-USER            PIC X(10).                             
                10 MCG02-CHRONO          PIC 9(07).                             
                10 MCG02-NBRMSG          PIC 9(07).                             
                10 MCG02-NBRENR          PIC 9(5).                              
                10 MCG02-TAILLE          PIC 9(5).                              
                10 MCG02-FILLER          PIC X(20).                             
             05 MCG02-MESSAGE            PIC X(38000).                          
                                                                                
