      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG11   EFG11                                              00000020
      ***************************************************************** 00000030
       01   EFG11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECORIGL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSECORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECORIGF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSECORIGI      PIC X(6).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLLIEUI   PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPNATL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MTYPNATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPNATF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTYPNATI  PIC X(30).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTIERSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTIERSEI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSOCCI    PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSERVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSERVF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSERVI    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCCL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSOCCF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLSOCCI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMFACTL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNUMFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMFACTF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNUMFACTI      PIC X(7).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDCREATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCREATF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDCREATI  PIC X(11).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDESTL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MSOCDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCDESTF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSOCDESTI      PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDESTL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLIEUDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIEUDESTF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIEUDESTI     PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECDESTL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MSECDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECDESTF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSECDESTI      PIC X(6).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDESTL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLIBDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBDESTF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBDESTI      PIC X(20).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTREL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLETTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTREF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLETTREI  PIC X(10).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MTIERSRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MTIERSRI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERVDESTL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MSERVDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSERVDESTF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSERVDESTI     PIC X(5).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTRRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLETTRRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTRRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLETTRRI  PIC X(10).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAL1L      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MLIBVAL1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAL1F      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBVAL1I      PIC X(12).                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMORIGL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNUMORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMORIGF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNUMORIGI      PIC X(7).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOMPTAL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDCOMPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCOMPTAF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDCOMPTAI      PIC X(10).                                 00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MDECHEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDECHEANF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MDECHEANI      PIC X(10).                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBVAL2L      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MLIBVAL2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBVAL2F      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBVAL2I      PIC X(9).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDORIGL   COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MDORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDORIGF   PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MDORIGI   PIC X(10).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXERCICEL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MEXERCICEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MEXERCICEF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MEXERCICEI     PIC X(4).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPERIODEL     COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MNPERIODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPERIODEF     PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNPERIODEI     PIC X(3).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPERIODEL     COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MLPERIODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLPERIODEF     PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLPERIODEI     PIC X(14).                                 00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPASSAGEL     COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MDPASSAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDPASSAGEF     PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MDPASSAGEI     PIC X(10).                                 00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCRITL      COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MLIBCRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBCRITF      PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MLIBCRITI      PIC X(5).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTVAL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLIBTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTVAF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLIBTVAI  PIC X(3).                                       00001490
           02 MTABFACI OCCURS   8 TIMES .                               00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITEREL    COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MCRITEREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRITEREF    PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MCRITEREI    PIC X(5).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001550
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MQTEI   PIC X(5).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPUHTL  COMP PIC S9(4).                                 00001590
      *--                                                                       
             03 MPUHTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPUHTF  PIC X.                                          00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MPUHTI  PIC X(13).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTTVAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
             03 MTTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTTVAF  PIC X.                                          00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MTTVAI  PIC X.                                          00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTHTL     COMP PIC S9(4).                            00001670
      *--                                                                       
             03 MMONTHTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMONTHTF     PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MMONTHTI     PIC X(13).                                 00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDESIGNL     COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MDESIGNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDESIGNF     PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MDESIGNI     PIC X(37).                                 00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTESCL      COMP PIC S9(4).                            00001750
      *--                                                                       
           02 MLIBTESCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBTESCF      PIC X.                                     00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MLIBTESCI      PIC X(13).                                 00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTESCL    COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MTESCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTESCF    PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MTESCI    PIC X(5).                                       00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMTESCL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MLMTESCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMTESCF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MLMTESCI  PIC X(3).                                       00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTESCL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MTOTESCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTESCF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MTOTESCI  PIC X(13).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTHTL   COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MTOTHTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTOTHTF   PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MTOTHTI   PIC X(14).                                      00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTVAL    COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MLTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTVAF    PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MLTVAI    PIC X(3).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTTVAL  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MTOTTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTTVAF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MTOTTVAI  PIC X(13).                                      00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTTCL    COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MLTTCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTTCF    PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MLTTCI    PIC X(3).                                       00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTTTCL  COMP PIC S9(4).                                 00002070
      *--                                                                       
           02 MTOTTTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTTTCF  PIC X.                                          00002080
           02 FILLER    PIC X(4).                                       00002090
           02 MTOTTTCI  PIC X(15).                                      00002100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002120
           02 FILLER    PIC X(4).                                       00002130
           02 MZONCMDI  PIC X(15).                                      00002140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002160
           02 FILLER    PIC X(4).                                       00002170
           02 MLIBERRI  PIC X(58).                                      00002180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002200
           02 FILLER    PIC X(4).                                       00002210
           02 MCODTRAI  PIC X(4).                                       00002220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002240
           02 FILLER    PIC X(4).                                       00002250
           02 MCICSI    PIC X(5).                                       00002260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002280
           02 FILLER    PIC X(4).                                       00002290
           02 MNETNAMI  PIC X(8).                                       00002300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002320
           02 FILLER    PIC X(4).                                       00002330
           02 MSCREENI  PIC X(4).                                       00002340
      ***************************************************************** 00002350
      * SDF: EFG11   EFG11                                              00002360
      ***************************************************************** 00002370
       01   EFG11O REDEFINES EFG11I.                                    00002380
           02 FILLER    PIC X(12).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDATJOUA  PIC X.                                          00002410
           02 MDATJOUC  PIC X.                                          00002420
           02 MDATJOUP  PIC X.                                          00002430
           02 MDATJOUH  PIC X.                                          00002440
           02 MDATJOUV  PIC X.                                          00002450
           02 MDATJOUO  PIC X(10).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MTIMJOUA  PIC X.                                          00002480
           02 MTIMJOUC  PIC X.                                          00002490
           02 MTIMJOUP  PIC X.                                          00002500
           02 MTIMJOUH  PIC X.                                          00002510
           02 MTIMJOUV  PIC X.                                          00002520
           02 MTIMJOUO  PIC X(5).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MFONCA    PIC X.                                          00002550
           02 MFONCC    PIC X.                                          00002560
           02 MFONCP    PIC X.                                          00002570
           02 MFONCH    PIC X.                                          00002580
           02 MFONCV    PIC X.                                          00002590
           02 MFONCO    PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MPAGEA    PIC X.                                          00002620
           02 MPAGEC    PIC X.                                          00002630
           02 MPAGEP    PIC X.                                          00002640
           02 MPAGEH    PIC X.                                          00002650
           02 MPAGEV    PIC X.                                          00002660
           02 MPAGEO    PIC 999.                                        00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MPAGEMAXA      PIC X.                                     00002690
           02 MPAGEMAXC PIC X.                                          00002700
           02 MPAGEMAXP PIC X.                                          00002710
           02 MPAGEMAXH PIC X.                                          00002720
           02 MPAGEMAXV PIC X.                                          00002730
           02 MPAGEMAXO      PIC 999.                                   00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNSOCA    PIC X.                                          00002760
           02 MNSOCC    PIC X.                                          00002770
           02 MNSOCP    PIC X.                                          00002780
           02 MNSOCH    PIC X.                                          00002790
           02 MNSOCV    PIC X.                                          00002800
           02 MNSOCO    PIC X(3).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MNLIEUA   PIC X.                                          00002830
           02 MNLIEUC   PIC X.                                          00002840
           02 MNLIEUP   PIC X.                                          00002850
           02 MNLIEUH   PIC X.                                          00002860
           02 MNLIEUV   PIC X.                                          00002870
           02 MNLIEUO   PIC X(3).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSECORIGA      PIC X.                                     00002900
           02 MSECORIGC PIC X.                                          00002910
           02 MSECORIGP PIC X.                                          00002920
           02 MSECORIGH PIC X.                                          00002930
           02 MSECORIGV PIC X.                                          00002940
           02 MSECORIGO      PIC X(6).                                  00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLLIEUA   PIC X.                                          00002970
           02 MLLIEUC   PIC X.                                          00002980
           02 MLLIEUP   PIC X.                                          00002990
           02 MLLIEUH   PIC X.                                          00003000
           02 MLLIEUV   PIC X.                                          00003010
           02 MLLIEUO   PIC X(20).                                      00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MTYPNATA  PIC X.                                          00003040
           02 MTYPNATC  PIC X.                                          00003050
           02 MTYPNATP  PIC X.                                          00003060
           02 MTYPNATH  PIC X.                                          00003070
           02 MTYPNATV  PIC X.                                          00003080
           02 MTYPNATO  PIC X(30).                                      00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MTIERSEA  PIC X.                                          00003110
           02 MTIERSEC  PIC X.                                          00003120
           02 MTIERSEP  PIC X.                                          00003130
           02 MTIERSEH  PIC X.                                          00003140
           02 MTIERSEV  PIC X.                                          00003150
           02 MTIERSEO  PIC X(8).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MSOCCA    PIC X.                                          00003180
           02 MSOCCC    PIC X.                                          00003190
           02 MSOCCP    PIC X.                                          00003200
           02 MSOCCH    PIC X.                                          00003210
           02 MSOCCV    PIC X.                                          00003220
           02 MSOCCO    PIC X(3).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MSERVA    PIC X.                                          00003250
           02 MSERVC    PIC X.                                          00003260
           02 MSERVP    PIC X.                                          00003270
           02 MSERVH    PIC X.                                          00003280
           02 MSERVV    PIC X.                                          00003290
           02 MSERVO    PIC X(5).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MLSOCCA   PIC X.                                          00003320
           02 MLSOCCC   PIC X.                                          00003330
           02 MLSOCCP   PIC X.                                          00003340
           02 MLSOCCH   PIC X.                                          00003350
           02 MLSOCCV   PIC X.                                          00003360
           02 MLSOCCO   PIC X(20).                                      00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MNUMFACTA      PIC X.                                     00003390
           02 MNUMFACTC PIC X.                                          00003400
           02 MNUMFACTP PIC X.                                          00003410
           02 MNUMFACTH PIC X.                                          00003420
           02 MNUMFACTV PIC X.                                          00003430
           02 MNUMFACTO      PIC X(7).                                  00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MDCREATA  PIC X.                                          00003460
           02 MDCREATC  PIC X.                                          00003470
           02 MDCREATP  PIC X.                                          00003480
           02 MDCREATH  PIC X.                                          00003490
           02 MDCREATV  PIC X.                                          00003500
           02 MDCREATO  PIC X(11).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MSOCDESTA      PIC X.                                     00003530
           02 MSOCDESTC PIC X.                                          00003540
           02 MSOCDESTP PIC X.                                          00003550
           02 MSOCDESTH PIC X.                                          00003560
           02 MSOCDESTV PIC X.                                          00003570
           02 MSOCDESTO      PIC X(3).                                  00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MLIEUDESTA     PIC X.                                     00003600
           02 MLIEUDESTC     PIC X.                                     00003610
           02 MLIEUDESTP     PIC X.                                     00003620
           02 MLIEUDESTH     PIC X.                                     00003630
           02 MLIEUDESTV     PIC X.                                     00003640
           02 MLIEUDESTO     PIC X(3).                                  00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MSECDESTA      PIC X.                                     00003670
           02 MSECDESTC PIC X.                                          00003680
           02 MSECDESTP PIC X.                                          00003690
           02 MSECDESTH PIC X.                                          00003700
           02 MSECDESTV PIC X.                                          00003710
           02 MSECDESTO      PIC X(6).                                  00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MLIBDESTA      PIC X.                                     00003740
           02 MLIBDESTC PIC X.                                          00003750
           02 MLIBDESTP PIC X.                                          00003760
           02 MLIBDESTH PIC X.                                          00003770
           02 MLIBDESTV PIC X.                                          00003780
           02 MLIBDESTO      PIC X(20).                                 00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MLETTREA  PIC X.                                          00003810
           02 MLETTREC  PIC X.                                          00003820
           02 MLETTREP  PIC X.                                          00003830
           02 MLETTREH  PIC X.                                          00003840
           02 MLETTREV  PIC X.                                          00003850
           02 MLETTREO  PIC X(10).                                      00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MTIERSRA  PIC X.                                          00003880
           02 MTIERSRC  PIC X.                                          00003890
           02 MTIERSRP  PIC X.                                          00003900
           02 MTIERSRH  PIC X.                                          00003910
           02 MTIERSRV  PIC X.                                          00003920
           02 MTIERSRO  PIC X(8).                                       00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MSERVDESTA     PIC X.                                     00003950
           02 MSERVDESTC     PIC X.                                     00003960
           02 MSERVDESTP     PIC X.                                     00003970
           02 MSERVDESTH     PIC X.                                     00003980
           02 MSERVDESTV     PIC X.                                     00003990
           02 MSERVDESTO     PIC X(5).                                  00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MLETTRRA  PIC X.                                          00004020
           02 MLETTRRC  PIC X.                                          00004030
           02 MLETTRRP  PIC X.                                          00004040
           02 MLETTRRH  PIC X.                                          00004050
           02 MLETTRRV  PIC X.                                          00004060
           02 MLETTRRO  PIC X(10).                                      00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MLIBVAL1A      PIC X.                                     00004090
           02 MLIBVAL1C PIC X.                                          00004100
           02 MLIBVAL1P PIC X.                                          00004110
           02 MLIBVAL1H PIC X.                                          00004120
           02 MLIBVAL1V PIC X.                                          00004130
           02 MLIBVAL1O      PIC X(12).                                 00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MNUMORIGA      PIC X.                                     00004160
           02 MNUMORIGC PIC X.                                          00004170
           02 MNUMORIGP PIC X.                                          00004180
           02 MNUMORIGH PIC X.                                          00004190
           02 MNUMORIGV PIC X.                                          00004200
           02 MNUMORIGO      PIC X(7).                                  00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MDCOMPTAA      PIC X.                                     00004230
           02 MDCOMPTAC PIC X.                                          00004240
           02 MDCOMPTAP PIC X.                                          00004250
           02 MDCOMPTAH PIC X.                                          00004260
           02 MDCOMPTAV PIC X.                                          00004270
           02 MDCOMPTAO      PIC X(10).                                 00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MDECHEANA      PIC X.                                     00004300
           02 MDECHEANC PIC X.                                          00004310
           02 MDECHEANP PIC X.                                          00004320
           02 MDECHEANH PIC X.                                          00004330
           02 MDECHEANV PIC X.                                          00004340
           02 MDECHEANO      PIC X(10).                                 00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MLIBVAL2A      PIC X.                                     00004370
           02 MLIBVAL2C PIC X.                                          00004380
           02 MLIBVAL2P PIC X.                                          00004390
           02 MLIBVAL2H PIC X.                                          00004400
           02 MLIBVAL2V PIC X.                                          00004410
           02 MLIBVAL2O      PIC X(9).                                  00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MDORIGA   PIC X.                                          00004440
           02 MDORIGC   PIC X.                                          00004450
           02 MDORIGP   PIC X.                                          00004460
           02 MDORIGH   PIC X.                                          00004470
           02 MDORIGV   PIC X.                                          00004480
           02 MDORIGO   PIC X(10).                                      00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MEXERCICEA     PIC X.                                     00004510
           02 MEXERCICEC     PIC X.                                     00004520
           02 MEXERCICEP     PIC X.                                     00004530
           02 MEXERCICEH     PIC X.                                     00004540
           02 MEXERCICEV     PIC X.                                     00004550
           02 MEXERCICEO     PIC X(4).                                  00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MNPERIODEA     PIC X.                                     00004580
           02 MNPERIODEC     PIC X.                                     00004590
           02 MNPERIODEP     PIC X.                                     00004600
           02 MNPERIODEH     PIC X.                                     00004610
           02 MNPERIODEV     PIC X.                                     00004620
           02 MNPERIODEO     PIC X(3).                                  00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MLPERIODEA     PIC X.                                     00004650
           02 MLPERIODEC     PIC X.                                     00004660
           02 MLPERIODEP     PIC X.                                     00004670
           02 MLPERIODEH     PIC X.                                     00004680
           02 MLPERIODEV     PIC X.                                     00004690
           02 MLPERIODEO     PIC X(14).                                 00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MDPASSAGEA     PIC X.                                     00004720
           02 MDPASSAGEC     PIC X.                                     00004730
           02 MDPASSAGEP     PIC X.                                     00004740
           02 MDPASSAGEH     PIC X.                                     00004750
           02 MDPASSAGEV     PIC X.                                     00004760
           02 MDPASSAGEO     PIC X(10).                                 00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MLIBCRITA      PIC X.                                     00004790
           02 MLIBCRITC PIC X.                                          00004800
           02 MLIBCRITP PIC X.                                          00004810
           02 MLIBCRITH PIC X.                                          00004820
           02 MLIBCRITV PIC X.                                          00004830
           02 MLIBCRITO      PIC X(5).                                  00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MLIBTVAA  PIC X.                                          00004860
           02 MLIBTVAC  PIC X.                                          00004870
           02 MLIBTVAP  PIC X.                                          00004880
           02 MLIBTVAH  PIC X.                                          00004890
           02 MLIBTVAV  PIC X.                                          00004900
           02 MLIBTVAO  PIC X(3).                                       00004910
           02 MTABFACO OCCURS   8 TIMES .                               00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MCRITEREA    PIC X.                                     00004940
             03 MCRITEREC    PIC X.                                     00004950
             03 MCRITEREP    PIC X.                                     00004960
             03 MCRITEREH    PIC X.                                     00004970
             03 MCRITEREV    PIC X.                                     00004980
             03 MCRITEREO    PIC X(5).                                  00004990
             03 FILLER       PIC X(2).                                  00005000
             03 MQTEA   PIC X.                                          00005010
             03 MQTEC   PIC X.                                          00005020
             03 MQTEP   PIC X.                                          00005030
             03 MQTEH   PIC X.                                          00005040
             03 MQTEV   PIC X.                                          00005050
             03 MQTEO   PIC X(5).                                       00005060
             03 FILLER       PIC X(2).                                  00005070
             03 MPUHTA  PIC X.                                          00005080
             03 MPUHTC  PIC X.                                          00005090
             03 MPUHTP  PIC X.                                          00005100
             03 MPUHTH  PIC X.                                          00005110
             03 MPUHTV  PIC X.                                          00005120
             03 MPUHTO  PIC ZZZZZZZZZ9,99.                              00005130
             03 FILLER       PIC X(2).                                  00005140
             03 MTTVAA  PIC X.                                          00005150
             03 MTTVAC  PIC X.                                          00005160
             03 MTTVAP  PIC X.                                          00005170
             03 MTTVAH  PIC X.                                          00005180
             03 MTTVAV  PIC X.                                          00005190
             03 MTTVAO  PIC X.                                          00005200
             03 FILLER       PIC X(2).                                  00005210
             03 MMONTHTA     PIC X.                                     00005220
             03 MMONTHTC     PIC X.                                     00005230
             03 MMONTHTP     PIC X.                                     00005240
             03 MMONTHTH     PIC X.                                     00005250
             03 MMONTHTV     PIC X.                                     00005260
             03 MMONTHTO     PIC ZZZZZZZZZ9,99.                         00005270
             03 FILLER       PIC X(2).                                  00005280
             03 MDESIGNA     PIC X.                                     00005290
             03 MDESIGNC     PIC X.                                     00005300
             03 MDESIGNP     PIC X.                                     00005310
             03 MDESIGNH     PIC X.                                     00005320
             03 MDESIGNV     PIC X.                                     00005330
             03 MDESIGNO     PIC X(37).                                 00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MLIBTESCA      PIC X.                                     00005360
           02 MLIBTESCC PIC X.                                          00005370
           02 MLIBTESCP PIC X.                                          00005380
           02 MLIBTESCH PIC X.                                          00005390
           02 MLIBTESCV PIC X.                                          00005400
           02 MLIBTESCO      PIC X(13).                                 00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MTESCA    PIC X.                                          00005430
           02 MTESCC    PIC X.                                          00005440
           02 MTESCP    PIC X.                                          00005450
           02 MTESCH    PIC X.                                          00005460
           02 MTESCV    PIC X.                                          00005470
           02 MTESCO    PIC Z9,99.                                      00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MLMTESCA  PIC X.                                          00005500
           02 MLMTESCC  PIC X.                                          00005510
           02 MLMTESCP  PIC X.                                          00005520
           02 MLMTESCH  PIC X.                                          00005530
           02 MLMTESCV  PIC X.                                          00005540
           02 MLMTESCO  PIC X(3).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MTOTESCA  PIC X.                                          00005570
           02 MTOTESCC  PIC X.                                          00005580
           02 MTOTESCP  PIC X.                                          00005590
           02 MTOTESCH  PIC X.                                          00005600
           02 MTOTESCV  PIC X.                                          00005610
           02 MTOTESCO  PIC ZZZZZZZZZ9,99.                              00005620
           02 FILLER    PIC X(2).                                       00005630
           02 MTOTHTA   PIC X.                                          00005640
           02 MTOTHTC   PIC X.                                          00005650
           02 MTOTHTP   PIC X.                                          00005660
           02 MTOTHTH   PIC X.                                          00005670
           02 MTOTHTV   PIC X.                                          00005680
           02 MTOTHTO   PIC Z(10)9,99.                                  00005690
           02 FILLER    PIC X(2).                                       00005700
           02 MLTVAA    PIC X.                                          00005710
           02 MLTVAC    PIC X.                                          00005720
           02 MLTVAP    PIC X.                                          00005730
           02 MLTVAH    PIC X.                                          00005740
           02 MLTVAV    PIC X.                                          00005750
           02 MLTVAO    PIC X(3).                                       00005760
           02 FILLER    PIC X(2).                                       00005770
           02 MTOTTVAA  PIC X.                                          00005780
           02 MTOTTVAC  PIC X.                                          00005790
           02 MTOTTVAP  PIC X.                                          00005800
           02 MTOTTVAH  PIC X.                                          00005810
           02 MTOTTVAV  PIC X.                                          00005820
           02 MTOTTVAO  PIC ZZZZZZZZZ9,99.                              00005830
           02 FILLER    PIC X(2).                                       00005840
           02 MLTTCA    PIC X.                                          00005850
           02 MLTTCC    PIC X.                                          00005860
           02 MLTTCP    PIC X.                                          00005870
           02 MLTTCH    PIC X.                                          00005880
           02 MLTTCV    PIC X.                                          00005890
           02 MLTTCO    PIC X(3).                                       00005900
           02 FILLER    PIC X(2).                                       00005910
           02 MTOTTTCA  PIC X.                                          00005920
           02 MTOTTTCC  PIC X.                                          00005930
           02 MTOTTTCP  PIC X.                                          00005940
           02 MTOTTTCH  PIC X.                                          00005950
           02 MTOTTTCV  PIC X.                                          00005960
           02 MTOTTTCO  PIC Z(11)9,99.                                  00005970
           02 FILLER    PIC X(2).                                       00005980
           02 MZONCMDA  PIC X.                                          00005990
           02 MZONCMDC  PIC X.                                          00006000
           02 MZONCMDP  PIC X.                                          00006010
           02 MZONCMDH  PIC X.                                          00006020
           02 MZONCMDV  PIC X.                                          00006030
           02 MZONCMDO  PIC X(15).                                      00006040
           02 FILLER    PIC X(2).                                       00006050
           02 MLIBERRA  PIC X.                                          00006060
           02 MLIBERRC  PIC X.                                          00006070
           02 MLIBERRP  PIC X.                                          00006080
           02 MLIBERRH  PIC X.                                          00006090
           02 MLIBERRV  PIC X.                                          00006100
           02 MLIBERRO  PIC X(58).                                      00006110
           02 FILLER    PIC X(2).                                       00006120
           02 MCODTRAA  PIC X.                                          00006130
           02 MCODTRAC  PIC X.                                          00006140
           02 MCODTRAP  PIC X.                                          00006150
           02 MCODTRAH  PIC X.                                          00006160
           02 MCODTRAV  PIC X.                                          00006170
           02 MCODTRAO  PIC X(4).                                       00006180
           02 FILLER    PIC X(2).                                       00006190
           02 MCICSA    PIC X.                                          00006200
           02 MCICSC    PIC X.                                          00006210
           02 MCICSP    PIC X.                                          00006220
           02 MCICSH    PIC X.                                          00006230
           02 MCICSV    PIC X.                                          00006240
           02 MCICSO    PIC X(5).                                       00006250
           02 FILLER    PIC X(2).                                       00006260
           02 MNETNAMA  PIC X.                                          00006270
           02 MNETNAMC  PIC X.                                          00006280
           02 MNETNAMP  PIC X.                                          00006290
           02 MNETNAMH  PIC X.                                          00006300
           02 MNETNAMV  PIC X.                                          00006310
           02 MNETNAMO  PIC X(8).                                       00006320
           02 FILLER    PIC X(2).                                       00006330
           02 MSCREENA  PIC X.                                          00006340
           02 MSCREENC  PIC X.                                          00006350
           02 MSCREENP  PIC X.                                          00006360
           02 MSCREENH  PIC X.                                          00006370
           02 MSCREENV  PIC X.                                          00006380
           02 MSCREENO  PIC X(4).                                       00006390
                                                                                
