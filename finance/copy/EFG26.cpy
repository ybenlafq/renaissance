      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFG21   EFG21                                              00000020
      ***************************************************************** 00000030
       01   EFG26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETABL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNETABL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNETABF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNETABI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOCOUTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDOCOUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDOCOUTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDOCOUTI  PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOCINL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDOCINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDOCINF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDOCINI   PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATFACTL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNATFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNATFACTF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNATFACTI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTXTVAL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MTXTVAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTXTVAF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTXTVAI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBJOURSL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNBJOURSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBJOURSF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNBJOURSI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGLL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MTYPREGLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREGLF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTYPREGLI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNHTCL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MJRNHTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNHTCF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MJRNHTCI  PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNTGCL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MJRNTGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNTGCF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MJRNTGCI  PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNHTPL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MJRNHTPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNHTPF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MJRNHTPI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNTGPL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MJRNTGPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNTGPF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MJRNTGPI  PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNHTFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MJRNHTFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNHTFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MJRNHTFI  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNTGFL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MJRNTGFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNTGFF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MJRNTGFI  PIC X(10).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(78).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCODTRAI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNETNAMI  PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * SDF: EFG21   EFG21                                              00000910
      ***************************************************************** 00000920
       01   EFG26O REDEFINES EFG26I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MDATJOUA  PIC X.                                          00000960
           02 MDATJOUC  PIC X.                                          00000970
           02 MDATJOUP  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUV  PIC X.                                          00001000
           02 MDATJOUO  PIC X(10).                                      00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTIMJOUA  PIC X.                                          00001030
           02 MTIMJOUC  PIC X.                                          00001040
           02 MTIMJOUP  PIC X.                                          00001050
           02 MTIMJOUH  PIC X.                                          00001060
           02 MTIMJOUV  PIC X.                                          00001070
           02 MTIMJOUO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNSOCA    PIC X.                                          00001100
           02 MNSOCC    PIC X.                                          00001110
           02 MNSOCP    PIC X.                                          00001120
           02 MNSOCH    PIC X.                                          00001130
           02 MNSOCV    PIC X.                                          00001140
           02 MNSOCO    PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNETABA   PIC X.                                          00001170
           02 MNETABC   PIC X.                                          00001180
           02 MNETABP   PIC X.                                          00001190
           02 MNETABH   PIC X.                                          00001200
           02 MNETABV   PIC X.                                          00001210
           02 MNETABO   PIC X(3).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDOCOUTA  PIC X.                                          00001240
           02 MDOCOUTC  PIC X.                                          00001250
           02 MDOCOUTP  PIC X.                                          00001260
           02 MDOCOUTH  PIC X.                                          00001270
           02 MDOCOUTV  PIC X.                                          00001280
           02 MDOCOUTO  PIC X(2).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MDOCINA   PIC X.                                          00001310
           02 MDOCINC   PIC X.                                          00001320
           02 MDOCINP   PIC X.                                          00001330
           02 MDOCINH   PIC X.                                          00001340
           02 MDOCINV   PIC X.                                          00001350
           02 MDOCINO   PIC X(2).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNATFACTA      PIC X.                                     00001380
           02 MNATFACTC PIC X.                                          00001390
           02 MNATFACTP PIC X.                                          00001400
           02 MNATFACTH PIC X.                                          00001410
           02 MNATFACTV PIC X.                                          00001420
           02 MNATFACTO      PIC X(5).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MTXTVAA   PIC X.                                          00001450
           02 MTXTVAC   PIC X.                                          00001460
           02 MTXTVAP   PIC X.                                          00001470
           02 MTXTVAH   PIC X.                                          00001480
           02 MTXTVAV   PIC X.                                          00001490
           02 MTXTVAO   PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNBJOURSA      PIC X.                                     00001520
           02 MNBJOURSC PIC X.                                          00001530
           02 MNBJOURSP PIC X.                                          00001540
           02 MNBJOURSH PIC X.                                          00001550
           02 MNBJOURSV PIC X.                                          00001560
           02 MNBJOURSO      PIC X(3).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MTYPREGLA      PIC X.                                     00001590
           02 MTYPREGLC PIC X.                                          00001600
           02 MTYPREGLP PIC X.                                          00001610
           02 MTYPREGLH PIC X.                                          00001620
           02 MTYPREGLV PIC X.                                          00001630
           02 MTYPREGLO      PIC X(2).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MJRNHTCA  PIC X.                                          00001660
           02 MJRNHTCC  PIC X.                                          00001670
           02 MJRNHTCP  PIC X.                                          00001680
           02 MJRNHTCH  PIC X.                                          00001690
           02 MJRNHTCV  PIC X.                                          00001700
           02 MJRNHTCO  PIC X(10).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MJRNTGCA  PIC X.                                          00001730
           02 MJRNTGCC  PIC X.                                          00001740
           02 MJRNTGCP  PIC X.                                          00001750
           02 MJRNTGCH  PIC X.                                          00001760
           02 MJRNTGCV  PIC X.                                          00001770
           02 MJRNTGCO  PIC X(10).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MJRNHTPA  PIC X.                                          00001800
           02 MJRNHTPC  PIC X.                                          00001810
           02 MJRNHTPP  PIC X.                                          00001820
           02 MJRNHTPH  PIC X.                                          00001830
           02 MJRNHTPV  PIC X.                                          00001840
           02 MJRNHTPO  PIC X(10).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MJRNTGPA  PIC X.                                          00001870
           02 MJRNTGPC  PIC X.                                          00001880
           02 MJRNTGPP  PIC X.                                          00001890
           02 MJRNTGPH  PIC X.                                          00001900
           02 MJRNTGPV  PIC X.                                          00001910
           02 MJRNTGPO  PIC X(10).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MJRNHTFA  PIC X.                                          00001940
           02 MJRNHTFC  PIC X.                                          00001950
           02 MJRNHTFP  PIC X.                                          00001960
           02 MJRNHTFH  PIC X.                                          00001970
           02 MJRNHTFV  PIC X.                                          00001980
           02 MJRNHTFO  PIC X(10).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MJRNTGFA  PIC X.                                          00002010
           02 MJRNTGFC  PIC X.                                          00002020
           02 MJRNTGFP  PIC X.                                          00002030
           02 MJRNTGFH  PIC X.                                          00002040
           02 MJRNTGFV  PIC X.                                          00002050
           02 MJRNTGFO  PIC X(10).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLIBERRA  PIC X.                                          00002080
           02 MLIBERRC  PIC X.                                          00002090
           02 MLIBERRP  PIC X.                                          00002100
           02 MLIBERRH  PIC X.                                          00002110
           02 MLIBERRV  PIC X.                                          00002120
           02 MLIBERRO  PIC X(78).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCODTRAA  PIC X.                                          00002150
           02 MCODTRAC  PIC X.                                          00002160
           02 MCODTRAP  PIC X.                                          00002170
           02 MCODTRAH  PIC X.                                          00002180
           02 MCODTRAV  PIC X.                                          00002190
           02 MCODTRAO  PIC X(4).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCICSA    PIC X.                                          00002220
           02 MCICSC    PIC X.                                          00002230
           02 MCICSP    PIC X.                                          00002240
           02 MCICSH    PIC X.                                          00002250
           02 MCICSV    PIC X.                                          00002260
           02 MCICSO    PIC X(5).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNETNAMA  PIC X.                                          00002290
           02 MNETNAMC  PIC X.                                          00002300
           02 MNETNAMP  PIC X.                                          00002310
           02 MNETNAMH  PIC X.                                          00002320
           02 MNETNAMV  PIC X.                                          00002330
           02 MNETNAMO  PIC X(8).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSCREENA  PIC X.                                          00002360
           02 MSCREENC  PIC X.                                          00002370
           02 MSCREENP  PIC X.                                          00002380
           02 MSCREENH  PIC X.                                          00002390
           02 MSCREENV  PIC X.                                          00002400
           02 MSCREENO  PIC X(4).                                       00002410
                                                                                
