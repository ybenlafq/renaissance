      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CODLM TABLE DES EXCEPTIONS AU CODIC    *        
      *----------------------------------------------------------------*        
       01  RVCODLM .                                                            
           05  CODLM-CTABLEG2    PIC X(15).                                     
           05  CODLM-CTABLEG2-REDEF REDEFINES CODLM-CTABLEG2.                   
               10  CODLM-NSOCDEP         PIC X(06).                             
               10  CODLM-NCODIC          PIC X(07).                             
           05  CODLM-WTABLEG     PIC X(80).                                     
           05  CODLM-WTABLEG-REDEF  REDEFINES CODLM-WTABLEG.                    
               10  CODLM-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCODLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CODLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CODLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CODLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CODLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
