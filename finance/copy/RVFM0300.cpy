      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0300.                                                            
           02  FM03-NENTITE                                                     
               PIC X(0005).                                                     
           02  FM03-CMETHODE                                                    
               PIC X(0003).                                                     
           02  FM03-LMETHODE                                                    
               PIC X(0030).                                                     
           02  FM03-CDOC                                                        
               PIC X(0006).                                                     
           02  FM03-WRIB                                                        
               PIC X(0001).                                                     
           02  FM03-WNUMAUTO                                                    
               PIC X(0001).                                                     
           02  FM03-WEAP                                                        
               PIC X(0001).                                                     
           02  FM03-DELAI                                                       
               PIC S9(3) COMP-3.                                                
           02  FM03-WACTIF                                                      
               PIC X(0001).                                                     
           02  FM03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-NENTITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-CMETHODE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-CMETHODE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-LMETHODE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-LMETHODE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-CDOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-CDOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-WRIB-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-WRIB-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-WNUMAUTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-WNUMAUTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-WEAP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-WEAP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-DELAI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-DELAI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-WACTIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
