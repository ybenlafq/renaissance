      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DETERMINATION DES CLES COMPTABLE                                00000020
      ***************************************************************** 00000030
       01   EFM71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOECSL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNOECSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOECSF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNOECSI   PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBECSL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLIBECSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBECSF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLIBECSI  PIC X(25).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSENSL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNSENSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSENSF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNSENSI   PIC X.                                          00000350
           02 MECRITD OCCURS   3 TIMES .                                00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MECRITL      COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MECRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MECRITF      PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MECRITI      PIC X(5).                                  00000400
           02 MLCRITD OCCURS   3 TIMES .                                00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCRITL      COMP PIC S9(4).                            00000420
      *--                                                                       
             03 MLCRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCRITF      PIC X.                                     00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MLCRITI      PIC X(13).                                 00000450
           02 MLIGNEI OCCURS   12 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MXI     PIC X.                                          00000500
             03 MCRITD OCCURS   3 TIMES .                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCRITL     COMP PIC S9(4).                            00000520
      *--                                                                       
               04 MCRITL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCRITF     PIC X.                                     00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MCRITI     PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTECGL      COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MCTECGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTECGF      PIC X.                                     00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MCTECGI      PIC X(6).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTIONL    COMP PIC S9(4).                            00000600
      *--                                                                       
             03 MSECTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECTIONF    PIC X.                                     00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MSECTIONI    PIC X(6).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRUBRL  COMP PIC S9(4).                                 00000640
      *--                                                                       
             03 MRUBRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRUBRF  PIC X.                                          00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MRUBRI  PIC X(6).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MDEFFETI     PIC X(10).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLIBERRI  PIC X(78).                                      00000750
      * CODE TRANSACTION                                                00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCODTRAI  PIC X(4).                                       00000800
      * NOM DU CICS                                                     00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      * CODE TERMINAL                                                   00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * DETERMINATION DES CLES COMPTABLE                                00000920
      ***************************************************************** 00000930
       01   EFM71O REDEFINES EFM71I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
      * DATE JOUR TRAITEMENT                                            00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MDATJOUA  PIC X.                                          00000980
           02 MDATJOUC  PIC X.                                          00000990
           02 MDATJOUP  PIC X.                                          00001000
           02 MDATJOUH  PIC X.                                          00001010
           02 MDATJOUV  PIC X.                                          00001020
           02 MDATJOUO  PIC X(10).                                      00001030
      * HEURE TRAITEMENT                                                00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUP  PIC X.                                          00001080
           02 MTIMJOUH  PIC X.                                          00001090
           02 MTIMJOUV  PIC X.                                          00001100
           02 MTIMJOUO  PIC X(5).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MPAGEA    PIC X.                                          00001130
           02 MPAGEC    PIC X.                                          00001140
           02 MPAGEP    PIC X.                                          00001150
           02 MPAGEH    PIC X.                                          00001160
           02 MPAGEV    PIC X.                                          00001170
           02 MPAGEO    PIC Z9.                                         00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MNBPA     PIC X.                                          00001200
           02 MNBPC     PIC X.                                          00001210
           02 MNBPP     PIC X.                                          00001220
           02 MNBPH     PIC X.                                          00001230
           02 MNBPV     PIC X.                                          00001240
           02 MNBPO     PIC Z9.                                         00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNOECSA   PIC X.                                          00001270
           02 MNOECSC   PIC X.                                          00001280
           02 MNOECSP   PIC X.                                          00001290
           02 MNOECSH   PIC X.                                          00001300
           02 MNOECSV   PIC X.                                          00001310
           02 MNOECSO   PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLIBECSA  PIC X.                                          00001340
           02 MLIBECSC  PIC X.                                          00001350
           02 MLIBECSP  PIC X.                                          00001360
           02 MLIBECSH  PIC X.                                          00001370
           02 MLIBECSV  PIC X.                                          00001380
           02 MLIBECSO  PIC X(25).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNSENSA   PIC X.                                          00001410
           02 MNSENSC   PIC X.                                          00001420
           02 MNSENSP   PIC X.                                          00001430
           02 MNSENSH   PIC X.                                          00001440
           02 MNSENSV   PIC X.                                          00001450
           02 MNSENSO   PIC X.                                          00001460
           02 DFHMS1 OCCURS   3 TIMES .                                 00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MECRITA      PIC X.                                     00001490
             03 MECRITC PIC X.                                          00001500
             03 MECRITP PIC X.                                          00001510
             03 MECRITH PIC X.                                          00001520
             03 MECRITV PIC X.                                          00001530
             03 MECRITO      PIC X(5).                                  00001540
           02 DFHMS2 OCCURS   3 TIMES .                                 00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MLCRITA      PIC X.                                     00001570
             03 MLCRITC PIC X.                                          00001580
             03 MLCRITP PIC X.                                          00001590
             03 MLCRITH PIC X.                                          00001600
             03 MLCRITV PIC X.                                          00001610
             03 MLCRITO      PIC X(13).                                 00001620
           02 MLIGNEO OCCURS   12 TIMES .                               00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MXA     PIC X.                                          00001650
             03 MXC     PIC X.                                          00001660
             03 MXP     PIC X.                                          00001670
             03 MXH     PIC X.                                          00001680
             03 MXV     PIC X.                                          00001690
             03 MXO     PIC X.                                          00001700
             03 DFHMS3 OCCURS   3 TIMES .                               00001710
               04 FILLER     PIC X(2).                                  00001720
               04 MCRITA     PIC X.                                     00001730
               04 MCRITC     PIC X.                                     00001740
               04 MCRITP     PIC X.                                     00001750
               04 MCRITH     PIC X.                                     00001760
               04 MCRITV     PIC X.                                     00001770
               04 MCRITO     PIC X(5).                                  00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MCTECGA      PIC X.                                     00001800
             03 MCTECGC PIC X.                                          00001810
             03 MCTECGP PIC X.                                          00001820
             03 MCTECGH PIC X.                                          00001830
             03 MCTECGV PIC X.                                          00001840
             03 MCTECGO      PIC X(6).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MSECTIONA    PIC X.                                     00001870
             03 MSECTIONC    PIC X.                                     00001880
             03 MSECTIONP    PIC X.                                     00001890
             03 MSECTIONH    PIC X.                                     00001900
             03 MSECTIONV    PIC X.                                     00001910
             03 MSECTIONO    PIC X(6).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MRUBRA  PIC X.                                          00001940
             03 MRUBRC  PIC X.                                          00001950
             03 MRUBRP  PIC X.                                          00001960
             03 MRUBRH  PIC X.                                          00001970
             03 MRUBRV  PIC X.                                          00001980
             03 MRUBRO  PIC X(6).                                       00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MDEFFETA     PIC X.                                     00002010
             03 MDEFFETC     PIC X.                                     00002020
             03 MDEFFETP     PIC X.                                     00002030
             03 MDEFFETH     PIC X.                                     00002040
             03 MDEFFETV     PIC X.                                     00002050
             03 MDEFFETO     PIC X(10).                                 00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLIBERRA  PIC X.                                          00002080
           02 MLIBERRC  PIC X.                                          00002090
           02 MLIBERRP  PIC X.                                          00002100
           02 MLIBERRH  PIC X.                                          00002110
           02 MLIBERRV  PIC X.                                          00002120
           02 MLIBERRO  PIC X(78).                                      00002130
      * CODE TRANSACTION                                                00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCODTRAA  PIC X.                                          00002160
           02 MCODTRAC  PIC X.                                          00002170
           02 MCODTRAP  PIC X.                                          00002180
           02 MCODTRAH  PIC X.                                          00002190
           02 MCODTRAV  PIC X.                                          00002200
           02 MCODTRAO  PIC X(4).                                       00002210
      * NOM DU CICS                                                     00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
      * CODE TERMINAL                                                   00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MSCREENA  PIC X.                                          00002320
           02 MSCREENC  PIC X.                                          00002330
           02 MSCREENP  PIC X.                                          00002340
           02 MSCREENH  PIC X.                                          00002350
           02 MSCREENV  PIC X.                                          00002360
           02 MSCREENO  PIC X(4).                                       00002370
                                                                                
