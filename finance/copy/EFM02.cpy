      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GESTION AUXILIAIRES                                       00000020
      ***************************************************************** 00000030
       01   EFM02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTITEL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTITEF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNENTITEI      PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTITEL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLENTITEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTITEF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLENTITEI      PIC X(20).                                 00000310
           02 MTABI OCCURS   14 TIMES .                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCMDL     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MZONCMDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONCMDF     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MZONCMDI     PIC X.                                     00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTYPAUXL    COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MWTYPAUXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWTYPAUXF    PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MWTYPAUXI    PIC X.                                     00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAUXL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MNAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNAUXF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MNAUXI  PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAUXL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MLAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLAUXF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MLAUXI  PIC X(30).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSAISIEL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MWSAISIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSAISIEF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MWSAISIEI    PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWAUXNCGL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MWAUXNCGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWAUXNCGF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MWAUXNCGI    PIC X.                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWLETTREL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MWLETTREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWLETTREF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MWLETTREI    PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRELANCEL   COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MWRELANCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWRELANCEF   PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MWRELANCEI   PIC X.                                     00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWAUXARFL    COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MWAUXARFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWAUXARFF    PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MWAUXARFI    PIC X.                                     00000680
      * ZONE CMD AIDA                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(78).                                      00000730
      * CODE TRANSACTION                                                00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      * CICS DE TRAVAIL                                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCICSI    PIC X(5).                                       00000830
      * NETNAME                                                         00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MNETNAMI  PIC X(8).                                       00000880
      * CODE TERMINAL                                                   00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(5).                                       00000930
      ***************************************************************** 00000940
      * GCT - GESTION AUXILIAIRES                                       00000950
      ***************************************************************** 00000960
       01   EFM02O REDEFINES EFM02I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
      * DATE DU JOUR                                                    00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
      * HEURE                                                           00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MTIMJOUA  PIC X.                                          00001090
           02 MTIMJOUC  PIC X.                                          00001100
           02 MTIMJOUP  PIC X.                                          00001110
           02 MTIMJOUH  PIC X.                                          00001120
           02 MTIMJOUV  PIC X.                                          00001130
           02 MTIMJOUO  PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MPAGEA    PIC X.                                          00001160
           02 MPAGEC    PIC X.                                          00001170
           02 MPAGEP    PIC X.                                          00001180
           02 MPAGEH    PIC X.                                          00001190
           02 MPAGEV    PIC X.                                          00001200
           02 MPAGEO    PIC Z9.                                         00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNBPA     PIC X.                                          00001230
           02 MNBPC     PIC X.                                          00001240
           02 MNBPP     PIC X.                                          00001250
           02 MNBPH     PIC X.                                          00001260
           02 MNBPV     PIC X.                                          00001270
           02 MNBPO     PIC Z9.                                         00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNENTITEA      PIC X.                                     00001300
           02 MNENTITEC PIC X.                                          00001310
           02 MNENTITEP PIC X.                                          00001320
           02 MNENTITEH PIC X.                                          00001330
           02 MNENTITEV PIC X.                                          00001340
           02 MNENTITEO      PIC X(5).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MLENTITEA      PIC X.                                     00001370
           02 MLENTITEC PIC X.                                          00001380
           02 MLENTITEP PIC X.                                          00001390
           02 MLENTITEH PIC X.                                          00001400
           02 MLENTITEV PIC X.                                          00001410
           02 MLENTITEO      PIC X(20).                                 00001420
           02 MTABO OCCURS   14 TIMES .                                 00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MZONCMDA     PIC X.                                     00001450
             03 MZONCMDC     PIC X.                                     00001460
             03 MZONCMDP     PIC X.                                     00001470
             03 MZONCMDH     PIC X.                                     00001480
             03 MZONCMDV     PIC X.                                     00001490
             03 MZONCMDO     PIC X.                                     00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MWTYPAUXA    PIC X.                                     00001520
             03 MWTYPAUXC    PIC X.                                     00001530
             03 MWTYPAUXP    PIC X.                                     00001540
             03 MWTYPAUXH    PIC X.                                     00001550
             03 MWTYPAUXV    PIC X.                                     00001560
             03 MWTYPAUXO    PIC X.                                     00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MNAUXA  PIC X.                                          00001590
             03 MNAUXC  PIC X.                                          00001600
             03 MNAUXP  PIC X.                                          00001610
             03 MNAUXH  PIC X.                                          00001620
             03 MNAUXV  PIC X.                                          00001630
             03 MNAUXO  PIC X(3).                                       00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MLAUXA  PIC X.                                          00001660
             03 MLAUXC  PIC X.                                          00001670
             03 MLAUXP  PIC X.                                          00001680
             03 MLAUXH  PIC X.                                          00001690
             03 MLAUXV  PIC X.                                          00001700
             03 MLAUXO  PIC X(30).                                      00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MWSAISIEA    PIC X.                                     00001730
             03 MWSAISIEC    PIC X.                                     00001740
             03 MWSAISIEP    PIC X.                                     00001750
             03 MWSAISIEH    PIC X.                                     00001760
             03 MWSAISIEV    PIC X.                                     00001770
             03 MWSAISIEO    PIC X.                                     00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MWAUXNCGA    PIC X.                                     00001800
             03 MWAUXNCGC    PIC X.                                     00001810
             03 MWAUXNCGP    PIC X.                                     00001820
             03 MWAUXNCGH    PIC X.                                     00001830
             03 MWAUXNCGV    PIC X.                                     00001840
             03 MWAUXNCGO    PIC X.                                     00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MWLETTREA    PIC X.                                     00001870
             03 MWLETTREC    PIC X.                                     00001880
             03 MWLETTREP    PIC X.                                     00001890
             03 MWLETTREH    PIC X.                                     00001900
             03 MWLETTREV    PIC X.                                     00001910
             03 MWLETTREO    PIC X.                                     00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MWRELANCEA   PIC X.                                     00001940
             03 MWRELANCEC   PIC X.                                     00001950
             03 MWRELANCEP   PIC X.                                     00001960
             03 MWRELANCEH   PIC X.                                     00001970
             03 MWRELANCEV   PIC X.                                     00001980
             03 MWRELANCEO   PIC X.                                     00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MWAUXARFA    PIC X.                                     00002010
             03 MWAUXARFC    PIC X.                                     00002020
             03 MWAUXARFP    PIC X.                                     00002030
             03 MWAUXARFH    PIC X.                                     00002040
             03 MWAUXARFV    PIC X.                                     00002050
             03 MWAUXARFO    PIC X.                                     00002060
      * ZONE CMD AIDA                                                   00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLIBERRA  PIC X.                                          00002090
           02 MLIBERRC  PIC X.                                          00002100
           02 MLIBERRP  PIC X.                                          00002110
           02 MLIBERRH  PIC X.                                          00002120
           02 MLIBERRV  PIC X.                                          00002130
           02 MLIBERRO  PIC X(78).                                      00002140
      * CODE TRANSACTION                                                00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
      * CICS DE TRAVAIL                                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCICSA    PIC X.                                          00002250
           02 MCICSC    PIC X.                                          00002260
           02 MCICSP    PIC X.                                          00002270
           02 MCICSH    PIC X.                                          00002280
           02 MCICSV    PIC X.                                          00002290
           02 MCICSO    PIC X(5).                                       00002300
      * NETNAME                                                         00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNETNAMA  PIC X.                                          00002330
           02 MNETNAMC  PIC X.                                          00002340
           02 MNETNAMP  PIC X.                                          00002350
           02 MNETNAMH  PIC X.                                          00002360
           02 MNETNAMV  PIC X.                                          00002370
           02 MNETNAMO  PIC X(8).                                       00002380
      * CODE TERMINAL                                                   00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MSCREENA  PIC X.                                          00002410
           02 MSCREENC  PIC X.                                          00002420
           02 MSCREENP  PIC X.                                          00002430
           02 MSCREENH  PIC X.                                          00002440
           02 MSCREENV  PIC X.                                          00002450
           02 MSCREENO  PIC X(5).                                       00002460
                                                                                
