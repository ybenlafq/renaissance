      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFT6103                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFT6103                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6103.                                                            
           02  FT61-CPDS                                                        
               PIC X(0004).                                                     
           02  FT61-SECTION                                                     
               PIC X(0006).                                                     
           02  FT61-LSECTIONC                                                   
               PIC X(0015).                                                     
           02  FT61-LSECTIONL                                                   
               PIC X(0030).                                                     
           02  FT61-CMASQUE                                                     
               PIC X(0006).                                                     
           02  FT61-WINTERFACE                                                  
               PIC X(0001).                                                     
           02  FT61-DCLOTURE                                                    
               PIC X(0008).                                                     
           02  FT61-DMAJ                                                        
               PIC X(0008).                                                     
           02  FT61-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FT61-NSOC                                                        
               PIC X(0005).                                                     
           02  FT61-WFIR                                                        
               PIC X(0001).                                                     
           02  FT61-TRANSPO                                                     
               PIC X(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT6103                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFT6103-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-CPDS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-CPDS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-LSECTIONC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-LSECTIONC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-LSECTIONL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-LSECTIONL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-CMASQUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-CMASQUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-WINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-WINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-DCLOTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-DCLOTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-WFIR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-WFIR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FT61-TRANSPO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FT61-TRANSPO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
