      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu COMMISSIONS SUR SERVICE                                    00000020
      ***************************************************************** 00000030
       01   EGC30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATTRTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATTRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATTRTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATTRTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOFFREL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCOFFREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOFFREF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCOFFREI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MENTETEI  PIC X(78).                                      00000370
           02 MTABLISTI OCCURS   13 TIMES .                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MSELI   PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTITE1L   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNENTITE1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNENTITE1F   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNENTITE1I   PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFAMI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCMARQI      PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLREFFOURNI  PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROFASSL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCPROFASSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCPROFASSF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCPROFASSI   PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBL    COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNBL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MNBF    PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNBI    PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTMONTANTL   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MTMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTMONTANTF   PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MTMONTANTI   PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * menu COMMISSIONS SUR SERVICE                                    00000960
      ***************************************************************** 00000970
       01   EGC30O REDEFINES EGC30I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC X(3).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATTRTA  PIC X.                                          00001290
           02 MDATTRTC  PIC X.                                          00001300
           02 MDATTRTP  PIC X.                                          00001310
           02 MDATTRTH  PIC X.                                          00001320
           02 MDATTRTV  PIC X.                                          00001330
           02 MDATTRTO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNCODICA  PIC X.                                          00001360
           02 MNCODICC  PIC X.                                          00001370
           02 MNCODICP  PIC X.                                          00001380
           02 MNCODICH  PIC X.                                          00001390
           02 MNCODICV  PIC X.                                          00001400
           02 MNCODICO  PIC X(7).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCOFFREA  PIC X.                                          00001430
           02 MCOFFREC  PIC X.                                          00001440
           02 MCOFFREP  PIC X.                                          00001450
           02 MCOFFREH  PIC X.                                          00001460
           02 MCOFFREV  PIC X.                                          00001470
           02 MCOFFREO  PIC X(7).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MENTETEA  PIC X.                                          00001500
           02 MENTETEC  PIC X.                                          00001510
           02 MENTETEP  PIC X.                                          00001520
           02 MENTETEH  PIC X.                                          00001530
           02 MENTETEV  PIC X.                                          00001540
           02 MENTETEO  PIC X(78).                                      00001550
           02 MTABLISTO OCCURS   13 TIMES .                             00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MSELA   PIC X.                                          00001580
             03 MSELC   PIC X.                                          00001590
             03 MSELP   PIC X.                                          00001600
             03 MSELH   PIC X.                                          00001610
             03 MSELV   PIC X.                                          00001620
             03 MSELO   PIC X.                                          00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MNENTITE1A   PIC X.                                     00001650
             03 MNENTITE1C   PIC X.                                     00001660
             03 MNENTITE1P   PIC X.                                     00001670
             03 MNENTITE1H   PIC X.                                     00001680
             03 MNENTITE1V   PIC X.                                     00001690
             03 MNENTITE1O   PIC X(7).                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MCFAMA  PIC X.                                          00001720
             03 MCFAMC  PIC X.                                          00001730
             03 MCFAMP  PIC X.                                          00001740
             03 MCFAMH  PIC X.                                          00001750
             03 MCFAMV  PIC X.                                          00001760
             03 MCFAMO  PIC X(5).                                       00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MCMARQA      PIC X.                                     00001790
             03 MCMARQC PIC X.                                          00001800
             03 MCMARQP PIC X.                                          00001810
             03 MCMARQH PIC X.                                          00001820
             03 MCMARQV PIC X.                                          00001830
             03 MCMARQO      PIC X(5).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MLREFFOURNA  PIC X.                                     00001860
             03 MLREFFOURNC  PIC X.                                     00001870
             03 MLREFFOURNP  PIC X.                                     00001880
             03 MLREFFOURNH  PIC X.                                     00001890
             03 MLREFFOURNV  PIC X.                                     00001900
             03 MLREFFOURNO  PIC X(20).                                 00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MCPROFASSA   PIC X.                                     00001930
             03 MCPROFASSC   PIC X.                                     00001940
             03 MCPROFASSP   PIC X.                                     00001950
             03 MCPROFASSH   PIC X.                                     00001960
             03 MCPROFASSV   PIC X.                                     00001970
             03 MCPROFASSO   PIC X(5).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MNBA    PIC X.                                          00002000
             03 MNBC    PIC X.                                          00002010
             03 MNBP    PIC X.                                          00002020
             03 MNBH    PIC X.                                          00002030
             03 MNBV    PIC X.                                          00002040
             03 MNBO    PIC X(3).                                       00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MTMONTANTA   PIC X.                                     00002070
             03 MTMONTANTC   PIC X.                                     00002080
             03 MTMONTANTP   PIC X.                                     00002090
             03 MTMONTANTH   PIC X.                                     00002100
             03 MTMONTANTV   PIC X.                                     00002110
             03 MTMONTANTO   PIC X(8).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
