      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM2500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM2500.                                                            
           02  FM25-NJRN                                                        
               PIC X(0003).                                                     
           02  FM25-LJRN                                                        
               PIC X(0025).                                                     
           02  FM25-WCTRPASS                                                    
               PIC X(0001).                                                     
           02  FM25-WCTRPART                                                    
               PIC X(0001).                                                     
           02  FM25-WNUMAUTO                                                    
               PIC X(0001).                                                     
           02  FM25-WJRNREG                                                     
               PIC X(0001).                                                     
           02  FM25-WEDTCRS                                                     
               PIC X(0001).                                                     
           02  FM25-COMPTE                                                      
               PIC X(0006).                                                     
           02  FM25-SECTION                                                     
               PIC X(0006).                                                     
           02  FM25-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FM25-STEAPP                                                      
               PIC X(0005).                                                     
           02  FM25-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FM25-NAUX                                                        
               PIC X(0003).                                                     
           02  FM25-WBATCHTP                                                    
               PIC X(0001).                                                     
           02  FM25-WEDTREG                                                     
               PIC X(0001).                                                     
           02  FM25-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM2500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM2500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-LJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-LJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WCTRPASS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WCTRPASS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WCTRPART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WCTRPART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WNUMAUTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WNUMAUTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WJRNREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WJRNREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WEDTCRS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WEDTCRS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-COMPTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-COMPTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-SSCOMPTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-SSCOMPTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-STEAPP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-STEAPP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WBATCHTP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WBATCHTP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-WEDTREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-WEDTREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
