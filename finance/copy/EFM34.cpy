      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - GESTION CODES JOURNAUX                                    00000020
      ***************************************************************** 00000030
       01   EFM34I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJRNL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNJRNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNJRNF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNJRNI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLJRNL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLJRNL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLJRNF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLJRNI    PIC X(25).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMAJL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMAJF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDMAJI    PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRPASSL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCTRPASSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTRPASSF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTRPASSI      PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJRNREGL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MJRNREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJRNREGF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MJRNREGI  PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMAUTOL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNUMAUTOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMAUTOF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMAUTOI      PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDTCRSL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MEDTCRSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEDTCRSF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MEDTCRSI  PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWBATCHTPL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MWBATCHTPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWBATCHTPF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWBATCHTPI     PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRPARTL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTRPARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTRPARTF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTRPARTI      PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPTEL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCOMPTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOMPTEF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCOMPTEI  PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTIONL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSECTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSECTIONF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSECTIONI      PIC X(6).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUBRIQUEL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MRUBRIQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MRUBRIQUEF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MRUBRIQUEI     PIC X(6).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTEAPPL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSTEAPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTEAPPF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSTEAPPI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCDEVISEI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLDEVISEI      PIC X(30).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNAUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNAUXF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNAUXI    PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPAUXL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MTYPAUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPAUXF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTYPAUXI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAUXL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLAUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLAUXF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLAUXI    PIC X(30).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIBERRI  PIC X(79).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCODTRAI  PIC X(4).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCICSI    PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNETNAMI  PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(4).                                       00001050
      ***************************************************************** 00001060
      * GCT - GESTION CODES JOURNAUX                                    00001070
      ***************************************************************** 00001080
       01   EFM34O REDEFINES EFM34I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MDATJOUA  PIC X.                                          00001120
           02 MDATJOUC  PIC X.                                          00001130
           02 MDATJOUP  PIC X.                                          00001140
           02 MDATJOUH  PIC X.                                          00001150
           02 MDATJOUV  PIC X.                                          00001160
           02 MDATJOUO  PIC X(10).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MTIMJOUA  PIC X.                                          00001190
           02 MTIMJOUC  PIC X.                                          00001200
           02 MTIMJOUP  PIC X.                                          00001210
           02 MTIMJOUH  PIC X.                                          00001220
           02 MTIMJOUV  PIC X.                                          00001230
           02 MTIMJOUO  PIC X(5).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNJRNA    PIC X.                                          00001260
           02 MNJRNC    PIC X.                                          00001270
           02 MNJRNP    PIC X.                                          00001280
           02 MNJRNH    PIC X.                                          00001290
           02 MNJRNV    PIC X.                                          00001300
           02 MNJRNO    PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLJRNA    PIC X.                                          00001330
           02 MLJRNC    PIC X.                                          00001340
           02 MLJRNP    PIC X.                                          00001350
           02 MLJRNH    PIC X.                                          00001360
           02 MLJRNV    PIC X.                                          00001370
           02 MLJRNO    PIC X(25).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDMAJA    PIC X.                                          00001400
           02 MDMAJC    PIC X.                                          00001410
           02 MDMAJP    PIC X.                                          00001420
           02 MDMAJH    PIC X.                                          00001430
           02 MDMAJV    PIC X.                                          00001440
           02 MDMAJO    PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCTRPASSA      PIC X.                                     00001470
           02 MCTRPASSC PIC X.                                          00001480
           02 MCTRPASSP PIC X.                                          00001490
           02 MCTRPASSH PIC X.                                          00001500
           02 MCTRPASSV PIC X.                                          00001510
           02 MCTRPASSO      PIC X.                                     00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MJRNREGA  PIC X.                                          00001540
           02 MJRNREGC  PIC X.                                          00001550
           02 MJRNREGP  PIC X.                                          00001560
           02 MJRNREGH  PIC X.                                          00001570
           02 MJRNREGV  PIC X.                                          00001580
           02 MJRNREGO  PIC X.                                          00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNUMAUTOA      PIC X.                                     00001610
           02 MNUMAUTOC PIC X.                                          00001620
           02 MNUMAUTOP PIC X.                                          00001630
           02 MNUMAUTOH PIC X.                                          00001640
           02 MNUMAUTOV PIC X.                                          00001650
           02 MNUMAUTOO      PIC X.                                     00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MEDTCRSA  PIC X.                                          00001680
           02 MEDTCRSC  PIC X.                                          00001690
           02 MEDTCRSP  PIC X.                                          00001700
           02 MEDTCRSH  PIC X.                                          00001710
           02 MEDTCRSV  PIC X.                                          00001720
           02 MEDTCRSO  PIC X.                                          00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MWBATCHTPA     PIC X.                                     00001750
           02 MWBATCHTPC     PIC X.                                     00001760
           02 MWBATCHTPP     PIC X.                                     00001770
           02 MWBATCHTPH     PIC X.                                     00001780
           02 MWBATCHTPV     PIC X.                                     00001790
           02 MWBATCHTPO     PIC X.                                     00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCTRPARTA      PIC X.                                     00001820
           02 MCTRPARTC PIC X.                                          00001830
           02 MCTRPARTP PIC X.                                          00001840
           02 MCTRPARTH PIC X.                                          00001850
           02 MCTRPARTV PIC X.                                          00001860
           02 MCTRPARTO      PIC X.                                     00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCOMPTEA  PIC X.                                          00001890
           02 MCOMPTEC  PIC X.                                          00001900
           02 MCOMPTEP  PIC X.                                          00001910
           02 MCOMPTEH  PIC X.                                          00001920
           02 MCOMPTEV  PIC X.                                          00001930
           02 MCOMPTEO  PIC X(6).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSECTIONA      PIC X.                                     00001960
           02 MSECTIONC PIC X.                                          00001970
           02 MSECTIONP PIC X.                                          00001980
           02 MSECTIONH PIC X.                                          00001990
           02 MSECTIONV PIC X.                                          00002000
           02 MSECTIONO      PIC X(6).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MRUBRIQUEA     PIC X.                                     00002030
           02 MRUBRIQUEC     PIC X.                                     00002040
           02 MRUBRIQUEP     PIC X.                                     00002050
           02 MRUBRIQUEH     PIC X.                                     00002060
           02 MRUBRIQUEV     PIC X.                                     00002070
           02 MRUBRIQUEO     PIC X(6).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MSTEAPPA  PIC X.                                          00002100
           02 MSTEAPPC  PIC X.                                          00002110
           02 MSTEAPPP  PIC X.                                          00002120
           02 MSTEAPPH  PIC X.                                          00002130
           02 MSTEAPPV  PIC X.                                          00002140
           02 MSTEAPPO  PIC X(3).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCDEVISEA      PIC X.                                     00002170
           02 MCDEVISEC PIC X.                                          00002180
           02 MCDEVISEP PIC X.                                          00002190
           02 MCDEVISEH PIC X.                                          00002200
           02 MCDEVISEV PIC X.                                          00002210
           02 MCDEVISEO      PIC X(3).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLDEVISEA      PIC X.                                     00002240
           02 MLDEVISEC PIC X.                                          00002250
           02 MLDEVISEP PIC X.                                          00002260
           02 MLDEVISEH PIC X.                                          00002270
           02 MLDEVISEV PIC X.                                          00002280
           02 MLDEVISEO      PIC X(30).                                 00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNAUXA    PIC X.                                          00002310
           02 MNAUXC    PIC X.                                          00002320
           02 MNAUXP    PIC X.                                          00002330
           02 MNAUXH    PIC X.                                          00002340
           02 MNAUXV    PIC X.                                          00002350
           02 MNAUXO    PIC X(3).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MTYPAUXA  PIC X.                                          00002380
           02 MTYPAUXC  PIC X.                                          00002390
           02 MTYPAUXP  PIC X.                                          00002400
           02 MTYPAUXH  PIC X.                                          00002410
           02 MTYPAUXV  PIC X.                                          00002420
           02 MTYPAUXO  PIC X(8).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MLAUXA    PIC X.                                          00002450
           02 MLAUXC    PIC X.                                          00002460
           02 MLAUXP    PIC X.                                          00002470
           02 MLAUXH    PIC X.                                          00002480
           02 MLAUXV    PIC X.                                          00002490
           02 MLAUXO    PIC X(30).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLIBERRA  PIC X.                                          00002520
           02 MLIBERRC  PIC X.                                          00002530
           02 MLIBERRP  PIC X.                                          00002540
           02 MLIBERRH  PIC X.                                          00002550
           02 MLIBERRV  PIC X.                                          00002560
           02 MLIBERRO  PIC X(79).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCODTRAA  PIC X.                                          00002590
           02 MCODTRAC  PIC X.                                          00002600
           02 MCODTRAP  PIC X.                                          00002610
           02 MCODTRAH  PIC X.                                          00002620
           02 MCODTRAV  PIC X.                                          00002630
           02 MCODTRAO  PIC X(4).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCICSA    PIC X.                                          00002660
           02 MCICSC    PIC X.                                          00002670
           02 MCICSP    PIC X.                                          00002680
           02 MCICSH    PIC X.                                          00002690
           02 MCICSV    PIC X.                                          00002700
           02 MCICSO    PIC X(5).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MNETNAMA  PIC X.                                          00002730
           02 MNETNAMC  PIC X.                                          00002740
           02 MNETNAMP  PIC X.                                          00002750
           02 MNETNAMH  PIC X.                                          00002760
           02 MNETNAMV  PIC X.                                          00002770
           02 MNETNAMO  PIC X(8).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MSCREENA  PIC X.                                          00002800
           02 MSCREENC  PIC X.                                          00002810
           02 MSCREENP  PIC X.                                          00002820
           02 MSCREENH  PIC X.                                          00002830
           02 MSCREENV  PIC X.                                          00002840
           02 MSCREENO  PIC X(4).                                       00002850
                                                                                
