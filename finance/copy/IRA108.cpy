      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRA108 AU 26/06/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,12,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,07,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRA108.                                                        
            05 NOMETAT-IRA108           PIC X(6) VALUE 'IRA108'.                
            05 RUPTURES-IRA108.                                                 
           10 IRA108-NSOCIETE           PIC X(03).                      007  003
           10 IRA108-TYPREGL            PIC X(12).                      010  012
           10 IRA108-CTYPDEMANDE        PIC X(05).                      022  005
           10 IRA108-NREGL              PIC X(07).                      027  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRA108-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 IRA108-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRA108.                                                   
           10 IRA108-BANQUE             PIC X(15).                      036  015
           10 IRA108-NDEMANDE           PIC X(05).                      051  005
           10 IRA108-NOM                PIC X(25).                      056  025
           10 IRA108-MTREMBTOT          PIC 9(09)V9(2).                 081  011
           10 IRA108-QTE                PIC 9(01)     .                 092  001
           10 IRA108-DATEPARAM          PIC X(08).                      093  008
           10 IRA108-DEDITLET           PIC X(08).                      101  008
           10 IRA108-DREGL              PIC X(08).                      109  008
            05 FILLER                      PIC X(396).                          
                                                                                
