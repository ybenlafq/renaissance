           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFG1300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFG1300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1300.                                                            
           02  FG13-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FG13-NSOC                                                        
               PIC X(0003).                                                     
           02  FG13-NLIEU                                                       
               PIC X(0003).                                                     
           02  FG13-COMPTECG                                                    
               PIC X(0006).                                                     
           02  FG13-NATFACT                                                     
               PIC X(0005).                                                     
           02  FG13-CVENT                                                       
               PIC X(0005).                                                     
           02  FG13-NSOCCOR                                                     
               PIC X(0003).                                                     
           02  FG13-COMPTESOC                                                   
               PIC X(0006).                                                     
           02  FG13-NAUXSOC                                                     
               PIC X(0001).                                                     
           02  FG13-DATEFF                                                      
               PIC X(0008).                                                     
           02  FG13-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFG1300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFG1300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-COMPTECG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-COMPTECG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NATFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NATFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-CVENT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-CVENT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NSOCCOR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NSOCCOR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-COMPTESOC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-COMPTESOC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-NAUXSOC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-NAUXSOC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-DATEFF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-DATEFF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FG13-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FG13-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
