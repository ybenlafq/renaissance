      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVFI8000                      *        
      ******************************************************************        
       01  RVFI8000.                                                            
      *                       ID                                                
           10 FI80-ID              PIC X(15).                                   
      *                       NLIGNE                                            
           10 FI80-NLIGNE          PIC S9(2)V USAGE COMP-3.                     
      *                       CODAPP                                            
           10 FI80-CODAPP          PIC X(10).                                   
      *                       COPCO                                             
           10 FI80-COPCO           PIC X(6).                                    
      *                       TYPMVT                                            
           10 FI80-TYPMVT          PIC X(15).                                   
      *                       FLUX                                              
           10 FI80-FLUX            PIC X(12).                                   
      *                       TITRE                                             
           10 FI80-TITRE           PIC X(35).                                   
      *                       EMETTEUR                                          
           10 FI80-EMETTEUR        PIC X(6).                                    
      *                       DESTINAT                                          
           10 FI80-DESTINAT        PIC X(6).                                    
      *                       PTVENTE                                           
           10 FI80-PTVENTE         PIC X(6).                                    
      *                       NARTICLE                                          
           10 FI80-NARTICLE        PIC X(7).                                    
      *                       LARTICLE                                          
           10 FI80-LARTICLE        PIC X(30).                                   
      *                       QTE                                               
           10 FI80-QTE             PIC S9(7)V USAGE COMP-3.                     
      *                       PFACT                                             
           10 FI80-PFACT           PIC S9(11)V9(2) USAGE COMP-3.                
      *                       SRP                                               
           10 FI80-SRP             PIC S9(11)V9(2) USAGE COMP-3.                
      *                       TAXE1                                             
           10 FI80-TAXE1           PIC S9(11)V9(3) USAGE COMP-3.                
      *                       TAXE2                                             
           10 FI80-TAXE2           PIC S9(11)V9(3) USAGE COMP-3.                
      *                       TAXE3                                             
           10 FI80-TAXE3           PIC S9(11)V9(3) USAGE COMP-3.                
      *                       TAXE4                                             
           10 FI80-TAXE4           PIC S9(11)V9(3) USAGE COMP-3.                
      *                       C1                                                
           10 FI80-C1              PIC S9(11)V9(2) USAGE COMP-3.                
      *                       C2                                                
           10 FI80-C2              PIC S9(11)V9(2) USAGE COMP-3.                
      *                       C3                                                
           10 FI80-C3              PIC S9(11)V9(2) USAGE COMP-3.                
      *                       C4                                                
           10 FI80-C4              PIC S9(11)V9(2) USAGE COMP-3.                
      *                       FLAGTAXE                                          
           10 FI80-FLAGTAXE        PIC X(1).                                    
      *                       ARTICLEC                                          
           10 FI80-ARTICLEC        PIC X(7).                                    
      *                       LIBELLE                                           
           10 FI80-LIBELLE         PIC X(40).                                   
      *                       REFERENCE                                         
           10 FI80-REFERENCE       PIC X(40).                                   
      *                       TXTVA                                             
           10 FI80-TXTVA           PIC S9(2)V9(2) USAGE COMP-3.                 
      *                       PTC                                               
           10 FI80-PTC             PIC S9(11)V9(2) USAGE COMP-3.                
      *                       FLAG_PTC_F                                        
           10 FI80-FLAG-PTC-F      PIC X(1).                                    
      *                       NSEQ                                              
           10 FI80-NSEQ            PIC X(2).                                    
      *                       DMVT                                              
           10 FI80-DMVT            PIC X(8).                                    
      *                       CREGROUP                                          
           10 FI80-CREGROUP        PIC X(5).                                    
      *                       LREGROUP                                          
           10 FI80-LREGROUP        PIC X(35).                                   
      *                       IDLOG                                             
           10 FI80-IDLOG           PIC X(15).                                   
      *                       DCREATION                                         
           10 FI80-DCREATION       PIC X(8).                                    
      *                       DTRAIT                                            
           10 FI80-DTRAIT          PIC X(8).                                    
      *                       DPRMP                                             
           10 FI80-DPRMP           PIC X(8).                                    
      *                       FPRMP                                             
           10 FI80-FPRMP           PIC X(1).                                    
      *                       DSYST                                             
           10 FI80-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 40      *        
      ******************************************************************        
                                                                                
