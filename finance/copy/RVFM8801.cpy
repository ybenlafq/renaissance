      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM8801                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM8801                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8801.                                                            
           02  FM88-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  FM88-CZONE                                                       
               PIC X(0002).                                                     
           02  FM88-TIERS                                                       
               PIC X(0015).                                                     
           02  FM88-LIBELLE                                                     
               PIC X(0030).                                                     
           02  FM88-DMAJ                                                        
               PIC X(0008).                                                     
           02  FM88-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FM88-TYPTIERS                                                    
               PIC X(0001).                                                     
           02  FM88-WSOC                                                        
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM8801                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM8801-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-CINTERFACE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-CINTERFACE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-CZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-TIERS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-TIERS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-TYPTIERS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-TYPTIERS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM88-WSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM88-WSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
