      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFM7300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFM7300                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVFM7300.                                                            
           02  FM73-CNOMPROG                                                    
               PIC X(0008).                                                     
           02  FM73-LNOMPROG                                                    
               PIC X(0030).                                                     
           02  FM73-CTYPE                                                       
               PIC X(0001).                                                     
           02  FM73-WTRT1                                                       
               PIC X(0001).                                                     
           02  FM73-WTRT2                                                       
               PIC X(0001).                                                     
           02  FM73-WSEMAINE                                                    
               PIC X(0007).                                                     
           02  FM73-NJRN                                                        
               PIC X(0003).                                                     
           02  FM73-NATURE                                                      
               PIC X(0003).                                                     
           02  FM73-CETAT                                                       
               PIC X(0008).                                                     
           02  FM73-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFM7300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFM7300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-CNOMPROG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-CNOMPROG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-LNOMPROG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-LNOMPROG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-WTRT1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-WTRT1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-WTRT2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-WTRT2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-WSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-WSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FM73-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FM73-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
