      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVFM9400                           *        
      ******************************************************************        
       01  RVFM9400.                                                            
      *                       CINTERFACE                                        
           10 FM94-CINTERFACE      PIC X(5).                                    
      *                       CNATOPER                                          
           10 FM94-CNATOPER        PIC X(5).                                    
      *                       CTYPOPER                                          
           10 FM94-CTYPOPER        PIC X(5).                                    
      *                       MONTANT                                           
           10 FM94-MONTANT         PIC S9(3)V USAGE COMP-3.                     
      *                       DUREE                                             
           10 FM94-DUREE           PIC S9(2)V USAGE COMP-3.                     
      *                       DELAI                                             
           10 FM94-DELAI           PIC S9(2)V USAGE COMP-3.                     
      *                       WPRO                                              
           10 FM94-WPRO            PIC X(1).                                    
      *                       WPOIDS                                            
           10 FM94-WPOIDS          PIC X(1).                                    
      *                       INFO1                                             
           10 FM94-INFO1           PIC X(20).                                   
      *                       INFO2                                             
           10 FM94-INFO2           PIC X(20).                                   
      *                       INFO3                                             
           10 FM94-INFO3           PIC X(20).                                   
      *                       TXANN1                                            
           10 FM94-TXANN1          PIC S9(3)V USAGE COMP-3.                     
      *                       TXANN2                                            
           10 FM94-TXANN2          PIC S9(3)V USAGE COMP-3.                     
      *                       TXANN3                                            
           10 FM94-TXANN3          PIC S9(3)V USAGE COMP-3.                     
      *                       TXANN4                                            
           10 FM94-TXANN4          PIC S9(3)V USAGE COMP-3.                     
      *                       TXANN5                                            
           10 FM94-TXANN5          PIC S9(3)V USAGE COMP-3.                     
      *                       DEFFET                                            
           10 FM94-DEFFET          PIC X(8).                                    
      *                       DMAJ                                              
           10 FM94-DMAJ            PIC X(8).                                    
      *                       DSYST                                             
           10 FM94-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
                                                                                
