      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSA0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSA0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSA0000.                                                            
           02  SA00-CINTERFACE                                                  
               PIC X(0005).                                                     
           02  SA00-NUMLOT                                                      
               PIC S9(0009) COMP-3.                                             
           02  SA00-DATEMAJ                                                     
               PIC X(0008).                                                     
           02  SA00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSA0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSA0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SA00-NUMLOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SA00-NUMLOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
