      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - REGLES COMPTABLES                                         00000020
      ***************************************************************** 00000030
       01   EFM74I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERFACEL   COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCINTERFACEF   PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCINTERFACEI   PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERFACEL   COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLINTERFACEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLINTERFACEF   PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLINTERFACEI   PIC X(25).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVAL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTVAF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTVAI    PIC X.                                          00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPOPERL     COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MCTYPOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPOPERF     PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCTYPOPERI     PIC X(5).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPOPERL     COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLTYPOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPOPERF     PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTYPOPERI     PIC X(25).                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGEOL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCGEOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCGEOF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCGEOI    PIC X.                                          00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNATOPERL     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MCNATOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCNATOPERF     PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCNATOPERI     PIC X(5).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNATOPERL     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MLNATOPERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNATOPERF     PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLNATOPERI     PIC X(25).                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWGROUPEL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MWGROUPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWGROUPEF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MWGROUPEI      PIC X.                                     00000590
           02 MTABI OCCURS   9 TIMES .                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MSELI   PIC X.                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPTEL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MCOMPTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMPTEF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MCOMPTEI     PIC X(6).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMPTEL    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MLCOMPTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMPTEF    PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MLCOMPTEI    PIC X(15).                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSENSL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MWSENSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWSENSF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MWSENSI      PIC X.                                     00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCUMULL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MWCUMULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWCUMULF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MWCUMULI     PIC X.                                     00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERSL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MNTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNTIERSF     PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MNTIERSI     PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLETTRAGEL  COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MNLETTRAGEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLETTRAGEF  PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MNLETTRAGEI  PIC X.                                     00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWANALL      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MWANALL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWANALF      PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MWANALI      PIC X.                                     00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTREPL    COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MCONTREPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCONTREPF    PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCONTREPI    PIC X(6).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCONTREPL   COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MLCONTREPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCONTREPF   PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MLCONTREPI   PIC X(15).                                 00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCUMULCL    COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MWCUMULCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWCUMULCF    PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MWCUMULCI    PIC X.                                     00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERSCL    COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MNTIERSCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNTIERSCF    PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MNTIERSCI    PIC X.                                     00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLETTRAGECL      COMP PIC S9(4).                       00001090
      *--                                                                       
             03 MNLETTRAGECL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MNLETTRAGECF      PIC X.                                00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MNLETTRAGECI      PIC X.                                00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWANALCL     COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MWANALCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWANALCF     PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MWANALCI     PIC X.                                     00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MDEFFETI     PIC X(10).                                 00001200
      * ZONE CMD AIDA                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLIBERRI  PIC X(79).                                      00001250
      * CODE TRANSACTION                                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      * CICS DE TRAVAIL                                                 00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCICSI    PIC X(5).                                       00001350
      * NETNAME                                                         00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MNETNAMI  PIC X(8).                                       00001400
      * CODE TERMINAL                                                   00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MSCREENI  PIC X(4).                                       00001450
      ***************************************************************** 00001460
      * GCT - REGLES COMPTABLES                                         00001470
      ***************************************************************** 00001480
       01   EFM74O REDEFINES EFM74I.                                    00001490
           02 FILLER    PIC X(12).                                      00001500
      * DATE DU JOUR                                                    00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATJOUA  PIC X.                                          00001530
           02 MDATJOUC  PIC X.                                          00001540
           02 MDATJOUP  PIC X.                                          00001550
           02 MDATJOUH  PIC X.                                          00001560
           02 MDATJOUV  PIC X.                                          00001570
           02 MDATJOUO  PIC X(10).                                      00001580
      * HEURE                                                           00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MTIMJOUA  PIC X.                                          00001610
           02 MTIMJOUC  PIC X.                                          00001620
           02 MTIMJOUP  PIC X.                                          00001630
           02 MTIMJOUH  PIC X.                                          00001640
           02 MTIMJOUV  PIC X.                                          00001650
           02 MTIMJOUO  PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MPAGEA    PIC X.                                          00001680
           02 MPAGEC    PIC X.                                          00001690
           02 MPAGEP    PIC X.                                          00001700
           02 MPAGEH    PIC X.                                          00001710
           02 MPAGEV    PIC X.                                          00001720
           02 MPAGEO    PIC Z9.                                         00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNBPA     PIC X.                                          00001750
           02 MNBPC     PIC X.                                          00001760
           02 MNBPP     PIC X.                                          00001770
           02 MNBPH     PIC X.                                          00001780
           02 MNBPV     PIC X.                                          00001790
           02 MNBPO     PIC Z9.                                         00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCINTERFACEA   PIC X.                                     00001820
           02 MCINTERFACEC   PIC X.                                     00001830
           02 MCINTERFACEP   PIC X.                                     00001840
           02 MCINTERFACEH   PIC X.                                     00001850
           02 MCINTERFACEV   PIC X.                                     00001860
           02 MCINTERFACEO   PIC X(5).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLINTERFACEA   PIC X.                                     00001890
           02 MLINTERFACEC   PIC X.                                     00001900
           02 MLINTERFACEP   PIC X.                                     00001910
           02 MLINTERFACEH   PIC X.                                     00001920
           02 MLINTERFACEV   PIC X.                                     00001930
           02 MLINTERFACEO   PIC X(25).                                 00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCTVAA    PIC X.                                          00001960
           02 MCTVAC    PIC X.                                          00001970
           02 MCTVAP    PIC X.                                          00001980
           02 MCTVAH    PIC X.                                          00001990
           02 MCTVAV    PIC X.                                          00002000
           02 MCTVAO    PIC X.                                          00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCTYPOPERA     PIC X.                                     00002030
           02 MCTYPOPERC     PIC X.                                     00002040
           02 MCTYPOPERP     PIC X.                                     00002050
           02 MCTYPOPERH     PIC X.                                     00002060
           02 MCTYPOPERV     PIC X.                                     00002070
           02 MCTYPOPERO     PIC X(5).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLTYPOPERA     PIC X.                                     00002100
           02 MLTYPOPERC     PIC X.                                     00002110
           02 MLTYPOPERP     PIC X.                                     00002120
           02 MLTYPOPERH     PIC X.                                     00002130
           02 MLTYPOPERV     PIC X.                                     00002140
           02 MLTYPOPERO     PIC X(25).                                 00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCGEOA    PIC X.                                          00002170
           02 MCGEOC    PIC X.                                          00002180
           02 MCGEOP    PIC X.                                          00002190
           02 MCGEOH    PIC X.                                          00002200
           02 MCGEOV    PIC X.                                          00002210
           02 MCGEOO    PIC X.                                          00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCNATOPERA     PIC X.                                     00002240
           02 MCNATOPERC     PIC X.                                     00002250
           02 MCNATOPERP     PIC X.                                     00002260
           02 MCNATOPERH     PIC X.                                     00002270
           02 MCNATOPERV     PIC X.                                     00002280
           02 MCNATOPERO     PIC X(5).                                  00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLNATOPERA     PIC X.                                     00002310
           02 MLNATOPERC     PIC X.                                     00002320
           02 MLNATOPERP     PIC X.                                     00002330
           02 MLNATOPERH     PIC X.                                     00002340
           02 MLNATOPERV     PIC X.                                     00002350
           02 MLNATOPERO     PIC X(25).                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MWGROUPEA      PIC X.                                     00002380
           02 MWGROUPEC PIC X.                                          00002390
           02 MWGROUPEP PIC X.                                          00002400
           02 MWGROUPEH PIC X.                                          00002410
           02 MWGROUPEV PIC X.                                          00002420
           02 MWGROUPEO      PIC X.                                     00002430
           02 MTABO OCCURS   9 TIMES .                                  00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MSELA   PIC X.                                          00002460
             03 MSELC   PIC X.                                          00002470
             03 MSELP   PIC X.                                          00002480
             03 MSELH   PIC X.                                          00002490
             03 MSELV   PIC X.                                          00002500
             03 MSELO   PIC X.                                          00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MCOMPTEA     PIC X.                                     00002530
             03 MCOMPTEC     PIC X.                                     00002540
             03 MCOMPTEP     PIC X.                                     00002550
             03 MCOMPTEH     PIC X.                                     00002560
             03 MCOMPTEV     PIC X.                                     00002570
             03 MCOMPTEO     PIC X(6).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MLCOMPTEA    PIC X.                                     00002600
             03 MLCOMPTEC    PIC X.                                     00002610
             03 MLCOMPTEP    PIC X.                                     00002620
             03 MLCOMPTEH    PIC X.                                     00002630
             03 MLCOMPTEV    PIC X.                                     00002640
             03 MLCOMPTEO    PIC X(15).                                 00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MWSENSA      PIC X.                                     00002670
             03 MWSENSC PIC X.                                          00002680
             03 MWSENSP PIC X.                                          00002690
             03 MWSENSH PIC X.                                          00002700
             03 MWSENSV PIC X.                                          00002710
             03 MWSENSO      PIC X.                                     00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MWCUMULA     PIC X.                                     00002740
             03 MWCUMULC     PIC X.                                     00002750
             03 MWCUMULP     PIC X.                                     00002760
             03 MWCUMULH     PIC X.                                     00002770
             03 MWCUMULV     PIC X.                                     00002780
             03 MWCUMULO     PIC X.                                     00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MNTIERSA     PIC X.                                     00002810
             03 MNTIERSC     PIC X.                                     00002820
             03 MNTIERSP     PIC X.                                     00002830
             03 MNTIERSH     PIC X.                                     00002840
             03 MNTIERSV     PIC X.                                     00002850
             03 MNTIERSO     PIC X.                                     00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MNLETTRAGEA  PIC X.                                     00002880
             03 MNLETTRAGEC  PIC X.                                     00002890
             03 MNLETTRAGEP  PIC X.                                     00002900
             03 MNLETTRAGEH  PIC X.                                     00002910
             03 MNLETTRAGEV  PIC X.                                     00002920
             03 MNLETTRAGEO  PIC X.                                     00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MWANALA      PIC X.                                     00002950
             03 MWANALC PIC X.                                          00002960
             03 MWANALP PIC X.                                          00002970
             03 MWANALH PIC X.                                          00002980
             03 MWANALV PIC X.                                          00002990
             03 MWANALO      PIC X.                                     00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MCONTREPA    PIC X.                                     00003020
             03 MCONTREPC    PIC X.                                     00003030
             03 MCONTREPP    PIC X.                                     00003040
             03 MCONTREPH    PIC X.                                     00003050
             03 MCONTREPV    PIC X.                                     00003060
             03 MCONTREPO    PIC X(6).                                  00003070
             03 FILLER       PIC X(2).                                  00003080
             03 MLCONTREPA   PIC X.                                     00003090
             03 MLCONTREPC   PIC X.                                     00003100
             03 MLCONTREPP   PIC X.                                     00003110
             03 MLCONTREPH   PIC X.                                     00003120
             03 MLCONTREPV   PIC X.                                     00003130
             03 MLCONTREPO   PIC X(15).                                 00003140
             03 FILLER       PIC X(2).                                  00003150
             03 MWCUMULCA    PIC X.                                     00003160
             03 MWCUMULCC    PIC X.                                     00003170
             03 MWCUMULCP    PIC X.                                     00003180
             03 MWCUMULCH    PIC X.                                     00003190
             03 MWCUMULCV    PIC X.                                     00003200
             03 MWCUMULCO    PIC X.                                     00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MNTIERSCA    PIC X.                                     00003230
             03 MNTIERSCC    PIC X.                                     00003240
             03 MNTIERSCP    PIC X.                                     00003250
             03 MNTIERSCH    PIC X.                                     00003260
             03 MNTIERSCV    PIC X.                                     00003270
             03 MNTIERSCO    PIC X.                                     00003280
             03 FILLER       PIC X(2).                                  00003290
             03 MNLETTRAGECA      PIC X.                                00003300
             03 MNLETTRAGECC PIC X.                                     00003310
             03 MNLETTRAGECP PIC X.                                     00003320
             03 MNLETTRAGECH PIC X.                                     00003330
             03 MNLETTRAGECV PIC X.                                     00003340
             03 MNLETTRAGECO      PIC X.                                00003350
             03 FILLER       PIC X(2).                                  00003360
             03 MWANALCA     PIC X.                                     00003370
             03 MWANALCC     PIC X.                                     00003380
             03 MWANALCP     PIC X.                                     00003390
             03 MWANALCH     PIC X.                                     00003400
             03 MWANALCV     PIC X.                                     00003410
             03 MWANALCO     PIC X.                                     00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MDEFFETA     PIC X.                                     00003440
             03 MDEFFETC     PIC X.                                     00003450
             03 MDEFFETP     PIC X.                                     00003460
             03 MDEFFETH     PIC X.                                     00003470
             03 MDEFFETV     PIC X.                                     00003480
             03 MDEFFETO     PIC X(10).                                 00003490
      * ZONE CMD AIDA                                                   00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MLIBERRA  PIC X.                                          00003520
           02 MLIBERRC  PIC X.                                          00003530
           02 MLIBERRP  PIC X.                                          00003540
           02 MLIBERRH  PIC X.                                          00003550
           02 MLIBERRV  PIC X.                                          00003560
           02 MLIBERRO  PIC X(79).                                      00003570
      * CODE TRANSACTION                                                00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
      * CICS DE TRAVAIL                                                 00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MCICSA    PIC X.                                          00003680
           02 MCICSC    PIC X.                                          00003690
           02 MCICSP    PIC X.                                          00003700
           02 MCICSH    PIC X.                                          00003710
           02 MCICSV    PIC X.                                          00003720
           02 MCICSO    PIC X(5).                                       00003730
      * NETNAME                                                         00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MNETNAMA  PIC X.                                          00003760
           02 MNETNAMC  PIC X.                                          00003770
           02 MNETNAMP  PIC X.                                          00003780
           02 MNETNAMH  PIC X.                                          00003790
           02 MNETNAMV  PIC X.                                          00003800
           02 MNETNAMO  PIC X(8).                                       00003810
      * CODE TERMINAL                                                   00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MSCREENA  PIC X.                                          00003840
           02 MSCREENC  PIC X.                                          00003850
           02 MSCREENP  PIC X.                                          00003860
           02 MSCREENH  PIC X.                                          00003870
           02 MSCREENV  PIC X.                                          00003880
           02 MSCREENO  PIC X(4).                                       00003890
                                                                                
