      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG29   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG29I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNLIEUL               PIC S9(4) COMP-5.
           02     MNLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUI               PIC X(3).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(19).
           02     MPAGEL                PIC S9(4) COMP-5.
           02     MPAGEF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEI                PIC XX.
           02     MPAGEMAL              PIC S9(4) COMP-5.
           02     MPAGEMAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEMAI              PIC XX.
           02     MSCOMPTL              PIC S9(4) COMP-5.
           02     MSCOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCOMPTI              PIC X(3).
           02     MECOMPTL              PIC S9(4) COMP-5.
           02     MECOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MECOMPTI              PIC X(3).
           02     MSELECTL              PIC S9(4) COMP-5.
           02     MSELECTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSELECTI              PIC X(1).
           02     DFH0001L              PIC S9(4) COMP-5.
           02     DFH0001F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0001I              PIC X(1).
           02     DFH0002L              PIC S9(4) COMP-5.
           02     DFH0002F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0002I              PIC X(1).
           02     MSECORIL              PIC S9(4) COMP-5.
           02     MSECORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECORII              PIC X(6).
           02     DFH0003L              PIC S9(4) COMP-5.
           02     DFH0003F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0003I              PIC X(6).
           02     DFH0004L              PIC S9(4) COMP-5.
           02     DFH0004F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0004I              PIC X(6).
           02     MSECTIOL              PIC S9(4) COMP-5.
           02     MSECTIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECTIOI              PIC X(6).
           02     DFH0005L              PIC S9(4) COMP-5.
           02     DFH0005F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0005I              PIC X(6).
           02     DFH0006L              PIC S9(4) COMP-5.
           02     DFH0006F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0006I              PIC X(6).
           02     DFH0007L              PIC S9(4) COMP-5.
           02     DFH0007F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0007I              PIC X(1).
           02     DFH0008L              PIC S9(4) COMP-5.
           02     DFH0008F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0008I              PIC X(1).
           02     DFH0009L              PIC S9(4) COMP-5.
           02     DFH0009F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0009I              PIC X(1).
           02     DFH0010L              PIC S9(4) COMP-5.
           02     DFH0010F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0010I              PIC X(6).
           02     DFH0011L              PIC S9(4) COMP-5.
           02     DFH0011F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0011I              PIC X(6).
           02     DFH0012L              PIC S9(4) COMP-5.
           02     DFH0012F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0012I              PIC X(6).
           02     DFH0013L              PIC S9(4) COMP-5.
           02     DFH0013F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0013I              PIC X(6).
           02     DFH0014L              PIC S9(4) COMP-5.
           02     DFH0014F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0014I              PIC X(6).
           02     DFH0015L              PIC S9(4) COMP-5.
           02     DFH0015F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0015I              PIC X(6).
           02     DFH0016L              PIC S9(4) COMP-5.
           02     DFH0016F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0016I              PIC X(1).
           02     DFH0017L              PIC S9(4) COMP-5.
           02     DFH0017F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0017I              PIC X(1).
           02     DFH0018L              PIC S9(4) COMP-5.
           02     DFH0018F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0018I              PIC X(1).
           02     DFH0019L              PIC S9(4) COMP-5.
           02     DFH0019F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0019I              PIC X(6).
           02     DFH0020L              PIC S9(4) COMP-5.
           02     DFH0020F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0020I              PIC X(6).
           02     DFH0021L              PIC S9(4) COMP-5.
           02     DFH0021F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0021I              PIC X(6).
           02     DFH0022L              PIC S9(4) COMP-5.
           02     DFH0022F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0022I              PIC X(6).
           02     DFH0023L              PIC S9(4) COMP-5.
           02     DFH0023F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0023I              PIC X(6).
           02     DFH0024L              PIC S9(4) COMP-5.
           02     DFH0024F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0024I              PIC X(6).
           02     DFH0025L              PIC S9(4) COMP-5.
           02     DFH0025F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0025I              PIC X(1).
           02     DFH0026L              PIC S9(4) COMP-5.
           02     DFH0026F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0026I              PIC X(1).
           02     DFH0027L              PIC S9(4) COMP-5.
           02     DFH0027F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0027I              PIC X(1).
           02     DFH0028L              PIC S9(4) COMP-5.
           02     DFH0028F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0028I              PIC X(6).
           02     DFH0029L              PIC S9(4) COMP-5.
           02     DFH0029F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0029I              PIC X(6).
           02     DFH0030L              PIC S9(4) COMP-5.
           02     DFH0030F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0030I              PIC X(6).
           02     DFH0031L              PIC S9(4) COMP-5.
           02     DFH0031F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0031I              PIC X(6).
           02     DFH0032L              PIC S9(4) COMP-5.
           02     DFH0032F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0032I              PIC X(6).
           02     DFH0033L              PIC S9(4) COMP-5.
           02     DFH0033F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0033I              PIC X(6).
           02     DFH0034L              PIC S9(4) COMP-5.
           02     DFH0034F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0034I              PIC X(1).
           02     DFH0035L              PIC S9(4) COMP-5.
           02     DFH0035F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0035I              PIC X(1).
           02     DFH0036L              PIC S9(4) COMP-5.
           02     DFH0036F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0036I              PIC X(1).
           02     DFH0037L              PIC S9(4) COMP-5.
           02     DFH0037F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0037I              PIC X(6).
           02     DFH0038L              PIC S9(4) COMP-5.
           02     DFH0038F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0038I              PIC X(6).
           02     DFH0039L              PIC S9(4) COMP-5.
           02     DFH0039F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0039I              PIC X(6).
           02     DFH0040L              PIC S9(4) COMP-5.
           02     DFH0040F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0040I              PIC X(6).
           02     DFH0041L              PIC S9(4) COMP-5.
           02     DFH0041F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0041I              PIC X(6).
           02     DFH0042L              PIC S9(4) COMP-5.
           02     DFH0042F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0042I              PIC X(6).
           02     DFH0043L              PIC S9(4) COMP-5.
           02     DFH0043F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0043I              PIC X(1).
           02     DFH0044L              PIC S9(4) COMP-5.
           02     DFH0044F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0044I              PIC X(1).
           02     DFH0045L              PIC S9(4) COMP-5.
           02     DFH0045F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0045I              PIC X(1).
           02     DFH0046L              PIC S9(4) COMP-5.
           02     DFH0046F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0046I              PIC X(6).
           02     DFH0047L              PIC S9(4) COMP-5.
           02     DFH0047F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0047I              PIC X(6).
           02     DFH0048L              PIC S9(4) COMP-5.
           02     DFH0048F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0048I              PIC X(6).
           02     DFH0049L              PIC S9(4) COMP-5.
           02     DFH0049F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0049I              PIC X(6).
           02     DFH0050L              PIC S9(4) COMP-5.
           02     DFH0050F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0050I              PIC X(6).
           02     DFH0051L              PIC S9(4) COMP-5.
           02     DFH0051F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0051I              PIC X(6).
           02     DFH0052L              PIC S9(4) COMP-5.
           02     DFH0052F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0052I              PIC X(1).
           02     DFH0053L              PIC S9(4) COMP-5.
           02     DFH0053F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0053I              PIC X(1).
           02     DFH0054L              PIC S9(4) COMP-5.
           02     DFH0054F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0054I              PIC X(1).
           02     DFH0055L              PIC S9(4) COMP-5.
           02     DFH0055F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0055I              PIC X(6).
           02     DFH0056L              PIC S9(4) COMP-5.
           02     DFH0056F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0056I              PIC X(6).
           02     DFH0057L              PIC S9(4) COMP-5.
           02     DFH0057F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0057I              PIC X(6).
           02     DFH0058L              PIC S9(4) COMP-5.
           02     DFH0058F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0058I              PIC X(6).
           02     DFH0059L              PIC S9(4) COMP-5.
           02     DFH0059F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0059I              PIC X(6).
           02     DFH0060L              PIC S9(4) COMP-5.
           02     DFH0060F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0060I              PIC X(6).
           02     DFH0061L              PIC S9(4) COMP-5.
           02     DFH0061F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0061I              PIC X(1).
           02     DFH0062L              PIC S9(4) COMP-5.
           02     DFH0062F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0062I              PIC X(1).
           02     DFH0063L              PIC S9(4) COMP-5.
           02     DFH0063F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0063I              PIC X(1).
           02     DFH0064L              PIC S9(4) COMP-5.
           02     DFH0064F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0064I              PIC X(6).
           02     DFH0065L              PIC S9(4) COMP-5.
           02     DFH0065F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0065I              PIC X(6).
           02     DFH0066L              PIC S9(4) COMP-5.
           02     DFH0066F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0066I              PIC X(6).
           02     DFH0067L              PIC S9(4) COMP-5.
           02     DFH0067F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0067I              PIC X(6).
           02     DFH0068L              PIC S9(4) COMP-5.
           02     DFH0068F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0068I              PIC X(6).
           02     DFH0069L              PIC S9(4) COMP-5.
           02     DFH0069F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0069I              PIC X(6).
           02     DFH0070L              PIC S9(4) COMP-5.
           02     DFH0070F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0070I              PIC X(1).
           02     DFH0071L              PIC S9(4) COMP-5.
           02     DFH0071F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0071I              PIC X(1).
           02     DFH0072L              PIC S9(4) COMP-5.
           02     DFH0072F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0072I              PIC X(1).
           02     DFH0073L              PIC S9(4) COMP-5.
           02     DFH0073F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0073I              PIC X(6).
           02     DFH0074L              PIC S9(4) COMP-5.
           02     DFH0074F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0074I              PIC X(6).
           02     DFH0075L              PIC S9(4) COMP-5.
           02     DFH0075F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0075I              PIC X(6).
           02     DFH0076L              PIC S9(4) COMP-5.
           02     DFH0076F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0076I              PIC X(6).
           02     DFH0077L              PIC S9(4) COMP-5.
           02     DFH0077F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0077I              PIC X(6).
           02     DFH0078L              PIC S9(4) COMP-5.
           02     DFH0078F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0078I              PIC X(6).
           02     DFH0079L              PIC S9(4) COMP-5.
           02     DFH0079F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0079I              PIC X(1).
           02     DFH0080L              PIC S9(4) COMP-5.
           02     DFH0080F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0080I              PIC X(1).
           02     DFH0081L              PIC S9(4) COMP-5.
           02     DFH0081F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0081I              PIC X(1).
           02     DFH0082L              PIC S9(4) COMP-5.
           02     DFH0082F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0082I              PIC X(6).
           02     DFH0083L              PIC S9(4) COMP-5.
           02     DFH0083F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0083I              PIC X(6).
           02     DFH0084L              PIC S9(4) COMP-5.
           02     DFH0084F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0084I              PIC X(6).
           02     DFH0085L              PIC S9(4) COMP-5.
           02     DFH0085F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0085I              PIC X(6).
           02     DFH0086L              PIC S9(4) COMP-5.
           02     DFH0086F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0086I              PIC X(6).
           02     DFH0087L              PIC S9(4) COMP-5.
           02     DFH0087F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0087I              PIC X(6).
           02     MSOCCOPL              PIC S9(4) COMP-5.
           02     MSOCCOPF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCCOPI              PIC X(3).
           02     MLIEUCOL              PIC S9(4) COMP-5.
           02     MLIEUCOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIEUCOI              PIC X(3).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(78).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(5).
       01  EFG29O REDEFINES EFG29I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUA               PIC X.
           02     MNLIEUC               PIC X.
           02     MNLIEUP               PIC X.
           02     MNLIEUH               PIC X.
           02     MNLIEUV               PIC X.
           02     MNLIEUO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(19).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEA                PIC X.
           02     MPAGEC                PIC X.
           02     MPAGEP                PIC X.
           02     MPAGEH                PIC X.
           02     MPAGEV                PIC X.
           02     MPAGEO                PIC 99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEMAA              PIC X.
           02     MPAGEMAC              PIC X.
           02     MPAGEMAP              PIC X.
           02     MPAGEMAH              PIC X.
           02     MPAGEMAV              PIC X.
           02     MPAGEMAO              PIC 99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCOMPTA              PIC X.
           02     MSCOMPTC              PIC X.
           02     MSCOMPTP              PIC X.
           02     MSCOMPTH              PIC X.
           02     MSCOMPTV              PIC X.
           02     MSCOMPTO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MECOMPTA              PIC X.
           02     MECOMPTC              PIC X.
           02     MECOMPTP              PIC X.
           02     MECOMPTH              PIC X.
           02     MECOMPTV              PIC X.
           02     MECOMPTO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSELECTA              PIC X.
           02     MSELECTC              PIC X.
           02     MSELECTP              PIC X.
           02     MSELECTH              PIC X.
           02     MSELECTV              PIC X.
           02     MSELECTO              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0001A              PIC X.
           02     DFH0001C              PIC X.
           02     DFH0001P              PIC X.
           02     DFH0001H              PIC X.
           02     DFH0001V              PIC X.
           02     DFH0001O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0002A              PIC X.
           02     DFH0002C              PIC X.
           02     DFH0002P              PIC X.
           02     DFH0002H              PIC X.
           02     DFH0002V              PIC X.
           02     DFH0002O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECORIA              PIC X.
           02     MSECORIC              PIC X.
           02     MSECORIP              PIC X.
           02     MSECORIH              PIC X.
           02     MSECORIV              PIC X.
           02     MSECORIO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0003A              PIC X.
           02     DFH0003C              PIC X.
           02     DFH0003P              PIC X.
           02     DFH0003H              PIC X.
           02     DFH0003V              PIC X.
           02     DFH0003O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0004A              PIC X.
           02     DFH0004C              PIC X.
           02     DFH0004P              PIC X.
           02     DFH0004H              PIC X.
           02     DFH0004V              PIC X.
           02     DFH0004O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECTIOA              PIC X.
           02     MSECTIOC              PIC X.
           02     MSECTIOP              PIC X.
           02     MSECTIOH              PIC X.
           02     MSECTIOV              PIC X.
           02     MSECTIOO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0005A              PIC X.
           02     DFH0005C              PIC X.
           02     DFH0005P              PIC X.
           02     DFH0005H              PIC X.
           02     DFH0005V              PIC X.
           02     DFH0005O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0006A              PIC X.
           02     DFH0006C              PIC X.
           02     DFH0006P              PIC X.
           02     DFH0006H              PIC X.
           02     DFH0006V              PIC X.
           02     DFH0006O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0007A              PIC X.
           02     DFH0007C              PIC X.
           02     DFH0007P              PIC X.
           02     DFH0007H              PIC X.
           02     DFH0007V              PIC X.
           02     DFH0007O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0008A              PIC X.
           02     DFH0008C              PIC X.
           02     DFH0008P              PIC X.
           02     DFH0008H              PIC X.
           02     DFH0008V              PIC X.
           02     DFH0008O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0009A              PIC X.
           02     DFH0009C              PIC X.
           02     DFH0009P              PIC X.
           02     DFH0009H              PIC X.
           02     DFH0009V              PIC X.
           02     DFH0009O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0010A              PIC X.
           02     DFH0010C              PIC X.
           02     DFH0010P              PIC X.
           02     DFH0010H              PIC X.
           02     DFH0010V              PIC X.
           02     DFH0010O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0011A              PIC X.
           02     DFH0011C              PIC X.
           02     DFH0011P              PIC X.
           02     DFH0011H              PIC X.
           02     DFH0011V              PIC X.
           02     DFH0011O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0012A              PIC X.
           02     DFH0012C              PIC X.
           02     DFH0012P              PIC X.
           02     DFH0012H              PIC X.
           02     DFH0012V              PIC X.
           02     DFH0012O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0013A              PIC X.
           02     DFH0013C              PIC X.
           02     DFH0013P              PIC X.
           02     DFH0013H              PIC X.
           02     DFH0013V              PIC X.
           02     DFH0013O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0014A              PIC X.
           02     DFH0014C              PIC X.
           02     DFH0014P              PIC X.
           02     DFH0014H              PIC X.
           02     DFH0014V              PIC X.
           02     DFH0014O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0015A              PIC X.
           02     DFH0015C              PIC X.
           02     DFH0015P              PIC X.
           02     DFH0015H              PIC X.
           02     DFH0015V              PIC X.
           02     DFH0015O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0016A              PIC X.
           02     DFH0016C              PIC X.
           02     DFH0016P              PIC X.
           02     DFH0016H              PIC X.
           02     DFH0016V              PIC X.
           02     DFH0016O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0017A              PIC X.
           02     DFH0017C              PIC X.
           02     DFH0017P              PIC X.
           02     DFH0017H              PIC X.
           02     DFH0017V              PIC X.
           02     DFH0017O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0018A              PIC X.
           02     DFH0018C              PIC X.
           02     DFH0018P              PIC X.
           02     DFH0018H              PIC X.
           02     DFH0018V              PIC X.
           02     DFH0018O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0019A              PIC X.
           02     DFH0019C              PIC X.
           02     DFH0019P              PIC X.
           02     DFH0019H              PIC X.
           02     DFH0019V              PIC X.
           02     DFH0019O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0020A              PIC X.
           02     DFH0020C              PIC X.
           02     DFH0020P              PIC X.
           02     DFH0020H              PIC X.
           02     DFH0020V              PIC X.
           02     DFH0020O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0021A              PIC X.
           02     DFH0021C              PIC X.
           02     DFH0021P              PIC X.
           02     DFH0021H              PIC X.
           02     DFH0021V              PIC X.
           02     DFH0021O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0022A              PIC X.
           02     DFH0022C              PIC X.
           02     DFH0022P              PIC X.
           02     DFH0022H              PIC X.
           02     DFH0022V              PIC X.
           02     DFH0022O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0023A              PIC X.
           02     DFH0023C              PIC X.
           02     DFH0023P              PIC X.
           02     DFH0023H              PIC X.
           02     DFH0023V              PIC X.
           02     DFH0023O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0024A              PIC X.
           02     DFH0024C              PIC X.
           02     DFH0024P              PIC X.
           02     DFH0024H              PIC X.
           02     DFH0024V              PIC X.
           02     DFH0024O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0025A              PIC X.
           02     DFH0025C              PIC X.
           02     DFH0025P              PIC X.
           02     DFH0025H              PIC X.
           02     DFH0025V              PIC X.
           02     DFH0025O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0026A              PIC X.
           02     DFH0026C              PIC X.
           02     DFH0026P              PIC X.
           02     DFH0026H              PIC X.
           02     DFH0026V              PIC X.
           02     DFH0026O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0027A              PIC X.
           02     DFH0027C              PIC X.
           02     DFH0027P              PIC X.
           02     DFH0027H              PIC X.
           02     DFH0027V              PIC X.
           02     DFH0027O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0028A              PIC X.
           02     DFH0028C              PIC X.
           02     DFH0028P              PIC X.
           02     DFH0028H              PIC X.
           02     DFH0028V              PIC X.
           02     DFH0028O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0029A              PIC X.
           02     DFH0029C              PIC X.
           02     DFH0029P              PIC X.
           02     DFH0029H              PIC X.
           02     DFH0029V              PIC X.
           02     DFH0029O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0030A              PIC X.
           02     DFH0030C              PIC X.
           02     DFH0030P              PIC X.
           02     DFH0030H              PIC X.
           02     DFH0030V              PIC X.
           02     DFH0030O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0031A              PIC X.
           02     DFH0031C              PIC X.
           02     DFH0031P              PIC X.
           02     DFH0031H              PIC X.
           02     DFH0031V              PIC X.
           02     DFH0031O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0032A              PIC X.
           02     DFH0032C              PIC X.
           02     DFH0032P              PIC X.
           02     DFH0032H              PIC X.
           02     DFH0032V              PIC X.
           02     DFH0032O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0033A              PIC X.
           02     DFH0033C              PIC X.
           02     DFH0033P              PIC X.
           02     DFH0033H              PIC X.
           02     DFH0033V              PIC X.
           02     DFH0033O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0034A              PIC X.
           02     DFH0034C              PIC X.
           02     DFH0034P              PIC X.
           02     DFH0034H              PIC X.
           02     DFH0034V              PIC X.
           02     DFH0034O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0035A              PIC X.
           02     DFH0035C              PIC X.
           02     DFH0035P              PIC X.
           02     DFH0035H              PIC X.
           02     DFH0035V              PIC X.
           02     DFH0035O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0036A              PIC X.
           02     DFH0036C              PIC X.
           02     DFH0036P              PIC X.
           02     DFH0036H              PIC X.
           02     DFH0036V              PIC X.
           02     DFH0036O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0037A              PIC X.
           02     DFH0037C              PIC X.
           02     DFH0037P              PIC X.
           02     DFH0037H              PIC X.
           02     DFH0037V              PIC X.
           02     DFH0037O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0038A              PIC X.
           02     DFH0038C              PIC X.
           02     DFH0038P              PIC X.
           02     DFH0038H              PIC X.
           02     DFH0038V              PIC X.
           02     DFH0038O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0039A              PIC X.
           02     DFH0039C              PIC X.
           02     DFH0039P              PIC X.
           02     DFH0039H              PIC X.
           02     DFH0039V              PIC X.
           02     DFH0039O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0040A              PIC X.
           02     DFH0040C              PIC X.
           02     DFH0040P              PIC X.
           02     DFH0040H              PIC X.
           02     DFH0040V              PIC X.
           02     DFH0040O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0041A              PIC X.
           02     DFH0041C              PIC X.
           02     DFH0041P              PIC X.
           02     DFH0041H              PIC X.
           02     DFH0041V              PIC X.
           02     DFH0041O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0042A              PIC X.
           02     DFH0042C              PIC X.
           02     DFH0042P              PIC X.
           02     DFH0042H              PIC X.
           02     DFH0042V              PIC X.
           02     DFH0042O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0043A              PIC X.
           02     DFH0043C              PIC X.
           02     DFH0043P              PIC X.
           02     DFH0043H              PIC X.
           02     DFH0043V              PIC X.
           02     DFH0043O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0044A              PIC X.
           02     DFH0044C              PIC X.
           02     DFH0044P              PIC X.
           02     DFH0044H              PIC X.
           02     DFH0044V              PIC X.
           02     DFH0044O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0045A              PIC X.
           02     DFH0045C              PIC X.
           02     DFH0045P              PIC X.
           02     DFH0045H              PIC X.
           02     DFH0045V              PIC X.
           02     DFH0045O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0046A              PIC X.
           02     DFH0046C              PIC X.
           02     DFH0046P              PIC X.
           02     DFH0046H              PIC X.
           02     DFH0046V              PIC X.
           02     DFH0046O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0047A              PIC X.
           02     DFH0047C              PIC X.
           02     DFH0047P              PIC X.
           02     DFH0047H              PIC X.
           02     DFH0047V              PIC X.
           02     DFH0047O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0048A              PIC X.
           02     DFH0048C              PIC X.
           02     DFH0048P              PIC X.
           02     DFH0048H              PIC X.
           02     DFH0048V              PIC X.
           02     DFH0048O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0049A              PIC X.
           02     DFH0049C              PIC X.
           02     DFH0049P              PIC X.
           02     DFH0049H              PIC X.
           02     DFH0049V              PIC X.
           02     DFH0049O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0050A              PIC X.
           02     DFH0050C              PIC X.
           02     DFH0050P              PIC X.
           02     DFH0050H              PIC X.
           02     DFH0050V              PIC X.
           02     DFH0050O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0051A              PIC X.
           02     DFH0051C              PIC X.
           02     DFH0051P              PIC X.
           02     DFH0051H              PIC X.
           02     DFH0051V              PIC X.
           02     DFH0051O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0052A              PIC X.
           02     DFH0052C              PIC X.
           02     DFH0052P              PIC X.
           02     DFH0052H              PIC X.
           02     DFH0052V              PIC X.
           02     DFH0052O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0053A              PIC X.
           02     DFH0053C              PIC X.
           02     DFH0053P              PIC X.
           02     DFH0053H              PIC X.
           02     DFH0053V              PIC X.
           02     DFH0053O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0054A              PIC X.
           02     DFH0054C              PIC X.
           02     DFH0054P              PIC X.
           02     DFH0054H              PIC X.
           02     DFH0054V              PIC X.
           02     DFH0054O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0055A              PIC X.
           02     DFH0055C              PIC X.
           02     DFH0055P              PIC X.
           02     DFH0055H              PIC X.
           02     DFH0055V              PIC X.
           02     DFH0055O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0056A              PIC X.
           02     DFH0056C              PIC X.
           02     DFH0056P              PIC X.
           02     DFH0056H              PIC X.
           02     DFH0056V              PIC X.
           02     DFH0056O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0057A              PIC X.
           02     DFH0057C              PIC X.
           02     DFH0057P              PIC X.
           02     DFH0057H              PIC X.
           02     DFH0057V              PIC X.
           02     DFH0057O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0058A              PIC X.
           02     DFH0058C              PIC X.
           02     DFH0058P              PIC X.
           02     DFH0058H              PIC X.
           02     DFH0058V              PIC X.
           02     DFH0058O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0059A              PIC X.
           02     DFH0059C              PIC X.
           02     DFH0059P              PIC X.
           02     DFH0059H              PIC X.
           02     DFH0059V              PIC X.
           02     DFH0059O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0060A              PIC X.
           02     DFH0060C              PIC X.
           02     DFH0060P              PIC X.
           02     DFH0060H              PIC X.
           02     DFH0060V              PIC X.
           02     DFH0060O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0061A              PIC X.
           02     DFH0061C              PIC X.
           02     DFH0061P              PIC X.
           02     DFH0061H              PIC X.
           02     DFH0061V              PIC X.
           02     DFH0061O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0062A              PIC X.
           02     DFH0062C              PIC X.
           02     DFH0062P              PIC X.
           02     DFH0062H              PIC X.
           02     DFH0062V              PIC X.
           02     DFH0062O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0063A              PIC X.
           02     DFH0063C              PIC X.
           02     DFH0063P              PIC X.
           02     DFH0063H              PIC X.
           02     DFH0063V              PIC X.
           02     DFH0063O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0064A              PIC X.
           02     DFH0064C              PIC X.
           02     DFH0064P              PIC X.
           02     DFH0064H              PIC X.
           02     DFH0064V              PIC X.
           02     DFH0064O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0065A              PIC X.
           02     DFH0065C              PIC X.
           02     DFH0065P              PIC X.
           02     DFH0065H              PIC X.
           02     DFH0065V              PIC X.
           02     DFH0065O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0066A              PIC X.
           02     DFH0066C              PIC X.
           02     DFH0066P              PIC X.
           02     DFH0066H              PIC X.
           02     DFH0066V              PIC X.
           02     DFH0066O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0067A              PIC X.
           02     DFH0067C              PIC X.
           02     DFH0067P              PIC X.
           02     DFH0067H              PIC X.
           02     DFH0067V              PIC X.
           02     DFH0067O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0068A              PIC X.
           02     DFH0068C              PIC X.
           02     DFH0068P              PIC X.
           02     DFH0068H              PIC X.
           02     DFH0068V              PIC X.
           02     DFH0068O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0069A              PIC X.
           02     DFH0069C              PIC X.
           02     DFH0069P              PIC X.
           02     DFH0069H              PIC X.
           02     DFH0069V              PIC X.
           02     DFH0069O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0070A              PIC X.
           02     DFH0070C              PIC X.
           02     DFH0070P              PIC X.
           02     DFH0070H              PIC X.
           02     DFH0070V              PIC X.
           02     DFH0070O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0071A              PIC X.
           02     DFH0071C              PIC X.
           02     DFH0071P              PIC X.
           02     DFH0071H              PIC X.
           02     DFH0071V              PIC X.
           02     DFH0071O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0072A              PIC X.
           02     DFH0072C              PIC X.
           02     DFH0072P              PIC X.
           02     DFH0072H              PIC X.
           02     DFH0072V              PIC X.
           02     DFH0072O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0073A              PIC X.
           02     DFH0073C              PIC X.
           02     DFH0073P              PIC X.
           02     DFH0073H              PIC X.
           02     DFH0073V              PIC X.
           02     DFH0073O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0074A              PIC X.
           02     DFH0074C              PIC X.
           02     DFH0074P              PIC X.
           02     DFH0074H              PIC X.
           02     DFH0074V              PIC X.
           02     DFH0074O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0075A              PIC X.
           02     DFH0075C              PIC X.
           02     DFH0075P              PIC X.
           02     DFH0075H              PIC X.
           02     DFH0075V              PIC X.
           02     DFH0075O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0076A              PIC X.
           02     DFH0076C              PIC X.
           02     DFH0076P              PIC X.
           02     DFH0076H              PIC X.
           02     DFH0076V              PIC X.
           02     DFH0076O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0077A              PIC X.
           02     DFH0077C              PIC X.
           02     DFH0077P              PIC X.
           02     DFH0077H              PIC X.
           02     DFH0077V              PIC X.
           02     DFH0077O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0078A              PIC X.
           02     DFH0078C              PIC X.
           02     DFH0078P              PIC X.
           02     DFH0078H              PIC X.
           02     DFH0078V              PIC X.
           02     DFH0078O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0079A              PIC X.
           02     DFH0079C              PIC X.
           02     DFH0079P              PIC X.
           02     DFH0079H              PIC X.
           02     DFH0079V              PIC X.
           02     DFH0079O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0080A              PIC X.
           02     DFH0080C              PIC X.
           02     DFH0080P              PIC X.
           02     DFH0080H              PIC X.
           02     DFH0080V              PIC X.
           02     DFH0080O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0081A              PIC X.
           02     DFH0081C              PIC X.
           02     DFH0081P              PIC X.
           02     DFH0081H              PIC X.
           02     DFH0081V              PIC X.
           02     DFH0081O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0082A              PIC X.
           02     DFH0082C              PIC X.
           02     DFH0082P              PIC X.
           02     DFH0082H              PIC X.
           02     DFH0082V              PIC X.
           02     DFH0082O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0083A              PIC X.
           02     DFH0083C              PIC X.
           02     DFH0083P              PIC X.
           02     DFH0083H              PIC X.
           02     DFH0083V              PIC X.
           02     DFH0083O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0084A              PIC X.
           02     DFH0084C              PIC X.
           02     DFH0084P              PIC X.
           02     DFH0084H              PIC X.
           02     DFH0084V              PIC X.
           02     DFH0084O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0085A              PIC X.
           02     DFH0085C              PIC X.
           02     DFH0085P              PIC X.
           02     DFH0085H              PIC X.
           02     DFH0085V              PIC X.
           02     DFH0085O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0086A              PIC X.
           02     DFH0086C              PIC X.
           02     DFH0086P              PIC X.
           02     DFH0086H              PIC X.
           02     DFH0086V              PIC X.
           02     DFH0086O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0087A              PIC X.
           02     DFH0087C              PIC X.
           02     DFH0087P              PIC X.
           02     DFH0087H              PIC X.
           02     DFH0087V              PIC X.
           02     DFH0087O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCCOPA              PIC X.
           02     MSOCCOPC              PIC X.
           02     MSOCCOPP              PIC X.
           02     MSOCCOPH              PIC X.
           02     MSOCCOPV              PIC X.
           02     MSOCCOPO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIEUCOA              PIC X.
           02     MLIEUCOC              PIC X.
           02     MLIEUCOP              PIC X.
           02     MLIEUCOH              PIC X.
           02     MLIEUCOV              PIC X.
           02     MLIEUCOO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(78).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(5).
