      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG11   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG11I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MFONCL                PIC S9(4) COMP-5.
           02     MFONCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MFONCI                PIC X(3).
           02     MPAGEL                PIC S9(4) COMP-5.
           02     MPAGEF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEI                PIC X(3).
           02     MPAGEMAL              PIC S9(4) COMP-5.
           02     MPAGEMAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEMAI              PIC X(3).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNLIEUL               PIC S9(4) COMP-5.
           02     MNLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUI               PIC X(3).
           02     MSECORIL              PIC S9(4) COMP-5.
           02     MSECORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECORII              PIC X(6).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(20).
           02     MTYPNATL              PIC S9(4) COMP-5.
           02     MTYPNATF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTYPNATI              PIC X(30).
           02     MTIERSEL              PIC S9(4) COMP-5.
           02     MTIERSEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIERSEI              PIC X(8).
           02     MSOCCL                PIC S9(4) COMP-5.
           02     MSOCCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCCI                PIC X(3).
           02     MSERVL                PIC S9(4) COMP-5.
           02     MSERVF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSERVI                PIC X(5).
           02     MLSOCCL               PIC S9(4) COMP-5.
           02     MLSOCCF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCI               PIC X(20).
           02     MNUMFACL              PIC S9(4) COMP-5.
           02     MNUMFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMFACI              PIC X(7).
           02     MDCREATL              PIC S9(4) COMP-5.
           02     MDCREATF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDCREATI              PIC X(11).
           02     MSOCDESL              PIC S9(4) COMP-5.
           02     MSOCDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCDESI              PIC X(3).
           02     MLIEUDEL              PIC S9(4) COMP-5.
           02     MLIEUDEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIEUDEI              PIC X(3).
           02     MSECDESL              PIC S9(4) COMP-5.
           02     MSECDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECDESI              PIC X(6).
           02     MLIBDESL              PIC S9(4) COMP-5.
           02     MLIBDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBDESI              PIC X(20).
           02     MLETTREL              PIC S9(4) COMP-5.
           02     MLETTREF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLETTREI              PIC X(10).
           02     MTIERSRL              PIC S9(4) COMP-5.
           02     MTIERSRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIERSRI              PIC X(8).
           02     MSERVDEL              PIC S9(4) COMP-5.
           02     MSERVDEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSERVDEI              PIC X(5).
           02     MLETTRRL              PIC S9(4) COMP-5.
           02     MLETTRRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLETTRRI              PIC X(10).
           02     MLIBVALL              PIC S9(4) COMP-5.
           02     MLIBVALF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBVALI              PIC X(12).
           02     MNUMORIL              PIC S9(4) COMP-5.
           02     MNUMORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMORII              PIC X(7).
           02     MDCOMPTL              PIC S9(4) COMP-5.
           02     MDCOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDCOMPTI              PIC X(10).
           02     MDECHEAL              PIC S9(4) COMP-5.
           02     MDECHEAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDECHEAI              PIC X(10).
           02     DFH0001L              PIC S9(4) COMP-5.
           02     DFH0001F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0001I              PIC X(9).
           02     MDORIGL               PIC S9(4) COMP-5.
           02     MDORIGF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MDORIGI               PIC X(10).
           02     MEXERCIL              PIC S9(4) COMP-5.
           02     MEXERCIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MEXERCII              PIC X(4).
           02     MNPERIOL              PIC S9(4) COMP-5.
           02     MNPERIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNPERIOI              PIC X(3).
           02     MLPERIOL              PIC S9(4) COMP-5.
           02     MLPERIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLPERIOI              PIC X(14).
           02     MDPASSAL              PIC S9(4) COMP-5.
           02     MDPASSAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDPASSAI              PIC X(10).
           02     MLIBCRIL              PIC S9(4) COMP-5.
           02     MLIBCRIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBCRII              PIC X(5).
           02     MLIBTVAL              PIC S9(4) COMP-5.
           02     MLIBTVAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBTVAI              PIC X(3).
           02     MCRITERL              PIC S9(4) COMP-5.
           02     MCRITERF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCRITERI              PIC X(5).
           02     MQTEL                 PIC S9(4) COMP-5.
           02     MQTEF                 PIC X.
           02     DFHMS                 PIC X(4).
           02     MQTEI                 PIC X(5).
           02     MPUHTL                PIC S9(4) COMP-5.
           02     MPUHTF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MPUHTI                PIC X(13).
           02     MTTVAL                PIC S9(4) COMP-5.
           02     MTTVAF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MTTVAI                PIC X(1).
           02     MMONTHTL              PIC S9(4) COMP-5.
           02     MMONTHTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MMONTHTI              PIC X(13).
           02     MDESIGNL              PIC S9(4) COMP-5.
           02     MDESIGNF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDESIGNI              PIC X(37).
           02     DFH0002L              PIC S9(4) COMP-5.
           02     DFH0002F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0002I              PIC X(5).
           02     DFH0003L              PIC S9(4) COMP-5.
           02     DFH0003F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0003I              PIC X(5).
           02     DFH0004L              PIC S9(4) COMP-5.
           02     DFH0004F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0004I              PIC X(13).
           02     DFH0005L              PIC S9(4) COMP-5.
           02     DFH0005F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0005I              PIC X(1).
           02     DFH0006L              PIC S9(4) COMP-5.
           02     DFH0006F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0006I              PIC X(13).
           02     DFH0007L              PIC S9(4) COMP-5.
           02     DFH0007F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0007I              PIC X(37).
           02     DFH0008L              PIC S9(4) COMP-5.
           02     DFH0008F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0008I              PIC X(5).
           02     DFH0009L              PIC S9(4) COMP-5.
           02     DFH0009F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0009I              PIC X(5).
           02     DFH0010L              PIC S9(4) COMP-5.
           02     DFH0010F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0010I              PIC X(13).
           02     DFH0011L              PIC S9(4) COMP-5.
           02     DFH0011F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0011I              PIC X(1).
           02     DFH0012L              PIC S9(4) COMP-5.
           02     DFH0012F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0012I              PIC X(13).
           02     DFH0013L              PIC S9(4) COMP-5.
           02     DFH0013F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0013I              PIC X(37).
           02     DFH0014L              PIC S9(4) COMP-5.
           02     DFH0014F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0014I              PIC X(5).
           02     DFH0015L              PIC S9(4) COMP-5.
           02     DFH0015F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0015I              PIC X(5).
           02     DFH0016L              PIC S9(4) COMP-5.
           02     DFH0016F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0016I              PIC X(13).
           02     DFH0017L              PIC S9(4) COMP-5.
           02     DFH0017F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0017I              PIC X(1).
           02     DFH0018L              PIC S9(4) COMP-5.
           02     DFH0018F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0018I              PIC X(13).
           02     DFH0019L              PIC S9(4) COMP-5.
           02     DFH0019F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0019I              PIC X(37).
           02     DFH0020L              PIC S9(4) COMP-5.
           02     DFH0020F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0020I              PIC X(5).
           02     DFH0021L              PIC S9(4) COMP-5.
           02     DFH0021F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0021I              PIC X(5).
           02     DFH0022L              PIC S9(4) COMP-5.
           02     DFH0022F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0022I              PIC X(13).
           02     DFH0023L              PIC S9(4) COMP-5.
           02     DFH0023F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0023I              PIC X(1).
           02     DFH0024L              PIC S9(4) COMP-5.
           02     DFH0024F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0024I              PIC X(13).
           02     DFH0025L              PIC S9(4) COMP-5.
           02     DFH0025F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0025I              PIC X(37).
           02     DFH0026L              PIC S9(4) COMP-5.
           02     DFH0026F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0026I              PIC X(5).
           02     DFH0027L              PIC S9(4) COMP-5.
           02     DFH0027F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0027I              PIC X(5).
           02     DFH0028L              PIC S9(4) COMP-5.
           02     DFH0028F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0028I              PIC X(13).
           02     DFH0029L              PIC S9(4) COMP-5.
           02     DFH0029F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0029I              PIC X(1).
           02     DFH0030L              PIC S9(4) COMP-5.
           02     DFH0030F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0030I              PIC X(13).
           02     DFH0031L              PIC S9(4) COMP-5.
           02     DFH0031F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0031I              PIC X(37).
           02     DFH0032L              PIC S9(4) COMP-5.
           02     DFH0032F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0032I              PIC X(5).
           02     DFH0033L              PIC S9(4) COMP-5.
           02     DFH0033F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0033I              PIC X(5).
           02     DFH0034L              PIC S9(4) COMP-5.
           02     DFH0034F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0034I              PIC X(13).
           02     DFH0035L              PIC S9(4) COMP-5.
           02     DFH0035F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0035I              PIC X(1).
           02     DFH0036L              PIC S9(4) COMP-5.
           02     DFH0036F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0036I              PIC X(13).
           02     DFH0037L              PIC S9(4) COMP-5.
           02     DFH0037F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0037I              PIC X(37).
           02     DFH0038L              PIC S9(4) COMP-5.
           02     DFH0038F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0038I              PIC X(5).
           02     DFH0039L              PIC S9(4) COMP-5.
           02     DFH0039F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0039I              PIC X(5).
           02     DFH0040L              PIC S9(4) COMP-5.
           02     DFH0040F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0040I              PIC X(13).
           02     DFH0041L              PIC S9(4) COMP-5.
           02     DFH0041F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0041I              PIC X(1).
           02     DFH0042L              PIC S9(4) COMP-5.
           02     DFH0042F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0042I              PIC X(13).
           02     DFH0043L              PIC S9(4) COMP-5.
           02     DFH0043F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0043I              PIC X(37).
           02     MLIBTESL              PIC S9(4) COMP-5.
           02     MLIBTESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBTESI              PIC X(13).
           02     MTESCL                PIC S9(4) COMP-5.
           02     MTESCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MTESCI                PIC X(5).
           02     MLMTESCL              PIC S9(4) COMP-5.
           02     MLMTESCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLMTESCI              PIC X(3).
           02     MTOTESCL              PIC S9(4) COMP-5.
           02     MTOTESCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTOTESCI              PIC X(13).
           02     MTOTHTL               PIC S9(4) COMP-5.
           02     MTOTHTF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MTOTHTI               PIC X(14).
           02     MLTVAL                PIC S9(4) COMP-5.
           02     MLTVAF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MLTVAI                PIC X(3).
           02     MTOTTVAL              PIC S9(4) COMP-5.
           02     MTOTTVAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTOTTVAI              PIC X(13).
           02     MLTTCL                PIC S9(4) COMP-5.
           02     MLTTCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MLTTCI                PIC X(3).
           02     MTOTTTCL              PIC S9(4) COMP-5.
           02     MTOTTTCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTOTTTCI              PIC X(15).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONCMDI              PIC X(15).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(58).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(4).
       01  EFG11O REDEFINES EFG11I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MFONCA                PIC X.
           02     MFONCC                PIC X.
           02     MFONCP                PIC X.
           02     MFONCH                PIC X.
           02     MFONCV                PIC X.
           02     MFONCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEA                PIC X.
           02     MPAGEC                PIC X.
           02     MPAGEP                PIC X.
           02     MPAGEH                PIC X.
           02     MPAGEV                PIC X.
           02     MPAGEO                PIC 999.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEMAA              PIC X.
           02     MPAGEMAC              PIC X.
           02     MPAGEMAP              PIC X.
           02     MPAGEMAH              PIC X.
           02     MPAGEMAV              PIC X.
           02     MPAGEMAO              PIC 999.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUA               PIC X.
           02     MNLIEUC               PIC X.
           02     MNLIEUP               PIC X.
           02     MNLIEUH               PIC X.
           02     MNLIEUV               PIC X.
           02     MNLIEUO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECORIA              PIC X.
           02     MSECORIC              PIC X.
           02     MSECORIP              PIC X.
           02     MSECORIH              PIC X.
           02     MSECORIV              PIC X.
           02     MSECORIO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTYPNATA              PIC X.
           02     MTYPNATC              PIC X.
           02     MTYPNATP              PIC X.
           02     MTYPNATH              PIC X.
           02     MTYPNATV              PIC X.
           02     MTYPNATO              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIERSEA              PIC X.
           02     MTIERSEC              PIC X.
           02     MTIERSEP              PIC X.
           02     MTIERSEH              PIC X.
           02     MTIERSEV              PIC X.
           02     MTIERSEO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCCA                PIC X.
           02     MSOCCC                PIC X.
           02     MSOCCP                PIC X.
           02     MSOCCH                PIC X.
           02     MSOCCV                PIC X.
           02     MSOCCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSERVA                PIC X.
           02     MSERVC                PIC X.
           02     MSERVP                PIC X.
           02     MSERVH                PIC X.
           02     MSERVV                PIC X.
           02     MSERVO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCA               PIC X.
           02     MLSOCCC               PIC X.
           02     MLSOCCP               PIC X.
           02     MLSOCCH               PIC X.
           02     MLSOCCV               PIC X.
           02     MLSOCCO               PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMFACA              PIC X.
           02     MNUMFACC              PIC X.
           02     MNUMFACP              PIC X.
           02     MNUMFACH              PIC X.
           02     MNUMFACV              PIC X.
           02     MNUMFACO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDCREATA              PIC X.
           02     MDCREATC              PIC X.
           02     MDCREATP              PIC X.
           02     MDCREATH              PIC X.
           02     MDCREATV              PIC X.
           02     MDCREATO              PIC X(11).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCDESA              PIC X.
           02     MSOCDESC              PIC X.
           02     MSOCDESP              PIC X.
           02     MSOCDESH              PIC X.
           02     MSOCDESV              PIC X.
           02     MSOCDESO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIEUDEA              PIC X.
           02     MLIEUDEC              PIC X.
           02     MLIEUDEP              PIC X.
           02     MLIEUDEH              PIC X.
           02     MLIEUDEV              PIC X.
           02     MLIEUDEO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECDESA              PIC X.
           02     MSECDESC              PIC X.
           02     MSECDESP              PIC X.
           02     MSECDESH              PIC X.
           02     MSECDESV              PIC X.
           02     MSECDESO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBDESA              PIC X.
           02     MLIBDESC              PIC X.
           02     MLIBDESP              PIC X.
           02     MLIBDESH              PIC X.
           02     MLIBDESV              PIC X.
           02     MLIBDESO              PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLETTREA              PIC X.
           02     MLETTREC              PIC X.
           02     MLETTREP              PIC X.
           02     MLETTREH              PIC X.
           02     MLETTREV              PIC X.
           02     MLETTREO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIERSRA              PIC X.
           02     MTIERSRC              PIC X.
           02     MTIERSRP              PIC X.
           02     MTIERSRH              PIC X.
           02     MTIERSRV              PIC X.
           02     MTIERSRO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSERVDEA              PIC X.
           02     MSERVDEC              PIC X.
           02     MSERVDEP              PIC X.
           02     MSERVDEH              PIC X.
           02     MSERVDEV              PIC X.
           02     MSERVDEO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLETTRRA              PIC X.
           02     MLETTRRC              PIC X.
           02     MLETTRRP              PIC X.
           02     MLETTRRH              PIC X.
           02     MLETTRRV              PIC X.
           02     MLETTRRO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBVALA              PIC X.
           02     MLIBVALC              PIC X.
           02     MLIBVALP              PIC X.
           02     MLIBVALH              PIC X.
           02     MLIBVALV              PIC X.
           02     MLIBVALO              PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMORIA              PIC X.
           02     MNUMORIC              PIC X.
           02     MNUMORIP              PIC X.
           02     MNUMORIH              PIC X.
           02     MNUMORIV              PIC X.
           02     MNUMORIO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDCOMPTA              PIC X.
           02     MDCOMPTC              PIC X.
           02     MDCOMPTP              PIC X.
           02     MDCOMPTH              PIC X.
           02     MDCOMPTV              PIC X.
           02     MDCOMPTO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDECHEAA              PIC X.
           02     MDECHEAC              PIC X.
           02     MDECHEAP              PIC X.
           02     MDECHEAH              PIC X.
           02     MDECHEAV              PIC X.
           02     MDECHEAO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0001A              PIC X.
           02     DFH0001C              PIC X.
           02     DFH0001P              PIC X.
           02     DFH0001H              PIC X.
           02     DFH0001V              PIC X.
           02     DFH0001O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDORIGA               PIC X.
           02     MDORIGC               PIC X.
           02     MDORIGP               PIC X.
           02     MDORIGH               PIC X.
           02     MDORIGV               PIC X.
           02     MDORIGO               PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MEXERCIA              PIC X.
           02     MEXERCIC              PIC X.
           02     MEXERCIP              PIC X.
           02     MEXERCIH              PIC X.
           02     MEXERCIV              PIC X.
           02     MEXERCIO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNPERIOA              PIC X.
           02     MNPERIOC              PIC X.
           02     MNPERIOP              PIC X.
           02     MNPERIOH              PIC X.
           02     MNPERIOV              PIC X.
           02     MNPERIOO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLPERIOA              PIC X.
           02     MLPERIOC              PIC X.
           02     MLPERIOP              PIC X.
           02     MLPERIOH              PIC X.
           02     MLPERIOV              PIC X.
           02     MLPERIOO              PIC X(14).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDPASSAA              PIC X.
           02     MDPASSAC              PIC X.
           02     MDPASSAP              PIC X.
           02     MDPASSAH              PIC X.
           02     MDPASSAV              PIC X.
           02     MDPASSAO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBCRIA              PIC X.
           02     MLIBCRIC              PIC X.
           02     MLIBCRIP              PIC X.
           02     MLIBCRIH              PIC X.
           02     MLIBCRIV              PIC X.
           02     MLIBCRIO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBTVAA              PIC X.
           02     MLIBTVAC              PIC X.
           02     MLIBTVAP              PIC X.
           02     MLIBTVAH              PIC X.
           02     MLIBTVAV              PIC X.
           02     MLIBTVAO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCRITERA              PIC X.
           02     MCRITERC              PIC X.
           02     MCRITERP              PIC X.
           02     MCRITERH              PIC X.
           02     MCRITERV              PIC X.
           02     MCRITERO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MQTEA                 PIC X.
           02     MQTEC                 PIC X.
           02     MQTEP                 PIC X.
           02     MQTEH                 PIC X.
           02     MQTEV                 PIC X.
           02     MQTEO                 PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPUHTA                PIC X.
           02     MPUHTC                PIC X.
           02     MPUHTP                PIC X.
           02     MPUHTH                PIC X.
           02     MPUHTV                PIC X.
           02     MPUHTO                PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTTVAA                PIC X.
           02     MTTVAC                PIC X.
           02     MTTVAP                PIC X.
           02     MTTVAH                PIC X.
           02     MTTVAV                PIC X.
           02     MTTVAO                PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MMONTHTA              PIC X.
           02     MMONTHTC              PIC X.
           02     MMONTHTP              PIC X.
           02     MMONTHTH              PIC X.
           02     MMONTHTV              PIC X.
           02     MMONTHTO              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDESIGNA              PIC X.
           02     MDESIGNC              PIC X.
           02     MDESIGNP              PIC X.
           02     MDESIGNH              PIC X.
           02     MDESIGNV              PIC X.
           02     MDESIGNO              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0002A              PIC X.
           02     DFH0002C              PIC X.
           02     DFH0002P              PIC X.
           02     DFH0002H              PIC X.
           02     DFH0002V              PIC X.
           02     DFH0002O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0003A              PIC X.
           02     DFH0003C              PIC X.
           02     DFH0003P              PIC X.
           02     DFH0003H              PIC X.
           02     DFH0003V              PIC X.
           02     DFH0003O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0004A              PIC X.
           02     DFH0004C              PIC X.
           02     DFH0004P              PIC X.
           02     DFH0004H              PIC X.
           02     DFH0004V              PIC X.
           02     DFH0004O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0005A              PIC X.
           02     DFH0005C              PIC X.
           02     DFH0005P              PIC X.
           02     DFH0005H              PIC X.
           02     DFH0005V              PIC X.
           02     DFH0005O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0006A              PIC X.
           02     DFH0006C              PIC X.
           02     DFH0006P              PIC X.
           02     DFH0006H              PIC X.
           02     DFH0006V              PIC X.
           02     DFH0006O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0007A              PIC X.
           02     DFH0007C              PIC X.
           02     DFH0007P              PIC X.
           02     DFH0007H              PIC X.
           02     DFH0007V              PIC X.
           02     DFH0007O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0008A              PIC X.
           02     DFH0008C              PIC X.
           02     DFH0008P              PIC X.
           02     DFH0008H              PIC X.
           02     DFH0008V              PIC X.
           02     DFH0008O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0009A              PIC X.
           02     DFH0009C              PIC X.
           02     DFH0009P              PIC X.
           02     DFH0009H              PIC X.
           02     DFH0009V              PIC X.
           02     DFH0009O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0010A              PIC X.
           02     DFH0010C              PIC X.
           02     DFH0010P              PIC X.
           02     DFH0010H              PIC X.
           02     DFH0010V              PIC X.
           02     DFH0010O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0011A              PIC X.
           02     DFH0011C              PIC X.
           02     DFH0011P              PIC X.
           02     DFH0011H              PIC X.
           02     DFH0011V              PIC X.
           02     DFH0011O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0012A              PIC X.
           02     DFH0012C              PIC X.
           02     DFH0012P              PIC X.
           02     DFH0012H              PIC X.
           02     DFH0012V              PIC X.
           02     DFH0012O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0013A              PIC X.
           02     DFH0013C              PIC X.
           02     DFH0013P              PIC X.
           02     DFH0013H              PIC X.
           02     DFH0013V              PIC X.
           02     DFH0013O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0014A              PIC X.
           02     DFH0014C              PIC X.
           02     DFH0014P              PIC X.
           02     DFH0014H              PIC X.
           02     DFH0014V              PIC X.
           02     DFH0014O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0015A              PIC X.
           02     DFH0015C              PIC X.
           02     DFH0015P              PIC X.
           02     DFH0015H              PIC X.
           02     DFH0015V              PIC X.
           02     DFH0015O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0016A              PIC X.
           02     DFH0016C              PIC X.
           02     DFH0016P              PIC X.
           02     DFH0016H              PIC X.
           02     DFH0016V              PIC X.
           02     DFH0016O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0017A              PIC X.
           02     DFH0017C              PIC X.
           02     DFH0017P              PIC X.
           02     DFH0017H              PIC X.
           02     DFH0017V              PIC X.
           02     DFH0017O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0018A              PIC X.
           02     DFH0018C              PIC X.
           02     DFH0018P              PIC X.
           02     DFH0018H              PIC X.
           02     DFH0018V              PIC X.
           02     DFH0018O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0019A              PIC X.
           02     DFH0019C              PIC X.
           02     DFH0019P              PIC X.
           02     DFH0019H              PIC X.
           02     DFH0019V              PIC X.
           02     DFH0019O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0020A              PIC X.
           02     DFH0020C              PIC X.
           02     DFH0020P              PIC X.
           02     DFH0020H              PIC X.
           02     DFH0020V              PIC X.
           02     DFH0020O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0021A              PIC X.
           02     DFH0021C              PIC X.
           02     DFH0021P              PIC X.
           02     DFH0021H              PIC X.
           02     DFH0021V              PIC X.
           02     DFH0021O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0022A              PIC X.
           02     DFH0022C              PIC X.
           02     DFH0022P              PIC X.
           02     DFH0022H              PIC X.
           02     DFH0022V              PIC X.
           02     DFH0022O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0023A              PIC X.
           02     DFH0023C              PIC X.
           02     DFH0023P              PIC X.
           02     DFH0023H              PIC X.
           02     DFH0023V              PIC X.
           02     DFH0023O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0024A              PIC X.
           02     DFH0024C              PIC X.
           02     DFH0024P              PIC X.
           02     DFH0024H              PIC X.
           02     DFH0024V              PIC X.
           02     DFH0024O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0025A              PIC X.
           02     DFH0025C              PIC X.
           02     DFH0025P              PIC X.
           02     DFH0025H              PIC X.
           02     DFH0025V              PIC X.
           02     DFH0025O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0026A              PIC X.
           02     DFH0026C              PIC X.
           02     DFH0026P              PIC X.
           02     DFH0026H              PIC X.
           02     DFH0026V              PIC X.
           02     DFH0026O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0027A              PIC X.
           02     DFH0027C              PIC X.
           02     DFH0027P              PIC X.
           02     DFH0027H              PIC X.
           02     DFH0027V              PIC X.
           02     DFH0027O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0028A              PIC X.
           02     DFH0028C              PIC X.
           02     DFH0028P              PIC X.
           02     DFH0028H              PIC X.
           02     DFH0028V              PIC X.
           02     DFH0028O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0029A              PIC X.
           02     DFH0029C              PIC X.
           02     DFH0029P              PIC X.
           02     DFH0029H              PIC X.
           02     DFH0029V              PIC X.
           02     DFH0029O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0030A              PIC X.
           02     DFH0030C              PIC X.
           02     DFH0030P              PIC X.
           02     DFH0030H              PIC X.
           02     DFH0030V              PIC X.
           02     DFH0030O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0031A              PIC X.
           02     DFH0031C              PIC X.
           02     DFH0031P              PIC X.
           02     DFH0031H              PIC X.
           02     DFH0031V              PIC X.
           02     DFH0031O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0032A              PIC X.
           02     DFH0032C              PIC X.
           02     DFH0032P              PIC X.
           02     DFH0032H              PIC X.
           02     DFH0032V              PIC X.
           02     DFH0032O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0033A              PIC X.
           02     DFH0033C              PIC X.
           02     DFH0033P              PIC X.
           02     DFH0033H              PIC X.
           02     DFH0033V              PIC X.
           02     DFH0033O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0034A              PIC X.
           02     DFH0034C              PIC X.
           02     DFH0034P              PIC X.
           02     DFH0034H              PIC X.
           02     DFH0034V              PIC X.
           02     DFH0034O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0035A              PIC X.
           02     DFH0035C              PIC X.
           02     DFH0035P              PIC X.
           02     DFH0035H              PIC X.
           02     DFH0035V              PIC X.
           02     DFH0035O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0036A              PIC X.
           02     DFH0036C              PIC X.
           02     DFH0036P              PIC X.
           02     DFH0036H              PIC X.
           02     DFH0036V              PIC X.
           02     DFH0036O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0037A              PIC X.
           02     DFH0037C              PIC X.
           02     DFH0037P              PIC X.
           02     DFH0037H              PIC X.
           02     DFH0037V              PIC X.
           02     DFH0037O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0038A              PIC X.
           02     DFH0038C              PIC X.
           02     DFH0038P              PIC X.
           02     DFH0038H              PIC X.
           02     DFH0038V              PIC X.
           02     DFH0038O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0039A              PIC X.
           02     DFH0039C              PIC X.
           02     DFH0039P              PIC X.
           02     DFH0039H              PIC X.
           02     DFH0039V              PIC X.
           02     DFH0039O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0040A              PIC X.
           02     DFH0040C              PIC X.
           02     DFH0040P              PIC X.
           02     DFH0040H              PIC X.
           02     DFH0040V              PIC X.
           02     DFH0040O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0041A              PIC X.
           02     DFH0041C              PIC X.
           02     DFH0041P              PIC X.
           02     DFH0041H              PIC X.
           02     DFH0041V              PIC X.
           02     DFH0041O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0042A              PIC X.
           02     DFH0042C              PIC X.
           02     DFH0042P              PIC X.
           02     DFH0042H              PIC X.
           02     DFH0042V              PIC X.
           02     DFH0042O              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0043A              PIC X.
           02     DFH0043C              PIC X.
           02     DFH0043P              PIC X.
           02     DFH0043H              PIC X.
           02     DFH0043V              PIC X.
           02     DFH0043O              PIC X(37).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBTESA              PIC X.
           02     MLIBTESC              PIC X.
           02     MLIBTESP              PIC X.
           02     MLIBTESH              PIC X.
           02     MLIBTESV              PIC X.
           02     MLIBTESO              PIC X(13).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTESCA                PIC X.
           02     MTESCC                PIC X.
           02     MTESCP                PIC X.
           02     MTESCH                PIC X.
           02     MTESCV                PIC X.
           02     MTESCO                PIC Z9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLMTESCA              PIC X.
           02     MLMTESCC              PIC X.
           02     MLMTESCP              PIC X.
           02     MLMTESCH              PIC X.
           02     MLMTESCV              PIC X.
           02     MLMTESCO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTOTESCA              PIC X.
           02     MTOTESCC              PIC X.
           02     MTOTESCP              PIC X.
           02     MTOTESCH              PIC X.
           02     MTOTESCV              PIC X.
           02     MTOTESCO              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTOTHTA               PIC X.
           02     MTOTHTC               PIC X.
           02     MTOTHTP               PIC X.
           02     MTOTHTH               PIC X.
           02     MTOTHTV               PIC X.
           02     MTOTHTO               PIC Z(10)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLTVAA                PIC X.
           02     MLTVAC                PIC X.
           02     MLTVAP                PIC X.
           02     MLTVAH                PIC X.
           02     MLTVAV                PIC X.
           02     MLTVAO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTOTTVAA              PIC X.
           02     MTOTTVAC              PIC X.
           02     MTOTTVAP              PIC X.
           02     MTOTTVAH              PIC X.
           02     MTOTTVAV              PIC X.
           02     MTOTTVAO              PIC ZZZZZZZZZ9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLTTCA                PIC X.
           02     MLTTCC                PIC X.
           02     MLTTCP                PIC X.
           02     MLTTCH                PIC X.
           02     MLTTCV                PIC X.
           02     MLTTCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTOTTTCA              PIC X.
           02     MTOTTTCC              PIC X.
           02     MTOTTTCP              PIC X.
           02     MTOTTTCH              PIC X.
           02     MTOTTTCV              PIC X.
           02     MTOTTTCO              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDP              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDV              PIC X.
           02     MZONCMDO              PIC X(15).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(58).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(4).
