      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG51   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG51I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MNSOCCEL              PIC S9(4) COMP-5.
           02     MNSOCCEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCCEI              PIC X(3).
           02     MLSOCCEL              PIC S9(4) COMP-5.
           02     MLSOCCEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCEI              PIC X(25).
           02     MNSOCEL               PIC S9(4) COMP-5.
           02     MNSOCEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCEI               PIC X(3).
           02     MNLIEUEL              PIC S9(4) COMP-5.
           02     MNLIEUEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUEI              PIC X(3).
           02     MLLIEUEL              PIC S9(4) COMP-5.
           02     MLLIEUEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUEI              PIC X(25).
           02     MNSOCCRL              PIC S9(4) COMP-5.
           02     MNSOCCRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCCRI              PIC X(3).
           02     MLSOCCRL              PIC S9(4) COMP-5.
           02     MLSOCCRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCRI              PIC X(25).
           02     MNSOCRL               PIC S9(4) COMP-5.
           02     MNSOCRF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCRI               PIC X(3).
           02     MNLIEURL              PIC S9(4) COMP-5.
           02     MNLIEURF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEURI              PIC X(3).
           02     MLLIEURL              PIC S9(4) COMP-5.
           02     MLLIEURF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEURI              PIC X(25).
           02     MCTYPEL               PIC S9(4) COMP-5.
           02     MCTYPEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MCTYPEI               PIC X(2).
           02     MLTYPEL               PIC S9(4) COMP-5.
           02     MLTYPEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLTYPEI               PIC X(30).
           02     MNATUREL              PIC S9(4) COMP-5.
           02     MNATUREF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNATUREI              PIC X(5).
           02     MLNATURL              PIC S9(4) COMP-5.
           02     MLNATURF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLNATURI              PIC X(30).
           02     MNUMFACL              PIC S9(4) COMP-5.
           02     MNUMFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMFACI              PIC X(7).
           02     MDPIECEL              PIC S9(4) COMP-5.
           02     MDPIECEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDPIECEI              PIC X(10).
           02     MDANNULL              PIC S9(4) COMP-5.
           02     MDANNULF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDANNULI              PIC X(10).
           02     MDMVTL                PIC S9(4) COMP-5.
           02     MDMVTF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MDMVTI                PIC X(10).
           02     MMESSAGL              PIC S9(4) COMP-5.
           02     MMESSAGF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MMESSAGI              PIC X(31).
           02     MDCOMPTL              PIC S9(4) COMP-5.
           02     MDCOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDCOMPTI              PIC X(10).
           02     MDECHL                PIC S9(4) COMP-5.
           02     MDECHF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MDECHI                PIC X(10).
           02     MDREGLL               PIC S9(4) COMP-5.
           02     MDREGLF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MDREGLI               PIC X(10).
           02     MDVALEUL              PIC S9(4) COMP-5.
           02     MDVALEUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDVALEUI              PIC X(10).
           02     MCONFIRL              PIC S9(4) COMP-5.
           02     MCONFIRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCONFIRI              PIC X(3).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(78).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(5).
       01  EFG51O REDEFINES EFG51I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCCEA              PIC X.
           02     MNSOCCEC              PIC X.
           02     MNSOCCEP              PIC X.
           02     MNSOCCEH              PIC X.
           02     MNSOCCEV              PIC X.
           02     MNSOCCEO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCEA              PIC X.
           02     MLSOCCEC              PIC X.
           02     MLSOCCEP              PIC X.
           02     MLSOCCEH              PIC X.
           02     MLSOCCEV              PIC X.
           02     MLSOCCEO              PIC X(25).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCEA               PIC X.
           02     MNSOCEC               PIC X.
           02     MNSOCEP               PIC X.
           02     MNSOCEH               PIC X.
           02     MNSOCEV               PIC X.
           02     MNSOCEO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUEA              PIC X.
           02     MNLIEUEC              PIC X.
           02     MNLIEUEP              PIC X.
           02     MNLIEUEH              PIC X.
           02     MNLIEUEV              PIC X.
           02     MNLIEUEO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUEA              PIC X.
           02     MLLIEUEC              PIC X.
           02     MLLIEUEP              PIC X.
           02     MLLIEUEH              PIC X.
           02     MLLIEUEV              PIC X.
           02     MLLIEUEO              PIC X(25).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCCRA              PIC X.
           02     MNSOCCRC              PIC X.
           02     MNSOCCRP              PIC X.
           02     MNSOCCRH              PIC X.
           02     MNSOCCRV              PIC X.
           02     MNSOCCRO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCRA              PIC X.
           02     MLSOCCRC              PIC X.
           02     MLSOCCRP              PIC X.
           02     MLSOCCRH              PIC X.
           02     MLSOCCRV              PIC X.
           02     MLSOCCRO              PIC X(25).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCRA               PIC X.
           02     MNSOCRC               PIC X.
           02     MNSOCRP               PIC X.
           02     MNSOCRH               PIC X.
           02     MNSOCRV               PIC X.
           02     MNSOCRO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEURA              PIC X.
           02     MNLIEURC              PIC X.
           02     MNLIEURP              PIC X.
           02     MNLIEURH              PIC X.
           02     MNLIEURV              PIC X.
           02     MNLIEURO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEURA              PIC X.
           02     MLLIEURC              PIC X.
           02     MLLIEURP              PIC X.
           02     MLLIEURH              PIC X.
           02     MLLIEURV              PIC X.
           02     MLLIEURO              PIC X(25).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCTYPEA               PIC X.
           02     MCTYPEC               PIC X.
           02     MCTYPEP               PIC X.
           02     MCTYPEH               PIC X.
           02     MCTYPEV               PIC X.
           02     MCTYPEO               PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLTYPEA               PIC X.
           02     MLTYPEC               PIC X.
           02     MLTYPEP               PIC X.
           02     MLTYPEH               PIC X.
           02     MLTYPEV               PIC X.
           02     MLTYPEO               PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNATUREA              PIC X.
           02     MNATUREC              PIC X.
           02     MNATUREP              PIC X.
           02     MNATUREH              PIC X.
           02     MNATUREV              PIC X.
           02     MNATUREO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLNATURA              PIC X.
           02     MLNATURC              PIC X.
           02     MLNATURP              PIC X.
           02     MLNATURH              PIC X.
           02     MLNATURV              PIC X.
           02     MLNATURO              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMFACA              PIC X.
           02     MNUMFACC              PIC X.
           02     MNUMFACP              PIC X.
           02     MNUMFACH              PIC X.
           02     MNUMFACV              PIC X.
           02     MNUMFACO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDPIECEA              PIC X.
           02     MDPIECEC              PIC X.
           02     MDPIECEP              PIC X.
           02     MDPIECEH              PIC X.
           02     MDPIECEV              PIC X.
           02     MDPIECEO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDANNULA              PIC X.
           02     MDANNULC              PIC X.
           02     MDANNULP              PIC X.
           02     MDANNULH              PIC X.
           02     MDANNULV              PIC X.
           02     MDANNULO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDMVTA                PIC X.
           02     MDMVTC                PIC X.
           02     MDMVTP                PIC X.
           02     MDMVTH                PIC X.
           02     MDMVTV                PIC X.
           02     MDMVTO                PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MMESSAGA              PIC X.
           02     MMESSAGC              PIC X.
           02     MMESSAGP              PIC X.
           02     MMESSAGH              PIC X.
           02     MMESSAGV              PIC X.
           02     MMESSAGO              PIC X(31).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDCOMPTA              PIC X.
           02     MDCOMPTC              PIC X.
           02     MDCOMPTP              PIC X.
           02     MDCOMPTH              PIC X.
           02     MDCOMPTV              PIC X.
           02     MDCOMPTO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDECHA                PIC X.
           02     MDECHC                PIC X.
           02     MDECHP                PIC X.
           02     MDECHH                PIC X.
           02     MDECHV                PIC X.
           02     MDECHO                PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDREGLA               PIC X.
           02     MDREGLC               PIC X.
           02     MDREGLP               PIC X.
           02     MDREGLH               PIC X.
           02     MDREGLV               PIC X.
           02     MDREGLO               PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDVALEUA              PIC X.
           02     MDVALEUC              PIC X.
           02     MDVALEUP              PIC X.
           02     MDVALEUH              PIC X.
           02     MDVALEUV              PIC X.
           02     MDVALEUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCONFIRA              PIC X.
           02     MCONFIRC              PIC X.
           02     MCONFIRP              PIC X.
           02     MCONFIRH              PIC X.
           02     MCONFIRV              PIC X.
           02     MCONFIRO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(78).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(5).
