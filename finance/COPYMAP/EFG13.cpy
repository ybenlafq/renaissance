      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG13   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG13I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MTITREL               PIC S9(4) COMP-5.
           02     MTITREF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MTITREI               PIC X(6).
           02     MSOCCL                PIC S9(4) COMP-5.
           02     MSOCCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCCI                PIC X(3).
           02     MSERVL                PIC S9(4) COMP-5.
           02     MSERVF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSERVI                PIC X(5).
           02     MLSOCCL               PIC S9(4) COMP-5.
           02     MLSOCCF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCI               PIC X(22).
           02     MPAGEL                PIC S9(4) COMP-5.
           02     MPAGEF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEI                PIC X(3).
           02     MPAGEMAL              PIC S9(4) COMP-5.
           02     MPAGEMAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEMAI              PIC X(3).
           02     MNUMFACL              PIC S9(4) COMP-5.
           02     MNUMFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMFACI              PIC X(7).
           02     MCTYPEL               PIC S9(4) COMP-5.
           02     MCTYPEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MCTYPEI               PIC X(2).
           02     MNATUREL              PIC S9(4) COMP-5.
           02     MNATUREF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNATUREI              PIC X(5).
           02     MSORIGL               PIC S9(4) COMP-5.
           02     MSORIGF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MSORIGI               PIC X(3).
           02     MLORIGL               PIC S9(4) COMP-5.
           02     MLORIGF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLORIGI               PIC X(3).
           02     MSECORIL              PIC S9(4) COMP-5.
           02     MSECORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECORII              PIC X(6).
           02     MSDESTL               PIC S9(4) COMP-5.
           02     MSDESTF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MSDESTI               PIC X(3).
           02     MLDESTL               PIC S9(4) COMP-5.
           02     MLDESTF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLDESTI               PIC X(3).
           02     MSECDESL              PIC S9(4) COMP-5.
           02     MSECDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECDESI              PIC X(6).
           02     MMONTTTL              PIC S9(4) COMP-5.
           02     MMONTTTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MMONTTTI              PIC X(15).
           02     MDPIECEL              PIC S9(4) COMP-5.
           02     MDPIECEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDPIECEI              PIC X(8).
           02     MDECHEAL              PIC S9(4) COMP-5.
           02     MDECHEAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDECHEAI              PIC X(5).
           02     MCONSULL              PIC S9(4) COMP-5.
           02     MCONSULF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCONSULI              PIC X(1).
           02     DFH0001L              PIC S9(4) COMP-5.
           02     DFH0001F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0001I              PIC X(7).
           02     DFH0002L              PIC S9(4) COMP-5.
           02     DFH0002F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0002I              PIC X(2).
           02     DFH0003L              PIC S9(4) COMP-5.
           02     DFH0003F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0003I              PIC X(5).
           02     DFH0004L              PIC S9(4) COMP-5.
           02     DFH0004F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0004I              PIC X(3).
           02     DFH0005L              PIC S9(4) COMP-5.
           02     DFH0005F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0005I              PIC X(3).
           02     DFH0006L              PIC S9(4) COMP-5.
           02     DFH0006F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0006I              PIC X(6).
           02     DFH0007L              PIC S9(4) COMP-5.
           02     DFH0007F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0007I              PIC X(3).
           02     DFH0008L              PIC S9(4) COMP-5.
           02     DFH0008F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0008I              PIC X(3).
           02     DFH0009L              PIC S9(4) COMP-5.
           02     DFH0009F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0009I              PIC X(6).
           02     DFH0010L              PIC S9(4) COMP-5.
           02     DFH0010F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0010I              PIC X(15).
           02     DFH0011L              PIC S9(4) COMP-5.
           02     DFH0011F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0011I              PIC X(8).
           02     DFH0012L              PIC S9(4) COMP-5.
           02     DFH0012F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0012I              PIC X(5).
           02     DFH0013L              PIC S9(4) COMP-5.
           02     DFH0013F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0013I              PIC X(1).
           02     DFH0014L              PIC S9(4) COMP-5.
           02     DFH0014F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0014I              PIC X(7).
           02     DFH0015L              PIC S9(4) COMP-5.
           02     DFH0015F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0015I              PIC X(2).
           02     DFH0016L              PIC S9(4) COMP-5.
           02     DFH0016F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0016I              PIC X(5).
           02     DFH0017L              PIC S9(4) COMP-5.
           02     DFH0017F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0017I              PIC X(3).
           02     DFH0018L              PIC S9(4) COMP-5.
           02     DFH0018F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0018I              PIC X(3).
           02     DFH0019L              PIC S9(4) COMP-5.
           02     DFH0019F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0019I              PIC X(6).
           02     DFH0020L              PIC S9(4) COMP-5.
           02     DFH0020F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0020I              PIC X(3).
           02     DFH0021L              PIC S9(4) COMP-5.
           02     DFH0021F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0021I              PIC X(3).
           02     DFH0022L              PIC S9(4) COMP-5.
           02     DFH0022F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0022I              PIC X(6).
           02     DFH0023L              PIC S9(4) COMP-5.
           02     DFH0023F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0023I              PIC X(15).
           02     DFH0024L              PIC S9(4) COMP-5.
           02     DFH0024F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0024I              PIC X(8).
           02     DFH0025L              PIC S9(4) COMP-5.
           02     DFH0025F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0025I              PIC X(5).
           02     DFH0026L              PIC S9(4) COMP-5.
           02     DFH0026F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0026I              PIC X(1).
           02     DFH0027L              PIC S9(4) COMP-5.
           02     DFH0027F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0027I              PIC X(7).
           02     DFH0028L              PIC S9(4) COMP-5.
           02     DFH0028F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0028I              PIC X(2).
           02     DFH0029L              PIC S9(4) COMP-5.
           02     DFH0029F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0029I              PIC X(5).
           02     DFH0030L              PIC S9(4) COMP-5.
           02     DFH0030F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0030I              PIC X(3).
           02     DFH0031L              PIC S9(4) COMP-5.
           02     DFH0031F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0031I              PIC X(3).
           02     DFH0032L              PIC S9(4) COMP-5.
           02     DFH0032F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0032I              PIC X(6).
           02     DFH0033L              PIC S9(4) COMP-5.
           02     DFH0033F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0033I              PIC X(3).
           02     DFH0034L              PIC S9(4) COMP-5.
           02     DFH0034F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0034I              PIC X(3).
           02     DFH0035L              PIC S9(4) COMP-5.
           02     DFH0035F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0035I              PIC X(6).
           02     DFH0036L              PIC S9(4) COMP-5.
           02     DFH0036F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0036I              PIC X(15).
           02     DFH0037L              PIC S9(4) COMP-5.
           02     DFH0037F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0037I              PIC X(8).
           02     DFH0038L              PIC S9(4) COMP-5.
           02     DFH0038F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0038I              PIC X(5).
           02     DFH0039L              PIC S9(4) COMP-5.
           02     DFH0039F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0039I              PIC X(1).
           02     DFH0040L              PIC S9(4) COMP-5.
           02     DFH0040F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0040I              PIC X(7).
           02     DFH0041L              PIC S9(4) COMP-5.
           02     DFH0041F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0041I              PIC X(2).
           02     DFH0042L              PIC S9(4) COMP-5.
           02     DFH0042F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0042I              PIC X(5).
           02     DFH0043L              PIC S9(4) COMP-5.
           02     DFH0043F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0043I              PIC X(3).
           02     DFH0044L              PIC S9(4) COMP-5.
           02     DFH0044F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0044I              PIC X(3).
           02     DFH0045L              PIC S9(4) COMP-5.
           02     DFH0045F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0045I              PIC X(6).
           02     DFH0046L              PIC S9(4) COMP-5.
           02     DFH0046F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0046I              PIC X(3).
           02     DFH0047L              PIC S9(4) COMP-5.
           02     DFH0047F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0047I              PIC X(3).
           02     DFH0048L              PIC S9(4) COMP-5.
           02     DFH0048F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0048I              PIC X(6).
           02     DFH0049L              PIC S9(4) COMP-5.
           02     DFH0049F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0049I              PIC X(15).
           02     DFH0050L              PIC S9(4) COMP-5.
           02     DFH0050F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0050I              PIC X(8).
           02     DFH0051L              PIC S9(4) COMP-5.
           02     DFH0051F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0051I              PIC X(5).
           02     DFH0052L              PIC S9(4) COMP-5.
           02     DFH0052F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0052I              PIC X(1).
           02     DFH0053L              PIC S9(4) COMP-5.
           02     DFH0053F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0053I              PIC X(7).
           02     DFH0054L              PIC S9(4) COMP-5.
           02     DFH0054F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0054I              PIC X(2).
           02     DFH0055L              PIC S9(4) COMP-5.
           02     DFH0055F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0055I              PIC X(5).
           02     DFH0056L              PIC S9(4) COMP-5.
           02     DFH0056F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0056I              PIC X(3).
           02     DFH0057L              PIC S9(4) COMP-5.
           02     DFH0057F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0057I              PIC X(3).
           02     DFH0058L              PIC S9(4) COMP-5.
           02     DFH0058F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0058I              PIC X(6).
           02     DFH0059L              PIC S9(4) COMP-5.
           02     DFH0059F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0059I              PIC X(3).
           02     DFH0060L              PIC S9(4) COMP-5.
           02     DFH0060F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0060I              PIC X(3).
           02     DFH0061L              PIC S9(4) COMP-5.
           02     DFH0061F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0061I              PIC X(6).
           02     DFH0062L              PIC S9(4) COMP-5.
           02     DFH0062F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0062I              PIC X(15).
           02     DFH0063L              PIC S9(4) COMP-5.
           02     DFH0063F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0063I              PIC X(8).
           02     DFH0064L              PIC S9(4) COMP-5.
           02     DFH0064F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0064I              PIC X(5).
           02     DFH0065L              PIC S9(4) COMP-5.
           02     DFH0065F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0065I              PIC X(1).
           02     DFH0066L              PIC S9(4) COMP-5.
           02     DFH0066F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0066I              PIC X(7).
           02     DFH0067L              PIC S9(4) COMP-5.
           02     DFH0067F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0067I              PIC X(2).
           02     DFH0068L              PIC S9(4) COMP-5.
           02     DFH0068F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0068I              PIC X(5).
           02     DFH0069L              PIC S9(4) COMP-5.
           02     DFH0069F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0069I              PIC X(3).
           02     DFH0070L              PIC S9(4) COMP-5.
           02     DFH0070F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0070I              PIC X(3).
           02     DFH0071L              PIC S9(4) COMP-5.
           02     DFH0071F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0071I              PIC X(6).
           02     DFH0072L              PIC S9(4) COMP-5.
           02     DFH0072F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0072I              PIC X(3).
           02     DFH0073L              PIC S9(4) COMP-5.
           02     DFH0073F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0073I              PIC X(3).
           02     DFH0074L              PIC S9(4) COMP-5.
           02     DFH0074F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0074I              PIC X(6).
           02     DFH0075L              PIC S9(4) COMP-5.
           02     DFH0075F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0075I              PIC X(15).
           02     DFH0076L              PIC S9(4) COMP-5.
           02     DFH0076F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0076I              PIC X(8).
           02     DFH0077L              PIC S9(4) COMP-5.
           02     DFH0077F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0077I              PIC X(5).
           02     DFH0078L              PIC S9(4) COMP-5.
           02     DFH0078F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0078I              PIC X(1).
           02     DFH0079L              PIC S9(4) COMP-5.
           02     DFH0079F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0079I              PIC X(7).
           02     DFH0080L              PIC S9(4) COMP-5.
           02     DFH0080F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0080I              PIC X(2).
           02     DFH0081L              PIC S9(4) COMP-5.
           02     DFH0081F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0081I              PIC X(5).
           02     DFH0082L              PIC S9(4) COMP-5.
           02     DFH0082F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0082I              PIC X(3).
           02     DFH0083L              PIC S9(4) COMP-5.
           02     DFH0083F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0083I              PIC X(3).
           02     DFH0084L              PIC S9(4) COMP-5.
           02     DFH0084F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0084I              PIC X(6).
           02     DFH0085L              PIC S9(4) COMP-5.
           02     DFH0085F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0085I              PIC X(3).
           02     DFH0086L              PIC S9(4) COMP-5.
           02     DFH0086F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0086I              PIC X(3).
           02     DFH0087L              PIC S9(4) COMP-5.
           02     DFH0087F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0087I              PIC X(6).
           02     DFH0088L              PIC S9(4) COMP-5.
           02     DFH0088F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0088I              PIC X(15).
           02     DFH0089L              PIC S9(4) COMP-5.
           02     DFH0089F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0089I              PIC X(8).
           02     DFH0090L              PIC S9(4) COMP-5.
           02     DFH0090F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0090I              PIC X(5).
           02     DFH0091L              PIC S9(4) COMP-5.
           02     DFH0091F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0091I              PIC X(1).
           02     DFH0092L              PIC S9(4) COMP-5.
           02     DFH0092F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0092I              PIC X(7).
           02     DFH0093L              PIC S9(4) COMP-5.
           02     DFH0093F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0093I              PIC X(2).
           02     DFH0094L              PIC S9(4) COMP-5.
           02     DFH0094F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0094I              PIC X(5).
           02     DFH0095L              PIC S9(4) COMP-5.
           02     DFH0095F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0095I              PIC X(3).
           02     DFH0096L              PIC S9(4) COMP-5.
           02     DFH0096F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0096I              PIC X(3).
           02     DFH0097L              PIC S9(4) COMP-5.
           02     DFH0097F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0097I              PIC X(6).
           02     DFH0098L              PIC S9(4) COMP-5.
           02     DFH0098F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0098I              PIC X(3).
           02     DFH0099L              PIC S9(4) COMP-5.
           02     DFH0099F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0099I              PIC X(3).
           02     DFH0100L              PIC S9(4) COMP-5.
           02     DFH0100F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0100I              PIC X(6).
           02     DFH0101L              PIC S9(4) COMP-5.
           02     DFH0101F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0101I              PIC X(15).
           02     DFH0102L              PIC S9(4) COMP-5.
           02     DFH0102F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0102I              PIC X(8).
           02     DFH0103L              PIC S9(4) COMP-5.
           02     DFH0103F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0103I              PIC X(5).
           02     DFH0104L              PIC S9(4) COMP-5.
           02     DFH0104F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0104I              PIC X(1).
           02     DFH0105L              PIC S9(4) COMP-5.
           02     DFH0105F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0105I              PIC X(7).
           02     DFH0106L              PIC S9(4) COMP-5.
           02     DFH0106F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0106I              PIC X(2).
           02     DFH0107L              PIC S9(4) COMP-5.
           02     DFH0107F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0107I              PIC X(5).
           02     DFH0108L              PIC S9(4) COMP-5.
           02     DFH0108F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0108I              PIC X(3).
           02     DFH0109L              PIC S9(4) COMP-5.
           02     DFH0109F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0109I              PIC X(3).
           02     DFH0110L              PIC S9(4) COMP-5.
           02     DFH0110F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0110I              PIC X(6).
           02     DFH0111L              PIC S9(4) COMP-5.
           02     DFH0111F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0111I              PIC X(3).
           02     DFH0112L              PIC S9(4) COMP-5.
           02     DFH0112F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0112I              PIC X(3).
           02     DFH0113L              PIC S9(4) COMP-5.
           02     DFH0113F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0113I              PIC X(6).
           02     DFH0114L              PIC S9(4) COMP-5.
           02     DFH0114F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0114I              PIC X(15).
           02     DFH0115L              PIC S9(4) COMP-5.
           02     DFH0115F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0115I              PIC X(8).
           02     DFH0116L              PIC S9(4) COMP-5.
           02     DFH0116F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0116I              PIC X(5).
           02     DFH0117L              PIC S9(4) COMP-5.
           02     DFH0117F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0117I              PIC X(1).
           02     DFH0118L              PIC S9(4) COMP-5.
           02     DFH0118F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0118I              PIC X(7).
           02     DFH0119L              PIC S9(4) COMP-5.
           02     DFH0119F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0119I              PIC X(2).
           02     DFH0120L              PIC S9(4) COMP-5.
           02     DFH0120F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0120I              PIC X(5).
           02     DFH0121L              PIC S9(4) COMP-5.
           02     DFH0121F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0121I              PIC X(3).
           02     DFH0122L              PIC S9(4) COMP-5.
           02     DFH0122F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0122I              PIC X(3).
           02     DFH0123L              PIC S9(4) COMP-5.
           02     DFH0123F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0123I              PIC X(6).
           02     DFH0124L              PIC S9(4) COMP-5.
           02     DFH0124F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0124I              PIC X(3).
           02     DFH0125L              PIC S9(4) COMP-5.
           02     DFH0125F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0125I              PIC X(3).
           02     DFH0126L              PIC S9(4) COMP-5.
           02     DFH0126F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0126I              PIC X(6).
           02     DFH0127L              PIC S9(4) COMP-5.
           02     DFH0127F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0127I              PIC X(15).
           02     DFH0128L              PIC S9(4) COMP-5.
           02     DFH0128F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0128I              PIC X(8).
           02     DFH0129L              PIC S9(4) COMP-5.
           02     DFH0129F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0129I              PIC X(5).
           02     DFH0130L              PIC S9(4) COMP-5.
           02     DFH0130F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0130I              PIC X(1).
           02     DFH0131L              PIC S9(4) COMP-5.
           02     DFH0131F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0131I              PIC X(7).
           02     DFH0132L              PIC S9(4) COMP-5.
           02     DFH0132F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0132I              PIC X(2).
           02     DFH0133L              PIC S9(4) COMP-5.
           02     DFH0133F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0133I              PIC X(5).
           02     DFH0134L              PIC S9(4) COMP-5.
           02     DFH0134F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0134I              PIC X(3).
           02     DFH0135L              PIC S9(4) COMP-5.
           02     DFH0135F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0135I              PIC X(3).
           02     DFH0136L              PIC S9(4) COMP-5.
           02     DFH0136F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0136I              PIC X(6).
           02     DFH0137L              PIC S9(4) COMP-5.
           02     DFH0137F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0137I              PIC X(3).
           02     DFH0138L              PIC S9(4) COMP-5.
           02     DFH0138F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0138I              PIC X(3).
           02     DFH0139L              PIC S9(4) COMP-5.
           02     DFH0139F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0139I              PIC X(6).
           02     DFH0140L              PIC S9(4) COMP-5.
           02     DFH0140F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0140I              PIC X(15).
           02     DFH0141L              PIC S9(4) COMP-5.
           02     DFH0141F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0141I              PIC X(8).
           02     DFH0142L              PIC S9(4) COMP-5.
           02     DFH0142F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0142I              PIC X(5).
           02     DFH0143L              PIC S9(4) COMP-5.
           02     DFH0143F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0143I              PIC X(1).
           02     DFH0144L              PIC S9(4) COMP-5.
           02     DFH0144F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0144I              PIC X(7).
           02     DFH0145L              PIC S9(4) COMP-5.
           02     DFH0145F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0145I              PIC X(2).
           02     DFH0146L              PIC S9(4) COMP-5.
           02     DFH0146F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0146I              PIC X(5).
           02     DFH0147L              PIC S9(4) COMP-5.
           02     DFH0147F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0147I              PIC X(3).
           02     DFH0148L              PIC S9(4) COMP-5.
           02     DFH0148F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0148I              PIC X(3).
           02     DFH0149L              PIC S9(4) COMP-5.
           02     DFH0149F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0149I              PIC X(6).
           02     DFH0150L              PIC S9(4) COMP-5.
           02     DFH0150F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0150I              PIC X(3).
           02     DFH0151L              PIC S9(4) COMP-5.
           02     DFH0151F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0151I              PIC X(3).
           02     DFH0152L              PIC S9(4) COMP-5.
           02     DFH0152F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0152I              PIC X(6).
           02     DFH0153L              PIC S9(4) COMP-5.
           02     DFH0153F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0153I              PIC X(15).
           02     DFH0154L              PIC S9(4) COMP-5.
           02     DFH0154F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0154I              PIC X(8).
           02     DFH0155L              PIC S9(4) COMP-5.
           02     DFH0155F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0155I              PIC X(5).
           02     DFH0156L              PIC S9(4) COMP-5.
           02     DFH0156F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0156I              PIC X(1).
           02     DFH0157L              PIC S9(4) COMP-5.
           02     DFH0157F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0157I              PIC X(7).
           02     DFH0158L              PIC S9(4) COMP-5.
           02     DFH0158F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0158I              PIC X(2).
           02     DFH0159L              PIC S9(4) COMP-5.
           02     DFH0159F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0159I              PIC X(5).
           02     DFH0160L              PIC S9(4) COMP-5.
           02     DFH0160F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0160I              PIC X(3).
           02     DFH0161L              PIC S9(4) COMP-5.
           02     DFH0161F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0161I              PIC X(3).
           02     DFH0162L              PIC S9(4) COMP-5.
           02     DFH0162F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0162I              PIC X(6).
           02     DFH0163L              PIC S9(4) COMP-5.
           02     DFH0163F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0163I              PIC X(3).
           02     DFH0164L              PIC S9(4) COMP-5.
           02     DFH0164F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0164I              PIC X(3).
           02     DFH0165L              PIC S9(4) COMP-5.
           02     DFH0165F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0165I              PIC X(6).
           02     DFH0166L              PIC S9(4) COMP-5.
           02     DFH0166F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0166I              PIC X(15).
           02     DFH0167L              PIC S9(4) COMP-5.
           02     DFH0167F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0167I              PIC X(8).
           02     DFH0168L              PIC S9(4) COMP-5.
           02     DFH0168F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0168I              PIC X(5).
           02     DFH0169L              PIC S9(4) COMP-5.
           02     DFH0169F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0169I              PIC X(1).
           02     DFH0170L              PIC S9(4) COMP-5.
           02     DFH0170F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0170I              PIC X(7).
           02     DFH0171L              PIC S9(4) COMP-5.
           02     DFH0171F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0171I              PIC X(2).
           02     DFH0172L              PIC S9(4) COMP-5.
           02     DFH0172F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0172I              PIC X(5).
           02     DFH0173L              PIC S9(4) COMP-5.
           02     DFH0173F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0173I              PIC X(3).
           02     DFH0174L              PIC S9(4) COMP-5.
           02     DFH0174F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0174I              PIC X(3).
           02     DFH0175L              PIC S9(4) COMP-5.
           02     DFH0175F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0175I              PIC X(6).
           02     DFH0176L              PIC S9(4) COMP-5.
           02     DFH0176F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0176I              PIC X(3).
           02     DFH0177L              PIC S9(4) COMP-5.
           02     DFH0177F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0177I              PIC X(3).
           02     DFH0178L              PIC S9(4) COMP-5.
           02     DFH0178F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0178I              PIC X(6).
           02     DFH0179L              PIC S9(4) COMP-5.
           02     DFH0179F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0179I              PIC X(15).
           02     DFH0180L              PIC S9(4) COMP-5.
           02     DFH0180F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0180I              PIC X(8).
           02     DFH0181L              PIC S9(4) COMP-5.
           02     DFH0181F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0181I              PIC X(5).
           02     DFH0182L              PIC S9(4) COMP-5.
           02     DFH0182F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0182I              PIC X(1).
           02     DFH0183L              PIC S9(4) COMP-5.
           02     DFH0183F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0183I              PIC X(7).
           02     DFH0184L              PIC S9(4) COMP-5.
           02     DFH0184F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0184I              PIC X(2).
           02     DFH0185L              PIC S9(4) COMP-5.
           02     DFH0185F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0185I              PIC X(5).
           02     DFH0186L              PIC S9(4) COMP-5.
           02     DFH0186F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0186I              PIC X(3).
           02     DFH0187L              PIC S9(4) COMP-5.
           02     DFH0187F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0187I              PIC X(3).
           02     DFH0188L              PIC S9(4) COMP-5.
           02     DFH0188F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0188I              PIC X(6).
           02     DFH0189L              PIC S9(4) COMP-5.
           02     DFH0189F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0189I              PIC X(3).
           02     DFH0190L              PIC S9(4) COMP-5.
           02     DFH0190F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0190I              PIC X(3).
           02     DFH0191L              PIC S9(4) COMP-5.
           02     DFH0191F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0191I              PIC X(6).
           02     DFH0192L              PIC S9(4) COMP-5.
           02     DFH0192F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0192I              PIC X(15).
           02     DFH0193L              PIC S9(4) COMP-5.
           02     DFH0193F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0193I              PIC X(8).
           02     DFH0194L              PIC S9(4) COMP-5.
           02     DFH0194F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0194I              PIC X(5).
           02     DFH0195L              PIC S9(4) COMP-5.
           02     DFH0195F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0195I              PIC X(1).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONCMDI              PIC X(14).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(58).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(4).
       01  EFG13O REDEFINES EFG13I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTITREA               PIC X.
           02     MTITREC               PIC X.
           02     MTITREP               PIC X.
           02     MTITREH               PIC X.
           02     MTITREV               PIC X.
           02     MTITREO               PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCCA                PIC X.
           02     MSOCCC                PIC X.
           02     MSOCCP                PIC X.
           02     MSOCCH                PIC X.
           02     MSOCCV                PIC X.
           02     MSOCCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSERVA                PIC X.
           02     MSERVC                PIC X.
           02     MSERVP                PIC X.
           02     MSERVH                PIC X.
           02     MSERVV                PIC X.
           02     MSERVO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCA               PIC X.
           02     MLSOCCC               PIC X.
           02     MLSOCCP               PIC X.
           02     MLSOCCH               PIC X.
           02     MLSOCCV               PIC X.
           02     MLSOCCO               PIC X(22).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEA                PIC X.
           02     MPAGEC                PIC X.
           02     MPAGEP                PIC X.
           02     MPAGEH                PIC X.
           02     MPAGEV                PIC X.
           02     MPAGEO                PIC 9(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEMAA              PIC X.
           02     MPAGEMAC              PIC X.
           02     MPAGEMAP              PIC X.
           02     MPAGEMAH              PIC X.
           02     MPAGEMAV              PIC X.
           02     MPAGEMAO              PIC 9(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMFACA              PIC X.
           02     MNUMFACC              PIC X.
           02     MNUMFACP              PIC X.
           02     MNUMFACH              PIC X.
           02     MNUMFACV              PIC X.
           02     MNUMFACO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCTYPEA               PIC X.
           02     MCTYPEC               PIC X.
           02     MCTYPEP               PIC X.
           02     MCTYPEH               PIC X.
           02     MCTYPEV               PIC X.
           02     MCTYPEO               PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNATUREA              PIC X.
           02     MNATUREC              PIC X.
           02     MNATUREP              PIC X.
           02     MNATUREH              PIC X.
           02     MNATUREV              PIC X.
           02     MNATUREO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSORIGA               PIC X.
           02     MSORIGC               PIC X.
           02     MSORIGP               PIC X.
           02     MSORIGH               PIC X.
           02     MSORIGV               PIC X.
           02     MSORIGO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLORIGA               PIC X.
           02     MLORIGC               PIC X.
           02     MLORIGP               PIC X.
           02     MLORIGH               PIC X.
           02     MLORIGV               PIC X.
           02     MLORIGO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECORIA              PIC X.
           02     MSECORIC              PIC X.
           02     MSECORIP              PIC X.
           02     MSECORIH              PIC X.
           02     MSECORIV              PIC X.
           02     MSECORIO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSDESTA               PIC X.
           02     MSDESTC               PIC X.
           02     MSDESTP               PIC X.
           02     MSDESTH               PIC X.
           02     MSDESTV               PIC X.
           02     MSDESTO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLDESTA               PIC X.
           02     MLDESTC               PIC X.
           02     MLDESTP               PIC X.
           02     MLDESTH               PIC X.
           02     MLDESTV               PIC X.
           02     MLDESTO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECDESA              PIC X.
           02     MSECDESC              PIC X.
           02     MSECDESP              PIC X.
           02     MSECDESH              PIC X.
           02     MSECDESV              PIC X.
           02     MSECDESO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MMONTTTA              PIC X.
           02     MMONTTTC              PIC X.
           02     MMONTTTP              PIC X.
           02     MMONTTTH              PIC X.
           02     MMONTTTV              PIC X.
           02     MMONTTTO              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDPIECEA              PIC X.
           02     MDPIECEC              PIC X.
           02     MDPIECEP              PIC X.
           02     MDPIECEH              PIC X.
           02     MDPIECEV              PIC X.
           02     MDPIECEO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDECHEAA              PIC X.
           02     MDECHEAC              PIC X.
           02     MDECHEAP              PIC X.
           02     MDECHEAH              PIC X.
           02     MDECHEAV              PIC X.
           02     MDECHEAO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCONSULA              PIC X.
           02     MCONSULC              PIC X.
           02     MCONSULP              PIC X.
           02     MCONSULH              PIC X.
           02     MCONSULV              PIC X.
           02     MCONSULO              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0001A              PIC X.
           02     DFH0001C              PIC X.
           02     DFH0001P              PIC X.
           02     DFH0001H              PIC X.
           02     DFH0001V              PIC X.
           02     DFH0001O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0002A              PIC X.
           02     DFH0002C              PIC X.
           02     DFH0002P              PIC X.
           02     DFH0002H              PIC X.
           02     DFH0002V              PIC X.
           02     DFH0002O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0003A              PIC X.
           02     DFH0003C              PIC X.
           02     DFH0003P              PIC X.
           02     DFH0003H              PIC X.
           02     DFH0003V              PIC X.
           02     DFH0003O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0004A              PIC X.
           02     DFH0004C              PIC X.
           02     DFH0004P              PIC X.
           02     DFH0004H              PIC X.
           02     DFH0004V              PIC X.
           02     DFH0004O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0005A              PIC X.
           02     DFH0005C              PIC X.
           02     DFH0005P              PIC X.
           02     DFH0005H              PIC X.
           02     DFH0005V              PIC X.
           02     DFH0005O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0006A              PIC X.
           02     DFH0006C              PIC X.
           02     DFH0006P              PIC X.
           02     DFH0006H              PIC X.
           02     DFH0006V              PIC X.
           02     DFH0006O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0007A              PIC X.
           02     DFH0007C              PIC X.
           02     DFH0007P              PIC X.
           02     DFH0007H              PIC X.
           02     DFH0007V              PIC X.
           02     DFH0007O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0008A              PIC X.
           02     DFH0008C              PIC X.
           02     DFH0008P              PIC X.
           02     DFH0008H              PIC X.
           02     DFH0008V              PIC X.
           02     DFH0008O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0009A              PIC X.
           02     DFH0009C              PIC X.
           02     DFH0009P              PIC X.
           02     DFH0009H              PIC X.
           02     DFH0009V              PIC X.
           02     DFH0009O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0010A              PIC X.
           02     DFH0010C              PIC X.
           02     DFH0010P              PIC X.
           02     DFH0010H              PIC X.
           02     DFH0010V              PIC X.
           02     DFH0010O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0011A              PIC X.
           02     DFH0011C              PIC X.
           02     DFH0011P              PIC X.
           02     DFH0011H              PIC X.
           02     DFH0011V              PIC X.
           02     DFH0011O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0012A              PIC X.
           02     DFH0012C              PIC X.
           02     DFH0012P              PIC X.
           02     DFH0012H              PIC X.
           02     DFH0012V              PIC X.
           02     DFH0012O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0013A              PIC X.
           02     DFH0013C              PIC X.
           02     DFH0013P              PIC X.
           02     DFH0013H              PIC X.
           02     DFH0013V              PIC X.
           02     DFH0013O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0014A              PIC X.
           02     DFH0014C              PIC X.
           02     DFH0014P              PIC X.
           02     DFH0014H              PIC X.
           02     DFH0014V              PIC X.
           02     DFH0014O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0015A              PIC X.
           02     DFH0015C              PIC X.
           02     DFH0015P              PIC X.
           02     DFH0015H              PIC X.
           02     DFH0015V              PIC X.
           02     DFH0015O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0016A              PIC X.
           02     DFH0016C              PIC X.
           02     DFH0016P              PIC X.
           02     DFH0016H              PIC X.
           02     DFH0016V              PIC X.
           02     DFH0016O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0017A              PIC X.
           02     DFH0017C              PIC X.
           02     DFH0017P              PIC X.
           02     DFH0017H              PIC X.
           02     DFH0017V              PIC X.
           02     DFH0017O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0018A              PIC X.
           02     DFH0018C              PIC X.
           02     DFH0018P              PIC X.
           02     DFH0018H              PIC X.
           02     DFH0018V              PIC X.
           02     DFH0018O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0019A              PIC X.
           02     DFH0019C              PIC X.
           02     DFH0019P              PIC X.
           02     DFH0019H              PIC X.
           02     DFH0019V              PIC X.
           02     DFH0019O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0020A              PIC X.
           02     DFH0020C              PIC X.
           02     DFH0020P              PIC X.
           02     DFH0020H              PIC X.
           02     DFH0020V              PIC X.
           02     DFH0020O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0021A              PIC X.
           02     DFH0021C              PIC X.
           02     DFH0021P              PIC X.
           02     DFH0021H              PIC X.
           02     DFH0021V              PIC X.
           02     DFH0021O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0022A              PIC X.
           02     DFH0022C              PIC X.
           02     DFH0022P              PIC X.
           02     DFH0022H              PIC X.
           02     DFH0022V              PIC X.
           02     DFH0022O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0023A              PIC X.
           02     DFH0023C              PIC X.
           02     DFH0023P              PIC X.
           02     DFH0023H              PIC X.
           02     DFH0023V              PIC X.
           02     DFH0023O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0024A              PIC X.
           02     DFH0024C              PIC X.
           02     DFH0024P              PIC X.
           02     DFH0024H              PIC X.
           02     DFH0024V              PIC X.
           02     DFH0024O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0025A              PIC X.
           02     DFH0025C              PIC X.
           02     DFH0025P              PIC X.
           02     DFH0025H              PIC X.
           02     DFH0025V              PIC X.
           02     DFH0025O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0026A              PIC X.
           02     DFH0026C              PIC X.
           02     DFH0026P              PIC X.
           02     DFH0026H              PIC X.
           02     DFH0026V              PIC X.
           02     DFH0026O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0027A              PIC X.
           02     DFH0027C              PIC X.
           02     DFH0027P              PIC X.
           02     DFH0027H              PIC X.
           02     DFH0027V              PIC X.
           02     DFH0027O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0028A              PIC X.
           02     DFH0028C              PIC X.
           02     DFH0028P              PIC X.
           02     DFH0028H              PIC X.
           02     DFH0028V              PIC X.
           02     DFH0028O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0029A              PIC X.
           02     DFH0029C              PIC X.
           02     DFH0029P              PIC X.
           02     DFH0029H              PIC X.
           02     DFH0029V              PIC X.
           02     DFH0029O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0030A              PIC X.
           02     DFH0030C              PIC X.
           02     DFH0030P              PIC X.
           02     DFH0030H              PIC X.
           02     DFH0030V              PIC X.
           02     DFH0030O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0031A              PIC X.
           02     DFH0031C              PIC X.
           02     DFH0031P              PIC X.
           02     DFH0031H              PIC X.
           02     DFH0031V              PIC X.
           02     DFH0031O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0032A              PIC X.
           02     DFH0032C              PIC X.
           02     DFH0032P              PIC X.
           02     DFH0032H              PIC X.
           02     DFH0032V              PIC X.
           02     DFH0032O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0033A              PIC X.
           02     DFH0033C              PIC X.
           02     DFH0033P              PIC X.
           02     DFH0033H              PIC X.
           02     DFH0033V              PIC X.
           02     DFH0033O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0034A              PIC X.
           02     DFH0034C              PIC X.
           02     DFH0034P              PIC X.
           02     DFH0034H              PIC X.
           02     DFH0034V              PIC X.
           02     DFH0034O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0035A              PIC X.
           02     DFH0035C              PIC X.
           02     DFH0035P              PIC X.
           02     DFH0035H              PIC X.
           02     DFH0035V              PIC X.
           02     DFH0035O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0036A              PIC X.
           02     DFH0036C              PIC X.
           02     DFH0036P              PIC X.
           02     DFH0036H              PIC X.
           02     DFH0036V              PIC X.
           02     DFH0036O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0037A              PIC X.
           02     DFH0037C              PIC X.
           02     DFH0037P              PIC X.
           02     DFH0037H              PIC X.
           02     DFH0037V              PIC X.
           02     DFH0037O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0038A              PIC X.
           02     DFH0038C              PIC X.
           02     DFH0038P              PIC X.
           02     DFH0038H              PIC X.
           02     DFH0038V              PIC X.
           02     DFH0038O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0039A              PIC X.
           02     DFH0039C              PIC X.
           02     DFH0039P              PIC X.
           02     DFH0039H              PIC X.
           02     DFH0039V              PIC X.
           02     DFH0039O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0040A              PIC X.
           02     DFH0040C              PIC X.
           02     DFH0040P              PIC X.
           02     DFH0040H              PIC X.
           02     DFH0040V              PIC X.
           02     DFH0040O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0041A              PIC X.
           02     DFH0041C              PIC X.
           02     DFH0041P              PIC X.
           02     DFH0041H              PIC X.
           02     DFH0041V              PIC X.
           02     DFH0041O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0042A              PIC X.
           02     DFH0042C              PIC X.
           02     DFH0042P              PIC X.
           02     DFH0042H              PIC X.
           02     DFH0042V              PIC X.
           02     DFH0042O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0043A              PIC X.
           02     DFH0043C              PIC X.
           02     DFH0043P              PIC X.
           02     DFH0043H              PIC X.
           02     DFH0043V              PIC X.
           02     DFH0043O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0044A              PIC X.
           02     DFH0044C              PIC X.
           02     DFH0044P              PIC X.
           02     DFH0044H              PIC X.
           02     DFH0044V              PIC X.
           02     DFH0044O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0045A              PIC X.
           02     DFH0045C              PIC X.
           02     DFH0045P              PIC X.
           02     DFH0045H              PIC X.
           02     DFH0045V              PIC X.
           02     DFH0045O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0046A              PIC X.
           02     DFH0046C              PIC X.
           02     DFH0046P              PIC X.
           02     DFH0046H              PIC X.
           02     DFH0046V              PIC X.
           02     DFH0046O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0047A              PIC X.
           02     DFH0047C              PIC X.
           02     DFH0047P              PIC X.
           02     DFH0047H              PIC X.
           02     DFH0047V              PIC X.
           02     DFH0047O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0048A              PIC X.
           02     DFH0048C              PIC X.
           02     DFH0048P              PIC X.
           02     DFH0048H              PIC X.
           02     DFH0048V              PIC X.
           02     DFH0048O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0049A              PIC X.
           02     DFH0049C              PIC X.
           02     DFH0049P              PIC X.
           02     DFH0049H              PIC X.
           02     DFH0049V              PIC X.
           02     DFH0049O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0050A              PIC X.
           02     DFH0050C              PIC X.
           02     DFH0050P              PIC X.
           02     DFH0050H              PIC X.
           02     DFH0050V              PIC X.
           02     DFH0050O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0051A              PIC X.
           02     DFH0051C              PIC X.
           02     DFH0051P              PIC X.
           02     DFH0051H              PIC X.
           02     DFH0051V              PIC X.
           02     DFH0051O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0052A              PIC X.
           02     DFH0052C              PIC X.
           02     DFH0052P              PIC X.
           02     DFH0052H              PIC X.
           02     DFH0052V              PIC X.
           02     DFH0052O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0053A              PIC X.
           02     DFH0053C              PIC X.
           02     DFH0053P              PIC X.
           02     DFH0053H              PIC X.
           02     DFH0053V              PIC X.
           02     DFH0053O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0054A              PIC X.
           02     DFH0054C              PIC X.
           02     DFH0054P              PIC X.
           02     DFH0054H              PIC X.
           02     DFH0054V              PIC X.
           02     DFH0054O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0055A              PIC X.
           02     DFH0055C              PIC X.
           02     DFH0055P              PIC X.
           02     DFH0055H              PIC X.
           02     DFH0055V              PIC X.
           02     DFH0055O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0056A              PIC X.
           02     DFH0056C              PIC X.
           02     DFH0056P              PIC X.
           02     DFH0056H              PIC X.
           02     DFH0056V              PIC X.
           02     DFH0056O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0057A              PIC X.
           02     DFH0057C              PIC X.
           02     DFH0057P              PIC X.
           02     DFH0057H              PIC X.
           02     DFH0057V              PIC X.
           02     DFH0057O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0058A              PIC X.
           02     DFH0058C              PIC X.
           02     DFH0058P              PIC X.
           02     DFH0058H              PIC X.
           02     DFH0058V              PIC X.
           02     DFH0058O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0059A              PIC X.
           02     DFH0059C              PIC X.
           02     DFH0059P              PIC X.
           02     DFH0059H              PIC X.
           02     DFH0059V              PIC X.
           02     DFH0059O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0060A              PIC X.
           02     DFH0060C              PIC X.
           02     DFH0060P              PIC X.
           02     DFH0060H              PIC X.
           02     DFH0060V              PIC X.
           02     DFH0060O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0061A              PIC X.
           02     DFH0061C              PIC X.
           02     DFH0061P              PIC X.
           02     DFH0061H              PIC X.
           02     DFH0061V              PIC X.
           02     DFH0061O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0062A              PIC X.
           02     DFH0062C              PIC X.
           02     DFH0062P              PIC X.
           02     DFH0062H              PIC X.
           02     DFH0062V              PIC X.
           02     DFH0062O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0063A              PIC X.
           02     DFH0063C              PIC X.
           02     DFH0063P              PIC X.
           02     DFH0063H              PIC X.
           02     DFH0063V              PIC X.
           02     DFH0063O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0064A              PIC X.
           02     DFH0064C              PIC X.
           02     DFH0064P              PIC X.
           02     DFH0064H              PIC X.
           02     DFH0064V              PIC X.
           02     DFH0064O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0065A              PIC X.
           02     DFH0065C              PIC X.
           02     DFH0065P              PIC X.
           02     DFH0065H              PIC X.
           02     DFH0065V              PIC X.
           02     DFH0065O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0066A              PIC X.
           02     DFH0066C              PIC X.
           02     DFH0066P              PIC X.
           02     DFH0066H              PIC X.
           02     DFH0066V              PIC X.
           02     DFH0066O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0067A              PIC X.
           02     DFH0067C              PIC X.
           02     DFH0067P              PIC X.
           02     DFH0067H              PIC X.
           02     DFH0067V              PIC X.
           02     DFH0067O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0068A              PIC X.
           02     DFH0068C              PIC X.
           02     DFH0068P              PIC X.
           02     DFH0068H              PIC X.
           02     DFH0068V              PIC X.
           02     DFH0068O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0069A              PIC X.
           02     DFH0069C              PIC X.
           02     DFH0069P              PIC X.
           02     DFH0069H              PIC X.
           02     DFH0069V              PIC X.
           02     DFH0069O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0070A              PIC X.
           02     DFH0070C              PIC X.
           02     DFH0070P              PIC X.
           02     DFH0070H              PIC X.
           02     DFH0070V              PIC X.
           02     DFH0070O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0071A              PIC X.
           02     DFH0071C              PIC X.
           02     DFH0071P              PIC X.
           02     DFH0071H              PIC X.
           02     DFH0071V              PIC X.
           02     DFH0071O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0072A              PIC X.
           02     DFH0072C              PIC X.
           02     DFH0072P              PIC X.
           02     DFH0072H              PIC X.
           02     DFH0072V              PIC X.
           02     DFH0072O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0073A              PIC X.
           02     DFH0073C              PIC X.
           02     DFH0073P              PIC X.
           02     DFH0073H              PIC X.
           02     DFH0073V              PIC X.
           02     DFH0073O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0074A              PIC X.
           02     DFH0074C              PIC X.
           02     DFH0074P              PIC X.
           02     DFH0074H              PIC X.
           02     DFH0074V              PIC X.
           02     DFH0074O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0075A              PIC X.
           02     DFH0075C              PIC X.
           02     DFH0075P              PIC X.
           02     DFH0075H              PIC X.
           02     DFH0075V              PIC X.
           02     DFH0075O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0076A              PIC X.
           02     DFH0076C              PIC X.
           02     DFH0076P              PIC X.
           02     DFH0076H              PIC X.
           02     DFH0076V              PIC X.
           02     DFH0076O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0077A              PIC X.
           02     DFH0077C              PIC X.
           02     DFH0077P              PIC X.
           02     DFH0077H              PIC X.
           02     DFH0077V              PIC X.
           02     DFH0077O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0078A              PIC X.
           02     DFH0078C              PIC X.
           02     DFH0078P              PIC X.
           02     DFH0078H              PIC X.
           02     DFH0078V              PIC X.
           02     DFH0078O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0079A              PIC X.
           02     DFH0079C              PIC X.
           02     DFH0079P              PIC X.
           02     DFH0079H              PIC X.
           02     DFH0079V              PIC X.
           02     DFH0079O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0080A              PIC X.
           02     DFH0080C              PIC X.
           02     DFH0080P              PIC X.
           02     DFH0080H              PIC X.
           02     DFH0080V              PIC X.
           02     DFH0080O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0081A              PIC X.
           02     DFH0081C              PIC X.
           02     DFH0081P              PIC X.
           02     DFH0081H              PIC X.
           02     DFH0081V              PIC X.
           02     DFH0081O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0082A              PIC X.
           02     DFH0082C              PIC X.
           02     DFH0082P              PIC X.
           02     DFH0082H              PIC X.
           02     DFH0082V              PIC X.
           02     DFH0082O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0083A              PIC X.
           02     DFH0083C              PIC X.
           02     DFH0083P              PIC X.
           02     DFH0083H              PIC X.
           02     DFH0083V              PIC X.
           02     DFH0083O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0084A              PIC X.
           02     DFH0084C              PIC X.
           02     DFH0084P              PIC X.
           02     DFH0084H              PIC X.
           02     DFH0084V              PIC X.
           02     DFH0084O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0085A              PIC X.
           02     DFH0085C              PIC X.
           02     DFH0085P              PIC X.
           02     DFH0085H              PIC X.
           02     DFH0085V              PIC X.
           02     DFH0085O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0086A              PIC X.
           02     DFH0086C              PIC X.
           02     DFH0086P              PIC X.
           02     DFH0086H              PIC X.
           02     DFH0086V              PIC X.
           02     DFH0086O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0087A              PIC X.
           02     DFH0087C              PIC X.
           02     DFH0087P              PIC X.
           02     DFH0087H              PIC X.
           02     DFH0087V              PIC X.
           02     DFH0087O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0088A              PIC X.
           02     DFH0088C              PIC X.
           02     DFH0088P              PIC X.
           02     DFH0088H              PIC X.
           02     DFH0088V              PIC X.
           02     DFH0088O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0089A              PIC X.
           02     DFH0089C              PIC X.
           02     DFH0089P              PIC X.
           02     DFH0089H              PIC X.
           02     DFH0089V              PIC X.
           02     DFH0089O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0090A              PIC X.
           02     DFH0090C              PIC X.
           02     DFH0090P              PIC X.
           02     DFH0090H              PIC X.
           02     DFH0090V              PIC X.
           02     DFH0090O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0091A              PIC X.
           02     DFH0091C              PIC X.
           02     DFH0091P              PIC X.
           02     DFH0091H              PIC X.
           02     DFH0091V              PIC X.
           02     DFH0091O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0092A              PIC X.
           02     DFH0092C              PIC X.
           02     DFH0092P              PIC X.
           02     DFH0092H              PIC X.
           02     DFH0092V              PIC X.
           02     DFH0092O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0093A              PIC X.
           02     DFH0093C              PIC X.
           02     DFH0093P              PIC X.
           02     DFH0093H              PIC X.
           02     DFH0093V              PIC X.
           02     DFH0093O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0094A              PIC X.
           02     DFH0094C              PIC X.
           02     DFH0094P              PIC X.
           02     DFH0094H              PIC X.
           02     DFH0094V              PIC X.
           02     DFH0094O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0095A              PIC X.
           02     DFH0095C              PIC X.
           02     DFH0095P              PIC X.
           02     DFH0095H              PIC X.
           02     DFH0095V              PIC X.
           02     DFH0095O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0096A              PIC X.
           02     DFH0096C              PIC X.
           02     DFH0096P              PIC X.
           02     DFH0096H              PIC X.
           02     DFH0096V              PIC X.
           02     DFH0096O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0097A              PIC X.
           02     DFH0097C              PIC X.
           02     DFH0097P              PIC X.
           02     DFH0097H              PIC X.
           02     DFH0097V              PIC X.
           02     DFH0097O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0098A              PIC X.
           02     DFH0098C              PIC X.
           02     DFH0098P              PIC X.
           02     DFH0098H              PIC X.
           02     DFH0098V              PIC X.
           02     DFH0098O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0099A              PIC X.
           02     DFH0099C              PIC X.
           02     DFH0099P              PIC X.
           02     DFH0099H              PIC X.
           02     DFH0099V              PIC X.
           02     DFH0099O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0100A              PIC X.
           02     DFH0100C              PIC X.
           02     DFH0100P              PIC X.
           02     DFH0100H              PIC X.
           02     DFH0100V              PIC X.
           02     DFH0100O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0101A              PIC X.
           02     DFH0101C              PIC X.
           02     DFH0101P              PIC X.
           02     DFH0101H              PIC X.
           02     DFH0101V              PIC X.
           02     DFH0101O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0102A              PIC X.
           02     DFH0102C              PIC X.
           02     DFH0102P              PIC X.
           02     DFH0102H              PIC X.
           02     DFH0102V              PIC X.
           02     DFH0102O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0103A              PIC X.
           02     DFH0103C              PIC X.
           02     DFH0103P              PIC X.
           02     DFH0103H              PIC X.
           02     DFH0103V              PIC X.
           02     DFH0103O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0104A              PIC X.
           02     DFH0104C              PIC X.
           02     DFH0104P              PIC X.
           02     DFH0104H              PIC X.
           02     DFH0104V              PIC X.
           02     DFH0104O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0105A              PIC X.
           02     DFH0105C              PIC X.
           02     DFH0105P              PIC X.
           02     DFH0105H              PIC X.
           02     DFH0105V              PIC X.
           02     DFH0105O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0106A              PIC X.
           02     DFH0106C              PIC X.
           02     DFH0106P              PIC X.
           02     DFH0106H              PIC X.
           02     DFH0106V              PIC X.
           02     DFH0106O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0107A              PIC X.
           02     DFH0107C              PIC X.
           02     DFH0107P              PIC X.
           02     DFH0107H              PIC X.
           02     DFH0107V              PIC X.
           02     DFH0107O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0108A              PIC X.
           02     DFH0108C              PIC X.
           02     DFH0108P              PIC X.
           02     DFH0108H              PIC X.
           02     DFH0108V              PIC X.
           02     DFH0108O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0109A              PIC X.
           02     DFH0109C              PIC X.
           02     DFH0109P              PIC X.
           02     DFH0109H              PIC X.
           02     DFH0109V              PIC X.
           02     DFH0109O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0110A              PIC X.
           02     DFH0110C              PIC X.
           02     DFH0110P              PIC X.
           02     DFH0110H              PIC X.
           02     DFH0110V              PIC X.
           02     DFH0110O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0111A              PIC X.
           02     DFH0111C              PIC X.
           02     DFH0111P              PIC X.
           02     DFH0111H              PIC X.
           02     DFH0111V              PIC X.
           02     DFH0111O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0112A              PIC X.
           02     DFH0112C              PIC X.
           02     DFH0112P              PIC X.
           02     DFH0112H              PIC X.
           02     DFH0112V              PIC X.
           02     DFH0112O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0113A              PIC X.
           02     DFH0113C              PIC X.
           02     DFH0113P              PIC X.
           02     DFH0113H              PIC X.
           02     DFH0113V              PIC X.
           02     DFH0113O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0114A              PIC X.
           02     DFH0114C              PIC X.
           02     DFH0114P              PIC X.
           02     DFH0114H              PIC X.
           02     DFH0114V              PIC X.
           02     DFH0114O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0115A              PIC X.
           02     DFH0115C              PIC X.
           02     DFH0115P              PIC X.
           02     DFH0115H              PIC X.
           02     DFH0115V              PIC X.
           02     DFH0115O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0116A              PIC X.
           02     DFH0116C              PIC X.
           02     DFH0116P              PIC X.
           02     DFH0116H              PIC X.
           02     DFH0116V              PIC X.
           02     DFH0116O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0117A              PIC X.
           02     DFH0117C              PIC X.
           02     DFH0117P              PIC X.
           02     DFH0117H              PIC X.
           02     DFH0117V              PIC X.
           02     DFH0117O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0118A              PIC X.
           02     DFH0118C              PIC X.
           02     DFH0118P              PIC X.
           02     DFH0118H              PIC X.
           02     DFH0118V              PIC X.
           02     DFH0118O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0119A              PIC X.
           02     DFH0119C              PIC X.
           02     DFH0119P              PIC X.
           02     DFH0119H              PIC X.
           02     DFH0119V              PIC X.
           02     DFH0119O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0120A              PIC X.
           02     DFH0120C              PIC X.
           02     DFH0120P              PIC X.
           02     DFH0120H              PIC X.
           02     DFH0120V              PIC X.
           02     DFH0120O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0121A              PIC X.
           02     DFH0121C              PIC X.
           02     DFH0121P              PIC X.
           02     DFH0121H              PIC X.
           02     DFH0121V              PIC X.
           02     DFH0121O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0122A              PIC X.
           02     DFH0122C              PIC X.
           02     DFH0122P              PIC X.
           02     DFH0122H              PIC X.
           02     DFH0122V              PIC X.
           02     DFH0122O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0123A              PIC X.
           02     DFH0123C              PIC X.
           02     DFH0123P              PIC X.
           02     DFH0123H              PIC X.
           02     DFH0123V              PIC X.
           02     DFH0123O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0124A              PIC X.
           02     DFH0124C              PIC X.
           02     DFH0124P              PIC X.
           02     DFH0124H              PIC X.
           02     DFH0124V              PIC X.
           02     DFH0124O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0125A              PIC X.
           02     DFH0125C              PIC X.
           02     DFH0125P              PIC X.
           02     DFH0125H              PIC X.
           02     DFH0125V              PIC X.
           02     DFH0125O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0126A              PIC X.
           02     DFH0126C              PIC X.
           02     DFH0126P              PIC X.
           02     DFH0126H              PIC X.
           02     DFH0126V              PIC X.
           02     DFH0126O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0127A              PIC X.
           02     DFH0127C              PIC X.
           02     DFH0127P              PIC X.
           02     DFH0127H              PIC X.
           02     DFH0127V              PIC X.
           02     DFH0127O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0128A              PIC X.
           02     DFH0128C              PIC X.
           02     DFH0128P              PIC X.
           02     DFH0128H              PIC X.
           02     DFH0128V              PIC X.
           02     DFH0128O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0129A              PIC X.
           02     DFH0129C              PIC X.
           02     DFH0129P              PIC X.
           02     DFH0129H              PIC X.
           02     DFH0129V              PIC X.
           02     DFH0129O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0130A              PIC X.
           02     DFH0130C              PIC X.
           02     DFH0130P              PIC X.
           02     DFH0130H              PIC X.
           02     DFH0130V              PIC X.
           02     DFH0130O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0131A              PIC X.
           02     DFH0131C              PIC X.
           02     DFH0131P              PIC X.
           02     DFH0131H              PIC X.
           02     DFH0131V              PIC X.
           02     DFH0131O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0132A              PIC X.
           02     DFH0132C              PIC X.
           02     DFH0132P              PIC X.
           02     DFH0132H              PIC X.
           02     DFH0132V              PIC X.
           02     DFH0132O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0133A              PIC X.
           02     DFH0133C              PIC X.
           02     DFH0133P              PIC X.
           02     DFH0133H              PIC X.
           02     DFH0133V              PIC X.
           02     DFH0133O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0134A              PIC X.
           02     DFH0134C              PIC X.
           02     DFH0134P              PIC X.
           02     DFH0134H              PIC X.
           02     DFH0134V              PIC X.
           02     DFH0134O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0135A              PIC X.
           02     DFH0135C              PIC X.
           02     DFH0135P              PIC X.
           02     DFH0135H              PIC X.
           02     DFH0135V              PIC X.
           02     DFH0135O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0136A              PIC X.
           02     DFH0136C              PIC X.
           02     DFH0136P              PIC X.
           02     DFH0136H              PIC X.
           02     DFH0136V              PIC X.
           02     DFH0136O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0137A              PIC X.
           02     DFH0137C              PIC X.
           02     DFH0137P              PIC X.
           02     DFH0137H              PIC X.
           02     DFH0137V              PIC X.
           02     DFH0137O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0138A              PIC X.
           02     DFH0138C              PIC X.
           02     DFH0138P              PIC X.
           02     DFH0138H              PIC X.
           02     DFH0138V              PIC X.
           02     DFH0138O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0139A              PIC X.
           02     DFH0139C              PIC X.
           02     DFH0139P              PIC X.
           02     DFH0139H              PIC X.
           02     DFH0139V              PIC X.
           02     DFH0139O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0140A              PIC X.
           02     DFH0140C              PIC X.
           02     DFH0140P              PIC X.
           02     DFH0140H              PIC X.
           02     DFH0140V              PIC X.
           02     DFH0140O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0141A              PIC X.
           02     DFH0141C              PIC X.
           02     DFH0141P              PIC X.
           02     DFH0141H              PIC X.
           02     DFH0141V              PIC X.
           02     DFH0141O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0142A              PIC X.
           02     DFH0142C              PIC X.
           02     DFH0142P              PIC X.
           02     DFH0142H              PIC X.
           02     DFH0142V              PIC X.
           02     DFH0142O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0143A              PIC X.
           02     DFH0143C              PIC X.
           02     DFH0143P              PIC X.
           02     DFH0143H              PIC X.
           02     DFH0143V              PIC X.
           02     DFH0143O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0144A              PIC X.
           02     DFH0144C              PIC X.
           02     DFH0144P              PIC X.
           02     DFH0144H              PIC X.
           02     DFH0144V              PIC X.
           02     DFH0144O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0145A              PIC X.
           02     DFH0145C              PIC X.
           02     DFH0145P              PIC X.
           02     DFH0145H              PIC X.
           02     DFH0145V              PIC X.
           02     DFH0145O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0146A              PIC X.
           02     DFH0146C              PIC X.
           02     DFH0146P              PIC X.
           02     DFH0146H              PIC X.
           02     DFH0146V              PIC X.
           02     DFH0146O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0147A              PIC X.
           02     DFH0147C              PIC X.
           02     DFH0147P              PIC X.
           02     DFH0147H              PIC X.
           02     DFH0147V              PIC X.
           02     DFH0147O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0148A              PIC X.
           02     DFH0148C              PIC X.
           02     DFH0148P              PIC X.
           02     DFH0148H              PIC X.
           02     DFH0148V              PIC X.
           02     DFH0148O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0149A              PIC X.
           02     DFH0149C              PIC X.
           02     DFH0149P              PIC X.
           02     DFH0149H              PIC X.
           02     DFH0149V              PIC X.
           02     DFH0149O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0150A              PIC X.
           02     DFH0150C              PIC X.
           02     DFH0150P              PIC X.
           02     DFH0150H              PIC X.
           02     DFH0150V              PIC X.
           02     DFH0150O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0151A              PIC X.
           02     DFH0151C              PIC X.
           02     DFH0151P              PIC X.
           02     DFH0151H              PIC X.
           02     DFH0151V              PIC X.
           02     DFH0151O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0152A              PIC X.
           02     DFH0152C              PIC X.
           02     DFH0152P              PIC X.
           02     DFH0152H              PIC X.
           02     DFH0152V              PIC X.
           02     DFH0152O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0153A              PIC X.
           02     DFH0153C              PIC X.
           02     DFH0153P              PIC X.
           02     DFH0153H              PIC X.
           02     DFH0153V              PIC X.
           02     DFH0153O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0154A              PIC X.
           02     DFH0154C              PIC X.
           02     DFH0154P              PIC X.
           02     DFH0154H              PIC X.
           02     DFH0154V              PIC X.
           02     DFH0154O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0155A              PIC X.
           02     DFH0155C              PIC X.
           02     DFH0155P              PIC X.
           02     DFH0155H              PIC X.
           02     DFH0155V              PIC X.
           02     DFH0155O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0156A              PIC X.
           02     DFH0156C              PIC X.
           02     DFH0156P              PIC X.
           02     DFH0156H              PIC X.
           02     DFH0156V              PIC X.
           02     DFH0156O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0157A              PIC X.
           02     DFH0157C              PIC X.
           02     DFH0157P              PIC X.
           02     DFH0157H              PIC X.
           02     DFH0157V              PIC X.
           02     DFH0157O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0158A              PIC X.
           02     DFH0158C              PIC X.
           02     DFH0158P              PIC X.
           02     DFH0158H              PIC X.
           02     DFH0158V              PIC X.
           02     DFH0158O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0159A              PIC X.
           02     DFH0159C              PIC X.
           02     DFH0159P              PIC X.
           02     DFH0159H              PIC X.
           02     DFH0159V              PIC X.
           02     DFH0159O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0160A              PIC X.
           02     DFH0160C              PIC X.
           02     DFH0160P              PIC X.
           02     DFH0160H              PIC X.
           02     DFH0160V              PIC X.
           02     DFH0160O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0161A              PIC X.
           02     DFH0161C              PIC X.
           02     DFH0161P              PIC X.
           02     DFH0161H              PIC X.
           02     DFH0161V              PIC X.
           02     DFH0161O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0162A              PIC X.
           02     DFH0162C              PIC X.
           02     DFH0162P              PIC X.
           02     DFH0162H              PIC X.
           02     DFH0162V              PIC X.
           02     DFH0162O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0163A              PIC X.
           02     DFH0163C              PIC X.
           02     DFH0163P              PIC X.
           02     DFH0163H              PIC X.
           02     DFH0163V              PIC X.
           02     DFH0163O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0164A              PIC X.
           02     DFH0164C              PIC X.
           02     DFH0164P              PIC X.
           02     DFH0164H              PIC X.
           02     DFH0164V              PIC X.
           02     DFH0164O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0165A              PIC X.
           02     DFH0165C              PIC X.
           02     DFH0165P              PIC X.
           02     DFH0165H              PIC X.
           02     DFH0165V              PIC X.
           02     DFH0165O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0166A              PIC X.
           02     DFH0166C              PIC X.
           02     DFH0166P              PIC X.
           02     DFH0166H              PIC X.
           02     DFH0166V              PIC X.
           02     DFH0166O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0167A              PIC X.
           02     DFH0167C              PIC X.
           02     DFH0167P              PIC X.
           02     DFH0167H              PIC X.
           02     DFH0167V              PIC X.
           02     DFH0167O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0168A              PIC X.
           02     DFH0168C              PIC X.
           02     DFH0168P              PIC X.
           02     DFH0168H              PIC X.
           02     DFH0168V              PIC X.
           02     DFH0168O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0169A              PIC X.
           02     DFH0169C              PIC X.
           02     DFH0169P              PIC X.
           02     DFH0169H              PIC X.
           02     DFH0169V              PIC X.
           02     DFH0169O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0170A              PIC X.
           02     DFH0170C              PIC X.
           02     DFH0170P              PIC X.
           02     DFH0170H              PIC X.
           02     DFH0170V              PIC X.
           02     DFH0170O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0171A              PIC X.
           02     DFH0171C              PIC X.
           02     DFH0171P              PIC X.
           02     DFH0171H              PIC X.
           02     DFH0171V              PIC X.
           02     DFH0171O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0172A              PIC X.
           02     DFH0172C              PIC X.
           02     DFH0172P              PIC X.
           02     DFH0172H              PIC X.
           02     DFH0172V              PIC X.
           02     DFH0172O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0173A              PIC X.
           02     DFH0173C              PIC X.
           02     DFH0173P              PIC X.
           02     DFH0173H              PIC X.
           02     DFH0173V              PIC X.
           02     DFH0173O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0174A              PIC X.
           02     DFH0174C              PIC X.
           02     DFH0174P              PIC X.
           02     DFH0174H              PIC X.
           02     DFH0174V              PIC X.
           02     DFH0174O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0175A              PIC X.
           02     DFH0175C              PIC X.
           02     DFH0175P              PIC X.
           02     DFH0175H              PIC X.
           02     DFH0175V              PIC X.
           02     DFH0175O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0176A              PIC X.
           02     DFH0176C              PIC X.
           02     DFH0176P              PIC X.
           02     DFH0176H              PIC X.
           02     DFH0176V              PIC X.
           02     DFH0176O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0177A              PIC X.
           02     DFH0177C              PIC X.
           02     DFH0177P              PIC X.
           02     DFH0177H              PIC X.
           02     DFH0177V              PIC X.
           02     DFH0177O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0178A              PIC X.
           02     DFH0178C              PIC X.
           02     DFH0178P              PIC X.
           02     DFH0178H              PIC X.
           02     DFH0178V              PIC X.
           02     DFH0178O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0179A              PIC X.
           02     DFH0179C              PIC X.
           02     DFH0179P              PIC X.
           02     DFH0179H              PIC X.
           02     DFH0179V              PIC X.
           02     DFH0179O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0180A              PIC X.
           02     DFH0180C              PIC X.
           02     DFH0180P              PIC X.
           02     DFH0180H              PIC X.
           02     DFH0180V              PIC X.
           02     DFH0180O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0181A              PIC X.
           02     DFH0181C              PIC X.
           02     DFH0181P              PIC X.
           02     DFH0181H              PIC X.
           02     DFH0181V              PIC X.
           02     DFH0181O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0182A              PIC X.
           02     DFH0182C              PIC X.
           02     DFH0182P              PIC X.
           02     DFH0182H              PIC X.
           02     DFH0182V              PIC X.
           02     DFH0182O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0183A              PIC X.
           02     DFH0183C              PIC X.
           02     DFH0183P              PIC X.
           02     DFH0183H              PIC X.
           02     DFH0183V              PIC X.
           02     DFH0183O              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0184A              PIC X.
           02     DFH0184C              PIC X.
           02     DFH0184P              PIC X.
           02     DFH0184H              PIC X.
           02     DFH0184V              PIC X.
           02     DFH0184O              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0185A              PIC X.
           02     DFH0185C              PIC X.
           02     DFH0185P              PIC X.
           02     DFH0185H              PIC X.
           02     DFH0185V              PIC X.
           02     DFH0185O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0186A              PIC X.
           02     DFH0186C              PIC X.
           02     DFH0186P              PIC X.
           02     DFH0186H              PIC X.
           02     DFH0186V              PIC X.
           02     DFH0186O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0187A              PIC X.
           02     DFH0187C              PIC X.
           02     DFH0187P              PIC X.
           02     DFH0187H              PIC X.
           02     DFH0187V              PIC X.
           02     DFH0187O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0188A              PIC X.
           02     DFH0188C              PIC X.
           02     DFH0188P              PIC X.
           02     DFH0188H              PIC X.
           02     DFH0188V              PIC X.
           02     DFH0188O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0189A              PIC X.
           02     DFH0189C              PIC X.
           02     DFH0189P              PIC X.
           02     DFH0189H              PIC X.
           02     DFH0189V              PIC X.
           02     DFH0189O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0190A              PIC X.
           02     DFH0190C              PIC X.
           02     DFH0190P              PIC X.
           02     DFH0190H              PIC X.
           02     DFH0190V              PIC X.
           02     DFH0190O              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0191A              PIC X.
           02     DFH0191C              PIC X.
           02     DFH0191P              PIC X.
           02     DFH0191H              PIC X.
           02     DFH0191V              PIC X.
           02     DFH0191O              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0192A              PIC X.
           02     DFH0192C              PIC X.
           02     DFH0192P              PIC X.
           02     DFH0192H              PIC X.
           02     DFH0192V              PIC X.
           02     DFH0192O              PIC Z(11)9,99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0193A              PIC X.
           02     DFH0193C              PIC X.
           02     DFH0193P              PIC X.
           02     DFH0193H              PIC X.
           02     DFH0193V              PIC X.
           02     DFH0193O              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0194A              PIC X.
           02     DFH0194C              PIC X.
           02     DFH0194P              PIC X.
           02     DFH0194H              PIC X.
           02     DFH0194V              PIC X.
           02     DFH0194O              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0195A              PIC X.
           02     DFH0195C              PIC X.
           02     DFH0195P              PIC X.
           02     DFH0195H              PIC X.
           02     DFH0195V              PIC X.
           02     DFH0195O              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDP              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDV              PIC X.
           02     MZONCMDO              PIC X(14).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(58).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(4).
