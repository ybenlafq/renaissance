      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG23   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG23I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MFONCL                PIC S9(4) COMP-5.
           02     MFONCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MFONCI                PIC X(3).
           02     MSOCOMPL              PIC S9(4) COMP-5.
           02     MSOCOMPF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCOMPI              PIC X(3).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(24).
           02     MPAGEEL               PIC S9(4) COMP-5.
           02     MPAGEEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEEI               PIC XX.
           02     MPAGEML               PIC S9(4) COMP-5.
           02     MPAGEMF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MPAGEMI               PIC XX.
           02     MNJRNGL               PIC S9(4) COMP-5.
           02     MNJRNGF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNJRNGI               PIC X(9).
           02     MLIBJRNL              PIC S9(4) COMP-5.
           02     MLIBJRNF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBJRNI              PIC X(30).
           02     MNJRNL                PIC S9(4) COMP-5.
           02     MNJRNF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNJRNI                PIC X(10).
           02     DFH0001L              PIC S9(4) COMP-5.
           02     DFH0001F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0001I              PIC X(9).
           02     DFH0002L              PIC S9(4) COMP-5.
           02     DFH0002F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0002I              PIC X(30).
           02     DFH0003L              PIC S9(4) COMP-5.
           02     DFH0003F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0003I              PIC X(10).
           02     DFH0004L              PIC S9(4) COMP-5.
           02     DFH0004F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0004I              PIC X(9).
           02     DFH0005L              PIC S9(4) COMP-5.
           02     DFH0005F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0005I              PIC X(30).
           02     DFH0006L              PIC S9(4) COMP-5.
           02     DFH0006F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0006I              PIC X(10).
           02     DFH0007L              PIC S9(4) COMP-5.
           02     DFH0007F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0007I              PIC X(9).
           02     DFH0008L              PIC S9(4) COMP-5.
           02     DFH0008F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0008I              PIC X(30).
           02     DFH0009L              PIC S9(4) COMP-5.
           02     DFH0009F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0009I              PIC X(10).
           02     DFH0010L              PIC S9(4) COMP-5.
           02     DFH0010F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0010I              PIC X(9).
           02     DFH0011L              PIC S9(4) COMP-5.
           02     DFH0011F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0011I              PIC X(30).
           02     DFH0012L              PIC S9(4) COMP-5.
           02     DFH0012F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0012I              PIC X(10).
           02     DFH0013L              PIC S9(4) COMP-5.
           02     DFH0013F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0013I              PIC X(9).
           02     DFH0014L              PIC S9(4) COMP-5.
           02     DFH0014F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0014I              PIC X(30).
           02     DFH0015L              PIC S9(4) COMP-5.
           02     DFH0015F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0015I              PIC X(10).
           02     DFH0016L              PIC S9(4) COMP-5.
           02     DFH0016F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0016I              PIC X(9).
           02     DFH0017L              PIC S9(4) COMP-5.
           02     DFH0017F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0017I              PIC X(30).
           02     DFH0018L              PIC S9(4) COMP-5.
           02     DFH0018F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0018I              PIC X(10).
           02     DFH0019L              PIC S9(4) COMP-5.
           02     DFH0019F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0019I              PIC X(9).
           02     DFH0020L              PIC S9(4) COMP-5.
           02     DFH0020F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0020I              PIC X(30).
           02     DFH0021L              PIC S9(4) COMP-5.
           02     DFH0021F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0021I              PIC X(10).
           02     DFH0022L              PIC S9(4) COMP-5.
           02     DFH0022F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0022I              PIC X(9).
           02     DFH0023L              PIC S9(4) COMP-5.
           02     DFH0023F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0023I              PIC X(30).
           02     DFH0024L              PIC S9(4) COMP-5.
           02     DFH0024F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0024I              PIC X(10).
           02     DFH0025L              PIC S9(4) COMP-5.
           02     DFH0025F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0025I              PIC X(9).
           02     DFH0026L              PIC S9(4) COMP-5.
           02     DFH0026F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0026I              PIC X(30).
           02     DFH0027L              PIC S9(4) COMP-5.
           02     DFH0027F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0027I              PIC X(10).
           02     DFH0028L              PIC S9(4) COMP-5.
           02     DFH0028F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0028I              PIC X(9).
           02     DFH0029L              PIC S9(4) COMP-5.
           02     DFH0029F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0029I              PIC X(30).
           02     DFH0030L              PIC S9(4) COMP-5.
           02     DFH0030F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0030I              PIC X(10).
           02     DFH0031L              PIC S9(4) COMP-5.
           02     DFH0031F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0031I              PIC X(9).
           02     DFH0032L              PIC S9(4) COMP-5.
           02     DFH0032F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0032I              PIC X(30).
           02     DFH0033L              PIC S9(4) COMP-5.
           02     DFH0033F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0033I              PIC X(10).
           02     DFH0034L              PIC S9(4) COMP-5.
           02     DFH0034F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0034I              PIC X(9).
           02     DFH0035L              PIC S9(4) COMP-5.
           02     DFH0035F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0035I              PIC X(30).
           02     DFH0036L              PIC S9(4) COMP-5.
           02     DFH0036F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0036I              PIC X(10).
           02     DFH0037L              PIC S9(4) COMP-5.
           02     DFH0037F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0037I              PIC X(9).
           02     DFH0038L              PIC S9(4) COMP-5.
           02     DFH0038F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0038I              PIC X(30).
           02     DFH0039L              PIC S9(4) COMP-5.
           02     DFH0039F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0039I              PIC X(10).
           02     DFH0040L              PIC S9(4) COMP-5.
           02     DFH0040F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0040I              PIC X(9).
           02     DFH0041L              PIC S9(4) COMP-5.
           02     DFH0041F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0041I              PIC X(30).
           02     DFH0042L              PIC S9(4) COMP-5.
           02     DFH0042F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0042I              PIC X(10).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONCMDI              PIC X(3).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(73).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(4).
       01  EFG23O REDEFINES EFG23I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MFONCA                PIC X.
           02     MFONCC                PIC X.
           02     MFONCP                PIC X.
           02     MFONCH                PIC X.
           02     MFONCV                PIC X.
           02     MFONCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCOMPA              PIC X.
           02     MSOCOMPC              PIC X.
           02     MSOCOMPP              PIC X.
           02     MSOCOMPH              PIC X.
           02     MSOCOMPV              PIC X.
           02     MSOCOMPO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(24).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEEA               PIC X.
           02     MPAGEEC               PIC X.
           02     MPAGEEP               PIC X.
           02     MPAGEEH               PIC X.
           02     MPAGEEV               PIC X.
           02     MPAGEEO               PIC 99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MPAGEMA               PIC X.
           02     MPAGEMC               PIC X.
           02     MPAGEMP               PIC X.
           02     MPAGEMH               PIC X.
           02     MPAGEMV               PIC X.
           02     MPAGEMO               PIC 99.
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNJRNGA               PIC X.
           02     MNJRNGC               PIC X.
           02     MNJRNGP               PIC X.
           02     MNJRNGH               PIC X.
           02     MNJRNGV               PIC X.
           02     MNJRNGO               PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBJRNA              PIC X.
           02     MLIBJRNC              PIC X.
           02     MLIBJRNP              PIC X.
           02     MLIBJRNH              PIC X.
           02     MLIBJRNV              PIC X.
           02     MLIBJRNO              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNJRNA                PIC X.
           02     MNJRNC                PIC X.
           02     MNJRNP                PIC X.
           02     MNJRNH                PIC X.
           02     MNJRNV                PIC X.
           02     MNJRNO                PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0001A              PIC X.
           02     DFH0001C              PIC X.
           02     DFH0001P              PIC X.
           02     DFH0001H              PIC X.
           02     DFH0001V              PIC X.
           02     DFH0001O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0002A              PIC X.
           02     DFH0002C              PIC X.
           02     DFH0002P              PIC X.
           02     DFH0002H              PIC X.
           02     DFH0002V              PIC X.
           02     DFH0002O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0003A              PIC X.
           02     DFH0003C              PIC X.
           02     DFH0003P              PIC X.
           02     DFH0003H              PIC X.
           02     DFH0003V              PIC X.
           02     DFH0003O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0004A              PIC X.
           02     DFH0004C              PIC X.
           02     DFH0004P              PIC X.
           02     DFH0004H              PIC X.
           02     DFH0004V              PIC X.
           02     DFH0004O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0005A              PIC X.
           02     DFH0005C              PIC X.
           02     DFH0005P              PIC X.
           02     DFH0005H              PIC X.
           02     DFH0005V              PIC X.
           02     DFH0005O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0006A              PIC X.
           02     DFH0006C              PIC X.
           02     DFH0006P              PIC X.
           02     DFH0006H              PIC X.
           02     DFH0006V              PIC X.
           02     DFH0006O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0007A              PIC X.
           02     DFH0007C              PIC X.
           02     DFH0007P              PIC X.
           02     DFH0007H              PIC X.
           02     DFH0007V              PIC X.
           02     DFH0007O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0008A              PIC X.
           02     DFH0008C              PIC X.
           02     DFH0008P              PIC X.
           02     DFH0008H              PIC X.
           02     DFH0008V              PIC X.
           02     DFH0008O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0009A              PIC X.
           02     DFH0009C              PIC X.
           02     DFH0009P              PIC X.
           02     DFH0009H              PIC X.
           02     DFH0009V              PIC X.
           02     DFH0009O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0010A              PIC X.
           02     DFH0010C              PIC X.
           02     DFH0010P              PIC X.
           02     DFH0010H              PIC X.
           02     DFH0010V              PIC X.
           02     DFH0010O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0011A              PIC X.
           02     DFH0011C              PIC X.
           02     DFH0011P              PIC X.
           02     DFH0011H              PIC X.
           02     DFH0011V              PIC X.
           02     DFH0011O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0012A              PIC X.
           02     DFH0012C              PIC X.
           02     DFH0012P              PIC X.
           02     DFH0012H              PIC X.
           02     DFH0012V              PIC X.
           02     DFH0012O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0013A              PIC X.
           02     DFH0013C              PIC X.
           02     DFH0013P              PIC X.
           02     DFH0013H              PIC X.
           02     DFH0013V              PIC X.
           02     DFH0013O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0014A              PIC X.
           02     DFH0014C              PIC X.
           02     DFH0014P              PIC X.
           02     DFH0014H              PIC X.
           02     DFH0014V              PIC X.
           02     DFH0014O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0015A              PIC X.
           02     DFH0015C              PIC X.
           02     DFH0015P              PIC X.
           02     DFH0015H              PIC X.
           02     DFH0015V              PIC X.
           02     DFH0015O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0016A              PIC X.
           02     DFH0016C              PIC X.
           02     DFH0016P              PIC X.
           02     DFH0016H              PIC X.
           02     DFH0016V              PIC X.
           02     DFH0016O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0017A              PIC X.
           02     DFH0017C              PIC X.
           02     DFH0017P              PIC X.
           02     DFH0017H              PIC X.
           02     DFH0017V              PIC X.
           02     DFH0017O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0018A              PIC X.
           02     DFH0018C              PIC X.
           02     DFH0018P              PIC X.
           02     DFH0018H              PIC X.
           02     DFH0018V              PIC X.
           02     DFH0018O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0019A              PIC X.
           02     DFH0019C              PIC X.
           02     DFH0019P              PIC X.
           02     DFH0019H              PIC X.
           02     DFH0019V              PIC X.
           02     DFH0019O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0020A              PIC X.
           02     DFH0020C              PIC X.
           02     DFH0020P              PIC X.
           02     DFH0020H              PIC X.
           02     DFH0020V              PIC X.
           02     DFH0020O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0021A              PIC X.
           02     DFH0021C              PIC X.
           02     DFH0021P              PIC X.
           02     DFH0021H              PIC X.
           02     DFH0021V              PIC X.
           02     DFH0021O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0022A              PIC X.
           02     DFH0022C              PIC X.
           02     DFH0022P              PIC X.
           02     DFH0022H              PIC X.
           02     DFH0022V              PIC X.
           02     DFH0022O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0023A              PIC X.
           02     DFH0023C              PIC X.
           02     DFH0023P              PIC X.
           02     DFH0023H              PIC X.
           02     DFH0023V              PIC X.
           02     DFH0023O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0024A              PIC X.
           02     DFH0024C              PIC X.
           02     DFH0024P              PIC X.
           02     DFH0024H              PIC X.
           02     DFH0024V              PIC X.
           02     DFH0024O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0025A              PIC X.
           02     DFH0025C              PIC X.
           02     DFH0025P              PIC X.
           02     DFH0025H              PIC X.
           02     DFH0025V              PIC X.
           02     DFH0025O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0026A              PIC X.
           02     DFH0026C              PIC X.
           02     DFH0026P              PIC X.
           02     DFH0026H              PIC X.
           02     DFH0026V              PIC X.
           02     DFH0026O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0027A              PIC X.
           02     DFH0027C              PIC X.
           02     DFH0027P              PIC X.
           02     DFH0027H              PIC X.
           02     DFH0027V              PIC X.
           02     DFH0027O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0028A              PIC X.
           02     DFH0028C              PIC X.
           02     DFH0028P              PIC X.
           02     DFH0028H              PIC X.
           02     DFH0028V              PIC X.
           02     DFH0028O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0029A              PIC X.
           02     DFH0029C              PIC X.
           02     DFH0029P              PIC X.
           02     DFH0029H              PIC X.
           02     DFH0029V              PIC X.
           02     DFH0029O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0030A              PIC X.
           02     DFH0030C              PIC X.
           02     DFH0030P              PIC X.
           02     DFH0030H              PIC X.
           02     DFH0030V              PIC X.
           02     DFH0030O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0031A              PIC X.
           02     DFH0031C              PIC X.
           02     DFH0031P              PIC X.
           02     DFH0031H              PIC X.
           02     DFH0031V              PIC X.
           02     DFH0031O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0032A              PIC X.
           02     DFH0032C              PIC X.
           02     DFH0032P              PIC X.
           02     DFH0032H              PIC X.
           02     DFH0032V              PIC X.
           02     DFH0032O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0033A              PIC X.
           02     DFH0033C              PIC X.
           02     DFH0033P              PIC X.
           02     DFH0033H              PIC X.
           02     DFH0033V              PIC X.
           02     DFH0033O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0034A              PIC X.
           02     DFH0034C              PIC X.
           02     DFH0034P              PIC X.
           02     DFH0034H              PIC X.
           02     DFH0034V              PIC X.
           02     DFH0034O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0035A              PIC X.
           02     DFH0035C              PIC X.
           02     DFH0035P              PIC X.
           02     DFH0035H              PIC X.
           02     DFH0035V              PIC X.
           02     DFH0035O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0036A              PIC X.
           02     DFH0036C              PIC X.
           02     DFH0036P              PIC X.
           02     DFH0036H              PIC X.
           02     DFH0036V              PIC X.
           02     DFH0036O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0037A              PIC X.
           02     DFH0037C              PIC X.
           02     DFH0037P              PIC X.
           02     DFH0037H              PIC X.
           02     DFH0037V              PIC X.
           02     DFH0037O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0038A              PIC X.
           02     DFH0038C              PIC X.
           02     DFH0038P              PIC X.
           02     DFH0038H              PIC X.
           02     DFH0038V              PIC X.
           02     DFH0038O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0039A              PIC X.
           02     DFH0039C              PIC X.
           02     DFH0039P              PIC X.
           02     DFH0039H              PIC X.
           02     DFH0039V              PIC X.
           02     DFH0039O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0040A              PIC X.
           02     DFH0040C              PIC X.
           02     DFH0040P              PIC X.
           02     DFH0040H              PIC X.
           02     DFH0040V              PIC X.
           02     DFH0040O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0041A              PIC X.
           02     DFH0041C              PIC X.
           02     DFH0041P              PIC X.
           02     DFH0041H              PIC X.
           02     DFH0041V              PIC X.
           02     DFH0041O              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0042A              PIC X.
           02     DFH0042C              PIC X.
           02     DFH0042P              PIC X.
           02     DFH0042H              PIC X.
           02     DFH0042V              PIC X.
           02     DFH0042O              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDP              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDV              PIC X.
           02     MZONCMDO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(73).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(4).
