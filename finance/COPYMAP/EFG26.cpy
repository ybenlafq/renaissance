      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG26   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG26I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNETABL               PIC S9(4) COMP-5.
           02     MNETABF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETABI               PIC X(3).
           02     MDOCOUTL              PIC S9(4) COMP-5.
           02     MDOCOUTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDOCOUTI              PIC X(2).
           02     MDOCINL               PIC S9(4) COMP-5.
           02     MDOCINF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MDOCINI               PIC X(2).
           02     MNATFACL              PIC S9(4) COMP-5.
           02     MNATFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNATFACI              PIC X(5).
           02     MTXTVAL               PIC S9(4) COMP-5.
           02     MTXTVAF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MTXTVAI               PIC X(5).
           02     MNBJOURL              PIC S9(4) COMP-5.
           02     MNBJOURF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNBJOURI              PIC X(3).
           02     MTYPREGL              PIC S9(4) COMP-5.
           02     MTYPREGF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTYPREGI              PIC X(2).
           02     MJRNHTCL              PIC S9(4) COMP-5.
           02     MJRNHTCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNHTCI              PIC X(10).
           02     MJRNTGCL              PIC S9(4) COMP-5.
           02     MJRNTGCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNTGCI              PIC X(10).
           02     MJRNHTPL              PIC S9(4) COMP-5.
           02     MJRNHTPF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNHTPI              PIC X(10).
           02     MJRNTGPL              PIC S9(4) COMP-5.
           02     MJRNTGPF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNTGPI              PIC X(10).
           02     MJRNHTFL              PIC S9(4) COMP-5.
           02     MJRNHTFF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNHTFI              PIC X(10).
           02     MJRNTGFL              PIC S9(4) COMP-5.
           02     MJRNTGFF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MJRNTGFI              PIC X(10).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(78).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(4).
       01  EFG26O REDEFINES EFG26I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETABA               PIC X.
           02     MNETABC               PIC X.
           02     MNETABP               PIC X.
           02     MNETABH               PIC X.
           02     MNETABV               PIC X.
           02     MNETABO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDOCOUTA              PIC X.
           02     MDOCOUTC              PIC X.
           02     MDOCOUTP              PIC X.
           02     MDOCOUTH              PIC X.
           02     MDOCOUTV              PIC X.
           02     MDOCOUTO              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDOCINA               PIC X.
           02     MDOCINC               PIC X.
           02     MDOCINP               PIC X.
           02     MDOCINH               PIC X.
           02     MDOCINV               PIC X.
           02     MDOCINO               PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNATFACA              PIC X.
           02     MNATFACC              PIC X.
           02     MNATFACP              PIC X.
           02     MNATFACH              PIC X.
           02     MNATFACV              PIC X.
           02     MNATFACO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTXTVAA               PIC X.
           02     MTXTVAC               PIC X.
           02     MTXTVAP               PIC X.
           02     MTXTVAH               PIC X.
           02     MTXTVAV               PIC X.
           02     MTXTVAO               PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNBJOURA              PIC X.
           02     MNBJOURC              PIC X.
           02     MNBJOURP              PIC X.
           02     MNBJOURH              PIC X.
           02     MNBJOURV              PIC X.
           02     MNBJOURO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTYPREGA              PIC X.
           02     MTYPREGC              PIC X.
           02     MTYPREGP              PIC X.
           02     MTYPREGH              PIC X.
           02     MTYPREGV              PIC X.
           02     MTYPREGO              PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNHTCA              PIC X.
           02     MJRNHTCC              PIC X.
           02     MJRNHTCP              PIC X.
           02     MJRNHTCH              PIC X.
           02     MJRNHTCV              PIC X.
           02     MJRNHTCO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNTGCA              PIC X.
           02     MJRNTGCC              PIC X.
           02     MJRNTGCP              PIC X.
           02     MJRNTGCH              PIC X.
           02     MJRNTGCV              PIC X.
           02     MJRNTGCO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNHTPA              PIC X.
           02     MJRNHTPC              PIC X.
           02     MJRNHTPP              PIC X.
           02     MJRNHTPH              PIC X.
           02     MJRNHTPV              PIC X.
           02     MJRNHTPO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNTGPA              PIC X.
           02     MJRNTGPC              PIC X.
           02     MJRNTGPP              PIC X.
           02     MJRNTGPH              PIC X.
           02     MJRNTGPV              PIC X.
           02     MJRNTGPO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNHTFA              PIC X.
           02     MJRNHTFC              PIC X.
           02     MJRNHTFP              PIC X.
           02     MJRNHTFH              PIC X.
           02     MJRNHTFV              PIC X.
           02     MJRNHTFO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MJRNTGFA              PIC X.
           02     MJRNTGFC              PIC X.
           02     MJRNTGFP              PIC X.
           02     MJRNTGFH              PIC X.
           02     MJRNTGFV              PIC X.
           02     MJRNTGFO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(78).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(4).
