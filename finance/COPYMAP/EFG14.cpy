      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG14   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG14I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MFONCL                PIC S9(4) COMP-5.
           02     MFONCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MFONCI                PIC X(3).
           02     MTITREL               PIC S9(4) COMP-5.
           02     MTITREF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MTITREI               PIC X(24).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNLIEUL               PIC S9(4) COMP-5.
           02     MNLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUI               PIC X(3).
           02     MSECORIL              PIC S9(4) COMP-5.
           02     MSECORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECORII              PIC X(6).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(20).
           02     MTYPNATL              PIC S9(4) COMP-5.
           02     MTYPNATF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTYPNATI              PIC X(30).
           02     MTIERSEL              PIC S9(4) COMP-5.
           02     MTIERSEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIERSEI              PIC X(8).
           02     MSOCCL                PIC S9(4) COMP-5.
           02     MSOCCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCCI                PIC X(3).
           02     MSERVL                PIC S9(4) COMP-5.
           02     MSERVF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSERVI                PIC X(5).
           02     MLSOCCL               PIC S9(4) COMP-5.
           02     MLSOCCF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCI               PIC X(20).
           02     MNUMFACL              PIC S9(4) COMP-5.
           02     MNUMFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMFACI              PIC X(7).
           02     MDCREATL              PIC S9(4) COMP-5.
           02     MDCREATF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDCREATI              PIC X(10).
           02     MSOCDESL              PIC S9(4) COMP-5.
           02     MSOCDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCDESI              PIC X(3).
           02     MLIEUDEL              PIC S9(4) COMP-5.
           02     MLIEUDEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIEUDEI              PIC X(3).
           02     MSECDESL              PIC S9(4) COMP-5.
           02     MSECDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECDESI              PIC X(6).
           02     MLIBDESL              PIC S9(4) COMP-5.
           02     MLIBDESF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBDESI              PIC X(20).
           02     MLETTREL              PIC S9(4) COMP-5.
           02     MLETTREF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLETTREI              PIC X(10).
           02     MTIERSRL              PIC S9(4) COMP-5.
           02     MTIERSRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIERSRI              PIC X(8).
           02     MSERVDEL              PIC S9(4) COMP-5.
           02     MSERVDEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSERVDEI              PIC X(5).
           02     MLETTRRL              PIC S9(4) COMP-5.
           02     MLETTRRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLETTRRI              PIC X(10).
           02     MLIBVALL              PIC S9(4) COMP-5.
           02     MLIBVALF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBVALI              PIC X(12).
           02     MNUMORIL              PIC S9(4) COMP-5.
           02     MNUMORIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMORII              PIC X(7).
           02     MDCOMPTL              PIC S9(4) COMP-5.
           02     MDCOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDCOMPTI              PIC X(10).
           02     MDECHEAL              PIC S9(4) COMP-5.
           02     MDECHEAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDECHEAI              PIC X(10).
           02     DFH0001L              PIC S9(4) COMP-5.
           02     DFH0001F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0001I              PIC X(9).
           02     MDORIGL               PIC S9(4) COMP-5.
           02     MDORIGF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MDORIGI               PIC X(10).
           02     MEXERCIL              PIC S9(4) COMP-5.
           02     MEXERCIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MEXERCII              PIC X(4).
           02     MNPERIOL              PIC S9(4) COMP-5.
           02     MNPERIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNPERIOI              PIC X(3).
           02     MLPERIOL              PIC S9(4) COMP-5.
           02     MLPERIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLPERIOI              PIC X(14).
           02     MDPASSAL              PIC S9(4) COMP-5.
           02     MDPASSAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDPASSAI              PIC X(10).
           02     MLIBCRIL              PIC S9(4) COMP-5.
           02     MLIBCRIF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBCRII              PIC X(15).
           02     MCRITERL              PIC S9(4) COMP-5.
           02     MCRITERF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCRITERI              PIC X(58).
           02     MENTETEL              PIC S9(4) COMP-5.
           02     MENTETEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MENTETEI              PIC X(31).
           02     MCOMMENL              PIC S9(4) COMP-5.
           02     MCOMMENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCOMMENI              PIC X(70).
           02     DFH0002L              PIC S9(4) COMP-5.
           02     DFH0002F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0002I              PIC X(70).
           02     DFH0003L              PIC S9(4) COMP-5.
           02     DFH0003F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0003I              PIC X(70).
           02     DFH0004L              PIC S9(4) COMP-5.
           02     DFH0004F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0004I              PIC X(70).
           02     DFH0005L              PIC S9(4) COMP-5.
           02     DFH0005F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0005I              PIC X(70).
           02     DFH0006L              PIC S9(4) COMP-5.
           02     DFH0006F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0006I              PIC X(70).
           02     DFH0007L              PIC S9(4) COMP-5.
           02     DFH0007F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0007I              PIC X(70).
           02     DFH0008L              PIC S9(4) COMP-5.
           02     DFH0008F              PIC X.
           02     DFHMS                 PIC X(4).
           02     DFH0008I              PIC X(70).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONCMDI              PIC X(15).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(58).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(4).
       01  EFG14O REDEFINES EFG14I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MFONCA                PIC X.
           02     MFONCC                PIC X.
           02     MFONCP                PIC X.
           02     MFONCH                PIC X.
           02     MFONCV                PIC X.
           02     MFONCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTITREA               PIC X.
           02     MTITREC               PIC X.
           02     MTITREP               PIC X.
           02     MTITREH               PIC X.
           02     MTITREV               PIC X.
           02     MTITREO               PIC X(24).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUA               PIC X.
           02     MNLIEUC               PIC X.
           02     MNLIEUP               PIC X.
           02     MNLIEUH               PIC X.
           02     MNLIEUV               PIC X.
           02     MNLIEUO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECORIA              PIC X.
           02     MSECORIC              PIC X.
           02     MSECORIP              PIC X.
           02     MSECORIH              PIC X.
           02     MSECORIV              PIC X.
           02     MSECORIO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTYPNATA              PIC X.
           02     MTYPNATC              PIC X.
           02     MTYPNATP              PIC X.
           02     MTYPNATH              PIC X.
           02     MTYPNATV              PIC X.
           02     MTYPNATO              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIERSEA              PIC X.
           02     MTIERSEC              PIC X.
           02     MTIERSEP              PIC X.
           02     MTIERSEH              PIC X.
           02     MTIERSEV              PIC X.
           02     MTIERSEO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCCA                PIC X.
           02     MSOCCC                PIC X.
           02     MSOCCP                PIC X.
           02     MSOCCH                PIC X.
           02     MSOCCV                PIC X.
           02     MSOCCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSERVA                PIC X.
           02     MSERVC                PIC X.
           02     MSERVP                PIC X.
           02     MSERVH                PIC X.
           02     MSERVV                PIC X.
           02     MSERVO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCA               PIC X.
           02     MLSOCCC               PIC X.
           02     MLSOCCP               PIC X.
           02     MLSOCCH               PIC X.
           02     MLSOCCV               PIC X.
           02     MLSOCCO               PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMFACA              PIC X.
           02     MNUMFACC              PIC X.
           02     MNUMFACP              PIC X.
           02     MNUMFACH              PIC X.
           02     MNUMFACV              PIC X.
           02     MNUMFACO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDCREATA              PIC X.
           02     MDCREATC              PIC X.
           02     MDCREATP              PIC X.
           02     MDCREATH              PIC X.
           02     MDCREATV              PIC X.
           02     MDCREATO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCDESA              PIC X.
           02     MSOCDESC              PIC X.
           02     MSOCDESP              PIC X.
           02     MSOCDESH              PIC X.
           02     MSOCDESV              PIC X.
           02     MSOCDESO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIEUDEA              PIC X.
           02     MLIEUDEC              PIC X.
           02     MLIEUDEP              PIC X.
           02     MLIEUDEH              PIC X.
           02     MLIEUDEV              PIC X.
           02     MLIEUDEO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECDESA              PIC X.
           02     MSECDESC              PIC X.
           02     MSECDESP              PIC X.
           02     MSECDESH              PIC X.
           02     MSECDESV              PIC X.
           02     MSECDESO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBDESA              PIC X.
           02     MLIBDESC              PIC X.
           02     MLIBDESP              PIC X.
           02     MLIBDESH              PIC X.
           02     MLIBDESV              PIC X.
           02     MLIBDESO              PIC X(20).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLETTREA              PIC X.
           02     MLETTREC              PIC X.
           02     MLETTREP              PIC X.
           02     MLETTREH              PIC X.
           02     MLETTREV              PIC X.
           02     MLETTREO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIERSRA              PIC X.
           02     MTIERSRC              PIC X.
           02     MTIERSRP              PIC X.
           02     MTIERSRH              PIC X.
           02     MTIERSRV              PIC X.
           02     MTIERSRO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSERVDEA              PIC X.
           02     MSERVDEC              PIC X.
           02     MSERVDEP              PIC X.
           02     MSERVDEH              PIC X.
           02     MSERVDEV              PIC X.
           02     MSERVDEO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLETTRRA              PIC X.
           02     MLETTRRC              PIC X.
           02     MLETTRRP              PIC X.
           02     MLETTRRH              PIC X.
           02     MLETTRRV              PIC X.
           02     MLETTRRO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBVALA              PIC X.
           02     MLIBVALC              PIC X.
           02     MLIBVALP              PIC X.
           02     MLIBVALH              PIC X.
           02     MLIBVALV              PIC X.
           02     MLIBVALO              PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMORIA              PIC X.
           02     MNUMORIC              PIC X.
           02     MNUMORIP              PIC X.
           02     MNUMORIH              PIC X.
           02     MNUMORIV              PIC X.
           02     MNUMORIO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDCOMPTA              PIC X.
           02     MDCOMPTC              PIC X.
           02     MDCOMPTP              PIC X.
           02     MDCOMPTH              PIC X.
           02     MDCOMPTV              PIC X.
           02     MDCOMPTO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDECHEAA              PIC X.
           02     MDECHEAC              PIC X.
           02     MDECHEAP              PIC X.
           02     MDECHEAH              PIC X.
           02     MDECHEAV              PIC X.
           02     MDECHEAO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0001A              PIC X.
           02     DFH0001C              PIC X.
           02     DFH0001P              PIC X.
           02     DFH0001H              PIC X.
           02     DFH0001V              PIC X.
           02     DFH0001O              PIC X(9).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDORIGA               PIC X.
           02     MDORIGC               PIC X.
           02     MDORIGP               PIC X.
           02     MDORIGH               PIC X.
           02     MDORIGV               PIC X.
           02     MDORIGO               PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MEXERCIA              PIC X.
           02     MEXERCIC              PIC X.
           02     MEXERCIP              PIC X.
           02     MEXERCIH              PIC X.
           02     MEXERCIV              PIC X.
           02     MEXERCIO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNPERIOA              PIC X.
           02     MNPERIOC              PIC X.
           02     MNPERIOP              PIC X.
           02     MNPERIOH              PIC X.
           02     MNPERIOV              PIC X.
           02     MNPERIOO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLPERIOA              PIC X.
           02     MLPERIOC              PIC X.
           02     MLPERIOP              PIC X.
           02     MLPERIOH              PIC X.
           02     MLPERIOV              PIC X.
           02     MLPERIOO              PIC X(14).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDPASSAA              PIC X.
           02     MDPASSAC              PIC X.
           02     MDPASSAP              PIC X.
           02     MDPASSAH              PIC X.
           02     MDPASSAV              PIC X.
           02     MDPASSAO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBCRIA              PIC X.
           02     MLIBCRIC              PIC X.
           02     MLIBCRIP              PIC X.
           02     MLIBCRIH              PIC X.
           02     MLIBCRIV              PIC X.
           02     MLIBCRIO              PIC X(15).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCRITERA              PIC X.
           02     MCRITERC              PIC X.
           02     MCRITERP              PIC X.
           02     MCRITERH              PIC X.
           02     MCRITERV              PIC X.
           02     MCRITERO              PIC X(58).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MENTETEA              PIC X.
           02     MENTETEC              PIC X.
           02     MENTETEP              PIC X.
           02     MENTETEH              PIC X.
           02     MENTETEV              PIC X.
           02     MENTETEO              PIC X(31).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCOMMENA              PIC X.
           02     MCOMMENC              PIC X.
           02     MCOMMENP              PIC X.
           02     MCOMMENH              PIC X.
           02     MCOMMENV              PIC X.
           02     MCOMMENO              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0002A              PIC X.
           02     DFH0002C              PIC X.
           02     DFH0002P              PIC X.
           02     DFH0002H              PIC X.
           02     DFH0002V              PIC X.
           02     DFH0002O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0003A              PIC X.
           02     DFH0003C              PIC X.
           02     DFH0003P              PIC X.
           02     DFH0003H              PIC X.
           02     DFH0003V              PIC X.
           02     DFH0003O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0004A              PIC X.
           02     DFH0004C              PIC X.
           02     DFH0004P              PIC X.
           02     DFH0004H              PIC X.
           02     DFH0004V              PIC X.
           02     DFH0004O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0005A              PIC X.
           02     DFH0005C              PIC X.
           02     DFH0005P              PIC X.
           02     DFH0005H              PIC X.
           02     DFH0005V              PIC X.
           02     DFH0005O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0006A              PIC X.
           02     DFH0006C              PIC X.
           02     DFH0006P              PIC X.
           02     DFH0006H              PIC X.
           02     DFH0006V              PIC X.
           02     DFH0006O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0007A              PIC X.
           02     DFH0007C              PIC X.
           02     DFH0007P              PIC X.
           02     DFH0007H              PIC X.
           02     DFH0007V              PIC X.
           02     DFH0007O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     DFH0008A              PIC X.
           02     DFH0008C              PIC X.
           02     DFH0008P              PIC X.
           02     DFH0008H              PIC X.
           02     DFH0008V              PIC X.
           02     DFH0008O              PIC X(70).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDP              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDV              PIC X.
           02     MZONCMDO              PIC X(15).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(58).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(4).
