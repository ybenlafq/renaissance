      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG50   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG50I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MSOCCL                PIC S9(4) COMP-5.
           02     MSOCCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MSOCCI                PIC X(3).
           02     MLSOCCL               PIC S9(4) COMP-5.
           02     MLSOCCF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLSOCCI               PIC X(24).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNLIEUL               PIC S9(4) COMP-5.
           02     MNLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUI               PIC X(3).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(24).
           02     MCTYPEL               PIC S9(4) COMP-5.
           02     MCTYPEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MCTYPEI               PIC X(2).
           02     MLTYPEL               PIC S9(4) COMP-5.
           02     MLTYPEF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLTYPEI               PIC X(30).
           02     MNATUREL              PIC S9(4) COMP-5.
           02     MNATUREF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNATUREI              PIC X(5).
           02     MLNATURL              PIC S9(4) COMP-5.
           02     MLNATURF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLNATURI              PIC X(30).
           02     MNUMFACL              PIC S9(4) COMP-5.
           02     MNUMFACF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMFACI              PIC X(7).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(78).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(5).
       01  EFG50O REDEFINES EFG50I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSOCCA                PIC X.
           02     MSOCCC                PIC X.
           02     MSOCCP                PIC X.
           02     MSOCCH                PIC X.
           02     MSOCCV                PIC X.
           02     MSOCCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLSOCCA               PIC X.
           02     MLSOCCC               PIC X.
           02     MLSOCCP               PIC X.
           02     MLSOCCH               PIC X.
           02     MLSOCCV               PIC X.
           02     MLSOCCO               PIC X(24).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUA               PIC X.
           02     MNLIEUC               PIC X.
           02     MNLIEUP               PIC X.
           02     MNLIEUH               PIC X.
           02     MNLIEUV               PIC X.
           02     MNLIEUO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(24).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCTYPEA               PIC X.
           02     MCTYPEC               PIC X.
           02     MCTYPEP               PIC X.
           02     MCTYPEH               PIC X.
           02     MCTYPEV               PIC X.
           02     MCTYPEO               PIC X(2).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLTYPEA               PIC X.
           02     MLTYPEC               PIC X.
           02     MLTYPEP               PIC X.
           02     MLTYPEH               PIC X.
           02     MLTYPEV               PIC X.
           02     MLTYPEO               PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNATUREA              PIC X.
           02     MNATUREC              PIC X.
           02     MNATUREP              PIC X.
           02     MNATUREH              PIC X.
           02     MNATUREV              PIC X.
           02     MNATUREO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLNATURA              PIC X.
           02     MLNATURC              PIC X.
           02     MLNATURP              PIC X.
           02     MLNATURH              PIC X.
           02     MLNATURV              PIC X.
           02     MLNATURO              PIC X(30).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMFACA              PIC X.
           02     MNUMFACC              PIC X.
           02     MNUMFACP              PIC X.
           02     MNUMFACH              PIC X.
           02     MNUMFACV              PIC X.
           02     MNUMFACO              PIC X(7).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(78).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(5).
