      *
      * cicsmap ../renaissance/finance/ -- 
      * EFG20   DFHMSD MODE=INOUT,STORAGE=AUTO
      *
       01  EFG20I.
           02     DFHMS                 PIC X(12).
           02     MDATJOUL              PIC S9(4) COMP-5.
           02     MDATJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDATJOUI              PIC X(10).
           02     MTIMJOUL              PIC S9(4) COMP-5.
           02     MTIMJOUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MTIMJOUI              PIC X(5).
           02     MNSOCL                PIC S9(4) COMP-5.
           02     MNSOCF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MNSOCI                PIC X(3).
           02     MNLIEUL               PIC S9(4) COMP-5.
           02     MNLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MNLIEUI               PIC X(3).
           02     MLLIEUL               PIC S9(4) COMP-5.
           02     MLLIEUF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLLIEUI               PIC X(19).
           02     MSCOMPTL              PIC S9(4) COMP-5.
           02     MSCOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCOMPTI              PIC X(3).
           02     MECOMPTL              PIC S9(4) COMP-5.
           02     MECOMPTF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MECOMPTI              PIC X(3).
           02     MZONCMDL              PIC S9(4) COMP-5.
           02     MZONCMDF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONCMDI              PIC X(1).
           02     MLIBTTEL              PIC S9(4) COMP-5.
           02     MLIBTTEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBTTEI              PIC X(22).
           02     MZONTTEL              PIC S9(4) COMP-5.
           02     MZONTTEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MZONTTEI              PIC X(1).
           02     MSECTIOL              PIC S9(4) COMP-5.
           02     MSECTIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSECTIOI              PIC X(6).
           02     MCIMPL                PIC S9(4) COMP-5.
           02     MCIMPF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCIMPI                PIC X(4).
           02     MNFRAISL              PIC S9(4) COMP-5.
           02     MNFRAISF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNFRAISI              PIC X(6).
           02     MNEXERCL              PIC S9(4) COMP-5.
           02     MNEXERCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNEXERCI              PIC X(4).
           02     MNPERIOL              PIC S9(4) COMP-5.
           02     MNPERIOF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNPERIOI              PIC X(3).
           02     MLIB5L                PIC S9(4) COMP-5.
           02     MLIB5F                PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIB5I                PIC X(55).
           02     MNUMSOCL              PIC S9(4) COMP-5.
           02     MNUMSOCF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNUMSOCI              PIC X(3).
           02     MLIBLIEL              PIC S9(4) COMP-5.
           02     MLIBLIEF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBLIEI              PIC X(23).
           02     MSGESTL               PIC S9(4) COMP-5.
           02     MSGESTF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MSGESTI               PIC X(3).
           02     MLGESTL               PIC S9(4) COMP-5.
           02     MLGESTF               PIC X.
           02     DFHMS                 PIC X(4).
           02     MLGESTI               PIC X(3).
           02     MLIB6L                PIC S9(4) COMP-5.
           02     MLIB6F                PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIB6I                PIC X(60).
           02     MLIB7L                PIC S9(4) COMP-5.
           02     MLIB7F                PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIB7I                PIC X(55).
           02     MDVALEUL              PIC S9(4) COMP-5.
           02     MDVALEUF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MDVALEUI              PIC X(10).
           02     MLIBERRL              PIC S9(4) COMP-5.
           02     MLIBERRF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MLIBERRI              PIC X(78).
           02     MCODTRAL              PIC S9(4) COMP-5.
           02     MCODTRAF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MCODTRAI              PIC X(4).
           02     MCICSL                PIC S9(4) COMP-5.
           02     MCICSF                PIC X.
           02     DFHMS                 PIC X(4).
           02     MCICSI                PIC X(5).
           02     MNETNAML              PIC S9(4) COMP-5.
           02     MNETNAMF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MNETNAMI              PIC X(8).
           02     MSCREENL              PIC S9(4) COMP-5.
           02     MSCREENF              PIC X.
           02     DFHMS                 PIC X(4).
           02     MSCREENI              PIC X(5).
       01  EFG20O REDEFINES EFG20I.
           02     DFHMS                 PIC X(12).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDATJOUA              PIC X.
           02     MDATJOUC              PIC X.
           02     MDATJOUP              PIC X.
           02     MDATJOUH              PIC X.
           02     MDATJOUV              PIC X.
           02     MDATJOUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MTIMJOUA              PIC X.
           02     MTIMJOUC              PIC X.
           02     MTIMJOUP              PIC X.
           02     MTIMJOUH              PIC X.
           02     MTIMJOUV              PIC X.
           02     MTIMJOUO              PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNSOCA                PIC X.
           02     MNSOCC                PIC X.
           02     MNSOCP                PIC X.
           02     MNSOCH                PIC X.
           02     MNSOCV                PIC X.
           02     MNSOCO                PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNLIEUA               PIC X.
           02     MNLIEUC               PIC X.
           02     MNLIEUP               PIC X.
           02     MNLIEUH               PIC X.
           02     MNLIEUV               PIC X.
           02     MNLIEUO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLLIEUA               PIC X.
           02     MLLIEUC               PIC X.
           02     MLLIEUP               PIC X.
           02     MLLIEUH               PIC X.
           02     MLLIEUV               PIC X.
           02     MLLIEUO               PIC X(19).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCOMPTA              PIC X.
           02     MSCOMPTC              PIC X.
           02     MSCOMPTP              PIC X.
           02     MSCOMPTH              PIC X.
           02     MSCOMPTV              PIC X.
           02     MSCOMPTO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MECOMPTA              PIC X.
           02     MECOMPTC              PIC X.
           02     MECOMPTP              PIC X.
           02     MECOMPTH              PIC X.
           02     MECOMPTV              PIC X.
           02     MECOMPTO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONCMDA              PIC X.
           02     MZONCMDC              PIC X.
           02     MZONCMDP              PIC X.
           02     MZONCMDH              PIC X.
           02     MZONCMDV              PIC X.
           02     MZONCMDO              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBTTEA              PIC X.
           02     MLIBTTEC              PIC X.
           02     MLIBTTEP              PIC X.
           02     MLIBTTEH              PIC X.
           02     MLIBTTEV              PIC X.
           02     MLIBTTEO              PIC X(22).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MZONTTEA              PIC X.
           02     MZONTTEC              PIC X.
           02     MZONTTEP              PIC X.
           02     MZONTTEH              PIC X.
           02     MZONTTEV              PIC X.
           02     MZONTTEO              PIC X(1).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSECTIOA              PIC X.
           02     MSECTIOC              PIC X.
           02     MSECTIOP              PIC X.
           02     MSECTIOH              PIC X.
           02     MSECTIOV              PIC X.
           02     MSECTIOO              PIC X(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCIMPA                PIC X.
           02     MCIMPC                PIC X.
           02     MCIMPP                PIC X.
           02     MCIMPH                PIC X.
           02     MCIMPV                PIC X.
           02     MCIMPO                PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNFRAISA              PIC X.
           02     MNFRAISC              PIC X.
           02     MNFRAISP              PIC X.
           02     MNFRAISH              PIC X.
           02     MNFRAISV              PIC X.
           02     MNFRAISO              PIC 9(6).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNEXERCA              PIC X.
           02     MNEXERCC              PIC X.
           02     MNEXERCP              PIC X.
           02     MNEXERCH              PIC X.
           02     MNEXERCV              PIC X.
           02     MNEXERCO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNPERIOA              PIC X.
           02     MNPERIOC              PIC X.
           02     MNPERIOP              PIC X.
           02     MNPERIOH              PIC X.
           02     MNPERIOV              PIC X.
           02     MNPERIOO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIB5A                PIC X.
           02     MLIB5C                PIC X.
           02     MLIB5P                PIC X.
           02     MLIB5H                PIC X.
           02     MLIB5V                PIC X.
           02     MLIB5O                PIC X(55).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNUMSOCA              PIC X.
           02     MNUMSOCC              PIC X.
           02     MNUMSOCP              PIC X.
           02     MNUMSOCH              PIC X.
           02     MNUMSOCV              PIC X.
           02     MNUMSOCO              PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBLIEA              PIC X.
           02     MLIBLIEC              PIC X.
           02     MLIBLIEP              PIC X.
           02     MLIBLIEH              PIC X.
           02     MLIBLIEV              PIC X.
           02     MLIBLIEO              PIC X(23).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSGESTA               PIC X.
           02     MSGESTC               PIC X.
           02     MSGESTP               PIC X.
           02     MSGESTH               PIC X.
           02     MSGESTV               PIC X.
           02     MSGESTO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLGESTA               PIC X.
           02     MLGESTC               PIC X.
           02     MLGESTP               PIC X.
           02     MLGESTH               PIC X.
           02     MLGESTV               PIC X.
           02     MLGESTO               PIC X(3).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIB6A                PIC X.
           02     MLIB6C                PIC X.
           02     MLIB6P                PIC X.
           02     MLIB6H                PIC X.
           02     MLIB6V                PIC X.
           02     MLIB6O                PIC X(60).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIB7A                PIC X.
           02     MLIB7C                PIC X.
           02     MLIB7P                PIC X.
           02     MLIB7H                PIC X.
           02     MLIB7V                PIC X.
           02     MLIB7O                PIC X(55).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MDVALEUA              PIC X.
           02     MDVALEUC              PIC X.
           02     MDVALEUP              PIC X.
           02     MDVALEUH              PIC X.
           02     MDVALEUV              PIC X.
           02     MDVALEUO              PIC X(10).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MLIBERRA              PIC X.
           02     MLIBERRC              PIC X.
           02     MLIBERRP              PIC X.
           02     MLIBERRH              PIC X.
           02     MLIBERRV              PIC X.
           02     MLIBERRO              PIC X(78).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCODTRAA              PIC X.
           02     MCODTRAC              PIC X.
           02     MCODTRAP              PIC X.
           02     MCODTRAH              PIC X.
           02     MCODTRAV              PIC X.
           02     MCODTRAO              PIC X(4).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MCICSA                PIC X.
           02     MCICSC                PIC X.
           02     MCICSP                PIC X.
           02     MCICSH                PIC X.
           02     MCICSV                PIC X.
           02     MCICSO                PIC X(5).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MNETNAMA              PIC X.
           02     MNETNAMC              PIC X.
           02     MNETNAMP              PIC X.
           02     MNETNAMH              PIC X.
           02     MNETNAMV              PIC X.
           02     MNETNAMO              PIC X(8).
           02     DFHMS                 PIC S9(4) COMP-5.
           02     MSCREENA              PIC X.
           02     MSCREENC              PIC X.
           02     MSCREENP              PIC X.
           02     MSCREENH              PIC X.
           02     MSCREENV              PIC X.
           02     MSCREENO              PIC X(5).
