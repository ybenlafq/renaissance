.PHONY: all clean target_TFG51_pco clean_TFG51_pco target_TFG50_pco clean_TFG50_pco target_TFG29_pco clean_TFG29_pco target_TFG28_pco clean_TFG28_pco target_TFG27_pco clean_TFG27_pco target_TFG25_pco clean_TFG25_pco target_TFG24_pco clean_TFG24_pco target_TFG23_pco clean_TFG23_pco target_TFG22_pco clean_TFG22_pco target_TFG21_cbl clean_TFG21_cbl target_TFG20_pco clean_TFG20_pco target_TFG14_pco clean_TFG14_pco target_TFG13_pco clean_TFG13_pco target_TFG12_pco clean_TFG12_pco target_TFG11_pco clean_TFG11_pco target_TFG10_pco clean_TFG10_pco target_MFG30_pco clean_MFG30_pco target_MFFNUM_cbl clean_MFFNUM_cbl target__settings clean__settings 

all: target_TFG51_pco target_TFG50_pco target_TFG29_pco target_TFG28_pco target_TFG27_pco target_TFG25_pco target_TFG24_pco target_TFG23_pco target_TFG22_pco target_TFG21_cbl target_TFG20_pco target_TFG14_pco target_TFG13_pco target_TFG12_pco target_TFG11_pco target_TFG10_pco target_MFG30_pco target_MFFNUM_cbl 

target_TFG51_pco: ../../../bin/TFG51.so

-include .cobolit.tmp.10.224.78.178/TFG51-393d9a049984a9819bd72035c94555118239339e.d

../../../bin/TFG51.so: TFG51.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG51.pco'
	cobc TFG51.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG51.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG51.so" -MT "../../../bin/TFG51.so" -MF .cobolit.tmp.10.224.78.178/TFG51-393d9a049984a9819bd72035c94555118239339e.d

clean_TFG51_pco: 
	rm -f "../../../bin/TFG51.so" .cobolit.tmp.10.224.78.178/TFG51-393d9a049984a9819bd72035c94555118239339e.d

target_TFG50_pco: ../../../bin/TFG50.so

-include .cobolit.tmp.10.224.78.178/TFG50-6ed7f0829b5428409dab4ddf9e5fa740e26b2495.d

../../../bin/TFG50.so: TFG50.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG50.pco'
	cobc TFG50.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG50.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG50.so" -MT "../../../bin/TFG50.so" -MF .cobolit.tmp.10.224.78.178/TFG50-6ed7f0829b5428409dab4ddf9e5fa740e26b2495.d

clean_TFG50_pco: 
	rm -f "../../../bin/TFG50.so" .cobolit.tmp.10.224.78.178/TFG50-6ed7f0829b5428409dab4ddf9e5fa740e26b2495.d

target_TFG29_pco: ../../../bin/TFG29.so

-include .cobolit.tmp.10.224.78.178/TFG29-00194397faba90c0697d279ccbabef3b6f9a1d22.d

../../../bin/TFG29.so: TFG29.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG29.pco'
	cobc TFG29.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG29.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG29.so" -MT "../../../bin/TFG29.so" -MF .cobolit.tmp.10.224.78.178/TFG29-00194397faba90c0697d279ccbabef3b6f9a1d22.d

clean_TFG29_pco: 
	rm -f "../../../bin/TFG29.so" .cobolit.tmp.10.224.78.178/TFG29-00194397faba90c0697d279ccbabef3b6f9a1d22.d

target_TFG28_pco: ../../../bin/TFG28.so

-include .cobolit.tmp.10.224.78.178/TFG28-cb90e38fd67a0f1d322b723ff59af213997c7f0f.d

../../../bin/TFG28.so: TFG28.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG28.pco'
	cobc TFG28.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG28.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG28.so" -MT "../../../bin/TFG28.so" -MF .cobolit.tmp.10.224.78.178/TFG28-cb90e38fd67a0f1d322b723ff59af213997c7f0f.d

clean_TFG28_pco: 
	rm -f "../../../bin/TFG28.so" .cobolit.tmp.10.224.78.178/TFG28-cb90e38fd67a0f1d322b723ff59af213997c7f0f.d

target_TFG27_pco: ../../../bin/TFG27.so

-include .cobolit.tmp.10.224.78.178/TFG27-0950a01f68b799a38dcb40ba49097bbc7503c00f.d

../../../bin/TFG27.so: TFG27.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG27.pco'
	cobc TFG27.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG27.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG27.so" -MT "../../../bin/TFG27.so" -MF .cobolit.tmp.10.224.78.178/TFG27-0950a01f68b799a38dcb40ba49097bbc7503c00f.d

clean_TFG27_pco: 
	rm -f "../../../bin/TFG27.so" .cobolit.tmp.10.224.78.178/TFG27-0950a01f68b799a38dcb40ba49097bbc7503c00f.d

target_TFG25_pco: ../../../bin/TFG25.so

-include .cobolit.tmp.10.224.78.178/TFG25-007c35cf344b411b806815674b169db600ce9a07.d

../../../bin/TFG25.so: TFG25.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG25.pco'
	cobc TFG25.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG25.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG25.so" -MT "../../../bin/TFG25.so" -MF .cobolit.tmp.10.224.78.178/TFG25-007c35cf344b411b806815674b169db600ce9a07.d

clean_TFG25_pco: 
	rm -f "../../../bin/TFG25.so" .cobolit.tmp.10.224.78.178/TFG25-007c35cf344b411b806815674b169db600ce9a07.d

target_TFG24_pco: ../../../bin/TFG24.so

-include .cobolit.tmp.10.224.78.178/TFG24-dcecfd1b7099feb75b9e773b93f254c2849a9ac2.d

../../../bin/TFG24.so: TFG24.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG24.pco'
	cobc TFG24.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG24.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG24.so" -MT "../../../bin/TFG24.so" -MF .cobolit.tmp.10.224.78.178/TFG24-dcecfd1b7099feb75b9e773b93f254c2849a9ac2.d

clean_TFG24_pco: 
	rm -f "../../../bin/TFG24.so" .cobolit.tmp.10.224.78.178/TFG24-dcecfd1b7099feb75b9e773b93f254c2849a9ac2.d

target_TFG23_pco: ../../../bin/TFG23.so

-include .cobolit.tmp.10.224.78.178/TFG23-ae996a6e6aeb21bf4251c31250b993f89c5da589.d

../../../bin/TFG23.so: TFG23.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG23.pco'
	cobc TFG23.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG23.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG23.so" -MT "../../../bin/TFG23.so" -MF .cobolit.tmp.10.224.78.178/TFG23-ae996a6e6aeb21bf4251c31250b993f89c5da589.d

clean_TFG23_pco: 
	rm -f "../../../bin/TFG23.so" .cobolit.tmp.10.224.78.178/TFG23-ae996a6e6aeb21bf4251c31250b993f89c5da589.d

target_TFG22_pco: ../../../bin/TFG22.so

-include .cobolit.tmp.10.224.78.178/TFG22-bfb4f5ffc6ab203db93f4f2755acb3a604e68853.d

../../../bin/TFG22.so: TFG22.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG22.pco'
	cobc TFG22.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG22.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG22.so" -MT "../../../bin/TFG22.so" -MF .cobolit.tmp.10.224.78.178/TFG22-bfb4f5ffc6ab203db93f4f2755acb3a604e68853.d

clean_TFG22_pco: 
	rm -f "../../../bin/TFG22.so" .cobolit.tmp.10.224.78.178/TFG22-bfb4f5ffc6ab203db93f4f2755acb3a604e68853.d

target_TFG21_cbl: ../../../bin/TFG21.so

-include .cobolit.tmp.10.224.78.178/TFG21-b340b614dd7d31ff989fc54097f09b45c9045865.d

../../../bin/TFG21.so: TFG21.cbl
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG21.cbl'
	cobc TFG21.cbl -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG21.so" -MT "../../../bin/TFG21.so" -MF .cobolit.tmp.10.224.78.178/TFG21-b340b614dd7d31ff989fc54097f09b45c9045865.d

clean_TFG21_cbl: 
	rm -f "../../../bin/TFG21.so" .cobolit.tmp.10.224.78.178/TFG21-b340b614dd7d31ff989fc54097f09b45c9045865.d

target_TFG20_pco: ../../../bin/TFG20.so

-include .cobolit.tmp.10.224.78.178/TFG20-0215012303a8086f44ed2cde000c366575e102e9.d

../../../bin/TFG20.so: TFG20.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG20.pco'
	cobc TFG20.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG20.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG20.so" -MT "../../../bin/TFG20.so" -MF .cobolit.tmp.10.224.78.178/TFG20-0215012303a8086f44ed2cde000c366575e102e9.d

clean_TFG20_pco: 
	rm -f "../../../bin/TFG20.so" .cobolit.tmp.10.224.78.178/TFG20-0215012303a8086f44ed2cde000c366575e102e9.d

target_TFG14_pco: ../../../bin/TFG14.so

-include .cobolit.tmp.10.224.78.178/TFG14-f490d0ee279e571c80514f87e1a6b720991e9369.d

../../../bin/TFG14.so: TFG14.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG14.pco'
	cobc TFG14.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG14.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG14.so" -MT "../../../bin/TFG14.so" -MF .cobolit.tmp.10.224.78.178/TFG14-f490d0ee279e571c80514f87e1a6b720991e9369.d

clean_TFG14_pco: 
	rm -f "../../../bin/TFG14.so" .cobolit.tmp.10.224.78.178/TFG14-f490d0ee279e571c80514f87e1a6b720991e9369.d

target_TFG13_pco: ../../../bin/TFG13.so

-include .cobolit.tmp.10.224.78.178/TFG13-2a802b5f4a9656cbb412a94550e4c179033ac271.d

../../../bin/TFG13.so: TFG13.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG13.pco'
	cobc TFG13.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG13.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG13.so" -MT "../../../bin/TFG13.so" -MF .cobolit.tmp.10.224.78.178/TFG13-2a802b5f4a9656cbb412a94550e4c179033ac271.d

clean_TFG13_pco: 
	rm -f "../../../bin/TFG13.so" .cobolit.tmp.10.224.78.178/TFG13-2a802b5f4a9656cbb412a94550e4c179033ac271.d

target_TFG12_pco: ../../../bin/TFG12.so

-include .cobolit.tmp.10.224.78.178/TFG12-5a68788ee8cea21ccaa113d0e8ffeb3ee58a8e69.d

../../../bin/TFG12.so: TFG12.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG12.pco'
	cobc TFG12.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG12.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG12.so" -MT "../../../bin/TFG12.so" -MF .cobolit.tmp.10.224.78.178/TFG12-5a68788ee8cea21ccaa113d0e8ffeb3ee58a8e69.d

clean_TFG12_pco: 
	rm -f "../../../bin/TFG12.so" .cobolit.tmp.10.224.78.178/TFG12-5a68788ee8cea21ccaa113d0e8ffeb3ee58a8e69.d

target_TFG11_pco: ../../../bin/TFG11.so

-include .cobolit.tmp.10.224.78.178/TFG11-9f5fe613942ef6f9015ee397a9bed4cc727a5a58.d

../../../bin/TFG11.so: TFG11.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG11.pco'
	cobc TFG11.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG11.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG11.so" -MT "../../../bin/TFG11.so" -MF .cobolit.tmp.10.224.78.178/TFG11-9f5fe613942ef6f9015ee397a9bed4cc727a5a58.d

clean_TFG11_pco: 
	rm -f "../../../bin/TFG11.so" .cobolit.tmp.10.224.78.178/TFG11-9f5fe613942ef6f9015ee397a9bed4cc727a5a58.d

target_TFG10_pco: ../../../bin/TFG10.so

-include .cobolit.tmp.10.224.78.178/TFG10-ef37e092529a0d957d8fda9fdc06236132e38b47.d

../../../bin/TFG10.so: TFG10.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/TFG10.pco'
	cobc TFG10.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh TFG10.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/TFG10.so" -MT "../../../bin/TFG10.so" -MF .cobolit.tmp.10.224.78.178/TFG10-ef37e092529a0d957d8fda9fdc06236132e38b47.d

clean_TFG10_pco: 
	rm -f "../../../bin/TFG10.so" .cobolit.tmp.10.224.78.178/TFG10-ef37e092529a0d957d8fda9fdc06236132e38b47.d

target_MFG30_pco: ../../../bin/MFG30.so

-include .cobolit.tmp.10.224.78.178/MFG30-90818ce235944ff91d3eeb00c073a2c58d11b534.d

../../../bin/MFG30.so: MFG30.pco
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/MFG30.pco'
	cobc MFG30.pco -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -preprocess="../../TOOLS/citproall.sh MFG30.pco" -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/MFG30.so" -MT "../../../bin/MFG30.so" -MF .cobolit.tmp.10.224.78.178/MFG30-90818ce235944ff91d3eeb00c073a2c58d11b534.d

clean_MFG30_pco: 
	rm -f "../../../bin/MFG30.so" .cobolit.tmp.10.224.78.178/MFG30-90818ce235944ff91d3eeb00c073a2c58d11b534.d

target_MFFNUM_cbl: ../../../bin/MFFNUM.so

-include .cobolit.tmp.10.224.78.178/MFFNUM-7270f9344f49686aa2da83fd00e7121450a529ad.d

../../../bin/MFFNUM.so: MFFNUM.cbl
	@echo 'building /app/decoz01/workspace_new/renaissance/finance/CICS/MFFNUM.cbl'
	cobc MFFNUM.cbl -I "/opt/ibm/cics/include" -I "/opt/ibm/db2/V10.5/include/cobol_mf" -I "../../achats/COPY/" -I "../../autres/COPY/" -I "../../finance/COPY/" -I "../../logistique/COPY/" -I "../../service/COPY/" -I "../../technique/COPY/" -I "../../technique/COPY/DESC/" -I "../../typologie/COPY/" -I "../../ventes/COPY/" -g -fixed -fsign-ebcdic -conf="../../TOOLS/cit.cfg" -m -L "/opt/cobol-it/lib" -L "/opt/SyncSort/DMExpress/dmexpress/lib" -L "/opt/ibm/db2/V10.5/lib32" -L "/opt/ibm/cics/lib" -o "../../../bin/MFFNUM.so" -MT "../../../bin/MFFNUM.so" -MF .cobolit.tmp.10.224.78.178/MFFNUM-7270f9344f49686aa2da83fd00e7121450a529ad.d

clean_MFFNUM_cbl: 
	rm -f "../../../bin/MFFNUM.so" .cobolit.tmp.10.224.78.178/MFFNUM-7270f9344f49686aa2da83fd00e7121450a529ad.d



clean: clean_TFG51_pco clean_TFG50_pco clean_TFG29_pco clean_TFG28_pco clean_TFG27_pco clean_TFG25_pco clean_TFG24_pco clean_TFG23_pco clean_TFG22_pco clean_TFG21_cbl clean_TFG20_pco clean_TFG14_pco clean_TFG13_pco clean_TFG12_pco clean_TFG11_pco clean_TFG10_pco clean_MFG30_pco clean_MFFNUM_cbl 

target__settings: 


clean__settings: 

