#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FV001P.ksh                       --- VERSION DU 08/10/2016 13:20
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFV001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/02/10 AT 10.43.19 BY PREPA2                       
#    STANDARDS: P  JOBSET: FV001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ECS                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FV001PA
       ;;
(FV001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FV001PAA
       ;;
(FV001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FG14FPC
# ******* FIC DE RECYCLAGE                                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FRECYCLP
# ******* FIC DE REPRISE EN CAS DE PLANTAGE DE LA VEILLE)                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PFV0/F07.BFV001AP.REPRISE
# ******* FIC DES INTERFACES CUMULES TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PNCGP/F07.BFTV01AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_98_25 98 CH 25
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FV001PAB
       ;;
(FV001PAB)
       m_CondExec 00,EQ,FV001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BFTV01 **  COBOL2/DB2                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FV001PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FV001PAD
       ;;
(FV001PAD)
       m_CondExec ${EXAAF},NE,YES 
# ******* FIC DES INTERFACES CUMULES TRIES                                     
       m_FileAssign -d SHR -g ${G_A1} FFTV01 ${DATA}/PNCGP/F07.BFTV01AP
# ******* TABLE  GA                                                            
#    RSGA01   : NAME=RSGA01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE FT                                                             
#    RSFT01   : NAME=RSFT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT01 /dev/null
#    RSFT10   : NAME=RSFT10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT10 /dev/null
#    RSFT12   : NAME=RSFT12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT12 /dev/null
#    RSFT13   : NAME=RSFT13,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT13 /dev/null
#    RSFT17   : NAME=RSFT17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT17 /dev/null
#    RSFT23   : NAME=RSFT23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT23 /dev/null
#    RSFT25   : NAME=RSFT25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT25 /dev/null
#    RSFT21   : NAME=RSFT21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT21 /dev/null
#    RSFT22   : NAME=RSFT22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT22 /dev/null
#    RSFT31   : NAME=RSFT31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT31 /dev/null
#    RSFT34   : NAME=RSFT34,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT34 /dev/null
#    RSFT43   : NAME=RSFT43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT43 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *                                                                            
# ******* JOURNAL(AUX) A RECYCLER EN FICHIER                                   
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RECYCLP
# *                                                                            
# **************W*A*R*N*I*N*G*****************************************         
# *C EST ICI QUE L ON INDIQUE SI LE TRAITEMANT EST EN SIMUL OU NON             
# *SI SIMUL METTRE SIMU . SI REEL METTRE REEL                                  
# **************W*A*R*N*I*N*G*****************************************         
# *                                                                            
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FV001PAD
# ******* FIC DE RECYCLAGES DES JOURNAUX PARAMèTRéS DANS FPARAM                
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FRECYC ${DATA}/PXX0/F07.FRECYCLP
# ******* ETAT D'ANOMALIE DES INTERFACES                                       
       m_OutputAssign -c 9 -w IFTV01 IFTV01
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTV01 
       JUMP_LABEL=FV001PAE
       ;;
(FV001PAE)
       m_CondExec 04,GE,FV001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
