#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SIGC2Y.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYSIGC2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/08/20 AT 11.59.23 BY PREPA2                       
#    STANDARDS: P  JOBSET: SIGC2Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# ********************************************************************         
#  PGM : BSIG20                                                                
#  ------------                                                                
#  REPRISE DU FICHIER SXGC2Y ISSU D'UNE EXPLO HRV3 (UNIX) AFIN DE              
#  PACKER LES ZONES DECIMALES                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SIGC2YA
       ;;
(SIGC2YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SIGC2YAA
       ;;
(SIGC2YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 FEXPLO ${DATA}/PXX0/F45.SXGC2Y
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F822.SXGC2Y
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSIG20 ${DATA}/PTEM/SIGC2YAA.BSIG20CY
       m_ProgramExec BSIG20 
# ********************************************************************         
#  TRI DU FICHIER ISSU DU PGM BSIG20                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAD
       ;;
(SIGC2YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SIGC2YAA.BSIG20CY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/SIGC2YAD.BSIG20DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_25 16 CH 25
 /FIELDS FLD_CH_59_2 59 CH 2
 /FIELDS FLD_PD_50_9 50 PD 9
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_PD_41_9 41 PD 9
 /FIELDS FLD_CH_61_7 61 CH 7
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_59_2 ASCENDING,
   FLD_CH_16_25 ASCENDING,
   FLD_CH_61_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_9,
    TOTAL FLD_PD_50_9
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGC2YAE
       ;;
(SIGC2YAE)
       m_CondExec 00,EQ,SIGC2YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG25                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER PREVISION POUR FDC                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAG
       ;;
(SIGC2YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d SHR FTYPPAIE ${DATA}/CORTEX4.P.MTXTFIX1/PAYPROY
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A2} FSIG20 ${DATA}/PTEM/SIGC2YAD.BSIG20DY
# ------  FICHIER POUR CONTROLE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FPARAM ${DATA}/PTEM/SIGC2YAG.BSIG25AY
# ------  FICHIER REPRIS DANS LE BSIG26                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FCTL ${DATA}/PTEM/SIGC2YAG.BSIG25BY
# ------  FICHIER POUR GCT (PAS D'INJECTION EN COMPTA)                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FGCT ${DATA}/PNCGY/F45.SIGGC2Y
# ------  FICHIER POUR FDC (PREVISION)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 FFDC ${DATA}/PNCGY/F45.SIGAFDC
# ------  FICHIER POUR FDC                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 61 -g +1 FREMUN ${DATA}/PXX0/F45.BSIG25GY
# ------  ETATS DE COMPTE RENDUS                                               
       m_OutputAssign -c 9 -w ISG010 FANOSYNT
       m_OutputAssign -c 9 -w ISG020 FANODET
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG25 
       JUMP_LABEL=SIGC2YAH
       ;;
(SIGC2YAH)
       m_CondExec 04,GE,SIGC2YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25  (CONTROLE ANALYTIQUE)               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAJ
       ;;
(SIGC2YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SIGC2YAG.BSIG25BY
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 SORTOUT ${DATA}/PTEM/SIGC2YAJ.BSIG26DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_51 1 CH 51
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_33 1 CH 33
 /KEYS
   FLD_CH_1_33 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2YAK
       ;;
(SIGC2YAK)
       m_CondExec 00,EQ,SIGC2YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE GENERAL)                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAM
       ;;
(SIGC2YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/SIGC2YAG.BSIG25BY
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 SORTOUT ${DATA}/PTEM/SIGC2YAM.BSIG26EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_1_21 1 CH 21
 /FIELDS FLD_CH_34_9 34 CH 9
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_52_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_52_8,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2YAN
       ;;
(SIGC2YAN)
       m_CondExec 00,EQ,SIGC2YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE RUBRIQUE)                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAQ
       ;;
(SIGC2YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/SIGC2YAG.BSIG25BY
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -g +1 SORTOUT ${DATA}/PTEM/SIGC2YAQ.BSIG26FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_21 1 CH 21
 /FIELDS FLD_CH_28_6 28 CH 6
 /FIELDS FLD_CH_34_9 34 CH 9
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_28_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_28_6,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2YAR
       ;;
(SIGC2YAR)
       m_CondExec 00,EQ,SIGC2YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG26                                                                
#  ------------                                                                
#  EDITION DES ETATS DE CONTROLE                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAT
       ;;
(SIGC2YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DES TRIS PRECEDANT                                      
       m_FileAssign -d SHR -g ${G_A6} FCTLANA ${DATA}/PTEM/SIGC2YAJ.BSIG26DY
       m_FileAssign -d SHR -g ${G_A7} FCTLGEN ${DATA}/PTEM/SIGC2YAM.BSIG26EY
       m_FileAssign -d SHR -g ${G_A8} FCTLRUB ${DATA}/PTEM/SIGC2YAQ.BSIG26FY
# ------  FICHIER DE CONTROLE SI OK OU ANO                                     
       m_FileAssign -d SHR -g ${G_A9} FPARAM ${DATA}/PTEM/SIGC2YAG.BSIG25AY
# ------  ETATS DE CONTROLE                                                    
       m_OutputAssign -c 9 -w ISG025 FGENE
       m_OutputAssign -c 9 -w ISG026 FANA
       m_OutputAssign -c 9 -w ISG027 FRUB
# ******************************************************************           
#  REMISE A ZERO DU FICHIER PROVENANT D'UNIX SXGC2Y / SX822YS                  
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG26 
       JUMP_LABEL=SIGC2YAU
       ;;
(SIGC2YAU)
       m_CondExec 04,GE,SIGC2YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SIGC2YAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YAX
       ;;
(SIGC2YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT1 ${DATA}/PXX0/F45.SXGC2Y
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT2 ${DATA}/PXX0/F822.SXGC2Y
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2YAX.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SIGC2YAY
       ;;
(SIGC2YAY)
       m_CondExec 16,NE,SIGC2YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2YZA
       ;;
(SIGC2YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
