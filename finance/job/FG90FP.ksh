#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG90FP.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG90F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/18 AT 17.23.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG90FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *************************************                                        
# *  DELETE DU FICHIER CUMUL DU MOIS                                           
# *************************************                                        
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG90FPA
       ;;
(FG90FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG90FPAA
       ;;
(FG90FPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG90FPAB
       ;;
(FG90FPAB)
       m_CondExec 16,NE,FG90FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG900                                                                
#  ------------                                                                
#  PURGE DES TABLES DU NETTING ON GARDE 3 MOIS EN LIGNE                        
#  CREATION DE 3 FICHIERS REPRIS DANS LE BFG950                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAD
       ;;
(FG90FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RTGA01 /dev/null
# ------- TABLES EN M.A.J                                                      
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG05 /dev/null
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG06 /dev/null
#    RSFG07   : NAME=RSFG07,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG07 /dev/null
# ------- PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------- FICHIERS REPRIS DANS LE BFG950                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 239 -t LSEQ -g +1 FFG01P ${DATA}/PNCGP/F07.BFG901AP
       m_FileAssign -d NEW,CATLG,DELETE -r 118 -t LSEQ -g +1 FFG06P ${DATA}/PNCGP/F07.BFG906AP
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FFG07P ${DATA}/PNCGP/F07.BFG907AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG900 
       JUMP_LABEL=FG90FPAE
       ;;
(FG90FPAE)
       m_CondExec 04,GE,FG90FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG901AP POUR LE PGM BFG950                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAG
       ;;
(FG90FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PNCGP/F07.BFG901AP
       m_FileAssign -d NEW,CATLG,DELETE -r 239 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG90FPAG.BFG901BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
 /* Record Type = F  Record Length = 239 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG90FPAH
       ;;
(FG90FPAH)
       m_CondExec 00,EQ,FG90FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG906AP POUR LE PGM BFG950                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAJ
       ;;
(FG90FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PNCGP/F07.BFG906AP
       m_FileAssign -d NEW,CATLG,DELETE -r 118 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG90FPAJ.BFG906BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = 118 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG90FPAK
       ;;
(FG90FPAK)
       m_CondExec 00,EQ,FG90FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG901AP POUR LE PGM BFG950                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAM
       ;;
(FG90FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PNCGP/F07.BFG907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG90FPAM.BFG907BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
 /* Record Type = F  Record Length = 81 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG90FPAN
       ;;
(FG90FPAN)
       m_CondExec 00,EQ,FG90FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG950                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER POUR MICROFICHE ORSID                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAQ
       ;;
(FG90FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RTGA10 /dev/null
# ------- FICHIERS ISSU DU BFG900                                              
       m_FileAssign -d SHR -g ${G_A4} FFG01P ${DATA}/PTEM/FG90FPAG.BFG901BP
       m_FileAssign -d SHR -g ${G_A5} FFG06P ${DATA}/PTEM/FG90FPAJ.BFG906BP
       m_FileAssign -d SHR -g ${G_A6} FFG07P ${DATA}/PTEM/FG90FPAM.BFG907BP
# ------- FICHIER POUR MICROFICHE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 179 -t LSEQ -g +1 FFGFACT ${DATA}/PTEM/FG90FPAQ.BFG950AP
# ------- PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG950 
       JUMP_LABEL=FG90FPAR
       ;;
(FG90FPAR)
       m_CondExec 04,GE,FG90FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG950AP POUR MICROFICHE ORSID                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAT
       ;;
(FG90FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/FG90FPAQ.BFG950AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.F07.BFG950BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_5 1 PD 5
 /FIELDS FLD_CH_47_133 47 CH 133
 /FIELDS FLD_CH_14_33 14 CH 33
 /KEYS
   FLD_CH_14_33 ASCENDING,
   FLD_PD_1_5 ASCENDING
 /* Record Type = F  Record Length = 133 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_47_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG90FPAU
       ;;
(FG90FPAU)
       m_CondExec 00,EQ,FG90FPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHERS                                                             
# ********************************************************************         
# ABJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":FGFACP"")                                                        
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":BFG950BP"",FGFACP.TXT)                                        
#  ":BFG950BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPAX PGM=JVMLDM76   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPAX
       ;;
(FG90FPAX)
       m_CondExec ${EXABJ},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ FICZIP ${DATA}/PXX0.FICHEML.FG90FP.FGFACP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPAX.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CDROMZIP,                                                           
#      FNAME=":FGFACP"",                                                       
#      NFNAME=PURNETT.ZIP                                                      
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT PAR CFT BL MICROLIST                                              
# ********************************************************************         
# ABT      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=FG90FP                                        
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFG90FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPBA
       ;;
(FG90FPBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPBA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** PREPARATION DU SEND FTP DU BL MICROFICHE          ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP FG90FPBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPBD
       ;;
(FG90FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FG90FPBD.FTFG90FP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU BORDEREAU DE LAISON PURNETT_DU                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG90FPBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPBG
       ;;
(FG90FPBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FG90FPBD.FTFG90FP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG90FPZA
       ;;
(FG90FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG90FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
