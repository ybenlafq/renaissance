#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFM01L.ksh                       --- VERSION DU 08/10/2016 17:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGFM01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/01 AT 10.21.51 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GFM01L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#     LOAD DES TABLESPACE FM                                                   
#  QUIESCE DES TABLES RTFM01-02-03-04-05-06-23-64-69-73                        
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFM01LA
       ;;
(GFM01LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFM01LAA
       ;;
(GFM01LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGFM01L
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GFM01LAA
       m_ProgramExec IEFBR14 "RDAR,GFM01L.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAD
       ;;
(GFM01LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGFM01L
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAE
       ;;
(GFM01LAE)
       m_CondExec 00,EQ,GFM01LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM01 A PARTIR DU TS RSFM01G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAG
       ;;
(GFM01LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM01   : NAME=RSFM01L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM01 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM01G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAG_RTFM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAH
       ;;
(GFM01LAH)
       m_CondExec 04,GE,GFM01LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM02 A PARTIR DU TS RSFM02G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAJ
       ;;
(GFM01LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM02   : NAME=RSFM02L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM02 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM02G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAJ_RTFM02.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAK
       ;;
(GFM01LAK)
       m_CondExec 04,GE,GFM01LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM03 A PARTIR DU TS RSFM03G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAM
       ;;
(GFM01LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM03   : NAME=RSFM03L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM03 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM03G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAM_RTFM03.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAN
       ;;
(GFM01LAN)
       m_CondExec 04,GE,GFM01LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM04 A PARTIR DU TS RSFM04G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAQ
       ;;
(GFM01LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM04   : NAME=RSFM04L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM04 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM04G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAQ_RTFM04.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAR
       ;;
(GFM01LAR)
       m_CondExec 04,GE,GFM01LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM05 A PARTIR DU TS RSFM05G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAT
       ;;
(GFM01LAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM05   : NAME=RSFM05L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM05G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAT_RTFM05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAU
       ;;
(GFM01LAU)
       m_CondExec 04,GE,GFM01LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM06 A PARTIR DU TS RSFM06G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LAX
       ;;
(GFM01LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM06   : NAME=RSFM06L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM06 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM06G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LAX_RTFM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LAY
       ;;
(GFM01LAY)
       m_CondExec 04,GE,GFM01LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM23 A PARTIR DU TS RSFM23G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBA
       ;;
(GFM01LBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM23   : NAME=RSFM23L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM23 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM23G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBA_RTFM23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBB
       ;;
(GFM01LBB)
       m_CondExec 04,GE,GFM01LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM54 A PARTIR DU TS RSFM54G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBD
       ;;
(GFM01LBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM54   : NAME=RSFM54L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM54 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM54G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBD_RTFM54.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBE
       ;;
(GFM01LBE)
       m_CondExec 04,GE,GFM01LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM55 A PARTIR DU TS RSFM55G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBG
       ;;
(GFM01LBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM55   : NAME=RSFM55L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM55 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM55G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBG_RTFM55.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBH
       ;;
(GFM01LBH)
       m_CondExec 04,GE,GFM01LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM56 A PARTIR DU TS RSFM56G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBJ
       ;;
(GFM01LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM56   : NAME=RSFM56L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM56 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM56G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBJ_RTFM56.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBK
       ;;
(GFM01LBK)
       m_CondExec 04,GE,GFM01LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM57 A PARTIR DU TS RSFM57G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBM
       ;;
(GFM01LBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM57   : NAME=RSFM57L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM57 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM57G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBM_RTFM57.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBN
       ;;
(GFM01LBN)
       m_CondExec 04,GE,GFM01LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM58 A PARTIR DU TS RSFM58G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBQ
       ;;
(GFM01LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM58   : NAME=RSFM58L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM58 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM58G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBQ_RTFM58.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBR
       ;;
(GFM01LBR)
       m_CondExec 04,GE,GFM01LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM59 A PARTIR DU TS RSFM59G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBT
       ;;
(GFM01LBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM59   : NAME=RSFM59L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM59 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM59G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBT_RTFM59.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBU
       ;;
(GFM01LBU)
       m_CondExec 04,GE,GFM01LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM64 A PARTIR DU TS RSFM64G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LBX
       ;;
(GFM01LBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM64   : NAME=RSFM64L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM64 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM64G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LBX_RTFM64.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LBY
       ;;
(GFM01LBY)
       m_CondExec 04,GE,GFM01LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM69 A PARTIR DU TS RSFM69G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCA
       ;;
(GFM01LCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM69   : NAME=RSFM69L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM69 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM69G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCA_RTFM69.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCB
       ;;
(GFM01LCB)
       m_CondExec 04,GE,GFM01LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM73 A PARTIR DU TS RSFM73G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCD
       ;;
(GFM01LCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM73   : NAME=RSFM73L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM73 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM73G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCD_RTFM73.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCE
       ;;
(GFM01LCE)
       m_CondExec 04,GE,GFM01LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM80 A PARTIR DU TS RSFM80G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCG
       ;;
(GFM01LCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM80   : NAME=RSFM80L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM80 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM80G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCG_RTFM80.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCH
       ;;
(GFM01LCH)
       m_CondExec 04,GE,GFM01LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM81 A PARTIR DU TS RSFM81G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCJ
       ;;
(GFM01LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM81   : NAME=RSFM81L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM81 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM81G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCJ_RTFM81.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCK
       ;;
(GFM01LCK)
       m_CondExec 04,GE,GFM01LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM82 A PARTIR DU TS RSFM82G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCM
       ;;
(GFM01LCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM82   : NAME=RSFM82L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM82 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM82G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCM_RTFM82.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCN
       ;;
(GFM01LCN)
       m_CondExec 04,GE,GFM01LCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM83 A PARTIR DU TS RSFM83G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCQ PGM=DSNUTILB   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCQ
       ;;
(GFM01LCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM83   : NAME=RSFM83L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM83 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM83G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCQ_RTFM83.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCR
       ;;
(GFM01LCR)
       m_CondExec 04,GE,GFM01LCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM84 A PARTIR DU TS RSFM84G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCT PGM=DSNUTILB   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCT
       ;;
(GFM01LCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM84   : NAME=RSFM84L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM84 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM84G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCT_RTFM84.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCU
       ;;
(GFM01LCU)
       m_CondExec 04,GE,GFM01LCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM87 A PARTIR DU TS RSFM87G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LCX
       ;;
(GFM01LCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM87   : NAME=RSFM87L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM87 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM87G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LCX_RTFM87.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LCY
       ;;
(GFM01LCY)
       m_CondExec 04,GE,GFM01LCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM88 A PARTIR DU TS RSFM88G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDA
       ;;
(GFM01LDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM88   : NAME=RSFM88L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM88 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM88G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDA_RTFM88.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDB
       ;;
(GFM01LDB)
       m_CondExec 04,GE,GFM01LDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM89 A PARTIR DU TS RSFM89G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDD
       ;;
(GFM01LDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM89   : NAME=RSFM89L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM89 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM89G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDD_RTFM89.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDE
       ;;
(GFM01LDE)
       m_CondExec 04,GE,GFM01LDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM90 A PARTIR DU TS RSFM90G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDG PGM=DSNUTILB   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDG
       ;;
(GFM01LDG)
       m_CondExec ${EXAFA},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM90   : NAME=RSFM90L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM90 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM90G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDG_RTFM90.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDH
       ;;
(GFM01LDH)
       m_CondExec 04,GE,GFM01LDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM92 A PARTIR DU TS RSFM92G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDJ PGM=DSNUTILB   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDJ
       ;;
(GFM01LDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM92   : NAME=RSFM92L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM92 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM92G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDJ_RTFM92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDK
       ;;
(GFM01LDK)
       m_CondExec 04,GE,GFM01LDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM91 A PARTIR DU TS RSFM91G :JOB UNLOAD BGA01G         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDM PGM=DSNUTILB   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDM
       ;;
(GFM01LDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM91L  : NAME=RSFM91L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM91L /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM91G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDM_RTFM91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDN
       ;;
(GFM01LDN)
       m_CondExec 04,GE,GFM01LDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM93 A PARTIR DU TS RSFM93G :JOB UNLOAD BGA01G         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDQ PGM=DSNUTILB   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDQ
       ;;
(GFM01LDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM93L  : NAME=RSFM93L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM93L /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01LDQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01L_GFM01LDQ_RTFM93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01LDR
       ;;
(GFM01LDR)
       m_CondExec 04,GE,GFM01LDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#   BFTS05 : ALIMENTATION DES TABLES FT                                        
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDT
       ;;
(GFM01LDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# *                                                                            
# ******** TABLES EN   M A J                                                   
# *                                                                            
# *****   TABLE                                                                
#    RSFM20   : NAME=RSFM20L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20 /dev/null
# *****   TABLE                                                                
#    RSFT25   : NAME=RSFT25L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT25 /dev/null
# *****   TABLE                                                                
#    RSFT30   : NAME=RSFT30L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT30 /dev/null
# *****   TABLE                                                                
#    RSFM64   : NAME=RSFM64L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFM64 /dev/null
# *****   TABLE                                                                
#    RSFT60   : NAME=RSFT60L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT60 /dev/null
# *****   TABLE                                                                
#    RSFT61   : NAME=RSFT61L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT61 /dev/null
# *****   TABLE                                                                
#    RSFT62   : NAME=RSFT62L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT62 /dev/null
# *****   TABLE                                                                
#    RSFT66   : NAME=RSFT66L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT66 /dev/null
# *****   TABLE                                                                
#    RSFT67   : NAME=RSFT67L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT67 /dev/null
# *****   TABLE                                                                
#    RSFT68   : NAME=RSFT68L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT68 /dev/null
# *                                                                            
# ******  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******                                                                       
# ******  FICHIER SAM EN ENTREE (TRIE DANS LE PCL BGA01G)                      
# ******                                                                       
       m_FileAssign -d SHR -g +0 FFTS00 ${DATA}/PNCGG/F99.BFTS00BG
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTS05 
       JUMP_LABEL=GFM01LDU
       ;;
(GFM01LDU)
       m_CondExec 04,GE,GFM01LDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BFTS15 : ALIMENTATION DES TABLES FT                                        
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01LDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01LDX
       ;;
(GFM01LDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** FICHIERS EN LECTURE ***********                                     
# *****   TABLE                                                                
       m_FileAssign -d SHR -g +0 FFM65 ${DATA}/PNCGG/F99.UNLOAD.RSFM65G
       m_FileAssign -d SHR -g +0 FFM85 ${DATA}/PNCGG/F99.UNLOAD.RSFM85G
       m_FileAssign -d SHR -g +0 FFM86 ${DATA}/PNCGG/F99.UNLOAD.RSFM86G
       m_FileAssign -d SHR -g +0 FFX00 ${DATA}/PNCGG/F99.UNLOAD.RSFX00G
# *                                                                            
# ******** TABLES EN   M A J                                                   
# *                                                                            
#    RSFM65   : NAME=RSFT65L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM65 /dev/null
#    RSFM85   : NAME=RSFT85L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM85 /dev/null
#    RSFM86   : NAME=RSFT86L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM86 /dev/null
#    RSFX00   : NAME=RSFX00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFX00 /dev/null
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTS15 
       JUMP_LABEL=GFM01LDY
       ;;
(GFM01LDY)
       m_CondExec 04,GE,GFM01LDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
