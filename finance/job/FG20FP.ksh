#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG20FP.ksh                       --- VERSION DU 09/10/2016 00:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG20F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.40.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG20FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE                  
#     REPRISE : OUI                                                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG20FPA
       ;;
(FG20FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG20FPAA
       ;;
(FG20FPAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# ******** FICHIER CONTENANT LE NUMERO DE RBA                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QFG20FP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG20FPAA
       m_ProgramExec IEFBR14 "RMAC,FG20FP.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FG20FPAD
       ;;
(FG20FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA                                   
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QFG20FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG20FPAE
       ;;
(FG20FPAE)
       m_CondExec 00,EQ,FG20FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FERMETURE DE LA BASE D'IMPRESSION SOUS CIDAR ET TRANSACTION FG10            
#  DISABLE DES TRANSACTIONS FF62 ET FF64 SOUS CINCP                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPAG PGM=PGMSVC34   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPAJ
       ;;
(FG20FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_ProgramExec WAITSS "0060"
# ********************************************************************         
#  BFX901 : CHARGEMENT DE LA TABLE RTFX90 NETTING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPAM
       ;;
(FG20FPAM)
       m_CondExec ${EXAAU},NE,YES 
# ******* UNLOAD DE LA TABLE FX00 DU GROUPE (BGA01G)                           
       m_FileAssign -d SHR -g +0 FFX90 ${DATA}/PNCGG/F99.FX00M907
# ********TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFX90   : NAME=RSFX90P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFX90 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFX901 
       JUMP_LABEL=FG20FPAN
       ;;
(FG20FPAN)
       m_CondExec 04,GE,FG20FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM93 A PARTIR DU TS RSFM93G :JOB UNLOAD BGA01G         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPAQ
       ;;
(FG20FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM93   : NAME=RSFM93FP,MODE=(U,N) - DYNAM=YES                           
       m_FileAssign -d SHR RSFM93 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG20FPAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/FG20FP_FG20FPAQ_RTFM93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FG20FPAR
       ;;
(FG20FPAR)
       m_CondExec 04,GE,FG20FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG180 : CHANGEMENT DES DATES D'ECHEANCES DE FACTURE FAISANT                
#           INTERVENIR UNE SOCI�T� DE GESTION DONT LA SOCI�T� COMPTABL         
#           VA ETRE INT�GR�E A UNE AUTRE SOCI�T� LE 1ER JOUR DU MOIS           
#           SUIVANT.                                                           
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPAT
       ;;
(FG20FPAT)
       m_CondExec ${EXABE},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ********TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
# -X-FG20FPR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* ETAT DE CONTROLE(ARCHIVE E.O.S DESCENDU A LA DEMANDE)                
       m_OutputAssign -c 9 -w IFG180 IFG180
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG180 
       JUMP_LABEL=FG20FPAU
       ;;
(FG20FPAU)
       m_CondExec 04,GE,FG20FPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG110 : EXTRACTION DES FACTURES INTER-SOCIETE NON ENCORE COMTABILI         
#              CREATION FICHIER FFGCTA : FACTURES A EDITEES                    
#              CREATION FICHIER FFGANO : ANOMALIES                             
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPAX
       ;;
(FG20FPAX)
       m_CondExec ${EXABJ},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES ECS NETTING                                                
#    RSFX90P  : NAME=RSFX90P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX90P /dev/null
# ******* TABLE DES LIEUX   (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES CORRESPONDANCES JOURNAUX                                   
#    RSFG12   : NAME=RSFG12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG12 /dev/null
# ******* TABLE DES CORRESPONDANCES DES COMPTES                                
#    RSFG13   : NAME=RSFG13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG13 /dev/null
#                                                                              
# ********TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES                                       
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES FACTURES  A EDITER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGCTA ${DATA}/PTEM/FG20FPAX.BFG110AP
# ******* FICHIER DES ANOMALIES A EDITER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGANO ${DATA}/PTEM/FG20FPAX.BFG110BP
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FG20FP
# ********* FSOCSIMU = SOCIETE/ENTITE GL A SIMULER                             
       m_FileAssign -d SHR FSOCSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FG20FP2
# ********* FCOMPTA  = DATE COMPTABLE DE SIMULATION                            
       m_FileAssign -d SHR FCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FG20FP3
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG110 
       JUMP_LABEL=FG20FPAY
       ;;
(FG20FPAY)
       m_CondExec 04,GE,FG20FPAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES A EDITER                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBA
       ;;
(FG20FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FG20FPAX.BFG110AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPBA.BFG210AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_37_8 37 CH 8
 /FIELDS FLD_CH_157_3 157 CH 3
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_14_23 14 CH 23
 /KEYS
   FLD_CH_1_13 ASCENDING,
   FLD_CH_37_8 ASCENDING,
   FLD_CH_157_3 ASCENDING,
   FLD_CH_14_23 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPBB
       ;;
(FG20FPBB)
       m_CondExec 00,EQ,FG20FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG210  : EDITION DES FACTURES INTER-SOCIETE                             
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBD PGM=BFG210     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBD
       ;;
(FG20FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A3} FFGCTA ${DATA}/PTEM/FG20FPBA.BFG210AP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w IFG210 IFG210
       m_ProgramExec BFG210 
# ********************************************************************         
#         TRI DU FICHIER DES ANOMALIES                                         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBG
       ;;
(FG20FPBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/FG20FPAX.BFG110BP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPBG.BFG200AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_37 1 CH 37
 /KEYS
   FLD_CH_1_37 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPBH
       ;;
(FG20FPBH)
       m_CondExec 00,EQ,FG20FPBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG200  : EDITION DES ANOMALIES D'EXTRACTION DES FACTURES                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBJ
       ;;
(FG20FPBJ)
       m_CondExec ${EXACD},NE,YES 
# ******* TABLE DES MESSAGES                                                   
#    RSGA99   : NAME=RSGA99FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA99 /dev/null
# ******* FICHIER DES ANOMALIES                                                
       m_FileAssign -d SHR -g ${G_A5} FFGANO ${DATA}/PTEM/FG20FPBG.BFG200AP
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w IFG200 IFG200
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG200 
       JUMP_LABEL=FG20FPBK
       ;;
(FG20FPBK)
       m_CondExec 04,GE,FG20FPBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG220 : EXTRACTION DES FACTURES INTER-SOCIETE NON ENCORE COMTABILI         
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBM
       ;;
(FG20FPBM)
       m_CondExec ${EXACI},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES                                       
#    RSFG02   : NAME=RSFG02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
# ******* TABLE DES COMMENTAIRES SUR FACTURE                                   
#    RSFG06   : NAME=RSFG06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG06 /dev/null
# ******* TABLE DES DETAILS DES FACTURATIONS MANUELLES                         
#    RSFG07   : NAME=RSFG07,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG07 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES FACTURES  A EDITER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG20FPBM.BFG220AP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG220 
       JUMP_LABEL=FG20FPBN
       ;;
(FG20FPBN)
       m_CondExec 04,GE,FG20FPBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES POUR LES FILIALES                        
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBQ
       ;;
(FG20FPBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPBQ.BFG220BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 175 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPBR
       ;;
(FG20FPBR)
       m_CondExec 00,EQ,FG20FPBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG230  : CREATION DE L'ETAT JFG011 DANS L'IMPRESSION GENERALISE         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBT PGM=DFSRRC00   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBT
       ;;
(FG20FPBT)
       m_CondExec ${EXACS},NE,YES 
# ACS     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG200RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG200RC1
# ******  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A7} FFGEDG ${DATA}/PTEM/FG20FPBQ.BFG220BP
# ******  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG20FPR2)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#         TRI DU FICHIER DES FACTURES POUR PARIS                               
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPBX
       ;;
(FG20FPBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG011 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG20FPBY
       ;;
(FG20FPBY)
       m_CondExec 00,EQ,FG20FPBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES POUR 505/913/930                         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCA
       ;;
(FG20FPCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FG20FPBM.BFG220AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG011A SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "913"
 /DERIVEDFIELD CST_1_4 "505"
 /DERIVEDFIELD CST_5_12 "930"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 OR FLD_CH_15_3 EQ CST_3_8 OR FLD_CH_15_3 EQ CST_5_12 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG20FPCB
       ;;
(FG20FPCB)
       m_CondExec 00,EQ,FG20FPCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES (E)                                      
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCD
       ;;
(FG20FPCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EDITION DES FACTURES POUR CUMUL                            
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/FG20FPBM.BFG220AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPCD.BFG220DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "E"
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_24_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG20FPCE
       ;;
(FG20FPCE)
       m_CondExec 00,EQ,FG20FPCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES POUR CUMUL MISCROLIST                    
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCG
       ;;
(FG20FPCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EDITION DES FACTURES POUR CUMUL                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BFG220CP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/FG20FPCD.BFG220DP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BFG220CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPCH
       ;;
(FG20FPCH)
       m_CondExec 00,EQ,FG20FPCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *** PARTIE AJOUTER PAR PHIL LE 251095 ******************************         
# ********************************************************************         
#  BFG100 : CALCUL ET GENERATION DES ECRITURES REGLEMENTS                      
#              CREATION FICHIER FFGREGL: FACTURATION REGLEES                   
#              CREATION FICHIER FFGCTA : EDITION ETAT COMPTA                   
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCJ
       ;;
(FG20FPCJ)
       m_CondExec ${EXADR},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX   (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
#                                                                              
# ******* TABLE DES CORRESPONDANCES JOURNAUX                                   
#    RSFG12   : NAME=RSFG12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG12 /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES                                       
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* ETAT REGLEMENT (POUR J.FAIDY)                                        
       m_OutputAssign -c 9 -w IFG100 IFG100
# ******* FICHIER DE L ETAT DE COMPTA (LRECL 180)                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FFGCTA ${DATA}/PTEM/FG20FPCJ.BFG100AP
# ******* FICHIER POUR FACTURATION REGLEES (LRECL 090)                         
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 FFGREGL ${DATA}/PTEM/FG20FPCJ.BFG100BP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG100 
       JUMP_LABEL=FG20FPCK
       ;;
(FG20FPCK)
       m_CondExec 04,GE,FG20FPCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DE L ETAT COMPTA                                      
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCM
       ;;
(FG20FPCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FG20FPCJ.BFG100AP
# ******* FICHIER DE DE L ETA COMPTA                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPCM.BFG100CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_36 1 CH 36
 /KEYS
   FLD_CH_1_36 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPCN
       ;;
(FG20FPCN)
       m_CondExec 00,EQ,FG20FPCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG210  : EDITION COMPTABILITE REGLEMENTS                                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCQ PGM=BFG210     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCQ
       ;;
(FG20FPCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A13} FFGCTA ${DATA}/PTEM/FG20FPCM.BFG100CP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w IFG210 IFG210
       m_ProgramExec BFG210 
# ********************************************************************         
#         TRI DU FICHIER REGLEMENTS                                            
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCT
       ;;
(FG20FPCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FG20FPCJ.BFG100BP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPCT.BFG100DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_26_14 26 CH 14
 /FIELDS FLD_CH_14_12 14 CH 12
 /KEYS
   FLD_CH_1_13 ASCENDING,
   FLD_CH_26_14 ASCENDING,
   FLD_CH_14_12 ASCENDING
 /* Record Type = F  Record Length = 090 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPCU
       ;;
(FG20FPCU)
       m_CondExec 00,EQ,FG20FPCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG215 : EDITION DES FACTURATIONS ARRIVES A ECHEANCE                        
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPCX
       ;;
(FG20FPCX)
       m_CondExec ${EXAEL},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES REGLEMENT A EDITER                                       
       m_FileAssign -d SHR -g ${G_A15} FFGREGL ${DATA}/PTEM/FG20FPCT.BFG100DP
#                                                                              
# ******* FICHIER DES REGLEMENTS A TRIER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG20FPCX.BFG100EP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG215 
       JUMP_LABEL=FG20FPCY
       ;;
(FG20FPCY)
       m_CondExec 04,GE,FG20FPCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES REGLEMENTS ( SORTIE DU PGM BFG215)                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDA
       ;;
(FG20FPDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG20FPDA.BFG100FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 175 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPDB
       ;;
(FG20FPDB)
       m_CondExec 00,EQ,FG20FPDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDD PGM=DFSRRC00   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDD
       ;;
(FG20FPDD)
       m_CondExec ${EXAEV},NE,YES 
# AEV     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG200RC2),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG200RC2
# ******  FICHIER DES REGLEMENTS                                               
       m_FileAssign -d SHR -g ${G_A17} FFGEDG ${DATA}/PTEM/FG20FPDA.BFG100FP
# ******  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG20FPR3)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#         TRI DU FICHIER DES FACTURES DE PARIS                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDG
       ;;
(FG20FPDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG012 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG20FPDH
       ;;
(FG20FPDH)
       m_CondExec 00,EQ,FG20FPDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES REGLEMENTS  (LA TOTALITE )                        
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDJ
       ;;
(FG20FPDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/FG20FPCX.BFG100EP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG015 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /KEYS
   FLD_CH_1_42 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG20FPDK
       ;;
(FG20FPDK)
       m_CondExec 00,EQ,FG20FPDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         MERGE DES DEUX FICHIERS FFGCTA (COMPTA + REGLEMENTS )                
#         POUR EDITION                                                         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDM
       ;;
(FG20FPDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/FG20FPCJ.BFG100AP
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/FG20FPAX.BFG110AP
# ******* FICHIER D'EDITION FACTURES + REGLEMENTS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.REGLFACT.JOUR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "O"
 /FIELDS FLD_CH_37_8 37 CH 8
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_166_1 166 CH 1
 /CONDITION CND_1 FLD_CH_166_1 NE CST_1_6 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_37_8 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG20FPDN
       ;;
(FG20FPDN)
       m_CondExec 00,EQ,FG20FPDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * FIN DE L INSERTION ***********************************************         
# ********************************************************************         
#  BFG211 : EDITION                                                            
#  IFG211 : JOURNAL DE DEVERSEMENT LA FACTURATION INTER-SOCIETE                
#  IFG212 : RECAPITULATIF DES JOURNAUX DE LA FACTURATION INTER-SOCIETE         
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDQ
       ;;
(FG20FPDQ)
       m_CondExec ${EXAFP},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES FACTURES  A EDITER                                       
       m_FileAssign -d SHR -g ${G_A22} FFGCTA ${DATA}/PEX0/F07.REGLFACT.JOUR
#                                                                              
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FG20FP
# ******* EDITIONS (VOIR COMMENT STEP)                                         
       m_OutputAssign -c 9 -w IFG211 IFG211
       m_OutputAssign -c 9 -w IFG212 IFG212
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG211 
       JUMP_LABEL=FG20FPDR
       ;;
(FG20FPDR)
       m_CondExec 04,GE,FG20FPDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REOUVERTURE DE LA BASE D'IMPRESSION SOUS CIDAR                              
#  ENABLE  DES TRANSACTIONS FF62 ET FF64 SOUS CINCP                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG20FPDT PGM=PGMSVC34   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPDT
       ;;
(FG20FPDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG20FPDT.sysin
       m_UtilityExec
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG20FPZA
       ;;
(FG20FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG20FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
