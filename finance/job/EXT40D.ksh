#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT40D.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDEXT40 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/08/20 AT 15.11.06 BY BURTECN                      
#    STANDARDS: P  JOBSET: EXT40D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                                                                              
#   CREE FIC DES ARTICLES DEMANDES DS TABLE GENERALISEES  CONSL                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EXT40DA
       ;;
(EXT40DA)
#
#EXT40DBJ
#EXT40DBJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT40DBJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EXT40DAA
       ;;
(EXT40DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#                                                                              
#      TABLE HISTO DE VENTES PAR                                               
#    RSHV28D  : NAME=RSHV28D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV28D /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/EXT40DAA.BEX040AD
#     CODE ETAT 005 POUR CREER LA TABLE RTEX40                                 
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/EXT40DAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=EXT40DAB
       ;;
(EXT40DAB)
       m_CondExec 04,GE,EXT40DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAD
       ;;
(EXT40DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EXT40DAA.BEX040AD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT40DAD.BEX040BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT40DAE
       ;;
(EXT40DAE)
       m_CondExec 00,EQ,EXT40DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX230                          *******         
#   ENRICHIE LES DONNEES EXTRAITES                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAG
       ;;
(EXT40DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  RTHV01 EN SEQUENTIEL TRIE PAR CODIC SOCIETE (GV000D)                        
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PXX0/F91.HV01AD
#      TABLE HISTO DE VENTES PAR                                               
#    RSHV28R  : NAME=RSHV28D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV28R /dev/null
# ******  CODE SOCIETE (991)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FDATE
$FMOISJ
_end
#     CODE ETAT 005 POUR CREER LA TABLE RTEX40                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT40DAG
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PTEM/EXT40DAD.BEX040BD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX002 ${DATA}/PTEM/EXT40DAG.BEX040CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX230 
       JUMP_LABEL=EXT40DAH
       ;;
(EXT40DAH)
       m_CondExec 04,GE,EXT40DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#            TRI PAR SOCIETE , LIEU , WSEQPRO                                  
#          ************************************                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAJ
       ;;
(EXT40DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/EXT40DAG.BEX040CD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT40DAJ.BEX040DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_177_4 177 PD 4
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_CH_574_3 574 CH 3
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_415_8 415 PD 8
 /KEYS
   FLD_PD_177_4 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_574_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT40DAK
       ;;
(EXT40DAK)
       m_CondExec 00,EQ,EXT40DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX003                          *******         
#   EXTRACTION DONNEES BUDGET                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAM
       ;;
(EXT40DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#      TABLE HISTO DE VENTES PAR                                               
#    RSHV28D  : NAME=RSHV28D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV28D /dev/null
       m_FileAssign -i FDATE
$FMOISJ
_end
#     CODE ETAT 005 POUR CREER LA TABLE RTEX40                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT40DAM
       m_FileAssign -d SHR -g ${G_A4} FEX002 ${DATA}/PTEM/EXT40DAJ.BEX040DD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX003 ${DATA}/PTEM/EXT40DAM.BEX040ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX003 
       JUMP_LABEL=EXT40DAN
       ;;
(EXT40DAN)
       m_CondExec 04,GE,EXT40DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES + SOMME DES QUANTITES NUMERIQUES                  
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAQ
       ;;
(EXT40DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/EXT40DAM.BEX040ED
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT40DAQ.BEX040FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /FIELDS FLD_PD_415_8 415 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_CH_7_130 7 CH 130
 /KEYS
   FLD_CH_7_130 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT40DAR
       ;;
(EXT40DAR)
       m_CondExec 00,EQ,EXT40DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX010                          *******         
#   CORRECTION DU NUMERO DE SEQUENCE                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAT
       ;;
(EXT40DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FMOISJ
_end
#     CODE ETAT 005 POUR CREER LA TABLE RTEX40                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT40DAT
#  FICHIER PARAMETRAGE DO                                                      
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A6} FEX009 ${DATA}/PTEM/EXT40DAQ.BEX040FD
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX010 ${DATA}/PTEM/EXT40DAT.BEX510AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX010 
       JUMP_LABEL=EXT40DAU
       ;;
(EXT40DAU)
       m_CondExec 04,GE,EXT40DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES WSEQPRO FLAG                                      
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DAX
       ;;
(EXT40DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/EXT40DAT.BEX510AD
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 SORTOUT ${DATA}/PTEM/EXT40DAX.BEX510BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_135 1 CH 135
 /KEYS
   FLD_CH_1_135 ASCENDING
 /* Record Type = F  Record Length = 735 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT40DAY
       ;;
(EXT40DAY)
       m_CondExec 00,EQ,EXT40DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX011                          *******         
#   CUMUL SUR WSEQPRO                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DBA
       ;;
(EXT40DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} FEX010 ${DATA}/PTEM/EXT40DAX.BEX510BD
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX011 ${DATA}/PTEM/EXT40DBA.BEX511AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX011 
       JUMP_LABEL=EXT40DBB
       ;;
(EXT40DBB)
       m_CondExec 04,GE,EXT40DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX012                          *******         
#   AJOUT DES TOTALISATIONS                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DBD
       ;;
(EXT40DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FMOISJ
_end
#     CODE ETAT 005 POUR CREER LA TABLE RTEX40                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT40DBD
#  FICHIER DE PARAMETRAGE DE LA DO                                             
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A9} FEX011 ${DATA}/PTEM/EXT40DBA.BEX511AD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX012 ${DATA}/PTEM/EXT40DBD.BEX512AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX012 
       JUMP_LABEL=EXT40DBE
       ;;
(EXT40DBE)
       m_CondExec 04,GE,EXT40DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTEX40                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DBG
       ;;
(EXT40DBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSEX40D  : NAME=RSEX40D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSEX40D /dev/null
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PTEM/EXT40DBD.BEX512AD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT40DBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EXT40D_EXT40DBG_RTEX40.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EXT40DBH
       ;;
(EXT40DBH)
       m_CondExec 04,GE,EXT40DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLE SPACE RSEX40  DE LA D BASE PDDEX00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT40DBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=EXT40DZA
       ;;
(EXT40DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT40DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
