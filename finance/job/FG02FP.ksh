#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG02FP.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG02F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.32.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FG02FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS TRESO AVEC OMIT DES ENREGS DE DEBUT ET FIN                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG02FPA
       ;;
(FG02FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG02FPAA
       ;;
(FG02FPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F99.TRESO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG02FPAA.BFG002A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "DEBUT"
 /DERIVEDFIELD CST_3_9 "FIN"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_1_27 1 CH 27
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_46_8 46 CH 8
 /CONDITION CND_1 FLD_CH_1_5 EQ CST_1_5 OR FLD_CH_1_3 EQ CST_3_9 
 /KEYS
   FLD_CH_1_27 ASCENDING,
   FLD_CH_46_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG02FPAB
       ;;
(FG02FPAB)
       m_CondExec 00,EQ,FG02FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG002A                                                               
#  -------------                                                               
#  TRAITEMENT DU FICHIER TRESO ISSU DU PC DA LA TRESORERIE                     
#    (PXX0.F99.TRESO)                                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAD
       ;;
(FG02FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFG10   : NAME=RSFG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG10 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ------  FICHIER TRESORERIE TRIE                                              
       m_FileAssign -d SHR -g ${G_A1} FFMICRO ${DATA}/PTEM/FG02FPAA.BFG002A
# ------  DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER EN PREPARATION POUR BFG000                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 185 -t LSEQ -g +1 FFG01 ${DATA}/PTEM/FG02FPAD.BFG002XP
# ------  COMPTE-RENDU D'EXECUTION IFG002A                                     
       m_OutputAssign -c 9 -w IFG002A IMPRIM
# ------  LISTE DES ANOAMLIES (FACTURES REJETEES)                              
       m_OutputAssign -c 9 -w IFG002B ILISTE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG002A 
       JUMP_LABEL=FG02FPAE
       ;;
(FG02FPAE)
       m_CondExec 04,GE,FG02FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FFG01 (BFG002XP) TRESO                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAG
       ;;
(FG02FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FG02FPAD.BFG002XP
       m_FileAssign -d NEW,CATLG,DELETE -r 185 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG02FPAG.BFG002YP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_54 1 CH 54
 /KEYS
   FLD_CH_1_54 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG02FPAH
       ;;
(FG02FPAH)
       m_CondExec 00,EQ,FG02FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG000                                                                
#  ------------                                                                
#  ALIMENTATION DE LA TABLE RTFG01 : FACTURATION GROUPE                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAJ
       ;;
(FG02FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA99   : NAME=RSGA99FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA99 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ------  FICHIER DES FACTURES TRESO                                           
       m_FileAssign -d SHR -g ${G_A3} FFG01 ${DATA}/PTEM/FG02FPAG.BFG002YP
# ------  DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ------  EDITIONS DE CONTROLE                                                 
       m_OutputAssign -c 9 -w BFG000A FGVENANO
       m_OutputAssign -c 9 -w BFG000B FGNATANO
       m_OutputAssign -c 9 -w BFG000C FGFRQANO
       m_OutputAssign -c 9 -w BFG000D FGANNANO
       m_OutputAssign -c 9 -w BFG000E FG10ANO
       m_OutputAssign -c 9 -w BFG000F FG01ANO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG000A 
       JUMP_LABEL=FG02FPAK
       ;;
(FG02FPAK)
       m_CondExec 04,GE,FG02FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE DES FICHIERS TRESO SUR UN FICHIER HISTORIQUE                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAM
       ;;
(FG02FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A4} IN1 ${DATA}/PTEM/FG02FPAA.BFG002A
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.TRESOHIS
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG02FPAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG02FPAN
       ;;
(FG02FPAN)
       m_CondExec 16,NE,FG02FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SUPPRESSION DES GENERATIONS DES FICHIERS TESO SI TRAITEMENT OK              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAQ
       ;;
(FG02FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG02FPAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG02FPAR
       ;;
(FG02FPAR)
       m_CondExec 16,NE,FG02FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UNE GENERATION A VIDE DU FICHIER TRESO POUR NE PAS               
#  PLANTER LE LENDEMAIN SI PAS DE TRANSFERT                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG02FPAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPAT
       ;;
(FG02FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.TRESO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG02FPAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG02FPAU
       ;;
(FG02FPAU)
       m_CondExec 16,NE,FG02FPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG02FPZA
       ;;
(FG02FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG02FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
