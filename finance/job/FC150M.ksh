#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FC150M.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFC150 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/12/13 AT 09.15.35 BY BURTECC                      
#    STANDARDS: P  JOBSET: FC150M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DU TABLESPACES: RSMQ15M                                             
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FC150MA
       ;;
(FC150MA)
#
#FC150MAA
#FC150MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#FC150MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FC150MAD
       ;;
(FC150MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/FC150MQ1
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/FC150MAD
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15M  : NAME=RSMQ15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15M /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTS075 ${DATA}/PXX0/F89.BNM175AM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTS076 ${DATA}/PXX0/F89.BNM176AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=FC150MAE
       ;;
(FC150MAE)
       m_CondExec 04,GE,FC150MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTS075 (ENTETE FACTURE)                                      
#  POUR PGM BFC150                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC150MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FC150MAG
       ;;
(FC150MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.BNM175AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PXX0/FC150MAG.FIC075AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC150MAH
       ;;
(FC150MAH)
       m_CondExec 00,EQ,FC150MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTS076 (LIEN FACTURE VENTES)                                 
#  POUR PGM BFC150                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC150MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FC150MAJ
       ;;
(FC150MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F89.BNM176AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PXX0/FC150MAJ.FIC076AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_14_14 14 CH 14
 /KEYS
   FLD_CH_14_14 ASCENDING,
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC150MAK
       ;;
(FC150MAK)
       m_CondExec 00,EQ,FC150MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROG    BFC150                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC150MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FC150MAM
       ;;
(FC150MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   ENTETE DE FACTURE                                                    
#    RTFA01   : NAME=RSFA01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTFA01 /dev/null
# *****   LIEN FACTURE/VENTE                                                   
#    RTFA02   : NAME=RSFA02M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTFA02 /dev/null
# *****   ANOMALIES                                                            
#    RTAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
#                                                                              
# ******  ENTETE DE FACTURE LOCALE                                             
       m_FileAssign -d SHR -g ${G_A3} FTS075 ${DATA}/PXX0/FC150MAG.FIC075AM
# ******  LIEN  FACTURE/VENTE                                                  
       m_FileAssign -d SHR -g ${G_A4} FTS076 ${DATA}/PXX0/FC150MAJ.FIC076AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFC150 
       JUMP_LABEL=FC150MAN
       ;;
(FC150MAN)
       m_CondExec 04,GE,FC150MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTS076 (LIEN FACTURE VENTES)                                 
#  POUR PGM BFC160                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC150MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FC150MAQ
       ;;
(FC150MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/FC150MAJ.FIC076AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PXX0/FC150MAQ.FIC076BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC150MAR
       ;;
(FC150MAR)
       m_CondExec 00,EQ,FC150MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROG    BFC160 : EDITION DE L ETAT IFC160 :RECAP FACTURES MAGASIN           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC150MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FC150MAT
       ;;
(FC150MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   LIEUX                                                                
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#                                                                              
# *****   ENTETE DE FACTURE LOCALE                                             
       m_FileAssign -d SHR -g ${G_A6} FTS075 ${DATA}/PXX0/FC150MAG.FIC075AM
# *****   LIEN   FACTURE/VENTE                                                 
       m_FileAssign -d SHR -g ${G_A7} FTS076 ${DATA}/PXX0/FC150MAQ.FIC076BM
#                                                                              
#                                                                              
# * MODIF LE 291100 CGA PLUS UTIL A DAL                                        
# ******  EDITION DE L ETAT IFC160                                             
#  IFC160   REPORT SYSOUT=(9,IFC160),LRECL=133,RECFM=FBA,BLKSIZE=1330          
       m_OutputAssign -c "*" IFC160
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFC160 
       JUMP_LABEL=FC150MAU
       ;;
(FC150MAU)
       m_CondExec 04,GE,FC150MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FC150MZA
       ;;
(FC150MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC150MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
