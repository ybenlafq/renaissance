#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA210D.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDRA210 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.12.36 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RA210D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER PROVENANT DE L'ECLATEMENT                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RA210DA
       ;;
(RA210DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RA210DAA
       ;;
(RA210DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FRA210BD
#         FILE  NAME=FRA2REJD,MODE=I                                           
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FRA210CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_263_10 263 CH 10
 /FIELDS FLD_CH_252_7 252 CH 7
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_11_6 11 CH 6
 /KEYS
   FLD_CH_11_6 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_252_7 ASCENDING,
   FLD_CH_263_10 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA210DAB
       ;;
(RA210DAB)
       m_CondExec 00,EQ,RA210DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA210 **  CREATION DES AVOIRS DE REMBOURSEMENT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA210DAD
       ;;
(RA210DAD)
       m_CondExec ${EXAAF},NE,YES 
# ******* FIC DES AVOIRS + REJETS TRI�S                                        
       m_FileAssign -d SHR -g ${G_A1} FRA210 ${DATA}/PXX0/F91.FRA210CD
# ******* TABLE EN LECTURE                                                     
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGQ96   : NAME=RSGQ96,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ96 /dev/null
#    RTGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTRX00   : NAME=RSRX00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRX00 /dev/null
# ******* TABLE EN ECRITURE                                                    
#    RTRA00   : NAME=RSRA00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA00 /dev/null
#    RTRA01   : NAME=RSRA01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA01 /dev/null
#        FDATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES DEMANDES DE REMBOURSEMENTS D'AVOIR _A REPRENDRE               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FREJETS ${DATA}/PXX0/F91.FRA2REJD
# ******* CET �TAT EST ENVOY� SOUS EOS DANS LA BOITE DFO                       
# IRA210   REPORT SYSOUT=(9,IRA210D),RECFM=FBA,LRECL=133,BLKSIZE=0             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IRA210 ${DATA}/PXX0/F91.FRA210DD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA210 
       JUMP_LABEL=RA210DAE
       ;;
(RA210DAE)
       m_CondExec 04,GE,RA210DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE _A Z�RO DU FICHIER VENANT DE SIEBEL                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210DAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA210DAG
       ;;
(RA210DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC ECLATE REMIS _A Z�RO                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FRA210BD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210DAG.sysin
       m_UtilityExec
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=RA210XXX,                                                           
#      FNAME=":FRA210DD""(0),                                                  
#      NFNAME=RA210D                                                           
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210DAH
       ;;
(RA210DAH)
       m_CondExec 16,NE,RA210DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTRA210D                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210DAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RA210DAJ
       ;;
(RA210DAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210DAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RA210DAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RA210DAM
       ;;
(RA210DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210DAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
