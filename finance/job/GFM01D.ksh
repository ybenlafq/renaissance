#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFM01D.ksh                       --- VERSION DU 08/10/2016 17:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGFM01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/01 AT 09.45.47 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GFM01D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#     LOAD DES TABLESPACE FM                                                   
#  QUIESCE DES TABLES RTFM01-02-03-04-05-06-23-64-69-73                        
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFM01DA
       ;;
(GFM01DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFM01DAA
       ;;
(GFM01DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGFM01D
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GFM01DAA
       m_ProgramExec IEFBR14 "RDAR,GFM01D.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAD
       ;;
(GFM01DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGFM01D
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAE
       ;;
(GFM01DAE)
       m_CondExec 00,EQ,GFM01DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM01 A PARTIR DU TS RSFM01G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAG
       ;;
(GFM01DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM01   : NAME=RSFM01D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM01 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM01G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAG_RTFM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAH
       ;;
(GFM01DAH)
       m_CondExec 04,GE,GFM01DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM02 A PARTIR DU TS RSFM02G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAJ
       ;;
(GFM01DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM02   : NAME=RSFM02D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM02 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM02G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAJ_RTFM02.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAK
       ;;
(GFM01DAK)
       m_CondExec 04,GE,GFM01DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM03 A PARTIR DU TS RSFM03G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAM
       ;;
(GFM01DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM03   : NAME=RSFM03D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM03 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM03G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAM_RTFM03.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAN
       ;;
(GFM01DAN)
       m_CondExec 04,GE,GFM01DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM04 A PARTIR DU TS RSFM04G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAQ
       ;;
(GFM01DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM04   : NAME=RSFM04D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM04 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM04G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAQ_RTFM04.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAR
       ;;
(GFM01DAR)
       m_CondExec 04,GE,GFM01DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM05 A PARTIR DU TS RSFM05G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAT
       ;;
(GFM01DAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM05   : NAME=RSFM05D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM05G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAT_RTFM05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAU
       ;;
(GFM01DAU)
       m_CondExec 04,GE,GFM01DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM06 A PARTIR DU TS RSFM06G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DAX
       ;;
(GFM01DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM06   : NAME=RSFM06D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM06 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM06G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DAX_RTFM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DAY
       ;;
(GFM01DAY)
       m_CondExec 04,GE,GFM01DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM23 A PARTIR DU TS RSFM23G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBA
       ;;
(GFM01DBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM23   : NAME=RSFM23D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM23 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM23G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBA_RTFM23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBB
       ;;
(GFM01DBB)
       m_CondExec 04,GE,GFM01DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM54 A PARTIR DU TS RSFM54G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBD
       ;;
(GFM01DBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM54   : NAME=RSFM54D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM54 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM54G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBD_RTFM54.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBE
       ;;
(GFM01DBE)
       m_CondExec 04,GE,GFM01DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM55 A PARTIR DU TS RSFM55G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBG
       ;;
(GFM01DBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM55   : NAME=RSFM55D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM55 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM55G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBG_RTFM55.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBH
       ;;
(GFM01DBH)
       m_CondExec 04,GE,GFM01DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM56 A PARTIR DU TS RSFM56G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBJ
       ;;
(GFM01DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM56   : NAME=RSFM56D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM56 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM56G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBJ_RTFM56.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBK
       ;;
(GFM01DBK)
       m_CondExec 04,GE,GFM01DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM57 A PARTIR DU TS RSFM57G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBM
       ;;
(GFM01DBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM57   : NAME=RSFM57D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM57 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM57G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBM_RTFM57.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBN
       ;;
(GFM01DBN)
       m_CondExec 04,GE,GFM01DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM58 A PARTIR DU TS RSFM58G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBQ
       ;;
(GFM01DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM58   : NAME=RSFM58D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM58 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM58G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBQ_RTFM58.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBR
       ;;
(GFM01DBR)
       m_CondExec 04,GE,GFM01DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM59 A PARTIR DU TS RSFM59G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBT
       ;;
(GFM01DBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM59   : NAME=RSFM59D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM59 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM59G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBT_RTFM59.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBU
       ;;
(GFM01DBU)
       m_CondExec 04,GE,GFM01DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM64 A PARTIR DU TS RSFM64G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DBX
       ;;
(GFM01DBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM64   : NAME=RSFM64D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM64 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM64G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DBX_RTFM64.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DBY
       ;;
(GFM01DBY)
       m_CondExec 04,GE,GFM01DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM69 A PARTIR DU TS RSFM69G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCA
       ;;
(GFM01DCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM69   : NAME=RSFM69D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM69 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM69G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCA_RTFM69.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCB
       ;;
(GFM01DCB)
       m_CondExec 04,GE,GFM01DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM73 A PARTIR DU TS RSFM73G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCD
       ;;
(GFM01DCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM73   : NAME=RSFM73D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM73 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM73G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCD_RTFM73.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCE
       ;;
(GFM01DCE)
       m_CondExec 04,GE,GFM01DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM80 A PARTIR DU TS RSFM80G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCG
       ;;
(GFM01DCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM80   : NAME=RSFM80D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM80 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM80G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCG_RTFM80.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCH
       ;;
(GFM01DCH)
       m_CondExec 04,GE,GFM01DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM81 A PARTIR DU TS RSFM81G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCJ
       ;;
(GFM01DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM81   : NAME=RSFM81D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM81 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM81G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCJ_RTFM81.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCK
       ;;
(GFM01DCK)
       m_CondExec 04,GE,GFM01DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM82 A PARTIR DU TS RSFM82G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCM
       ;;
(GFM01DCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM82   : NAME=RSFM82D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM82 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM82G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCM_RTFM82.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCN
       ;;
(GFM01DCN)
       m_CondExec 04,GE,GFM01DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM83 A PARTIR DU TS RSFM83G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCQ PGM=DSNUTILB   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCQ
       ;;
(GFM01DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM83   : NAME=RSFM83D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM83 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM83G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCQ_RTFM83.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCR
       ;;
(GFM01DCR)
       m_CondExec 04,GE,GFM01DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM84 A PARTIR DU TS RSFM84G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCT PGM=DSNUTILB   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCT
       ;;
(GFM01DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM84   : NAME=RSFM84D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM84 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM84G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCT_RTFM84.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCU
       ;;
(GFM01DCU)
       m_CondExec 04,GE,GFM01DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM87 A PARTIR DU TS RSFM87G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DCX
       ;;
(GFM01DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM87   : NAME=RSFM87D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM87 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM87G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DCX_RTFM87.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DCY
       ;;
(GFM01DCY)
       m_CondExec 04,GE,GFM01DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM88 A PARTIR DU TS RSFM88G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDA
       ;;
(GFM01DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM88   : NAME=RSFM88D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM88 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM88G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDA_RTFM88.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDB
       ;;
(GFM01DDB)
       m_CondExec 04,GE,GFM01DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM89 A PARTIR DU TS RSFM89G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDD
       ;;
(GFM01DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM89   : NAME=RSFM89D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM89 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM89G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDD_RTFM89.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDE
       ;;
(GFM01DDE)
       m_CondExec 04,GE,GFM01DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM90 A PARTIR DU TS RSFM90G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDG PGM=DSNUTILB   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDG
       ;;
(GFM01DDG)
       m_CondExec ${EXAFA},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM90   : NAME=RSFM90D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM90 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM90G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDG_RTFM90.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDH
       ;;
(GFM01DDH)
       m_CondExec 04,GE,GFM01DDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM92 A PARTIR DU TS RSFM92G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDJ PGM=DSNUTILB   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDJ
       ;;
(GFM01DDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM92   : NAME=RSFM92D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM92 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM92G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDJ_RTFM92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDK
       ;;
(GFM01DDK)
       m_CondExec 04,GE,GFM01DDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM91 A PARTIR DU TS RSFM91G :JOB UNLOAD BGA01G         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDM PGM=DSNUTILB   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDM
       ;;
(GFM01DDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM91D  : NAME=RSFM91D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM91D /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM91G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDM_RTFM91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDN
       ;;
(GFM01DDN)
       m_CondExec 04,GE,GFM01DDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM93 A PARTIR DU TS RSFM93G :JOB UNLOAD BGA01G         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDQ PGM=DSNUTILB   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDQ
       ;;
(GFM01DDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM93D  : NAME=RSFM93D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM93D /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFM01DDQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GFM01D_GFM01DDQ_RTFM93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFM01DDR
       ;;
(GFM01DDR)
       m_CondExec 04,GE,GFM01DDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BFTS05 : ALIMENTATION DES TABLES FT                                        
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDT
       ;;
(GFM01DDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# *                                                                            
# ******** TABLES EN   M A J                                                   
# *                                                                            
# *****   TABLE                                                                
#    RSFM20   : NAME=RSFM20D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20 /dev/null
# *****   TABLE                                                                
#    RSFT25   : NAME=RSFT25D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT25 /dev/null
# *****   TABLE                                                                
#    RSFT30   : NAME=RSFT30D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT30 /dev/null
# *****   TABLE                                                                
#    RSFM64   : NAME=RSFM64D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFM64 /dev/null
# *****   TABLE                                                                
#    RSFT60   : NAME=RSFT60D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT60 /dev/null
# *****   TABLE                                                                
#    RSFT61   : NAME=RSFT61D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT61 /dev/null
# *****   TABLE                                                                
#    RSFT62   : NAME=RSFT62D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT62 /dev/null
# *****   TABLE                                                                
#    RSFT66   : NAME=RSFT66D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT66 /dev/null
# *****   TABLE                                                                
#    RSFT67   : NAME=RSFT67D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT67 /dev/null
# *****   TABLE                                                                
#    RSFT68   : NAME=RSFT68D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT68 /dev/null
# *                                                                            
# ******  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******                                                                       
# ******  FICHIER SAM EN ENTREE (TRIE DANS LE PCL BGA01G)                      
# ******                                                                       
       m_FileAssign -d SHR -g +0 FFTS00 ${DATA}/PNCGG/F99.BFTS00BG
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTS05 
       JUMP_LABEL=GFM01DDU
       ;;
(GFM01DDU)
       m_CondExec 04,GE,GFM01DDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BFTS15 : ALIMENTATION DES TABLES FT                                        
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFM01DDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GFM01DDX
       ;;
(GFM01DDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** FICHIERS EN LECTURE ***********                                     
# *****   TABLE                                                                
       m_FileAssign -d SHR -g +0 FFM65 ${DATA}/PNCGG/F99.UNLOAD.RSFM65G
       m_FileAssign -d SHR -g +0 FFM85 ${DATA}/PNCGG/F99.UNLOAD.RSFM85G
       m_FileAssign -d SHR -g +0 FFM86 ${DATA}/PNCGG/F99.UNLOAD.RSFM86G
       m_FileAssign -d SHR -g +0 FFX00 ${DATA}/PNCGG/F99.UNLOAD.RSFX00G
# *                                                                            
# ******** TABLES EN   M A J                                                   
# *                                                                            
#    RSFM65   : NAME=RSFT65D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM65 /dev/null
#    RSFM85   : NAME=RSFT85D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM85 /dev/null
#    RSFM86   : NAME=RSFT86D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFM86 /dev/null
#    RSFX00   : NAME=RSFX00D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFX00 /dev/null
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTS15 
       JUMP_LABEL=GFM01DDY
       ;;
(GFM01DDY)
       m_CondExec 04,GE,GFM01DDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
