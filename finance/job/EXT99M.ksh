#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT99M.ksh                       --- VERSION DU 08/10/2016 13:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMEXT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/03/17 AT 15.14.08 BY BURTECB                      
#    STANDARDS: P  JOBSET: EXT99M                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ : RSGA09M RSGA11M RSGA12M RSGA29M              
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  ET UTILISER LA CHAINE DE RECOVER DB2 DB2RBM AVEC CE RBA EN PREP             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EXT99MA
       ;;
(EXT99MA)
#
#EXT99MAA
#EXT99MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT99MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    95/03/17 AT 15.14.08 BY BURTECB                  
# *    JOBSET INFORMATION:    NAME...: EXT99M                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'MAJ CODES ETATS'                       
# *                           APPL...: REPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EXT99MAD
       ;;
(EXT99MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES CODES ETATS VENANT DE EXT99P                             
       m_FileAssign -d SHR -g +0 FEX998 ${DATA}/PXX0/F07.BEX998AP
#                                                                              
# ******  CODE MARKETING                                                       
#    RSGA09M  : NAME=RSGA09M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09M /dev/null
# ******  RELATION ETAT FAMILLE/RAYON                                          
#    RSGA11M  : NAME=RSGA11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******  RELATION FAMILLE/CODE MARKETING                                      
#    RSGA12M  : NAME=RSGA12M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12M /dev/null
# ******  LIBELLES DES IMBRICATIONS CODE MARKETING                             
#    RSGA29M  : NAME=RSGA29M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29M /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX999 
       JUMP_LABEL=EXT99MAE
       ;;
(EXT99MAE)
       m_CondExec 04,GE,EXT99MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
