#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT99L.ksh                       --- VERSION DU 08/10/2016 12:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLEXT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/08/14 AT 15.40.17 BY BURTECB                      
#    STANDARDS: P  JOBSET: EXT99L                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ : RSGA09L RSGA11L RSGA12L RSGA29L              
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  ET UTILISER LA CHAINE DE RECOVER DB2 DB2RBD AVEC CE RBA EN PREP             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EXT99LA
       ;;
(EXT99LA)
#
#EXT99LAA
#EXT99LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT99LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    95/08/14 AT 15.40.17 BY BURTECB                  
# *    JOBSET INFORMATION:    NAME...: EXT99L                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'MAJ CODES ETATS'                       
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EXT99LAD
       ;;
(EXT99LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES CODES ETATS VENANT DE EXT99P                             
       m_FileAssign -d SHR -g +0 FEX998 ${DATA}/PXX0/F07.BEX998AP
#                                                                              
# ******  CODE MARKETING                                                       
#    RSGA09L  : NAME=RSGA09L,MODE=U - DYNAM=YES                                
# -X-RSGA09L  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA09L /dev/null
# ******  RELATION ETAT FAMILLE/RAYON                                          
#    RSGA11L  : NAME=RSGA11L,MODE=U - DYNAM=YES                                
# -X-RSGA11L  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA11L /dev/null
# ******  RELATION FAMILLE/CODE MARKETING                                      
#    RSGA12L  : NAME=RSGA12L,MODE=U - DYNAM=YES                                
# -X-RSGA12L  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA12L /dev/null
# ******  LIBELLES DES IMBRICATIONS CODE MARKETING                             
#    RSGA29L  : NAME=RSGA29L,MODE=U - DYNAM=YES                                
# -X-RSGA29L  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA29L /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX999 
       JUMP_LABEL=EXT99LAE
       ;;
(EXT99LAE)
       m_CondExec 04,GE,EXT99LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
