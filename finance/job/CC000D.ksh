#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC000D.ksh                       --- VERSION DU 09/10/2016 00:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDCC000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/14 AT 17.43.07 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CC000D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#       *  TRI DU FICHIER RECYCLAGE NEM                                        
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC000DA
       ;;
(CC000DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CC000DAA
       ;;
(CC000DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
#                                                                              
# ******* FIC RECYCLAGE NEM                                                    
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001BD
#                                                                              
# ******* FIC EN SORTIE POUR PGM BCC000                                        
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -g +1 SORTOUT ${DATA}/PTEM/CC000DAA.BCC000AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 " "
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_20_6 20 CH 06
 /FIELDS FLD_CH_1_8 1 CH 08
 /CONDITION CND_3 FLD_CH_26_3 NE CST_1_7 
 /KEYS
   FLD_CH_1_8 ASCENDING
 /SUMMARIZE
 /INCLUDE CND_3
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_20_6,FLD_CH_1_8,FLD_CH_26_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=CC000DAB
       ;;
(CC000DAB)
       m_CondExec 00,EQ,CC000DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          TRI DU FICHIER RECYCLAGE ICS (ISSU DE LA CHAINE FTICSL)             
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CC000DAD
       ;;
(CC000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC RECYCLAGE ICS                                                    
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FTRECYDD
#                                                                              
# ******* FIC EN SORTIE POUR PGM BCC000                                        
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -g +1 SORTOUT ${DATA}/PTEM/CC000DAD.BCC000BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "NEM"
 /DERIVEDFIELD CST_3_11 "CAI"
 /FIELDS FLD_CH_1_8 1 CH 08
 /FIELDS FLD_CH_377_3 377 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_102_6 102 CH 6
 /FIELDS FLD_CH_370_3 370 CH 3
 /FIELDS FLD_CH_144_8 144 CH 8
 /CONDITION CND_3 FLD_CH_1_3 EQ CST_1_7 AND FLD_CH_370_3 EQ CST_3_11 
 /KEYS
   FLD_CH_1_8 ASCENDING
 /SUMMARIZE
 /INCLUDE CND_3
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_102_6,FLD_CH_144_8,FLD_CH_377_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=CC000DAE
       ;;
(CC000DAE)
       m_CondExec 00,EQ,CC000DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCC000 **  COBOL2/DB2 .CREATION D UN FICHIER POUR LOAD RTCC00           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CC000DAG
       ;;
(CC000DAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC DES RECYCLAGES NEM TRIE                                          
       m_FileAssign -d SHR -g ${G_A1} FRECNEM ${DATA}/PTEM/CC000DAA.BCC000AD
#                                                                              
# ******* FIC DES RECYCLAGES ICS TRIE                                          
       m_FileAssign -d SHR -g ${G_A2} FRECICS ${DATA}/PTEM/CC000DAD.BCC000BD
#                                                                              
# ******* TABLE  DB2 EN LECTURE                                                
#                                                                              
#    RSFT34   : NAME=RSFT34D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT34 /dev/null
# ******* TABLE FT                                                             
#    RSCC01   : NAME=RSCC01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSCC01 /dev/null
#    RSMQ15   : NAME=RSMQ15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#         FIC EN SORTIE POUR LOAD RTCC00 (LRECL27)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 FCAIREC ${DATA}/PTEM/CC000DAG.BCC000CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC000 
       JUMP_LABEL=CC000DAH
       ;;
(CC000DAH)
       m_CondExec 04,GE,CC000DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTCC00                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CC000DAJ
       ;;
(CC000DAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ****** FIC DE LOAD                                                           
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PTEM/CC000DAG.BCC000CD
#    RSCC00   : NAME=RSCC00D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSCC00 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CC000DAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/CC000D_CC000DAJ_RTCC00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=CC000DAK
       ;;
(CC000DAK)
       m_CondExec 04,GE,CC000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *************************                                                    
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CC000DZA
       ;;
(CC000DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CC000DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
