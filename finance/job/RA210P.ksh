#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA210P.ksh                       --- VERSION DU 17/10/2016 18:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRA210 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.10.17 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RA210P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER PROVENANT DE SIEBEL                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RA210PA
       ;;
(RA210PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=RA210PAA
       ;;
(RA210PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# ******* FIC VENANT DE LA CHAINE SIEBEL VIA GATEWAY                           
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F99.FRA210AP
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDAL ${DATA}/PXX0/F89.FRA210BM
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDIF ${DATA}/PXX0/F07.FRA210BP
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDNN ${DATA}/PXX0/F61.FRA210BL
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDO ${DATA}/PXX0/F16.FRA210BO
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDPM ${DATA}/PXX0/F91.FRA210BD
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FICDRA ${DATA}/PXX0/F45.FRA210BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "907"
 /DERIVEDFIELD CST_1_14 "916"
 /DERIVEDFIELD CST_1_20 "945"
 /DERIVEDFIELD CST_1_17 "991"
 /DERIVEDFIELD CST_1_11 "961"
 /DERIVEDFIELD CST_1_5 "989"
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_33_10 33 CH 10
 /CONDITION CND_1 FLD_CH_11_3 EQ CST_1_5 
 /CONDITION CND_2 FLD_CH_11_3 EQ CST_1_8 
 /CONDITION CND_4 FLD_CH_11_3 EQ CST_1_14 
 /CONDITION CND_6 FLD_CH_11_3 EQ CST_1_20 
 /CONDITION CND_3 FLD_CH_11_3 EQ CST_1_11 
 /CONDITION CND_5 FLD_CH_11_3 EQ CST_1_17 
 /KEYS
   FLD_CH_11_6 ASCENDING,
   FLD_CH_33_10 ASCENDING
 /MT_OUTFILE_ASG FICDAL
 /INCLUDE CND_1
 /MT_OUTFILE_ASG FICDIF
 /INCLUDE CND_2
 /MT_OUTFILE_ASG FICDNN
 /INCLUDE CND_3
 /MT_OUTFILE_ASG FICDO
 /INCLUDE CND_4
 /MT_OUTFILE_ASG FICDPM
 /INCLUDE CND_5
 /MT_OUTFILE_ASG FICDRA
 /INCLUDE CND_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RA210PAB
       ;;
(RA210PAB)
       m_CondExec 00,EQ,RA210PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER PROVENANT DE L'ECLATEMENT                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAD
       ;;
(RA210PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.FRA210BP
#         FILE  NAME=FRA2REJP,MODE=I                                           
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FRA210CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_CH_263_10 263 CH 10
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_252_7 252 CH 7
 /KEYS
   FLD_CH_11_6 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_252_7 ASCENDING,
   FLD_CH_263_10 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA210PAE
       ;;
(RA210PAE)
       m_CondExec 00,EQ,RA210PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA210 **  CREATION DES AVOIRS DE REMBOURSEMENT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAG
       ;;
(RA210PAG)
       m_CondExec ${EXAAK},NE,YES 
# ******* FIC DES AVOIRS + REJETS TRI�S                                        
       m_FileAssign -d SHR -g ${G_A2} FRA210 ${DATA}/PXX0/F07.FRA210CP
# ******* TABLE EN LECTURE                                                     
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGQ96   : NAME=RSGQ96,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ96 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTRX00   : NAME=RSRX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRX00 /dev/null
# ******* TABLE EN ECRITURE                                                    
#    RTRA00   : NAME=RSRA00P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA00 /dev/null
#    RTRA01   : NAME=RSRA01P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA01 /dev/null
#        FDATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES DEMANDES DE REMBOURSEMENTS D'AVOIR _A REPRENDRE               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FREJETS ${DATA}/PXX0/F07.FRA2REJP
# ******* CET �TAT EST ENVOY� SOUS EOS DANS LA BOITE DFO                       
# IRA210   REPORT SYSOUT=(9,IRA210),RECFM=FBA,LRECL=133,BLKSIZE=0              
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IRA210 ${DATA}/PXX0/F07.FRA210DP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA210 
       JUMP_LABEL=RA210PAH
       ;;
(RA210PAH)
       m_CondExec 04,GE,RA210PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO DES FICHIERS FRA210AP QUOTIDIEN DANS UN FICHIER HISTO                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAJ
       ;;
(RA210PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ------------- REPRO FIC SIEBEL DANS FIC HISTO                                
       m_FileAssign -d SHR IN1 ${DATA}/PXX0/FTP.F99.FRA210AP
# ------------- FICHIER HISTORIQUE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.HISTO.FRA210AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210PAK
       ;;
(RA210PAK)
       m_CondExec 16,NE,RA210PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  DELETE DU FICHIER CUMUL DU MOIS                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAM
       ;;
(RA210PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210PAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210PAN
       ;;
(RA210PAN)
       m_CondExec 16,NE,RA210PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE _A Z�RO DU FICHIER DE REJET + FIC VENANT DE SIEBEL            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAQ
       ;;
(RA210PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
# ******  FIC AVANT ET APRES ECALTEMENT REMIS _A Z�RO                           
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g ${G_A3} OUT1 ${DATA}/PXX0/F07.FRA210BP
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/FTP.F99.FRA210AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210PAQ.sysin
       m_UtilityExec
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=RA210XXX,                                                           
#      FNAME=":FRA210DP""(0),                                                  
#      NFNAME=RA210P                                                           
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210PAR
       ;;
(RA210PAR)
       m_CondExec 16,NE,RA210PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTRA210P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAT
       ;;
(RA210PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210PAT.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RA210PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RA210PAX
       ;;
(RA210PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210PAX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
