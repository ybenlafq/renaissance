#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG14FP.ksh                       --- VERSION DU 08/10/2016 22:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG14F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/08 AT 15.27.34 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG14FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSRC10FP RSRC20 RSFG02 AVANT MAJ                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG14FPA
       ;;
(FG14FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG14FPAA
       ;;
(FG14FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QFG14FP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG14FPAA
       m_ProgramExec IEFBR14 "RMAC,FG14FP.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FG14FPAD
       ;;
(FG14FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QFG14FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG14FPAE
       ;;
(FG14FPAE)
       m_CondExec 00,EQ,FG14FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG140 :  PREPARATION DES ECRITURES COMPTABLES COSYS                        
#            +++         DES ECRITURES POUR POSTING G.L PARIS ET METZ          
#            +++         DES ECRITURES COMPTA DANS GCV                         
#            +++         DES ECRITURES COMPTA DANS GCT                         
#    REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME ET BACKOUT=JOBSET         
#    ==> VERIFIER LE BACKOUT FG14FPR1: RECOVER TO RBA DE RSRC10FP RSRC         
#                                      RSFG02                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG14FPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG14FPAG
       ;;
(FG14FPAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES CORRESPONDANCES PERIODES                                   
#    RSFG11   : NAME=RSFG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG11 /dev/null
# ******* TABLE DE CONTROLE DES TRANSFERT APPC                                 
#    RSRC10FP : NAME=RSRC10FP,MODE=U - DYNAM=YES                               
# -X-FG14FPR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSRC10FP /dev/null
# ******  TABLE APPC DES EDITIONS COMPTABLES                                   
#    RSRC20   : NAME=RSRC20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRC20 /dev/null
# ******* TABLE COMPTABLE GROUPE                                               
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES ECRITURES COMPTABLES POUR COSYS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 FCOSYS ${DATA}/PXX0/F07.BFG140AP
# ******* FICHIERS  GCT TOUTES FILIALES                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT907 ${DATA}/PXX0/F07.FG14FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT989 ${DATA}/PXX0/F89.FG14FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT945 ${DATA}/PXX0/F45.FG14FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT961 ${DATA}/PXX0/F61.FG14FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT991 ${DATA}/PXX0/F91.FG14FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT996 ${DATA}/PXX0/F96.FG14FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT997 ${DATA}/PXX0/F44.FG14FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT916 ${DATA}/PXX0/F16.FG14FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT908 ${DATA}/PXX0/F08.FG14FPX
       m_FileAssign -d SHR FGCT975 /dev/null
       m_FileAssign -d SHR FGCT994 /dev/null
#                                                                              
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FG14FP
# ********* FSOCSIMU = SOCIETE/ENTITE GL A SIMULER                             
       m_FileAssign -d SHR FSOCSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FG14FP2
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG140 
       JUMP_LABEL=FG14FPAH
       ;;
(FG14FPAH)
       m_CondExec 04,GE,FG14FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG115 : GENERATION D'UN FICHIER A DESTINATION D'HADOOP                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG14FPAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG14FPAJ
       ;;
(FG14FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSFI10   : NAME=RSFG02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 CSVFG02 ${DATA}/PXX0/F07.BFG115AP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG115 
       JUMP_LABEL=FG14FPAK
       ;;
(FG14FPAK)
       m_CondExec 04,GE,FG14FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTFG14FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG14FPAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG14FPAM
       ;;
(FG14FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG14FPAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FG14FPAM.FTFG14FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFG14FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG14FPAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG14FPAQ
       ;;
(FG14FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FG14FPAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FG14FPAM.FTFG14FP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG14FPZA
       ;;
(FG14FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG14FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
