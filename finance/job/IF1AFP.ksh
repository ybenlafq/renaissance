#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IF1AFP.ksh                       --- VERSION DU 09/10/2016 05:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIF1AF -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/04 AT 23.48.42 BY BURTEC2                      
#    STANDARDS: P  JOBSET: IF1AFP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  MODIF LE 17.03.03 CCA : AJOUT FILIALE LUXEMBOURG                            
#  MODIF LE 05.08.15 FAC ET MANU AJOUT COPY BIFDTYAP DANS BIF001AP             
# ********************************************************************         
#  MODIF GUFF LE 21.05.2015 AJOUT FIC BIFJMDAP                                 
# ********************************************************************         
#                                                                              
#    CUMUL DES MOUVEMENTS POUR CETELEM, SUITE PASSAGE EN GDG ET                
#          SUPPRESSION DU CUMUL DANS IF99FP.                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IF1AFPA
       ;;
(IF1AFPA)
#
#IF1AFPAJ
#IF1AFPAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#IF1AFPAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/11/04 AT 23.48.41 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: IF1AFP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-IF1AFP'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IF1AFPAA
       ;;
(IF1AFPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/FTP.F07.CETELEM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CETREPRI
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PXX0/FTP.F07.SOFINCO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SOFREPRI
       m_FileAssign -d SHR -g +0 IN3 ${DATA}/PXX0/FTP.F07.CAR907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CARREPRI
       m_FileAssign -d SHR -g +0 IN4 ${DATA}/PXX0/FTP.F07.AME907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.AMEREPRI
       m_FileAssign -d SHR -g +0 IN5 ${DATA}/PXX0/F07.TLV907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.TELREPRI
       m_FileAssign -d SHR -g +0 IN6 ${DATA}/PXX0/FTP.F07.PROMOSTP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.PROREPRI
       m_FileAssign -d SHR -g +0 IN7 ${DATA}/PXX0/FTP.F07.CETREL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CTRREPRI
       m_FileAssign -d SHR -g +0 IN8 ${DATA}/PXX0/F07.SAFIGBNP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SFIBNPR
       m_FileAssign -d SHR -g +0 IN9 ${DATA}/PXX0/FTP.F07.EXTLIBNP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.EXTBNPR
       m_FileAssign -d SHR -g +0 IN10 ${DATA}/PXX0/F07.SAFIGLCL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SFILCLR
       m_FileAssign -d SHR -g +0 IN11 ${DATA}/PXX0/FTP.F07.PAYPAL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.PAYREPRI
       m_FileAssign -d SHR -g +0 IN12 ${DATA}/PXX0/FTP.F07.BIFJRBAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.BIFJMDAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.JRBREPRI
       m_FileAssign -d SHR -g +0 IN13 ${DATA}/PXX0/FTP.F07.BIFJMPAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.JMPREPRI
       m_FileAssign -d SHR -g +0 IN14 ${DATA}/PXX0/FTP.F07.FEVOLLOA
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FLOAREPR
# ******* FICHIERS SORTIE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.CETREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F07.SOFREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F07.CARREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F07.AMEREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F07.TELREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/F07.PROREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F07.CTRREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT8 ${DATA}/PXX0/F07.SFIBNPR
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT9 ${DATA}/PXX0/F07.EXTBNPR
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/F07.SFILCLR
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/F07.PAYREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F07.JRBREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/F07.JMPREPRI
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/F07.FLOAREPR
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF1AFPAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF1AFPAB
       ;;
(IF1AFPAB)
       m_CondExec 16,NE,IF1AFPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    CREATION A VIDE D UNE GENERATION POUR FIC GCT                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF1AFPAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IF1AFPAD
       ;;
(IF1AFPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
# *****   RAZ FICHIERS FINANCEMENTS MICRO ****                                 
# *****   ATTENTION FICHIERS SORTIE EN MODE=I(PAS DE GDG)                      
# *****   SAUF CETELEM / SOFINCO                                               
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
# *****   FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F45.IF00FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F89.IF00FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F07.IF00FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F61.IF00FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F91.IF00FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/F96.IF00FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F44.IF00FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT8 ${DATA}/PXX0/F16.IF00FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT9 ${DATA}/PXX0/F08.IF00FPX
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/FTP.F07.SOFINCO
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/FTP.F07.CETELEM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/FTP.F07.CAR907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/FTP.F07.AME907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/F07.TLV907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/FTP.F07.PROMOSTP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT16 ${DATA}/PXX0/FTP.F07.CETREL
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT17 ${DATA}/PXX0/F07.SAFIGBNP
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/FTP.F07.EXTLIBNP
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT19 ${DATA}/PXX0/F07.SAFIGLCL
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT20 ${DATA}/PXX0/FTP.F07.PAYPAL
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT21 ${DATA}/PXX0/FTP.F07.BIFJRBAP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT22 ${DATA}/PXX0/FTP.F07.BIFJMPAP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT23 ${DATA}/PXX0/FTP.F07.BIFJMDAP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT24 ${DATA}/PXX0/F07.FLOADAR
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF1AFPAD.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF1AFPAE
       ;;
(IF1AFPAE)
       m_CondExec 16,NE,IF1AFPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF1AFPAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
