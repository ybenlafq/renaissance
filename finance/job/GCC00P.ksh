#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GCC00P.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGCC00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/20 AT 14.49.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: GCC00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  DELETE DU FICHER IPC002CP                                                   
#  REPRISE OUI                                                                 
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GCC00PA
       ;;
(GCC00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       RUN=${RUN}
       JUMP_LABEL=GCC00PAA
       ;;
(GCC00PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GCC00PAA.sysin
       m_UtilityExec
#                                                                              
#  BPC002 : CREATION FICHIER ICS                                               
#           CREATION ETAT JUSTIFCATIF CARTES PERIMEES VERS EOS                 
#                                                                              
#  REPRISE : NON                                                               
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAD
       ;;
(GCC00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN M.A.J                                                      
#    RSPC01   : NAME=RSPC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
# ******  FICHIER ICS                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS ${DATA}/PXX0/F07.BPC002AP
# ******  FICHIER ICS1                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS1 ${DATA}/PXX0/F07.BPC002BP
# ******  FICHIER FFMICRO                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FFMICRO ${DATA}/PXX0/F07.BPC002DP
#         FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  EDITION ETAT JUSTIFICATIF CARTES PERIMEES                            
#  IPC002   REPORT SYSOUT=(9,IPC002)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ IPC002 ${DATA}/PXX0.F07.IPC002AP
#  IPC002A  REPORT SYSOUT=(9,IPC002A)                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ IPC002A ${DATA}/PXX0.F07.IPC002BP
#                                                                              
#  EDITION IPC002                                                              
#                                                                              
#  REPRISE : OUI                                                               
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAG PGM=IEBGENER   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BPC002 
       JUMP_LABEL=GCC00PAG
       ;;
(GCC00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.F07.IPC002AP
       m_OutputAssign -c 9 -w IPC002 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
#  EDITION IPC002A                                                             
#                                                                              
#  REPRISE : OUI                                                               
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAJ PGM=IEBGENER   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAJ
       ;;
(GCC00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.F07.IPC002BP
       m_OutputAssign -c 9 -w IPC002A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
#  ZIP DES FICHERS IPC002AP ET IPC002BP                                        
#                                                                              
# AAU      STEP  PGM=PKZIP,RSTRT=SAME                                          
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    DATA  *,MBR=GCC00P1                                                 
# -ECHO                                                                        
# -NOSYSIN                                                                     
# -ACTION(ADD)                                                                 
# -TRAN(ASCII850)                                                              
# -ARCHIVE_SPACE_PRIMARY(00002000)                                             
# -ARCHIVE_SPACE_SECONDARY(00002000)                                           
# -ARCHIVE(PXX0.F99.IPC002CP)                                                  
# -ZIPPED_DSN(PXX0.F07.IPC002AP,IPC002AP.TXT)                                  
# -ZIPPED_DSN(PXX0.F07.IPC002BP,IPC002BP.TXT)                                  
# PXX0.F07.IPC002AP                                                            
# PXX0.F07.IPC002BP                                                            
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GCC00PAK
       ;;
(GCC00PAK)
       m_CondExec 00,EQ,GCC00PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAM PGM=JVMLDM76   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAM
       ;;
(GCC00PAM)
       m_CondExec ${EXAAU},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ FICZIP ${DATA}/PXX0.F99.IPC002CP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GCC00P1.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
#                                                                              
#  TRANSFERT VERS (GATEWAY) XFBPRO FICHIER IPC002CP                            
#  REPRISE : OUI                                                               
#                                                                              
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT,RSTRT=SAME                            
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=IPC002,                                                             
#      FNAME=":IPC002CP"",                                                     
#      NFNAME=IPC002.ZIP                                                       
#          DATAEND                                                             
#                                                                              
#  DELETE DES FICHERS IPC002AP ET IPC002BP                                     
#  REPRISE OUI                                                                 
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAQ
       ;;
(GCC00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GCC00PAQ.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GCC00PAR
       ;;
(GCC00PAR)
       m_CondExec 16,NE,GCC00PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTGCC00P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAT
       ;;
(GCC00PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GCC00PAT.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GCC00PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GCC00PAX
       ;;
(GCC00PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GCC00PAX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
