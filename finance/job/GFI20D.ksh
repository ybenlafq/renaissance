#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFI20D.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGFI20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/09 AT 14.09.17 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GFI20D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DE LA TABLE RTFI10 AVANT MISE A JOUR                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFI20DA
       ;;
(GFI20DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFI20DAA
       ;;
(GFI20DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGFI20D
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GFI20DAA
       m_ProgramExec IEFBR14 "RDAR,GFI20D.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GFI20DAD
       ;;
(GFI20DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGFI20D
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GFI20DAE
       ;;
(GFI20DAE)
       m_CondExec 00,EQ,GFI20DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           COPY DE LA DATE DE DERNIER PASSAGE DE LA CHAINE                    
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAG
       ;;
(GFI20DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F91.DATEPFID
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.BFI015DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20DAH
       ;;
(GFI20DAH)
       m_CondExec 00,EQ,GFI20DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI005 : EXTRACTION DES MOUVEMENTS DE STOCK A FACTURER                      
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAJ
       ;;
(GFI20DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  MVTS DE STOCKS                                                       
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS40D  : NAME=RSGS40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS40D /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE DU DERNIER MVT TRAITE + DERNIER N� FACTURE                      
       m_FileAssign -d SHR -g ${G_A2} FDATSEQ ${DATA}/PEX0/F91.BFI015DD
#                                                                              
# ******  FICHIER DES MUTATIONS SELECTIONNEES POUR FACTURATION                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFI005 ${DATA}/PTEM/GFI20DAJ.BFI005AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI005 
       JUMP_LABEL=GFI20DAK
       ;;
(GFI20DAK)
       m_CondExec 04,GE,GFI20DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI010 : TRAITEMENT DES MVTS DE STOCK A FACTURER                            
# ********************************************************************         
#  LES ERREURS SONT EN PRINCIPE ECRITES DANS LA TABLE RTAN00                   
#  SPUFFI RDAR : SELECT LIBERR FROM RTAN00 WHERE CNOMPGRM = 'BFI010'           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAM
       ;;
(GFI20DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  PARAMETRES FAMILLES                                                  
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  MVTS DE STOCKS                                                       
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  PRMP + PCF                                                           
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  TABLE COMMANDES FOURNISSEURS                                         
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
# ******  HISTO DES PRMPS                                                      
#    RSGG70   : NAME=RSGG70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
# ******  CMDES EN ATTENTE DE RECEPTION                                        
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
# ******  DETAIL DES MUTATIONS                                                 
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******  CALENDRIER DES MUTATIONS                                             
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  HISTO DES FACTURES INTERNES                                          
#    RSFI10   : NAME=RSFI10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# ******  FICHIER DES MUTATIONS SELECTIONNEES POUR FACTURATION                 
       m_FileAssign -d SHR -g ${G_A3} FFI005 ${DATA}/PTEM/GFI20DAJ.BFI005AD
#                                                                              
# ******  FICHIER DES MUTATIONS A FACTURER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFI010 ${DATA}/PTEM/GFI20DAM.BFI010AD
# ******  FICHIER DES MUTATIONS A EDITER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FFI011 ${DATA}/PTEM/GFI20DAM.BFI010CD
# ******  FACTURES D EXCEPTION (NE SERT PAS A MARSEILLE)                       
       m_FileAssign -d SHR FFI013 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE DU DERNIER MVT TRAITE + DERNIER N� FACTURE                      
       m_FileAssign -d SHR -g ${G_A4} FDATSEQ ${DATA}/PEX0/F91.BFI015DD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI010 
       JUMP_LABEL=GFI20DAN
       ;;
(GFI20DAN)
       m_CondExec 04,GE,GFI20DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        TRI DU FICHIER DES MOUVEMENTS A FACTURER                              
# ********************************************************************         
#    NSOC + NLIEU : 9,6      CTYPE   : 16,5      CSENS : 15,1                  
#            NDOC : 21,7     WSEQFAM : 35,3      CODIC : 28,7                  
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAQ
       ;;
(GFI20DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GFI20DAM.BFI010AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GFI20DAQ.BFI010BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_7 21 CH 7
 /FIELDS FLD_CH_35_3 35 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_15_1 15 CH 1
 /FIELDS FLD_CH_28_7 28 CH 7
 /FIELDS FLD_CH_9_6 9 CH 6
 /KEYS
   FLD_CH_9_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_15_1 ASCENDING,
   FLD_CH_21_7 ASCENDING,
   FLD_CH_35_3 ASCENDING,
   FLD_CH_28_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20DAR
       ;;
(GFI20DAR)
       m_CondExec 00,EQ,GFI20DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        TRI DU FICHIER DES MUTATIONS A EDITER                                 
# ********************************************************************         
#    CTYPSOC : 8,3     NSOC :  1,3    NLIEU : 4,3    CTYPE    : 11,5           
#     NMUT   :16,7  WSEQFAM :101,3    NCODIC:44,7    WSENSMVT : 37,1           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAT
       ;;
(GFI20DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GFI20DAM.BFI010CD
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GFI20DAT.BFI010DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_101_3 101 CH 3
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /KEYS
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_16_7 ASCENDING,
   FLD_CH_101_3 ASCENDING,
   FLD_CH_44_7 ASCENDING,
   FLD_CH_37_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20DAU
       ;;
(GFI20DAU)
       m_CondExec 00,EQ,GFI20DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI015 : EDITION DES FACTURES ET MAJ DE L'HISTORIQUE (RTFI10)               
# ********************************************************************         
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DAX
       ;;
(GFI20DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  PARAMETRES FAMILLE                                                   
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  MESSAGES D'ANOMALIES                                                 
#    RSGA99   : NAME=RSGA99D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA99 /dev/null
#                                                                              
# ******  HISTORIQUES FACTURATION                                              
#    RSFI10   : NAME=RSFI10D,MODE=U - DYNAM=YES                                
# -X-GFI20DR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSFI10 /dev/null
#                                                                              
# ******  SOCIETE TRAITEE = 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MAJ DATE DU DERNIER MVT TRAITE + DERNIER N� FACTURE                  
       m_FileAssign -d OLD,KEEP,DELETE -g ${G_A7} FDATSEQ ${DATA}/PEX0/F91.BFI015DD
#                                                                              
# ******  PARAMETRE DU N� DE TELEX A EDITER SUR FACTURE                        
#         EDITION TELEX=O ,PAS D'EDITION=N OU BLANC                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GFI20DAX
#                                                                              
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d SHR -g ${G_A8} FFI010 ${DATA}/PTEM/GFI20DAQ.BFI010BD
#                                                                              
# ******  FICHIER DES FACTURES DESTINE A RTFG01 (F.I.S)                        
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFG01 ${DATA}/PTEM/GFI20DAX.FFG01AD
#                                                                              
# ******  EDITION DES FACTURES ET DES AVOIRS DE MATERIEL                       
       m_OutputAssign -c 9 -w IFI015 IFI015
# ******  EDITION DES FACTURES ET DES AVOIRS DE FRAIS DE GESTION               
       m_OutputAssign -c 9 -w IFI016 IFI016
# ******  EDITION DES FACTURES ET DES AVOIRS DE FRAIS DE GESTION               
# FPRMP    REPORT SYSOUT=(9,IFI017),                                           
#               RECFM=FBA,BLKSIZE=133,LRECL=133,SPIN=UNALLOC                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI015 
       JUMP_LABEL=GFI20DAY
       ;;
(GFI20DAY)
       m_CondExec 04,GE,GFI20DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    ALIMENTATION DU FICHIER CUMUL DES FACTURES INTER SOCIETES                 
#                 DESTINES A LA TABLE RTFG01 (RMAC)                            
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBA
       ;;
(GFI20DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FACTURES DU JOUR                                                     
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GFI20DAX.FFG01AD
# *****   FICHIER DES FACTURES INTER SOCIETES                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FFG01BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_244_7 244 PD 7
 /FIELDS FLD_PD_83_7 83 PD 7
 /FIELDS FLD_PD_76_7 76 PD 7
 /FIELDS FLD_CH_62_14 62 CH 14
 /FIELDS FLD_CH_93_24 93 CH 24
 /FIELDS FLD_PD_186_7 186 PD 7
 /FIELDS FLD_PD_55_7 55 PD 7
 /FIELDS FLD_PD_90_3 90 PD 3
 /KEYS
   FLD_CH_1_39 ASCENDING,
   FLD_CH_62_14 ASCENDING,
   FLD_CH_93_24 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_55_7,
    TOTAL FLD_PD_76_7,
    TOTAL FLD_PD_83_7,
    TOTAL FLD_PD_90_3,
    TOTAL FLD_PD_186_7,
    TOTAL FLD_PD_244_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20DBB
       ;;
(GFI20DBB)
       m_CondExec 00,EQ,GFI20DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI020 : EDITION DES LIGNES DE MUTATIONS FACTUREES                          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBD
       ;;
(GFI20DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d SHR -g ${G_A10} FFI011 ${DATA}/PTEM/GFI20DAT.BFI010DD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES LIGNES DE MUTATIONS FACTUREES                            
       m_OutputAssign -c 9 -w IFI020 IFI020
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI020 
       JUMP_LABEL=GFI20DBE
       ;;
(GFI20DBE)
       m_CondExec 04,GE,GFI20DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI022 : GENERATION D'UN FICHIER A DESTINATION D'HADOOP                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBG
       ;;
(GFI20DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSFI10   : NAME=RSFI10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 CSVFI10 ${DATA}/PXX0/F91.BFI022AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g +0 FDATSEQ ${DATA}/PEX0/F91.DATEPFID
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI022 
       JUMP_LABEL=GFI20DBH
       ;;
(GFI20DBH)
       m_CondExec 04,GE,GFI20DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTGFI20D                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBJ
       ;;
(GFI20DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20DBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GFI20DBJ.FTGFI20D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGFI20D                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBM
       ;;
(GFI20DBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20DBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GFI20DBJ.FTGFI20D(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#     ECRITURE DE LA DATE DE PASSAGE DE LA CHAINE                              
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20DBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DBQ
       ;;
(GFI20DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PEX0/F91.BFI015DD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.DATEPFID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20DBR
       ;;
(GFI20DBR)
       m_CondExec 00,EQ,GFI20DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GFI20DZA
       ;;
(GFI20DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
