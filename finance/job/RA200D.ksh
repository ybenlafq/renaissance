#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA200D.ksh                       --- VERSION DU 08/10/2016 22:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDRA200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/12/07 AT 14.29.35 BY PREPA2                       
#    STANDARDS: P  JOBSET: RA200D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER PROVENANT DE LA CHAINE AV600D                                
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RA200DA
       ;;
(RA200DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RA200DAA
       ;;
(RA200DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# ******* FIC VENANT DE LA CHAINE AV600D                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BAV600AD
# ******* FIC DE RECYCLADE EN CAS DE PLANTAGE LA VEILLE (RA200D)               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.RA20RECD
# ******* FIC REJET DE LA VEILLE (RA200D)                                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.RA20REJD
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 SORTOUT ${DATA}/PTEM/RA200DAA.BRA200AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /KEYS
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_17_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA200DAB
       ;;
(RA200DAB)
       m_CondExec 00,EQ,RA200DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA200 **  COBOL2/DB2  CREATION DES AVOIRS DE REMBOURSEMENT             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA200DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA200DAD
       ;;
(RA200DAD)
       m_CondExec ${EXAAF},NE,YES 
# ******* FIC DES AVOIRS + REJETS TRI�S                                        
       m_FileAssign -d SHR -g ${G_A1} FRA200 ${DATA}/PTEM/RA200DAA.BRA200AD
# ******* TABLE GA                                                             
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FT                                                             
#    RSGQ96   : NAME=RSGQ96D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ96 /dev/null
#    RSRA00   : NAME=RSRA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
#    RSLI00   : NAME=RSLI00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSRA00   : NAME=RSRA00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
#    RSRA01   : NAME=RSRA01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRA01 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES DEMANDES DE REMBOURSEMENTS D'AVOIR _A REPRENDRE               
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 FREJETS ${DATA}/PXX0/F91.RA20REJD
# ******* CET �TAT EST ENVOY� SOUS EOS DANS LA BOITE DFO,IE01008,ALSD          
       m_OutputAssign -c 9 -w IRA200 IRA200
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA200 
       JUMP_LABEL=RA200DAE
       ;;
(RA200DAE)
       m_CondExec 04,GE,RA200DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE _A Z�RO DU FICHIER DE RECYCLAGE                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA200DAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA200DAG
       ;;
(RA200DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
# ******  FIC DE RECYCLAGE REMIS _A Z�RO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 OUT1 ${DATA}/PXX0/F91.RA20RECD
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 OUT2 ${DATA}/PXX0/F91.BAV600AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA200DAG.sysin
       m_UtilityExec
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA200DAH
       ;;
(RA200DAH)
       m_CondExec 16,NE,RA200DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RA200DZA
       ;;
(RA200DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA200DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
