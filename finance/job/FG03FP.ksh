#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG03FP.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG03F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.37.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG03FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  QUIESCE DES TABLES RTFG01 ET RTFG05 MAJ DANS LE PGM BFG150                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG03FPA
       ;;
(FG03FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG03FPAA
       ;;
(FG03FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QFG03FP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG03FPAA
       m_ProgramExec IEFBR14 "RMAC,FG03FP.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FG03FPAD
       ;;
(FG03FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA                                   
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QFG03FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG03FPAE
       ;;
(FG03FPAE)
       m_CondExec 00,EQ,FG03FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG150                                                                
#  ------------                                                                
#  CALCUL DE LA RENUMERATION                                                   
# ********************************************************************         
#  REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAG
       ;;
(FG03FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
# -X-FG03FPR0 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSFG01 /dev/null
#    RSFG05   : NAME=RSFG05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG05 /dev/null
#    RSFG02   : NAME=RSFG02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
# ------  DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ENTRANT DANS LE BFG260                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFGTAB ${DATA}/PTEM/FG03FPAG.BFG260AP
# ------  FICHIER ENTRANT DANS LE BFG261                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FFGJRN ${DATA}/PTEM/FG03FPAG.BFG261AP
# ------  FICHIER ENTRANT DANS LE BFG262                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FFGCOMP ${DATA}/PTEM/FG03FPAG.BFG262AP
# ------  COMPTE-RENDU D'EXECUTION IFG150                                      
       m_OutputAssign -c 9 -w IFG150 IFG150
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG150 
       JUMP_LABEL=FG03FPAH
       ;;
(FG03FPAH)
       m_CondExec 04,GE,FG03FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG260AP                                                     
# ********************************************************************         
#  REPRISE : EN TETE DE CHAINE APRES BACKOUT DU JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAJ
       ;;
(FG03FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FG03FPAG.BFG260AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPAJ.BFG260BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_1 59 CH 1
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_59_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPAK
       ;;
(FG03FPAK)
       m_CondExec 00,EQ,FG03FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG260                                                                
#  ------------                                                                
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAM
       ;;
(FG03FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A3} FFGTAB ${DATA}/PTEM/FG03FPAJ.BFG260BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION                      
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG03FPAM.BFG260CP
# ------  EDITION DE CONTROLE                                                  
       m_OutputAssign -c 9 -w IFG260 IFG260
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG260 
       JUMP_LABEL=FG03FPAN
       ;;
(FG03FPAN)
       m_CondExec 04,GE,FG03FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAQ
       ;;
(FG03FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/FG03FPAM.BFG260CP
# ******* EDITION DU TABLEAU DE REMUNERATION                                   
       m_OutputAssign -c 9 -w JFG012A SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG03FPAR
       ;;
(FG03FPAR)
       m_CondExec 00,EQ,FG03FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG260CP POUR PGM BFG230                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAT
       ;;
(FG03FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FG03FPAM.BFG260CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPAT.BFG260DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPAU
       ;;
(FG03FPAU)
       m_CondExec 00,EQ,FG03FPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG261AP                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPAX
       ;;
(FG03FPAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FG03FPAG.BFG261AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPAX.BFG261BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_56_1 56 CH 1
 /FIELDS FLD_CH_18_8 18 CH 08
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_18_8 DESCENDING,
   FLD_CH_56_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPAY
       ;;
(FG03FPAY)
       m_CondExec 00,EQ,FG03FPAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG261                                                                
#  ------------                                                                
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBA
       ;;
(FG03FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A7} FFGJRN ${DATA}/PTEM/FG03FPAX.BFG261BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION                      
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG03FPBA.BFG261CP
# ------  EDITION DE CONTROLE                                                  
       m_OutputAssign -c 9 -w IFG261 IFG261
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG261 
       JUMP_LABEL=FG03FPBB
       ;;
(FG03FPBB)
       m_CondExec 04,GE,FG03FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBD
       ;;
(FG03FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FG03FPBA.BFG261CP
# ******* JOURNAL DE REMUNERATION                                              
       m_OutputAssign -c 9 -w JFG012B SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG03FPBE
       ;;
(FG03FPBE)
       m_CondExec 00,EQ,FG03FPBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG261CP POUR PGM BFG230                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBG
       ;;
(FG03FPBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FG03FPBA.BFG261CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPBG.BFG261DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPBH
       ;;
(FG03FPBH)
       m_CondExec 00,EQ,FG03FPBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG262AP                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBJ
       ;;
(FG03FPBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/FG03FPAG.BFG262AP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPBJ.BFG262BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_62_1 62 CH 1
 /FIELDS FLD_CH_24_8 24 CH 08
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_8 DESCENDING,
   FLD_CH_62_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPBK
       ;;
(FG03FPBK)
       m_CondExec 00,EQ,FG03FPBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG262                                                                
#  ------------                                                                
#  PREPARATION POUR EDITION DU TABLEAU DE RENUMERATION                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBM
       ;;
(FG03FPBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A11} FFGCOMP ${DATA}/PTEM/FG03FPBJ.BFG262BP
# ------  FICHIER POUR CREATION ETAT DANS LA BASE EDITION                      
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG03FPBM.BFG262CP
# ------  EDITION DE CONTROLE                                                  
       m_OutputAssign -c 9 -w IFG262 IFG262
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG262 
       JUMP_LABEL=FG03FPBN
       ;;
(FG03FPBN)
       m_CondExec 04,GE,FG03FPBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR EDITION A PARIS DANS DISPATCH                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBQ
       ;;
(FG03FPBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FG03FPBM.BFG262CP
# ******* RAPPROCHEMENT COMPTABLE                                              
       m_OutputAssign -c 9 -w JFG012C SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG03FPBR
       ;;
(FG03FPBR)
       m_CondExec 00,EQ,FG03FPBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFG262CP POUR PGM BFG230                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBT
       ;;
(FG03FPBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/FG03FPBM.BFG262CP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG03FPBT.BFG262DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG03FPBU
       ;;
(FG03FPBU)
       m_CondExec 00,EQ,FG03FPBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFG230             ETAT IFG260                                        
#  ------------             -----------                                        
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPBX PGM=DFSRRC00   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPBX
       ;;
(FG03FPBX)
       m_CondExec ${EXACX},NE,YES 
# ACX     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG260RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG260RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A14} FFGEDG ${DATA}/PTEM/FG03FPAT.BFG260DP
# ------  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR1)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#  PGM : BFG230             ETAT IFG261                                        
#  ------------             -----------                                        
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPCA PGM=DFSRRC00   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPCA
       ;;
(FG03FPCA)
       m_CondExec ${EXADC},NE,YES 
# ADC     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG261RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG261RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A15} FFGEDG ${DATA}/PTEM/FG03FPBG.BFG261DP
# ------  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR2)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#  PGM : BFG230             ETAT IFG262                                        
#  ------------             -----------                                        
#  CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISEE                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG03FPCD PGM=DFSRRC00   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPCD
       ;;
(FG03FPCD)
       m_CondExec ${EXADH},NE,YES 
# ADH     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG262RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG262RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ------  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A16} FFGEDG ${DATA}/PTEM/FG03FPBT.BFG262DP
# ------  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG03FPR3)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG03FPZA
       ;;
(FG03FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG03FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
