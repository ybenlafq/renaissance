#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPS00P.ksh                       --- VERSION DU 08/10/2016 13:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPPS00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/12/31 AT 10.45.11 BY BURTECA                      
#    STANDARDS: P  JOBSET: PPS00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  UNLOAD DES DONNEES PSE       (RTPS00 : PSE )                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PPS00PA
       ;;
(PPS00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2012/12/31 AT 10.45.11 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: PPS00P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'PURGE PSE'                             
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PPS00PAA
       ;;
(PPS00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/PPS00PAA.UNPS00P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PPS00PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  PGM : BPS300                                                                
#  ------------                                                                
#  PURGE DU FICHIER SEQUENTIEL A PARTIR DE FPARAM ==> NB MOIS A EPURER         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PPS00PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PPS00PAD
       ;;
(PPS00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN ENTREE                                                     
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
# ------  FICHIER D UNLOAD                                                     
       m_FileAssign -d SHR -g ${G_A1} FPS00 ${DATA}/PTEM/PPS00PAA.UNPS00P
# ------  FICHIER PARAM (NB MOIS A EPURER)                                     
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PPS00PAD
# ------  FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ------  FICHIER D UNLOAD                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 FPS00L ${DATA}/PXX0/F07.PS00UNP
# ------  FICHIER DES MVTS PSE EPURES                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 FPS00E ${DATA}/PXX0/F07.BPS300AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS300 
       JUMP_LABEL=PPS00PAE
       ;;
(PPS00PAE)
       m_CondExec 04,GE,PPS00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR LE CODE INSEE                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PPS00PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PPS00PAG
       ;;
(PPS00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER POUR LOAD RTPS00                                              
       m_FileAssign -d OLD -g ${G_A2} SORTIN ${DATA}/PXX0/F07.PS00UNP
# ****** FICHIER TRI� SUR CODE INSEE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.PS00LOP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_5 4 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_CH_20_7 20 CH 7
 /KEYS
   FLD_CH_4_5 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_20_7 ASCENDING,
   FLD_CH_17_3 ASCENDING,
   FLD_CH_1_3 ASCENDING
 /* Record Type = F  Record Length = 128 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PPS00PAH
       ;;
(PPS00PAH)
       m_CondExec 00,EQ,PPS00PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTPS00                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#  SI PLANTAGE DANS CETTE ETAPE IL FAUT ABSOLUMENT LA RELANCER POUR            
#      RECHARGER LA TABLE RTPS00 QUI EST PRIS DANS LES VENTES EN GV00          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PPS00PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PPS00PAJ
       ;;
(PPS00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTPS00                                                
       m_FileAssign -d OLD -g ${G_A3} SYSREC ${DATA}/PXX0/F07.PS00LOP
#    RSPS00   : NAME=RSPS00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSPS00 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PPS00PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PPS00P_PPS00PAJ_RTPS00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PPS00PAK
       ;;
(PPS00PAK)
       m_CondExec 04,GE,PPS00PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   RUNSTATS RSPS00                                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PPS00PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PPS00PZA
       ;;
(PPS00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PPS00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
