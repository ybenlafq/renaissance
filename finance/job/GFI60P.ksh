#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFI60P.ksh                       --- VERSION DU 08/10/2016 21:58
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGFI60 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/18 AT 14.10.08 BY PREPA3                       
#    STANDARDS: P  JOBSET: GFI60P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BFI060                                                                
# ********************************************************************         
#  CONSTITUTION DE L'EXTRACTION                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFI60PA
       ;;
(GFI60PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFI60PAA
       ;;
(GFI60PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g +0 FDATSEQ ${DATA}/PXX0/F07.FFIDATAP
       m_FileAssign -d SHR -g +0 FNUMSEQ ${DATA}/PXX0/F07.FFINUMAP
       m_FileAssign -d SHR -g +0 FCPROG ${DATA}/PXX0/F07.FFCPRGAP
# ************** TABLE EN LECTURE                                              
#    RTGA00   : NAME=RSGA00,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTFI60   : NAME=RSFI60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI60 /dev/null
#    RTSU20   : NAME=RSSU20,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTSU20 /dev/null
#                                                                              
#         FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDATNEW ${DATA}/PTEM/GFI60PAA.FFIDATBP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FNUMNEW ${DATA}/PTEM/GFI60PAA.FFINUMBP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FICFI60 ${DATA}/PXX0/F07.FFI060AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI060 
       JUMP_LABEL=GFI60PAB
       ;;
(GFI60PAB)
       m_CondExec 04,GE,GFI60PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO DU FICHIER DATE + NUMSEQ + FCPROG                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI60PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GFI60PAD
       ;;
(GFI60PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A1} IN1 ${DATA}/PTEM/GFI60PAA.FFIDATBP
       m_FileAssign -d SHR -g ${G_A2} IN2 ${DATA}/PTEM/GFI60PAA.FFINUMBP
       m_FileAssign -d SHR IN3 ${DATA}/CORTEX4.P.MTXTFIX2/GFI60PAD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FFIDATAP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F07.FFINUMAP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F07.FFCPRGAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI60PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GFI60PAE
       ;;
(GFI60PAE)
       m_CondExec 16,NE,GFI60PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
