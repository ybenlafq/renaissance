#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC100D.ksh                       --- VERSION DU 09/10/2016 00:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDCC100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/09/28 AT 11.09.42 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CC100D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE                         
#  REPRISE: VERIFER BACKOUT-PLAN (RECOVER TO RBA , VERIFIER LE RBA ..)         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC100DA
       ;;
(CC100DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CC100DAA
       ;;
(CC100DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
#                                                                              
# *****   FICHIER CONTENANT LE NUMERO DE RBA                                   
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QCC100AD
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/CC100DAA
       m_ProgramExec IEFBR14 "RDAR,CC100D.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=CC100DAD
       ;;
(CC100DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# *****   FICHIER CONTENANT LE NUMERO DE RBA                                   
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QCC100AD
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=CC100DAE
       ;;
(CC100DAE)
       m_CondExec 00,EQ,CC100DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCC100                                                                  
#  1. SUPPRESSION DES ENREGISTREMENTS DE RTCC01 (CAISSES OUVERTES)             
#     DONT LA DATE DE VALIDATION EST INF�RIEURE _A LA DATE DE TRAITEMEN         
#     MOINS LE D�LAI PARAM�TR� DANS LA SOUS-TABLE DELAI                        
#     (PAR D�FAUT, DELAI DE 90 JOURS), SUR LE CODE PROGRAMME BCC100            
#                                                                              
#  2. SUPPRESSION DES ENREGISTREMENTS DE RTCC02 (SACOCHES _A RECEVOIR),         
#     DE RTCC03 (REMONT�ES LOG DE CAISSE) ET DE RTCC04 (REMONT�ES              
#     SYNTH�TIQUES DE CAISSE) POUR LESQUELS IL N'EXISTE PLUS                   
#     D'ENREGISTREMENTS DANS LA TABLE RTCC01                                   
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC100DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CC100DAG
       ;;
(CC100DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER PARAM�TRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# *****   TABLES EN MAJ                                                        
#    RSCC01   : NAME=RSCC01D,MODE=U - DYNAM=YES                                
# -X-CC100DR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSCC01 /dev/null
#    RSCC02   : NAME=RSCC02D,MODE=U - DYNAM=YES                                
# -X-RSCC02D  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSCC02 /dev/null
#    RSCC03   : NAME=RSCC03D,MODE=U - DYNAM=YES                                
# -X-RSCC03D  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSCC03 /dev/null
#    RSCC04   : NAME=RSCC04D,MODE=U - DYNAM=YES                                
# -X-RSCC04D  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSCC04 /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC100 
       JUMP_LABEL=CC100DAH
       ;;
(CC100DAH)
       m_CondExec 04,GE,CC100DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
