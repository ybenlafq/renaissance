#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT99Y.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYEXT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/11/22 AT 16.55.53 BY PREPA2                       
#    STANDARDS: P  JOBSET: EXT99Y                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  BEX999 : MISE A JOUR DES CODES ETATS DES TABLES ARTICLES DE LYON            
#           EN VUE DES STATISTIQUES                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EXT99YA
       ;;
(EXT99YA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  95/11/22 AT 16.55.52 BY PREPA2                   
# *    JOBSET INFORMATION:    NAME...: EXT99Y                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'MAJ CODES ETATS'                       
# *                           APPL...: REPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EXT99YAA
       ;;
(EXT99YAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    EXTRAC. DONNEES NCP POUR LES EXTRACTEURS*                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES CODES ETATS VENANT DE EXT99P                             
       m_FileAssign -d SHR -g +0 FEX998 ${DATA}/PXX0/F07.BEX998AP
#                                                                              
# ******  CODE MARKETING                                                       
#    RSGA09Y  : NAME=RSGA09Y,MODE=U - DYNAM=YES                                
# -X-RSGA09Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA09Y /dev/null
# ******  RELATION ETAT FAMILLE/RAYON                                          
#    RSGA11Y  : NAME=RSGA11Y,MODE=U - DYNAM=YES                                
# -X-RSGA11Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA11Y /dev/null
# ******  RELATION FAMILLE/CODE MARKETING                                      
#    RSGA12Y  : NAME=RSGA12Y,MODE=U - DYNAM=YES                                
# -X-RSGA12Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA12Y /dev/null
# ******  LIBELLES DES IMBRICATIONS CODE MARKETING                             
#    RSGA29Y  : NAME=RSGA29Y,MODE=U - DYNAM=YES                                
# -X-RSGA29Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGA29Y /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX999 
       JUMP_LABEL=EXT99YAB
       ;;
(EXT99YAB)
       m_CondExec 04,GE,EXT99YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
