#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA210M.ksh                       --- VERSION DU 09/10/2016 05:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMRA210 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.12.51 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RA210M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER PROVENANT DE L'ECLATEMENT                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RA210MA
       ;;
(RA210MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RA210MAA
       ;;
(RA210MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FRA210BM
#         FILE  NAME=FRA2REJM,MODE=I                                           
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FRA210CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_263_10 263 CH 10
 /FIELDS FLD_CH_252_7 252 CH 7
 /KEYS
   FLD_CH_11_6 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_252_7 ASCENDING,
   FLD_CH_263_10 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA210MAB
       ;;
(RA210MAB)
       m_CondExec 00,EQ,RA210MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA210 **  CREATION DES AVOIRS DE REMBOURSEMENT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA210MAD
       ;;
(RA210MAD)
       m_CondExec ${EXAAF},NE,YES 
# ******* FIC DES AVOIRS + REJETS TRI�S                                        
       m_FileAssign -d SHR -g ${G_A1} FRA210 ${DATA}/PXX0/F89.FRA210CM
# ******* TABLE EN LECTURE                                                     
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGQ96   : NAME=RSGQ96,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ96 /dev/null
#    RTGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTRX00   : NAME=RSRX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRX00 /dev/null
# ******* TABLE EN ECRITURE                                                    
#    RTRA00   : NAME=RSRA00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA00 /dev/null
#    RTRA01   : NAME=RSRA01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA01 /dev/null
#        FDATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES DEMANDES DE REMBOURSEMENTS D'AVOIR _A REPRENDRE               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FREJETS ${DATA}/PXX0/F89.FRA2REJM
# ******* CET �TAT EST ENVOY� SOUS EOS DANS LA BOITE DFO                       
# IRA210   REPORT SYSOUT=(9,IRA210M),RECFM=FBA,LRECL=133,BLKSIZE=0             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IRA210 ${DATA}/PXX0/F89.FRA210DM
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA210 
       JUMP_LABEL=RA210MAE
       ;;
(RA210MAE)
       m_CondExec 04,GE,RA210MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE _A Z�RO DU FICHIER VENANT DE SIEBEL                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210MAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA210MAG
       ;;
(RA210MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC ECLATE REMIS _A Z�RO                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F89.FRA210BM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210MAG.sysin
       m_UtilityExec
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=RA210XXX,                                                           
#      FNAME=":FRA210DM""(0),                                                  
#      NFNAME=RA210M                                                           
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210MAH
       ;;
(RA210MAH)
       m_CondExec 16,NE,RA210MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DEPENDANCE POUR PLAN                                                       
#  CHANGEMENT PCL => RA210M <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRA210M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210MAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RA210MAJ
       ;;
(RA210MAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210MAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RA210MAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RA210MAM
       ;;
(RA210MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210MAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
