#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FV1A1P.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFV1A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/02/10 AT 10.43.45 BY PREPA2                       
#    STANDARDS: P  JOBSET: FV1A1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CONSTITUTION DU FICHIER REPRISE POUR G.C.T EN CAS DE PLANTA         
#          DE LA CHAINE FV001P                                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ANCIEN FORMAT                          
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FV1A1PA
       ;;
(FV1A1PA)
#
#FV1A1PBX
#FV1A1PBX Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#FV1A1PBX
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2009/02/10 AT 10.43.45 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: FV1A1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FV001P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FV1A1PAA
       ;;
(FV1A1PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FG14FPC
# ******* FIC DE RECYCLAGE DE PAIE (ENTR�E DE FV001P LA VEILLE)                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FRECYCLP
# ******* FIC DE REPRISE EN CAS DE PLANTAGE DE LA VEILLE)                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PFV0/F07.BFV001AP.REPRISE
# ******* FIC DES INTERFACES CUMULES TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PFV0/F07.BFV001AP.REPRISE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_98_25 98 CH 25
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FV1A1PAB
       ;;
(FV1A1PAB)
       m_CondExec 00,EQ,FV1A1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FV1A1PAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
