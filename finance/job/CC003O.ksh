#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC003O.ksh                       --- VERSION DU 09/10/2016 00:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POCC003 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/10/25 AT 09.28.57 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CC003O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM BCC003 **  COBOL2/DB2 .ALIMENTATION RTCC03                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC003OA
       ;;
(CC003OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CC003OAA
       ;;
(CC003OAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE  DB2 EN LECTURE                                                
#                                                                              
#    RSFT34   : NAME=RSFT34O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT34 /dev/null
# ******* TABLE EM                                                             
#    RSEM53   : NAME=RSEM53O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEM53 /dev/null
#    RSEM54   : NAME=RSEM54O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEM54 /dev/null
#    RSMQ15   : NAME=RSMQ15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* TABLE  DB2 EN MAJ                                                    
#                                                                              
#    RSCC01   : NAME=RSCC01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSCC01 /dev/null
# ******* TABLE EM                                                             
#    RSCC03   : NAME=RSCC03O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSCC03 /dev/null
#    RSEM54   : NAME=RSEM54O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEM54 /dev/null
#    RSMQ15   : NAME=RSMQ15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC003 
       JUMP_LABEL=CC003OAB
       ;;
(CC003OAB)
       m_CondExec 04,GE,CC003OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCC006 **   EXTRACTION AVEC ENVOI AUX FILIALE  D UN MAIL                
#  DANS CE PROGRAME INTERRO DES S/TABLES RVCCPA2 + RVCCC02                     
#  AJOUT ET REMPLACE LE PGM BCC004 LE 14/11/2011                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC003OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CC003OAD
       ;;
(CC003OAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* TABLE  DB2 EN MAJ                                                    
#                                                                              
#    RSCC03   : NAME=RSCC03O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSCC03 /dev/null
#                                                                              
# ******* FICHIER EN SORTIE (LREL 100)                                         
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFCC04 ${DATA}/PTEM/CC003OAD.BCC004AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC006 
       JUMP_LABEL=CC003OAE
       ;;
(CC003OAE)
       m_CondExec 04,GE,CC003OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU STEP PRECEDENT                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC003OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CC003OAG
       ;;
(CC003OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/CC003OAD.BCC004AO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CC003OAG.BCC004BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_45_8 45 PD 8
 /FIELDS FLD_PD_37_8 37 PD 8
 /FIELDS FLD_CH_1_27 1 CH 27
 /FIELDS FLD_CH_34_3 34 CH 3
 /KEYS
   FLD_CH_1_27 ASCENDING,
   FLD_CH_34_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_37_8,
    TOTAL FLD_PD_45_8
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CC003OAH
       ;;
(CC003OAH)
       m_CondExec 00,EQ,CC003OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCC007 **   TRAITEMENT DU FICHIER INTER/STEP ET ENVOI GETWAY            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC003OAJ PGM=BCC007     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CC003OAJ
       ;;
(CC003OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  FIC PRECEDENT                                                               
       m_FileAssign -d SHR -g ${G_A2} FFCC04E ${DATA}/PTEM/CC003OAG.BCC004BO
#  FIC D ENVOI A LA GETWAY                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFCC04S ${DATA}/PXX0/F16.BCC006AO.GETWAY
       m_ProgramExec BCC007 
# ********************************************************************         
# *************************                                                    
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CC003OZA
       ;;
(CC003OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CC003OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
