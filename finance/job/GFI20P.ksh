#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFI20P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGFI20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/09 AT 14.10.18 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GFI20P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#           COPY DE LA DATE DE DERNIER PASSAGE DE LA CHAINE                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFI20PA
       ;;
(GFI20PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFI20PAA
       ;;
(GFI20PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.DATEPFIP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.BFI015DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PAB
       ;;
(GFI20PAB)
       m_CondExec 00,EQ,GFI20PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI005 : EXTRACTION DES MOUVEMENTS DE STOCK A FACTURER                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAD
       ;;
(GFI20PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES MVTS DE STOCKS                                             
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE DU DERNIER MVT TRAITE + DERNIER N� FACTURE                      
       m_FileAssign -d SHR -g ${G_A1} FDATSEQ ${DATA}/PEX0/F07.BFI015DP
# ****** FICHIER DES MUTATIONS SELECTIONNEES POUR FACTURATION                  
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFI005 ${DATA}/PTEM/GFI20PAD.BFI005AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI005 
       JUMP_LABEL=GFI20PAE
       ;;
(GFI20PAE)
       m_CondExec 04,GE,GFI20PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI010 : TRAITEMENT DES MVTS DE STOCK A FACTURER                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAG
       ;;
(GFI20PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE DES PARAMETRES FAMILLES                                        
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  TABLE DES MVTS DE STOCKS                                             
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  TABLE DES PRMP + PCF                                                 
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  TABLE COMMANDES FOURNISSEURS                                         
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
# ******  TABLE HISTO                                                          
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
# ******  TABLE DES CMDES EN ATTENTE DE RECEPTION                              
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
# ******  TABLE DETAIL DES MUTATIONS                                           
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******  TABLE GENERALITE MUTATIONS                                           
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  TABLE HISTO DES FACTURES INTERNES                                    
#    RSFI10   : NAME=RSFI10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER DES MUTATIONS SELECTIONNEES POUR FACTURATION                 
       m_FileAssign -d SHR -g ${G_A2} FFI005 ${DATA}/PTEM/GFI20PAD.BFI005AP
# ******  FICHIER DES MUTATIONS A FACTURER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFI010 ${DATA}/PTEM/GFI20PAG.BFI010AP
# ******  FICHIER DES MUTATIONS A EDITER                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FFI011 ${DATA}/PTEM/GFI20PAG.BFI010CP
# ******  FICHIER DES FACTURES D EXCEPTION                                     
       m_FileAssign -d SHR FFI013 /dev/null
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE DU DERNIER MVT TRAITE + DERNIER N� FACTURE                      
       m_FileAssign -d SHR -g ${G_A3} FDATSEQ ${DATA}/PEX0/F07.BFI015DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI010 
       JUMP_LABEL=GFI20PAH
       ;;
(GFI20PAH)
       m_CondExec 04,GE,GFI20PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES MOUVEMENTS A FACTURER                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAJ
       ;;
(GFI20PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GFI20PAG.BFI010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GFI20PAJ.BFI011BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_7 21 CH 7
 /FIELDS FLD_CH_9_6 9 CH 6
 /FIELDS FLD_CH_28_7 28 CH 7
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_35_3 35 CH 3
 /FIELDS FLD_CH_15_1 15 CH 1
 /KEYS
   FLD_CH_9_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_15_1 ASCENDING,
   FLD_CH_21_7 ASCENDING,
   FLD_CH_35_3 ASCENDING,
   FLD_CH_28_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PAK
       ;;
(GFI20PAK)
       m_CondExec 00,EQ,GFI20PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES MUTATIONS A EDITER                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAM
       ;;
(GFI20PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GFI20PAG.BFI010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GFI20PAM.BFI010DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PAN
       ;;
(GFI20PAN)
       m_CondExec 00,EQ,GFI20PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI015 : EDITION DES FACTURES ET MAJ DE L'HISTORIQUE (RTFI10)               
#  REPRISE: OUI                                                                
# ********************************************************************         
#         IMPORTANT VOIR COMMENTAIRE EN TETE DE PCL                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAQ
       ;;
(GFI20PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  PARAMETRES FAMILLES                                                  
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  TABLE HISTORIQUES FACTURATIONS                                       
#    RSFI10   : NAME=RSFI10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFI10 /dev/null
#                                                                              
# ******  SOCIETE TRAITEE: 907                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MAJ DATE DE MVT TRAITE + DERNIER N� FACTURE                          
       m_FileAssign -d OLD,KEEP,DELETE -g ${G_A6} FDATSEQ ${DATA}/PEX0/F07.BFI015DP
#                                                                              
# ******  N� DE TELEX A EDITER                                                 
#         FPARAM=O EDITION,FPARAM=N OU BLANC PAS EDITION                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GFI20PAQ
#                                                                              
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d SHR -g ${G_A7} FFI010 ${DATA}/PTEM/GFI20PAJ.BFI011BP
#                                                                              
# *****   FICHIER DES FACTURES INTER SOCIETES                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFG01 ${DATA}/PTEM/GFI20PAQ.FFG01AP
#                                                                              
# ******  EDITION DES FACTURES ET DES AVOIRS DE MATERIEL                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IFI015 ${DATA}/PXX0/F07.IFI015AP
#  IFI015   REPORT SYSOUT=(9,IFI015),RECFM=FBA,BLKSIZE=133,LRECL=133,          
#               SPIN=UNALLOC                                                   
# ******  EDITION DES FACTURES ET DES AVOIRS DE FRAIS DE GESTION               
       m_OutputAssign -c 9 -w IFI016 IFI016
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI015 
       JUMP_LABEL=GFI20PAR
       ;;
(GFI20PAR)
       m_CondExec 04,GE,GFI20PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ALIMENTATION DU FICHIER CUMUL DES FACTURES INTER SOCIETES                   
#  DESTINES A LA TABLE RTFG01 (RMAC) CHAINE FG00FP                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAT
       ;;
(GFI20PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FACTURES DU JOUR                                                     
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GFI20PAQ.FFG01AP
# *****   FICHIER DES FACTURES INTER SOCIETES                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FFG01BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_186_7 186 PD 7
 /FIELDS FLD_PD_76_7 76 PD 7
 /FIELDS FLD_PD_83_7 83 PD 7
 /FIELDS FLD_PD_90_3 90 PD 3
 /FIELDS FLD_CH_62_14 62 CH 14
 /FIELDS FLD_PD_55_7 55 PD 7
 /FIELDS FLD_PD_244_7 244 PD 7
 /FIELDS FLD_CH_93_24 93 CH 24
 /KEYS
   FLD_CH_1_39 ASCENDING,
   FLD_CH_62_14 ASCENDING,
   FLD_CH_93_24 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_55_7,
    TOTAL FLD_PD_76_7,
    TOTAL FLD_PD_83_7,
    TOTAL FLD_PD_90_3,
    TOTAL FLD_PD_186_7,
    TOTAL FLD_PD_244_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PAU
       ;;
(GFI20PAU)
       m_CondExec 00,EQ,GFI20PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI020 : EDITION DES LIGNES DE MUTATIONS FACTUREES                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PAX
       ;;
(GFI20PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d SHR -g ${G_A9} FFI011 ${DATA}/PTEM/GFI20PAM.BFI010DP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES LIGNES DE MUTATIONS FACTUREES                            
# ******  SUPRESS DE L'ENTREE SOUS EOS LE 220501 CGA PLUS UTIL                 
# IFI020   REPORT SYSOUT=(9,IFI020),SPIN=UNALLOC                               
       m_OutputAssign -c Z IFI020
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI020 
       JUMP_LABEL=GFI20PAY
       ;;
(GFI20PAY)
       m_CondExec 04,GE,GFI20PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFI022 : GENERATION D'UN FICHIER A DESTINATION D'HADOOP                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBA
       ;;
(GFI20PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSFI10   : NAME=RSFI10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFI10 /dev/null
# ******  FICHIER TRIE DES MUTATIONS A FACTURER                                
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 CSVFI10 ${DATA}/PXX0/F07.BFI022AP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g +0 FDATSEQ ${DATA}/PEX0/F07.DATEPFIP
# ******  SUPRESS DE L'ENTREE SOUS EOS LE 220501 CGA PLUS UTIL                 
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI022 
       JUMP_LABEL=GFI20PBB
       ;;
(GFI20PBB)
       m_CondExec 04,GE,GFI20PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTGFI20P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBD
       ;;
(GFI20PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GFI20PBD.FTGFI20P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGFI20P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBG
       ;;
(GFI20PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20PBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GFI20PBD.FTGFI20P(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  ECRITURE DE LA DATE DE PASSAGE DE LA CHAINE APRES BFI015                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBJ
       ;;
(GFI20PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PEX0/F07.BFI015DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.DATEPFIP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PBK
       ;;
(GFI20PBK)
       m_CondExec 00,EQ,GFI20PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DU FICHIER ISSU DU BNM070 POUR TRAITEMENT HEBDO                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBM
       ;;
(GFI20PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION CAISSES DU JOUR                                              
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/F07.IFI015AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.IFI015BP
# ******  EDITION DESTINE AUX CDROM EN FIN DE MOIS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.IFI015BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI20PBN
       ;;
(GFI20PBN)
       m_CondExec 00,EQ,GFI20PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IFI015 ETAT DES FACTURES                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI20PBQ PGM=IEBGENER   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PBQ
       ;;
(GFI20PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A13} SYSUT1 ${DATA}/PXX0/F07.IFI015AP
       m_OutputAssign -c 9 -w IFI015 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GFI20PBR
       ;;
(GFI20PBR)
       m_CondExec 00,EQ,GFI20PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GFI20PZA
       ;;
(GFI20PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI20PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
