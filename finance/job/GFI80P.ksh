#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFI80P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGFI80 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/16 AT 14.45.25 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GFI80P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BFI080 EXTRACTION DES LIGNES � VALORISER ET VALORISATION   **         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GFI80PA
       ;;
(GFI80PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GFI80PAA
       ;;
(GFI80PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FPARAM                                                               
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.FFI080AP
# ******* FSIMU                                                                
       m_FileAssign -d SHR -g +0 FSIMU ${DATA}/PXX0/F07.FFI080BP
# ************** TABLE EN LECTURE                                              
#    RTFI71   : NAME=RSFI71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI71 /dev/null
#    RTFI72   : NAME=RSFI72,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI72 /dev/null
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
#    RTGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT01 /dev/null
#    RTTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH00 /dev/null
#    RTTH14   : NAME=RSTH14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH14 /dev/null
#    RTTH15   : NAME=RSTH15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH15 /dev/null
# ************** TABLE DES ECRITURE                                            
#    RTFI70   : NAME=RSFI70,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI70 /dev/null
#    RTFI80   : NAME=RSFI80,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI80 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI080 
       JUMP_LABEL=GFI80PAB
       ;;
(GFI80PAB)
       m_CondExec 04,GE,GFI80PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFI090 EXTRACTION DES LIGNES _A FACTURER                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAD
       ;;
(GFI80PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ************** TABLE EN LECTURE                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGF01   : NAME=RSGF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGF01 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTLI01   : NAME=RSLI01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI01 /dev/null
#    RTLV05   : NAME=RSLV05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV05 /dev/null
#    RTPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT01 /dev/null
#    RTTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH00 /dev/null
# ************** TABLE DES ECRITURE                                            
#    RTFI80   : NAME=RSFI80,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI80 /dev/null
#                                                                              
#         FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 700 -t LSEQ -g +1 FEXTRACT ${DATA}/PTEM/GFI80PAD.FFI080CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI090 
       JUMP_LABEL=GFI80PAE
       ;;
(GFI80PAE)
       m_CondExec 04,GE,GFI80PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER                                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAG
       ;;
(GFI80PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GFI80PAD.FFI080CP
       m_FileAssign -d NEW,CATLG,DELETE -r 700 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FFI080DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_44 1 CH 44
 /KEYS
   FLD_CH_1_44 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GFI80PAH
       ;;
(GFI80PAH)
       m_CondExec 00,EQ,GFI80PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFI095 MISE EN FORME LIGNES _A FACTURER                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAJ
       ;;
(GFI80PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#         FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FEXTRACE ${DATA}/PXX0/F07.FFI080DP
#         FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 700 -t LSEQ -g +1 FEXTRACS ${DATA}/PXX0/F07.FFI080EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFI095 
       JUMP_LABEL=GFI80PAK
       ;;
(GFI80PAK)
       m_CondExec 04,GE,GFI80PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFR050 CALCUL DES PRMP FRANCHIS�S                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAM
       ;;
(GFI80PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ************** TABLE EN LECTURE                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS30 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFI80   : NAME=RSFI80,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI80 /dev/null
#    RTFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFR50 /dev/null
# ************** TABLE DES ECRITURE                                            
#    RTFR50   : NAME=RSFR50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFR50 /dev/null
#    RTFI80   : NAME=RSFI80,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFI80 /dev/null
#                                                                              
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFR050 
       JUMP_LABEL=GFI80PAN
       ;;
(GFI80PAN)
       m_CondExec 04,GE,GFI80PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTGFI80P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAQ
       ;;
(GFI80PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI80PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GFI80PAQ.FTGFI80P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FFI080EP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GFI80PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PAT
       ;;
(GFI80PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI80PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GFI80PAQ.FTGFI80P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GFI80PZA
       ;;
(GFI80PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GFI80PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
