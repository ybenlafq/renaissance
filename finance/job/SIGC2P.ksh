#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SIGC2P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSIGC2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/08/20 AT 11.55.55 BY PREPA2                       
#    STANDARDS: P  JOBSET: SIGC2P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# ********************************************************************         
#  PGM : BSIG20                                                                
#  ------------                                                                
#  REPRISE DU FICHIER SXGC2P ISSU D'UNE EXPLO HRV3 (UNIX) AFIN DE              
#  PACKER LES ZONES DECIMALES                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SIGC2PA
       ;;
(SIGC2PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SIGC2PAA
       ;;
(SIGC2PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 FEXPLO ${DATA}/PXX0/F07.SXGC2P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F829.SXGC2P
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSIG20 ${DATA}/PTEM/SIGC2PAA.BSIG20CP
       m_ProgramExec BSIG20 
# ********************************************************************         
#  TRI DU FICHIER ISSU DU PGM BSIG20                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAD
       ;;
(SIGC2PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SIGC2PAA.BSIG20CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/SIGC2PAD.BSIG20DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_2 59 CH 2
 /FIELDS FLD_CH_61_7 61 CH 7
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_CH_16_25 16 CH 25
 /FIELDS FLD_PD_50_9 50 PD 9
 /FIELDS FLD_PD_41_9 41 PD 9
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_59_2 ASCENDING,
   FLD_CH_16_25 ASCENDING,
   FLD_CH_61_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_9,
    TOTAL FLD_PD_50_9
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGC2PAE
       ;;
(SIGC2PAE)
       m_CondExec 00,EQ,SIGC2PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG25                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER PREVISION POUR FDC                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAG
       ;;
(SIGC2PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR FTYPPAIE ${DATA}/CORTEX4.P.MTXTFIX1/PAYPROP
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A2} FSIG20 ${DATA}/PTEM/SIGC2PAD.BSIG20DP
# ------  FICHIER POUR CONTROLE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FPARAM ${DATA}/PTEM/SIGC2PAG.BSIG25AP
# ------  FICHIER REPRIS DANS LE BSIG26                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FCTL ${DATA}/PTEM/SIGC2PAG.BSIG25BP
# ------  FICHIER POUR GCT (PAS D'INJECTION EN COMPTA)                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FGCT ${DATA}/PNCGP/F07.SIGGC2P
# ------  FICHIER POUR FDC (PREVISION)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 FFDC ${DATA}/PNCGP/F07.SIGAFDC
# ------  FICHIER POUR FDC (PREVISION)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 61 -g +1 FREMUN ${DATA}/PXX0/F07.BSIG25GP
# ------  ETATS DE COMPTE RENDUS                                               
       m_OutputAssign -c 9 -w ISG010 FANOSYNT
       m_OutputAssign -c 9 -w ISG020 FANODET
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG25 
       JUMP_LABEL=SIGC2PAH
       ;;
(SIGC2PAH)
       m_CondExec 04,GE,SIGC2PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25  (CONTROLE ANALYTIQUE)               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAJ
       ;;
(SIGC2PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SIGC2PAG.BSIG25BP
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 SORTOUT ${DATA}/PTEM/SIGC2PAJ.BSIG26DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_33 1 CH 33
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_1_51 1 CH 51
 /KEYS
   FLD_CH_1_33 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2PAK
       ;;
(SIGC2PAK)
       m_CondExec 00,EQ,SIGC2PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE GENERAL)                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAM
       ;;
(SIGC2PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/SIGC2PAG.BSIG25BP
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 SORTOUT ${DATA}/PTEM/SIGC2PAM.BSIG26EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_34_9 34 CH 9
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_1_21 1 CH 21
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_52_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_52_8,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2PAN
       ;;
(SIGC2PAN)
       m_CondExec 00,EQ,SIGC2PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE RUBRIQUE)                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAQ
       ;;
(SIGC2PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/SIGC2PAG.BSIG25BP
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -g +1 SORTOUT ${DATA}/PTEM/SIGC2PAQ.BSIG26FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_34_9 34 CH 9
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_CH_28_6 28 CH 6
 /FIELDS FLD_CH_1_21 1 CH 21
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_28_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_28_6,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2PAR
       ;;
(SIGC2PAR)
       m_CondExec 00,EQ,SIGC2PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG26                                                                
#  ------------                                                                
#  EDITION DES ETATS DE CONTROLE                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAT
       ;;
(SIGC2PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DES TRIS PRECEDANT                                      
       m_FileAssign -d SHR -g ${G_A6} FCTLANA ${DATA}/PTEM/SIGC2PAJ.BSIG26DP
       m_FileAssign -d SHR -g ${G_A7} FCTLGEN ${DATA}/PTEM/SIGC2PAM.BSIG26EP
       m_FileAssign -d SHR -g ${G_A8} FCTLRUB ${DATA}/PTEM/SIGC2PAQ.BSIG26FP
# ------  FICHIER DE CONTROLE SI OK OU ANO                                     
       m_FileAssign -d SHR -g ${G_A9} FPARAM ${DATA}/PTEM/SIGC2PAG.BSIG25AP
# ------  ETATS DE CONTROLE                                                    
       m_OutputAssign -c 9 -w ISG025 FGENE
       m_OutputAssign -c 9 -w ISG026 FANA
       m_OutputAssign -c 9 -w ISG027 FRUB
# ******************************************************************           
#  REMISE A ZERO DU FICHIER PROVENANT D'UNIX SXGC2P / SX829PS                  
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG26 
       JUMP_LABEL=SIGC2PAU
       ;;
(SIGC2PAU)
       m_CondExec 04,GE,SIGC2PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SIGC2PAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PAX
       ;;
(SIGC2PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT1 ${DATA}/PXX0/F07.SXGC2P
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT2 ${DATA}/PXX0/F829.SXGC2P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2PAX.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SIGC2PAY
       ;;
(SIGC2PAY)
       m_CondExec 16,NE,SIGC2PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2PZA
       ;;
(SIGC2PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
