#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRA00M.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGRA00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 19.11.35 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GRA00M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DU TABLESPACE RSRA02M                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRA00MA
       ;;
(GRA00MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRA00MAA
       ;;
(GRA00MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR         *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGRA00M
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GRA00MAA
       m_ProgramExec IEFBR14 "RDAR,GRA00M.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRA00MAD
       ;;
(GRA00MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGRA00M
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRA00MAE
       ;;
(GRA00MAE)
       m_CondExec 00,EQ,GRA00MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRA101 : EDITE LES DEMANDES SAISIES OU MAJ DANS LA JOURNEE                  
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAG
       ;;
(GRA00MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE SUR RDAR                                            
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSRA00   : NAME=RSRA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA101 ${DATA}/PTEM/GRA00MAG.IRA101AM
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA102 ${DATA}/PTEM/GRA00MAG.IRA102AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA101 
       JUMP_LABEL=GRA00MAH
       ;;
(GRA00MAH)
       m_CondExec 04,GE,GRA00MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA101AM ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAJ
       ;;
(GRA00MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA101                                               
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GRA00MAG.IRA101AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MAJ.IRA101BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_40_2 40 CH 2
 /FIELDS FLD_BI_10_25 10 CH 25
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_35_5 35 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_25 ASCENDING,
   FLD_BI_35_5 ASCENDING,
   FLD_BI_40_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MAK
       ;;
(GRA00MAK)
       m_CondExec 00,EQ,GRA00MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAM
       ;;
(GRA00MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/GRA00MAJ.IRA101BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MAM.IRA101CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MAN
       ;;
(GRA00MAN)
       m_CondExec 04,GE,GRA00MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAQ
       ;;
(GRA00MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GRA00MAM.IRA101CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MAQ.IRA101DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MAR
       ;;
(GRA00MAR)
       m_CondExec 00,EQ,GRA00MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA101                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAT
       ;;
(GRA00MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/GRA00MAJ.IRA101BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/GRA00MAQ.IRA101DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA101                                          
       m_OutputAssign -c 9 -w IRA101 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MAU
       ;;
(GRA00MAU)
       m_CondExec 04,GE,GRA00MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA102AM ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MAX
       ;;
(GRA00MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA101                                               
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GRA00MAG.IRA102AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MAX.IRA102BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_5 10 CH 5
 /FIELDS FLD_BI_15_2 15 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING,
   FLD_BI_15_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MAY
       ;;
(GRA00MAY)
       m_CondExec 00,EQ,GRA00MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBA
       ;;
(GRA00MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/GRA00MAX.IRA102BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MBA.IRA102CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MBB
       ;;
(GRA00MBB)
       m_CondExec 04,GE,GRA00MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBD
       ;;
(GRA00MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GRA00MBA.IRA102CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MBD.IRA102DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MBE
       ;;
(GRA00MBE)
       m_CondExec 00,EQ,GRA00MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA102                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBG
       ;;
(GRA00MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/GRA00MAX.IRA102BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/GRA00MBD.IRA102DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA102                                          
       m_OutputAssign -c 9 -w IRA102 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MBH
       ;;
(GRA00MBH)
       m_CondExec 04,GE,GRA00MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRA102 : EDITE LES DEMANDES A PAYER ET ARRIVEES A ECHEANCE                  
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBJ
       ;;
(GRA00MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE SUR RDAR                                            
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSRA00   : NAME=RSRA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
# ******  FICHIER DES PARAMETRES TP                                            
       m_FileAssign -d SHR FRP000 ${DATA}/CORTEX4.P.MTXTFIX5/BRA102AM
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA103 ${DATA}/PTEM/GRA00MBJ.IRA103AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA102 
       JUMP_LABEL=GRA00MBK
       ;;
(GRA00MBK)
       m_CondExec 04,GE,GRA00MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA103AP ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBM
       ;;
(GRA00MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA102                                               
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GRA00MBJ.IRA103AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MBM.IRA103BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_32_2 32 CH 2
 /FIELDS FLD_BI_22_5 22 CH 5
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_10_12 10 CH 12
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_12 ASCENDING,
   FLD_BI_22_5 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_32_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MBN
       ;;
(GRA00MBN)
       m_CondExec 00,EQ,GRA00MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBQ
       ;;
(GRA00MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/GRA00MBM.IRA103BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MBQ.IRA103CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MBR
       ;;
(GRA00MBR)
       m_CondExec 04,GE,GRA00MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBT
       ;;
(GRA00MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GRA00MBQ.IRA103CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MBT.IRA103DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MBU
       ;;
(GRA00MBU)
       m_CondExec 00,EQ,GRA00MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA103                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MBX
       ;;
(GRA00MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/GRA00MBM.IRA103BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/GRA00MBT.IRA103DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA103                                          
       m_OutputAssign -c 9 -w IRA103 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MBY
       ;;
(GRA00MBY)
       m_CondExec 04,GE,GRA00MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRA103 : EDITE LES REGLTS GENERES OU EDITES DANS LA JOURNEE                 
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCA
       ;;
(GRA00MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE SUR RDAR                                            
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSRA00   : NAME=RSRA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
#    RSRA02   : NAME=RSRA02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA02 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA104 ${DATA}/PTEM/GRA00MCA.IRA104AM
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA105 ${DATA}/PTEM/GRA00MCA.IRA105AM
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA109 ${DATA}/PTEM/GRA00MCA.IRA109AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA103 
       JUMP_LABEL=GRA00MCB
       ;;
(GRA00MCB)
       m_CondExec 04,GE,GRA00MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA104AP ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCD
       ;;
(GRA00MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA103                                               
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GRA00MCA.IRA104AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MCD.IRA104BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_12 10 CH 12
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_32_7 32 CH 7
 /FIELDS FLD_BI_22_5 22 CH 5
 /FIELDS FLD_BI_39_2 39 CH 2
 /FIELDS FLD_BI_7_3 7 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_12 ASCENDING,
   FLD_BI_22_5 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_32_7 ASCENDING,
   FLD_BI_39_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MCE
       ;;
(GRA00MCE)
       m_CondExec 00,EQ,GRA00MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCG
       ;;
(GRA00MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PTEM/GRA00MCD.IRA104BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MCG.IRA104CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MCH
       ;;
(GRA00MCH)
       m_CondExec 04,GE,GRA00MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCJ
       ;;
(GRA00MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GRA00MCG.IRA104CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MCJ.IRA104DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MCK
       ;;
(GRA00MCK)
       m_CondExec 00,EQ,GRA00MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA104                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCM
       ;;
(GRA00MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A20} FEXTRAC ${DATA}/PTEM/GRA00MCD.IRA104BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A21} FCUMULS ${DATA}/PTEM/GRA00MCJ.IRA104DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA104                                          
# MODIF LE 291100 CGA ETAT PLUS UTIL A DAL                                     
# FEDITION REPORT SYSOUT=(9,IRA104),RECFM=VA                                   
       m_OutputAssign -c "*" FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MCN
       ;;
(GRA00MCN)
       m_CondExec 04,GE,GRA00MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA105AM ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCQ
       ;;
(GRA00MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA103                                               
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/GRA00MCA.IRA105AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MCQ.IRA105BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_32_7 32 CH 7
 /FIELDS FLD_BI_22_5 22 CH 5
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_12 10 CH 12
 /FIELDS FLD_BI_39_2 39 CH 2
 /FIELDS FLD_BI_27_5 27 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_12 ASCENDING,
   FLD_BI_22_5 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_32_7 ASCENDING,
   FLD_BI_39_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MCR
       ;;
(GRA00MCR)
       m_CondExec 00,EQ,GRA00MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCT
       ;;
(GRA00MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/GRA00MCQ.IRA105BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MCT.IRA105CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MCU
       ;;
(GRA00MCU)
       m_CondExec 04,GE,GRA00MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MCX
       ;;
(GRA00MCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/GRA00MCT.IRA105CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MCX.IRA105DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MCY
       ;;
(GRA00MCY)
       m_CondExec 00,EQ,GRA00MCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA105                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDA
       ;;
(GRA00MDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A25} FEXTRAC ${DATA}/PTEM/GRA00MCQ.IRA105BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A26} FCUMULS ${DATA}/PTEM/GRA00MCX.IRA105DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA105                                          
       m_OutputAssign -c 9 -w IRA105 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MDB
       ;;
(GRA00MDB)
       m_CondExec 04,GE,GRA00MDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA109AP ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDD
       ;;
(GRA00MDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA103                                               
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PTEM/GRA00MCA.IRA109AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MDD.IRA109BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_39_2 39 CH 2
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_10_12 10 CH 12
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_22_5 22 CH 5
 /FIELDS FLD_BI_32_7 32 CH 7
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_12 ASCENDING,
   FLD_BI_22_5 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_32_7 ASCENDING,
   FLD_BI_39_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MDE
       ;;
(GRA00MDE)
       m_CondExec 00,EQ,GRA00MDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDG
       ;;
(GRA00MDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A28} FEXTRAC ${DATA}/PTEM/GRA00MDD.IRA109BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MDG.IRA109CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MDH
       ;;
(GRA00MDH)
       m_CondExec 04,GE,GRA00MDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDJ
       ;;
(GRA00MDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GRA00MDG.IRA109CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MDJ.IRA109DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MDK
       ;;
(GRA00MDK)
       m_CondExec 00,EQ,GRA00MDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA109                                                   
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDM
       ;;
(GRA00MDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A30} FEXTRAC ${DATA}/PTEM/GRA00MDD.IRA109BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A31} FCUMULS ${DATA}/PTEM/GRA00MDJ.IRA109DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA109                                          
       m_OutputAssign -c 9 -w IRA109 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MDN
       ;;
(GRA00MDN)
       m_CondExec 04,GE,GRA00MDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSA105 : CREATION D'UN FICHIER POUR EDITION IRA108                          
#           CREATION D'UN FICHIER POUR GCV (GRA00M) REPRIS DANS FV01FP         
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDQ
       ;;
(GRA00MDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE SUR RDAR                                            
#    RSFX00   : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  TABLES EN MAJ SUR RDAR                                               
#    RSRA02   : NAME=RSRA02M,MODE=(U,U) - DYNAM=YES                            
# -X-GRA00MR1 - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRA02 /dev/null
# ******  TABLES EN MAJ (ANCIENNE RTFV19)                                      
#    RSFV19   : NAME=RSFT29M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFV19 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER D'EXTRACTION POUR GENERATEUR D'ETAT                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRA108 ${DATA}/PTEM/GRA00MDQ.IRA108AM
# ******  FICHIER D'EXTRACTION ENTRANT DANS TRAITEMENT FV001M                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFTV02 ${DATA}/PXX0/F89.GRA00M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSA105 
       JUMP_LABEL=GRA00MDR
       ;;
(GRA00MDR)
       m_CondExec 04,GE,GRA00MDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IRA108AM ENTRANT DANS LE BEG050                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDT
       ;;
(GRA00MDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU BRA105                                               
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/GRA00MDQ.IRA108AM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MDT.IRA108BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_7 27 CH 7
 /FIELDS FLD_BI_10_12 10 CH 12
 /FIELDS FLD_BI_34_2 34 CH 2
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_22_5 22 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_12 ASCENDING,
   FLD_BI_22_5 ASCENDING,
   FLD_BI_27_7 ASCENDING,
   FLD_BI_34_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MDU
       ;;
(GRA00MDU)
       m_CondExec 00,EQ,GRA00MDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MDX
       ;;
(GRA00MDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A33} FEXTRAC ${DATA}/PTEM/GRA00MDT.IRA108BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRA00MDX.IRA108CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRA00MDY
       ;;
(GRA00MDY)
       m_CondExec 04,GE,GRA00MDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MEA
       ;;
(GRA00MEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/GRA00MDX.IRA108CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRA00MEA.IRA108DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRA00MEB
       ;;
(GRA00MEB)
       m_CondExec 00,EQ,GRA00MEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IRA108 (LISTES DES REGLTS EDITES)                        
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MED
       ;;
(GRA00MED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A35} FEXTRAC ${DATA}/PTEM/GRA00MDT.IRA108BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A36} FCUMULS ${DATA}/PTEM/GRA00MEA.IRA108DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  FICHIER D'IMPRESSION IRA108                                          
       m_OutputAssign -c 9 -w IRA108 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRA00MEE
       ;;
(GRA00MEE)
       m_CondExec 04,GE,GRA00MED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA300 **  COBOL2/DB2  EXTRACTION DES REMBOURSEMENTS D'AVOIRS           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MEG
       ;;
(GRA00MEG)
       m_CondExec ${EXAGO},NE,YES 
#                                                                              
#    RSPT01   : NAME=RSPT01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSRA00   : NAME=RSRA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA00 /dev/null
#    RSRA01   : NAME=RSRA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA01 /dev/null
#    RSRA02   : NAME=RSRA02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRA02 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSRX00   : NAME=RSRX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES REMBOURSEMENTS D'AVOIR                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FRA30S ${DATA}/PXX0/F89.BRA300AM
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA300 
       JUMP_LABEL=GRA00MEH
       ;;
(GRA00MEH)
       m_CondExec 04,GE,GRA00MEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER VERS USERS NOTES VIA GATEWAY                               
#  REPRISE : OUI                                                               
# ********************************************************************         
# AGT      STEP  PGM=IEFBR14,PATTERN=CFT,RSTRT=SAME                            
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BRA300AM                                                            
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGRA00M                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MEJ PGM=EZACFSM1   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MEJ
       ;;
(GRA00MEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRA00MEJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GRA00MEJ.FTGRA00M
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGRA00M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRA00MEM PGM=FTP        ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MEM
       ;;
(GRA00MEM)
       m_CondExec ${EXAGY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GRA00MEM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GRA00MEJ.FTGRA00M(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GRA00MEQ PGM=EZACFSM1   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MEQ
       ;;
(GRA00MEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRA00MEQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRA00MZA
       ;;
(GRA00MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRA00MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
