#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG00FP.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG00F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/29 AT 10.25.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FG00FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EASYTREAVE POUR METTRE LE FICHIER EN PACK�                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG00FPA
       ;;
(FG00FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+2'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG00FPAA
       ;;
(FG00FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ----------------------                                                      
# **************************************                                       
       m_OutputAssign -c X SYSPRINT
# *****   FICHIER EN PROVENANCE GENERIX KESA VIA GATEWAY                       
       m_FileAssign -d SHR FILEA ${DATA}/PXX0/FTP.GENERIX.DAFG01G
# *****   FICHIER A TRIER PACK�                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 SORTIE ${DATA}/PTEM/FG00FPAA.DAFG01AP
       m_OutputAssign -c T IMPRIM
       m_OutputAssign -c T SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG00FPAA
       m_ProgramExec FG00FPAA
# ********************************************************************         
#  CREATION DES MUTS DU JOUR A TRAITER                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAD
       ;;
(FG00FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER DES MUTS CONTREMARQUES DACEM VENANT DE BPCS (FG001B)         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.AS400.DAFG01
#         FICHIER DES MUTS DACEM VVENANT DE GENERIX                            
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PTEM/FG00FPAA.DAFG01AP
#         FICHIER DES MUT BERCY  VENANT DE BPCS (FG001V)                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F75.DAFG01V
#         FICHIER DE  RECYCLAGE (MUTS EN ERREUR)                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BFG001BB
# *****   FICHIER DES MUTS DU JOUR A TRAITER                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG00FPAD.BFG001AB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_33 1 CH 33
 /KEYS
   FLD_CH_1_33 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPAE
       ;;
(FG00FPAE)
       m_CondExec 00,EQ,FG00FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG001N : MISE EN FORME DES INFOS DES MUTS DACEM POUR EDITION DES           
#           FACTURES DU NETTING                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAG
       ;;
(FG00FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX GROUP                                                
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
#                                                                              
# ******* FIC DES MUTS DU JOUR A TRAITER                                       
       m_FileAssign -d SHR -g ${G_A2} FIDFAC3 ${DATA}/PTEM/FG00FPAD.BFG001AB
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC MUTS DACEM AVEC INFOS NETTING                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFG01 ${DATA}/PTEM/FG00FPAG.FFG01AB
# ******* FIC DE RECYCLAGE DES MUTS EN ERREUR                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 FRECYC ${DATA}/PXX0/F07.BFG001BB
# ******* EDITION DES MUTS EN ERREUR                                           
       m_OutputAssign -c 9 -w BFG001 IMPRIM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG001N 
       JUMP_LABEL=FG00FPAH
       ;;
(FG00FPAH)
       m_CondExec 04,GE,FG00FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DES MVTS VENANT DU MICRO ET RECYCLAGE                                  
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAJ
       ;;
(FG00FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER DE LA G.A.S (GARANTIE A SERVIR) DU MICRO A.FORESTIER         
       m_FileAssign -d SHR SORTIN ${DATA}/IF02001.NETTING.GAS
# *****   FICHIER FFMICRO (PGM BEL165) PCL EL130P                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F07.FFMICRO
# *****   FICHIER SRP ISSU DES CHAINES SP030X                                  
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FFG40AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F94.FFG40AR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FFG40AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FFG40AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FFG40AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FFG40AL
# *****   FICHIER SRP ISSU DES CHAINES SP030X                                  
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FFG30AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F94.FFG30AR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FFG30AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FFG30AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FFG30AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FFG30AL
# *****   FICHIER GAS/BERCY ISSU DU MICRO DU CONTROLE DE GESTION               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.FGASP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG00FPAJ.BFG002CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_18_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPAK
       ;;
(FG00FPAK)
       m_CondExec 00,EQ,FG00FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IEBGENER : PASSAGE DU FICHIER R�SULTANT DE 80 A 116                        
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAM PGM=IEBGENER   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAM
       ;;
(FG00FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER GAS / MICRO / SRP TRI�S                                      
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PTEM/FG00FPAJ.BFG002CP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SYSUT2 ${DATA}/PTEM/FG00FPAM.BFG002WP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_2  "                                    "
 /FIELDS FLD_CH_1_80 1 CH 80
 /COPY
 /MT_OUTFILE_ASG SYSUT2
 /REFORMAT FLD_CH_1_80,CST_1_2
_end
       m_FileSort -s SYSIN -i SYSUT1
       JUMP_LABEL=FG00FPAN
       ;;
(FG00FPAN)
       m_CondExec 00,EQ,FG00FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DES MVTS VENANT DU MICRO ET RECYCLAGE                                  
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAQ
       ;;
(FG00FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER NEM ISSU DES CHAINES NM020X                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM002AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNM002AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGR/F94.BNM002AR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.BNM002AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNM002AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BNM002AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM002AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGB/F96.BNM002AB
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BNM002AX
# *****   FICHIER ISSU BMO01P ET FILIALES  DARTY MOBILE                        
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BMO000BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BMO000BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BMO000BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BMO000BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BMO000BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BMO000BY
# *****   FICHIER GAS / MICRO / SRP TRI�S PASS� EN RECL 116                    
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/FG00FPAM.BFG002WP
# *****   FICHIER DE RECYCLAGE G.A.S                                           
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BFG002BP
# ******* FICHIER DES FACTURES PACIFICA (FG12FP)                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PMACP/F07.BFP120AP
# ******* FICHIER REMONT� DE SAP                                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.FGSAPFP
# ******* FICHIER CARTES CADEAUX NETTING (GCC00P)                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BPC002DP
# ******* FICHIER ASSURANCE PAR ABONNEMENT (HD001P)                            
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.HD0001EP
# ******* FICHIER ISSU DE LA CHAINE FC049P                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FPC049CP
# ******* FICHIER ISSU DE LA CHAINE FC049P                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FPC050CP
# ******* FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG00FPAQ.BFG002ZP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_18_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPAR
       ;;
(FG00FPAR)
       m_CondExec 00,EQ,FG00FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG002 : GENERATION DU FICHIER COMPTA POUR GL                               
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAT
       ;;
(FG00FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
# ******* M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX GROUPE                                               
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* MESSAGES D'ANOS                                                      
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *****   FICHIER GAS / MICRO / SRP / NEM TRI�S                                
       m_FileAssign -d SHR -g ${G_A5} FFMICRO ${DATA}/PTEM/FG00FPAQ.BFG002ZP
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -d SHR -g +0 DATMGD ${DATA}/PXX0/F07.BFG002EP
#                                                                              
# ******* DATE MGD                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER MGD                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FREJET ${DATA}/PTEM/FG00FPAT.BFG002IP
# ******* FICHIER RECYCLAGE ANOMALIES (SAV PSE GAS)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FRECYC ${DATA}/PXX0/F07.BFG002BP
# ******* FICHIER EN PREPARATION POUR BFG000                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFG01 ${DATA}/PTEM/FG00FPAT.BFG002DP
# ******* COMPTE-RENDU                                                         
       m_OutputAssign -c 9 -w IFG002 IMPRIM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG002 
       JUMP_LABEL=FG00FPAU
       ;;
(FG00FPAU)
       m_CondExec 04,GE,FG00FPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION DES FIC MGD                                                     
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPAX
       ;;
(FG00FPAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  ****   FICHIER DES FACTURES INTER SOCIETES FILIALES                         
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FG00FPAT.BFG002IP
# *****   FICHIER DE CUMUL FACTURES RECL=243                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BFG002JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_7 "240"
 /DERIVEDFIELD CST_1_3 "240"
 /FIELDS FLD_CH_7_3 07 CH 03
 /FIELDS FLD_CH_13_3 13 CH 03
 /CONDITION CND_1 FLD_CH_7_3 EQ CST_1_3 OR FLD_CH_13_3 EQ CST_3_7 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPAY
       ;;
(FG00FPAY)
       m_CondExec 00,EQ,FG00FPAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION DES FIC MGD                                                     
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBA
       ;;
(FG00FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  ****   FICHIER DES FACTURES INTER SOCIETES FILIALES                         
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.BFG002JP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BFG002HP
# *****   FICHIER DE CUMUL FACTURES RECL=243                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BFG002HP
# ****************************************************                         
#  EASYTRIEVE                                                                  
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBD PGM=EZTPA00    ** ID=ABT                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPBD
       ;;
(FG00FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} FICHI ${DATA}/PXX0/F07.BFG002JP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FICHO ${DATA}/PXX0/F07.BFGMGDIP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG00FPBD
       m_ProgramExec FG00FPBD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FG00FPBB
       ;;
(FG00FPBB)
       m_CondExec 00,EQ,FG00FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION DES FIC                                                         
#   TRI PREPARATION DU FICHIER POUR GL                                         
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBG
       ;;
(FG00FPBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  ****   FICHIER DES FACTURES INTER SOCIETES FILIALES                         
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FG00FPAG.FFG01AB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FFG01BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FFG01BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FFG01BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FFG01BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FFG01BY
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/FG00FPAT.BFG002DP
# *****   FICHIER DE CUMUL FACTURES RECL=243                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG00FPBG.BFG000AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_90_3 90 PD 3
 /FIELDS FLD_CH_62_14 62 CH 14
 /FIELDS FLD_PD_83_7 83 PD 7
 /FIELDS FLD_PD_186_7 186 PD 7
 /FIELDS FLD_PD_244_7 244 PD 7
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_CH_205_36 205 CH 36
 /FIELDS FLD_PD_76_7 76 PD 7
 /FIELDS FLD_CH_93_24 93 CH 24
 /FIELDS FLD_PD_55_7 55 PD 7
 /KEYS
   FLD_CH_1_39 ASCENDING,
   FLD_CH_62_14 ASCENDING,
   FLD_CH_93_24 ASCENDING,
   FLD_CH_205_36 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_55_7,
    TOTAL FLD_PD_76_7,
    TOTAL FLD_PD_83_7,
    TOTAL FLD_PD_90_3,
    TOTAL FLD_PD_186_7,
    TOTAL FLD_PD_244_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG00FPBH
       ;;
(FG00FPBH)
       m_CondExec 00,EQ,FG00FPBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG000 : ALIMENTATION DE LA TABLE RTFG01 : FACTURATION GROUPE               
#              A PARTIR DES MVTS DE FACTURES MGI (BFI015)                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBJ
       ;;
(FG00FPBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX GROUPE                                               
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* MESSAGES D'ANOS (RDAR)                                               
#    RSGA99   : NAME=RSGA99FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA99 /dev/null
# ******* MESSAGES D'ANOS                                                      
#    RSAN00   : NAME=RSAN00FP,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# ******* FICHIER DES FACTURES MGI                                             
       m_FileAssign -d SHR -g ${G_A11} FFG01 ${DATA}/PTEM/FG00FPBG.BFG000AP
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* MAJ TABLE DES FACTURES GROUPE                                        
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG06 /dev/null
#                                                                              
# ******* EDITIONS ANOMALIES DES CRITERES DE VENTILATION SUR FACTURE           
       m_OutputAssign -c 9 -w BFG000A FGVENANO
# ******* EDITIONS ANOMALIES SUR LA NATURE DES FACTURES                        
       m_OutputAssign -c 9 -w BFG000B FGNATANO
# ******* EDITIONS ANOMALIES SUR LA FREQUENCE DES FACTURES                     
       m_OutputAssign -c 9 -w BFG000C FGFRQANO
# ******* EDITIONS ANOMALIES DES FRAIS ANNEXES DES FACTURES                    
       m_OutputAssign -c 9 -w BFG000D FGANNANO
# ******* EDITIONS ANOMALIES SUR LES TAUX DES FRAIS ANNEXES DES FACTUR         
       m_OutputAssign -c 9 -w BFG000E FG10ANO
# ******* EDITIONS ANOMALIES DES ENRS QUI N ONT PU ETRE INSERES                
       m_OutputAssign -c 9 -w BFG000F FG01ANO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG000 
       JUMP_LABEL=FG00FPBK
       ;;
(FG00FPBK)
       m_CondExec 04,GE,FG00FPBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES FICHIERS DES FACTURES                                     
#  REPRISE : OUI                                                               
# **************                                                               
#  ATTENTION LE FICHIER BFG002AP EST UN FICHIER NON GDG VENANT D'UN            
#  TRANSFERT MICRO CE QUI EXPLIQUE LE FICHIER EN OUT EN MODE=I                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBM PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBM
       ;;
(FG00FPBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
# IN39     FILE  MODE=N,NAME=DAFG01G                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FFG01BD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F61.FFG01BL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F07.FFG01BP
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ -g +1 OUT4 ${DATA}/PTEM/FG00FPBM.FFG01BR
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F45.FFG01BY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/F96.BFG002AB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F89.FFG01BM
       m_FileAssign -d SHR OUT8 ${DATA}/IF02001.NETTING.GAS
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT9 ${DATA}/PEX0/F07.FFMICRO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g ${G_A12} OUT10 ${DATA}/PTEM/FG00FPAG.FFG01AB
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/FTP.AS400.DAFG01
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F07.FFG30AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/F94.FFG30AR
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/F45.FFG30AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/F89.FFG30AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT16 ${DATA}/PXX0/F91.FFG30AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT17 ${DATA}/PXX0/F61.FFG30AL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/F07.FFG40AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT19 ${DATA}/PXX0/F94.FFG40AR
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT20 ${DATA}/PXX0/F45.FFG40AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT21 ${DATA}/PXX0/F89.FFG40AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT22 ${DATA}/PXX0/F91.FFG40AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT23 ${DATA}/PXX0/F61.FFG40AL
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT24 ${DATA}/PNCGL/F61.BNM002AL
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT25 ${DATA}/PNCGP/F07.BNM002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT26 ${DATA}/PNCGR/F94.BNM002AR
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT27 ${DATA}/PNCGY/F45.BNM002AY
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT28 ${DATA}/PNCGM/F89.BNM002AM
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT29 ${DATA}/PNCGO/F16.BNM002AO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT30 ${DATA}/PNCGD/F91.BNM002AD
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT31 ${DATA}/PNCGB/F96.BNM002AB
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT32 ${DATA}/P908/SEM.BNM002AX
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT33 ${DATA}/PXX0/F99.FGASP
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ -g +1 OUT34 ${DATA}/PXX0/F75.DAFG01V
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT35 ${DATA}/PMACP/F07.BFP120AP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT36 ${DATA}/PNCGO/F16.MGINTGO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT37 ${DATA}/PXX0/F07.BPC002DP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT38 ${DATA}/PXX0/F07.HD0001EP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT39 ${DATA}/PXX0/F07.FPC049CP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT40 ${DATA}/PXX0/F07.FPC050CP
# OUT39    FILE  MODE=O,NAME=DAFG01G                                           
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPBM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG00FPBN
       ;;
(FG00FPBN)
       m_CondExec 16,NE,FG00FPBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO DES FICHIERS DAFG01G QUOTIDIEN DANS UN FICHIER HISTO                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBQ
       ;;
(FG00FPBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ------------- REPRO FIC GENERIX DANS FIC HISTO                               
       m_FileAssign -d SHR IN1 ${DATA}/PXX0/FTP.GENERIX.DAFG01G
# ------------- FICHIER HISTORIQUE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 85 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/GENERIX.HISTO.DAFG01G
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPBQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG00FPBR
       ;;
(FG00FPBR)
       m_CondExec 16,NE,FG00FPBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SUPPRESSION DES GENERATIONS DES FICHIERS DAFG01G SI TRAITEMENT OK           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBT PGM=IDCAMS     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBT
       ;;
(FG00FPBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPBT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG00FPBU
       ;;
(FG00FPBU)
       m_CondExec 16,NE,FG00FPBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UNE GENERATION A VIDE DU FICHIER DAFG01G POUR NE PAS             
#  PLANTER LE LENDEMAIN SI PAS DE TRANSFERT                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPBX PGM=IDCAMS     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPBX
       ;;
(FG00FPBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 85 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.GENERIX.DAFG01G
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPBX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG00FPBY
       ;;
(FG00FPBY)
       m_CondExec 16,NE,FG00FPBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTFG00FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPCA PGM=EZACFSM1   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPCA
       ;;
(FG00FPCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FG00FPCA.FTFG00FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFG00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG00FPCD PGM=FTP        ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPCD
       ;;
(FG00FPCD)
       m_CondExec ${EXADH},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPCD.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FG00FPCA.FTFG00FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG00FPZA
       ;;
(FG00FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG00FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
