#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IF01FP.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIF01F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/15 AT 09.49.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: IF01FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIF040 : EXTRACTION                                                         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IF01FPA
       ;;
(IF01FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+3'}
       G_A6=${G_A6:-'+3'}
       RUN=${RUN}
       JUMP_LABEL=IF01FPAA
       ;;
(IF01FPAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
# **    DEPENDANCE PLAN        ****                                            
# *********************************                                            
# ******* TABLES EN LECTURE                                                    
#    RSIF02   : NAME=RSIF02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF02 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ********FICHIER EN ENTR�E PROVENANT DE LA CHAINE IF00FP                      
       m_FileAssign -d SHR -g +0 FCARTBQE ${DATA}/PXX0/F07.BIF040AP
# ********FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FPAIECC ${DATA}/PXX0/F99.BIF040BP
# ********FICHIER FDATE DU JOUR                                                
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIF040 
       JUMP_LABEL=IF01FPAB
       ;;
(IF01FPAB)
       m_CondExec 04,GE,IF01FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT   : ECLATEMENT PAR FILIALES                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAD
       ;;
(IF01FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F99.BIF040BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F07.BIF040PP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F45.BIF040YP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F16.BIF040OP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "945"
 /DERIVEDFIELD CST_1_14 "961"
 /DERIVEDFIELD CST_3_18 "916"
 /DERIVEDFIELD CST_1_3 "907"
 /DERIVEDFIELD CST_3_10 "989"
 /FIELDS FLD_CH_48_3 48 CH 3
 /CONDITION CND_3 FLD_CH_48_3 EQ CST_1_14 OR FLD_CH_48_3 EQ CST_3_18 
 /CONDITION CND_2 FLD_CH_48_3 EQ CST_1_6 OR FLD_CH_48_3 EQ CST_3_10 OR 
 /CONDITION CND_1 FLD_CH_48_3 EQ CST_1_3 
 /COPY
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IF01FPAE
       ;;
(IF01FPAE)
       m_CondExec 00,EQ,IF01FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTIF01FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAG
       ;;
(IF01FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/IF01FPAG.FTIF01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIF01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAJ
       ;;
(IF01FPAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.IF01FPAG.FTIF01FP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTIF01FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAM
       ;;
(IF01FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A3} SYSOUT ${DATA}/PTEM/IF01FPAG.FTIF01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIF01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAQ
       ;;
(IF01FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.IF01FPAG.FTIF01FP(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTIF01FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAT
       ;;
(IF01FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A5} SYSOUT ${DATA}/PTEM/IF01FPAG.FTIF01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIF01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPAX
       ;;
(IF01FPAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.IF01FPAG.FTIF01FP(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# **   RAZ DU FICHIER BIF040AP                                                 
# **   REPRISE : OUI                                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF01FPBA PGM=IDCAMS     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPBA
       ;;
(IF01FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ****    FICHIER DE BIF040AP CADO CARTE                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.BIF040AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPBA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF01FPBB
       ;;
(IF01FPBB)
       m_CondExec 16,NE,IF01FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IF01FPZA
       ;;
(IF01FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF01FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
