#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FC170L.ksh                       --- VERSION DU 08/10/2016 22:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLFC170 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 06/04/03 AT 17.13.51 BY PREPA2                       
#    STANDARDS: P  JOBSET: FC170L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='LILLE'                                                             
# ********************************************************************         
#  BFC0170 : EXTRACTION DES COMMANDES CLIENTS AVEC DEMANDE DE FACTURE          
# --------------------------------------------------------------------         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FC170LA
       ;;
(FC170LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=FC170LAA
       ;;
(FC170LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SIMU/REEL                                                  
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FC170LAA
# ******  TABLE EN LECTURE                                                     
#    RSBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS00 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGV02   : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSPR00   : NAME=RSPR00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSGS65   : NAME=RSGS65L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS65 /dev/null
#    RSFM04   : NAME=RSFM04L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04 /dev/null
#    RSFM05   : NAME=RSFM05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM05 /dev/null
# ******  TABLE EN MISE A JOUR                                                 
#    RSGV10   : NAME=RSGV10L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  FICHIER DES CDES CLIENTS AVEC DEMANDE DE FACTURES                    
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -g +1 FFC170 ${DATA}/PXX0/F61.FFC170L
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFC170 
       JUMP_LABEL=FC170LAB
       ;;
(FC170LAB)
       m_CondExec 04,GE,FC170LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
