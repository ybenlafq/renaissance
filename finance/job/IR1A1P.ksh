#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IR1A1P.ksh                       --- VERSION DU 08/10/2016 13:43
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIR1A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/11/28 AT 11.39.30 BY PREPA2                       
#    STANDARDS: P  JOBSET: IR1A1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION POUR FIC SAP                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IR1A1PA
       ;;
(IR1A1PA)
#
#IR1A1PAG
#IR1A1PAG Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#IR1A1PAG
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON MONDAY    2011/11/28 AT 11.39.30 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: IR1A1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-IR1A1P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IR1A1PAA
       ;;
(IR1A1PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
# *****   FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PXX0/F07.BIR010PP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT2 ${DATA}/PXX0/F16.BIR010PO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT3 ${DATA}/PXX0/F45.BIR010PY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT4 ${DATA}/PXX0/F61.BIR010PL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT5 ${DATA}/PXX0/F89.BIR010PM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT6 ${DATA}/PXX0/F91.BIR010PD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IR1A1PAA.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IR1A1PAB
       ;;
(IR1A1PAB)
       m_CondExec 16,NE,IR1A1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IR1A1PAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
