#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FC170G.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGFC170 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/21 AT 16.36.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: FC170G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='GROUPE'                                                            
# ********************************************************************         
# **************************************                                       
# *  DELETE DU FICHIER ZIPPER        ***                                       
# **************************************                                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FC170GA
       ;;
(FC170GA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FC170GAA
       ;;
(FC170GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170GAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FC170GAB
       ;;
(FC170GAB)
       m_CondExec 16,NE,FC170GAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE DES FICHIERS FFC170                                                   
# --------------------------------------------------------------------         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAD
       ;;
(FC170GAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FFC170P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FFC170Y
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FFC170M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FFC170D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FFC170L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FFC170O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FPC170AP
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FC170GAD.BFC170AG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_18 2 CH 18
 /KEYS
   FLD_CH_2_18 ASCENDING
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC170GAE
       ;;
(FC170GAE)
       m_CondExec 00,EQ,FC170GAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFC175  :                                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAG
       ;;
(FC170GAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} FFC170 ${DATA}/PTEM/FC170GAD.BFC170AG
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 FFC175 ${DATA}/PXX0/F99.FFC170G
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFC175 
       JUMP_LABEL=FC170GAH
       ;;
(FC170GAH)
       m_CondExec 04,GE,FC170GAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BFC175                                               
# --------------------------------------------------------------------         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAJ
       ;;
(FC170GAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F99.FFC170G
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FFC170GT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_18 2 CH 18
 /KEYS
   FLD_CH_2_18 ASCENDING
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC170GAK
       ;;
(FC170GAK)
       m_CondExec 00,EQ,FC170GAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFC177  : ECLATEMENT MAG DARTY/FRANCHISE                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAM
       ;;
(FC170GAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE EN LECTURE                                                     
#    RSLI00   : NAME=RSLI00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FFC175 ${DATA}/PXX0/F99.FFC170GT
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 FFCDTY ${DATA}/PXX0/F99.FC177DAP
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 FFCFRA ${DATA}/PXX0/F99.FC177FAP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFC177 
       JUMP_LABEL=FC170GAN
       ;;
(FC170GAN)
       m_CondExec 04,GE,FC170GAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FRANCHISE ISSU DU BFC177                                     
# --------------------------------------------------------------------         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAQ
       ;;
(FC170GAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F99.FC177FAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.FC177FHP
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FC177FHP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_18 2 CH 18
 /KEYS
   FLD_CH_2_18 ASCENDING
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC170GAR
       ;;
(FC170GAR)
       m_CondExec 00,EQ,FC170GAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DARTY ISSU DU BFC177                                         
# --------------------------------------------------------------------         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAT
       ;;
(FC170GAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F99.FC177DAP
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FC177DTP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_18 2 CH 18
 /KEYS
   FLD_CH_2_18 ASCENDING
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FC170GAU
       ;;
(FC170GAU)
       m_CondExec 00,EQ,FC170GAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  RENAME DU FICHIER CUMUL                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FC170GAX
       ;;
(FC170GAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A6} IN1 ${DATA}/PXX0/F99.FC177DTP
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ OUT1 ${DATA}/PXX0.F99.FFC170AG
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170GAX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FC170GAY
       ;;
(FC170GAY)
       m_CondExec 16,NE,FC170GAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DU FICHER FACTURE SAGE                                                  
# ********************************************************************         
# ABO      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    DATA  *,MBR=FC170AG                                                 
# -ECHO                                                                        
# -NOSYSIN                                                                     
# -ACTION(ADD)                                                                 
# -TRAN(ASCII850)                                                              
# -ARCHIVE_SPACE_PRIMARY(00001500)                                             
# -ARCHIVE_SPACE_SECONDARY(00001500)                                           
# -ARCHUNIT(TEM350)                                                            
# -ARCHIVE(PXX0.F99.FFC171AG)                                                  
# -ZIPPED_DSN(PXX0.F99.FFC170AG,FFC170AG.TXT)                                  
# PXX0.F99.FFC170AG                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GBA PGM=JVMLDM76   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FC170GBA
       ;;
(FC170GBA)
       m_CondExec ${EXABO},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ FICZIP ${DATA}/PXX0.F99.FFC171AG
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170AG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FFC171AG                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFC170G                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FC170GBD
       ;;
(FC170GBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170GBD.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#               CREATION D'UNE GéNéRATION VIDE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FC170GBG PGM=IDCAMS     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FC170GBG
       ;;
(FC170GBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 473 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FPC170AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170GBG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FC170GBH
       ;;
(FC170GBH)
       m_CondExec 16,NE,FC170GBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FC170GZA
       ;;
(FC170GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FC170GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
