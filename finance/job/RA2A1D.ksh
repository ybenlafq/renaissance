#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA2A1D.ksh                       --- VERSION DU 08/10/2016 22:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDRA2A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/09/26 AT 17.35.49 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RA2A1D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CREATION D'UN FICHIER DE REPRISE EN CAS DE PLANTAGE                 
#          DE LA CHAINE RA200D                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RA2A1DA
       ;;
(RA2A1DA)
#
#RA2A1DAM
#RA2A1DAM Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#RA2A1DAM
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2007/09/26 AT 17.35.49 BY BURTEC2                
# *    JOBSET INFORMATION:    NAME...: RA2A1D                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-RA200D'                            
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RA2A1DAA
       ;;
(RA2A1DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************************                                          
# ******* FIC VENANT DE LA CHAINE AV600D                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BAV600AD
# ******* FIC DE RECYCLAGE EN CAS DE PLANTAGE LA VEILLE (RA200D)               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.RA20RECD
# ******* FIC REJET DE LA VEILLE (RA200D)                                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.RA20REJD
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 SORTOUT ${DATA}/PXX0/F91.RA20RECD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /KEYS
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_17_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA2A1DAB
       ;;
(RA2A1DAB)
       m_CondExec 00,EQ,RA2A1DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE � Z�RO DU FICHIER DE LA CHAINE AV600D (BAV600AD)             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA2A1DAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA2A1DAD
       ;;
(RA2A1DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC DE RECYCLAGE REMIS � Z�RO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 OUT1 ${DATA}/PXX0/F91.BAV600AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA2A1DAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA2A1DAE
       ;;
(RA2A1DAE)
       m_CondExec 16,NE,RA2A1DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE � Z�RO DU FICHIER RA20REJD APRES COPY DS LE RA20RECD         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA2A1DAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA2A1DAG
       ;;
(RA2A1DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC DE RECYCLAGE REMIS � Z�RO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 OUT1 ${DATA}/PXX0/F91.RA20REJD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA2A1DAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA2A1DAH
       ;;
(RA2A1DAH)
       m_CondExec 16,NE,RA2A1DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA2A1DAJ PGM=CZX2PTRT   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
