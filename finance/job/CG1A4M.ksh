#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CG1A4M.ksh                       --- VERSION DU 08/10/2016 13:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMCG1A4 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/29 AT 17.44.04 BY PREPA2                       
#    STANDARDS: P  JOBSET: CG1A4M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CONSTITUTION DU FICHIER REPRISE POUR S.A.P EN CAS DE PLANTA         
#          DE LA CHAINE CGSAPM                                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ECS                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CG1A4MA
       ;;
(CG1A4MA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2009/01/29 AT 17.44.04 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: CG1A4M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-CGSAPM'                            
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CG1A4MAA
       ;;
(CG1A4MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FG14FPM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.IF00FPM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.PSE00M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GRA00M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GBA00M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GBA02M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.AV001M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.AV201M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.CC200M
# ******* FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BCG000M.REPRISE
# ******* FICHIER REPRISE FUSIONNE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/F89.BCG000M.REPRISE
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_98_25 98 CH 25
 /FIELDS FLD_CH_18_10 18 CH 10
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CG1A4MAB
       ;;
(CG1A4MAB)
       m_CondExec 00,EQ,CG1A4MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
