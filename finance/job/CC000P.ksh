#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC000P.ksh                       --- VERSION DU 09/10/2016 00:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCC000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/14 AT 17.45.12 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CC000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#       *  TRI DU FICHIER RECYCLAGE NEM                                        
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC000PA
       ;;
(CC000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CC000PAA
       ;;
(CC000PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
#                                                                              
# ******* FIC RECYCLAGE NEM                                                    
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM001BP
#                                                                              
# ******* FIC EN SORTIE POUR PGM BCC000                                        
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -g +1 SORTOUT ${DATA}/PTEM/CC000PAA.BCC000AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 " "
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_1_8 1 CH 08
 /FIELDS FLD_CH_20_6 20 CH 06
 /CONDITION CND_3 FLD_CH_26_3 NE CST_1_7 
 /KEYS
   FLD_CH_1_8 ASCENDING
 /SUMMARIZE
 /INCLUDE CND_3
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_20_6,FLD_CH_1_8,FLD_CH_26_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=CC000PAB
       ;;
(CC000PAB)
       m_CondExec 00,EQ,CC000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          TRI DU FICHIER RECYCLAGE ICS (ISSU DE LA CHAINE FTICSL)             
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CC000PAD
       ;;
(CC000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC RECYCLAGE ICS                                                    
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FTRECYDP
#                                                                              
# ******* FIC EN SORTIE POUR PGM BCC000                                        
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -g +1 SORTOUT ${DATA}/PTEM/CC000PAD.BCC000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_11 "CAI"
 /DERIVEDFIELD CST_1_7 "NEM"
 /FIELDS FLD_CH_1_8 1 CH 08
 /FIELDS FLD_CH_102_6 102 CH 6
 /FIELDS FLD_CH_377_3 377 CH 3
 /FIELDS FLD_CH_370_3 370 CH 3
 /FIELDS FLD_CH_144_8 144 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_3 FLD_CH_1_3 EQ CST_1_7 AND FLD_CH_370_3 EQ CST_3_11 
 /KEYS
   FLD_CH_1_8 ASCENDING
 /SUMMARIZE
 /INCLUDE CND_3
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_102_6,FLD_CH_144_8,FLD_CH_377_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=CC000PAE
       ;;
(CC000PAE)
       m_CondExec 00,EQ,CC000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCC000 **  COBOL2/DB2 .CREATION D UN FICHIER POUR LOAD RTCC00           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CC000PAG
       ;;
(CC000PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC DES RECYCLAGES NEM TRIE                                          
       m_FileAssign -d SHR -g ${G_A1} FRECNEM ${DATA}/PTEM/CC000PAA.BCC000AP
#                                                                              
# ******* FIC DES RECYCLAGES ICS TRIE                                          
       m_FileAssign -d SHR -g ${G_A2} FRECICS ${DATA}/PTEM/CC000PAD.BCC000BP
#                                                                              
# ******* TABLE  DB2 EN LECTURE                                                
#                                                                              
#    RSFT34   : NAME=RSFT34,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT34 /dev/null
# ******* TABLE FT                                                             
#    RSCC01   : NAME=RSCC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSCC01 /dev/null
#    RSMQ15   : NAME=RSMQ15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMQ15 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
#         FIC EN SORTIE POUR LOAD RTCC00 (LRECL27)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 FCAIREC ${DATA}/PTEM/CC000PAG.BCC000CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC000 
       JUMP_LABEL=CC000PAH
       ;;
(CC000PAH)
       m_CondExec 04,GE,CC000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTCC00                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC000PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CC000PAJ
       ;;
(CC000PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ****** FIC DE LOAD                                                           
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PTEM/CC000PAG.BCC000CP
#    RSCC00   : NAME=RSCC00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSCC00 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CC000PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/CC000P_CC000PAJ_RTCC00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=CC000PAK
       ;;
(CC000PAK)
       m_CondExec 04,GE,CC000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *************************                                                    
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CC000PZA
       ;;
(CC000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CC000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
