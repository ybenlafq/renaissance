#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CG1A2F.ksh                       --- VERSION DU 08/10/2016 13:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFCG1A2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/10 AT 15.43.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CG1A2F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CONSTITUTION DU FICHIER REPRISE POUR S.A.P EN CAS DE PLANTA         
#          DE LA CHAINE CGICSF                                                 
#                                                                              
# ********************************************************************         
#  TRI DES FICHIERS ICS DIF                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CG1A2FA
       ;;
(CG1A2FA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/07/10 AT 15.43.28 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CG1A2F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-CGICSF'                            
# *                           APPL...: IMPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CG1A2FAA
       ;;
(CG1A2FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DES FV001 ET DES CC200                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FTI01D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FTI01L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FTI01M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FTI01O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FTI01Y
# ******* FICHIER DE RECYCLAGE                                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.CGICSF0
# ******* FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.CGICSF.REPRISE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.CGICSF.REPRISE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_235_1 235 CH 1
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_10 18 CH 10
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_235_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CG1A2FAB
       ;;
(CG1A2FAB)
       m_CondExec 00,EQ,CG1A2FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
