#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IF00FP.ksh                       --- VERSION DU 20/10/2016 12:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIF00F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/24 AT 09.57.58 BY BURTEC2                      
#    STANDARDS: P  JOBSET: IF00FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DU  TABLESPACE RSIF10 AVANT MAJ                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IF00FPA
       ;;
(IF00FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+2'}
       G_A42=${G_A42:-'+2'}
       G_A43=${G_A43:-'+2'}
       G_A44=${G_A44:-'+2'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IF00FPAA
       ;;
(IF00FPAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
# **    DEPENDANCE PLAN        ****                                            
# *********************************                                            
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QIF00FP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/IF00FPAA
       m_ProgramExec IEFBR14 "RMAC,IF00FP.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPAD
       ;;
(IF00FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  CUMUL FICHERS CAR907AP                                               
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.CAR907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BIF040AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BIF040AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPAE
       ;;
(IF00FPAE)
       m_CondExec 00,EQ,IF00FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DE LA VALEUR DU RBA                                                
#    REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME ET BACKOUT=JOBSET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAG PGM=IEBGENER   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAG
       ;;
(IF00FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QIF00FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPAH
       ;;
(IF00FPAH)
       m_CondExec 00,EQ,IF00FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFX901 : CHARGEMENT DE LA TABLE RTFX90 NETTING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAJ
       ;;
(IF00FPAJ)
       m_CondExec ${EXAAP},NE,YES 
# ******* UNLOAD DE LA TABLE FX00 DU GROUPE (BGA01G)                           
       m_FileAssign -d SHR -g +0 FFX90 ${DATA}/PNCGG/F99.FX00M907
# ********TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFX90   : NAME=RSFX90P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFX90 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFX901 
       JUMP_LABEL=IF00FPAK
       ;;
(IF00FPAK)
       m_CondExec 04,GE,IF00FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTFM93 A PARTIR DU TS RSFM93G :JOB UNLOAD BGA01G         
#  SOUS RMAC                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAM
       ;;
(IF00FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSFM93   : NAME=RSFM93FP,MODE=(U,N) - DYNAM=YES                           
       m_FileAssign -d SHR RSFM93 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGG/F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IF00FP_IF00FPAM_RTFM93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPAN
       ;;
(IF00FPAN)
       m_CondExec 04,GE,IF00FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIF00CF  REFORMATAGE DU FICHIER SOFINCO ---> SOFDAR ---> FINASOF            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAQ PGM=BIF00CF    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAQ
       ;;
(IF00FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER PARAMETRE SOFINCO                                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PARMSOF
# ******  FICHIER SOFINCO NOUVEAU FORMAT EN PROVENANCE DE SOFINCO (200         
       m_FileAssign -d SHR -g +0 FSOFNEW ${DATA}/PXX0/FTP.F07.SOFINCO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SOFREPRI
# ******  FICHIER SOFINCO ANCIEN FORMAT RECONDUIT POUR DARTY (140)             
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FSOFINCO ${DATA}/PNCGP/F07.SOFDAR
       m_ProgramExec BIF00CF 
#                                                                              
# ********************************************************************         
#  BIF00CF  REFORMATAGE DU FICHIER EVOLIS                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAT PGM=BIF00CF    ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAT
       ;;
(IF00FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER PARAMETRE SOFINCO                                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PARMLOA
# ******  FICHIER SOFINCO NOUVEAU FORMAT EN PROVENANCE DE SOFINCO (200         
       m_FileAssign -d SHR -g +0 FSOFNEW ${DATA}/PXX0/FTP.F07.FEVOLLOA
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FLOAREPR
# ******  FICHIER SOFINCO ANCIEN FORMAT RECONDUIT POUR DARTY (140)             
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FSOFINCO ${DATA}/PXX0/F07.FLOADAR
       m_ProgramExec BIF00CF 
#                                                                              
# ********************************************************************         
#  BIF00SG  REFORMATAGE DU FICHIER JRB PROVENANT DE LA GATEWAY                 
#           TRANSACTIONS CB DE LA SG POUR DARTY.COM                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPAX PGM=BIF00SG    ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPAX
       ;;
(IF00FPAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER JRB PROVENANT DE LA GATEWAY EN 500                           
       m_FileAssign -d SHR -g +0 FIFJRBI ${DATA}/PXX0/FTP.F07.BIFJRBAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.BIFJMDAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.JRBREPRI
# ******  FICHIER JRB EN 80                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FIFJRBO ${DATA}/PXX0/F07.BIFJRBBP
# ******  FICHIER EN 150                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIFCOF ${DATA}/PXX0/F07.BIFCOFAP
       m_ProgramExec BIF00SG 
#                                                                              
# ********************************************************************         
# **  PGM BIF000 EXTRACTION DES DIFFERENTS ORGANISMES BANCAIRE                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBA
       ;;
(IF00FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DE CONTROLE ET SUIVI                                           
#    RSIF10   : NAME=RSIF10,MODE=U - DYNAM=YES                                 
# -X-IF00FPR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSIF10 /dev/null
#                                                                              
# ******* FIC DE LOA                                                           
       m_FileAssign -d SHR -g ${G_A2} FSOFLOA ${DATA}/PXX0/F07.FLOADAR
# ******* FIC DE KYRIEL                                                        
       m_FileAssign -d SHR FKYRIEL /dev/null
# ******* FIC DE SOFINCO REMIS AU LRECL DARTY                                  
       m_FileAssign -d SHR -g ${G_A3} FSOFINCO ${DATA}/PNCGP/F07.SOFDAR
# ******* FIC DE CETELEM                                                       
       m_FileAssign -d SHR -g +0 FCETELEM ${DATA}/PXX0/FTP.F07.CETELEM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CETREPRI
# ******* FIC DE CARTES BANCAIRES                                              
       m_FileAssign -d SHR -g +0 FCARTBQE ${DATA}/PXX0/FTP.F07.CAR907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CARREPRI
# ******* FIC JRB (STE ATOS)                                                   
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PXX0/F07.BIFJRBBP
# ******* FIC DE COFINOGA                                                      
       m_FileAssign -d SHR -g ${G_A5} FCOFINO ${DATA}/PXX0/F07.BIFCOFAP
# ******* FIC DES CARTES AMERICAN EXPRESS                                      
       m_FileAssign -d SHR -g +0 FCARTAMX ${DATA}/PXX0/FTP.F07.AME907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.AMEREPRI
# ******* FIC DE TELETRAITEMENT                                                
       m_FileAssign -d SHR -g +0 FTELEPAI ${DATA}/PXX0/F07.TLV907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.TELREPRI
# ******* FIC PROMOSTIM                                                        
       m_FileAssign -d SHR -g +0 FPROMOST ${DATA}/PXX0/FTP.F07.PROMOSTP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.PROREPRI
# ******* FIC DES CARTES BANCAIRES DES MAG DE LUXEMBOURG (STE CETREL)          
       m_FileAssign -d SHR -g +0 FCETREL ${DATA}/PXX0/FTP.F07.CETREL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CTRREPRI
# ******* FIC PAIEMENTS PAYPAL                                                 
       m_FileAssign -d SHR -g +0 FPAYPAL ${DATA}/PXX0/FTP.F07.PAYPAL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.PAYREPRI
# ******* FIC IMAGE CHEQUE (STE SAFIG)                                         
       m_FileAssign -d SHR -g +0 FIMCHEQ ${DATA}/PXX0/F07.SAFIGBNP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SFIBNPR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SAFIGLCL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SFILCLR
# ******* FIC IMAGE CHEQUE (STE EXTELIA)                                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.EXTLIBNP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.EXTBNPR
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ***** FIC FPARAM OPTIONNEL PERMET DE TRAITER QUE CERTAINS ORGANISMES         
       m_FileAssign -d SHR FPARAM /dev/null
# ******* FIC DE SORTIE STANDARD DARTY (192 DE LONG)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 FIF000 ${DATA}/PMACP/F07.BIF000AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIF000 
       JUMP_LABEL=IF00FPBB
       ;;
(IF00FPBB)
       m_CondExec 04,GE,IF00FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBD
       ;;
(IF00FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER STANDARD DARTY                                               
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PMACP/F07.BIF000AP
# *****   FICHIER DE RECYCLAGE ANOMALIES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 FICMGD ${DATA}/PTEM/IF00FPBD.BIFMGDAP
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 FICDTY ${DATA}/PXX0/F07.BIFDTYAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /FIELDS   FD_67_08_CH 67 8 CH,
           FD_CH_31_10 31 10 CH,
           FD_CH_31_07 31 07 CH,
           FD_CH_31_13 31 13  CH
/CONDITION CND_1 FD_67_08_CH LT "20150101" AND
                 ( FD_CH_31_10 = "9492676267"   OR
                   FD_CH_31_07 = "2480676"      OR
                   FD_CH_31_07 = "0370885"      OR
                   FD_CH_31_13 = "ZVZW3ZTDWE6WS"
                 )
/COPY
/MT_OUTFILE_ASG FICMGD
/INCLUDE CND_1
/MT_OUTFILE_ASG FICDTY
/OMIT  CND_1
 /COPY
 /MT_OUTFILE_ASG FICMGD
 /MT_OUTFILE_ASG FICDTY
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IF00FPBE
       ;;
(IF00FPBE)
       m_CondExec 00,EQ,IF00FPBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBG PGM=EZTPA00    ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBG
       ;;
(IF00FPBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} FICHI ${DATA}/PTEM/IF00FPBD.BIFMGDAP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FICHO ${DATA}/PXX0/F07.BIFMGDCP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/IF00FPBG
       m_ProgramExec IF00FPBG
# ********************************************************************         
#   TRI DES MVTS                                                               
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBJ
       ;;
(IF00FPBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER STANDARD DARTY                                               
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/IF00FPBD.BIFMGDAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BIFMGDBP
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BIFMGDBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPBK
       ;;
(IF00FPBK)
       m_CondExec 00,EQ,IF00FPBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DES MVTS VENANT DU FIC STANDARD DARTY + RECYCLAGE EVENTUELS            
#   DU FICHIER D ANOMALIES DU TRAITEMENT PRECEDENT                             
#      TRI CODE ORGANISME              1,3                                     
#          DATE DE RECEPTION           4,8                                     
#          HEURE DE RECEPTION         12,4                                     
#          IDENTIFIANT                31,20                                    
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBM
       ;;
(IF00FPBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER STANDARD DARTY                                               
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F07.BIFDTYAP
# *****   FICHIER DE RECYCLAGE ANOMALIES                                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PMACP/F07.BIF001AP.FINARECY
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPBM.BIF002AP
#  SORT DU FICHIER CARTE BANCAIRE POUR RE CREDITATION                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_8 4 CH 8
 /FIELDS FLD_CH_12_4 12 CH 4
 /FIELDS FLD_CH_31_20 31 CH 20
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_8 ASCENDING,
   FLD_CH_12_4 ASCENDING,
   FLD_CH_31_20 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPBN
       ;;
(IF00FPBN)
       m_CondExec 00,EQ,IF00FPBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBQ
       ;;
(IF00FPBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.CAR907AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.RAR907AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "54"
 /DERIVEDFIELD CST_3_8 "57"
 /FIELDS FLD_CH_1_37 1 CH 37
 /FIELDS FLD_CH_1_2 1 CH 2
 /CONDITION CND_1 FLD_CH_1_2 EQ CST_1_4 OR FLD_CH_1_2 EQ CST_3_8 
 /KEYS
   FLD_CH_1_37 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPBR
       ;;
(IF00FPBR)
       m_CondExec 00,EQ,IF00FPBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIF010 : CREATION DES LIGNES DE COMPTABILISATION                            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBT
       ;;
(IF00FPBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES SOCIETES COMPTABLES                                        
#    RSIF00   : NAME=RSIF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF00 /dev/null
# ******* TABLE COUPLE SOCIETE COMPTABLE/ORGANISME                             
#    RSIF01   : NAME=RSIF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF01 /dev/null
# ******* TABLE COUPLE ORGANISME/IDENTIFIANT                                   
#    RSIF02   : NAME=RSIF02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF02 /dev/null
# ******* TABLE REGLES DE COMPTABILISATION                                     
#    RSIF03   : NAME=RSIF03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF03 /dev/null
# ******* TABLE CODIFICATION DES ECS                                           
#    RSIF04   : NAME=RSIF04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF04 /dev/null
# ******* TABLE CODIFICATION DES LIBELLES                                      
#    RSIF05   : NAME=RSIF05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF05 /dev/null
# ******* TABLE PERIODE DE COMPTABILISATION                                    
#    RSIF06   : NAME=RSIF06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF06 /dev/null
# ******* TABLE CODIFICATION DES REGLES                                        
#    RSIF07   : NAME=RSIF07,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF07 /dev/null
# ******* TABLE CODIFICATION DES REGLES DE LETTRAGE                            
#    RSIF08   : NAME=RSIF08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF08 /dev/null
# ******* TABLE DE CONTROLE ET SUIVI                                           
#    RSIF10   : NAME=RSIF10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF10 /dev/null
# ******* TABLE                                                                
#    RSFX00   : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE                                                                
#    RSFX90   : NAME=RSFX90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX90 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A10} FIF000 ${DATA}/PTEM/IF00FPBM.BIF002AP
# ******* FICHIER RECYCLAGE ANOMALIES                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 FIFANO ${DATA}/PMACP/F07.BIF001AP.FINARECY
# ******* FICHIER DES REJETS NON RECYCLABLES (LRECL 192) (NE SERT PLUS         
       m_FileAssign -d SHR FIFREJ /dev/null
# ******* FICHIER DETAILS DES MVTS EN CUMULS (LRECL 192) (NE SERT PLUS         
       m_FileAssign -d SHR FDETAIL /dev/null
# ******* FICHIER STAT POUR MICRO (LRECL 080)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FIFSTAT ${DATA}/PTEM/IF00FPBT.BIF005AP
# ******* FICHIER POUR GENERATEUR D ETAT (LRECL 512)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC1 ${DATA}/PTEM/IF00FPBT.BIF006AP
# ******* FICHIER POUR GENERATEUR D ETAT MICROFICHE (LRECL 512)                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC2 ${DATA}/PTEM/IF00FPBT.BIF007AP
# ******* FICHIER POUR GENERATEUR D ETAT IIF010 (ETAT)                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC3 ${DATA}/PTEM/IF00FPBT.BIF013AP
# ******* FICHIER POUR GENERATEUR D ETAT IIF020 (PAPIER)                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC4 ${DATA}/PTEM/IF00FPBT.BIF014AP
# ******* FICHIER POUR GENERATEUR D ETAT IIF040 (PAPIER)                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC5 ${DATA}/PTEM/IF00FPBT.BIF015AP
# ******* FICHIER DES LIGNES A COMPTABILISER (LRECL 192)                       
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 FIF010 ${DATA}/PTEM/IF00FPBT.BIF008AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIF010 
       JUMP_LABEL=IF00FPBU
       ;;
(IF00FPBU)
       m_CondExec 04,GE,IF00FPBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER FIF010 ISSU DU PGM BIF010                                   
#      SOCIETE ETABLISSEMENT           1,6                                     
#      MODE DE PAIEMENT               53,3                                     
#      CODE JOURNAL                  188,3                                     
#      DATE DE FINANCEMENT            90,08                                    
#      CHAMP DE CUMUL                 25,20                                    
#      TYPE DE MONTANT               176,05                                    
#      CLE COMPTABLE                   7,18                                    
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPBX
       ;;
(IF00FPBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DES LIGNES A COMPTABILISER (LRECL 192)                       
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IF00FPBT.BIF008AP
# *****   FICHIER DE SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BIF008BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_144_4 144 PD 4
 /FIELDS FLD_PD_167_6 167 PD 6
 /FIELDS FLD_CH_53_3 53 CH 3
 /FIELDS FLD_CH_90_8 90 CH 8
 /FIELDS FLD_CH_25_20 25 CH 20
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_188_3 188 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_53_3 ASCENDING,
   FLD_CH_188_3 ASCENDING,
   FLD_CH_90_8 ASCENDING,
   FLD_CH_25_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_144_4,
    TOTAL FLD_PD_167_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPBY
       ;;
(IF00FPBY)
       m_CondExec 00,EQ,IF00FPBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIF500 : CREATION DES INTERFACES COMPTA POUR GCT + COSYS                    
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCA
       ;;
(IF00FPCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE REGLES DE COMPTABILISATION                                     
#    RSIF03   : NAME=RSIF03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF03 /dev/null
# ******* TABLE CODIFICATION DES LIBELLES                                      
#    RSIF05   : NAME=RSIF05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF05 /dev/null
# ******* TABLE PERIODE DE COMPTABILISATION                                    
#    RSIF06   : NAME=RSIF06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF06 /dev/null
# ******* TABLE CODIFICATION DES REGLES DE LETTRAGE                            
#    RSIF08   : NAME=RSIF08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF08 /dev/null
# ******* TABLE                                                                
#    RSFG11   : NAME=RSFG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG11 /dev/null
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A12} FIF010 ${DATA}/PXX0/F99.BIF008BP
# ******* FICHIER GCT A DESTINATION DU FV001P (LRECL 200)                      
# ******* UN FICHIER POUR CHAQUE SOCIETE A TRAITER                             
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT907 ${DATA}/PXX0/F07.IF00FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT945 ${DATA}/PXX0/F45.IF00FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT989 ${DATA}/PXX0/F89.IF00FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT961 ${DATA}/PXX0/F61.IF00FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT991 ${DATA}/PXX0/F91.IF00FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT996 ${DATA}/PXX0/F96.IF00FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT997 ${DATA}/PXX0/F44.IF00FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT916 ${DATA}/PXX0/F16.IF00FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGCT908 ${DATA}/PXX0/F08.IF00FPX
       m_FileAssign -d SHR FGCT994 /dev/null
       m_FileAssign -d SHR FGCT975 /dev/null
# ******* FICHIER COSYS (NE SERT PLUS)                                         
       m_FileAssign -d SHR FIFCOS /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIF500 
       JUMP_LABEL=IF00FPCB
       ;;
(IF00FPCB)
       m_CondExec 04,GE,IF00FPCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER FIFSTAT ISSU DU PGM BIF010                                  
#      DATE DE CREATION                1,8                                     
#      DATE DE FINANCEMENT             9,8                                     
#      CODE ORGANISME                 17,3                                     
#      TYPE DE CREDIT                 20,1                                     
#      MODE DE PAIEMENT               21,3                                     
#      SOCIETE                        24,3                                     
#      LIEU                           27,3                                     
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCD
       ;;
(IF00FPCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER STAT POUR MICRO (LRECL 080)                                  
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/IF00FPBT.BIF005AP
# ******* FICHIER STAT POUR MICRO TRIE                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPCD.BIF005BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_3 21 CH 03
 /FIELDS FLD_CH_20_1 20 CH 1
 /FIELDS FLD_PD_30_4 30 PD 4
 /FIELDS FLD_PD_34_5 34 PD 5
 /FIELDS FLD_PD_39_4 39 PD 4
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_PD_43_5 43 PD 5
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_17_3 ASCENDING,
   FLD_CH_20_1 ASCENDING,
   FLD_CH_21_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_30_4,
    TOTAL FLD_PD_34_5,
    TOTAL FLD_PD_39_4,
    TOTAL FLD_PD_43_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPCE
       ;;
(IF00FPCE)
       m_CondExec 00,EQ,IF00FPCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIF020 : CUMUL DES STATISTIQUES                                             
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCG PGM=BIF020     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCG
       ;;
(IF00FPCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A14} FIFSTATI ${DATA}/PTEM/IF00FPCD.BIF005BP
# *****   FICHIER ANCIEN CUMUL                                                 
       m_FileAssign -d SHR -g +0 FIFSTATC ${DATA}/PMACP/F07.BIF005CP.STAMICRO
# *****   FICHIER NOUVEAU CUMUL                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FIFSTATO ${DATA}/PMACP/F07.BIF005CP.STAMICRO
       m_ProgramExec BIF020 
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BIF006AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCJ
       ;;
(IF00FPCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/IF00FPBT.BIF006AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPCJ.BIF006BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_73 1 CH 73
 /KEYS
   FLD_CH_1_73 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPCK
       ;;
(IF00FPCK)
       m_CondExec 00,EQ,IF00FPCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIF006CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCM
       ;;
(IF00FPCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/IF00FPCJ.BIF006BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IF00FPCM.BIF006CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IF00FPCN
       ;;
(IF00FPCN)
       m_CondExec 04,GE,IF00FPCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCQ
       ;;
(IF00FPCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/IF00FPCM.BIF006CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPCQ.BIF006FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPCR
       ;;
(IF00FPCR)
       m_CondExec 00,EQ,IF00FPCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IFF006                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCT
       ;;
(IF00FPCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PTEM/IF00FPCJ.BIF006BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A19} FCUMULS ${DATA}/PTEM/IF00FPCQ.BIF006FP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIF000 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IF00FPCU
       ;;
(IF00FPCU)
       m_CondExec 04,GE,IF00FPCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIF007AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPCX
       ;;
(IF00FPCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/IF00FPBT.BIF007AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPCX.BIF007BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_73 1 CH 73
 /KEYS
   FLD_CH_1_73 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPCY
       ;;
(IF00FPCY)
       m_CondExec 00,EQ,IF00FPCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIF007CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDA
       ;;
(IF00FPDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/IF00FPCX.BIF007BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IF00FPDA.BIF007CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IF00FPDB
       ;;
(IF00FPDB)
       m_CondExec 04,GE,IF00FPDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDD
       ;;
(IF00FPDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/IF00FPDA.BIF007CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPDD.BIF007FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPDE
       ;;
(IF00FPDE)
       m_CondExec 00,EQ,IF00FPDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IFF007                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDG
       ;;
(IF00FPDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/IF00FPCX.BIF007BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A24} FCUMULS ${DATA}/PTEM/IF00FPDD.BIF007FP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
# * FEDITION REPORT SYSOUT=(9,IIF020),RECFM=VA                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ -g +1 FEDITION ${DATA}/PXX0/F07.IFF007AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IF00FPDH
       ;;
(IF00FPDH)
       m_CondExec 04,GE,IF00FPDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES ETAT DES DETAILS DE FINANCEMENT                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDJ
       ;;
(IF00FPDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION DES DETAILS DE FINANCEMENT                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.IFF007BP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PXX0/F07.IFF007AP
# ******  EDITION DESTINE AUX MICRO-FICHES SUR 1 MOIS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.IFF007BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPDK
       ;;
(IF00FPDK)
       m_CondExec 00,EQ,IF00FPDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIF013AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDM
       ;;
(IF00FPDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PTEM/IF00FPBT.BIF013AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPDM.BIF013BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_73 1 CH 73
 /KEYS
   FLD_CH_1_73 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPDN
       ;;
(IF00FPDN)
       m_CondExec 00,EQ,IF00FPDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIF013CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDQ
       ;;
(IF00FPDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A27} FEXTRAC ${DATA}/PTEM/IF00FPDM.BIF013BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IF00FPDQ.BIF013CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IF00FPDR
       ;;
(IF00FPDR)
       m_CondExec 04,GE,IF00FPDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDT
       ;;
(IF00FPDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/IF00FPDQ.BIF013CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPDT.BIF013FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPDU
       ;;
(IF00FPDU)
       m_CondExec 00,EQ,IF00FPDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IFF007                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPDX
       ;;
(IF00FPDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A29} FEXTRAC ${DATA}/PTEM/IF00FPDM.BIF013BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A30} FCUMULS ${DATA}/PTEM/IF00FPDT.BIF013FP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIF010 FEDITION
# *FEDITION FILE  NAME=IFF007AP,MODE=O                                         
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IF00FPDY
       ;;
(IF00FPDY)
       m_CondExec 04,GE,IF00FPDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIF014AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEA
       ;;
(IF00FPEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/IF00FPBT.BIF014AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPEA.BIF014BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_73 1 CH 73
 /KEYS
   FLD_CH_1_73 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPEB
       ;;
(IF00FPEB)
       m_CondExec 00,EQ,IF00FPEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIF014CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPED
       ;;
(IF00FPED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A32} FEXTRAC ${DATA}/PTEM/IF00FPEA.BIF014BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IF00FPED.BIF014CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IF00FPEE
       ;;
(IF00FPEE)
       m_CondExec 04,GE,IF00FPED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEG
       ;;
(IF00FPEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/IF00FPED.BIF014CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPEG.BIF014FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPEH
       ;;
(IF00FPEH)
       m_CondExec 00,EQ,IF00FPEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IFF007                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEJ
       ;;
(IF00FPEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A34} FEXTRAC ${DATA}/PTEM/IF00FPEA.BIF014BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A35} FCUMULS ${DATA}/PTEM/IF00FPEG.BIF014FP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIF020 FEDITION
# *FEDITION FILE  NAME=IFF014AP,MODE=O                                         
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IF00FPEK
       ;;
(IF00FPEK)
       m_CondExec 04,GE,IF00FPEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIF015AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEM
       ;;
(IF00FPEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/IF00FPBT.BIF015AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPEM.BIF015BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_24_2 24 CH 2
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_16_8 16 CH 8
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_24_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPEN
       ;;
(IF00FPEN)
       m_CondExec 00,EQ,IF00FPEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIF015CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEQ
       ;;
(IF00FPEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A37} FEXTRAC ${DATA}/PTEM/IF00FPEM.BIF015BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IF00FPEQ.BIF015CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IF00FPER
       ;;
(IF00FPER)
       m_CondExec 04,GE,IF00FPEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPET
       ;;
(IF00FPET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/IF00FPEQ.BIF015CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IF00FPET.BIF015FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IF00FPEU
       ;;
(IF00FPEU)
       m_CondExec 00,EQ,IF00FPET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIF040                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPEX
       ;;
(IF00FPEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71FP,MODE=(I,U) - DYNAM=YES                           
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30F,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A39} FEXTRAC ${DATA}/PTEM/IF00FPEM.BIF015BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A40} FCUMULS ${DATA}/PTEM/IF00FPET.BIF015FP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIF040 FEDITION
# *FEDITION FILE  NAME=IFF015AP,MODE=O                                         
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IF00FPEY
       ;;
(IF00FPEY)
       m_CondExec 04,GE,IF00FPEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO DU FICHIER AME907 EN FCAMEXAP                                         
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFA PGM=IDCAMS     ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFA
       ;;
(IF00FPFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ************ CARTES AMERICAN EXPRESS ******                                  
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/FTP.F07.AME907AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FCAMEXAP
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FCAMEXAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFA.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPFB
       ;;
(IF00FPFB)
       m_CondExec 16,NE,IF00FPFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#      RAZ DES FICHIERS ISSUS DE LA CHAINE IF00FP                              
# **** RAZ FICHIERS FINANCEMENTS MICRO ****                                    
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFD PGM=IDCAMS     ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFD
       ;;
(IF00FPFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ************ CARTES KYRIEL    *************                                  
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGP/F07.KYRIEL
# ************* CARTES TELETRAITEMENT *******                                  
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT2 ${DATA}/PNCGP/F07.TELTRAI
# *************   SOFINCO ********************                                 
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/FTP.F07.SOFINCO
# *************  CETELEM *******************                                   
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/FTP.F07.CETELEM
# ************ CARTES BANCAIRES *************                                  
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/FTP.F07.CAR907AP
# ************ CARTES AMERICAN EXPRESS ******                                  
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/FTP.F07.AME907AP
# ************ CARTES TELETRAITEMENT **FIC MICRO *****                         
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F07.TLV907AP
# ************ FICHIER SOFINCO (RECONDUIT FORMAT DARTY) ***                    
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A41} OUT8 ${DATA}/PNCGP/F07.SOFDAR
# ******* FIC PROMOSTIM                                                        
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ -g +1 OUT9 ${DATA}/PXX0/FTP.F07.PROMOSTP
# ******* FIC FCETREL                                                          
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/FTP.F07.CETREL
# ******* FIC IMAGES CHEQUES SAFIG                                             
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/F07.SAFIGBNP
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F07.SAFIGLCL
# ******* FIC IMAGES CHEQUES EXTELIA                                           
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/FTP.F07.EXTLIBNP
# ******* FIC PAYPAL                                                           
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/FTP.F07.PAYPAL
# ************ FICHIERS JRB ******                                             
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/FTP.F07.BIFJRBAP
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A42} OUT16 ${DATA}/PXX0/F07.BIFJRBBP
# ************ FICHIERS COFINOGA ***                                           
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g ${G_A43} OUT17 ${DATA}/PXX0/F07.BIFCOFAP
# ************ FICHIERS JMD ******                                             
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/FTP.F07.BIFJMDAP
# ************ FICHIERS JMD ******                                             
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A44} OUT19 ${DATA}/PXX0/F07.FLOADAR
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFD.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPFE
       ;;
(IF00FPFE)
       m_CondExec 16,NE,IF00FPFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **   RAZ DES FICHIER DE REPRISE                                              
# **   REPRISE : OUI                                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFG PGM=IDCAMS     ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFG
       ;;
(IF00FPFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ****    FICHIER DE REPRISE CETELEM EN CAS DE PLANTAGE                        
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.CETREPRI
# ****    FICHIER DE REPRISE CETELEM EN CAS DE PLANTAGE                        
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F07.SOFREPRI
# ****    FICHIER DE REPRISE CETELEM EN CAS DE PLANTAGE                        
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F07.CARREPRI
# ****    FICHIER DE REPRISE AMERICAN EXPRESS EN CAS DE PLANTAGE               
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F07.AMEREPRI
# ****    FICHIER DE REPRISE TELETRAITEMENT EN CAS DE PLANTAGE                 
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F07.TELREPRI
# ****    FICHIER DE REPRISE PROMOSTIM EN CAS DE PLANTAGE                      
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/F07.PROREPRI
# ****    FICHIER DE REPRISE FCETREL EN CAS DE PLANTAGE                        
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F07.CTRREPRI
# ****    FICHIER DE REPRISE SAFIG EN CAS DE PLANTAGE                          
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT8 ${DATA}/PXX0/F07.SFIBNPR
# ****    FICHIER DE REPRISE EXTELIA EN CAS DE PLANTAGE                        
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT9 ${DATA}/PXX0/F07.EXTBNPR
# ****                                                                         
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/F07.SFILCLR
# ****    FICHIER DE REPRISE PAYPAL                                            
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/F07.PAYREPRI
# ****    FICHIER DE REPRISE JRB                                               
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F07.JRBREPRI
# ****    FICHIER DE REPRISE JRB                                               
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/F07.FLOAREPR
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPFH
       ;;
(IF00FPFH)
       m_CondExec 16,NE,IF00FPFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IFF020  ETAT DES ETATS DES DETAILS DE FINANCEMENT                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFJ PGM=IEBGENER   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFJ
       ;;
(IF00FPFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A45} SYSUT1 ${DATA}/PXX0/F07.IFF007AP
       m_OutputAssign -c 9 -w IIF020 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IF00FPFK
       ;;
(IF00FPFK)
       m_CondExec 00,EQ,IF00FPFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTIF00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFM PGM=FTP        ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFM
       ;;
(IF00FPFM)
       m_CondExec ${EXAIM},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFM.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTIF00FP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFQ PGM=EZACFSM1   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFQ
       ;;
(IF00FPFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/IF00FPFQ.FTIF00FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIF00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IF00FPFT PGM=FTP        ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPFT
       ;;
(IF00FPFT)
       m_CondExec ${EXAIW},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPFT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.IF00FPFQ.FTIF00FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IF00FPZA
       ;;
(IF00FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IF00FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
