#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RA210Y.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYRA210 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.10.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RA210Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER PROVENANT DE L'ECLATEMENT                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RA210YA
       ;;
(RA210YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RA210YAA
       ;;
(RA210YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FRA210BY
#         FILE  NAME=FRA2REJY,MODE=I                                           
# ******* FIC ECLAT�  PAR FILIALE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.FRA210CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_CH_263_10 263 CH 10
 /FIELDS FLD_CH_252_7 252 CH 7
 /KEYS
   FLD_CH_11_6 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_252_7 ASCENDING,
   FLD_CH_263_10 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RA210YAB
       ;;
(RA210YAB)
       m_CondExec 00,EQ,RA210YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRA210 **  CREATION DES AVOIRS DE REMBOURSEMENT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RA210YAD
       ;;
(RA210YAD)
       m_CondExec ${EXAAF},NE,YES 
# ******* FIC DES AVOIRS + REJETS TRI�S                                        
       m_FileAssign -d SHR -g ${G_A1} FRA210 ${DATA}/PXX0/F45.FRA210CY
# ******* TABLE EN LECTURE                                                     
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGQ96   : NAME=RSGQ96,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ96 /dev/null
#    RTGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTRX00   : NAME=RSRX00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRX00 /dev/null
# ******* TABLE EN ECRITURE                                                    
#    RTRA00   : NAME=RSRA00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA00 /dev/null
#    RTRA01   : NAME=RSRA01Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTRA01 /dev/null
#        FDATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES DEMANDES DE REMBOURSEMENTS D'AVOIR _A REPRENDRE               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FREJETS ${DATA}/PXX0/F45.FRA2REJY
# ******* CET �TAT EST ENVOY� SOUS EOS DANS LA BOITE DFO                       
# IRA210   REPORT SYSOUT=(9,IRA210Y),RECFM=FBA,LRECL=133,BLKSIZE=0             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IRA210 ${DATA}/PXX0/F45.FRA210DY
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRA210 
       JUMP_LABEL=RA210YAE
       ;;
(RA210YAE)
       m_CondExec 04,GE,RA210YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          REMISE _A Z�RO DU FICHIER VENANT DE SIEBEL                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210YAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RA210YAG
       ;;
(RA210YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC ECLATE REMIS _A Z�RO                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F45.FRA210BY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210YAG.sysin
       m_UtilityExec
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=RA210XXX,                                                           
#      FNAME=":FRA210DY""(0),                                                  
#      NFNAME=RA210Y                                                           
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RA210YAH
       ;;
(RA210YAH)
       m_CondExec 16,NE,RA210YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DEPENDANCE POUR PLAN                                                       
#  CHANGEMENT PCL => RA210Y <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRA210Y                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RA210YAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RA210YAJ
       ;;
(RA210YAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210YAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RA210YAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RA210YAM
       ;;
(RA210YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RA210YAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
