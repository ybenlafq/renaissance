#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG25FP.ksh                       --- VERSION DU 09/10/2016 05:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG25F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 14.44.40 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG25FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BFG250 : CREATION DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIE         
#                    EMISES ET RECUES PAR SOCIETE (JFG012)                     
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG25FPA
       ;;
(FG25FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG25FPAA
       ;;
(FG25FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES LIEUX GROUPE                                               
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ECRITURES COMPTABLES                                       
#    RSFG02   : NAME=RSFG02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG02 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER RECAPITULATIF DES FACTURES  A EDITER                         
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG25FPAA.BFG250AP
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG250 
       JUMP_LABEL=FG25FPAB
       ;;
(FG25FPAB)
       m_CondExec 04,GE,FG25FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIETE              
#                       DES FILIALES                                           
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG25FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FG25FPAD
       ;;
(FG25FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FG25FPAA.BFG250AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG25FPAD.BFG250BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG25FPAE
       ;;
(FG25FPAE)
       m_CondExec 00,EQ,FG25FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG25FPAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG25FPAG
       ;;
(FG25FPAG)
       m_CondExec ${EXAAK},NE,YES 
# AAK     IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG250RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG250RC1
# ******  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A2} FFGEDG ${DATA}/PTEM/FG25FPAD.BFG250BP
# ******  BASE IMPRESSION GENERALISEE                                          
# DIGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG25FPR1)                      
# DIGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#         TRI DU FICHIER RECAPITULATIF DES FACTURES INTER-SOCIETE              
#                        POUR PARIS                                            
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG25FPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG25FPAJ
       ;;
(FG25FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/FG25FPAA.BFG250AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG012 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG25FPAK
       ;;
(FG25FPAK)
       m_CondExec 00,EQ,FG25FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG25FPZA
       ;;
(FG25FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG25FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
