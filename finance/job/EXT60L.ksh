#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT60L.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLEXT60 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/10/02 AT 14.39.59 BY BURTEC5                      
#    STANDARDS: P  JOBSET: EXT60L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
#  BEX001 : CREE FIC DES ARTICLES DEMANDES DS TABLE GENERALISEE EXTR2          
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EXT60LA
       ;;
(EXT60LA)
#
#EXT60LBD
#EXT60LBD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT60LBD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EXT60LAA
       ;;
(EXT60LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  HISTO DE VENTES                                                      
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PXX0/F61.HV01AL
#                                                                              
# ******  ARTICLES DEMANDES                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PXX0/EXT60LAA.BEX620AL
# ******  CODE ETAT 007 POUR CREER LA TABLE RTEX60                             
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/EXT60LAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=EXT60LAB
       ;;
(EXT60LAB)
       m_CondExec 04,GE,EXT60LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DES ARTICLES DEMANDES PAR ARTICLE : 559,7,A                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAD
       ;;
(EXT60LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/EXT60LAA.BEX620AL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/EXT60LAD.BEX620BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT60LAE
       ;;
(EXT60LAE)
       m_CondExec 00,EQ,EXT60LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX002 : ENRICHIE LES DONNEES EXTRAITES                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAG
       ;;
(EXT60LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  RTHV01 EN SEQUENTIEL TRIE PAR CODIC ET SOCIETE (GV000L)              
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PXX0/F61.HV01AL
# ******  ARTICLES DEMANDES TRI�S                                              
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PXX0/EXT60LAD.BEX620BL
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  CODE SOCIETE (961)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  CODE ETAT 007 POUR CREER LA TABLE RTEX60                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT60LAG
#                                                                              
# ******  FIC ARTICLES DEMANDES ENRICHIS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX002 ${DATA}/PXX0/EXT60LAG.BEX620CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX002 
       JUMP_LABEL=EXT60LAH
       ;;
(EXT60LAH)
       m_CondExec 04,GE,EXT60LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI PAR CODES RUPTURES + SOMME DES QUANTITES NUMERIQUES                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAJ
       ;;
(EXT60LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/EXT60LAG.BEX620CL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/F61.BEX620DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_CH_7_130 7 CH 130
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_PD_415_8 415 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /KEYS
   FLD_CH_7_130 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT60LAK
       ;;
(EXT60LAK)
       m_CondExec 00,EQ,EXT60LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX010 : CORRECTION DU NUMERO DE SEQUENCE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAM
       ;;
(EXT60LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  CODE ETAT 007 POUR CREER LA TABLE RTEX60                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT60LAM
# ******  FICHIER PARAMETRAGE DO                                               
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A4} FEX009 ${DATA}/PXX0/F61.BEX620DL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX010 ${DATA}/PXX0/EXT60LAM.BEX710AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX010 
       JUMP_LABEL=EXT60LAN
       ;;
(EXT60LAN)
       m_CondExec 04,GE,EXT60LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES WSEQPRO FLAG                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAQ
       ;;
(EXT60LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/EXT60LAM.BEX710AL
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 SORTOUT ${DATA}/PXX0/EXT60LAQ.BEX710BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_135 1 CH 135
 /KEYS
   FLD_CH_1_135 ASCENDING
 /* Record Type = F  Record Length = 735 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT60LAR
       ;;
(EXT60LAR)
       m_CondExec 00,EQ,EXT60LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX011 : CUMUL PAR WSEQPRO                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAT
       ;;
(EXT60LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FEX010 ${DATA}/PXX0/EXT60LAQ.BEX710BL
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX011 ${DATA}/PXX0/EXT60LAT.BEX711AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX011 
       JUMP_LABEL=EXT60LAU
       ;;
(EXT60LAU)
       m_CondExec 04,GE,EXT60LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX012 : AJOUT DES TOTALISATIONS                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LAX
       ;;
(EXT60LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  CODE ETAT 007 POUR CREER LA TABLE RTEX60                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT60LAX
# ******  FICHIER PARAMETRAGE DO                                               
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A7} FEX011 ${DATA}/PXX0/EXT60LAT.BEX711AL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX012 ${DATA}/PEX0/F61.RELOAD.EX60RL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX012 
       JUMP_LABEL=EXT60LAY
       ;;
(EXT60LAY)
       m_CondExec 04,GE,EXT60LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTEX60                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LBA
       ;;
(EXT60LBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FIC SERVANT AU LOAD                                                  
       m_FileAssign -d SHR -g ${G_A8} SYSREC ${DATA}/PEX0/F61.RELOAD.EX60RL
# ******  TABLE EN MAJ                                                         
#    RSEX60L  : NAME=RSEX60L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSEX60L /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT60LBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EXT60L_EXT60LBA_RTEX60.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EXT60LBB
       ;;
(EXT60LBB)
       m_CondExec 04,GE,EXT60LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY DU TABLESPACE RSEX60D DE LA BASE PLDEX00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT60LBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=EXT60LZA
       ;;
(EXT60LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT60LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
