#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT20Y.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYEXT20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/09/30 AT 18.14.49 BY BURTECO                      
#    STANDARDS: P  JOBSET: EXT20Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BEX001 : CREE FIC DES ARTICLES DEMANDES DS TABLE GENERALISEE  EXTR2         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EXT20YA
       ;;
(EXT20YA)
#
#EXT20YBD
#EXT20YBD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT20YBD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EXT20YAA
       ;;
(EXT20YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  HISTO DE VENTES PAR SOC,ARTICLES,MAGASINS,MOIS                       
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PGV0/F45.HV01AY
#                                                                              
# ******  FIC DES ARTICLES EXTRAITS                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PXX0/EXT20YAA.BEX020AY
#                                                                              
# ******  CODE ETAT 003 POUR CREER LA TABLE RTEX20                             
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/EXT20YAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=EXT20YAB
       ;;
(EXT20YAB)
       m_CondExec 04,GE,EXT20YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION SUR CODIC : 559,7,A                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAD
       ;;
(EXT20YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/EXT20YAA.BEX020AY
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/EXT20YAD.BEX020BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT20YAE
       ;;
(EXT20YAE)
       m_CondExec 00,EQ,EXT20YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX002 : ENRICHISSEMENT DES DONNEES EXTRAITES                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAG
       ;;
(EXT20YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  RTHV01 EN SEQUENTIEL TRIE (VENANT DE GV000R)                         
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PGV0/F45.HV01AY
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  CODE ETAT 003 POUR CREER LA TABLE RTEX20                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT20YAG
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PXX0/EXT20YAD.BEX020BY
#                                                                              
# ******  EXTRACTION ENRICHIES                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX002 ${DATA}/PXX0/EXT20YAG.BEX020CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX002 
       JUMP_LABEL=EXT20YAH
       ;;
(EXT20YAH)
       m_CondExec 04,GE,EXT20YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION ARTICLES                                       
#            CODES RUPTURES + SOMME DES QUANTITES NUMERIQUES                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAJ
       ;;
(EXT20YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/EXT20YAG.BEX020CY
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/EXT20YAJ.BEX020DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_415_8 415 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_CH_7_130 7 CH 130
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /KEYS
   FLD_CH_7_130 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT20YAK
       ;;
(EXT20YAK)
       m_CondExec 00,EQ,EXT20YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX010 : CORRECTION DU NUMERO DE SEQUENCE                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAM
       ;;
(EXT20YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  CODE ETAT 003 POUR CREER LA TABLE RTEX20                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT20YAM
# ******  FICHIER PARAMETRAGE DO                                               
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A4} FEX009 ${DATA}/PXX0/EXT20YAJ.BEX020DY
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX010 ${DATA}/PXX0/EXT20YAM.BEX310AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX010 
       JUMP_LABEL=EXT20YAN
       ;;
(EXT20YAN)
       m_CondExec 04,GE,EXT20YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER ARTICLE :CODE RUPTURE,NSEQPRO,FLAG                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAQ
       ;;
(EXT20YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/EXT20YAM.BEX310AY
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 SORTOUT ${DATA}/PXX0/EXT20YAQ.BEX310BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_135 1 CH 135
 /KEYS
   FLD_CH_1_135 ASCENDING
 /* Record Type = F  Record Length = 735 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT20YAR
       ;;
(EXT20YAR)
       m_CondExec 00,EQ,EXT20YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX011 : CUMUL PAR WSEQPRO                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAT
       ;;
(EXT20YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FEX010 ${DATA}/PXX0/EXT20YAQ.BEX310BY
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX011 ${DATA}/PXX0/EXT20YAT.BEX311AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX011 
       JUMP_LABEL=EXT20YAU
       ;;
(EXT20YAU)
       m_CondExec 04,GE,EXT20YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX012 : AJOUT DES TOTALISATIONS                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YAX
       ;;
(EXT20YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE DE FIN DE MOIS                                          
       m_FileAssign -i FDATE
$FMOISJ
_end
# ******  FICHIER PARAMETRAGE DO                                               
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
# ******  CODE ETAT 003 POUR CREER LA TABLE RTEX20                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT20YAX
       m_FileAssign -d SHR -g ${G_A7} FEX011 ${DATA}/PXX0/EXT20YAT.BEX311AY
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX012 ${DATA}/PXX0/EXT20YAX.BEX312AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX012 
       JUMP_LABEL=EXT20YAY
       ;;
(EXT20YAY)
       m_CondExec 04,GE,EXT20YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTEX20                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YBA
       ;;
(EXT20YBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD DE P945.RTEX20                                       
       m_FileAssign -d SHR -g ${G_A8} SYSREC ${DATA}/PXX0/EXT20YAX.BEX312AY
# ******  SUIVI LIVR� PAR MARQUE                                               
#    RSEX20Y  : NAME=RSEX20Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSEX20Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSEX20Y /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT20YBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EXT20Y_EXT20YBA_RTEX20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EXT20YBB
       ;;
(EXT20YBB)
       m_CondExec 04,GE,EXT20YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY DU TABLESPACE RSEX20R DE LA BASE PYDEX00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT20YBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=EXT20YZA
       ;;
(EXT20YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT20YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
