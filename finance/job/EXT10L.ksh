#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT10L.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLEXT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/10/14 AT 16.01.01 BY BURTECN                      
#    STANDARDS: P  JOBSET: EXT10L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#   CREE FIC DES ARTICLES DEMANDES DS TABLE GENERALISEES  EXTR1                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EXT10LA
       ;;
(EXT10LA)
#
#EXT10LBG
#EXT10LBG Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT10LBG
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EXT10LAA
       ;;
(EXT10LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVE MENSUELLE             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **  FICHIER DATE  DE FIN DE MOIS                                             
       m_FileAssign -i FDATE
$FMOISJ
_end
#      TABLE HISTO DE VENTES PAR SOC,ARTICLES,MAGASINS,MOIS                    
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/EXT10LAA.BEX010AL
#     CODE ETAT 002 POUR CREER LA TABLE RTEX10                                 
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/EXT10LAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=EXT10LAB
       ;;
(EXT10LAB)
       m_CondExec 04,GE,EXT10LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAD
       ;;
(EXT10LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EXT10LAA.BEX010AL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT10LAD.BEX010BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT10LAE
       ;;
(EXT10LAE)
       m_CondExec 00,EQ,EXT10LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX002                          *******         
#   ENRICHIE LES DONNEES EXTRAITES                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAG
       ;;
(EXT10LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  CODE SOCIETE (961)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FDATE
$FDATE
_end
#     CODE ETAT 002 POUR CREER LA TABLE RTEX10                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT10LAG
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PTEM/EXT10LAD.BEX010BL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX002 ${DATA}/PTEM/EXT10LAG.BEX010CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX002 
       JUMP_LABEL=EXT10LAH
       ;;
(EXT10LAH)
       m_CondExec 04,GE,EXT10LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES + SOMME DES QUANTITES NUMERIQUES                  
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAJ
       ;;
(EXT10LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/EXT10LAG.BEX010CL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT10LAJ.BEX010DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_CH_7_130 7 CH 130
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /FIELDS FLD_PD_415_8 415 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /KEYS
   FLD_CH_7_130 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT10LAK
       ;;
(EXT10LAK)
       m_CondExec 00,EQ,EXT10LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX010                          *******         
#   CORRECTION DU NUMERO DE SEQUENCE                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAM
       ;;
(EXT10LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#     CODE ETAT 002 POUR CREER LA TABLE RTEX10                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT10LAM
#  FICHIER PARAMETRE DO                                                        
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A4} FEX009 ${DATA}/PTEM/EXT10LAJ.BEX010DL
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX010 ${DATA}/PTEM/EXT10LAM.BEX210AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX010 
       JUMP_LABEL=EXT10LAN
       ;;
(EXT10LAN)
       m_CondExec 04,GE,EXT10LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES + WSEQPRO FLAG                                    
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAQ
       ;;
(EXT10LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/EXT10LAM.BEX210AL
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 SORTOUT ${DATA}/PTEM/EXT10LAQ.BEX210BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_135 1 CH 135
 /KEYS
   FLD_CH_1_135 ASCENDING
 /* Record Type = F  Record Length = 735 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT10LAR
       ;;
(EXT10LAR)
       m_CondExec 00,EQ,EXT10LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX011                          *******         
#   CUMUL PAR WSEQPRO                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAT
       ;;
(EXT10LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} FEX010 ${DATA}/PTEM/EXT10LAQ.BEX210BL
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX011 ${DATA}/PTEM/EXT10LAT.BEX211AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX011 
       JUMP_LABEL=EXT10LAU
       ;;
(EXT10LAU)
       m_CondExec 04,GE,EXT10LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX012                          *******         
#   AJOUT DES TOTALISATIONS                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LAX
       ;;
(EXT10LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#     CODE ETAT 002 POUR CREER LA TABLE RTEX10                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT10LAX
#  FICHIER PARAMETRE DO                                                        
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A7} FEX011 ${DATA}/PTEM/EXT10LAT.BEX211AL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX012 ${DATA}/PTEM/EXT10LAX.BEX212AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX012 
       JUMP_LABEL=EXT10LAY
       ;;
(EXT10LAY)
       m_CondExec 04,GE,EXT10LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FILE ISSSU DU PROGRAM(BEX012) AVANT LOAD POUR CAUSE DE PERF          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LBA
       ;;
(EXT10LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/EXT10LAX.BEX212AL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT10LBA.BEX212BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_26 7 CH 26
 /FIELDS FLD_CH_33_26 33 CH 26
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_26 ASCENDING,
   FLD_CH_33_26 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT10LBB
       ;;
(EXT10LBB)
       m_CondExec 00,EQ,EXT10LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTEX10                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LBD
       ;;
(EXT10LBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSEX10   : NAME=RSEX10L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSEX10 /dev/null
       m_FileAssign -d SHR -g ${G_A9} SYSREC ${DATA}/PTEM/EXT10LBA.BEX212BL
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT10LBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EXT10L_EXT10LBD_RTEX10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EXT10LBE
       ;;
(EXT10LBE)
       m_CondExec 04,GE,EXT10LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLE SPACE RSEX10L DE LA D BASE PLDEX00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT10LBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=EXT10LZA
       ;;
(EXT10LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT10LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
