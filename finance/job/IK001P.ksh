#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IK001P.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIK001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 22.07.30 BY BURTEC6                      
#    STANDARDS: P  JOBSET: IK001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD NCODIC DE RTGA00                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IK001PA
       ;;
(IK001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=IK001PAA
       ;;
(IK001PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 7 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/IK001PAA.BIK001AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR NCODIC                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAD
       ;;
(IK001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IK001PAA.BIK001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IK001PAD.BIK001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 1 CH 7
 /KEYS
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IK001PAE
       ;;
(IK001PAE)
       m_CondExec 00,EQ,IK001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BIK001 :                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAG
       ;;
(IK001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA09 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA20 /dev/null
#    RTGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA21 /dev/null
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA29 /dev/null
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
#    RTGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA92   : NAME=RSGA92,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA92 /dev/null
#    RTGA93   : NAME=RSGA93,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA93 /dev/null
#                                                                              
# *****   FICHIER PARAM�TRE                                                    
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/IK001PAG
# *****   FICHIER NCODIC                                                       
       m_FileAssign -d SHR -g ${G_A2} FCODIC ${DATA}/PTEM/IK001PAD.BIK001BP
#                                                                              
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK001 ${DATA}/PXX0/F07.FIK001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK002 ${DATA}/PXX0/F07.FIK002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK003 ${DATA}/PTEM/IK001PAG.FIK003AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK004 ${DATA}/PXX0/F07.FIK004AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK005 ${DATA}/PXX0/F07.FIK005AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FIK006 ${DATA}/PXX0/F07.FIK006AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIK001 
       JUMP_LABEL=IK001PAH
       ;;
(IK001PAH)
       m_CondExec 04,GE,IK001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUM : ELIMINATION DES DOUBLONS DU FICHIER FIK003                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAJ
       ;;
(IK001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER EN ENTR�E                                                    
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/IK001PAG.FIK003AP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FIK003BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_150 1 CH 150
 /KEYS
   FLD_BI_1_150 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IK001PAK
       ;;
(IK001PAK)
       m_CondExec 00,EQ,IK001PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FIK001 (TRI PAR CODIC)                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAM
       ;;
(IK001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER EN ENTR�E                                                    
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.FIK001AP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IK001PAM.FIK001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IK001PAN
       ;;
(IK001PAN)
       m_CondExec 00,EQ,IK001PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BIK002 : ENRICHISSEMENT DU FICHIER FIK001                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAQ
       ;;
(IK001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RTGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN59 /dev/null
#                                                                              
# *****   FICHIER PARAM�TRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EN ENTR�E                                                    
       m_FileAssign -d SHR -g ${G_A5} FIK001I ${DATA}/PTEM/IK001PAM.FIK001BP
#                                                                              
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g ${G_A6} FIK001O ${DATA}/PXX0/F07.FIK001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIK002 
       JUMP_LABEL=IK001PAR
       ;;
(IK001PAR)
       m_CondExec 04,GE,IK001PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK001AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK001.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK001AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK001AP,                                                           
#      FNAME=":FIK001AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK002AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK002.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK002AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK002AP,                                                           
#      FNAME=":FIK002AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK003AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK003.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK003AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK003AP,                                                           
#      FNAME=":FIK003BP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK004AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK004.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK004AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABT      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK004AP,                                                           
#      FNAME=":FIK004AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK005AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK005.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK005AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABY      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK005AP,                                                           
#      FNAME=":FIK005AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER PAR CFT DU FICHIER FIK006AP SUR VNI351. *                  
# ********************************************************************         
#  ENVOI EXTRAC DE DONNEES POUR DES FICHIERS D'INTERFACE KLEE                  
#  NOM   VNI351                                                                
#  IP  10.135.1.75                                                             
#  R�PERTOIRE D:\KLEE\PROD\INTERFACES\SFXIMPORT                                
#  NON FICHIER MICRO : FIK006.TXT                                              
#  NON FICHIER HOST  : PXX0.F07.FIK006AP                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ACD      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# ***************************************                                      
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FIK006AP,                                                           
#      FNAME=":FIK006AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAT
       ;;
(IK001PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PAT.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IK001PAX
       ;;
(IK001PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PAX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBA
       ;;
(IK001PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBD
       ;;
(IK001PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBG
       ;;
(IK001PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBJ
       ;;
(IK001PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBM
       ;;
(IK001PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PBQ PGM=EZACFSM1   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBQ
       ;;
(IK001PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PBT PGM=FTP        ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBT
       ;;
(IK001PBT)
       m_CondExec ${EXACS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBT.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PBX PGM=EZACFSM1   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IK001PBX
       ;;
(IK001PBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PBX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTIK001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IK001PCA PGM=FTP        ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IK001PCA
       ;;
(IK001PCA)
       m_CondExec ${EXADC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PCA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP IK001PCD PGM=EZACFSM1   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IK001PCD
       ;;
(IK001PCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PCD.sysin
       m_ProgramExec EZACFSM1
# *******************************************************************          
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IK001PZA
       ;;
(IK001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IK001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
