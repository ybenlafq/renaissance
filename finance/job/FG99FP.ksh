#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG99FP.ksh                       --- VERSION DU 09/10/2016 05:42
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG99F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/06/19 AT 11.48.20 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG99FP                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  UNLOAD DE LA TABLE RTFG01                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FG99FPA
       ;;
(FG99FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2000/06/19 AT 11.48.20 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: FG99FP                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'REORG FGXX'                            
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FG99FPAA
       ;;
(FG99FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 212 -g +1 SYSREC01 ${DATA}/PXX0/F07.FG01UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG99FPAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSFG01                                        
# ********************************************************************         
#   REPRISE : NON   VERIFIER LE BACKOUT CORTEX                                 
#                  REPRISE FORCEE AU STEP PRECEDANT                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG99FPAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FG99FPAG
       ;;
(FG99FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 33 -g +1 SYSREC01 ${DATA}/PXX0/F07.FG05UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG99FPAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSFG05                                        
# ********************************************************************         
#   REPRISE : NON   VERIFIER LE BACKOUT CORTEX                                 
#                  REPRISE FORCEE AU STEP PRECEDANT                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG99FPAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG99FPAM
       ;;
(FG99FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 118 -g +1 SYSREC01 ${DATA}/PXX0/F07.FG06UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG99FPAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSFG06                                        
# ********************************************************************         
#   REPRISE : NON   VERIFIER LE BACKOUT CORTEX                                 
#                  REPRISE FORCEE AU STEP PRECEDANT                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG99FPAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG99FPAT
       ;;
(FG99FPAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 81 -g +1 SYSREC01 ${DATA}/PXX0/F07.FG07UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG99FPAT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSFG07                                        
# ********************************************************************         
#   REPRISE : NON   VERIFIER LE BACKOUT CORTEX                                 
#                  REPRISE FORCEE AU STEP PRECEDANT                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG99FPAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG99FPZA
       ;;
(FG99FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG99FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
