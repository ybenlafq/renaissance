#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FT2REP.ksh                       --- VERSION DU 08/10/2016 12:42
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFT2RE -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/02 AT 15.22.11 BY BURTECC                      
#    STANDARDS: P  JOBSET: FT2REP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#              CUMUL EN CAS DE PLANTAGE DE FTCREG                              
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE SAFIG                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FT2REPA
       ;;
(FT2REPA)
#
#FT2REPAJ
#FT2REPAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#FT2REPAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2013/07/02 AT 15.22.11 BY BURTECC                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FT2REP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FTCREG'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FT2REPAA
       ;;
(FT2REPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER SAFIGKDO                                                         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.SAFIGKDO
# *** FICHIER REPRISE J-1                                                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.SAFIREPR
# *** FICHIER DE REPRISE POUR (J+1)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.SAFIREPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_15 01 CH 15
 /KEYS
   FLD_BI_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FT2REPAB
       ;;
(FT2REPAB)
       m_CondExec 00,EQ,FT2REPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#              CUMUL EN CAS DE PLANTAGE DE FTCREG                              
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE EXTELIA                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FT2REPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FT2REPAD
       ;;
(FT2REPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER EXTLIKDO                                                         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.EXTLIKDO
# *** FICHIER REPRISE J-1                                                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.EXTLREPR
# *** FICHIER DE REPRISE POUR (J+1)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.EXTLREPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_15 01 CH 15
 /KEYS
   FLD_BI_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FT2REPAE
       ;;
(FT2REPAE)
       m_CondExec 00,EQ,FT2REPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#              CUMUL EN CAS DE PLANTAGE DE FTCREG                              
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE (NOTE DE FRAIS - NOTILUS)           
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      SORT                                                                
# *** FICHIER NOTE DE FRAIS (NDF - PRV - AVA - DUE)                            
# SORTIN   FILE  NAME=NDF000AG,MODE=I                                          
#          FILE  NAME=PRV000AG,MODE=I                                          
#          FILE  NAME=AVA000AG,MODE=I                                          
#          FILE  NAME=DUE000AG,MODE=I                                          
# *** FICHIER REPRISE J-1                                                      
#          FILE  NAME=NOTEREPR,MODE=I                                          
# *** FICHIER DE REPRISE POUR (J+1)                                            
# SORTOUT  FILE  NAME=NOTEREPR,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT  FIELDS=(01,15,BI,A)                                                   
#          DATAEND                                                             
# ********************************************************************         
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FT2REPAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
