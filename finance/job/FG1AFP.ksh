#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG1AFP.ksh                       --- VERSION DU 08/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG1AF -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/02/10 AT 10.46.10 BY PREPA2                       
#    STANDARDS: P  JOBSET: FG1AFP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION POUR PARIS ET TOUTES FILIALES            
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FG1AFPA
       ;;
(FG1AFPA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2009/02/10 AT 10.46.10 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: FG1AFP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FG14FP'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FG1AFPAA
       ;;
(FG1AFPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
# ******** RECREATION DES FICHIERS POUR LIBERATION DES DEPENDANCES             
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT1 ${DATA}/PXX0/F96.FG14FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT2 ${DATA}/PXX0/F91.FG14FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT3 ${DATA}/PXX0/F44.FG14FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT4 ${DATA}/PXX0/F61.FG14FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT5 ${DATA}/PXX0/F89.FG14FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT6 ${DATA}/PXX0/F16.FG14FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT7 ${DATA}/PXX0/F07.FG14FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT8 ${DATA}/PXX0/F45.FG14FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT9 ${DATA}/PXX0/F08.FG14FPX
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT10 ${DATA}/PXX0/F07.FG14FPA
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT11 ${DATA}/PXX0/F07.FG14FPC
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG1AFPAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FG1AFPAB
       ;;
(FG1AFPAB)
       m_CondExec 16,NE,FG1AFPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
