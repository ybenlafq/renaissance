#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT61P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEXT61 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/12/01 AT 13.20.10 BY BURTECN                      
#    STANDARDS: P  JOBSET: EXT61P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#     TRI PAR CODES RUPTURES + SOMME DES QUANTITES NUMERIQUES                  
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EXT61PA
       ;;
(EXT61PA)
#
#EXT61PAT
#EXT61PAT Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT61PAT
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EXT61PAA
       ;;
(EXT61PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# PRED     LINK  NAME=$EXT60B,MODE=I                                           
#                                                                              
# SORTIN   FILE  NAME=BEX620DB,MODE=I                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BEX620DD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BEX620DL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BEX620DM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BEX620DO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BEX620DP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BEX620DY
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/EXT61PAA.BEX061AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_2  "999"
 /FIELDS FLD_PD_303_8 303 PD 8
 /FIELDS FLD_PD_407_8 407 PD 8
 /FIELDS FLD_PD_479_8 479 PD 8
 /FIELDS FLD_PD_519_8 519 PD 8
 /FIELDS FLD_PD_263_8 263 PD 8
 /FIELDS FLD_CH_7_130 7 CH 130
 /FIELDS FLD_PD_463_8 463 PD 8
 /FIELDS FLD_PD_231_8 231 PD 8
 /FIELDS FLD_PD_439_8 439 PD 8
 /FIELDS FLD_PD_527_8 527 PD 8
 /FIELDS FLD_PD_399_8 399 PD 8
 /FIELDS FLD_PD_447_8 447 PD 8
 /FIELDS FLD_PD_279_8 279 PD 8
 /FIELDS FLD_PD_351_8 351 PD 8
 /FIELDS FLD_PD_551_8 551 PD 8
 /FIELDS FLD_PD_391_8 391 PD 8
 /FIELDS FLD_PD_503_8 503 PD 8
 /FIELDS FLD_PD_455_8 455 PD 8
 /FIELDS FLD_PD_327_8 327 PD 8
 /FIELDS FLD_PD_247_8 247 PD 8
 /FIELDS FLD_PD_511_8 511 PD 8
 /FIELDS FLD_PD_239_8 239 PD 8
 /FIELDS FLD_PD_495_8 495 PD 8
 /FIELDS FLD_PD_319_8 319 PD 8
 /FIELDS FLD_PD_295_8 295 PD 8
 /FIELDS FLD_PD_487_8 487 PD 8
 /FIELDS FLD_PD_375_8 375 PD 8
 /FIELDS FLD_PD_311_8 311 PD 8
 /FIELDS FLD_PD_543_8 543 PD 8
 /FIELDS FLD_PD_271_8 271 PD 8
 /FIELDS FLD_PD_431_8 431 PD 8
 /FIELDS FLD_PD_287_8 287 PD 8
 /FIELDS FLD_PD_223_8 223 PD 8
 /FIELDS FLD_PD_359_8 359 PD 8
 /FIELDS FLD_PD_471_8 471 PD 8
 /FIELDS FLD_PD_255_8 255 PD 8
 /FIELDS FLD_PD_383_8 383 PD 8
 /FIELDS FLD_PD_423_8 423 PD 8
 /FIELDS FLD_PD_535_8 535 PD 8
 /FIELDS FLD_PD_367_8 367 PD 8
 /FIELDS FLD_PD_343_8 343 PD 8
 /FIELDS FLD_PD_335_8 335 PD 8
 /FIELDS FLD_CH_4_597 4 CH 597
 /FIELDS FLD_PD_415_8 415 PD 8
 /KEYS
   FLD_CH_7_130 ASCENDING
 /MT_INFILE_SORT SORTIN
 /REFORMAT CST_0_2,FLD_CH_4_597
 /SUMMARIZE 
    TOTAL FLD_PD_223_8,
    TOTAL FLD_PD_231_8,
    TOTAL FLD_PD_239_8,
    TOTAL FLD_PD_247_8,
    TOTAL FLD_PD_255_8,
    TOTAL FLD_PD_263_8,
    TOTAL FLD_PD_271_8,
    TOTAL FLD_PD_279_8,
    TOTAL FLD_PD_287_8,
    TOTAL FLD_PD_295_8,
    TOTAL FLD_PD_303_8,
    TOTAL FLD_PD_311_8,
    TOTAL FLD_PD_319_8,
    TOTAL FLD_PD_327_8,
    TOTAL FLD_PD_335_8,
    TOTAL FLD_PD_343_8,
    TOTAL FLD_PD_351_8,
    TOTAL FLD_PD_359_8,
    TOTAL FLD_PD_367_8,
    TOTAL FLD_PD_375_8,
    TOTAL FLD_PD_383_8,
    TOTAL FLD_PD_391_8,
    TOTAL FLD_PD_399_8,
    TOTAL FLD_PD_407_8,
    TOTAL FLD_PD_415_8,
    TOTAL FLD_PD_423_8,
    TOTAL FLD_PD_431_8,
    TOTAL FLD_PD_439_8,
    TOTAL FLD_PD_447_8,
    TOTAL FLD_PD_455_8,
    TOTAL FLD_PD_463_8,
    TOTAL FLD_PD_471_8,
    TOTAL FLD_PD_479_8,
    TOTAL FLD_PD_487_8,
    TOTAL FLD_PD_495_8,
    TOTAL FLD_PD_503_8,
    TOTAL FLD_PD_511_8,
    TOTAL FLD_PD_519_8,
    TOTAL FLD_PD_527_8,
    TOTAL FLD_PD_535_8,
    TOTAL FLD_PD_543_8,
    TOTAL FLD_PD_551_8
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=EXT61PAB
       ;;
(EXT61PAB)
       m_CondExec 00,EQ,EXT61PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX010                          *******         
#   CORRECTION DU NUMERO DE SEQUENCE                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAD
       ;;
(EXT61PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   DATE  DE FIN DE MOIS                                                 
       m_FileAssign -i FDATE
$FMOISJ
_end
# *****   CODE ETAT 007 POUR CREER LA TABLE RTEX60G                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT61PAD
#  FICHIER PARAMETRAGE DO                                                      
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A1} FEX009 ${DATA}/PTEM/EXT61PAA.BEX061AP
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX010 ${DATA}/PTEM/EXT61PAD.BEX061BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX010 
       JUMP_LABEL=EXT61PAE
       ;;
(EXT61PAE)
       m_CondExec 04,GE,EXT61PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODES RUPTURES WSEQPRO FLAG                                      
#   ***********************************************************                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAG
       ;;
(EXT61PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/EXT61PAD.BEX061BP
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 SORTOUT ${DATA}/PTEM/EXT61PAG.BEX061CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_135 1 CH 135
 /KEYS
   FLD_CH_1_135 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EXT61PAH
       ;;
(EXT61PAH)
       m_CondExec 00,EQ,EXT61PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX011                          *******         
#   CUMUL PAR WSEQPRO                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAJ
       ;;
(EXT61PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} FEX010 ${DATA}/PTEM/EXT61PAG.BEX061CP
       m_FileAssign -d NEW,CATLG,DELETE -r 735 -g +1 FEX011 ${DATA}/PTEM/EXT61PAJ.BEX061DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX011 
       JUMP_LABEL=EXT61PAK
       ;;
(EXT61PAK)
       m_CondExec 04,GE,EXT61PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ******             EXECUTION BEX012                          *******         
#   AJOUT DES TOTALISATIONS                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAM
       ;;
(EXT61PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   DATE  DE FIN DE MOIS                                                 
       m_FileAssign -i FDATE
$FMOISJ
_end
# *****   CODE ETAT 007 POUR CREER LA TABLE RTEX60G                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EXT61PAM
#  FICHIER PARAMETRAGE DO                                                      
       m_FileAssign -d SHR FEXTSO ${DATA}/SYS3.BURTEC2.CARD/EXT
       m_FileAssign -d SHR -g ${G_A4} FEX011 ${DATA}/PTEM/EXT61PAJ.BEX061DP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX012 ${DATA}/PTEM/EXT61PAM.BEX061EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX012 
       JUMP_LABEL=EXT61PAN
       ;;
(EXT61PAN)
       m_CondExec 04,GE,EXT61PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTEX60G                                                   
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAQ
       ;;
(EXT61PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
#    RSEX60G  : NAME=RSEX60G,MODE=(U,N) - DYNAM=YES                            
# -X-RSEX60G  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSEX60G /dev/null
       m_FileAssign -d SHR -g ${G_A5} SYSREC ${DATA}/PTEM/EXT61PAM.BEX061EP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT61PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EXT61P_EXT61PAQ_RTEX60G.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EXT61PAR
       ;;
(EXT61PAR)
       m_CondExec 04,GE,EXT61PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLE SPACE RSEX60G         BASE PPDEX00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PAX
       ;;
(EXT61PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF120AG DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q120AG(&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F120AG
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p EXT61P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=EXT61PAY
       ;;
(EXT61PAY)
       m_CondExec 04,GE,EXT61PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS LIVREE DETAIL  QMF120DG                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EXT61PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PBA
       ;;
(EXT61PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF120DG DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q120DG(&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F120DG
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p EXT61P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=EXT61PBB
       ;;
(EXT61PBB)
       m_CondExec 04,GE,EXT61PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EXT61PZA
       ;;
(EXT61PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EXT61PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
