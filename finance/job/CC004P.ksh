#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC004P.ksh                       --- VERSION DU 08/10/2016 12:42
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCC004 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.21.34 BY BURTEC6                      
#    STANDARDS: P  JOBSET: CC004P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM BMD232 **  COBOL2/DB2                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC004PA
       ;;
(CC004PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CC004PAA
       ;;
(CC004PAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE  DB2 EN LECTURE                                                
#                                                                              
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER EN SORTIE (LREL 200) A ENVOYE PAR CFT                        
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FMD232 ${DATA}/PXX0/F07.BMD232AP.GATEWAY
#                                                                              
# ******* FICHIER EN SORTIE (LREL 080)                                         
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDCFT ${DATA}/PXX0/F07.BMD232BP.PARAM.GETAWAY
#                                                                              
# ********FICHIER EN SORTIE (LREL 080)                                         
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDCFT2 ${DATA}/PXX0/F07.BMD232CP.PARAM.GATEWAY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD232 
       JUMP_LABEL=CC004PAB
       ;;
(CC004PAB)
       m_CondExec 04,GE,CC004PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          TRI DES FICHIERS CUMULS DES CHAINES CC003 PGM BCC007                
#          ET FOURNITURE POUR ENVOI FIC VERS GETAWAY                           
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC004PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CC004PAD
       ;;
(CC004PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BCC006AP.GETWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BCC006AD.GETWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BCC006AL.GETWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BCC006AM.GETWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BCC006AO.GETWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BCC006AY.GETWAY
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BCC006CF.GATEWAY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CC004PAE
       ;;
(CC004PAE)
       m_CondExec 00,EQ,CC004PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SYSIN GENERE PAR PGM                                                       
#   ENVOI DU FICHIER BMD232BP VIA XFB-GATEWAY QUI ENVOIE IDF=BCC006CF          
#   CI-DESSOUS COMPTE RENDU D'EXECUTION                                        
#   PART=XFBPRO,IDF=BCC006CF,SAPPL=THEMIS,FNAME=PXX0.F                         
#   07.BCC006CF.GATEWAY(0),NFNAME=ALIMENTATIONNEM_2012                         
#   0910.TXT)                                                                  
#   REPRISE : OUI                                                              
# ********************************************************************         
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    FILE  NAME=BMD232BP,MODE=I                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FT??????                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC004PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CC004PAG
       ;;
(CC004PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PXX0/F07.BMD232BP.PARAM.GETAWAY
# ********************************************************************         
#   SYSIN GENERE PAR PGM                                                       
#   ENVOI DU FICHIER BMD232CP VIA XFB-GATEWAY QUI ENVOIE IDF=BMD232CP          
#   PART=XFBPRO,IDF=BMD232CP,SAPPL=THEMIS,FNAME=PXX0.F                         
#   07.BMD232AP.GATEWAY(0),NFNAME=ALIMENTATIONPM20_201                         
#   20910.TXT)                                                                 
#   REPRISE : OUI                                                              
# ********************************************************************         
#  SEND PART=XFBPRO,                                                           
#    IDF=BMD232CP,                                                             
#  SAPPL=THEMIS,                                                               
#    FNAME=PXX0.F07.BMD232AP.GETAWAY(0),                                       
#  AlimentationPM20_20120909.txt                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    FILE  NAME=BMD232CP,MODE=I                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FT??????                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CC004PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CC004PAJ
       ;;
(CC004PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR -g ${G_A2} SYSIN ${DATA}/PXX0/F07.BMD232CP.PARAM.GATEWAY
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
