#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CG1A4D.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDCG1A4 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/29 AT 17.42.54 BY PREPA2                       
#    STANDARDS: P  JOBSET: CG1A4D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CONSTITUTION DU FICHIER REPRISE POUR S.A.P EN CAS DE PLANTA         
#          DE LA CHAINE CGSAPD                                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ECS                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CG1A4DA
       ;;
(CG1A4DA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2009/01/29 AT 17.42.54 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: CG1A4D                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-CGSAPD'                            
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CG1A4DAA
       ;;
(CG1A4DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FG14FPD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.PSE00D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GRA00D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GBA00D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GBA02D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.IF00FPD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.AV001D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.AV201D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.CC200D
# ******* FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BCG000D.REPRISE
# ******* FICHIER DE REPRISE FUSIONNE                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/F91.BCG000D.REPRISE
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_98_25 98 CH 25
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CG1A4DAB
       ;;
(CG1A4DAB)
       m_CondExec 00,EQ,CG1A4DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
