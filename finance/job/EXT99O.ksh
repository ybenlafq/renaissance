#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT99O.ksh                       --- VERSION DU 08/10/2016 22:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POEXT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/03 AT 11.57.14 BY BURTECN                      
#    STANDARDS: P  JOBSET: EXT99O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ : RSGA09O RSGA11O RSGA12O RSGA29O              
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  ET UTILISER LA CHAINE DE RECOVER DB2 DB2RBO AVEC CE RBA EN PREP             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EXT99OA
       ;;
(EXT99OA)
#
#EXT99OAA
#EXT99OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT99OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2003/01/03 AT 11.57.14 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: EXT99O                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'MAJ CODES ETATS'                       
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EXT99OAD
       ;;
(EXT99OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES CODES ETATS VENANT DE EXT99P                             
       m_FileAssign -d SHR -g +0 FEX998 ${DATA}/PXX0/F07.BEX998AP
#                                                                              
# ******  CODE MARKETING                                                       
#    RSGA09O  : NAME=RSGA09O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09O /dev/null
# ******  RELATION ETAT FAMILLE/RAYON                                          
#    RSGA11O  : NAME=RSGA11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
# ******  RELATION FAMILLE/CODE MARKETING                                      
#    RSGA12O  : NAME=RSGA12O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******  LIBELLES DES IMBRICATIONS CODE MARKETING                             
#    RSGA29O  : NAME=RSGA29O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29O /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX999 
       JUMP_LABEL=EXT99OAE
       ;;
(EXT99OAE)
       m_CondExec 04,GE,EXT99OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
