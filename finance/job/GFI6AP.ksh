#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GFI6AP.ksh                       --- VERSION DU 08/10/2016 22:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGFI6A -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/07/29 AT 15.47.43 BY BURTECC                      
#    STANDARDS: P  JOBSET: GFI6AP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   G�N�RATION D'UN MESSAGE AOI POUR AVERTIR DE LA CR�ATION DU LIEN            
#   VIRTUEL                                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GFI6APA
       ;;
(GFI6APA)
#
#GFI6APAA
#GFI6APAA Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#GFI6APAA
#
       EXACX=${EXACX:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2014/07/29 AT 15.47.43 BY BURTECC                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GFI6AP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-GFI60P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
