#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SIGC2O.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POSIGC2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/08/20 AT 11.53.36 BY PREPA2                       
#    STANDARDS: P  JOBSET: SIGC2O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# ********************************************************************         
#  PGM : BSIG20                                                                
#  ------------                                                                
#  REPRISE DU FICHIER SXGC2O ISSU D'UNE EXPLO HRV3 (UNIX) AFIN DE              
#  PACKER LES ZONES DECIMALES                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SIGC2OA
       ;;
(SIGC2OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SIGC2OAA
       ;;
(SIGC2OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 FEXPLO ${DATA}/PXX0/F16.SXGC2O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F821.SXGC2O
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSIG20 ${DATA}/PTEM/SIGC2OAA.BSIG20CO
       m_ProgramExec BSIG20 
# ********************************************************************         
#  TRI DU FICHIER ISSU DU PGM BSIG20                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAD
       ;;
(SIGC2OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SIGC2OAA.BSIG20CO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/SIGC2OAD.BSIG20DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_25 16 CH 25
 /FIELDS FLD_CH_59_2 59 CH 2
 /FIELDS FLD_PD_41_9 41 PD 9
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_PD_50_9 50 PD 9
 /FIELDS FLD_CH_61_7 61 CH 7
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_59_2 ASCENDING,
   FLD_CH_16_25 ASCENDING,
   FLD_CH_61_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_9,
    TOTAL FLD_PD_50_9
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGC2OAE
       ;;
(SIGC2OAE)
       m_CondExec 00,EQ,SIGC2OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG25                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER PREVISION POUR FDC                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAG
       ;;
(SIGC2OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -d SHR FTYPPAIE ${DATA}/CORTEX4.P.MTXTFIX1/PAYPROO
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A2} FSIG20 ${DATA}/PTEM/SIGC2OAD.BSIG20DO
# ------  FICHIER POUR CONTROLE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FPARAM ${DATA}/PTEM/SIGC2OAG.BSIG25AO
# ------  FICHIER REPRIS DANS LE BSIG26                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FCTL ${DATA}/PTEM/SIGC2OAG.BSIG25BO
# ------  FICHIER POUR GCT (PAS D'INJECTION EN COMPTA)                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FGCT ${DATA}/PNCGO/F16.SIGGC2O
# ------  FICHIER POUR FDC (PREVISION)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 FFDC ${DATA}/PNCGO/F16.SIGAFDC
# ------  FICHIER POUR FDC (PREVISION)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 61 -g +1 FREMUN ${DATA}/PXX0/F16.BSIG25GO
# ------  ETATS DE COMPTE RENDUS                                               
       m_OutputAssign -c 9 -w ISG010 FANOSYNT
       m_OutputAssign -c 9 -w ISG020 FANODET
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG25 
       JUMP_LABEL=SIGC2OAH
       ;;
(SIGC2OAH)
       m_CondExec 04,GE,SIGC2OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25  (CONTROLE ANALYTIQUE)               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAJ
       ;;
(SIGC2OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SIGC2OAG.BSIG25BO
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 SORTOUT ${DATA}/PTEM/SIGC2OAJ.BSIG26DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_1_51 1 CH 51
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_33 1 CH 33
 /KEYS
   FLD_CH_1_33 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2OAK
       ;;
(SIGC2OAK)
       m_CondExec 00,EQ,SIGC2OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE GENERAL)                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAM
       ;;
(SIGC2OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/SIGC2OAG.BSIG25BO
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 SORTOUT ${DATA}/PTEM/SIGC2OAM.BSIG26EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_21 1 CH 21
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_34_9 34 CH 9
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_43_9 43 CH 9
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_52_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_52_8,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2OAN
       ;;
(SIGC2OAN)
       m_CondExec 00,EQ,SIGC2OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE RUBRIQUE)                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAQ
       ;;
(SIGC2OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/SIGC2OAG.BSIG25BO
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -g +1 SORTOUT ${DATA}/PTEM/SIGC2OAQ.BSIG26FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_34_9 34 CH 9
 /FIELDS FLD_CH_28_6 28 CH 6
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_21 1 CH 21
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_28_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_28_6,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGC2OAR
       ;;
(SIGC2OAR)
       m_CondExec 00,EQ,SIGC2OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG26                                                                
#  ------------                                                                
#  EDITION DES ETATS DE CONTROLE                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAT
       ;;
(SIGC2OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DES TRIS PRECEDANT                                      
       m_FileAssign -d SHR -g ${G_A6} FCTLANA ${DATA}/PTEM/SIGC2OAJ.BSIG26DO
       m_FileAssign -d SHR -g ${G_A7} FCTLGEN ${DATA}/PTEM/SIGC2OAM.BSIG26EO
       m_FileAssign -d SHR -g ${G_A8} FCTLRUB ${DATA}/PTEM/SIGC2OAQ.BSIG26FO
# ------  FICHIER DE CONTROLE SI OK OU ANO                                     
       m_FileAssign -d SHR -g ${G_A9} FPARAM ${DATA}/PTEM/SIGC2OAG.BSIG25AO
# ------  ETATS DE CONTROLE                                                    
       m_OutputAssign -c 9 -w ISG025 FGENE
       m_OutputAssign -c 9 -w ISG026 FANA
       m_OutputAssign -c 9 -w ISG027 FRUB
# ******************************************************************           
#  REMISE A ZERO DU FICHIER PROVENANT D'UNIX SXGC2O / SX821OS                  
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG26 
       JUMP_LABEL=SIGC2OAU
       ;;
(SIGC2OAU)
       m_CondExec 04,GE,SIGC2OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SIGC2OAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OAX
       ;;
(SIGC2OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT1 ${DATA}/PXX0/F16.SXGC2O
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT2 ${DATA}/PXX0/F821.SXGC2O
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2OAX.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SIGC2OAY
       ;;
(SIGC2OAY)
       m_CondExec 16,NE,SIGC2OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SIGC2OZA
       ;;
(SIGC2OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGC2OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
