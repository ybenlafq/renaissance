#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CC100P.ksh                       --- VERSION DU 08/10/2016 22:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCC100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/02/11 AT 10.47.15 BY BURTEC6                      
#    STANDARDS: P  JOBSET: CC100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM BCC100                                                                  
#  1. SUPPRESSION DES ENREGISTREMENTS DE RTCC01 (CAISSES OUVERTES)             
#     DONT LA DATE DE VALIDATION EST INF�RIEURE � LA DATE DE TRAITEMEN         
#     MOINS LE D�LAI PARAM�TR� DANS LA SOUS-TABLE DELAI                        
#     (PAR D�FAUT, DELAI DE 90 JOURS), SUR LE CODE PROGRAMME BCC100            
#                                                                              
#  2. SUPPRESSION DES ENREGISTREMENTS DE RTCC02 (SACOCHES � RECEVOIR),         
#     DE RTCC03 (REMONT�ES LOG DE CAISSE) ET DE RTCC04 (REMONT�ES              
#     SYNTH�TIQUES DE CAISSE) POUR LESQUELS IL N'EXISTE PLUS                   
#     D'ENREGISTREMENTS DANS LA TABLE RTCC01                                   
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CC100PA
       ;;
(CC100PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=CC100PAA
       ;;
(CC100PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER PARAM�TRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# *****   TABLES EN MAJ                                                        
#    RSCC01   : NAME=RSCC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSCC01 /dev/null
#    RSCC02   : NAME=RSCC02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSCC02 /dev/null
#    RSCC03   : NAME=RSCC03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSCC03 /dev/null
#    RSCC04   : NAME=RSCC04,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSCC04 /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCC100 
       JUMP_LABEL=CC100PAB
       ;;
(CC100PAB)
       m_CondExec 04,GE,CC100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
