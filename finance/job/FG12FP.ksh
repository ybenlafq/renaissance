#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG12FP.ksh                       --- VERSION DU 08/10/2016 13:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG12F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/06/22 AT 10.00.43 BY BURTEC5                      
#    STANDARDS: P  JOBSET: FG12FP                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  BFG120 : CREATION DES FACTURES DES ABONNEMENTS DANS RTFG01                  
#              (DEFINITION DES ABONNEMENT DANS RTFG03 PAR TFG10)               
#           MAJ DE RTFG01-05-06                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG12FPA
       ;;
(FG12FPA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=FG12FPAA
       ;;
(FG12FPAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
#                                                                              
# ******* TABLE DES MVTS DE FACTURATION INTER-SOCIETE  GROUPE                  
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES ABONNEMENTS POUR LA FACTURATION GROUPE                     
#    RSFG03   : NAME=RSFG03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG03 /dev/null
# ******* TABLE DES COMMENTAIRES SUR FACTURE                                   
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG06 /dev/null
# ******* TABLE DES VENTILATIONS DES FACTURES INTER-SOCIETE GROUPE             
#    RSFG07   : NAME=RSFG07,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG07 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *******   EDITION COMPTE RENDU                                               
       m_OutputAssign -c 9 -w IFG120 IFG120
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG120 
       JUMP_LABEL=FG12FPAB
       ;;
(FG12FPAB)
       m_CondExec 04,GE,FG12FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
