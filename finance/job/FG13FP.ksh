#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FG13FP.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFG13F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/02 AT 09.37.40 BY BURTECA                      
#    STANDARDS: P  JOBSET: FG13FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE                  
#     REPRISE : OUI                                                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FG13FPA
       ;;
(FG13FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FG13FPAA
       ;;
(FG13FPAA)
       m_CondExec ${EXAAA},NE,YES 
#  DEPENDANCES POUR PLAN :             *                                       
# ******** FICHIER CONTENANT LE NUMERO DE RBA                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QFG13FP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FG13FPAA
       m_ProgramExec IEFBR14 "RMAC,FG13FP.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPAD
       ;;
(FG13FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ******* FICHIER CONTENANT LE NUMERO DE RBA                                   
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QFG13FP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPAE
       ;;
(FG13FPAE)
       m_CondExec 00,EQ,FG13FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFG130 :EXTRACTION DES FACTURES DES FRAIS ANNEXES AUX MUTATIONS DE          
#                                  MARCHANDISES DANS RTFG01                    
#          CREATION FIC FFGRMUT:   EDITION FACTURES DESTINEE A                 
#                                  L'IMPRESSION GENERALISEE                    
#                                                                              
#  REPRISE: EN TETE DE CHAINE APRES BACKOUT DU JOBSET                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAG
       ;;
(FG13FPAG)
       m_CondExec ${EXAAK},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
# ******* TABLE DES CALCUL DES FRAIS ANNEXES DES FACTURES INTERSOCIETE         
#    RSFG10   : NAME=RSFG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG10 /dev/null
#                                                                              
# ******* TABLE DES FACTURES INTER-SOCIETE                                     
#    RSFG01   : NAME=RSFG01,MODE=U - DYNAM=YES                                 
# -X-FG13FPR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSFG01 /dev/null
# ******* TABLE DES COMMENTAIRES SUR FACTURES                                  
#    RSFG06   : NAME=RSFG06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG06 /dev/null
# ******* TABLE DETAILS D'UNE VENTILATION DE FACTURES INTERSOCIETES            
#    RSFG07   : NAME=RSFG07,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFG07 /dev/null
#                                                                              
# ******* CARTE PARAMETRE POUR REPRISE                                         
       m_FileAssign -d SHR FREPRISE ${DATA}/CORTEX4.P.MTXTFIX1/FG13FP1
       m_FileAssign -d SHR FDCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FG13FP2
       m_FileAssign -d SHR FSOCCTA ${DATA}/CORTEX4.P.MTXTFIX1/FG13FP3
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DU MOIS MMSSAA                                                  
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER DES FACTURES ANNEXES A EDITER                                
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFGRMUT ${DATA}/PTEM/FG13FPAG.BFG130AP
# ******* FICHIER DES ESCOMPTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FFGESCPT ${DATA}/PTEM/FG13FPAG.BFG245AP
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG130 
       JUMP_LABEL=FG13FPAH
       ;;
(FG13FPAH)
       m_CondExec 04,GE,FG13FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES A EDITER                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAJ
       ;;
(FG13FPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FG13FPAG.BFG130AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG13FPAJ.BFG130BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_58_7 58 PD 7
 /FIELDS FLD_PD_65_7 65 PD 7
 /FIELDS FLD_PD_72_7 72 PD 7
 /FIELDS FLD_PD_54_4 54 PD 4
 /FIELDS FLD_CH_7_47 7 CH 47
 /FIELDS FLD_PD_79_7 79 PD 7
 /KEYS
   FLD_CH_7_47 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_54_4,
    TOTAL FLD_PD_58_7,
    TOTAL FLD_PD_65_7,
    TOTAL FLD_PD_72_7,
    TOTAL FLD_PD_79_7
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG13FPAK
       ;;
(FG13FPAK)
       m_CondExec 00,EQ,FG13FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG240  : MISE EN FORME DU FICHIER D'EDITION DE FACTURE ANNEXES          
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAM
       ;;
(FG13FPAM)
       m_CondExec ${EXAAU},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
#                                                                              
# ****** DATE : JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DU MOIS MMSSAA                                                  
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FFGRMUT ${DATA}/PTEM/FG13FPAJ.BFG130BP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG13FPAM.BFG240AP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDGA ${DATA}/PTEM/FG13FPAM.BFG240CP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG240 
       JUMP_LABEL=FG13FPAN
       ;;
(FG13FPAN)
       m_CondExec 04,GE,FG13FPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES FILIALES                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAQ
       ;;
(FG13FPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG13FPAQ.BFG240BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG13FPAR
       ;;
(FG13FPAR)
       m_CondExec 00,EQ,FG13FPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAT PGM=DFSRRC00   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAT
       ;;
(FG13FPAT)
       m_CondExec ${EXABE},NE,YES 
# BE      IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG130RC1),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG130RC1
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ******  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A5} FFGEDG ${DATA}/PTEM/FG13FPAQ.BFG240BP
# ******  BASE IMPRESSION GENERALISEE                                          
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
# IGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG13FPR2)                       
# IGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                   
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#         TRI DU FICHIER DES FACTURES DE PARIS                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPAX
       ;;
(FG13FPAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG012 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPAY
       ;;
(FG13FPAY)
       m_CondExec 00,EQ,FG13FPAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE PARIS                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ******************************************************** SANS MODIF          
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBA
       ;;
(FG13FPBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012P),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBB
       ;;
(FG13FPBB)
       m_CondExec 00,EQ,FG13FPBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE DPM                                   
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ****************************************************** NEW *********         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBD
       ;;
(FG13FPBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012D),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "991"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBE
       ;;
(FG13FPBE)
       m_CondExec 00,EQ,FG13FPBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE DAL                                   
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBG
       ;;
(FG13FPBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012M),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "989"
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBH
       ;;
(FG13FPBH)
       m_CondExec 00,EQ,FG13FPBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE LUXEMBOURG                            
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBJ
       ;;
(FG13FPBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012X),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AX
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBK
       ;;
(FG13FPBK)
       m_CondExec 00,EQ,FG13FPBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE MGI OUEST                             
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************* NEW ******         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBM
       ;;
(FG13FPBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012O),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_1_42 1 CH 42
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBN
       ;;
(FG13FPBN)
       m_CondExec 00,EQ,FG13FPBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE LYON                                  
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************* NEW ******         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBQ
       ;;
(FG13FPBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012Y),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBR
       ;;
(FG13FPBR)
       m_CondExec 00,EQ,FG13FPBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE LILLE                                 
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************* NEW ******         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBT
       ;;
(FG13FPBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012L),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBU
       ;;
(FG13FPBU)
       m_CondExec 00,EQ,FG13FPBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES FACTURES DE CAPROFEM                              
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************* NEW ******         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPBX
       ;;
(FG13FPBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FG13FPAM.BFG240CP
# ******* FICHIER D'EDITION DES FACTURES                                       
#  //SORTOUT  DD  SYSOUT=(9,JFG012K),LRECL=133,RECFM=FBA,SPIN=UNALLOC          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ SORTOUT ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "801"
 /DERIVEDFIELD CST_1_4 "997"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_43_133 43 CH 133
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 OR FLD_CH_15_3 EQ CST_3_8 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPBY
       ;;
(FG13FPBY)
       m_CondExec 00,EQ,FG13FPBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES ESCOMPTES A EDITER                                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCA
       ;;
(FG13FPCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/FG13FPAG.BFG245AP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG13FPCA.BFG245BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_41 7 CH 41
 /KEYS
   FLD_CH_7_41 ASCENDING
 /* Record Type = F  Record Length = 70 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG13FPCB
       ;;
(FG13FPCB)
       m_CondExec 00,EQ,FG13FPCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG245  : MISE EN FORME DU FICHIER D'EDITION DES ESCOMPTES               
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCD
       ;;
(FG13FPCD)
       m_CondExec ${EXADH},NE,YES 
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#         M907.RTGA01                                                          
#    RSGA01FP : NAME=RSGA01FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA01FP /dev/null
# ******* TABLE DES COMPTES (RDAR)                                             
#    RSGA10FP : NAME=RSGA10FP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR RSGA10FP /dev/null
#                                                                              
# ****** DATE : JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DU MOIS MMSSAA                                                  
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A16} FFGESCPT ${DATA}/PTEM/FG13FPCA.BFG245BP
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 FFGEDG ${DATA}/PTEM/FG13FPCD.BFG245CP
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFG245 
       JUMP_LABEL=FG13FPCE
       ;;
(FG13FPCE)
       m_CondExec 04,GE,FG13FPCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         TRI DU FICHIER DES ESCOMPTES FILIALES                                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCG
       ;;
(FG13FPCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/FG13FPCD.BFG245CP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FG13FPCG.BFG245DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FG13FPCH
       ;;
(FG13FPCH)
       m_CondExec 00,EQ,FG13FPCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     BFG230  : CREATION DE L'ETAT JFG012 DANS L'IMPRESSION GENERALISE         
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCJ PGM=DFSRRC00   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCJ
       ;;
(FG13FPCJ)
       m_CondExec ${EXADR},NE,YES 
# DR      IMSSTEP PGM=BFG230,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,FG130RC3),RSTRT=SAME                                  
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:CORTEX4.MVS.MLNKMOD
#         DD DSN=IMSVS.RESLIB,                                                 
#         DISP=SHR                                                             
# DFSRESLB DD DSN=IMSVS.RESLIB,                                                
#          DISP=SHR                                                            
# IMSACB   DD  DSN=SYS3.EXPLOIT.ACBS,DISP=SHR                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.FG130RC3
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" DDOTV02
# ******  FICHIER DES FACTURES A INSERER                                       
       m_FileAssign -d SHR -g ${G_A18} FFGEDG ${DATA}/PTEM/FG13FPCG.BFG245DP
# ******  BASE IMPRESSION GENERALISEE                                          
# IGVP0   FILE  NAME=PDIGVP0P,MODE=U,REST=(YES,FG13FPR4)                       
# IGVIP   FILE  NAME=PDIGVI0P,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PPA0.F07.PDIGVP0P
       m_FileAssign -d SHR DIGVIP ${DATA}/PPA0.F07.PDIGVI0P
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX5/DDITV02
       m_ProgramExec -b TPIGG 
# ********************************************************************         
#         TRI DU FICHIER DES ESCOMPTES DE PARIS                                
#     REPRISE : EN TETE DE CHAINE APRES BACKOUT JOBSET                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCM
       ;;
(FG13FPCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/FG13FPCD.BFG245CP
# ******* FICHIER D'EDITION DES FACTURES                                       
       m_OutputAssign -c 9 -w JFG012 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_43_133 43 CH 133
 /FIELDS FLD_CH_1_42 1 CH 42
 /FIELDS FLD_CH_15_3 15 CH 3
 /CONDITION CND_1 FLD_CH_15_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_42 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_43_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=FG13FPCN
       ;;
(FG13FPCN)
       m_CondExec 00,EQ,FG13FPCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012P FACTURE NETTING POUR PARIS                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCQ PGM=IEBGENER   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCQ
       ;;
(FG13FPCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AP
       m_OutputAssign -c 9 -w JFG012P SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPCR
       ;;
(FG13FPCR)
       m_CondExec 00,EQ,FG13FPCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012D FACTURE NETTING POUR MARSEILLE                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCT PGM=IEBGENER   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCT
       ;;
(FG13FPCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AD
       m_OutputAssign -c 9 -w JFG012D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPCU
       ;;
(FG13FPCU)
       m_CondExec 00,EQ,FG13FPCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012M FACTURE NETTING POUR METZ                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPCX PGM=IEBGENER   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPCX
       ;;
(FG13FPCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AM
       m_OutputAssign -c 9 -w JFG012M SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPCY
       ;;
(FG13FPCY)
       m_CondExec 00,EQ,FG13FPCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012X FACTURE NETTING POUR LUXEMBOURG                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPDA PGM=IEBGENER   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPDA
       ;;
(FG13FPDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AX
       m_OutputAssign -c 9 -w JFG012X SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPDB
       ;;
(FG13FPDB)
       m_CondExec 00,EQ,FG13FPDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012O FACTURE NETTING POUR MGI OUEST                              
# ********************************************************************         
#  AEV      STEP  PGM=IEBGENER,LANG=UTIL                                       
#                                                                              
#  SYSUT1   FILE  NAME=BFG241AO,MODE=I                                         
#  SYSUT2   REPORT SYSOUT=(9,JFG012O),FREE=CLOSE                               
#  //SYSIN    DD DUMMY                                                         
# ********************************************************************         
#  EDITION JFG012Y FACTURE NETTING POUR LYON                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPDD PGM=IEBGENER   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPDD
       ;;
(FG13FPDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AY
       m_OutputAssign -c 9 -w JFG012Y SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPDE
       ;;
(FG13FPDE)
       m_CondExec 00,EQ,FG13FPDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012L FACTURE NETTING POUR LILLE                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPDG PGM=IEBGENER   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPDG
       ;;
(FG13FPDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AL
       m_OutputAssign -c 9 -w JFG012L SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPDH
       ;;
(FG13FPDH)
       m_CondExec 00,EQ,FG13FPDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JFG012K FACTURE NETTING POUR CAPROFEM                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FG13FPDJ PGM=IEBGENER   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPDJ
       ;;
(FG13FPDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXFG0F.BFG241AK
       m_OutputAssign -c 9 -w JFG012K SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=FG13FPDK
       ;;
(FG13FPDK)
       m_CondExec 00,EQ,FG13FPDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FG13FPZA
       ;;
(FG13FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FG13FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
