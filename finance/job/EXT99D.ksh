#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EXT99D.ksh                       --- VERSION DU 08/10/2016 13:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDEXT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/03/03 AT 11.27.45 BY BURTECL                      
#    STANDARDS: P  JOBSET: EXT99D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ : RSGA09D RSGA11D RSGA12D RSGA29D              
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  ET UTILISER LA CHAINE DE RECOVER DB2 DB2RBD AVEC CE RBA EN PREP             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EXT99DA
       ;;
(EXT99DA)
#
#EXT99DAA
#EXT99DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#EXT99DAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   98/03/03 AT 11.27.43 BY BURTECL                  
# *    JOBSET INFORMATION:    NAME...: EXT99D                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'MAJ CODES ETATS'                       
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EXT99DAD
       ;;
(EXT99DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES CODES ETATS VENANT DE EXT99P                             
       m_FileAssign -d SHR -g +0 FEX998 ${DATA}/PXX0/F07.BEX998AP
#                                                                              
# ******  CODE MARKETING                                                       
#    RSGA09D  : NAME=RSGA09D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09D /dev/null
# ******  RELATION ETAT FAMILLE/RAYON                                          
#    RSGA11D  : NAME=RSGA11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11D /dev/null
# ******  RELATION FAMILLE/CODE MARKETING                                      
#    RSGA12D  : NAME=RSGA12D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12D /dev/null
# ******  LIBELLES DES IMBRICATIONS CODE MARKETING                             
#    RSGA29D  : NAME=RSGA29D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29D /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX999 
       JUMP_LABEL=EXT99DAE
       ;;
(EXT99DAE)
       m_CondExec 04,GE,EXT99DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
