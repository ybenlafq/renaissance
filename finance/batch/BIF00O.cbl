      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID.  BIF00O.                                             00030000
       AUTHOR.  C-EST-MAURICE.                                          00040000
      ******************************************************************00050000
      * CREATION M A 20/10/2015                                        *00060002
      * TRAITEMENT DU FICHIER SOFINCO LOA. PGM EST UNE DUPPLI DU       *00061002
      * BIF00S A L'ORIGINE.                                            *00062002
      ******************************************************************00070000
       ENVIRONMENT DIVISION.                                            00080000
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00090000
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                   00100000
      *    DECIMAL-POINT IS COMMA.                                      00110000
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00120000
       FILE-CONTROL.                                                    00130000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FSOFLOA   ASSIGN TO FSOFLOA                           00140000
      *                     FILE STATUS IS ST-LOA.                      00150000
      *--                                                                       
           SELECT FSOFLOA   ASSIGN TO FSOFLOA                                   
                            FILE STATUS IS ST-LOA                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00160000
       FILE SECTION.                                                    00170000
       FD  FSOFLOA                                                      00180000
           RECORDING F                                                  00190000
           BLOCK 0 RECORDS                                              00200000
           LABEL RECORD STANDARD.                                       00210000
                                                                        00220000
       01  FSOFLOA-RECORD           PIC X(120).                         00230000
                                                                        00240000
       WORKING-STORAGE SECTION.                                         00250000
       01  WS-BIF00O.                                                   00260000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     COMP   VALUE ZERO.       00270000
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)     COMP   VALUE ZERO.       00280000
      *--                                                                       
           05  L                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
2002       05  W-DEVISE-FRF      PIC X(3)             VALUE 'FRF'.      00281002
2002       05  W-DEVISE-EUR      PIC X(3)             VALUE 'EUR'.      00282002
           05  W-MTNET           PIC S9(13)V99 COMP-3 VALUE ZERO.       00290000
           05  ST-LOA            PIC 99               VALUE ZERO.       00300000
           05  W-ZERO            PIC X(20)            VALUE ALL '0'.    00310000
           05  W-BETDATC         PIC X(8)             VALUE 'BETDATC'.  00320000
                                                                        00330000
       01  DSECT-SOFLOA.                                                00340000
           05  SOF-CODENR         PIC XXX.                              00350000
               88  SOF-TETE           VALUE 'AAA'.                      00360000
               88  SOF-FIN            VALUE 'ZZZ'.                      00370000
           05  SOF-NVENDEUR       PIC X(11).                            00380000
           05  SOF-CENSEIGNE      PIC XX.                               00390000
           05  SOF-NDOSSIER       PIC X(11).                            00400000
           05  SOF-NOMCLIENT      PIC X(15).                            00410000
           05  SOF-BDC            PIC X(12).                            00420000
           05  SOF-MTACHAT        PIC 9(6)V99.                          00430000
           05  SOF-MTVERSE        PIC 9(6)V99.                          00440000
           05  SOF-MTFINANCE      PIC 9(6)V99.                          00450000
AD02       05  SOF-DACHAT         PIC X(8).                             00460000
           05  SOF-DFINANCE       PIC X(8).                             00470000
           05  SOF-RFINANCE       PIC X(7).                             00480000
           05  SOF-RENSEIGNE      PIC XX.                               00490000
2002       05  SOF-CDEVISE        PIC X(03).                            00500002
AD02       05  SOF-CTYPMVT        PIC X(03).                            00501002
      *    05  FILLER             PIC X(11).                            00501002
0209       05  SOF-WANNUL         PIC X(01).                            00501002
0209       05  FILLER             PIC X(10).                            00501002
                                                                        00510000
       01  TETE-SOFLOA.                                                 00520000
           05  FILLER             PIC X(27).                            00530000
           05  SOF-DFICORG        PIC X(8).                             00540000
           05  FILLER             PIC X(15).                            00550000
                                                                        00560000
       01  FIN-SOFLOA.                                                  00570000
           05  FILLER             PIC X(19).                            00580000
           05  SOF-NFICORG        PIC 9(8).                             00590000
           05  FILLER             PIC X(13).                            00600000
                                                                        00610000
           COPY FIF000.                                                 00620000
           COPY ABENDCOP.                                               00630000
           COPY WORKDATC.                                               00640000
      *---------------------------------------                          00650000
       LINKAGE                         SECTION.                         00660000
      *---------------------------------------                          00670000
       01  L-ORGANISME.                                                 00680000
           05  L-CORGANISME     PIC XXX.                                00690000
           05  L-FSTATUS        PIC 99.                                 00700000
           05  L-NBRECUS        PIC S9(5)       COMP-3.                 00710000
           05  L-NBREJETS       PIC S9(5)       COMP-3.                 00720000
           05  L-LGIDENT        PIC S999        COMP-3.                 00730000
           05  L-SUFFIXE        PIC X.                                  00740000
           05  L-REPRISE        PIC XXX.                                00750000
           05  L-DFICORG        PIC X(8).                               00760000
           05  L-NFICORG        PIC S9(7)       COMP-3.                 00770000
      *                                                                 00780000
       01  L-FIF000             PIC X(192).                             00790000
      *                                                                 00800000
       01  L-DATE               PIC X(8).                               00810000
      *================================================                 00820000
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.            00830000
      *================================================                 00840000
      *---------------------------------------                          00850000
       MODULE-BIF00O                   SECTION.                         00860000
      *---------------------------------------                          00870000
           DISPLAY 'MAO BIF00O'                                                 
           DISPLAY 'LGIDENT : ' L-LGIDENT                                       
           DISPLAY 'IF00    : ' DSECT-IF000                                     
           MOVE L-LGIDENT           TO L.                               00880000
           MOVE L-FIF000            TO DSECT-IF000.                     00890000
           IF L-FSTATUS = 99                                            00900000
              OPEN INPUT FSOFLOA                                        00910000
              MOVE ST-LOA           TO L-FSTATUS                        00920000
           END-IF.                                                      00930000
           IF ST-LOA = 00                                               00940000
              PERFORM LECTURE-FSOFLOA                                   00950000
           END-IF.                                                      00960000
                                                                        00970000
           IF ST-LOA = 00                                               00980000
              PERFORM TRAITEMENT-SOFLOA                                 00990000
           END-IF.                                                      01000000
                                                                        01010000
           IF ST-LOA = 10                                               01020000
              CLOSE FSOFLOA                                             01030000
           END-IF.                                                      01040000
                                                                        01050000
           GOBACK.                                                      01060000
      *---------------------------------------                          01070000
       TRAITEMENT-SOFLOA               SECTION.                         01080000
      *---------------------------------------                          01090000
           PERFORM LECTURE-FSOFLOA                                      01100000
                   UNTIL ST-LOA = 10                                    01110000
                   OR    SOF-CODENR = 'AVA'                             01120000
                   OR    SOF-CODENR = 'AH9'.                            01130000
                                                                        01140000
           IF ST-LOA NOT = 10                                           01150000
              PERFORM TRAITEMENT-FICHIER-SOF                            01160000
              ADD  1                TO L-NBRECUS                        01170000
              MOVE DSECT-IF000      TO L-FIF000                         01180000
           END-IF.                                                      01190000
      *---------------------------------------                          01200000
       TRAITEMENT-FICHIER-SOF          SECTION.                         01210000
      *---------------------------------------                          01220000
           MOVE SPACES              TO IF000-NIDENT.                    01230000
           MOVE SOF-NVENDEUR (1:L)  TO IF000-NIDENT.                    01240000
           MOVE SOF-DFINANCE        TO IF000-DFINANCE.                  01250000
AD02       MOVE SOF-DACHAT          TO IF000-DOPER.                     01250000
           MOVE SOF-NOMCLIENT       TO IF000-NOMCLIENT.                 01260000
           MOVE SOF-BDC             TO IF000-REFINTERNE.                01270000
AD02  *    MOVE '*'                 TO IF000-CTYPMVT.                   01280000
           IF SOF-CTYPMVT = 'LOA' THEN                                          
              MOVE  '1'  TO  IF000-CTYPMVT                                      
           ELSE                                                                 
              IF SOF-CTYPMVT = 'DAR' THEN                                       
                 MOVE  '2'  TO  IF000-CTYPMVT                                   
              END-IF                                                            
AD02       END-IF                                                               
           MOVE SOF-NDOSSIER        TO IF000-NPORTEUR.                  01290000
           MOVE 1                   TO IF000-NFACTURES.                 01300000
           COMPUTE IF000-MTNET  = SOF-MTFINANCE.                        01310000
           IF SOF-CODENR = 'AVA'                                        01320000
              COMPUTE IF000-MTCOM  = SOF-MTVERSE                        01330000
              COMPUTE IF000-MTBRUT = SOF-MTACHAT                        01340000
           ELSE                                                         01350000
              COMPUTE IF000-MTCOM  = SOF-MTACHAT                        01360000
                                   - SOF-MTVERSE                        01370000
                                   - SOF-MTFINANCE                      01380000
              COMPUTE IF000-MTBRUT = IF000-MTNET + IF000-MTCOM          01390000
0209          IF SOF-WANNUL = 'O'                                               
0209             MULTIPLY  -1 BY  IF000-MTCOM                                   
0209             MULTIPLY  -1 BY  IF000-MTBRUT                                  
0209             MULTIPLY  -1 BY  IF000-MTNET                                   
0209          END-IF                                                            
           END-IF.                                                      01400000
      *                                                                 01410000
2002       PERFORM CONTROLE-DEVISE.                                     01420002
           PERFORM CONTROLE-REJETS.                                     01421002
      *                                                                 01421102
      *---------------------------------------                          01421202
2002   CONTROLE-DEVISE                SECTION.                          01421303
      *---------------------------------------                          01421402
      *                                                                 01421502
2002       IF SOF-CDEVISE = SPACES THEN                                 01421603
2002          MOVE W-DEVISE-FRF TO IF000-CDEVISE                        01421903
2002       ELSE                                                         01421903
2002          MOVE SOF-CDEVISE TO IF000-CDEVISE                         01421903
2002       END-IF.                                                      01422003
2002  *                                                                 01422003
2002   FIN-CONTROLE-DEVISE.              EXIT.                          01422003
      *                                                                 01423002
      *---------------------------------------                          01430000
       CONTROLE-REJETS                 SECTION.                         01440000
      *---------------------------------------                          01450000
      *                                                                 01451002
           MOVE SPACES              TO IF000-CODANO.                    01460000
           MOVE SPACES              TO IF000-TYPANO.                    01470000
                                                                        01480000
      *----     REJETS BLOQUANTS -----                                  01490000
           IF SOF-NVENDEUR (1:L) NOT > W-ZERO (1:L)                     01500000
      *    OR SOF-NVENDEUR (1:L) NOT NUMERIC                            01510000
              MOVE '01'                TO IF000-CODANO                  01520000
              MOVE 'R'                 TO IF000-TYPANO                  01530000
           END-IF.                                                      01540000
                                                                        01550000
           IF IF000-CODANO = SPACES                                     01560000
              IF IF000-MTBRUT = ZERO                                    01570000
              OR IF000-MTBRUT = ZERO                                    01580000
                 MOVE '08'             TO IF000-CODANO                  01590000
                 MOVE 'R'              TO IF000-TYPANO                  01600000
              END-IF                                                    01610000
           END-IF.                                                      01620000
                                                                        01630000
           IF IF000-CODANO = SPACES                                     01640000
              IF IF000-MTNET = ZERO                                     01650000
                 MOVE '09'             TO IF000-CODANO                  01660000
                 MOVE 'R'              TO IF000-TYPANO                  01670000
              END-IF                                                    01680000
           END-IF.                                                      01690000
                                                                        01700000
           IF IF000-CODANO = SPACES                                     01710000
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM              01720000
              IF IF000-MTNET NOT = W-MTNET                              01730000
                 MOVE '10'             TO IF000-CODANO                  01740000
                 MOVE 'R'              TO IF000-TYPANO                  01750000
              END-IF                                                    01760000
           END-IF.                                                      01770000
                                                                        01780000
      *----     REJETS NON BLOQUANTS -----                              01790000
           MOVE IF000-DFINANCE      TO GFSAMJ-0                         01800000
           MOVE '5'                 TO GFDATA                           01810000
           PERFORM APPEL-BETDATC                                        01820000
           IF GFVDAT NOT = '1'                                          01830000
              MOVE L-DATE           TO IF000-DFINANCE                   01840000
              IF IF000-CODANO = SPACES                                  01850000
                 MOVE '03'             TO IF000-CODANO                  01860000
                 MOVE 'C'              TO IF000-TYPANO                  01870000
              END-IF                                                    01880000
           END-IF.                                                      01890000
      *                                                                         
AD02       MOVE IF000-DOPER         TO GFSAMJ-0                         01800000
           MOVE '5'                 TO GFDATA                           01810000
           PERFORM APPEL-BETDATC                                        01820000
           IF GFVDAT NOT = '1'                                          01830000
              MOVE L-DATE           TO IF000-DOPER                      01840000
              IF IF000-CODANO = SPACES                                  01850000
                 MOVE '03'             TO IF000-CODANO                  01860000
                 MOVE 'C'              TO IF000-TYPANO                  01870000
              END-IF                                                    01880000
AD02       END-IF.                                                      01890000
                                                                        01900000
AD02  *    IF IF000-NOMCLIENT NOT > SPACES                              01910000
      *       IF IF000-CODANO = SPACES                                  01920000
      *          MOVE '11'             TO IF000-CODANO                  01930000
      *          MOVE 'C'              TO IF000-TYPANO                  01940000
      *       END-IF                                                    01950000
AD02  *    END-IF.                                                      01960000
                                                                        01970000
           IF IF000-REFINTERNE (4:6) = '000000'                         01980000
              MOVE '999998'         TO IF000-REFINTERNE (4:6)           01990000
              IF IF000-CODANO = SPACES                                  02000000
                 MOVE '12'             TO IF000-CODANO                  02010000
                 MOVE 'C'              TO IF000-TYPANO                  02020000
              END-IF                                                    02030000
           END-IF.                                                      02040000
                                                                        02050000
           PERFORM VARYING I FROM 9 BY -1                               02060000
                   UNTIL   I < 4                                        02070000
                   OR      IF000-REFINTERNE (4:6) = '999998'            02080000
              IF IF000-REFINTERNE (I:1) < '0'                           02090000
                 MOVE '999998'      TO IF000-REFINTERNE (4:6)           02100000
                 IF IF000-CODANO = SPACES                               02110000
                    MOVE '12'             TO IF000-CODANO               02120000
                    MOVE 'C'              TO IF000-TYPANO               02130000
                 END-IF                                                 02140000
              END-IF                                                    02150000
           END-PERFORM.                                                 02160000
      *                                                                 02160103
2002       IF IF000-CDEVISE = W-DEVISE-FRF OR                           02161003
2002          IF000-CDEVISE = W-DEVISE-EUR THEN                         02161003
2002          NEXT SENTENCE                                                     
2002        ELSE                                                                
2002          IF IF000-CODANO = SPACES                                  02162003
2002             MOVE '33'             TO IF000-CODANO                  02163003
2002             MOVE 'C'              TO IF000-TYPANO                  02164003
2002          END-IF                                                    02165003
2002       END-IF.                                                      02166003
      *                                                                 02167003
      *---------------------------------------                          02170000
       LECTURE-FSOFLOA                 SECTION.                         02180000
      *---------------------------------------                          02190000
           READ FSOFLOA INTO DSECT-SOFLOA.                              02200000
                                                                        02210000
           MOVE ST-LOA              TO L-FSTATUS.                       02220000
                                                                        02230000
           IF ST-LOA = 00                                               02240000
              IF SOF-TETE                                               02250000
                 MOVE DSECT-SOFLOA     TO TETE-SOFLOA                   02260000
                 MOVE SOF-DFICORG      TO L-DFICORG                     02270000
                 MOVE ZERO             TO L-NBRECUS                     02280000
                 MOVE ZERO             TO L-NBREJETS                    02290000
              END-IF                                                    02300000
                                                                        02310000
              IF SOF-FIN                                                02320000
                 MOVE DSECT-SOFLOA     TO FIN-SOFLOA                    02330000
                 MOVE SOF-NFICORG      TO L-NFICORG                     02340000
              END-IF                                                    02350000
           END-IF.                                                      02360000
      *---------------------------------------                          02370000
       APPEL-BETDATC                   SECTION.                         02380000
      *---------------------------------------                          02390000
           CALL W-BETDATC USING WORK-BETDATC.                           02400000
