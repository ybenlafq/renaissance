      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BRA900.                                                     
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO FDATE.                                      
      *--                                                                       
            SELECT FDATE  ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN ASSIGN TO FSYSIN.                                     
      *--                                                                       
            SELECT FSYSIN ASSIGN TO FSYSIN                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD              
           DATA RECORD FDATE-DSECT.                                             
       01  FDATE-DSECT     PIC X(80).                                           
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD             
           DATA RECORD FSYSIN-DSECT.                                            
       01  FSYSIN-DSECT    PIC X(80).                                           
       WORKING-STORAGE SECTION.                                                 
       01  DSECT-FDATE.                                                         
           05 FDATE-JJMMSSAA PIC XXXXXXXX.                                      
           05 FILLER         PIC X(72).                                         
       01  SYSIN-DATA        PIC X(80).                                         
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * DATES CALCULEES                                                         
      ***************************************************************           
       01  W-FDATE  PIC X(8).                                                   
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE FDATE JJMMSSAA                                                
      * - TRANSFORMATION AU FORMAT SSAAMMJJ                                     
      * - GENERATION DE SYSIN                                                   
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           OPEN INPUT FDATE                                                     
           OUTPUT FSYSIN.                                                       
      *                                                                         
           READ FDATE INTO DSECT-FDATE AT END                           01090024
                MOVE 'FICHIER FMOIS VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           DISPLAY 'FDATE TRAITE      : ' FDATE-JJMMSSAA.                       
           MOVE '1'             TO GFDATA.                                      
           MOVE FDATE-JJMMSSAA  TO GFJJMMSSAA                                   
           CALL BETDATC      USING WORK-BETDATC                                 
           IF GFVDAT NOT = '1'                                                  
              STRING 'BETDATC : ' GF-MESS-ERR                                   
              DELIMITED BY SIZE INTO ABEND-MESS                                 
              PERFORM ABEND-PROGRAMME                                           
           ELSE                                                                 
              MOVE GFSAMJ-0           TO W-FDATE                                
              DISPLAY 'DATE CALCULEE : ' W-FDATE                                
           END-IF.                                                              
           CLOSE FDATE.                                                 01130024
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY '*** GENERATION SYSIN ***'                                   
      *                                                                         
           MOVE 'FASTUNLOAD'   TO SYSIN-DATA                                    
              PERFORM WRITE-FSYSIN                                              
           MOVE '   EXCP YES' TO SYSIN-DATA                                     
              PERFORM WRITE-FSYSIN                                              
           MOVE '   SQL-ACCESS EXTENSION '  TO SYSIN-DATA                       
              PERFORM WRITE-FSYSIN                                              
           MOVE '   OUTPUT-FORMAT C' TO SYSIN-DATA                              
              PERFORM WRITE-FSYSIN                                              
           MOVE '   QUOTE NONE' TO SYSIN-DATA                                   
              PERFORM WRITE-FSYSIN                                              
           MOVE '   ROW-DELIMITER '';'' ' TO SYSIN-DATA                         
              PERFORM WRITE-FSYSIN                                              
           MOVE '   VALIDATE-HEADER NO ' TO SYSIN-DATA                          
              PERFORM WRITE-FSYSIN                                              
           MOVE 'SELECT NSOCIETE!!'';'' ' TO SYSIN-DATA(1:21)                   
           MOVE '!!NLIEU!!'';'' ' TO SYSIN-DATA(21:13)                          
              PERFORM WRITE-FSYSIN                                              
           MOVE '   !!LNOM!!'';''!!LPRENOM!!'';''!! ' TO SYSIN-DATA             
              PERFORM WRITE-FSYSIN                                              
           MOVE '   CHAR(MTREMBTOT)!!'';''!!NDEMANDE!!' TO SYSIN-DATA           
              PERFORM WRITE-FSYSIN                                              
           MOVE '   '';''!!NAVOIRCLIENT!!'';''!! ' TO SYSIN-DATA                
              PERFORM WRITE-FSYSIN                                              
           MOVE '   SUBSTR(DCREATION, 7, 2)!!''/'' ' TO SYSIN-DATA(1:32)        
           MOVE '!!SUBSTR(DCREATION, 5, 2)!!''/'' ' TO SYSIN-DATA(32:31)        
              PERFORM WRITE-FSYSIN                                              
           MOVE '   !!SUBSTR(DCREATION, 3, 2)'   TO SYSIN-DATA                  
              PERFORM WRITE-FSYSIN                                              
           MOVE 'FROM RTRA00'  TO SYSIN-DATA                                    
              PERFORM WRITE-FSYSIN                                              
           MOVE 'WHERE CTYPDEMANDE = ''AVOIR'' '  TO SYSIN-DATA                 
              PERFORM WRITE-FSYSIN                                              
           STRING '  AND DCREATION = ''' W-FDATE ''' ; '                        
               DELIMITED SIZE INTO SYSIN-DATA                                   
              PERFORM WRITE-FSYSIN.                                             
      *                                                                         
           CLOSE FSYSIN.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
       WRITE-FSYSIN           SECTION.                                          
           WRITE FSYSIN-DSECT FROM SYSIN-DATA.                                  
       FIN-WRITE-FSYSIN.   EXIT.                                                
      ******************************************************************        
       ABEND-PROGRAMME        SECTION.                                          
           MOVE  'BRA900'     TO ABEND-PROG.                                    
           CALL  'ABEND'   USING ABEND-PROG  ABEND-MESS.                        
