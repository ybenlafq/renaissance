      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00C.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      * MODIF 20/02/2001 -  DSA047                                     *        
      * AJOUT DU CONTROLE DE LA DEVISE :                               *        
      *  - SI LA DEVISE EST 'FRF' OU 'EUR'      -> ON NE FAIT RIEN     *        
      *  - SI LA DEVISE N'EST PAS RENSEIGNEE    -> ON LA FORCE A 'FRF' *        
      *  - SI LA DEVISE N'EST NI 'FRF' NI 'EUR' -> ON CREE 1 ANOMALIE  *        
      *                                            NON BLOQUANTE       *        
      * LA MODIFICATION EST REFERENCEE PAR 2002 DANS LA COL A          *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCETELEM  ASSIGN TO FCETELEM                                  
      *                     FILE STATUS IS ST-CET.                              
      *--                                                                       
           SELECT FCETELEM  ASSIGN TO FCETELEM                                  
                            FILE STATUS IS ST-CET                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FCETELEM                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FCETELEM-RECORD          PIC X(100).                                 
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF00C.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                  PIC S9(4)     COMP   VALUE ZERO.              
      *--                                                                       
           05  I                  PIC S9(4) COMP-5   VALUE ZERO.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                  PIC S9(4)     COMP   VALUE ZERO.              
      *--                                                                       
           05  L                  PIC S9(4) COMP-5   VALUE ZERO.                
      *}                                                                        
2002       05  W-DEVISE-FRF      PIC X(3)              VALUE 'FRF'.             
2002       05  W-DEVISE-EUR      PIC X(3)              VALUE 'EUR'.             
           05  W-MTNET            PIC S9(13)V99 COMP-3 VALUE ZERO.              
           05  W-NFICORG          PIC S9(7)     COMP-3 VALUE ZERO.              
           05  ST-CET             PIC 99               VALUE ZERO.              
           05  W-ZERO             PIC X(20)            VALUE ALL '0'.           
           05  W-DFICORG                               VALUE SPACES.            
               10  W-DFICORG-N    PIC 9(8).                                     
           05  W-BETDATC          PIC X(8)             VALUE 'BETDATC'.         
           05  W-DTMVT.                                                         
               10  W-DTMVTSSAA    PIC 9(4).                                     
               10  FILLER         REDEFINES W-DTMVTSSAA.                        
                   15  FILLER     PIC XXX.                                      
                   15  W-DTMVTAU  PIC 9.                                        
               10  W-DTMVTMM      PIC 99.                                       
               10  W-DTMVTJJ      PIC 99.                                       
       01  DSECT-CETELEM.                                                       
           05  CET-CODENR         PIC XX.                                       
           05  FILLER             PIC X(3).                                     
           05  CET-DFINANCE       PIC X(6).                                     
           05  FILLER    REDEFINES CET-DFINANCE.                                
               10  FILLER             PIC XX.                                   
               10  CET-ANNEE          PIC 9.                                    
               10  CET-MOIS.                                                    
                   15  CET-MOISN           PIC 9.                               
               10  CET-JOUR           PIC 99.                                   
           05  CET-ECRITURE       PIC X(5).                                     
           05  CET-REFERENCE      PIC X(6).                                     
           05  CET-DACHAT         PIC X(6).                                     
           05  CET-COMPTE         PIC X(5).                                     
           05  CET-BDC            PIC X(9).                                     
           05  CET-MTNET          PIC S9(9)V99.                                 
2002       05  CET-CDEVISE        PIC XXX.                                      
           05  CET-NOMCLIENT      PIC X(4).                                     
           05  CET-NCHEQUE        PIC X(7).                                     
           05  CET-NDOSSIER       PIC X(8).                                     
           05  CET-NIDENT         PIC X(10).                                    
           05  CET-CTYPCREDIT     PIC X.                                        
           05  CET-MTCOM          PIC S9(5)V99.                                 
           05  CET-DFICORG.                                                     
               10  CET-DFICORG-N      PIC S9(9)     COMP-3.                     
           05  FILLER             PIC XX.                                       
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000             PIC X(192).                                     
       01  L-DATE.                                                              
           05  L-DATESSAA       PIC 9(4).                                       
           05  L-DATEMM         PIC 99.                                         
           05  L-DATEJJ         PIC 99.                                         
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00C                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L.                                       
           MOVE L-FIF000            TO DSECT-IF000.                             
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FCETELEM                                               
              MOVE ST-CET           TO L-FSTATUS                                
           END-IF.                                                              
           IF ST-CET = 00                                                       
              PERFORM LECTURE-FCETELEM                                          
           END-IF.                                                              
           IF ST-CET = 00                                                       
              PERFORM TRAITEMENT-CETELEM                                        
           END-IF.                                                              
           IF ST-CET = 10                                                       
              CLOSE FCETELEM                                                    
              MOVE W-NFICORG     TO L-NFICORG                                   
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       TRAITEMENT-CETELEM              SECTION.                                 
      *---------------------------------------                                  
           IF CET-DFICORG NOT = SPACES                                          
              MOVE CET-DFICORG-N    TO W-DFICORG-N                              
              IF W-DFICORG NOT = L-DFICORG                                      
                 MOVE W-DFICORG        TO L-DFICORG                             
                 MOVE W-NFICORG        TO L-NFICORG                             
                 MOVE ZERO             TO L-NBRECUS                             
                 MOVE ZERO             TO L-NBREJETS                            
                 MOVE ZERO             TO W-NFICORG                             
              END-IF                                                            
           END-IF.                                                              
           PERFORM TRAITEMENT-FICHIER-CET.                                      
           ADD  1                   TO L-NBRECUS.                               
           ADD  1                   TO W-NFICORG.                               
           MOVE DSECT-IF000         TO L-FIF000.                                
      *---------------------------------------                                  
       TRAITEMENT-FICHIER-CET          SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-NIDENT.                            
           MOVE CET-NIDENT (1:L)    TO IF000-NIDENT.                            
           MOVE CET-NOMCLIENT       TO IF000-NOMCLIENT.                         
           MOVE CET-BDC             TO IF000-REFINTERNE.                        
           MOVE CET-CTYPCREDIT      TO IF000-CTYPMVT.                           
           MOVE CET-NDOSSIER        TO IF000-NDOSSIER.                          
           MOVE 1                   TO IF000-NFACTURES.                         
           COMPUTE IF000-MTBRUT = CET-MTNET + CET-MTCOM.                        
           COMPUTE IF000-MTCOM  = CET-MTCOM.                                    
           COMPUTE IF000-MTNET  = CET-MTNET.                                    
      *                                                                         
2002       PERFORM CONTROLE-DEVISE.                                             
           PERFORM CONTROLE-REJETS.                                             
      *                                                                         
      *---------------------------------------                                  
2002   CONTROLE-DEVISE                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
2002       IF CET-CDEVISE = SPACES THEN                                         
2002          MOVE W-DEVISE-FRF TO IF000-CDEVISE                                
2002       ELSE                                                                 
2002          MOVE CET-CDEVISE TO IF000-CDEVISE                                 
2002       END-IF.                                                              
2002  *                                                                         
2002   FIN-CONTROLE-DEVISE.              EXIT.                                  
      *                                                                         
      *---------------------------------------                                  
       CONTROLE-REJETS                 SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-CODANO.                            
           MOVE SPACES              TO IF000-TYPANO.                            
           IF CET-NIDENT (1:L) NOT > W-ZERO (1:L)                               
           OR CET-NIDENT (1:L) NOT NUMERIC                                      
              MOVE '01'             TO IF000-CODANO                             
              MOVE 'R'              TO IF000-TYPANO                             
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTBRUT = ZERO                                            
                 MOVE '08'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTNET = ZERO                                             
                 MOVE '09'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM                      
              IF IF000-MTNET NOT = W-MTNET                                      
                 MOVE '10'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF CET-DFICORG NOT = SPACES                                          
              MOVE CET-DFINANCE        TO GFAMJ-1                               
              MOVE '7'                 TO GFDATA                                
           ELSE                                                                 
              EVALUATE CET-MOIS                                                 
                       WHEN 'O'     MOVE 10        TO W-DTMVTMM                 
                       WHEN 'N'     MOVE 11        TO W-DTMVTMM                 
                       WHEN 'D'     MOVE 12        TO W-DTMVTMM                 
                       WHEN OTHER   MOVE CET-MOISN TO W-DTMVTMM                 
              END-EVALUATE                                                      
              MOVE CET-JOUR            TO W-DTMVTJJ                             
              MOVE L-DATESSAA          TO W-DTMVTSSAA                           
              MOVE CET-ANNEE           TO W-DTMVTAU                             
              IF W-DTMVT > L-DATE                                               
                 PERFORM UNTIL W-DTMVT NOT > L-DATE                             
                    COMPUTE W-DTMVTSSAA = W-DTMVTSSAA - 10                      
                 END-PERFORM                                                    
              END-IF                                                            
              MOVE W-DTMVT             TO GFSAMJ-0                              
              MOVE '5'                 TO GFDATA                                
           END-IF.                                                              
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT NOT = '1'                                                  
              MOVE L-DATE           TO IF000-DFINANCE                           
              IF IF000-CODANO = SPACES                                          
                 MOVE '03'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           ELSE                                                                 
              MOVE GFSAMJ-0            TO IF000-DFINANCE                        
           END-IF.                                                              
           IF CET-DACHAT > '000000'                                             
              MOVE CET-DACHAT          TO GFAMJ-1                               
              MOVE '7'                 TO GFDATA                                
              PERFORM APPEL-BETDATC                                             
              IF GFVDAT = '1'                                                   
                 MOVE GFSAMJ-0            TO IF000-DOPER                        
                 IF IF000-DFINANCE < IF000-DOPER                                
                    IF IF000-CODANO = SPACES                                    
                       MOVE '04'             TO IF000-CODANO                    
                       MOVE 'C'              TO IF000-TYPANO                    
                    END-IF                                                      
                 END-IF                                                         
              ELSE                                                              
                 MOVE L-DATE              TO IF000-DOPER                        
                 IF IF000-CODANO = SPACES                                       
                    MOVE '02'             TO IF000-CODANO                       
                    MOVE 'C'              TO IF000-TYPANO                       
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE L-DATE              TO IF000-DOPER                           
           END-IF.                                                              
           IF IF000-NOMCLIENT NOT > SPACES                                      
              IF IF000-CODANO = SPACES                                          
                 MOVE '11'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-REFINTERNE NOT > SPACES                                     
              MOVE '999999999'      TO IF000-REFINTERNE                         
              IF IF000-CODANO = SPACES                                          
                 MOVE '12'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-NDOSSIER NOT > SPACES                                       
              MOVE '999999'         TO IF000-NDOSSIER                           
              IF IF000-CODANO = SPACES                                          
                 MOVE '13'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
2002       IF IF000-CDEVISE = W-DEVISE-FRF OR                                   
2002          IF000-CDEVISE = W-DEVISE-EUR THEN                                 
2002          NEXT SENTENCE                                                     
2002        ELSE                                                                
2002          IF IF000-CODANO = SPACES                                          
2002             MOVE '33'             TO IF000-CODANO                          
2002             MOVE 'C'              TO IF000-TYPANO                          
2002          END-IF                                                            
2002       END-IF.                                                              
      *---------------------------------------                                  
       LECTURE-FCETELEM                SECTION.                                 
      *---------------------------------------                                  
           READ FCETELEM INTO DSECT-CETELEM.                                    
           MOVE ST-CET              TO L-FSTATUS.                               
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
