      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  MIF000.                                                     
       AUTHOR.                     M-DUPREY.                                    
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
       01  WS-MIF0.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  I                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LGPHYS            PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  LGPHYS            PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *    05  I-MAX             PIC S9(4)     VALUE ZERO  COMP.                
           05  TABLE-CHAMPS.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  T-MAX             PIC S9(4)       COMP.                      
      *--                                                                       
               10  T-MAX             PIC S9(4) COMP-5.                          
      *}                                                                        
               10  T-CHAMPS      OCCURS 50.                                     
                   15  NOMCHAMP      PIC X(10).                                 
                   15  TYPCHAMP      PIC X.                                     
                   15  LGCHAMP       PIC S9(3)       COMP-3.                    
                   15  DECCHAMP      PIC S9(3)       COMP-3.                    
      ***************************************************************           
       LINKAGE SECTION.                                                         
       01  Z-COMMAREA-MIF0.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MIF0-CDRET      PIC S9(4)     COMP.                         
      *--                                                                       
           05  COMM-MIF0-CDRET      PIC S9(4) COMP-5.                           
      *}                                                                        
           05  COMM-MIF0-MESS       PIC X(64).                                  
           05  COMM-MIF0-NOMCHAMP   PIC X(10).                                  
           05  COMM-MIF0-TYPCHAMP   PIC X.                                      
           05  COMM-MIF0-POSCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-LGCHAMP    PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-DECCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-CHAMPS     PIC X(752).                                 
      ***************************************************************           
       PROCEDURE DIVISION USING Z-COMMAREA-MIF0.                                
      *                                                                         
           PERFORM INITIALISATION-MIF000                                        
           IF COMM-MIF0-CDRET = ZERO                                            
              PERFORM TRAIT-MIF000                                              
           END-IF.                                                              
           PERFORM FIN-PROGRAMME.                                               
      *---------------------------------------                                  
       TRAIT-MIF000                    SECTION.                                 
      *---------------------------------------                                  
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > T-MAX                                            
                   OR      NOMCHAMP (I)  = COMM-MIF0-NOMCHAMP                   
              MOVE LGCHAMP (I)   TO LGPHYS                                      
              IF TYPCHAMP (I) = 'P'                                             
                 COMPUTE LGPHYS = (LGPHYS + 1) / 2                              
              END-IF                                                            
              ADD  LGPHYS           TO COMM-MIF0-POSCHAMP                       
           END-PERFORM.                                                         
           IF I > T-MAX                                                         
              MOVE +100             TO COMM-MIF0-CDRET                          
              STRING 'CHAMP '                                                   
                     COMM-MIF0-NOMCHAMP                                         
                     ' INCONNU'                                                 
                     DELIMITED BY SIZE INTO COMM-MIF0-MESS                      
           ELSE                                                                 
              MOVE TYPCHAMP (I)     TO COMM-MIF0-TYPCHAMP                       
              MOVE LGCHAMP (I)      TO COMM-MIF0-LGCHAMP                        
              MOVE DECCHAMP (I)     TO COMM-MIF0-DECCHAMP                       
           END-IF.                                                              
      *---------------------------------------                                  
       INITIALISATION-MIF000           SECTION.                                 
      *---------------------------------------                                  
           MOVE ZERO                   TO COMM-MIF0-CDRET.                      
           MOVE SPACES                 TO COMM-MIF0-MESS.                       
           MOVE +1                     TO COMM-MIF0-POSCHAMP.                   
           MOVE COMM-MIF0-CHAMPS       TO TABLE-CHAMPS.                         
      *    PERFORM VARYING I-MAX FROM 1 BY 1                                    
      *            UNTIL   I-MAX > 50                                           
      *            OR      NOMCHAMP (I-MAX) = HIGH-VALUE                        
      *    END-PERFORM.                                                         
      *    IF I-MAX > 50                                                        
      *       MOVE +50              TO COMM-MIF0-CDRET                          
      *       MOVE 'IL Y A PLUS DE 50 CHAMPS DECRITS'                           
      *                             TO COMM-MIF0-MESS                           
      *    END-IF.                                                              
      *---------------------------------------                                  
       FIN-PROGRAMME                   SECTION.                                 
      *---------------------------------------                                  
           GOBACK.                                                              
