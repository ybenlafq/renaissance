      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  MIF010.                                                     
       AUTHOR.                     M-DUPREY.                                    
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
       01  WS-MIF010.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  I                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  J                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  J                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  K                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  K                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  L                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  P                 PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  P                 PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IP                PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  IP                PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IL                PIC S9(4)     VALUE ZERO  COMP.                
      *--                                                                       
           05  IL                PIC S9(4)     VALUE ZERO COMP-5.               
      *}                                                                        
           05  W-DEBA.                                                          
               10  W-DEB         PIC 999       VALUE ZERO.                      
           05  W-FINA.                                                          
               10  W-FIN         PIC 999       VALUE ZERO.                      
      ***************************************************************           
       LINKAGE SECTION.                                                         
       01  Z-COMMAREA-MIF0.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MIF0-CDRET      PIC S9(4)     COMP.                         
      *--                                                                       
           05  COMM-MIF0-CDRET      PIC S9(4) COMP-5.                           
      *}                                                                        
           05  COMM-MIF0-MESS       PIC X(64).                                  
           05  COMM-MIF0-NOMCHAMP   PIC X(10).                                  
           05  COMM-MIF0-TYPCHAMP   PIC X.                                      
           05  COMM-MIF0-POSCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-LGCHAMP    PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-DECCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-CHAMPS     PIC X(752).                                 
       01  W-INPUT               PIC X(4096).                                   
       01  W-MASQUE              PIC X(5).                                      
       01  W-OUTPUT              PIC X(4096).                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  PZ                    PIC S9(4) COMP.                                
      *--                                                                       
       01  PZ                    PIC S9(4) COMP-5.                              
      *}                                                                        
      ***************************************************************           
       PROCEDURE DIVISION USING Z-COMMAREA-MIF0                                 
                                W-INPUT                                         
                                W-MASQUE                                        
                                W-OUTPUT                                        
                                PZ.                                             
      *                                                                         
           MOVE COMM-MIF0-POSCHAMP     TO IP.                                   
           MOVE COMM-MIF0-LGCHAMP      TO IL.                                   
           IF COMM-MIF0-TYPCHAMP = 'P'                                          
              COMPUTE IL = (COMM-MIF0-LGCHAMP + 1) / 2                          
           END-IF.                                                              
           IF W-MASQUE > SPACES                                                 
              PERFORM ALIMENTER-MASQUE                                          
           ELSE                                                                 
              MOVE W-INPUT  (IP:IL) TO W-OUTPUT (PZ:IL)                         
              ADD  IL               TO PZ                                       
           END-IF.                                                              
      *---------------------------------------                                  
       ALIMENTER-MASQUE                SECTION.                                 
      *---------------------------------------                                  
           IF W-MASQUE (1:1) = 'J'                                              
           OR W-MASQUE (1:1) = 'M'                                              
           OR W-MASQUE (1:1) = 'S'                                              
           OR W-MASQUE (1:1) = 'A'                                              
              PERFORM ALIMENTER-MASQUE-DATE                                     
           ELSE                                                                 
              PERFORM ALIMENTER-MASQUE-ALPHA                                    
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       ALIMENTER-MASQUE-DATE           SECTION.                                 
      *---------------------------------------                                  
           PERFORM VARYING I FROM 5 BY -1                                       
                   UNTIL   I < 1                                                
                   OR      W-MASQUE (I:1) > SPACES                              
           END-PERFORM.                                                         
           IF COMM-MIF0-TYPCHAMP = 'S'                                          
              MOVE 2                TO L                                        
           ELSE                                                                 
              MOVE ZERO             TO L                                        
           END-IF.                                                              
           PERFORM VARYING J FROM 1 BY 1                                        
                   UNTIL   J > I                                                
              IF W-MASQUE (J:1) = 'J'                                           
                 COMPUTE P = IP + L + 4                                         
                 MOVE W-INPUT  (P:2) TO W-OUTPUT (PZ:2)                         
                 ADD  2              TO PZ                                      
              END-IF                                                            
              IF W-MASQUE (J:1) = 'M'                                           
                 COMPUTE P = IP + L + 2                                         
                 MOVE W-INPUT  (P:2) TO W-OUTPUT (PZ:2)                         
                 ADD  2              TO PZ                                      
              END-IF                                                            
              IF W-MASQUE (J:1) = 'A'                                           
                 COMPUTE P = IP + L                                             
                 MOVE W-INPUT  (P:2) TO W-OUTPUT (PZ:2)                         
                 ADD  2              TO PZ                                      
              END-IF                                                            
              IF W-MASQUE (J:2) = 'SA'                                          
                 COMPUTE P = IP                                                 
                 MOVE W-INPUT  (P:4) TO W-OUTPUT (PZ:4)                         
                 ADD  4              TO PZ                                      
                 ADD  1              TO J                                       
              END-IF                                                            
           END-PERFORM.                                                         
      *---------------------------------------                                  
       ALIMENTER-MASQUE-ALPHA          SECTION.                                 
      *---------------------------------------                                  
           MOVE ZERO                TO W-DEB.                                   
           MOVE ZERO                TO W-FIN.                                   
           PERFORM VARYING J FROM 1 BY 1                                        
                   UNTIL   J > 5                                                
                   OR W-MASQUE (J:1) = ','                                      
                   OR W-MASQUE (J:1) = ' '                                      
           END-PERFORM.                                                         
           COMPUTE L = (J - 1).                                                 
           COMPUTE P = 3 - L + 1.                                               
           MOVE W-MASQUE (1:L)           TO W-DEBA (P:L).                       
           IF W-MASQUE (J:1) = ','                                              
              COMPUTE J = J + 1                                                 
              MOVE J                TO I                                        
              PERFORM VARYING J FROM J BY 1                                     
                      UNTIL   J > 5                                             
                      OR W-MASQUE (J:1) = ' '                                   
              END-PERFORM                                                       
              COMPUTE L = (J - 1) - (I - 1)                                     
              COMPUTE P = 3 - L + 1                                             
              MOVE W-MASQUE (I:L)               TO W-FINA (P:L)                 
           END-IF.                                                              
           IF W-FIN = ZERO                                                      
              IF W-DEB > ZERO                                                   
                 COMPUTE W-FIN = IL - (W-DEB - 1)                               
              END-IF                                                            
           END-IF.                                                              
           IF W-DEB > ZERO                                                      
              COMPUTE IP = IP + (W-DEB - 1)                                     
              MOVE W-FIN            TO IL                                       
              MOVE W-INPUT  (IP:IL) TO W-OUTPUT (PZ:IL)                         
              ADD  IL               TO PZ                                       
           END-IF.                                                              
