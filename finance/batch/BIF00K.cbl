      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00K.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      * MODIF 20/02/2001 -  DSA047                                     *        
      * AJOUT DU CONTROLE DE LA DEVISE :                               *        
      *  - SI LA DEVISE EST 'FRF' OU 'EUR'      -> ON NE FAIT RIEN     *        
      *  - SI LA DEVISE N'EST PAS RENSEIGNEE    -> ON LA FORCE A 'FRF' *        
      *  - SI LA DEVISE N'EST NI 'FRF' NI 'EUR' -> ON CREE 1 ANOMALIE  *        
      *                                            NON BLOQUANTE       *        
      * LA MODIFICATION EST REFERENCEE PAR 2002 DANS LA COL A          *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FKYRIEL   ASSIGN TO FKYRIEL                                   
      *                     FILE STATUS IS ST-KYR.                              
      *--                                                                       
           SELECT FKYRIEL   ASSIGN TO FKYRIEL                                   
                            FILE STATUS IS ST-KYR                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FINTERM   ASSIGN TO FINTERM                                   
      *                     FILE STATUS IS ST-INT.                              
      *--                                                                       
           SELECT FINTERM   ASSIGN TO FINTERM                                   
                            FILE STATUS IS ST-INT                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FKYRIEL                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FKYRIEL-RECORD           PIC X(200).                                 
       FD  FINTERM                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FINTERM-RECORD           PIC X(89).                                  
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF00K.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  L                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
2002       05  W-DEVISE-FRF      PIC X(3)             VALUE 'FRF'.              
2002       05  W-DEVISE-EUR      PIC X(3)             VALUE 'EUR'.              
           05  W-SIGNE           PIC S9        COMP-3 VALUE +1.                 
           05  W-MTBRUT-CRE      PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  W-MTBRUT-CPT      PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  W-MTCOM-CRE       PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  W-MTCOM-CPT       PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  ST-KYR            PIC 99               VALUE ZERO.               
           05  ST-INT            PIC 99               VALUE ZERO.               
           05  W-ZERO            PIC X(20)            VALUE ALL '0'.            
           05  W-DFINANCE        PIC X(8)             VALUE SPACES.             
           05  W-NIDENT          PIC X(8)             VALUE SPACES.             
           05  W-BETDATC         PIC X(8)             VALUE 'BETDATC'.          
       01  DSECT-KYRIEL.                                                        
           05  KYR-CODENR         PIC XX.                                       
               88  KYR-TETE               VALUE '01'.                           
               88  KYR-FIN                VALUE '99'.                           
               88  KYR-DETAIL             VALUES '11' '12' '13' '14'            
                                                 '15' '16' '21' '22'            
                                                 '34' '36' '37' '38'            
                                                 '81' '82' '83' '84'            
                                                 '85' '86'                      
                                                 '90' '91' '92'                 
                                                 '94' '96' '97' '98'.           
               88  KYR-BRUTCRE            VALUES '11' '81'.                     
               88  KYR-BRUTCPT            VALUES '16' '86'.                     
               88  KYR-COMCRE             VALUES '38' '98'.                     
               88  KYR-COMCPT             VALUES '36' '96'.                     
               88  KYR-CREDIT             VALUES '11' '12' '13' '14'            
                                                 '15' '16' '81' '82'            
                                                 '83' '84' '85' '86'            
                                                 '90'.                          
               88  KYR-DEBIT              VALUES '21' '22' '34' '36'            
                                                 '37' '38' '91' '92'            
                                                 '94' '96' '97' '98'.           
           05  KYR-DATE           PIC X(8).                                     
           05  FILLER             PIC X(190).                                   
       01  DETAIL-KYRIEL REDEFINES DSECT-KYRIEL.                                
           05  FILLER             PIC XX.                                       
           05  KYR-DFINANCE       PIC X(8).                                     
           05  KYR-DOPER          PIC X(8).                                     
           05  KYR-REFCLI         PIC X(11).                                    
           05  KYR-NOMCLI         PIC X(24).                                    
           05  KYR-PRENOMCLI      PIC X(16).                                    
           05  KYR-REFINTERNE     PIC X(12).                                    
           05  KYR-NIDENT         PIC X(8).                                     
2002       05  KYR-CDEVISE        PIC X(03).                                    
           05  KYR-FILLER         PIC X(90).                                    
           05  KYR-MTCREDIT       PIC 9(7)V99.                                  
           05  KYR-MTDEBIT        PIC 9(7)V99.                                  
       01  TETE-KYRIEL   REDEFINES DSECT-KYRIEL.                                
           05  FILLER             PIC XX.                                       
           05  KYR-DFICORG        PIC X(8).                                     
           05  FILLER             PIC X(190).                                   
       01  FIN-KYRIEL    REDEFINES DSECT-KYRIEL.                                
           05  FILLER             PIC X(21).                                    
           05  KYR-NFICORG        PIC 9(9).                                     
           05  FILLER             PIC X(170).                                   
       01  DSECT-FINTERM.                                                       
           05  INT-CODENR         PIC XX.                                       
               88  INT-TETE           VALUE  '01'.                              
               88  INT-DETAIL         VALUES '11' '16' '36' '38'                
                                             '81' '86'.                         
               88  INT-FIN            VALUE  '99'.                              
           05  INT-CTYPMVT        PIC X.                                        
           05  INT-DFINANCE       PIC X(8).                                     
           05  INT-DFICORG REDEFINES INT-DFINANCE                               
                                  PIC X(8).                                     
           05  INT-DOPER          PIC X(8).                                     
           05  INT-REFCLI         PIC X(11).                                    
           05  INT-NOMCLI         PIC X(24).                                    
           05  INT-REFINTERNE     PIC X(12).                                    
           05  INT-NIDENT         PIC X(8).                                     
           05  INT-MTBRUT         PIC S9(7)V99 COMP-3.                          
           05  INT-NFICORG REDEFINES INT-MTBRUT                                 
                                  PIC S9(9)    COMP-3.                          
           05  INT-MTCOM          PIC S9(7)V99 COMP-3.                          
           05  INT-MTNET          PIC S9(7)V99 COMP-3.                          
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000          PIC X(192).                                        
      *                                                                         
       01  L-DATE            PIC X(8).                                          
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00K                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L.                                       
           MOVE L-FIF000            TO DSECT-IF000.                             
           IF L-FSTATUS = 99                                                    
              PERFORM DEBUT-BIF00K                                              
           END-IF.                                                              
           IF ST-INT = 00                                                       
              PERFORM LECTURE-FINTERM                                           
           END-IF.                                                              
           IF ST-INT = 00                                                       
              PERFORM TRAITEMENT-KYRIEL                                         
           END-IF.                                                              
           IF ST-INT = 10                                                       
              CLOSE FINTERM                                                     
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       DEBUT-BIF00K                    SECTION.                                 
      *---------------------------------------                                  
           OPEN  INPUT FKYRIEL                                                  
                OUTPUT FINTERM.                                                 
           INITIALIZE DSECT-FINTERM.                                            
           PERFORM LECTURE-FKYRIEL.                                             
           PERFORM UNTIL ST-KYR = 10                                            
              IF KYR-TETE                                                       
                 PERFORM ECRITURE-FINTERM                                       
                 MOVE SPACES           TO W-DFINANCE                            
                 MOVE SPACES           TO W-NIDENT                              
              END-IF                                                            
              IF KYR-FIN                                                        
                 PERFORM ECRITURE-COMMISSIONS                                   
                 PERFORM ECRITURE-FINTERM                                       
              END-IF                                                            
              IF KYR-DETAIL                                                     
                 IF KYR-DFINANCE NOT = W-DFINANCE                               
                    PERFORM ECRITURE-COMMISSIONS                                
                 ELSE                                                           
                    IF KYR-NIDENT NOT = W-NIDENT                                
                       PERFORM ECRITURE-COMMISSIONS                             
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              IF KYR-BRUTCRE                                                    
                 IF KYR-DEBIT                                                   
                    COMPUTE INT-MTBRUT = KYR-MTDEBIT - KYR-MTCREDIT             
                 ELSE                                                           
                    COMPUTE INT-MTBRUT = KYR-MTCREDIT - KYR-MTDEBIT             
                 END-IF                                                         
                 COMPUTE W-MTBRUT-CRE = W-MTBRUT-CRE + INT-MTBRUT               
                 MOVE '2'              TO INT-CTYPMVT                           
                 PERFORM ECRITURE-FINTERM                                       
              END-IF                                                            
              IF KYR-BRUTCPT                                                    
                 IF KYR-DEBIT                                                   
                    COMPUTE INT-MTBRUT = KYR-MTDEBIT - KYR-MTCREDIT             
                 ELSE                                                           
                    COMPUTE INT-MTBRUT = KYR-MTCREDIT - KYR-MTDEBIT             
                 END-IF                                                         
                 COMPUTE W-MTBRUT-CPT = W-MTBRUT-CPT + INT-MTBRUT               
                 MOVE '1'              TO INT-CTYPMVT                           
                 PERFORM ECRITURE-FINTERM                                       
              END-IF                                                            
              IF KYR-COMCRE                                                     
                 IF KYR-DEBIT                                                   
                    COMPUTE W-MTCOM-CRE = W-MTCOM-CRE                           
                                        + (KYR-MTDEBIT - KYR-MTCREDIT)          
                 ELSE                                                           
                    COMPUTE W-MTCOM-CRE = W-MTCOM-CRE                           
                                        + (KYR-MTCREDIT - KYR-MTDEBIT)          
                 END-IF                                                         
              END-IF                                                            
              IF KYR-COMCPT                                                     
                 IF KYR-DEBIT                                                   
                    COMPUTE W-MTCOM-CPT = W-MTCOM-CPT                           
                                        + (KYR-MTDEBIT - KYR-MTCREDIT)          
                 ELSE                                                           
                    COMPUTE W-MTCOM-CPT = W-MTCOM-CPT                           
                                        + (KYR-MTCREDIT - KYR-MTDEBIT)          
                 END-IF                                                         
              END-IF                                                            
              PERFORM LECTURE-FKYRIEL                                           
           END-PERFORM.                                                         
           CLOSE FKYRIEL                                                        
                 FINTERM.                                                       
           OPEN INPUT FINTERM.                                                  
           MOVE ST-INT              TO L-FSTATUS.                               
           IF ST-INT NOT = 00                                                   
              GOBACK                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       ECRITURE-FINTERM                SECTION.                                 
      *---------------------------------------                                  
           MOVE KYR-CODENR          TO INT-CODENR.                              
           MOVE KYR-DFINANCE        TO INT-DFINANCE.                            
           MOVE KYR-DOPER           TO INT-DOPER.                               
           MOVE KYR-REFCLI          TO INT-REFCLI.                              
           MOVE KYR-NOMCLI          TO INT-NOMCLI.                              
           MOVE KYR-REFINTERNE      TO INT-REFINTERNE.                          
           MOVE KYR-NIDENT          TO INT-NIDENT.                              
           IF KYR-FIN                                                           
              MOVE KYR-NFICORG      TO INT-NFICORG                              
           END-IF.                                                              
           PERFORM WRITE-FINTERM.                                               
      *---------------------------------------                                  
       ECRITURE-COMMISSIONS            SECTION.                                 
      *---------------------------------------                                  
           IF W-MTBRUT-CPT NOT = ZERO                                           
           OR W-MTCOM-CPT  NOT = ZERO                                           
              MOVE '36'                TO INT-CODENR                            
              MOVE '1'                 TO INT-CTYPMVT                           
              MOVE W-DFINANCE          TO INT-DFINANCE                          
              MOVE SPACES              TO INT-REFCLI                            
              MOVE SPACES              TO INT-NOMCLI                            
              MOVE SPACES              TO INT-REFINTERNE                        
              MOVE W-NIDENT            TO INT-NIDENT                            
              MOVE W-MTCOM-CPT         TO INT-MTCOM                             
              COMPUTE INT-MTNET = W-MTBRUT-CPT - W-MTCOM-CPT                    
              PERFORM WRITE-FINTERM                                             
           END-IF.                                                              
           IF W-MTBRUT-CRE NOT = ZERO                                           
           OR W-MTCOM-CRE  NOT = ZERO                                           
              MOVE '38'                TO INT-CODENR                            
              MOVE '2'                 TO INT-CTYPMVT                           
              MOVE W-DFINANCE          TO INT-DFINANCE                          
              MOVE SPACES              TO INT-REFCLI                            
              MOVE SPACES              TO INT-NOMCLI                            
              MOVE SPACES              TO INT-REFINTERNE                        
              MOVE W-NIDENT            TO INT-NIDENT                            
              MOVE W-MTCOM-CRE         TO INT-MTCOM                             
              COMPUTE INT-MTNET = W-MTBRUT-CRE + W-MTCOM-CRE                    
              PERFORM WRITE-FINTERM                                             
           END-IF.                                                              
           MOVE KYR-DFINANCE        TO W-DFINANCE.                              
           MOVE KYR-NIDENT          TO W-NIDENT.                                
           MOVE ZERO                TO W-MTBRUT-CPT.                            
           MOVE ZERO                TO W-MTCOM-CPT.                             
           MOVE ZERO                TO W-MTBRUT-CRE.                            
           MOVE ZERO                TO W-MTCOM-CRE.                             
      *---------------------------------------                                  
       WRITE-FINTERM                   SECTION.                                 
      *---------------------------------------                                  
           WRITE FINTERM-RECORD FROM DSECT-FINTERM.                             
           IF ST-INT NOT = 00                                                   
              MOVE ST-INT              TO L-FSTATUS                             
              GOBACK                                                            
           END-IF.                                                              
           MOVE ZERO                TO INT-MTBRUT                               
                                       INT-MTCOM                                
                                       INT-MTNET.                               
      *---------------------------------------                                  
       TRAITEMENT-KYRIEL               SECTION.                                 
      *---------------------------------------                                  
           PERFORM LECTURE-FINTERM                                              
                   UNTIL ST-INT = 10                                            
                   OR    INT-DETAIL.                                            
           IF ST-INT NOT = 10                                                   
              PERFORM TRAITEMENT-KYR                                            
              ADD  1                TO L-NBRECUS                                
              MOVE DSECT-IF000      TO L-FIF000                                 
           END-IF.                                                              
      *---------------------------------------                                  
       TRAITEMENT-KYR                  SECTION.                                 
      *---------------------------------------                                  
           MOVE INT-DFINANCE        TO IF000-DFINANCE.                          
           MOVE INT-DOPER           TO IF000-DOPER.                             
           MOVE INT-REFCLI          TO IF000-NPORTEUR.                          
           MOVE INT-CTYPMVT         TO IF000-CTYPMVT                            
           MOVE 1                   TO IF000-NFACTURES.                         
           MOVE INT-MTBRUT          TO IF000-MTBRUT.                            
           MOVE INT-MTCOM           TO IF000-MTCOM.                             
           MOVE INT-MTNET           TO IF000-MTNET.                             
           MOVE INT-NIDENT          TO IF000-NIDENT.                            
           MOVE INT-NOMCLI          TO IF000-NOMCLIENT.                         
      *                                                                         
2002       PERFORM CONTROLE-DEVISE.                                             
           PERFORM CONTROLE-REJETS.                                             
      *                                                                         
      *---------------------------------------                                  
2002   CONTROLE-DEVISE                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
2002       IF KYR-CDEVISE = SPACES THEN                                         
2002          MOVE W-DEVISE-FRF TO IF000-CDEVISE                                
2002       ELSE                                                                 
2002          MOVE KYR-CDEVISE TO IF000-CDEVISE                                 
2002       END-IF.                                                              
2002  *                                                                         
2002   FIN-CONTROLE-DEVISE.              EXIT.                                  
      *                                                                         
      *---------------------------------------                                  
       CONTROLE-REJETS                 SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-CODANO.                            
           MOVE SPACES              TO IF000-TYPANO.                            
           IF IF000-NIDENT (1:L) NOT > W-ZERO (1:L)                             
           OR IF000-NIDENT (1:L) NOT NUMERIC                                    
              MOVE '01'                TO IF000-CODANO                          
              MOVE 'R'                 TO IF000-TYPANO                          
           END-IF.                                                              
           MOVE IF000-DFINANCE      TO GFSAMJ-0.                                
           MOVE '5'                 TO GFDATA.                                  
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DFINANCE                        
           ELSE                                                                 
              MOVE L-DATE              TO IF000-DFINANCE                        
              IF IF000-CODANO = SPACES                                          
                 MOVE '03'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           MOVE IF000-DOPER         TO GFSAMJ-0.                                
           MOVE '5'                 TO GFDATA.                                  
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DOPER                           
           ELSE                                                                 
              MOVE L-DATE              TO IF000-DOPER                           
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-DFINANCE < IF000-DOPER                                   
                 MOVE '04'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
2002       IF IF000-CDEVISE = W-DEVISE-FRF OR                                   
2002          IF000-CDEVISE = W-DEVISE-EUR THEN                                 
2002          NEXT SENTENCE                                                     
2002        ELSE                                                                
2002          IF IF000-CODANO = SPACES                                          
2002             MOVE '33'             TO IF000-CODANO                          
2002             MOVE 'C'              TO IF000-TYPANO                          
2002          END-IF                                                            
2002       END-IF.                                                              
      *                                                                         
      *---------------------------------------                                  
       LECTURE-FKYRIEL                 SECTION.                                 
      *---------------------------------------                                  
           READ FKYRIEL INTO DSECT-KYRIEL.                                      
           IF ST-KYR NOT = 00 AND                                               
              ST-KYR NOT = 10                                                   
              MOVE ST-KYR              TO L-FSTATUS                             
              GOBACK                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       LECTURE-FINTERM                 SECTION.                                 
      *---------------------------------------                                  
           READ FINTERM INTO DSECT-FINTERM                                      
           MOVE ST-INT              TO L-FSTATUS.                               
           IF ST-INT = 00                                                       
              IF INT-TETE                                                       
                 MOVE INT-DFICORG      TO L-DFICORG                             
                 MOVE ZERO             TO L-NBRECUS                             
                 MOVE ZERO             TO L-NBREJETS                            
              END-IF                                                            
              IF INT-FIN                                                        
                 MOVE INT-NFICORG      TO L-NFICORG                             
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
