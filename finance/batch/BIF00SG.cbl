      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BIF00SG.                                          00000040
       AUTHOR.        DSA015-EC.                                        00000050
       DATE-WRITTEN.  13/05/2011.                                       00000060
      *----------------------------------------------------------------*00000080
      * EXTRACTION DES TRANSACTION DE FINANCEMENTS DARTY.COM (SOC GEN) *00000090
      * DANS LE FICHIER JRB ET MISE AU FORMAT "STANDARD" CB            *        
      *----------------------------------------------------------------*00000080
      * MODIF EC 26/10/2011 : CLE D'ENREG POUR CUMUL N� DE REMISE AU   *        
      *                       LIEU DE DATE DE REMISE                   *        
      *                       SIGNET 1011                              *        
      *----------------------------------------------------------------*00000080
      * MODIF EC 27/10/2011 : CREATION D'UN 2EME FICHIER POUR COFINOGA *        
      *                       SIGNET 1011B                             *        
      *----------------------------------------------------------------*00000080
      * MODIF EC 12/01/2012 : AJOUT DE LA DATE DE VENTE DANS LA CLE DE *        
      *                       CUMUL                                    *        
      *                       SIGNET 0112                                       
      * MODIF MA 02/07/2015 : PRISE EN COMPTE TYPE DE CARTE MASTERCARD *        
      *                       SIGNET MA0207                            *        
      * MODIF MA 07/07/2015 : PRISE EN COMPTE TYPE DE CARTE MAESTRO    *        
      *                       SIGNET MA0707                            *        
      * MODIF MA 16/07/2015 : CONTROLE DES MONTANTS BRUTS, NET ET COMM *        
      *                       CALCUL MT NET OU MT COMMISSION           *        
      *                       PARAGRAPHE CALCUL-MONTANTS. SIGNET MA1607*        
      * RAJOUT MODIFS ARKEA   CONTRATS N� 0376916 0377226              *        
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN TO FDATE.                           00000250
      *--                                                                       
           SELECT FDATE      ASSIGN TO FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIFJRBI    ASSIGN TO FIFJRBI                          00000250
      *                      FILE STATUS ST-FIFJRBI.                            
      *--                                                                       
           SELECT FIFJRBI    ASSIGN TO FIFJRBI                                  
                             FILE STATUS ST-FIFJRBI                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIFJRBO    ASSIGN TO FIFJRBO                          00000250
      *                      FILE STATUS ST-FIFJRBO.                            
      *--                                                                       
           SELECT FIFJRBO    ASSIGN TO FIFJRBO                                  
                             FILE STATUS ST-FIFJRBO                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
1011B *    SELECT FIFCOF     ASSIGN TO FIFCOF                           00000250
1011B *                      FILE STATUS ST-FIFCOF.                             
      *--                                                                       
           SELECT FIFCOF     ASSIGN TO FIFCOF                                   
                             FILE STATUS ST-FIFCOF                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *                                                                 00000600
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FDATE.                                                          
            03  FILLER      PIC X(80).                                          
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FIFJRBI                                                      00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FIFJRBI-ENR.                                                 00000480
           10  FIFJRBI-ENREG          PICTURE X(500).                   00000480
      *                                                                 00000480
       FD   FIFJRBO                                                             
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   FIFJRBO-ENR.                                                        
      *** DESCRIPTION DU FICHIER FORMAT "STANDARD" CB                           
            03   FILLER                    PIC X(80).                           
1011B  FD   FIFCOF                                                              
"           RECORDING F                                                         
"           BLOCK 0  RECORDS                                                    
"           LABEL RECORD STANDARD.                                              
"      01   FIFCOF-ENR.                                                         
1011B       03   FILLER                    PIC X(150).                          
      *                                                                 00000430
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       77 ST-FIFJRBO                 PIC 9(02) VALUE ZERO.                      
1011B  77 ST-FIFCOF                  PIC 9(02) VALUE ZERO.                      
       77 ST-FIFJRBI                 PIC 9(02) VALUE ZERO.                      
       01 WW-FIFJRBI                 PIC X(500).                                
      *   COPIES D'ABEND ET DE CONTROLE DE DATE.                                
          COPY  SYKWZINO.                                                       
          COPY  SYBWERRO.                                                       
          COPY  ABENDCOP.                                               00001860
          COPY  WORKDATC.                                               00001870
      *   COMMUNICATION AVEC DB2.                                               
          COPY SYKWSQ10.                                                        
          COPY SYBWDIV0.                                                        
                                                                        00001910
       01 ENREG-FIFJRBI.                                                00001940
          05 FIFJRBI-ENTETE                PIC X(08).                           
          05 FIFJRBI-MERCHANT-COUNTRY      PIC X(02).                           
          05 FIFJRBI-MERCHANT-ID           PIC X(15).                           
          05 FIFJRBI-CONTRACT              PIC X(10).                           
          05 FIFJRBI-PAYMENT-DATE          PIC X(08).                           
          05 FIFJRBI-TRANSACTION-ID        PIC X(06).                           
          05 FIFJRBI-ORIGIN-AMOUNT         PIC X(10).                           
          05 FIFJRBI-CURRENCY-CODE         PIC X(03).                           
          05 FIFJRBI-CARD-TYPE             PIC X(10).                           
          05 FIFJRBI-ORDER-ID              PIC X(08).                           
          05 FIFJRBI-RETURN-CONTEXT        PIC X(01).                           
          05 FIFJRBI-CUSTOMER-ID           PIC X(01).                           
          05 FIFJRBI-OPERATION-TYPE        PIC X(02).                           
          05 FIFJRBI-OPERATION-NUMBER      PIC X(01).                           
          05 FIFJRBI-REMITTANCE-DATE       PIC X(08).                           
          05 FIFJRBI-REMITTANCE-TIME       PIC X(06).                           
          05 FIFJRBI-BRUT-AMOUNT           PIC X(15) JUST RIGHT.                
          05 FIFJRBI-MATCH-STATUS          PIC X(08).                           
          05 FIFJRBI-REMITTANCE-NB         PIC X(06).                           
          05 FIFJRBI-NET-AMOUNT            PIC X(15) JUST RIGHT.                
          05 FIFJRBI-COMMISSION-AMOUNT     PIC X(15) JUST RIGHT.                
          05 FIFJRBI-COMMISSION-CURRENCY   PIC X(03).                           
          05 FIFJRBI-COMMISSION-OPER-CODE  PIC X(01).                           
          05 FIFJRBI-AUTHOR-ID             PIC X(01).                           
          05 FIFJRBI-SPECIFIC-DATA         PIC X(01).                           
          05 FIFJRBI-REJECT-AMOUNT         PIC X(01).                           
          05 FIFJRBI-REJECT-REASON         PIC X(01).                           
       01 DSECT-CARTBQE.                                                        
          05 CBQ-CODENR         PIC XX.                                         
             88  CBQ-CAISSE                 VALUE '20'.                         
             88  CBQ-DETAIL                 VALUES '10' '12' '14' '17'          
                                                   '50' '52' '54'               
                                                   '57'.                        
             88  CBQ-REMB                   VALUES '35' '36'.                   
             88  CBQ-CREDIT                 VALUES '10' '50' '14' '54'          
                                                   '36'.                        
             88  CBQ-DEBIT                  VALUES '12' '52' '17' '57'          
                                                   '35'.                        
          05 CBQ-DATE           PIC X(6).                                       
          05 CBQ-RIEN           PIC X(9).                                       
          05 CBQ-NPORTEUR       PIC X(19).                                      
          05 CBQ-TETE-CAISSE.                                                   
             10 CBQ-NIDENT         PIC X(10).                                   
             10 CBQ-NLOT           PIC X(7).                                    
             10 CBQ-NREMISE        PIC X(6).                                    
             10 CBQ-FILLER         PIC X(21).                                   
             10 FILLER         REDEFINES CBQ-FILLER.                            
                15  FILLER         PIC X(7).                                    
                15  CBQ-MTREMB     PIC 9(6)V99.                                 
                15  FILLER         PIC X(6).                                    
          05 CBQ-LIGNE-DETAIL   REDEFINES CBQ-TETE-CAISSE.                      
             10 CBQ-TOPCLUB        PIC X.                                       
             10 CBQ-NAUTOR         PIC X(4).                                    
             10 CBQ-TOPAUTO        PIC X.                                       
             10 CBQ-MTBRUT         PIC 9(10)V99.                                
             10 CBQ-MTCOM          PIC 9(6)V99.                                 
             10 CBQ-MTNET          PIC 9(10)V99.                                
             10 CBQ-CDEVISE        PIC X(3).                                    
             10 FILLER             PIC X(3).                                    
       01  TETE-CARTBQE.                                                        
           05  CBQ-CODENR-TETE    PIC X(02) VALUE '90'.                         
           05  FILLER             PIC X(22) VALUE SPACE.                        
           05  CBQ-DFICORG        PIC X(6).                                     
           05  FILLER             PIC X(50) VALUE SPACE.                        
       01  FIN-CARTBQE.                                                         
           05  CBQ-CODENR-FIN     PIC X(02) VALUE '98'.                         
           05  FILLER             PIC X(22) VALUE SPACE.                        
           05  CBQ-NFICORG        PIC 9(6).                                     
           05  FILLER             PIC X(50) VALUE SPACE.                        
1011B  COPY FIFCOF.                                                             
      *** DESCRIPTION DU FICHIER DE SORTIE                                      
       01 ENREG-FIFJRBO           PIC X(80).                                    
1011B  01 ENREG-FIFCOF            PIC X(150).                                   
      *   NOM DES MODULES APPELES.                                              
       01 BETDATC                    PIC X(08) VALUE 'BETDATC'.                 
       01 WS-BRUT-AMOUNT             PIC 9(10).                                 
       01 WS-NET-AMOUNT              PIC 9(10).                                 
       01 WS-COMMISSION-AMOUNT       PIC 9(10).                                 
1011B  01 WS-TOT-BRUT-COF            PIC S9(10) VALUE 0.                        
"      01 WS-TOT-NET-COF             PIC S9(10) VALUE 0.                        
1011B  01 WS-TOT-COM-COF             PIC S9(10) VALUE 0.                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 I                          PIC S9(04) COMP.                           
      *--                                                                       
       01 I                          PIC S9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 J                          PIC S9(04) COMP.                           
      *--                                                                       
       01 J                          PIC S9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 I-MAX                      PIC S9(04) COMP.                           
      *--                                                                       
       01 I-MAX                      PIC S9(04) COMP-5.                         
      *}                                                                        
       01 WS-TABLE-CUMULS.                                                      
          05 TAB-CUMULS       OCCURS 99.                                        
0112         10 TAB-CLE-ENREG-1.                                                
0112            15 TAB-NIDENT                PIC X(10).                         
0112            15 TAB-NREMISE               PIC X(06).                         
0112            15 TAB-REMITTANCE-DATE       PIC X(08).                         
0112         10 TAB-DETAIL OCCURS 99.                                           
0112            15 TAB-CLE-ENREG-2             PIC X(10).                       
                15 TAB-CODENR                  PIC XX.                          
                15 TAB-DATE                    PIC X(06).                       
                15 TAB-BRUT-TOT                PIC S9(15).                      
                15 TAB-NET-TOT                 PIC S9(15).                      
                15 TAB-COMMISSION-TOT          PIC S9(15).                      
      *   VARIABLES DE TRAVAIL.                                                 
       01 STATUT-FIFJRBI             PIC X(02).                                 
      *{ remove-comma-in-dde 1.5                                                
      *   88 STATUT-FIFJRBI-OK      VALUE '00' , '  ', '10'.                    
      *--                                                                       
          88 STATUT-FIFJRBI-OK      VALUE '00'   '  '  '10'.                    
      *}                                                                        
          88 FIN-FIFJRBI            VALUE '10'.                                 
       01 STATUT-FIFJRBO             PIC  X(02).                                
      *{ remove-comma-in-dde 1.5                                                
      *   88 STATUT-FIFJRBO-OK      VALUE '00' , '  '.                          
      *--                                                                       
          88 STATUT-FIFJRBO-OK      VALUE '00'   '  '.                          
      *}                                                                        
1011B  01 STATUT-FIFCOF              PIC  X(02).                                
      *{ remove-comma-in-dde 1.5                                                
1011B *   88 STATUT-FIFCOF-OK       VALUE '00' , '  '.                          
      *--                                                                       
          88 STATUT-FIFCOF-OK       VALUE '00'   '  '.                          
      *}                                                                        
       01 WS-CPT-ECRIS-DETAIL        PIC S9(07) COMP-3   VALUE 0.               
1011B  01 WS-CPT-ECRIS-DETAIL-COF    PIC S9(07) COMP-3   VALUE 0.               
       01 WS-CPT-ECRIS-EN-TETE       PIC S9(07) COMP-3   VALUE 0.               
1011B  01 WS-CPT-ECRIS-EN-TETE-COF   PIC S9(07) COMP-3   VALUE 0.               
       01 WS-NBR-LUS-FIFJRBI         PIC S9(07) COMP-3   VALUE 0.               
       01 WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.          
       01 WS-HHMMSSCC                PIC X(08)           VALUE SPACES.          
       01 WS-CLE-ENREG.                                                         
         03 WS-CLE-ENREG-1.                                                     
          05 WS-CONTRACT             PIC X(10).                                 
1011      05 WS-REMITTANCE-NB        PIC X(06).                                 
1011      05 WS-REMITTANCE-DATE      PIC X(08).                                 
0112     03 WS-CLE-ENREG-2.                                                     
0112      05 WS-PAYMENT-DATE         PIC X(08).                                 
0112      05 WS-SENS                 PIC X(02).                                 
0112   01 WS-CLE-ENREG-SAVE          PIC X(24).                                 
       01 WS-WHEN-COMPILED.                                                     
          05 WS-WHEN-COMPILED-MM-1      PIC XX.                                 
          05 FILLER                     PIC X.                                  
          05 WS-WHEN-COMPILED-JJ-1      PIC XX.                                 
          05 FILLER                     PIC X.                                  
          05 WS-WHEN-COMPILED-AA-1      PIC XX.                                 
          05 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.                
      *                                                                         
       01  FDATE-ENREG.                                                         
            02  FDAT.                                                           
                04  FDATE-JJ   PIC X(02).                                       
                04  FDATE-MM   PIC X(02).                                       
                04  FDATE-SS   PIC X(02).                                       
                04  FDATE-AA   PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
       01 DATE-COMPILE.                                                         
          05 COMPILE-JJ            PIC XX.                                      
          05 FILLER                PIC X VALUE '/'.                             
          05 COMPILE-MM            PIC XX.                                      
          05 FILLER                PIC XXX VALUE '/20'.                         
          05 COMPILE-AA            PIC XX.                                      
1011B  01 WS-TIME                  PIC 9(08).                                   
      *                                                                         
       01 WS-RECHERCHE-TAB           PIC X(01).                                 
          88 TROUVE-DS-TAB           VALUE '0'.                                 
          88 NON-TROUVE-DS-TAB       VALUE '1'.                                 
       01 WS-ENREG-A-TRAITER         PIC X(01).                                 
          88 ENREG-DEBIT             VALUE '0'.                                 
          88 ENREG-CREDIT            VALUE '1'.                                 
          88 ENREG-FIN               VALUE '2'.                                 
      *{ remove-comma-in-dde 1.5                                                
      *   88 ENREG-A-TRAITER         VALUE '0', '1'.                            
      *--                                                                       
          88 ENREG-A-TRAITER         VALUE '0'  '1'.                            
      *}                                                                        
          88 ENREG-A-TRAITER-KO      VALUE '3'.                                 
1011B  01 WS-TYPE-ENRE               PIC X(01).                                 
"         88 ENREG-CB                VALUE '0'.                                 
"         88 ENREG-COF               VALUE '1'.                                 
1011B     88 ENREG-AUTRE             VALUE '2'.                                 
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION                                                      
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BIF00SGO     SECTION.                                             
      *---------------                                                          
      *    INITIALISATION                                               00001900
           PERFORM  DEBUT                                               00002180
      *                                                                         
           PERFORM UNTIL FIN-FIFJRBI                                            
      *       REMISE A ZERO DU TABLEAU                                          
              INITIALIZE WS-TABLE-CUMULS                                        
              MOVE 0  TO I-MAX                                                  
      *       ON TRAITE LE FICHIER JUSQU'A UN ENREGISTREMENT DE FIN             
              PERFORM UNTIL ENREG-FIN                                           
                         OR FIN-FIFJRBI                                         
                 IF ENREG-A-TRAITER                                             
                    INSPECT FIFJRBI-BRUT-AMOUNT                                 
                       REPLACING ALL ' ' BY  '0'                                
                    INSPECT FIFJRBI-NET-AMOUNT                                  
                       REPLACING ALL ' ' BY  '0'                                
                    INSPECT FIFJRBI-COMMISSION-AMOUNT                           
                       REPLACING ALL ' ' BY  '0'                                
                    MOVE FIFJRBI-BRUT-AMOUNT                                    
                      TO WS-BRUT-AMOUNT                                         
                    MOVE FIFJRBI-NET-AMOUNT                                     
                      TO WS-NET-AMOUNT                                          
                    MOVE FIFJRBI-COMMISSION-AMOUNT                              
                      TO WS-COMMISSION-AMOUNT                                   
1011B               IF ENREG-CB                                                 
                       DISPLAY '==========================='                    
                       DISPLAY 'WS-CONTRACT : ' WS-CONTRACT                     
      * RAJOUT CONTRAT ARKEA : 0376916                                          
      * RAJOUT CONTRAT ARKEA : 0377226  MGD ARKEA                               
MA1607                 IF WS-CONTRACT (01:07) =  '2480676'                      
MA1708                 OR WS-CONTRACT (01:07) =  '0376916'                      
MA2509                 OR WS-CONTRACT (01:07) =  '0377226'                      
MA1607                    PERFORM CALCUL-MONTANTS                               
MA1607                 END-IF                                                   
                       PERFORM CALCUL-SOLDES                                    
1011B               ELSE                                                        
"                      IF ENREG-COF                                             
"                         PERFORM WRITE-FIFCOF-DETAIL                           
"                      END-IF                                                   
1011B               END-IF                                                      
                 END-IF                                                         
                 PERFORM READ-FIFJRBI                                           
              END-PERFORM                                                       
      *       ON ECRIT CE QU'ON A STOCKE EN TABLE INTERNE                       
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > I-MAX                     
                 PERFORM WRITE-FIFJRBO-EN-TETE                                  
0112             PERFORM WITH TEST BEFORE VARYING J FROM 1 BY 1                 
      *{ reorder-array-condition 1.1                                            
      *            UNTIL TAB-CLE-ENREG-2(I , J) = SPACES                        
      *               OR J > 99                                                 
      *--                                                                       
                   UNTIL J > 99                                                 
                      OR TAB-CLE-ENREG-2(I , J) = SPACES                        
      *}                                                                        
                    PERFORM WRITE-FIFJRBO-DETAIL                                
0112             END-PERFORM                                                    
              END-PERFORM                                                       
      *       ON RECOMMENCE A LIRE DANS LE CAS OU ON A CONCATENE EN             
      *       ENTREE DU PROG PLUSIEURS FICHIERS                                 
              PERFORM READ-FIFJRBI UNTIL ENREG-A-TRAITER                        
                                      OR FIN-FIFJRBI                            
           END-PERFORM                                                          
      *                                                                         
           PERFORM  FIN-BIF00SGO                                                
           .                                                                    
      *                                                                         
       FIN-MODULE-BIF00SGO. EXIT.                                               
              EJECT                                                     00002620
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME                                        
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! EXTRACTION DES TRANSACTIONS CB DARTY.COM     !'  00002400
           DISPLAY  '! DE LA SG ET MISE AU FORMAT "STANDARD"        !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'          
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED                               
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ                             
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA                             
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM                             
           DISPLAY  '! VERSION DU 16/01/2012 COMPILEE LE ' DATE-COMPILE         
      *    RECUPERATION DE DATE ET HEURE DE TRAITEMENT                          
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD                               
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4)                   00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTREE                                       
           OPEN  OUTPUT FIFJRBO                                         00002950
1011B                   FIFCOF                                                  
           OPEN  INPUT  FIFJRBI FDATE                                   00002950
      *                                                                         
      *    RECUPERATION ET CONTROLE DU FDATE                                    
           READ   FDATE  INTO   FDATE-ENREG   AT END                            
                MOVE  ' FDATE ! DATE DU JOUR : JJMMSSAA '                       
                                       TO   ABEND-MESS                          
                MOVE  01                     TO   ABEND-CODE                    
           PERFORM  FIN-ANORMALE.                                               
           MOVE '1' TO GFDATA                                                   
           MOVE FDATE-JJ  TO GFJOUR                                             
           MOVE FDATE-MM  TO GFMOIS                                             
           MOVE FDATE-SS  TO GFSIECLE                                           
           MOVE FDATE-AA  TO GFANNEE                                            
           CALL BETDATC USING WORK-BETDATC.                                     
           IF GFVDAT = 0                                                        
              MOVE 'DATE INCORRECTE  ' TO ABEND-MESS                            
              MOVE  02                     TO   ABEND-CODE                      
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           DISPLAY 'DATE PRISE EN COMPTE : ' GFSAMJ-0                           
           INITIALIZE STATUT-FIFJRBI STATUT-FIFJRBO                             
           SET ENREG-A-TRAITER-KO TO TRUE                                       
      *                                                                 00001900
      *    LECTURE DU FICHIER EN ENTREE JUSQU'A CE QU'ON TROUVE UN ENREG00001900
      *    A TRAITER                                                            
           PERFORM  READ-FIFJRBI UNTIL ENREG-A-TRAITER                          
                                 OR FIN-FIFJRBI                                 
           MOVE WS-CLE-ENREG-1 TO WS-CLE-ENREG-SAVE                             
           IF NOT FIN-FIFJRBI                                                   
      *    ECRITURE DE L'EN-TETE DE FICHIER                                     
              PERFORM WRITE-FIFJRBO-EN-TETE-FIC                                 
           END-IF                                                               
           .                                                                    
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME                                                
      *-----------------------------------------------------------------        
       FIN-BIF00SGO SECTION.                                            00007910
           IF WS-CPT-ECRIS-EN-TETE NOT = 0                                      
1011B         OR WS-CPT-ECRIS-DETAIL-COF NOT = 0                                
      *       ECRITURE DE LE LIGNE DE FIN DE FICHIER                            
              PERFORM WRITE-FIFJRBO-FIN-FICHIER                                 
           END-IF                                                               
      *    MESSAGE DE FIN DE PROGRAMME                                          
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS-FIFJRBI00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS EN-TETE ECRIS CB  : '         00002400
                    WS-CPT-ECRIS-EN-TETE                                        
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS DETAIL  ECRIS CB  : '         00002400
                    WS-CPT-ECRIS-DETAIL                                         
                    '.'                                                 00002400
1011B      DISPLAY  'NBRE ENREGISTREMENTS EN-TETE ECRIS COF : '         00002400
"                   WS-CPT-ECRIS-EN-TETE-COF                                    
"                   '.'                                                 00002400
"          DISPLAY  'NBRE ENREGISTREMENTS DETAIL  ECRIS COF : '         00002400
"                   WS-CPT-ECRIS-DETAIL-COF                                     
1011B               '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FIFJRBO FIFJRBI                                               
1011B             FIFCOF                                                        
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BIF00SGO. EXIT.                                          00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME                                               
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FIFJRBI FIFJRBO                                               
1011B             FIFCOF                                                        
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BIF00SG' TO   ABEND-PROG                              00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS                 00009280
           .                                                                    
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
       TRAITER-FICHIER-CB  SECTION.                                             
       FIN-TRAITER-FICHIER-CB.   EXIT.                                          
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTREE                                            
      *-----------------------------------------------------------------        
       READ-FIFJRBI SECTION.                                                    
           INITIALIZE ENREG-FIFJRBI                                             
           READ FIFJRBI INTO   WW-FIFJRBI                                       
                AT END     SET FIN-FIFJRBI    TO TRUE                           
                NOT AT END ADD 1  TO WS-NBR-LUS-FIFJRBI                         
      *{ Tr-Hexa-Map 1.5                                                        
      *         UNSTRING WW-FIFJRBI    DELIMITED BY    X'05'                    
      *--                                                                       
                UNSTRING WW-FIFJRBI    DELIMITED BY    X'09'                    
      *}                                                                        
                    INTO FIFJRBI-ENTETE                                         
                         FIFJRBI-MERCHANT-COUNTRY                               
                         FIFJRBI-MERCHANT-ID                                    
                         FIFJRBI-CONTRACT                                       
                         FIFJRBI-PAYMENT-DATE                                   
                         FIFJRBI-TRANSACTION-ID                                 
                         FIFJRBI-ORIGIN-AMOUNT                                  
                         FIFJRBI-CURRENCY-CODE                                  
                         FIFJRBI-CARD-TYPE                                      
                         FIFJRBI-ORDER-ID                                       
                         FIFJRBI-RETURN-CONTEXT                                 
                         FIFJRBI-CUSTOMER-ID                                    
                         FIFJRBI-OPERATION-TYPE                                 
                         FIFJRBI-OPERATION-NUMBER                               
                         FIFJRBI-REMITTANCE-DATE                                
                         FIFJRBI-REMITTANCE-TIME                                
                         FIFJRBI-BRUT-AMOUNT                                    
                         FIFJRBI-MATCH-STATUS                                   
                         FIFJRBI-REMITTANCE-NB                                  
                         FIFJRBI-NET-AMOUNT                                     
                         FIFJRBI-COMMISSION-AMOUNT                              
                         FIFJRBI-COMMISSION-CURRENCY                            
                         FIFJRBI-COMMISSION-OPER-CODE                           
                         FIFJRBI-AUTHOR-ID                                      
                         FIFJRBI-SPECIFIC-DATA                                  
                         FIFJRBI-REJECT-AMOUNT                                  
                         FIFJRBI-REJECT-REASON                                  
           MOVE ST-FIFJRBI TO STATUT-FIFJRBI                                    
           IF NOT STATUT-FIFJRBI-OK                                     59350001
              DISPLAY 'PB LECTURE  FIFJRBI ' STATUT-FIFJRBI             59350001
              PERFORM  FIN-ANORMALE                                             
           ELSE                                                                 
              MOVE FIFJRBI-CONTRACT        TO WS-CONTRACT                       
1011          MOVE FIFJRBI-REMITTANCE-DATE TO WS-REMITTANCE-DATE                
1011          MOVE FIFJRBI-REMITTANCE-NB   TO WS-REMITTANCE-NB                  
0112          MOVE FIFJRBI-PAYMENT-DATE    TO WS-PAYMENT-DATE                   
0112          MOVE FIFJRBI-OPERATION-TYPE  TO WS-SENS                           
              IF FIFJRBI-ENTETE = 'FIN'                                         
                 SET   ENREG-FIN                TO TRUE                         
              ELSE                                                              
                 EVALUATE FIFJRBI-OPERATION-TYPE                                
                    WHEN 'DT'                                                   
                       SET ENREG-DEBIT        TO TRUE                           
                    WHEN 'CT'                                                   
                       SET ENREG-CREDIT       TO TRUE                           
                    WHEN OTHER                                                  
                       SET ENREG-A-TRAITER-KO TO TRUE                           
                 END-EVALUATE                                                   
1011B *          ON RECUPERE LE TYPE DE TRANSACTION CB OU COFINOGA              
"                EVALUATE FIFJRBI-CARD-TYPE                                     
"                   WHEN 'VISA'                                                 
"                   WHEN 'CB'                                                   
MA0207              WHEN 'MASTERCARD'                                           
MA0707              WHEN 'MAESTRO'                                              
"                      SET ENREG-CB       TO TRUE                               
"                   WHEN 'COFINOGA'                                             
"                      SET ENREG-COF      TO TRUE                               
"                   WHEN OTHER                                                  
"                      SET ENREG-AUTRE    TO TRUE                               
1011B            END-EVALUATE                                                   
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       FIN-READ-FIFJRBI. EXIT.                                          00009300
      *                                                                         
       WRITE-FIFJRBO-EN-TETE-FIC       SECTION.                         59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           STRING GFJOUR                                                        
                  GFMOIS                                                        
                  GFANNEE                                                       
           DELIMITED BY SIZE INTO CBQ-DFICORG                                   
           MOVE TETE-CARTBQE      TO ENREG-FIFJRBO                              
           WRITE FIFJRBO-ENR    FROM ENREG-FIFJRBO                              
                                                                        59340001
           MOVE ST-FIFJRBO     TO STATUT-FIFJRBO                                
           IF NOT STATUT-FIFJRBO-OK                                     59350001
              DISPLAY 'PB ECRITURE FIFJRBO ' STATUT-FIFJRBO             59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           .                                                                    
       FIN-WRITE-FIFJRBO-EN-TETE-FIC. EXIT.                                     
      *                                                                 59420001
       WRITE-FIFJRBO-EN-TETE           SECTION.                         59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           INITIALIZE DSECT-CARTBQE                                             
           MOVE '20'                         TO CBQ-CODENR                      
           MOVE TAB-REMITTANCE-DATE(I)(3:6)  TO CBQ-DATE                        
           MOVE TAB-NIDENT(I)                TO CBQ-NIDENT                      
           MOVE TAB-NREMISE(I)               TO CBQ-NREMISE                     
           MOVE DSECT-CARTBQE TO ENREG-FIFJRBO                                  
           WRITE FIFJRBO-ENR    FROM ENREG-FIFJRBO                              
                                                                        59340001
           MOVE ST-FIFJRBO     TO STATUT-FIFJRBO                                
           IF NOT STATUT-FIFJRBO-OK                                     59350001
              DISPLAY 'PB ECRITURE FIFJRBO ' STATUT-FIFJRBO             59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           ADD 1 TO WS-CPT-ECRIS-EN-TETE                                        
           .                                                                    
       FIN-WRITE-FIFJRBO-EN-TETE. EXIT.                                         
      *                                                                 59420001
MA1607 CALCUL-MONTANTS                 SECTION.                                 
      *----------------------------------------                         59280001
           IF ENREG-DEBIT                                                       
              DISPLAY 'ENREG DEBIT  IE MT COMM RENSEIGNE'                       
           END-IF                                                               
           IF ENREG-CREDIT                                                      
              DISPLAY 'ENREG CREDIT IE MT COMM = ZERO   '                       
           END-IF                                                               
      *    DISPLAY '11 MT BRT : ' WS-BRUT-AMOUNT                                
      *    DISPLAY '11 MT NET : ' WS-NET-AMOUNT                                 
      *    DISPLAY '11 MT COM : ' WS-COMMISSION-AMOUNT.                         
           IF WS-BRUT-AMOUNT = 0                                                
              DISPLAY '========================================'                
              DISPLAY 'MONTANT BRUT = ZERO, ARRET DU TRAITEMENT'                
              DISPLAY 'ENREGISTREMENT N� : ' WS-NBR-LUS-FIFJRBI                 
              DISPLAY 'MONTANT BRUT = ZERO, ARRET DU TRAITEMENT'                
              DISPLAY 'ENREGISTREMENT N� : ' WS-NBR-LUS-FIFJRBI                 
              DISPLAY '========================================'                
              MOVE    'MONTANT BRUT = ZERO, ARRET DU TRAITEMENT'                
                      TO   ABEND-MESS                                           
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
      * POUR UN ENREGISTREMENT DT, DEBIT L'UN DES DEUX MONTANTS                 
      * DOIT ETRE RENSEIGNE.                                                    
           IF WS-NET-AMOUNT        = 0 AND                                      
              WS-COMMISSION-AMOUNT = 0 AND                                      
              ENREG-DEBIT                                                       
              DISPLAY '========================================'                
              DISPLAY 'DEBIT ET MNT NET ET MNT COMMISSION = ZERO'               
              DISPLAY 'ARRET DU TRAITEMENT'                                     
              DISPLAY 'ENREGISTREMENT N� : ' WS-NBR-LUS-FIFJRBI                 
              DISPLAY 'DEBIT ET MNT NET ET MNT COMMISSION = ZERO'               
              DISPLAY 'ARRET DU TRAITEMENT'                                     
              DISPLAY 'ENREGISTREMENT N� : ' WS-NBR-LUS-FIFJRBI                 
              DISPLAY '========================================'                
              MOVE    'DEBIT ET MT NET ET COMM = ZERO, PLANTAGE'                
                      TO   ABEND-MESS                                           
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           IF WS-NET-AMOUNT = 0                                                 
              COMPUTE WS-NET-AMOUNT = WS-BRUT-AMOUNT -                          
                                      WS-COMMISSION-AMOUNT                      
           END-IF                                                               
      *    IF WS-COMMISSION-AMOUNT = 0                                          
      *       COMPUTE WS-COMMISSION-AMOUNT = WS-BRUT-AMOUNT -                   
      *                                      WS-NET-AMOUNT                      
      *    END-IF.                                                              
      *    DISPLAY '22 MT NET : ' WS-NET-AMOUNT                                 
      *    DISPLAY '22 MT COM : ' WS-COMMISSION-AMOUNT.                         
           .                                                                    
MA1607 FIN-CALCUL-MONTANTS.     EXIT.                                           
       CALCUL-SOLDES                   SECTION.                                 
      *----------------------------------------                         59280001
           SET NON-TROUVE-DS-TAB TO TRUE                                        
           PERFORM WITH TEST AFTER VARYING I FROM 1 BY 1                        
             UNTIL TROUVE-DS-TAB OR I > I-MAX                                   
              IF WS-CLE-ENREG-1 = TAB-CLE-ENREG-1(I)                            
                 SET TROUVE-DS-TAB TO TRUE                                      
              END-IF                                                            
           END-PERFORM                                                          
           IF NON-TROUVE-DS-TAB                                                 
              ADD 1 TO I-MAX                                                    
              MOVE WS-CLE-ENREG-1               TO TAB-CLE-ENREG-1(I)           
           END-IF                                                               
           SET NON-TROUVE-DS-TAB TO TRUE                                        
           PERFORM WITH TEST AFTER VARYING J FROM 1 BY 1                        
             UNTIL TROUVE-DS-TAB OR TAB-CLE-ENREG-2(I , J) = ' '                
              IF WS-CLE-ENREG-2 = TAB-CLE-ENREG-2(I , J)                        
                 SET TROUVE-DS-TAB TO TRUE                                      
              END-IF                                                            
           END-PERFORM                                                          
           IF NON-TROUVE-DS-TAB                                                 
              MOVE WS-CLE-ENREG-2            TO TAB-CLE-ENREG-2(I , J)          
              MOVE FIFJRBI-PAYMENT-DATE(3:6) TO TAB-DATE(I , J)                 
              MOVE 0                      TO TAB-BRUT-TOT(I , J)                
                                             TAB-NET-TOT(I , J)                 
                                              TAB-COMMISSION-TOT(I , J)         
           END-IF                                                               
           IF ENREG-DEBIT                                                       
              ADD WS-BRUT-AMOUNT          TO TAB-BRUT-TOT(I , J)                
              ADD WS-NET-AMOUNT           TO TAB-NET-TOT(I , J)                 
              ADD WS-COMMISSION-AMOUNT    TO TAB-COMMISSION-TOT(I , J)          
           ELSE                                                                 
              SUBTRACT WS-BRUT-AMOUNT     FROM TAB-BRUT-TOT(I , J)              
              SUBTRACT WS-NET-AMOUNT      FROM TAB-NET-TOT(I , J)               
              SUBTRACT WS-COMMISSION-AMOUNT                                     
                  FROM TAB-COMMISSION-TOT(I , J)                                
           END-IF                                                               
           .                                                                    
       FIN-CALCUL-SOLDES.       EXIT.                                           
       WRITE-FIFJRBO-DETAIL            SECTION.                         59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           MOVE SPACES              TO CBQ-NPORTEUR                             
           MOVE 'EUR'               TO CBQ-CDEVISE                              
           MOVE TAB-DATE(I , J)         TO CBQ-DATE                             
      *    LES MONTANTS SONT EN CENTIEME DE CENTIMES D'EUROS DANS LE JRB        
           COMPUTE CBQ-MTBRUT ROUNDED = TAB-BRUT-TOT(I , J) / 10000             
           COMPUTE CBQ-MTCOM ROUNDED = TAB-COMMISSION-TOT(I , J) / 10000        
           COMPUTE CBQ-MTNET ROUNDED = TAB-NET-TOT(I , J) / 10000               
           IF TAB-BRUT-TOT(I , J) > 0                                           
              MOVE '50'         TO   CBQ-CODENR                                 
           ELSE                                                                 
              MOVE '52'         TO   CBQ-CODENR                                 
           END-IF                                                               
           MOVE DSECT-CARTBQE   TO   ENREG-FIFJRBO                              
           WRITE FIFJRBO-ENR    FROM ENREG-FIFJRBO                              
                                                                        59340001
           MOVE ST-FIFJRBO     TO STATUT-FIFJRBO                                
           IF NOT STATUT-FIFJRBO-OK                                     59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           ADD 1 TO WS-CPT-ECRIS-DETAIL                                         
           .                                                                    
       FIN-WRITE-FIFJRBO-DETAIL.  EXIT.                                         
      *                                                                 59420001
1011B  WRITE-FIFCOF-DETAIL    SECTION.                                          
0112       IF WS-CPT-ECRIS-DETAIL-COF = 0                                       
1011B         MOVE GFSAMJ-0 TO COF-DCREAT-TETE                                  
"             ACCEPT WS-TIME FROM TIME                                          
"             MOVE WS-TIME(1:4) TO COF-HCREAT-TETE                              
"             MOVE 'E'      TO COF-CODENR-TETE                                  
"             MOVE COF-ENTETE TO ENREG-FIFCOF                                   
"                                                                               
"             WRITE FIFCOF-ENR    FROM ENREG-FIFCOF                             
"                                                                               
"             MOVE ST-FIFCOF      TO STATUT-FIFCOF                              
"             IF NOT STATUT-FIFCOF-OK                                   59350001
"                DISPLAY 'PB ECRITURE FIFCOF ' STATUT-FIFCOF            59350001
"                PERFORM  FIN-ANORMALE                                          
"             END-IF                                                            
1011B         ADD 1 TO WS-CPT-ECRIS-EN-TETE-COF                                 
0112       END-IF                                                               
1011B      MOVE 'D'             TO COF-CODENR                                   
"          MOVE 'CO'            TO COF-TYPMT                                    
"          MOVE FIFJRBI-PAYMENT-DATE     TO COF-DTRANS                          
"          MOVE FIFJRBI-REMITTANCE-TIME  TO COF-HTRANS                          
"          MOVE FIFJRBI-REMITTANCE-DATE  TO COF-DREMISE                         
"          MOVE FIFJRBI-CONTRACT         TO COF-IDCONTRAT                       
"          MOVE FIFJRBI-ORDER-ID         TO COF-NVENTE                          
"          MOVE FIFJRBI-REMITTANCE-NB    TO COF-NREMISE                         
"          MOVE SPACES                   TO COF-NPORTEUR                        
"                                           COF-NAUTOR                          
"          COMPUTE COF-MTBRUT = WS-BRUT-AMOUNT / 10000                          
"          COMPUTE COF-MTNET  = WS-NET-AMOUNT  / 10000                          
"          COMPUTE COF-MTCOM =  WS-COMMISSION-AMOUNT  / 10000                   
"          IF ENREG-DEBIT                                                       
"             MOVE 'D'          TO COF-SENS                                     
"             ADD COF-MTBRUT TO WS-TOT-BRUT-COF                                 
"             ADD COF-MTNET  TO WS-TOT-NET-COF                                  
"             ADD COF-MTCOM  TO WS-TOT-COM-COF                                  
"          ELSE                                                                 
"             MOVE 'C'          TO COF-SENS                                     
"             SUBTRACT COF-MTBRUT FROM WS-TOT-BRUT-COF                          
"             SUBTRACT COF-MTNET  FROM WS-TOT-NET-COF                           
"             SUBTRACT COF-MTCOM  FROM WS-TOT-COM-COF                           
"          END-IF                                                               
"          MOVE 'EUR'                    TO COF-DEVISE                          
"                                                                               
"          MOVE DSECT-COFINOGA  TO ENREG-FIFCOF                                 
"                                                                               
"          WRITE FIFCOF-ENR    FROM ENREG-FIFCOF                                
"                                                                       59340001
"          MOVE ST-FIFCOF      TO STATUT-FIFCOF                                 
"          IF NOT STATUT-FIFCOF-OK                                      59350001
"             PERFORM  FIN-ANORMALE                                             
"          END-IF                                                               
"          ADD 1 TO WS-CPT-ECRIS-DETAIL-COF                                     
"          .                                                                    
1011B  FIN-WRITE-FIFCOF-DETAIL.   EXIT.                                         
       WRITE-FIFJRBO-FIN-FICHIER   SECTION.                             59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           MOVE WS-CPT-ECRIS-DETAIL TO CBQ-NFICORG                              
           MOVE FIN-CARTBQE         TO ENREG-FIFJRBO                            
           WRITE FIFJRBO-ENR        FROM ENREG-FIFJRBO                          
                                                                        59340001
           MOVE ST-FIFJRBO     TO STATUT-FIFJRBO                                
           IF NOT STATUT-FIFJRBO-OK                                     59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
0112       IF WS-CPT-ECRIS-DETAIL-COF > 0                                       
1011B         MOVE 'F'                      TO COF-CODENR-FIN                   
"             MOVE GFSAMJ-0                 TO COF-DCREAT-FIN                   
"             MOVE WS-TIME(1:4)             TO COF-HCREAT-FIN                   
"             MOVE WS-CPT-ECRIS-DETAIL-COF  TO COF-NBENR                        
"             MOVE WS-TOT-BRUT-COF          TO COF-TOTBRUT                      
"             MOVE WS-TOT-NET-COF           TO COF-TOTNET                       
"             MOVE WS-TOT-COM-COF           TO COF-TOTCOM                       
"                                                                               
"             MOVE COF-FIN             TO ENREG-FIFCOF                          
"                                                                               
"             WRITE FIFCOF-ENR         FROM ENREG-FIFCOF                        
"                                                                       59340001
"             MOVE ST-FIFCOF      TO STATUT-FIFCOF                              
"             IF NOT STATUT-FIFCOF-OK                                   59350001
"                PERFORM  FIN-ANORMALE                                          
1011B         END-IF                                                            
0112       END-IF                                                               
           .                                                                    
       FIN-WRITE-FIFJRBO-FIN-FICHIER. EXIT.                                     
       COPY SYBCERRO.                                                           
      *                                                                 59260001
