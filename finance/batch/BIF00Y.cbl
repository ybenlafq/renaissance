      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00Y.                                                     
       AUTHOR.  DSA015.                                                         
      ******************************************************************        
      * OCTOBRE 2009 - CREATION                                                 
      ******************************************************************        
      * FEVRIER 2010 - TRAITEMENT DES TRANSACTIONS D'AJUSTEMENT DU              
      *                COMPTE T1201 ET T0106                                    
      ******************************************************************        
      *    JUIN 2012 - TRAITEMENT DES TRANSACTIONS D'AJUSTEMENT DU              
      *                COMPTE DE TYPE T15XX (BLOCAGE POUR LITIGE)               
      ******************************************************************        
      * JANVIER 2013 - TRAITEMENT DES TRANSACTIONS DE CORRECTION DU             
      *                COMPTE DE TYPE T1900                                     
      * 16/09/2015   - TRAITEMENT DES TRANSACTIONS DE TYPE T1200 ET             
      *                T1202. SIGNET MA1609.                                    
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPAYPAL   ASSIGN TO FPAYPAL                                   
      *                     FILE STATUS IS ST-PPL.                              
      *--                                                                       
           SELECT FPAYPAL   ASSIGN TO FPAYPAL                                   
                            FILE STATUS IS ST-PPL                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FPAYPAL                                                              
           RECORDING V                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD                                                
           RECORD CONTAINS 1 TO 500 CHARACTERS                          00000470
           DATA RECORD IS FPAYPAL-ENR.                                  00000470
       01  FPAYPAL-ENR.                                                         
           10  FPAYPAL-ENREG          PICTURE X(01)                     00000480
                                      OCCURS 1 TO 500 TIMES                     
                                      DEPENDING ON I-LONG.                      
       WORKING-STORAGE SECTION.                                                 
       01 ENREG-FPAYPAL.                                                        
          05 ENREG-FPAYPAL-CODENR       PIC X(02).                              
             88 ENREG-RH             VALUE 'RH'.                                
      *      88 ENREG-FH             VALUE 'FH'.                                
             88 ENREG-SH             VALUE 'SH'.                                
             88 ENREG-CH             VALUE 'CH'.                                
             88 ENREG-SB             VALUE 'SB'.                                
             88 ENREG-SF             VALUE 'SF'.                                
      *      88 ENREG-SC             VALUE 'SC'.                                
             88 ENREG-RF             VALUE 'RF'.                                
      *      88 ENREG-RC             VALUE 'RC'.                                
      *      88 ENREG-FF             VALUE 'FF'.                                
          05 ENREG-FPAYPAL-DONNEES      PIC X(498).                             
       01 ENREG-FPAYPAL-DONNES-B        PIC X(498).                             
       01 ENREG-FPAYPAL-RH REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-RH-DATGEN.                                                 
             10 FPAYPAL-RH-DFICORG.                                             
                15 FPAYPAL-RH-DFICORG-SS      PIC X(02).                        
                15 FPAYPAL-RH-DFICORG-AA      PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-RH-DFICORG-MM      PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-RH-DFICORG-JJ      PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-RH-TIMORG.                                              
                15 FPAYPAL-RH-DFICORG-HH      PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-RH-DFICORG-MIN     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-RH-DFICORG-SEC     PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-RH-DFICORG-OFFSET  PIC X(05).                           
          05 FPAYPAL-RH-WINDOW-A.                                               
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-RH-WINDOW             PIC X(05).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-RH-ACCOUNTID-A.                                            
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-RH-ACCOUNTID          PIC X(20).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-RH-VERSION            PIC X(03).                           
          05 FPAYPAL-RH-FILLER             PIC X(401).                          
      *01 ENREG-FPAYPAL-FH.                                                     
      *   05 FPAYPAL-FH-                                                        
       01 ENREG-FPAYPAL-SH REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-SH-REPORT-DEB.                                             
             10 FPAYPAL-SH-DMVT-DEB.                                            
                15 FPAYPAL-SH-DMVT-DEB-SS     PIC X(02).                        
                15 FPAYPAL-SH-DMVT-DEB-AA     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-DMVT-DEB-MM     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-DMVT-DEB-JJ     PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SH-HMVT-DEB.                                            
                15 FPAYPAL-SH-HMVT-DEB-HH     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-HMVT-DEB-MIN    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-HMVT-DEB-SEC    PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SH-REPORT-DEB-OFF  PIC X(05).                           
          05 FPAYPAL-SH-REPORT-FIN.                                             
             10 FPAYPAL-SH-DMVT-FIN.                                            
                15 FPAYPAL-SH-DMVT-FIN-SSAA   PIC X(04).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-DMVT-FIN-MM     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-DMVT-FIN-JJ     PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SH-HMVT-FIN.                                            
                15 FPAYPAL-SH-HMVT-FIN-HH     PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-HMVT-FIN-MIN    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SH-HMVT-FIN-SEC    PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SH-REPORT-FIN-OFF  PIC X(05).                           
          05 FPAYPAL-SH-ACCOUNTID-A.                                            
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SH-ACCOUNTID          PIC X(20).                        
             10 FILLER                        PIC X(01).                        
       01 ENREG-FPAYPAL-CH REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-CH-TITRES             PIC X(497).                          
       01 ENREG-FPAYPAL-SB REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-SB-NTRANSID-A.                                             
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-NTRANSID           PIC X(19).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-INVOICEID-A.                                            
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-INVOICEID          PIC X(127).                       
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-REFID-A.                                                
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-REFID              PIC X(19).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-REFTYPE-A.                                              
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-REFTYPE            PIC X(03).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-TRANS-CODE-A.                                           
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-TRANS-CODE.                                          
                15 FPAYPAL-SB-TRANS-CODE-1      PIC X(03).                      
                15 FPAYPAL-SB-TRANS-CODE-2      PIC X(02).                      
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-DATE-INIT-TRANS.                                        
             10 FPAYPAL-SB-DATE-INIT.                                           
                15 FPAYPAL-SB-DATE-INIT-SS    PIC X(02).                        
                15 FPAYPAL-SB-DATE-INIT-AA    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-DATE-INIT-MM    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-DATE-INIT-JJ    PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SB-TIME-INIT.                                           
                15 FPAYPAL-SB-TIME-INIT-HH    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-TIME-INIT-MIN   PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-TIME-INIT-SEC   PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SB-DINIT-OFFSET    PIC X(05).                           
          05 FPAYPAL-SB-DATE-COMP-TRANS.                                        
             10 FPAYPAL-SB-DATE-COMP.                                           
                15 FPAYPAL-SB-DATE-COMP-SSAA  PIC X(04).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-DATE-COMP-HH    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-DATE-COMP-JJ    PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SB-TIME-COMP.                                           
                15 FPAYPAL-SB-TIME-COMP-HH    PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-TIME-COMP-MIN   PIC X(02).                        
                15 FILLER                     PIC X(01).                        
                15 FPAYPAL-SB-TIME-COMP-SEC   PIC X(02).                        
             10 FILLER                     PIC X(01).                           
             10 FPAYPAL-SB-DCOMP-OFFSET    PIC X(05).                           
          05 FPAYPAL-SB-SENS-A.                                                 
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-SENS               PIC X(02).                        
                88 FPAYPAL-SB-CREDIT          VALUE 'CR'.                       
                88 FPAYPAL-SB-DEBIT           VALUE 'DR'.                       
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-TRANS-MT              PIC X(26).                        
          05 FPAYPAL-SB-TRANS-DEVISE-A.                                         
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-TRANS-DEVISE       PIC X(03).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-SENS-COMM-A.                                            
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-SENS-COMM          PIC X(02).                        
                88 FPAYPAL-SB-COMM-CREDIT     VALUE 'CR'.                       
                88 FPAYPAL-SB-COMM-DEBIT      VALUE 'DR'.                       
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-COMM-MT            PIC X(26).                           
          05 FPAYPAL-SB-COMM-DEVISE-A.                                          
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SB-COMM-DEVISE        PIC X(03).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SB-FILLER             PIC X(200).                          
       01 ENREG-FPAYPAL-SF REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-SF-DEVISE-A.                                               
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SF-DEVISE             PIC X(03).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SF-MT-CRE-TOT         PIC X(26).                           
          05 FPAYPAL-SF-MT-DEB-TOT         PIC X(26).                           
          05 FPAYPAL-SF-MT-COM-CRE-TOT     PIC X(26).                           
          05 FPAYPAL-SF-MT-COM-DEB-TOT     PIC X(26).                           
          05 FPAYPAL-SF-SENS-BALANCE-AV-A.                                      
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SF-SENS-BALANCE-AV    PIC X(02).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SF-MT-BALANCE-AV      PIC X(26).                           
          05 FPAYPAL-SF-SENS-BALANCE-AP-A.                                      
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-SF-SENS-BALANCE-AP    PIC X(02).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-SF-MT-BALANCE-AP      PIC X(26).                           
          05 FPAYPAL-SF-NRECORDS           PIC X(07).                           
      *01 ENREG-FPAYPAL-SC.                                                     
      *   05 FPAYPAL-SC-                                                        
       01 ENREG-FPAYPAL-RF REDEFINES ENREG-FPAYPAL-DONNES-B.                    
          05 FPAYPAL-RF-DEVISE-A.                                               
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-RF-DEVISE             PIC X(03).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-RF-MT-CRE-TOT         PIC X(26).                           
          05 FPAYPAL-RF-MT-DEB-TOT         PIC X(26).                           
          05 FPAYPAL-RF-MT-COM-CRE-TOT     PIC X(26).                           
          05 FPAYPAL-RF-MT-COM-DEB-TOT     PIC X(26).                           
          05 FPAYPAL-RF-SENS-BALANCE-AV-A.                                      
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-RF-SENS-BALANCE-AV    PIC X(02).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-RF-MT-BALANCE-AV      PIC X(26).                           
          05 FPAYPAL-RF-SENS-BALANCE-AP-A.                                      
             10 FILLER                        PIC X(01).                        
             10 FPAYPAL-RF-SENS-BALANCE-AP    PIC X(02).                        
             10 FILLER                        PIC X(01).                        
          05 FPAYPAL-RF-MT-BALANCE-AP      PIC X(26).                           
          05 FPAYPAL-RF-NFICORG            PIC X(07).                           
      *01 ENREG-FPAYPAL-RC.                                                     
      *   05 FPAYPAL-RC-                                                        
      *01 ENREG-FPAYPAL-FF.                                                     
      *   05 FPAYPAL-FF-                                                        
       01 WS-BIF00Y.                                                            
          05  W-DEVISE-EUR       PIC X(3)             VALUE 'EUR'.              
          05  W-MTNET            PIC S9(13)V99 COMP-3 VALUE ZERO.               
          05  W-BETDATC          PIC X(8)             VALUE 'BETDATC'.          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  L                  PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
          05  L                  PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
          05  W-CARAC            PIC X(01).                                     
      *   ZONES DE MONTANTS DU FICHIERS CONVERTIES EN NUMERIQUE                 
          05 FPAYPAL-SB-TRANS-MT-9            PIC 9(09)V99.                     
          05 FPAYPAL-SB-COMM-MT-9             PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-CRE-TOT-9          PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-DEB-TOT-9          PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-COM-CRE-TOT-9      PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-COM-DEB-TOT-9      PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-BALANCE-AV-9       PIC 9(07)V99.                     
          05 FPAYPAL-SF-MT-BALANCE-AP-9       PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-CRE-TOT-9          PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-DEB-TOT-9          PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-COM-CRE-TOT-9      PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-COM-DEB-TOT-9      PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-BALANCE-AV-9       PIC 9(07)V99.                     
          05 FPAYPAL-RF-MT-BALANCE-AP-9       PIC 9(07)V99.                     
      *   VARIABLES POUR CONVERSION DES MONTANTS                                
          05  W-MONTANT          PIC X(26).                                     
          05  W-MONTANT-9        PIC 9(09)V99.                                  
          05  W-MONTANT-EDIT     PIC 999999999,99.                              
          05  W-MONTANT-EDIT-2.                                                 
              10 W-MONTANT-ENT      PIC 9(09).                                  
              10 W-SEP              PIC X(01) VALUE ','.                        
              10 W-MONTANT-DEC      PIC 9(02).                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  J                  PIC S9(04) COMP VALUE 0.                       
      *--                                                                       
          05  J                  PIC S9(04) COMP-5 VALUE 0.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  J-MAX              PIC S9(04) COMP VALUE 11.                      
      *--                                                                       
          05  J-MAX              PIC S9(04) COMP-5 VALUE 11.                    
      *}                                                                        
          05  W-NOMBRE           PIC X(07).                                     
          05  W-NOMBRE-9         PIC 9(07).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  K                  PIC S9(04) COMP VALUE 0.                       
      *--                                                                       
          05  K                  PIC S9(04) COMP-5 VALUE 0.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  K-MAX              PIC S9(04) COMP VALUE 7.                       
      *--                                                                       
          05  K-MAX              PIC S9(04) COMP-5 VALUE 7.                     
      *}                                                                        
       77 ST-PPL             PIC 99           VALUE 0.                          
       77 I-LONG             PIC S9(3) COMP-3 VALUE 0.                          
       77 WS-NBR-LUS-FPAYPAL PIC S9(3) COMP-3 VALUE 0.                          
       01 WW-FPAYPAL            PIC X(500).                                     
       01 FLAG-FIN-FPAYPAL       PIC X(01) VALUE '0'.                           
          88  FIN-FPAYPAL            VALUE '1'.                                 
       01 FLAG-CHAINE-CARAC      PIC X(01) VALUE '0'.                           
          88  FIN-CHAINE-CARAC-KO    VALUE '0'.                                 
          88  FIN-CHAINE-CARAC       VALUE '1'.                                 
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000          PIC X(192).                                        
      *                                                                         
       01  L-DATE            PIC X(8).                                          
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00Y                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L                                        
           MOVE L-FIF000            TO DSECT-IF000                              
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FPAYPAL                                                
              MOVE ST-PPL           TO L-FSTATUS                                
           END-IF                                                               
           IF ST-PPL = 00                                                       
              PERFORM LECTURE-FPAYPAL                                           
           END-IF                                                               
           IF ST-PPL = 00                                                       
              PERFORM TRAITEMENT-PAYPAL                                         
           END-IF                                                               
           IF ST-PPL = 10                                                       
              CLOSE FPAYPAL                                                     
           END-IF                                                               
           GOBACK                                                               
           .                                                                    
       TRAITEMENT-PAYPAL               SECTION.                                 
           PERFORM LECTURE-FPAYPAL                                              
                   UNTIL ST-PPL = 10                                            
                   OR ENREG-RH                                                  
                   OR ( ENREG-SB AND ( FPAYPAL-SB-TRANS-CODE-1 = 'T00'          
                      OR  FPAYPAL-SB-TRANS-CODE-1 = 'T11'                       
0612                  OR  FPAYPAL-SB-TRANS-CODE-1 = 'T15'                       
1301                  OR  FPAYPAL-SB-TRANS-CODE = 'T1900'                       
1202                  OR  FPAYPAL-SB-TRANS-CODE = 'T1201'                       
MA1609                OR  FPAYPAL-SB-TRANS-CODE = 'T1200'                       
MA1609                OR  FPAYPAL-SB-TRANS-CODE = 'T1202'                       
1202                  OR  FPAYPAL-SB-TRANS-CODE = 'T0106' ) )                   
           IF ST-PPL NOT = 10                                                   
              IF ENREG-RH                                                       
                 PERFORM TRAITEMENT-ENTETE                                      
              END-IF                                                            
              IF ENREG-SB                                                       
                 PERFORM TRAITEMENT-FICHIER                                     
                 ADD  1                TO L-NBRECUS                             
                 MOVE DSECT-IF000      TO L-FIF000                              
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       TRAITEMENT-ENTETE               SECTION.                                 
           MOVE SPACES                 TO IF000-CODANO                          
           MOVE FPAYPAL-RH-DFICORG-JJ  TO GFJJ                                  
           MOVE FPAYPAL-RH-DFICORG-MM  TO GFMM                                  
           MOVE FPAYPAL-RH-DFICORG-SS  TO GFSS                                  
           MOVE FPAYPAL-RH-DFICORG-AA  TO GFAA                                  
           MOVE '1'                    TO GFDATA                                
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DREMISE                         
           ELSE                                                                 
              MOVE '05'                TO IF000-CODANO                          
              STRING FPAYPAL-RH-DFICORG-SS                                      
                     FPAYPAL-RH-DFICORG-AA                                      
                     FPAYPAL-RH-DFICORG-MM                                      
                     FPAYPAL-RH-DFICORG-JJ                                      
              DELIMITED BY SIZE INTO IF000-DREMISE                              
           END-IF                                                               
           MOVE SPACES               TO IF000-NIDENT                            
           MOVE FPAYPAL-RH-ACCOUNTID(1:L) TO IF000-NIDENT                       
           MOVE IF000-DREMISE(3:6) TO IF000-NREMISE                             
           PERFORM LECTURE-FPAYPAL                                              
                   UNTIL ST-PPL = 10                                            
1202               OR ( ENREG-SB AND ( FPAYPAL-SB-TRANS-CODE-1 = 'T00'          
1202                  OR  FPAYPAL-SB-TRANS-CODE-1 = 'T11'                       
1202                  OR  FPAYPAL-SB-TRANS-CODE = 'T1201'                       
1202                  OR  FPAYPAL-SB-TRANS-CODE = 'T0106' ) )                   
           .                                                                    
       TRAITEMENT-FICHIER              SECTION.                                 
           MOVE FPAYPAL-SB-DATE-INIT-JJ TO GFJJ                                 
           MOVE FPAYPAL-SB-DATE-INIT-MM TO GFMM                                 
           MOVE FPAYPAL-SB-DATE-INIT-SS TO GFSS                                 
           MOVE FPAYPAL-SB-DATE-INIT-AA TO GFAA                                 
           MOVE '1'                 TO GFDATA                                   
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DOPER                           
              MOVE GFSAMJ-0            TO IF000-DFINANCE                        
           ELSE                                                                 
              STRING  FPAYPAL-SB-DATE-INIT-JJ                                   
                      FPAYPAL-SB-DATE-INIT-MM                                   
                      FPAYPAL-SB-DATE-INIT-SS                                   
                      FPAYPAL-SB-DATE-INIT-AA                                   
              DELIMITED BY SIZE INTO IF000-DOPER                                
              MOVE IF000-DOPER TO    IF000-DFINANCE                             
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           MOVE SPACES              TO IF000-NPORTEUR                           
           MOVE '*'                 TO IF000-CTYPMVT                            
           MOVE 1                   TO IF000-NFACTURES                          
           IF FPAYPAL-SB-CREDIT                                                 
              COMPUTE IF000-MTBRUT = FPAYPAL-SB-TRANS-MT-9                      
           ELSE                                                                 
              COMPUTE IF000-MTBRUT = -1 * FPAYPAL-SB-TRANS-MT-9                 
           END-IF                                                               
           IF FPAYPAL-SB-COMM-DEBIT                                             
              COMPUTE IF000-MTCOM = FPAYPAL-SB-COMM-MT-9                        
           ELSE                                                                 
              COMPUTE IF000-MTCOM = -1 * FPAYPAL-SB-COMM-MT-9                   
           END-IF                                                               
           COMPUTE IF000-MTNET = IF000-MTBRUT - IF000-MTCOM                     
      *                                                                         
           MOVE FPAYPAL-SB-TRANS-DEVISE TO IF000-CDEVISE                        
           PERFORM CONTROLE-REJETS                                              
           .                                                                    
      *                                                                         
       CONTROLE-REJETS                 SECTION.                                 
           IF IF000-CODANO = SPACES                                             
              IF IF000-DFINANCE < IF000-DOPER                                   
                 MOVE '04'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-DREMISE < IF000-DOPER                                    
                 MOVE '06'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTBRUT = ZERO                                            
                 MOVE '08'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTNET = ZERO                                             
                 MOVE '09'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM                      
              IF IF000-MTNET NOT = W-MTNET                                      
                 MOVE '10'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
      *                                                                         
           IF IF000-CDEVISE NOT = W-DEVISE-EUR                                  
              IF IF000-CODANO = SPACES                                          
                 MOVE '33'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       LECTURE-FPAYPAL                 SECTION.                                 
           READ FPAYPAL INTO   WW-FPAYPAL                                       
      *         AT END     SET FIN-FPAYPAL    TO TRUE                           
      *         NOT AT END ADD 1  TO WS-NBR-LUS-                                
           IF ST-PPL = 00                                                       
              MOVE WW-FPAYPAL(2:2) TO ENREG-FPAYPAL-CODENR                      
              MOVE WW-FPAYPAL(6:494) TO ENREG-FPAYPAL-DONNEES                   
              EVALUATE TRUE                                                     
                 WHEN ENREG-RH                                                  
                    UNSTRING ENREG-FPAYPAL-DONNEES                              
                     DELIMITED BY ','   INTO                                    
                       FPAYPAL-RH-DATGEN                                        
                       FPAYPAL-RH-WINDOW-A                                      
                       FPAYPAL-RH-ACCOUNTID-A                                   
                       FPAYPAL-RH-VERSION                                       
                       FPAYPAL-RH-FILLER                                        
                    END-UNSTRING                                                
                    STRING                                                      
                       FPAYPAL-RH-DFICORG-SS                                    
                       FPAYPAL-RH-DFICORG-AA                                    
                       FPAYPAL-RH-DFICORG-MM                                    
                       FPAYPAL-RH-DFICORG-JJ                                    
                    DELIMITED BY SIZE INTO L-DFICORG                            
                 WHEN ENREG-SH                                                  
                    UNSTRING ENREG-FPAYPAL-DONNEES                              
                     DELIMITED BY ',' INTO                                      
                       FPAYPAL-SH-REPORT-DEB                                    
                       FPAYPAL-SH-REPORT-FIN                                    
                       FPAYPAL-SH-ACCOUNTID-A                                   
                 WHEN ENREG-SB                                                  
                    UNSTRING ENREG-FPAYPAL-DONNEES                              
                     DELIMITED BY ',' INTO                                      
                       FPAYPAL-SB-NTRANSID-A                                    
                       FPAYPAL-SB-INVOICEID-A                                   
                       FPAYPAL-SB-REFID-A                                       
                       FPAYPAL-SB-REFTYPE-A                                     
                       FPAYPAL-SB-TRANS-CODE-A                                  
                       FPAYPAL-SB-DATE-INIT-TRANS                               
                       FPAYPAL-SB-DATE-COMP-TRANS                               
                       FPAYPAL-SB-SENS-A                                        
                       FPAYPAL-SB-TRANS-MT                                      
                       FPAYPAL-SB-TRANS-DEVISE-A                                
                       FPAYPAL-SB-SENS-COMM-A                                   
                       FPAYPAL-SB-COMM-MT                                       
                       FPAYPAL-SB-COMM-DEVISE-A                                 
                       FPAYPAL-SB-FILLER                                        
                     MOVE FPAYPAL-SB-TRANS-MT                                   
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SB-TRANS-MT-9                                 
                     MOVE FPAYPAL-SB-COMM-MT                                    
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SB-COMM-MT-9                                  
                 WHEN ENREG-SF                                                  
                    UNSTRING ENREG-FPAYPAL-DONNEES                              
                     DELIMITED BY ',' INTO                                      
                       FPAYPAL-SF-DEVISE-A                                      
                       FPAYPAL-SF-MT-CRE-TOT                                    
                       FPAYPAL-SF-MT-DEB-TOT                                    
                       FPAYPAL-SF-MT-COM-CRE-TOT                                
                       FPAYPAL-SF-MT-COM-DEB-TOT                                
                       FPAYPAL-SF-SENS-BALANCE-AV-A                             
                       FPAYPAL-SF-MT-BALANCE-AV                                 
                       FPAYPAL-SF-SENS-BALANCE-AP-A                             
                       FPAYPAL-SF-MT-BALANCE-AP                                 
                       FPAYPAL-SF-NRECORDS                                      
                     MOVE FPAYPAL-SF-MT-CRE-TOT                                 
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-CRE-TOT-9                               
                     MOVE FPAYPAL-SF-MT-DEB-TOT                                 
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-DEB-TOT-9                               
                     MOVE FPAYPAL-SF-MT-COM-CRE-TOT                             
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-COM-CRE-TOT-9                           
                     MOVE FPAYPAL-SF-MT-COM-DEB-TOT                             
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-COM-DEB-TOT-9                           
                     MOVE FPAYPAL-SF-MT-BALANCE-AV                              
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-BALANCE-AV-9                            
                     MOVE FPAYPAL-SF-MT-BALANCE-AP                              
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-SF-MT-BALANCE-AP-9                            
                 WHEN ENREG-RF                                                  
                    UNSTRING ENREG-FPAYPAL-DONNEES                              
                     DELIMITED BY ',' INTO                                      
                       FPAYPAL-RF-DEVISE-A                                      
                       FPAYPAL-RF-MT-CRE-TOT                                    
                       FPAYPAL-RF-MT-DEB-TOT                                    
                       FPAYPAL-RF-MT-COM-CRE-TOT                                
                       FPAYPAL-RF-MT-COM-DEB-TOT                                
                       FPAYPAL-RF-SENS-BALANCE-AV-A                             
                       FPAYPAL-RF-MT-BALANCE-AV                                 
                       FPAYPAL-RF-SENS-BALANCE-AP-A                             
                       FPAYPAL-RF-MT-BALANCE-AP                                 
                       FPAYPAL-RF-NFICORG                                       
                     MOVE FPAYPAL-RF-MT-CRE-TOT                                 
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-CRE-TOT-9                               
                     MOVE FPAYPAL-RF-MT-DEB-TOT                                 
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-DEB-TOT-9                               
                     MOVE FPAYPAL-RF-MT-COM-CRE-TOT                             
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-COM-CRE-TOT-9                           
                     MOVE FPAYPAL-RF-MT-COM-DEB-TOT                             
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-COM-DEB-TOT-9                           
                     MOVE FPAYPAL-RF-MT-BALANCE-AV                              
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-BALANCE-AV-9                            
                     MOVE FPAYPAL-RF-MT-BALANCE-AP                              
                       TO W-MONTANT                                             
                     PERFORM CADRAGE-MONTANT                                    
                     MOVE W-MONTANT-9                                           
                       TO FPAYPAL-RF-MT-BALANCE-AP-9                            
                    MOVE FPAYPAL-RF-NFICORG TO W-NOMBRE                         
                    PERFORM CADRAGE-NUMERIQUE                                   
                    MOVE W-NOMBRE-9 TO L-NFICORG                                
              END-EVALUATE                                                      
           END-IF                                                               
           MOVE ST-PPL              TO L-FSTATUS                                
           .                                                                    
       CADRAGE-MONTANT                 SECTION.                                 
      * MISE EN FORME DU MONTANT                                                
      *  LE MONTANT A UNE LONGUEUR VARIABLE DANS LE FICHIER D'ENTREE            
      *  IL EST RECUPERE DANS UNE ZONE ALPHA, SEUL LES PREMIERS CARACT          
      *  SONT ALIMENTES DANS LA VARIABLE. ON CHERCHE LE 1ER CARACT              
      *  BLANC ET ON RECONSTITUE PARTIE ENTIERE ET PARTIE DECIMALE              
           SET FIN-CHAINE-CARAC-KO TO TRUE                                      
           MOVE 0 TO J                                                          
           PERFORM UNTIL J > J-MAX OR FIN-CHAINE-CARAC                          
               ADD 1 TO J                                                       
               MOVE W-MONTANT(J:1) TO W-CARAC                                   
               IF W-CARAC = SPACES                                              
                  SET FIN-CHAINE-CARAC TO TRUE                                  
               END-IF                                                           
           END-PERFORM                                                          
           IF J > J-MAX                                                         
      *       LE MONTANT EST TROP LONG (PLUS DE 11 CHIFFRES ) !!!               
              MOVE '?2'             TO IF000-CODANO                             
              MOVE 'C'              TO IF000-TYPANO                             
           ELSE                                                                 
              IF J < 4                                                          
      *       SI LE 1ER BLANC EST AU MAX LE 3EME CARACTERE, LE MONTANT          
      *       NE COMPRENDS QUE DES CENTIMES                                     
                IF J = 2                                                        
      *            SI LE 1ER BLANC EST LE 2E CARACTERE, ON A QUE                
      *            UNITES                                                       
                   MOVE 0 TO W-MONTANT-DEC(1:1)                                 
                   MOVE W-MONTANT(1:1) TO W-MONTANT-DEC(2:1)                    
                   MOVE 0 TO W-MONTANT-ENT                                      
                ELSE                                                            
                   MOVE W-MONTANT(1:J - 1) TO W-MONTANT-DEC                     
                   MOVE 0 TO W-MONTANT-ENT                                      
                END-IF                                                          
             ELSE                                                               
      *         SINON ON RECUPERE PARTIE ENTIERE ET PARTIE DECIMALE             
                MOVE W-MONTANT(1:J - 3) TO W-MONTANT-ENT                        
                MOVE W-MONTANT(J - 2:2) TO W-MONTANT-DEC                        
             END-IF                                                             
           END-IF                                                               
           MOVE W-MONTANT-EDIT-2 TO W-MONTANT-EDIT                              
           MOVE W-MONTANT-EDIT TO W-MONTANT-9                                   
           .                                                                    
       CADRAGE-NUMERIQUE               SECTION.                                 
      * MISE EN FORME D'UN NOMBRE                                               
      *  CERTAINS NOMBRES ONT UNE LONGUEUR VARIABLE DANS LE                     
      *  FICHIER D'ENTREE. ON LES RECUPERE DANS UNE ZONE ALPHA, SEULS           
      *  LES 1ER CARACTERES SONT ALIMENTES DANS LA VARIABLE. ON CHERCHE         
      *  LE 1ER CARACT BLANC DE LA VARIABLE ET ON FORMATE EN NUMERIQUE          
           SET FIN-CHAINE-CARAC-KO TO TRUE                                      
           MOVE 0 TO K                                                          
           PERFORM UNTIL K > K-MAX OR FIN-CHAINE-CARAC                          
               ADD 1 TO K                                                       
               MOVE W-NOMBRE(K:1) TO W-CARAC                                    
               IF W-CARAC = SPACES                                              
                  SET FIN-CHAINE-CARAC TO TRUE                                  
               END-IF                                                           
           END-PERFORM                                                          
           IF K > K-MAX                                                         
      *       LE NOMBRE EST TROP LONG (PLUS DE 7 CHIFFRES ) !!!                 
              MOVE '?3'             TO IF000-CODANO                             
           ELSE                                                                 
              MOVE W-NOMBRE(1:K - 1) TO W-NOMBRE-9                              
           END-IF                                                               
           .                                                                    
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
