      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00G.                                                     
       AUTHOR.  DSA015.                                                         
      ******************************************************************        
      * OCTOBRE 2011 - CREATION                                                 
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCOFINO   ASSIGN TO FCOFINO                                   
      *                     FILE STATUS IS ST-COF.                              
      *--                                                                       
           SELECT FCOFINO   ASSIGN TO FCOFINO                                   
                            FILE STATUS IS ST-COF                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FCOFINO                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FCOFINO-ENR                PIC X(150).                               
       WORKING-STORAGE SECTION.                                                 
       01 WS-BIF00G.                                                            
          05  W-DEVISE-EUR      PIC X(3)             VALUE 'EUR'.               
          05  W-MTNET            PIC S9(13)V99 COMP-3 VALUE ZERO.               
          05  W-BETDATC          PIC X(8)             VALUE 'BETDATC'.          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  L                  PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
          05  L                  PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
       77 ST-COF             PIC 99           VALUE 0.                          
           COPY FIFCOF.                                                         
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000          PIC X(192).                                        
      *                                                                         
       01  L-DATE            PIC X(8).                                          
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00G                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L                                        
           MOVE L-FIF000            TO DSECT-IF000                              
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FCOFINO                                                
              MOVE ST-COF           TO L-FSTATUS                                
           END-IF                                                               
           IF ST-COF = 00                                                       
              PERFORM LECTURE-FCOFINO                                           
           END-IF                                                               
           IF ST-COF = 00                                                       
              PERFORM TRAITEMENT-COFINOGA                                       
           END-IF                                                               
           IF ST-COF = 10                                                       
              MOVE ZERO TO L-NBRECUS                                            
              CLOSE FCOFINO                                                     
           END-IF                                                               
           GOBACK                                                               
           .                                                                    
       TRAITEMENT-COFINOGA             SECTION.                                 
           IF ST-COF NOT = 10                                                   
              IF COF-CODENR = 'E'                                               
                 PERFORM TRAITEMENT-ENTETE                                      
              END-IF                                                            
              IF COF-CODENR = 'D'                                               
                 PERFORM TRAITEMENT-FICHIER                                     
                 ADD  1                TO L-NBRECUS                             
                 MOVE DSECT-IF000      TO L-FIF000                              
              END-IF                                                            
              IF COF-CODENR = 'F'                                               
                 MOVE SPACES TO         L-FIF000                                
                 MOVE DSECT-COFINOGA TO COF-FIN                                 
                 MOVE COF-DCREAT-FIN TO L-DFICORG                               
                 MOVE COF-NBENR      TO L-NFICORG                               
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       TRAITEMENT-ENTETE               SECTION.                                 
           MOVE DSECT-COFINOGA         TO COF-ENTETE                            
           MOVE SPACES                 TO IF000-CODANO                          
           MOVE '1'                    TO GFDATA                                
           MOVE COF-DCREAT-TETE-JJ     TO GFJJ                                  
           MOVE COF-DCREAT-TETE-MM     TO GFMM                                  
           MOVE COF-DCREAT-TETE-SS     TO GFSS                                  
           MOVE COF-DCREAT-TETE-AA     TO GFAA                                  
           MOVE '1'                    TO GFDATA                                
           DISPLAY 'COF-DCREAT-TETE ' COF-DCREAT-TETE                           
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DREMISE                         
           ELSE                                                                 
              MOVE '05'                TO IF000-CODANO                          
              MOVE COF-DCREAT-TETE TO IF000-DREMISE                             
           END-IF                                                               
           PERFORM LECTURE-FCOFINO                                              
           .                                                                    
       TRAITEMENT-FICHIER              SECTION.                                 
           MOVE COF-DREMISE         TO GFSAMJ-0                                 
           MOVE '5'                 TO GFDATA                                   
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DOPER                           
                                          IF000-DFINANCE                        
                                          IF000-DREMISE                         
           ELSE                                                                 
              MOVE COF-DREMISE TO    IF000-DREMISE                              
                                     IF000-DOPER                                
              MOVE IF000-DOPER TO    IF000-DFINANCE                             
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           MOVE COF-NPORTEUR        TO IF000-NPORTEUR                           
           IF COF-TYPMT = 'CR' THEN                                             
              MOVE  '2'  TO  IF000-CTYPMVT                                      
           ELSE                                                                 
              IF COF-TYPMT = 'CO' THEN                                          
                 MOVE  '1'  TO  IF000-CTYPMVT                                   
              END-IF                                                            
           END-IF                                                               
           MOVE 1                   TO IF000-NFACTURES                          
           MOVE COF-IDCONTRAT(1:L)  TO IF000-NIDENT                             
           MOVE COF-NREMISE         TO IF000-NREMISE                            
           IF COF-SENS = 'D'                                                    
              COMPUTE IF000-MTBRUT = COF-MTBRUT                                 
              COMPUTE IF000-MTNET  = COF-MTNET                                  
              COMPUTE IF000-MTCOM  = COF-MTCOM                                  
           ELSE                                                                 
              COMPUTE IF000-MTBRUT = -1 * COF-MTBRUT                            
              COMPUTE IF000-MTNET  = -1 * COF-MTNET                             
              COMPUTE IF000-MTCOM  = -1 * COF-MTCOM                             
           END-IF                                                               
      *                                                                         
           MOVE COF-DEVISE           TO IF000-CDEVISE                           
           PERFORM CONTROLE-REJETS                                              
           .                                                                    
      *                                                                         
       CONTROLE-REJETS                 SECTION.                                 
           IF IF000-CODANO = SPACES                                             
              IF IF000-DFINANCE < IF000-DOPER                                   
                 MOVE '04'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-DREMISE < IF000-DOPER                                    
                 MOVE '06'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTBRUT = ZERO                                            
                 MOVE '08'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTNET = ZERO                                             
                 MOVE '09'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM                      
              IF IF000-MTNET NOT = W-MTNET                                      
                 MOVE '10'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF                                                               
      *                                                                         
           IF IF000-CDEVISE NOT = W-DEVISE-EUR                                  
              IF IF000-CODANO = SPACES                                          
                 MOVE '33'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       LECTURE-FCOFINO                 SECTION.                                 
           READ FCOFINO INTO   DSECT-COFINOGA                                   
           MOVE ST-COF TO L-FSTATUS                                             
           .                                                                    
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
