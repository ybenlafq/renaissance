      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00T.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      * MODIF 20/02/2001 -  DSA047                                     *        
      * AJOUT DU CONTROLE DE LA DEVISE :                               *        
      *  - SI LA DEVISE EST 'F' OU 'E'          -> ON ECRIT 'FRF' OU   *        
      *                                   'EUR' DANS FICHIER DE SORTIE *        
      *  - SI LA DEVISE N'EST PAS RENSEIGNEE    -> ON LA FORCE A 'FRF' *        
      *  - SI LA DEVISE N'EST NI 'F' NI 'E'     -> ON CREE 1 ANOMALIE  *        
      *                                            NON BLOQUANTE       *        
      * LA MODIFICATION EST REFERENCEE PAR 2002 DANS LA COL A          *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTELEPAI  ASSIGN TO FTELEPAI                                  
      *                     FILE STATUS IS ST-TEL.                              
      *--                                                                       
           SELECT FTELEPAI  ASSIGN TO FTELEPAI                                  
                            FILE STATUS IS ST-TEL                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FTELEPAI                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FTELEPAI-RECORD          PIC X(80).                                  
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF00T.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)    COMP   VALUE ZERO.                
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)    COMP   VALUE ZERO.                
      *--                                                                       
           05  L                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
2002       05  W-DEVISE-FRF      PIC X               VALUE 'F'.                 
2002       05  W-DEVISE-EUR      PIC X               VALUE 'E'.                 
2002       05  W-LIB-FRF         PIC X(3)            VALUE 'FRF'.               
2002       05  W-LIB-EUR         PIC X(3)            VALUE 'EUR'.               
           05  ST-TEL            PIC 99              VALUE ZERO.                
           05  W-ZERO            PIC X(20)           VALUE ALL '0'.             
           05  W-MTNET           PIC S9(9)V99 COMP-3 VALUE ZERO.                
           05  W-BETDATC         PIC X(8)            VALUE 'BETDATC'.           
       01  DSECT-TELEPAI.                                                       
           05  TEL-CODENR         PIC XXX.                                      
               88  TEL-TETE                   VALUE 'DEB'.                      
               88  TEL-FIN                    VALUE 'FIN'.                      
               88  TEL-DETAIL                 VALUE 'TLP'.                      
           05  TEL-DATE           PIC X(8).                                     
           05  TEL-NIDENT.                                                      
               10  TEL-NSOCIETE       PIC X(3).                                 
               10  TEL-NLIEU          PIC X(3).                                 
           05  TEL-NCDE           PIC X(7).                                     
           05  TEL-NOMCLIENT      PIC X(25).                                    
           05  TEL-CTYPMVT        PIC X.                                        
           05  TEL-MTBRUT         PIC S9(9)V99.                                 
           05  TEL-MTCOM          PIC S9(5)V99.                                 
           05  TEL-MTNETA.                                                      
               10  TEL-MTNET          PIC S9(9)V99.                             
           05  TEL-CDEVISE        PIC X.                                        
       01  TETE-TELEPAI.                                                        
           05  FILLER             PIC X(6).                                     
           05  TEL-DFICORG        PIC X(8).                                     
           05  FILLER             PIC X(66).                                    
       01  FIN-TELEPAI.                                                         
           05  FILLER             PIC X(7).                                     
           05  TEL-NFICORG        PIC 9(5).                                     
           05  FILLER             PIC X(68).                                    
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000         PIC X(192).                                         
       01  L-DATE           PIC X(8).                                           
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00T                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L.                                       
           MOVE L-FIF000            TO DSECT-IF000.                             
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FTELEPAI                                               
              MOVE ST-TEL           TO L-FSTATUS                                
           END-IF.                                                              
           IF ST-TEL = 00                                                       
              PERFORM LECTURE-FTELEPAI                                          
           END-IF.                                                              
           IF ST-TEL = 00                                                       
              PERFORM TRAITEMENT-TELEPAI                                        
           END-IF.                                                              
           IF ST-TEL = 10                                                       
              CLOSE FTELEPAI                                                    
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       TRAITEMENT-TELEPAI              SECTION.                                 
      *---------------------------------------                                  
           PERFORM LECTURE-FTELEPAI                                             
                   UNTIL ST-TEL = 10                                            
                   OR    TEL-DETAIL.                                            
           IF ST-TEL NOT = 10                                                   
              PERFORM TRAITEMENT-FICHIER-TEL                                    
              ADD  1                TO  L-NBRECUS                               
           END-IF.                                                              
           MOVE DSECT-IF000         TO L-FIF000.                                
      *---------------------------------------                                  
       TRAITEMENT-FICHIER-TEL          SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES                 TO IF000-NIDENT.                         
           MOVE TEL-NIDENT (1:L)       TO IF000-NIDENT.                         
           MOVE TEL-DATE               TO IF000-DOPER.                          
           MOVE TEL-DATE               TO IF000-DFINANCE.                       
           STRING TEL-NLIEU                                                     
                  TEL-NCDE (2:6)                                                
                  DELIMITED BY SIZE  INTO IF000-REFINTERNE.                     
           MOVE TEL-NOMCLIENT          TO IF000-NOMCLIENT.                      
           MOVE TEL-CTYPMVT            TO IF000-CTYPMVT.                        
           MOVE 1                      TO IF000-NFACTURES.                      
           COMPUTE IF000-MTBRUT = TEL-MTBRUT * 1.                               
           COMPUTE IF000-MTCOM  = TEL-MTCOM * 1.                                
           IF TEL-MTNETA = SPACES                                               
           OR TEL-MTNET  = ZERO                                                 
              MOVE ZERO           TO IF000-MTNET                                
           ELSE                                                                 
              COMPUTE IF000-MTNET  = TEL-MTNET * 1                              
           END-IF.                                                              
      *                                                                         
2002       PERFORM CONTROLE-DEVISE.                                             
           PERFORM CONTROLE-REJETS.                                             
      *                                                                         
      *---------------------------------------                                  
2002   CONTROLE-DEVISE                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
2002       IF TEL-CDEVISE = SPACES                                              
2002          OR TEL-CDEVISE = W-DEVISE-FRF THEN                                
2002          MOVE W-LIB-FRF TO IF000-CDEVISE                                   
2002       ELSE                                                                 
2002          IF TEL-CDEVISE = W-DEVISE-EUR THEN                                
2002             MOVE W-LIB-EUR TO IF000-CDEVISE                                
2002          END-IF                                                            
2002       END-IF.                                                              
2002  *                                                                         
2002   FIN-CONTROLE-DEVISE.              EXIT.                                  
      *                                                                         
      *---------------------------------------                                  
       CONTROLE-REJETS                 SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-CODANO.                            
           MOVE SPACES              TO IF000-TYPANO.                            
           IF TEL-NIDENT (1:L) NOT > W-ZERO (1:L)                               
           OR TEL-NIDENT (1:L) NOT NUMERIC                                      
              MOVE '01'             TO IF000-CODANO                             
              MOVE 'R'              TO IF000-TYPANO                             
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTBRUT = ZERO                                            
              OR IF000-MTBRUT = ZERO                                            
                 MOVE '08'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-MTNET = ZERO                                                
              COMPUTE IF000-MTNET = IF000-MTBRUT - IF000-MTCOM                  
              IF IF000-CODANO = SPACES                                          
                 MOVE '09'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM                      
              IF IF000-MTNET NOT = W-MTNET                                      
                 MOVE '10'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           MOVE IF000-DOPER         TO GFSAMJ-0                                 
           MOVE '5'                 TO GFDATA                                   
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT NOT = '1'                                                  
              MOVE L-DATE           TO IF000-DOPER                              
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           MOVE IF000-DFINANCE     TO GFSAMJ-0                                  
           MOVE '5'                TO GFDATA                                    
           PERFORM APPEL-BETDATC                                                
           IF GFVDAT NOT = '1'                                                  
              MOVE L-DATE          TO IF000-DFINANCE                            
              IF IF000-CODANO = SPACES                                          
                 MOVE '03'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           ELSE                                                                 
              IF IF000-DFINANCE < IF000-DOPER                                   
                 MOVE '04'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
2002       IF IF000-CDEVISE = W-DEVISE-FRF OR                                   
2002          IF000-CDEVISE = W-DEVISE-EUR THEN                                 
2002          NEXT SENTENCE                                                     
2002        ELSE                                                                
2002          IF IF000-CODANO = SPACES                                          
2002             MOVE '33'             TO IF000-CODANO                          
2002             MOVE 'C'              TO IF000-TYPANO                          
2002          END-IF                                                            
2002       END-IF.                                                              
      *                                                                         
      *---------------------------------------                                  
       LECTURE-FTELEPAI                SECTION.                                 
      *---------------------------------------                                  
           READ FTELEPAI INTO DSECT-TELEPAI.                                    
           MOVE ST-TEL              TO L-FSTATUS.                               
           IF ST-TEL = 00                                                       
              IF TEL-TETE                                                       
                 MOVE DSECT-TELEPAI    TO TETE-TELEPAI                          
                 MOVE TEL-DFICORG      TO L-DFICORG                             
                 MOVE ZERO             TO L-NBRECUS                             
                 MOVE ZERO             TO L-NBREJETS                            
              END-IF                                                            
              IF TEL-FIN                                                        
                 MOVE DSECT-TELEPAI    TO FIN-TELEPAI                           
                 MOVE TEL-NFICORG      TO L-NFICORG                             
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
