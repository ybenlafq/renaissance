      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00A.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      * MODIF EURO - DSA047 - 09 JANVIER 2002                          *        
      * ON MET 'EUR' DANS LE FICHIER DE SORTIE FIF000 ( 2002 EN COL 1 )*        
      ******************************************************************        
      * MODIF ECE - DSA015 - AVRIL 2010                                *        
      * AJOUT DU TRAITEMENT DES ENREG DE TYPE 30 (ENREG SUITE � REGUL  *        
      *  SUR LE COMPTE) - SIGNET 0410                                  *        
      ******************************************************************        
      * MODIF ECE - DSA015 - MAI 2011                                  *        
      * ON NE TRAITE PAS LES ENREG DE FRAIS (TYPE 30 ET MONTANT BRUT = *        
      * MONTANT DE COMMISSION) - SIGNET 0511                           *        
      ******************************************************************        
      * MODIF ECE - DSA015 - MAI 2013                                  *        
      * BUG SUR LE TRAITEMENT DES ENREG DE TYPE 30 : SI LA REMISE NE   *        
      * CONTIENT QUE CET ENREG POUR 1 LIEU, LE MOUVEMENT NE DOIT PAS   *        
      * ETRE REJETE                                                             
      *  ==> CORRECTION DE L'ALIMENTATION DES ZONES AVANT APPEL AU     *        
      *      TRAITEMENTS DE CONTROLE                                            
      * SIGNET 0513                                                             
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCARTAMX  ASSIGN TO FCARTAMX                                  
      *                     FILE STATUS IS ST-CAM.                              
      *--                                                                       
           SELECT FCARTAMX  ASSIGN TO FCARTAMX                                  
                            FILE STATUS IS ST-CAM                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FCARTAMX                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FCARTAMX-RECORD          PIC X(450).                                 
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF00A.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  L                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
           05  W-DEV             PIC X(03)            VALUE 'EUR'.              
           05  W-MTNET           PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  ST-CAM            PIC 99               VALUE ZERO.               
           05  W-ZERO            PIC X(20)            VALUE ALL '0'.            
           05  W-BETDATC         PIC X(8)             VALUE 'BETDATC'.          
       01  DSECT-CARTAMX.                                                       
      * ENTETE                                                                  
           05  CAM-TYPE-ENT        PIC X(6).                                    
               88  CAM-ENTETE          VALUE 'DFHDR '.                          
               88  CAM-FIN             VALUE 'DFTLR '.                          
           05  FILLER              PIC X(26).                                   
           05  CAM-CODENR          PIC 9.                                       
               88  CAM-RECAP           VALUE 1.                                 
               88  CAM-DETAIL          VALUE 2.                                 
           05  CAM-TYPE-ENR        PIC XX.                                      
               88  CAM-SOC             VALUE '10'.                              
               88  CAM-ROC             VALUE '60'.                              
0410           88  CAM-REGUL           VALUE '30'.                              
           05  FILLER              PIC X(415).                                  
       01  DFHDR-CARTAMX.                                                       
      * TYPE ENTETE                                                             
           05  FILLER              PIC X(6).                                    
           05  CAM-DFICORG         PIC X(8).                                    
           05  FILLER              PIC X(436).                                  
       01  DFTLR-CARTAMX.                                                       
      * TYPE FIN                                                                
           05  FILLER              PIC X(84).                                   
           05  CAM-NFICORG         PIC 9(7).                                    
           05  FILLER              PIC X(359).                                  
       01  SOC-CARTAMX.                                                         
      * TYPE SOC                                                                
           05  FILLER              PIC X(13).                                   
           05  CAM-DFINANCE        PIC X(8).                                    
           05  FILLER              PIC X.                                       
           05  CAM-NIDENT          PIC X(10).                                   
           05  FILLER              PIC X(8).                                    
           05  CAM-DREMISE         PIC X(8).                                    
           05  CAM-MTBRUT-SOC      PIC S9(13)V99.                               
           05  FILLER              PIC X(15).                                   
           05  CAM-MTCOM-SOC       PIC S9(13)V99.                               
           05  FILLER              PIC X(15).                                   
           05  CAM-MTNET-SOC       PIC S9(13)V99.                               
           05  FILLER              PIC X(62).                                   
           05  CAM-NFACTURES       PIC S9(5).                                   
           05  FILLER              PIC X(59).                                   
           05  CAM-NREMISE         PIC X(6).                                    
           05  FILLER              PIC X(195).                                  
      *                                                                         
       01  ROC-CARTAMX.                                                         
      * TYPE ROC                                                                
           05  FILLER              PIC X(40).                                   
           05  CAM-MTBRUT-ROC      PIC S9(9)V99.                                
           05  CAM-DOPER           PIC X(8).                                    
           05  FILLER              PIC X(30).                                   
           05  CAM-NPORTEUR        PIC X(15).                                   
           05  FILLER              PIC X(346).                                  
0410   01  REGUL-CARTAMX.                                                       
"     * TYPE REGUL (ENREG DE TYPE 30)                                           
"          05  FILLER              PIC X(13).                                   
"          05  CAM-DREGL           PIC X(08).                                   
"          05  FILLER              PIC X(01).                                   
"          05  CAM-NCONTRAT        PIC X(10).                                   
"          05  FILLER              PIC X(19).                                   
"          05  CAM-MTBRUT-REGUL    PIC S9(13)V99.                               
"          05  CAM-MTCOM-REGUL     PIC S9(13)V99.                               
"          05  FILLER              PIC X(15).                                   
0410       05  CAM-MTNET-REGUL     PIC S9(13)V99.                               
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01 L-FIF000              PIC X(192).                                     
      *                                                                         
       01 L-DATE                PIC X(8).                                       
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00A                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L.                                       
           MOVE L-FIF000            TO DSECT-IF000.                             
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FCARTAMX                                               
              MOVE ST-CAM           TO L-FSTATUS                                
           END-IF.                                                              
           IF ST-CAM = 00                                                       
              PERFORM LECTURE-FCARTAMX                                          
           END-IF.                                                              
           IF ST-CAM = 00                                                       
              PERFORM TRAITEMENT-CARTAMX                                        
           END-IF.                                                              
           IF ST-CAM = 10                                                       
              CLOSE FCARTAMX                                                    
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       TRAITEMENT-CARTAMX              SECTION.                                 
      *---------------------------------------                                  
           PERFORM LECTURE-FCARTAMX                                             
                   UNTIL ST-CAM = 10                                            
                   OR   (CAM-DETAIL AND (CAM-SOC OR CAM-ROC                     
0410                  OR CAM-REGUL ) )                                          
           IF ST-CAM NOT = 10                                                   
              MOVE SPACES           TO IF000-CODANO                             
              IF CAM-SOC                                                        
                 PERFORM TRAITEMENT-SOC                                         
              END-IF                                                            
              IF CAM-ROC                                                        
                 PERFORM TRAITEMENT-ROC                                         
              END-IF                                                            
0410          IF CAM-REGUL                                                      
"                PERFORM TRAITEMENT-REGUL                                       
0410          END-IF                                                            
              ADD  1                TO L-NBRECUS                                
              MOVE DSECT-IF000      TO L-FIF000                                 
           END-IF.                                                              
      *---------------------------------------                                  
       TRAITEMENT-SOC                  SECTION.                                 
      *---------------------------------------                                  
           MOVE DSECT-CARTAMX       TO SOC-CARTAMX.                             
           MOVE SPACES              TO IF000-NIDENT.                            
           MOVE CAM-NIDENT(1:L)     TO IF000-NIDENT.                            
           MOVE CAM-DREMISE         TO IF000-DREMISE.                           
           MOVE CAM-DREMISE         TO IF000-DOPER.                             
           MOVE CAM-NREMISE         TO IF000-NREMISE.                           
           MOVE CAM-DFINANCE        TO IF000-DFINANCE.                          
           MOVE ZERO                TO IF000-MTBRUT.                            
           COMPUTE IF000-MTCOM = CAM-MTCOM-SOC * -1.                            
           COMPUTE IF000-MTNET = CAM-MTNET-SOC.                                 
           MOVE CAM-NFACTURES       TO IF000-NFACTURES.                         
           MOVE '*'                 TO IF000-CTYPMVT.                           
           MOVE SPACES              TO IF000-NPORTEUR.                          
2002       MOVE W-DEV            TO IF000-CDEVISE.                              
           PERFORM CONTROLE-REJETS.                                             
      *---------------------------------------                                  
0410   TRAITEMENT-REGUL                SECTION.                                 
"     *---------------------------------------                                  
"          MOVE DSECT-CARTAMX       TO REGUL-CARTAMX.                           
"                                                                               
"          MOVE SPACES              TO IF000-NIDENT.                            
"          MOVE CAM-NCONTRAT  (1:L) TO IF000-NIDENT                             
0513                                   CAM-NIDENT                               
"          MOVE CAM-DREGL           TO IF000-DREMISE.                           
"          MOVE CAM-DREGL           TO IF000-DOPER.                             
"          MOVE CAM-NREMISE         TO IF000-NREMISE.                           
"          MOVE CAM-DFINANCE        TO IF000-DFINANCE.                          
"          MOVE ZERO                TO IF000-MTBRUT.                            
"          COMPUTE IF000-MTCOM  = CAM-MTCOM-REGUL * -1.                         
"          COMPUTE IF000-MTNET  = CAM-MTNET-REGUL                               
"          COMPUTE IF000-MTBRUT = CAM-MTBRUT-REGUL                              
"          MOVE ZERO                TO IF000-NFACTURES.                         
"          MOVE '*'                 TO IF000-CTYPMVT.                           
"          MOVE SPACES              TO IF000-NPORTEUR.                          
"          MOVE W-DEV            TO IF000-CDEVISE.                              
0513       MOVE CAM-MTBRUT-REGUL    TO CAM-MTBRUT-SOC                           
0513       MOVE CAM-MTCOM-REGUL     TO CAM-MTCOM-SOC                            
0513       MOVE CAM-MTNET-REGUL     TO CAM-MTNET-SOC                            
"                                                                               
0410       PERFORM CONTROLE-REJETS.                                             
      *---------------------------------------                                  
       TRAITEMENT-ROC                  SECTION.                                 
      *---------------------------------------                                  
           MOVE DSECT-CARTAMX       TO ROC-CARTAMX.                             
           MOVE SPACES              TO IF000-NIDENT.                            
           MOVE CAM-NIDENT (1:L)    TO IF000-NIDENT.                            
           MOVE CAM-DREMISE         TO IF000-DREMISE.                           
           MOVE CAM-NREMISE         TO IF000-NREMISE.                           
           MOVE CAM-DFINANCE        TO IF000-DFINANCE.                          
           MOVE CAM-DOPER           TO IF000-DOPER.                             
           COMPUTE IF000-MTBRUT = CAM-MTBRUT-ROC.                               
           MOVE ZERO                TO IF000-MTCOM.                             
           MOVE ZERO                TO IF000-MTNET.                             
           MOVE ZERO                TO IF000-NFACTURES.                         
           MOVE '*'                 TO IF000-CTYPMVT.                           
           MOVE CAM-NPORTEUR        TO IF000-NPORTEUR.                          
2002       MOVE W-DEV               TO IF000-CDEVISE.                           
           PERFORM CONTROLE-REJETS.                                             
      *---------------------------------------                                  
       CONTROLE-REJETS                 SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-CODANO.                            
           MOVE SPACES              TO IF000-TYPANO.                            
           IF CAM-NIDENT (1:L) NOT > W-ZERO (1:L)                               
           OR CAM-NIDENT (1:L) NOT NUMERIC                                      
              MOVE '01'             TO IF000-CODANO                             
              MOVE 'R'              TO IF000-TYPANO                             
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF CAM-MTBRUT-SOC = ZERO                                          
              OR CAM-MTBRUT-ROC = ZERO                                          
                 MOVE '08'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF CAM-MTNET-SOC = ZERO                                           
                 MOVE '09'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = CAM-MTBRUT-SOC + CAM-MTCOM-SOC                  
              IF CAM-MTNET-SOC NOT = W-MTNET                                    
                 MOVE '10'             TO IF000-CODANO                          
                 MOVE 'R'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           MOVE IF000-DOPER      TO GFSAMJ-0.                                   
           MOVE '5'              TO GFDATA.                                     
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT NOT = '1'                                                  
              MOVE L-DATE           TO IF000-DOPER                              
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
           MOVE IF000-DFINANCE     TO GFSAMJ-0.                                 
           MOVE '5'                TO GFDATA.                                   
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT NOT = '1'                                                  
              MOVE L-DATE             TO IF000-DFINANCE                         
              IF IF000-CODANO = SPACES                                          
                 MOVE '03'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           ELSE                                                                 
              IF IF000-DFINANCE < IF000-DOPER                                   
                 IF IF000-CODANO = SPACES                                       
                    MOVE '04'             TO IF000-CODANO                       
                    MOVE 'C'              TO IF000-TYPANO                       
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF CAM-NREMISE NOT > SPACES                                          
              MOVE '999999'            TO IF000-NREMISE                         
              IF IF000-CODANO = SPACES                                          
                 MOVE '07'             TO IF000-CODANO                          
                 MOVE 'C'              TO IF000-TYPANO                          
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       LECTURE-FCARTAMX                SECTION.                                 
      *---------------------------------------                                  
           READ FCARTAMX INTO DSECT-CARTAMX.                                    
           MOVE ST-CAM              TO L-FSTATUS.                               
           IF ST-CAM = 00                                                       
              IF CAM-ENTETE                                                     
                 MOVE DSECT-CARTAMX       TO DFHDR-CARTAMX                      
                 MOVE CAM-DFICORG         TO L-DFICORG                          
                 MOVE ZERO                TO L-NBRECUS                          
                 MOVE ZERO                TO L-NBREJETS                         
              END-IF                                                            
              IF CAM-FIN                                                        
                 MOVE DSECT-CARTAMX       TO DFTLR-CARTAMX                      
                 MOVE CAM-NFICORG         TO L-NFICORG                          
              END-IF                                                            
0511  *  SI ON EST SUR UN ENREGISTREMENT DE REGUL ET QUE LE MT BRUT             
"     *  EST EGAL AU MT DE COMMISSION, IL NE S'AGIT PAS D'UNE ANNUL             
"     *  MAIS DE LA FACTURATION DE FRAIS DIVERS QU'ON NE DOIT PAS               
"     *  COMPTABILISER DANS L'INTERFACE                                         
"             IF CAM-REGUL                                                      
"                MOVE DSECT-CARTAMX       TO REGUL-CARTAMX                      
"                IF CAM-MTBRUT-REGUL = CAM-MTCOM-REGUL                          
"                   MOVE '99'             TO   CAM-TYPE-ENR                     
"                END-IF                                                         
0511          END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
