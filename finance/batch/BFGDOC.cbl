      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BFGDOC.                                     
       AUTHOR. A-PHILIPPO   DSA005.                                             
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : NETTING                                         *        
      *  PROGRAMME   : BFGDOC                                          *        
      *  CREATION    : 25/01/1999                                      *        
      *  OBJET       : FORMATAGE FICHIER DES DOC DE FACTURES A EDITER  *        
      *                POUR TRAITEMENT ANNUEL DU SRP                   *        
      *                ----------------------------------------------  *        
      *                                                                *        
      *  FICHIER ENTREE : FFGEDG  LG = 175                             *        
      *                                                                *        
      *  FICHIER SORTIE : FFGDOC  LG = 133                             *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FFGEDG      ASSIGN TO FFGEDG.                                 
      *--                                                                       
           SELECT FFGEDG      ASSIGN TO FFGEDG                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FFGDOC      ASSIGN TO FFGDOC.                                 
      *--                                                                       
           SELECT FFGDOC      ASSIGN TO FFGDOC                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *----FICHIER FFGEDG                                                       
       FD  FFGEDG                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FFGEDG-ENREG.                                                        
         02 FFGEDG-CLE.                                                         
1          05 FILLER              PIC X(14).                                    
15         05 FFGEDG-CLE-NSOC     PIC X(03).                                    
18         05 FILLER              PIC X(25).                                    
         02 FFGEDG-DONNEES.                                                     
43         05 FFGEDG-ASA          PIC X(01).                                    
44         05 FFGEDG-NSOC         PIC X(03).                                    
47         05 FILLER              PIC X(129).                                   
      *-------LG = 175                                                          
      *                                                                         
      *----FICHIER DE SORTIE, FFGDOC                                            
       FD  FFGDOC                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FFGDOC-ENREG.                                                        
1          05 FFGDOC-ASA          PIC X(01).                                    
2          05 FFGDOC-NSOC         PIC X(03).                                    
5          05 FILLER              PIC X(129).                                   
      *-------LG = 133                                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY ABENDCOP.                                                       
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL                
      *--------------------------------------------------------------*          
           COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
      *                                                                         
      *****COPY WORKDATC.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES AU PROGRAMME                                         
      *--------------------------------------------------------------*          
      *                                                                         
       01 FILLER.                                                               
          02 CPT-FFGEDG         PIC S9(7) COMP-3  VALUE 0.                      
          02 CPT-FFGDOC         PIC S9(7) COMP-3  VALUE 0.                      
      *                                                                         
          02 FLAG-FFGEDG        PIC X(01) VALUE '0'.                            
           88 SUITE-FFGEDG                VALUE '0'.                            
           88 FIN-FFGEDG                  VALUE '1'.                            
      *                                                                         
      *                                                                         
          02 CALL-UTILITIES.                                                    
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *      05 ABEND    PIC X(08) VALUE 'ABEND   '.                            
      *--                                                                       
             05 MWABEND    PIC X(11) VALUE 'MW-ABEND   '.                         
      *}                                                                        
      *                                                                         
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                           
      *--------------------------------------------------------------*          
       01 FILLER   PIC X(16)    VALUE '*** Z-INOUT ****'.                       
       01 Z-INOUT  PIC X(4096)  VALUE SPACE.                                    
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                           
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
           COPY SYKWMONT.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BFGDOC.                                                           
      *--------------                                                           
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
      *------------------------------                                           
       MODULE-ENTREE      SECTION.                                              
      *------------------------------                                           
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT  FFGEDG                                                   
                OUTPUT FFGDOC.                                                  
      *                                                                         
      *----LECTURE / VERIFICATION FICHIER FFGEDG                                
           MOVE '0'  TO  FLAG-FFGEDG.                                           
           READ FFGEDG                                                          
             AT END                                                             
                DISPLAY '*** FFGEDG VIDE  *** '                                 
                DISPLAY '--> RIEN A TRAITER'                                    
                SET FIN-FFGEDG  TO  TRUE                                        
             NOT END                                                            
                COMPUTE CPT-FFGEDG = CPT-FFGEDG + 1                             
           END-READ.                                                            
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
      *                                                                         
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *--------------------------------                                         
       MODULE-TRAITEMENT       SECTION.                                         
      *--------------------------------                                         
      *                                                                         
           PERFORM UNTIL FIN-FFGEDG                                             
              MOVE FFGEDG-DONNEES   TO  FFGDOC-ENREG                            
      *                                                                         
              IF FFGEDG-ASA = '1'                                               
                 MOVE FFGEDG-CLE-NSOC  TO  FFGDOC-NSOC                          
              END-IF                                                            
      *                                                                         
      *                                                                         
      *-------ECRITURE FICHIER DE SORTIE, FFGDOC                                
              WRITE FFGDOC-ENREG                                                
              COMPUTE CPT-FFGDOC = CPT-FFGDOC + 1                               
      *                                                                         
      *-------LECTURE ENREG SUIVANT                                             
              READ FFGEDG                                                       
                AT END                                                          
                   SET FIN-FFGEDG  TO  TRUE                                     
                NOT END                                                         
                   COMPUTE CPT-FFGEDG = CPT-FFGEDG + 1                          
              END-READ                                                          
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT.   EXIT.                                           
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                 M O D U L E - S O R T I E                     *         
      *****************************************************************         
      *---------------------------                                              
       MODULE-SORTIE      SECTION.                                              
      *---------------------------                                              
      *                                                                         
      *----FERMETURE DES FICHIERS                                               
           CLOSE  FFGEDG  FFGDOC.                                               
      *                                                                         
      *                                                                         
           DISPLAY '********************************************'.              
           DISPLAY '*** PROGRAMME BFGDOC TERMINE NORMALEMENT ***'.              
           DISPLAY '********************************************'.              
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES FFGEDG    : ' CPT-FFGEDG.                  
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE ECRITURES FFGDOC   : ' CPT-FFGDOC.                  
           DISPLAY '*'.                                                         
           DISPLAY '*'.                                                         
           DISPLAY '********************************************'.              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN .                                                           
      *--                                                                       
           EXIT PROGRAM .                                                       
      *}                                                                        
      *                                                                         
      *---------------------------                                              
       SORTIE-ANORMALE    SECTION.                                              
      *---------------------------                                              
      *----FERMETURE DES FICHIERS                                               
           CLOSE  FFGEDG  FFGDOC.                                               
      *                                                                         
           DISPLAY '*************************************'.                     
           DISPLAY '**** PROGRAMME BFGDOC INTERROMPU ****'.                     
           DISPLAY '*************************************'.                     
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES  FFGEDG : ' CPT-FFGEDG.                    
           DISPLAY '*  NBRE ECRITURES FFGDOC : ' CPT-FFGDOC.                    
           DISPLAY '*'.                                                         
           DISPLAY '*************************************'.                     
           MOVE 'BFGDOC'    TO  ABEND-PROG.                                     
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND   USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL  MWABEND   USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.    EXIT.                                            
      *                                                                         
      *--------------------------------------------------------------*          
      *     MODULES D'ABANDON                                                   
      *--------------------------------------------------------------*          
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
       12-COPY SECTION. CONTINUE. COPY SYKPMONT.                                
      *--------------------------------------------------------------*          
      *     MODULES DB2                                                         
      *--------------------------------------------------------------*          
   ****AIDA-SQL-04 SECTION. CONTINUE. COPY SYKSRETC.                            
      *                                                                         
      *                                                                         
