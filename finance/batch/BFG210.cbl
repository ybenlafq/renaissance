      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                BFG210.                                       
       AUTHOR.                    DSA011-FC.                                    
      *================================================================*        
      *                                                                *        
      *       EDITION DES GENERATIONS DE COMPTABILISATION              *        
      *          FACTURATION INTER-SOCIETES                            *        
      *                                                                *        
      *================================================================*        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFGCTA  ASSIGN TO FFGCTA.                                    
      *--                                                                       
            SELECT FFGCTA  ASSIGN TO FFGCTA                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IFG210  ASSIGN TO IFG210.                                    
      *--                                                                       
            SELECT IFG210  ASSIGN TO IFG210                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *     F I C H I E R   D A T E   J O U R                                   
      * -------------------------------------------------------                 
       FD   FDATE                                                               
            RECORDING F LABEL RECORD STANDARD.                                  
       01  FDATE-ENREG                 PIC  X(80).                              
      *     F I C H I E R   D E S   A N O M A L I E S                           
      * ----------------------------------------------                          
       FD  FFGCTA                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FFGCTA-ENREG PIC X(180).                                             
      *                                                                         
      *     F I C H I E R   I M P R E S S I O N                                 
      * ---------------------------------------                                 
       FD  IFG210                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  IFG210-ENREG    PIC X(133).                                          
      *                                                                         
      *================================================================*        
       WORKING-STORAGE            SECTION.                                      
      *================================================================*        
      *                                                                         
      * MODULES APPELES PAR CALL                                                
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                   PIC X(08) VALUE 'ABEND   '.                  
      *--                                                                       
       77  MWABEND             PIC X(11) VALUE 'MW-ABEND   '.               
      *}                                                                        
       77  BETDATC                 PIC X(08) VALUE 'BETDATC '.                  
      *77  SPDATDB2                PIC X(08) VALUE 'SPDATDB2'.                  
      *77  MGA001                  PIC X(08) VALUE 'MGA001  '.                  
      *                                                                         
       01  VARIABLES-NUM.                                                       
           05  I                   PIC S9(3) COMP-3.                            
           05  W-L                 PIC S9(3) COMP-3.                            
           05  W-P                 PIC S9(3) COMP-3.                            
           05  W-NLP               PIC S9(3) COMP-3.                            
      *    05  W-NPP               PIC S9(3) COMP-3.                            
           05  W-LUS-CTA           PIC S9(7) COMP-3.                            
           05  W-DEBIT             PIC S9(11)V99 COMP-3.                        
           05  W-CREDIT            PIC S9(11)V99 COMP-3.                        
       01  VARIABLES-ALPHA.                                                     
           05  W-NSOCCOMPT         PIC X(3).                                    
           05  W-NETAB             PIC X(3).                                    
           05  W-NJRN              PIC X(10).                                   
           05  W-NPIECE            PIC X(10).                                   
           05  W-NSOCE             PIC X(3).                                    
           05  W-CCHRONO           PIC X(2).                                    
           05  W-NLIEUE            PIC X(3).                                    
           05  W-NSOCR             PIC X(3).                                    
           05  W-NLIEUR            PIC X(3).                                    
           05  W-NUMF              PIC X(7).                                    
           05  W-TYPP              PIC X(3).                                    
           05  W-NATF              PIC X(5).                                    
           05  W-DATF              PIC X(8).                                    
           05  W-DECH              PIC X(5).                                    
           05  W-DCTA              PIC X(8).                                    
           05  W-NERR              PIC X(4).                                    
           05  W-LPIECE            PIC X(20).                                   
       01  VARIABLES-TEST.                                                      
           05  W-FIN-CTA           PIC 9.                                       
             88  FIN-CTA VALUE 1.                                               
           05  W-PAGE-OUVERTE      PIC 9.                                       
             88  PAGE-OUVERTE VALUE 0.                                          
             88  PAGE-FERMEE  VALUE 1.                                          
      *================================================================*        
       COPY SWFGCTA.                                                            
       COPY ABENDCOP.                                                           
       COPY WORKDATC.                                                           
      *================================================================*        
      *---------------------------------------------------------------          
       01      IFG210-DSECT.                                                    
           03  IFG210-ASA           PIC  X(01).                                 
           03  IFG210-LIG           PIC  X(132).                                
       01  E00.                                                                 
           05 FILLER   PIC X(24) VALUE 'I F G 2 1 0'.                           
           05 FILLER   PIC X(79) VALUE                                          
           'COMPTABILISATION DES ECRITURES DE LA BASE DE DONNEES GROUPE'        
                                                                       .        
           05 FILLER   PIC X(09) VALUE 'EDITE LE '.                             
           05 E00-DATE PIC X(12).                                               
           05 FILLER   PIC X(04) VALUE 'PAGE'.                                  
           05 E00-PAGE PIC ZZZ9.                                                
       01  E01.                                                                 
           05 FILLER   PIC X(35) VALUE SPACE.                                   
GCT        05 E01-LIB  PIC X(50).                                               
       01  E02.                                                                 
           05 FILLER    PIC X(29) VALUE 'SOCIETE / ETABLISSEMENT   :'.          
           05 E02-NSOC  PIC X(04) VALUE SPACE.                                  
           05 E02-NETAB PIC X(03) VALUE SPACE.                                  
       01  E03.                                                                 
           05 FILLER    PIC X(28) VALUE 'EXERCICE / PERIODE / JOUR :'.          
           05 E03-NEXER PIC X(05).                                              
           05 E03-NPER  PIC X(04).                                              
           05 E03-DCTA  PIC X(10).                                              
           05 FILLER    PIC X(19) VALUE SPACE.                                  
           05 FILLER    PIC X(29) VALUE 'SOCIETE EN CORRESPONDANCE :'.          
           05 E03-NSOCC PIC X(03).                                              
      *                                0        1         2         3           
      *                                123456789012345678901234567890           
       01  E04.                                                                 
           05 FILLER  PIC X(132) VALUE ALL '-'.                                 
       01  E05.                                                                 
           05 FILLER  PIC X(30) VALUE '! LIEU ! LIEU !        FACTURA'.         
           05 FILLER  PIC X(30) VALUE 'TION        !  JOURNAL  ! N� P'.         
           05 FILLER  PIC X(30) VALUE 'IECE  !COMPTE    ! SECT.! RUBR'.         
           05 FILLER  PIC X(30) VALUE '.!VENT.!TVA!           MONTANT'.         
           05 FILLER  PIC X(12) VALUE 'S          !'.                           
       01  E06.                                                                 
           05 FILLER  PIC X(30) VALUE '! EMET.! REC. ! TYP NUMERO NAT'.         
           05 FILLER  PIC X(30) VALUE 'URE   DATE  !           !     '.         
           05 FILLER  PIC X(30) VALUE '      !    /TIERS!      !     '.         
           05 FILLER  PIC X(30) VALUE ' !     !   !         DEBIT    '.         
           05 FILLER  PIC X(12) VALUE '    CREDIT !'.                           
       01  E07.                                                                 
           05 FILLER  PIC X(30) VALUE '!------+------+---------------'.         
           05 FILLER  PIC X(30) VALUE '------------+-----------+-----'.         
           05 FILLER  PIC X(30) VALUE '------+----------+------+-----'.         
           05 FILLER  PIC X(30) VALUE '-+-----+---+------------------'.         
           05 FILLER  PIC X(12) VALUE '-----------!'.                           
       01  E07B       PIC X(132).                                               
       01  D01.                                                                 
           05 FILLER     PIC X(01).                                             
           05 D01-NSOCE  PIC X(03).                                             
           05 D01-NLIEUE PIC X(03).                                             
           05 D01-LIB1   PIC X(01).                                             
           05 D01-NSOCR  PIC X(03).                                             
           05 D01-NLIEUR PIC X(03).                                             
           05 D01-LIB2   PIC X(02).                                             
           05 D01-LPIECE.                                                       
            7 D01-TYPP   PIC X(03).                                             
            7 D01-NUMF   PIC X(08).                                             
            7 D01-NATF   PIC X(06).                                             
            7 D01-DATF   PIC X(08).                                             
           05 FILLER     PIC X(03).                                             
           05 D01-NJRN   PIC X(10).                                             
           05 FILLER     PIC X(01).                                             
           05 D01-NPIECE PIC X(10).                                             
           05 FILLER     PIC X(2).                                              
           05 D01-COMPTE PIC X(7).                                              
           05 D01-NTIERS PIC X(3).                                              
           05 FILLER     PIC X(1).                                              
           05 D01-SECT   PIC X(6).                                              
           05 FILLER     PIC X(1).                                              
           05 D01-RUBR   PIC X(6).                                              
           05 FILLER     PIC X(1).                                              
           05 D01-CVENT  PIC X(5).                                              
           05 FILLER     PIC X(2).                                              
           05 D01-TXTVA  PIC X(1).                                              
           05 FILLER     PIC X(2).                                              
           05 D01-DEB.                                                          
            8 D01-DEBIT  PIC Z(10)9,99 BLANK WHEN ZERO.                         
           05 D01-CRE.                                                          
            8 D01-CREDIT PIC Z(10)9,99 BLANK WHEN ZERO.                         
           05 FILLER     PIC X(02).                                             
      *================================================================*        
      *    C  O  D  E  S    R  E  T  O  U  R  S    S  Q  L             *        
      *================================================================*        
       01  CODE-RETOUR             PIC  X(01)  VALUE  '0'.                      
           88  TROUVE              VALUE  '0'.                                  
           88  NON-TROUVE          VALUE  '1'.                                  
      *    88  EXISTE-DEJA         VALUE  '2'.                                  
      *    88  DOUBLE              VALUE  '7'.                                  
      *                                                                         
      *================================================================*        
      *    D E S C R I P T I O N     D E S     T A B L E S     D B 2  *         
      *================================================================*        
      *                                                                         
      *    EXEC SQL INCLUDE SQLCA    END-EXEC.                                  
      *                                                                         
      *    EXEC SQL INCLUDE          END-EXEC.                                  
      ******************************************************************        
      *                                                                         
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *                                                                         
      *                                                                         
      ******************************************************************        
      *===============================================================*         
           PERFORM DEBUT                                                        
      *                                                                         
           IF FIN-CTA                                                           
              PERFORM SAUT1                                                     
           ELSE                                                                 
              PERFORM TRAITEMENT-CTA UNTIL FIN-CTA                              
           END-IF                                                               
      *                                                                         
           MOVE '********** FIN DE LISTE IFG210 **********'                     
                    TO IFG210-LIG                                               
           MOVE '0' TO IFG210-ASA                                               
           PERFORM     IFG210-WRITE                                             
           PERFORM FIN-NORMALE                                                  
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP    RUN.                                                         
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *===============================================================*         
      *===============================================================*         
       DEBUT SECTION.                                                           
      *===============================================================*         
           OPEN INPUT   FDATE                                                   
                        FFGCTA                                                  
                OUTPUT  IFG210.                                                 
           DISPLAY '*=============================================*'.           
           DISPLAY '*          DEBUT DU PROGRAMME BFG210          *'.           
           DISPLAY '*=============================================*'.           
      *--- INIT GENERALES WORK                                                  
           INITIALIZE VARIABLES-NUM                                             
                      VARIABLES-ALPHA                                           
                      VARIABLES-TEST                                            
      *--- VERIFICATION DATE DE TRAITEMENT                                      
           READ FDATE   INTO GFJJMMSSAA END                                     
                MOVE '*** FDATE VIDE *** ' TO ABEND-MESS                        
                PERFORM FIN-ANORMALE                                            
           END-READ                                                             
           MOVE '1' TO GFDATA                                                   
           CALL BETDATC USING WORK-BETDATC                                      
           IF   GFVDAT NOT = '1'                                                
                MOVE 'FDATE REJETEE PER BETDATC' TO ABEND-MESS                  
                PERFORM FIN-ANORMALE                                            
           END-IF                                                               
           DISPLAY '*  DATE DE TRAITEMENT : ' GFJMSA-5                          
           DISPLAY '*'                                                          
      *--- LECTURE INITIALE FFGCTA                                              
           READ FFGCTA INTO FFGCTA-DSECT                                        
                END           MOVE 'FICHIER FFGCTA VIDE ' TO ABEND-MESS         
                              PERFORM FIN-ANORMALE                              
            NOT END                                                             
              EVALUATE FFGCTA-CNOMPGRM                                          
                WHEN 'FG100'  MOVE '  GENERATION DES REGLEMENTS'                
                                                TO E01-LIB                      
                WHEN 'FG110'  MOVE 'COMPTABILISATION DES FACTURES '             
                                                TO E01-LIB                      
GCT             WHEN 'FSIMU'                                                    
GCT               MOVE 'COMPTABILISATION DES FACTURES - SIMULATION GCT'         
GCT                                             TO E01-LIB                      
                WHEN OTHER    MOVE 'PROGRAMME INCONNU ' TO ABEND-MESS           
                              PERFORM FIN-ANORMALE                              
                END-EVALUATE                                                    
           END-READ.                                                            
           READ FFGCTA INTO FFGCTA-DSECT                                        
                END    SET FIN-CTA TO TRUE                                      
           END-READ.                                                            
      *--- PRECHARGEMENT DES ZONES D'ENTETE                                     
           MOVE     GFJMSA-5   TO E00-DATE                                      
           MOVE     E07        TO E07B                                          
           INSPECT  E07B REPLACING ALL '-' BY  ' '.                             
           INSPECT  E07B REPLACING ALL '+' BY  '!'.                             
           MOVE     E07B  TO D01.                                               
      *===============================================================*         
      *===============================================================*         
       TRAITEMENT-CTA SECTION.                                                  
           MOVE       FFGCTA-NSOCCOMPT    TO E02-NSOC                           
           MOVE       FFGCTA-NETAB        TO E02-NETAB                          
           MOVE       FFGCTA-NSOCCOR      TO E03-NSOCC                          
           MOVE       FFGCTA-NEXERCICE    TO E03-NEXER                          
           MOVE       FFGCTA-NPERIODE     TO E03-NPER                           
           STRING     FFGCTA-DCOMPTA(7:2) '/'                                   
                      FFGCTA-DCOMPTA(5:2) '/'                                   
                      FFGCTA-DCOMPTA(1:4) '/'                                   
                      DELIMITED SIZE    INTO E03-DCTA                           
           MOVE       FFGCTA-DCOMPTA      TO W-DCTA                             
      *                                                                         
           PERFORM    SAUT-PAGE                                                 
      *                                                                         
           MOVE       FFGCTA-NSOCCOMPT    TO W-NSOCCOMPT                        
           MOVE       FFGCTA-NETAB        TO W-NETAB                            
      *                                                                         
           PERFORM UNTIL FIN-CTA                                                
                   OR FFGCTA-NSOCCOMPT NOT =   W-NSOCCOMPT                      
                   OR FFGCTA-NETAB     NOT =   W-NETAB                          
                   OR FFGCTA-NSOCCOR   NOT = E03-NSOCC                          
                   OR FFGCTA-NEXERCICE NOT = E03-NEXER                          
                   OR FFGCTA-NPERIODE  NOT = E03-NPER                           
                   OR FFGCTA-DCOMPTA   NOT =   W-DCTA                           
              MOVE    FFGCTA-NSOCCOMPT TO    W-NSOCCOMPT                        
              MOVE    FFGCTA-NJRN      TO    W-NJRN                             
              MOVE    FFGCTA-NPIECE    TO    W-NPIECE                           
              MOVE    FFGCTA-NSOCORIG  TO    W-NSOCE                            
              MOVE    FFGCTA-NLIEUORIG TO    W-NLIEUE                           
              MOVE    FFGCTA-NSOCDEST  TO    W-NSOCR                            
              MOVE    FFGCTA-NLIEUDEST TO    W-NLIEUR                           
              MOVE    FFGCTA-NUMFACT   TO    W-NUMF                             
              MOVE    FFGCTA-CTYPPIECE TO    W-TYPP                             
              MOVE    FFGCTA-NATFACT   TO    W-NATF                             
              MOVE    FFGCTA-LIBELLE   TO    W-LPIECE                           
              STRING  FFGCTA-DPIECE   (7:2) '/'                                 
                      FFGCTA-DPIECE   (5:2) '/'                                 
                      FFGCTA-DPIECE   (3:2)                                     
                      DELIMITED SIZE INTO    W-DATF                             
              STRING  FFGCTA-DECHEANC (7:2) '/'                                 
                      FFGCTA-DECHEANC (5:2)                                     
                      DELIMITED SIZE INTO    W-DECH                             
              MOVE    0                TO    W-NLP                              
                                             W-DEBIT                            
                                             W-CREDIT                           
      *       CHANGEMENT DE FACTURE                                             
              PERFORM UNTIL  FIN-CTA                                            
                   OR FFGCTA-NSOCCOMPT NOT = W-NSOCCOMPT                        
                   OR FFGCTA-NJRN      NOT = W-NJRN                             
                   OR FFGCTA-NPIECE    NOT = W-NPIECE                           
                 IF  W-L > 62                                                   
                     IF PAGE-OUVERTE                                            
                        MOVE E04   TO IFG210-LIG                                
                        PERFORM IFG210-WRITE                                    
                     ELSE                                                       
                        SET PAGE-OUVERTE TO TRUE                                
                     END-IF                                                     
                     PERFORM SAUT-PAGE                                          
                     MOVE 1  TO W-NLP                                           
                 ELSE                                                           
                     ADD  1  TO W-NLP                                           
                 END-IF                                                         
                 EVALUATE W-NLP                                                 
                    WHEN 1                                                      
                      MOVE W-NSOCE     TO    D01-NSOCE                          
                      MOVE W-NLIEUE    TO    D01-NLIEUE                         
                      MOVE W-NSOCR     TO    D01-NSOCR                          
                      MOVE W-NLIEUR    TO    D01-NLIEUR                         
                      MOVE W-NUMF      TO    D01-NUMF                           
                      MOVE W-TYPP      TO    D01-TYPP                           
                      MOVE W-NATF      TO    D01-NATF                           
                      MOVE W-DATF      TO    D01-DATF                           
                      MOVE FFGCTA-NJRN    TO D01-NJRN                           
                      MOVE FFGCTA-NPIECE  TO D01-NPIECE                         
                    WHEN 2                                                      
                      MOVE SPACE       TO    D01-LIB1                           
                      STRING 'ECHEANCE : ' W-DECH                               
                          DELIMITED SIZE INTO D01-LPIECE                        
                    WHEN OTHER                                                  
                      MOVE SPACE       TO    D01-LIB1                           
                                             D01-LIB2                           
                 END-EVALUATE                                                   
                 MOVE FFGCTA-COMPTE    TO    D01-COMPTE                         
                 MOVE FFGCTA-NTIERS    TO    D01-NTIERS                         
                 MOVE FFGCTA-SECTION   TO    D01-SECT                           
                 MOVE FFGCTA-RUBRIQUE  TO    D01-RUBR                           
                 MOVE FFGCTA-CVENT     TO    D01-CVENT                          
                 MOVE FFGCTA-CTAUXTVA  TO    D01-TXTVA                          
                 IF   FFGCTA-SENS = 'D'                                         
                      MOVE FFGCTA-MONTANT TO D01-DEBIT                          
                      ADD  FFGCTA-MONTANT TO   W-DEBIT                          
                 ELSE                                                           
                      MOVE FFGCTA-MONTANT TO D01-CREDIT                         
                      ADD  FFGCTA-MONTANT TO   W-CREDIT                         
                 END-IF                                                         
                 MOVE   D01            TO IFG210-LIG                            
                 PERFORM                  IFG210-WRITE                          
                 MOVE E07B        TO D01                                        
                 READ FFGCTA INTO FFGCTA-DSECT                                  
                      END SET FIN-CTA TO TRUE                                   
                 END-READ                                                       
                 ADD    1  TO W-LUS-CTA                                         
              END-PERFORM                                                       
      *                                                                         
              MOVE   E07B TO D01                                                
              MOVE   ALL '-' TO D01-DEB                                         
                                D01-CRE                                         
              MOVE   SPACE   TO D01-LIB1                                        
                                D01-LIB2                                        
              MOVE   D01     TO IFG210-LIG                                      
              PERFORM           IFG210-WRITE                                    
      *                                                                         
              MOVE   W-DEBIT  TO D01-DEBIT                                      
              MOVE   W-CREDIT TO D01-CREDIT                                     
              MOVE   D01      TO IFG210-LIG                                     
              PERFORM            IFG210-WRITE                                   
              MOVE   E07B TO D01                                                
      *                                                                         
              MOVE   E04            TO IFG210-LIG                               
              PERFORM                  IFG210-WRITE                             
              SET PAGE-FERMEE TO TRUE                                           
           END-PERFORM.                                                         
      *===============================================================*         
       SAUT-PAGE SECTION.                                                       
      *===============================================================*         
           IF W-L > 50                                                          
              OR W-NSOCCOMPT NOT = FFGCTA-NSOCCOMPT                             
              OR W-NETAB     NOT = FFGCTA-NETAB                                 
              PERFORM SAUT1                                                     
           END-IF                                                               
           MOVE '0'     TO IFG210-ASA                                           
           MOVE E03     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE '-'     TO IFG210-ASA                                           
           MOVE E04     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE ' '     TO IFG210-ASA                                           
           MOVE E05     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE E06     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE E07     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE.                                        
       SAUT1 SECTION.                                                           
           ADD 1 TO W-P                                                         
           MOVE     W-P TO E00-PAGE                                             
           MOVE '1'     TO IFG210-ASA                                           
           MOVE E00     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE ' '     TO IFG210-ASA                                           
           MOVE E01     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE                                         
           MOVE '0'     TO IFG210-ASA                                           
           MOVE E02     TO IFG210-LIG                                           
           PERFORM         IFG210-WRITE.                                        
      *===============================================================*         
      *===============================================================*         
       IFG210-WRITE SECTION.                                                    
      *===============================================================*         
           WRITE    IFG210-ENREG FROM IFG210-DSECT                              
           EVALUATE IFG210-ASA                                                  
               WHEN '1'        MOVE 1 TO W-L                                    
               WHEN ' '        ADD  1 TO W-L                                    
               WHEN '0'        ADD  2 TO W-L                                    
               WHEN '-'        ADD  3 TO W-L                                    
           END-EVALUATE.                                                        
      *===============================================================*         
      *===============================================================*         
       FIN-NORMALE SECTION.                                                     
      *===============================================================*         
           PERFORM FERMETURE-FICHIERS.                                          
           DISPLAY '*'                                                          
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           DISPLAY '*      FIN NORMALE DU PROGRAMME BFG210'                     
           DISPLAY '*================================================='         
           DISPLAY '*'                                                          
           DISPLAY '* LIGNES LUES DANS FFGCTA      : ' W-LUS-CTA                
           DISPLAY '*'                                                          
           DISPLAY '* NB PAGES DANS IFG210         : ' W-P                      
           DISPLAY '*'                                                          
           DISPLAY '*================================================='.        
      *================================================================*        
      *===============================================================*         
       FIN-ANORMALE SECTION.                                                    
      *===============================================================*         
           PERFORM FERMETURE-FICHIERS                                           
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           DISPLAY '*'                                                          
           DISPLAY '*** ANOMALIE PENDANT EXECUTION DU BFG210 ***'               
           DISPLAY '*** ' ABEND-MESS                                            
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           MOVE   'BFG210'  TO ABEND-PROG.                                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL    ABEND USING ABEND-PROG ABEND-MESS.                           
      *--                                                                       
           CALL    MWABEND USING ABEND-PROG ABEND-MESS.                        
      *}                                                                        
      *================================================================*        
      *================================================================*        
       FERMETURE-FICHIERS SECTION.                                              
      *================================================================*        
           CLOSE FDATE                                                          
                 FFGCTA                                                         
                 IFG210.                                                        
      *================================================================*        
