      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BFI097.                                                 
       AUTHOR.     AD - APSIDE.                                                 
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : PURGE DES TABLES FIxx                           *        
      *  INTERFACE   : /                                               *        
      *  PERIODICITE : MENSUEL                                         *        
      *  PROGRAMME   : BFI097                                          *        
      *  CREATION    : 31/07/2015                                      *        
      *  FONCTION    : REFORMATAGE DU FICHIER SAP AVANT ENVOI A CAFOM  *        
      *                POUR RAJOUTER DES DONNEES                       *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FDATE                                           *        
      *                FRTFI70I : UNLOAD DE LA TABLE RTFI70            *        
      *                FRTFI80I : UNLOAD DE LA TABLE RTFI70            *        
      *                                                                *        
      *  EN SORTIE   :                                                 *        
      *                FRTFI70O : EN-COURS A GARDER DANS LA TABLE FI70 *        
      *                FRTFI75O : A ARCHIVER DANS LA TABLE FI75        *        
      *                FRTFI80O : EN-COURS A GARDER DANS LA TABLE FI80 *        
      *                FRTFI85O : A ARCHIVER DANS LA TABLE FI75        *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE     ASSIGN TO  FDATE.                                   
      *--                                                                       
           SELECT FDATE     ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI70I  ASSIGN TO  FRTFI70I.                                
      *--                                                                       
           SELECT FRTFI70I  ASSIGN TO  FRTFI70I                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI80I  ASSIGN TO  FRTFI80I.                                
      *--                                                                       
           SELECT FRTFI80I  ASSIGN TO  FRTFI80I                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI70O  ASSIGN TO  FRTFI70O.                                
      *--                                                                       
           SELECT FRTFI70O  ASSIGN TO  FRTFI70O                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI80O  ASSIGN TO  FRTFI80O.                                
      *--                                                                       
           SELECT FRTFI80O  ASSIGN TO  FRTFI80O                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI75O  ASSIGN TO  FRTFI75O.                                
      *--                                                                       
           SELECT FRTFI75O  ASSIGN TO  FRTFI75O                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRTFI85O  ASSIGN TO  FRTFI85O.                                
      *--                                                                       
           SELECT FRTFI85O  ASSIGN TO  FRTFI85O                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIER EN ENTREE                                                    
      *                                                                         
      *                                                                         
      *------ FICHIER FDATE                                                     
       FD  FDATE                                                                
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENR       PIC X(80).                                           
      *                                                                         
       FD  FRTFI70I                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI70I-ENR     PIC X(272).                                         
      *                                                                         
       FD  FRTFI80I                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI80I-ENR     PIC X(505).                                         
      *                                                                         
      *                                                                         
      *--- FICHIER EN SORTIE                                                    
      *                                                                         
      *------ FICHIER EN SORTIE                                                 
       FD  FRTFI70O                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI70O-ENR     PIC X(272).                                         
      *                                                                         
       FD  FRTFI80O                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI80O-ENR     PIC X(505).                                         
      *                                                                         
       FD  FRTFI75O                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI75O-ENR     PIC X(272).                                         
      *                                                                         
       FD  FRTFI85O                                                             
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FRTFI85O-ENR     PIC X(505).                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL     *          
      *--------------------------------------------------------------*          
      *    COPY SYKWSQ10.                                                       
      *    COPY ZLIBERRG.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
      *--- FDATE                                                                
       01  W-ENR-FDATE.                                                         
           05 FDATE-JJMMSSAA              PIC X(8).                             
           05 FILLER                      PIC X(72).                            
      *                                                                         
      *--- ON N'A PAS BESOIN DU DETAIL FRTFI70 JUSTE DE LA DTRAIT               
       01  FRTFI70I-ENREGISTREMENT.                                             
1          05 FILLER      PIC X(257) VALUE SPACES.                              
258        05 F70-DTRAIT  PIC X(008) VALUE SPACES.                              
266        05 FILLER      PIC X(007) VALUE SPACES.                              
273   *                                                                         
      *                                                                         
      *--- ON N'A PAS BESOIN DU DETAIL FRTFI80 JUSTE DE LA DTRAIT               
       01  FRTFI80I-ENREGISTREMENT.                                             
1          05 FILLER      PIC X(417) VALUE SPACES.                              
418        05 F80-DTRAIT  PIC X(008) VALUE SPACES.                              
426        05 FILLER      PIC X(080) VALUE SPACES.                              
506   *                                                                         
      *                                                                         
      *--- COMPTEURS D'ENREGISTREMENTS                                          
      *                                                                         
       01 CPT-GENERAL.                                                          
          03 CPT-FRTFI70I-LU   PIC 9(13) COMP-3   VALUE ZERO.                   
          03 CPT-FRTFI80I-LU   PIC 9(13) COMP-3   VALUE ZERO.                   
          03 CPT-FRTFI70O-EC   PIC 9(13) COMP-3   VALUE ZERO.                   
          03 CPT-FRTFI80O-EC   PIC 9(13) COMP-3   VALUE ZERO.                   
          03 CPT-FRTFI75O-EC   PIC 9(13) COMP-3   VALUE ZERO.                   
          03 CPT-FRTFI85O-EC   PIC 9(13) COMP-3   VALUE ZERO.                   
      *                                                                         
       01  PIC-EDIT            PIC Z(12)9.                                      
      *                                                                         
      *--- STATISTIQUES DE TRAITEMENT                                           
      *                                                                         
       01 CR-STAT.                                                              
          03 CR-TIME.                                                           
             05 CR-HEURE         PIC 9(2).                                      
             05 CR-MINUTE        PIC 9(2).                                      
             05 CR-SECONDE       PIC 9(2).                                      
             05 FILLER           PIC 9(2).                                      
          03 CR-DUREE            PIC 9(8).                                      
          03 CR-DATE             PIC 9(6).                                      
          03 CR-JOUR-DEBUT       PIC 9(5).                                      
          03 CR-JOUR-FIN         PIC 9(5).                                      
      *                                                                         
      *--- DATE                                                                 
       01 W-D-JOUR               PIC X(08) VALUE SPACES.                        
       01 W-D-PURGE              PIC X(08) VALUE SPACES.                        
      *                                                                         
      *--- FLAG                                                                 
       01 ST-FDATE          PIC 99 VALUE ZERO.                                  
       01 F-FRTFI70I         PIC 9     VALUE 0.                                 
          88 DEBUT-FRTFI70I            VALUE 0.                                 
          88 FIN-FRTFI70I              VALUE 1.                                 
       01 F-FRTFI80I         PIC 9     VALUE 0.                                 
          88 DEBUT-FRTFI80I            VALUE 0.                                 
          88 FIN-FRTFI80I              VALUE 1.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
       01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC / BFTNPER / FNUM                     *          
      *--------------------------------------------------------------*          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BFI097.                                                           
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM TRAITEMENT-FRTFI70                                           
           PERFORM TRAITEMENT-FRTFI80                                           
           PERFORM AFFICHAGE-COMPTEUR                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BFI097.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-          BIENVENUE DANS LE PROGRAMME          -'          
           DISPLAY '-                    BFI097                     -'          
           DISPLAY '-------------------------------------------------'          
      *                                                                         
           MOVE 'BFI097' TO  ABEND-PROG                                         
      *                                                                         
           ACCEPT CR-DATE FROM DATE                                             
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-JOUR-DEBUT FROM DAY                                        
      *                                                                         
           COMPUTE CR-DUREE  = CR-HEURE * 3600                                  
                             + CR-MINUTE * 60 + CR-SECONDE                      
      *                                                                         
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-                                               -'          
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           SET DEBUT-FRTFI70I TO TRUE                                           
           SET DEBUT-FRTFI80I TO TRUE                                           
           OPEN INPUT   FRTFI70I                                                
                        FRTFI80I                                                
           OPEN OUTPUT  FRTFI70O                                                
                        FRTFI80O                                                
                        FRTFI75O                                                
                        FRTFI85O                                                
      *                                                                         
      ***  LECTURE ET CONTROLE DATE CARTE PARAMETRE                             
           OPEN INPUT FDATE                                                     
           READ FDATE INTO W-ENR-FDATE                                          
           IF ST-FDATE = 10                                                     
              DISPLAY '>>********************************************'          
              DISPLAY '** ABEND - DATE VIDE  : '                                
              DISPLAY '***'                                                     
              MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                          
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE '1'            TO GFDATA.                                       
           MOVE FDATE-JJMMSSAA TO GFJJMMSSAA.                                   
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY '>>********************************************'          
              DISPLAY '** ABEND - ERREUR DATE : ' FDATE-JJMMSSAA                
              DISPLAY '***'                                                     
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              GO TO FIN-ANORMALE                                                
           ELSE                                                                 
              MOVE  GFSAMJ-0       TO W-D-JOUR                                  
              COMPUTE GFQNT0 = GFQNT0 + 1                                       
              MOVE '3' TO GFDATA                                                
              CALL 'BETDATC' USING WORK-BETDATC                                 
              IF GFVDAT NOT = '1'                                               
                 DISPLAY 'DATE DE TRAITEMENT INVALIDE '                         
                 MOVE '** DATE DE TRAITE INVALIDE **' TO ABEND-MESS             
                 GO TO FIN-ANORMALE                                             
              ELSE                                                              
                 MOVE '13'           TO GFAJOUX                                 
                 MOVE 'B' TO GFDATA                                             
                 CALL 'BETDATC' USING WORK-BETDATC                              
                 IF GFVDAT NOT = '1'                                            
                    DISPLAY 'CALCUL DATE EPUISE INVALIDE '                      
                    MOVE '** DATE DE TRAITE INVALIDE **' TO ABEND-MESS          
                    GO TO FIN-ANORMALE                                          
                 ELSE                                                           
                    MOVE  GFSAMJ-0       TO W-D-PURGE                           
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           DISPLAY '**********************************************'             
           DISPLAY '** DATE DU JOUR            : ' W-D-JOUR                     
           DISPLAY '** DATE DE PURGE           : ' W-D-PURGE                    
           DISPLAY '**********************************************'.            
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *                                                                         
      *----------------------------------------                                 
       TRAITEMENT-FRTFI70              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      * POUR CHAQUE TABLE                                                       
      * ON SEPARE LES ENREGISTREMENTS QU'ON GARDE ET CEUX QUI SERONT    R       
      * SERONT STOCKES DANS LA TABLE D'ARCHIVE                                  
      *                                                                         
           INITIALIZE CPT-GENERAL                                               
      *                                                                         
           PERFORM LECTURE-FRTFI70I                                             
      *                                                                         
           PERFORM UNTIL FIN-FRTFI70I                                           
      *                                                                         
              IF F70-DTRAIT < W-D-PURGE AND                                     
                 F70-DTRAIT > SPACES    THEN                                    
                 ADD 1 TO CPT-FRTFI75O-EC                                       
                 WRITE  FRTFI75O-ENR FROM FRTFI70I-ENREGISTREMENT               
              ELSE                                                              
                 ADD 1 TO CPT-FRTFI70O-EC                                       
                 WRITE  FRTFI70O-ENR FROM FRTFI70I-ENREGISTREMENT               
              END-IF                                                            
      *                                                                         
              PERFORM LECTURE-FRTFI70I                                          
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT-FRTFI70.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       TRAITEMENT-FRTFI80              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      * POUR CHAQUE TABLE                                                       
      * ON SEPARE LES ENREGISTREMENTS QU'ON GARDE ET CEUX QUI SERONT    R       
      * SERONT STOCKES DANS LA TABLE D'ARCHIVE                                  
      *                                                                         
           PERFORM LECTURE-FRTFI80I                                             
      *                                                                         
           PERFORM UNTIL FIN-FRTFI80I                                           
      *                                                                         
              IF F80-DTRAIT < W-D-PURGE AND                                     
                 F80-DTRAIT > SPACES    THEN                                    
                 ADD 1 TO CPT-FRTFI85O-EC                                       
                 WRITE  FRTFI85O-ENR FROM FRTFI80I-ENREGISTREMENT               
              ELSE                                                              
                 ADD 1 TO CPT-FRTFI80O-EC                                       
                 WRITE  FRTFI80O-ENR FROM FRTFI80I-ENREGISTREMENT               
              END-IF                                                            
      *                                                                         
              PERFORM LECTURE-FRTFI80I                                          
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT-FRTFI80.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       AFFICHAGE-COMPTEUR              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '- AFFICHAGE DES COMPTEURS :                     -'          
           MOVE  CPT-FRTFI70I-LU  TO  PIC-EDIT                                  
           DISPLAY '-  NBRE DE LIGNES LUES FI70      '                          
                   PIC-EDIT '  -'.                                              
           MOVE  CPT-FRTFI70O-EC TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES ECRITES FI70   '                          
                   PIC-EDIT '  -'                                               
           MOVE  CPT-FRTFI75O-EC TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES ECRITES FI75   '                          
                   PIC-EDIT '  -'                                               
           MOVE  CPT-FRTFI80I-LU  TO  PIC-EDIT                                  
           DISPLAY '-  NBRE DE LIGNES LUES FI80      '                          
                   PIC-EDIT '  -'.                                              
           MOVE  CPT-FRTFI80O-EC TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES ECRITES FI80   '                          
                   PIC-EDIT '  -'                                               
           MOVE  CPT-FRTFI85O-EC TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES ECRITES FI85   '                          
                   PIC-EDIT '  -'.                                              
      *                                                                         
       FIN-AFFICHAGE-COMPTEUR.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FRTFI70I                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE FRTFI70I-ENREGISTREMENT                                   
      *                                                                         
           READ FRTFI70I INTO FRTFI70I-ENREGISTREMENT                           
             AT END                                                             
                SET FIN-FRTFI70I TO TRUE                                        
                MOVE LOW-VALUE TO  FRTFI70I-ENREGISTREMENT                      
             NOT END                                                            
                COMPUTE CPT-FRTFI70I-LU = CPT-FRTFI70I-LU + 1                   
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FRTFI70I.              EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FRTFI80I                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FRTFI80I INTO FRTFI80I-ENREGISTREMENT                           
             AT END                                                             
                SET FIN-FRTFI80I TO TRUE                                        
                MOVE LOW-VALUE TO  FRTFI80I-ENREGISTREMENT                      
             NOT END                                                            
                COMPUTE CPT-FRTFI80I-LU = CPT-FRTFI80I-LU + 1                   
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FRTFI80I.              EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           CLOSE FDATE                                                          
                 FRTFI70I                                                       
                 FRTFI80I                                                       
                 FRTFI70O                                                       
                 FRTFI80O                                                       
                 FRTFI75O                                                       
                 FRTFI85O                                                       
      *                                                                         
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-DATE FROM DATE                                             
           DISPLAY '-                                               -'          
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           COMPUTE CR-DUREE  = CR-DUREE - CR-HEURE * 3600                       
                             - CR-MINUTE * 60 - CR-SECONDE                      
           ACCEPT CR-JOUR-FIN FROM DAY                                          
           IF CR-JOUR-FIN > CR-JOUR-DEBUT                                       
              COMPUTE CR-DUREE  = CR-DUREE                                      
                                + (CR-JOUR-FIN - CR-JOUR-DEBUT) * 86400         
           END-IF                                                               
           DIVIDE CR-DUREE  BY 3600 GIVING CR-HEURE                             
                                                   REMAINDER CR-MINUTE          
           DIVIDE CR-MINUTE BY 60 GIVING CR-MINUTE REMAINDER CR-SECONDE         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       DUREE TOTALE DU PROGRAMME '                         
                   CR-HEURE ':' CR-MINUTE ':' CR-SECONDE '      -'              
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       CETTE NUIT TOUT S''EST BIEN PASSE        -'         
           DISPLAY '-------------------------------------------------'          
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY '><                    ' ABEND-PROG(1:6)                     
                                               '                  ><'           
           DISPLAY '><     FIN ANORMALE PROVOQUEE PAR PROGRAMME   ><'           
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY ABEND-MESS                                                   
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
      *                                                                         
       FIN-ANORMALE        SECTION.                                             
      *----------------------------*                                            
           CLOSE FDATE                                                          
                 FRTFI70I                                                       
                 FRTFI80I                                                       
                 FRTFI70O                                                       
                 FRTFI80O                                                       
                 FRTFI75O                                                       
                 FRTFI85O                                                       
           DISPLAY '**************************************************'         
           DISPLAY '**                                              **'         
           DISPLAY '**   BFI097 : EXECUTION TERMINEE ANORMALEMENT   **'         
           DISPLAY '**                                              **'         
           DISPLAY '**************************************************'         
           DISPLAY '**                                              **'         
           DISPLAY '**   MESSAGE ABEND-MESS (--> CALL ABEND) :      **'         
           DISPLAY '**                                              **'         
           DISPLAY '**     ' ABEND-MESS                           ' **'         
           DISPLAY '**                                              **'         
           DISPLAY '**************************************************'         
           MOVE 'BFI097'    TO ABEND-PROG.                                      
           CALL 'ABEND'  USING ABEND-PROG ABEND-MESS.                           
      *                                                                         
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      **                 C'EST FINI !!!                               **        
      ******************************************************************        
      ******************************************************************        
