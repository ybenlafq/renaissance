      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BCG030.                                                      
      *****************************************************************         
      *                    B C G 0 3 0                                          
      *                  *-*-*-*-*-*-*-*                                        
      *  DATE/AUTEUR     : 02/11/2007                                           
      *  AUTEUR          : DSA015                                               
      *  PROJET          : CONVERGENCE                                          
      *  FONCTION        : CREATION D'UN FICHIER DE REPRISE                     
      *****************************************************************         
0313  * DSA015 - EC - MARS 2013 - SIGNET 0313                         *         
      *  UTILISATION D'UN NOUVEAU TYPE DE TIERS : LE TIERS CPD        *         
      *   ALIMENTATION DE ZONES SPECIFIQUES LIEES A CES TIERS         *         
      *   TAILLE FFTV02 PASSE A 400                                   *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FFTV02  ASSIGN TO FFTV02.                                     
      *--                                                                       
           SELECT FFTV02  ASSIGN TO FFTV02                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FICANO  ASSIGN TO FICANO.                                     
      *--                                                                       
           SELECT FICANO  ASSIGN TO FICANO                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FREPRIS ASSIGN TO FREPRIS.                                    
      *--                                                                       
           SELECT FREPRIS ASSIGN TO FREPRIS                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      * FICHIER COMPTABLE GCT                                                   
       FD  FFTV02                                                               
           RECORDING F                                                          
           LABEL RECORD STANDARD                                                
           BLOCK CONTAINS 0 RECORDS.                                            
0313   01  ENR-FFTV02                  PIC X(400).                              
      * FICHIER DES ANOMALIES DE TRANSCO                                        
       FD  FICANO                                                               
           RECORDING F                                                          
           LABEL RECORD STANDARD                                                
           BLOCK CONTAINS 0 RECORDS.                                            
       01  ENR-FICANO                  PIC X(50).                               
      * FICHIER DE REPRSISE                                                     
       FD  FREPRIS                                                              
           RECORDING F                                                          
           LABEL RECORD STANDARD                                                
           BLOCK CONTAINS 0 RECORDS.                                            
0313   01  ENR-FREPRIS                 PIC X(400).                              
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *    CONSTANTES - VARAIBLES                                               
       77 WC-ABEND                       PIC X(08)  VALUE 'ABEND'.              
      * COMPTEURS                                                               
       77 CPT-LIGNES-LUES                PIC 9(10)  VALUE ZERO.                 
       77 CPT-ANO-LUES                   PIC 9(10)  VALUE ZERO.                 
       77 CPT-LIGNES-REPRIS              PIC 9(10)  VALUE ZERO.                 
       77 WC-BETDATI                     PIC X(07)  VALUE 'BETDATI'.            
       77 WS-CODLANG                     PIC X(02).                             
       01 DATE-COMPILE.                                                         
          05 COMPILE-JJ                  PIC XX.                                
          05 FILLER                      PIC X      VALUE '/'.                  
          05 COMPILE-MM                  PIC XX.                                
          05 FILLER                      PIC XXX    VALUE '/20'.                
          05 COMPILE-AA                  PIC XX.                                
       01 WS-IND-EDIT                    PIC +999999.                           
       01 WS-DATE-JOUR                   PIC X(10).                             
       01 WS-HEURE.                                                             
          05 WS-HEURE-HH                    PIC X(02).                          
          05 WS-HEURE-MM                    PIC X(02).                          
          05 WS-HEURE-SS                    PIC X(02).                          
          05 WS-HEURE-CS                    PIC X(02).                          
       01 WS-CLE.                                                               
          05 WS-NSOC                     PIC X(05).                             
          05 WS-NETAB                    PIC X(03).                             
          05 WS-NJRN                     PIC X(03).                             
          05 FILLER                      PIC X(01).                             
          05 WS-CINTERFACE               PIC X(05).                             
          05 WS-NPIECE                   PIC X(10).                             
       01 WS-VAR.                                                               
      * VARIABLES                                                               
          05 WS-NBLIGNE           PIC 9(04)  VALUE 0.                           
          05 WS-NPIECESAP-X.                                                    
             10 WS-NPIECESAP      PIC 9(02)  VALUE 0.                           
          05 WS-TAUXTVA-X.                                                      
             10 WS-TAUXTVA        PIC 99V99.                                    
          05 WS-CTVA              PIC X(02).                                    
          05 WS-SENS              PIC X(01).                                    
          05 WS-SENS-REPORT       PIC X(01).                                    
          05 WS-MONTANT-REPORT    PIC S9(13)V99 COMP-3 VALUE 0.                 
      * FLAGS                                                                   
       01 WS-FLAG-FFTV02                  PIC X(01) VALUE '0'.                  
          88 WS-FIN-FFTV02-KO             VALUE '0'.                            
          88 WS-FIN-FFTV02                VALUE '1'.                            
       01 WS-FLAG-FICANO                  PIC X(01) VALUE '0'.                  
          88 WS-FIN-FICANO-KO             VALUE '0'.                            
          88 WS-FIN-FICANO                VALUE '1'.                            
      *    DECRIPTIONS DES FICHIERS                                             
0313       COPY FFTV03.                                                         
           COPY FCG010.                                                         
       01 FICANO-ENR.                                                           
          05 FICANO-CLE.                                                        
             10 FICANO-NSOC               PIC X(05).                            
             10 FICANO-NETAB              PIC X(03).                            
             10 FICANO-NJRN               PIC X(03).                            
             10 FILLER                    PIC X(01).                            
             10 FICANO-CINTERFACE         PIC X(05).                            
             10 FICANO-NPIECE             PIC X(10).                            
          05 FILLER                       PIC X(23).                            
           COPY SYKWSQ10.                                                       
           COPY ABENDCOP.                                                       
           COPY SYBWDIV0.                                                       
           COPY WORKDATI.                                                       
       PROCEDURE DIVISION.                                                      
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       MODULE-ENTREE           SECTION.                                         
           DISPLAY ' *-*-*-*-*-*'                                               
           DISPLAY ' B C G 0 3 0'                                               
           DISPLAY ' *-*-*-*-*-*'                                               
           DISPLAY '            '                                               
           OPEN INPUT FFTV02 FICANO                                             
           OPEN OUTPUT  FREPRIS                                                 
           MOVE WHEN-COMPILED               TO Z-WHEN-COMPILED                  
           MOVE Z-WHEN-COMPILED(4:2)        TO COMPILE-JJ                       
           MOVE Z-WHEN-COMPILED(7:2)        TO COMPILE-AA                       
           MOVE Z-WHEN-COMPILED(1:2)        TO COMPILE-MM                       
           DISPLAY 'VERSION DU 29/03/2013 - COMPILE DU ' DATE-COMPILE           
           DISPLAY '            '                                               
           .                                                                    
       FIN-MODULE-ENTREE.      EXIT.                                            
       MODULE-TRAITEMENT       SECTION.                                         
           SET WS-FIN-FICANO-KO  TO TRUE                                        
           SET WS-FIN-FFTV02-KO TO TRUE                                         
           PERFORM LECTURE-FICANO                                               
           IF WS-FIN-FICANO                                                     
              SET WS-FIN-FFTV02 TO TRUE                                         
           ELSE                                                                 
              PERFORM LECTURE-FFTV02                                            
           END-IF                                                               
           PERFORM UNTIL WS-FIN-FFTV02                                          
              EVALUATE TRUE                                                     
                 WHEN FFTV02-CLE < FICANO-CLE                                   
                    PERFORM LECTURE-FFTV02                                      
                 WHEN FFTV02-CLE = FICANO-CLE                                   
                    PERFORM ECRITURE-FREPRIS                                    
                    PERFORM LECTURE-FFTV02                                      
                 WHEN FFTV02-CLE > FICANO-CLE                                   
                    IF WS-FIN-FICANO                                            
                       SET WS-FIN-FFTV02 TO TRUE                                
                    ELSE                                                        
                       PERFORM LECTURE-FICANO                                   
                    END-IF                                                      
              END-EVALUATE                                                      
           END-PERFORM                                                          
           .                                                                    
       FIN-MODULE-TRAITEMENT.  EXIT.                                            
      *                                                                         
       MODULE-SORTIE           SECTION.                                         
           DISPLAY '                         '                                  
           DISPLAY '*-*-*-*-*-*-*-*-*-*-*-*-*'                                  
           DISPLAY ' S T A T I S T I Q U E S '                                  
           DISPLAY '*-*-*-*-*-*-*-*-*-*-*-*-*'                                  
           DISPLAY '                         '                                  
           DISPLAY  '                        '                                  
           DISPLAY  'NOMBRE DE PIECES EN ANOMALIE        : '                    
                    CPT-ANO-LUES                                                
           DISPLAY  '                        '                                  
           DISPLAY  'NOMBRE DE LIGNES COMPTABLES LUES    : '                    
                    CPT-LIGNES-LUES                                             
           DISPLAY  '                        '                                  
           DISPLAY  'NOMBRE DE LIGNES COMPTABLES REPRISES : '                   
                    CPT-LIGNES-REPRIS                                           
           CLOSE FFTV02 FICANO FREPRIS                                          
           .                                                                    
       FIN-MODULE-SORTIE.      EXIT.                                            
      *================================================================*        
      *                     TRAITEMENT DES FICHIERS                    *        
      *================================================================*        
       LECTURE-FFTV02          SECTION.                                         
           READ FFTV02 INTO FFTV02-ENR                                          
           AT END                                                               
                 SET WS-FIN-FFTV02 TO TRUE                                      
           NOT AT END                                                           
                 ADD 1   TO  CPT-LIGNES-LUES                                    
           .                                                                    
       FIN-LECTURE-FFTV02.     EXIT.                                            
      *                                                                         
       LECTURE-FICANO          SECTION.                                         
           READ FICANO INTO FICANO-ENR                                          
           AT END                                                               
                 SET WS-FIN-FICANO TO TRUE                                      
           NOT AT END                                                           
                 ADD 1   TO  CPT-ANO-LUES                                       
           .                                                                    
       FIN-LECTURE-FICANO.     EXIT.                                            
      *                                                                         
       ECRITURE-FREPRIS        SECTION.                                         
           WRITE ENR-FREPRIS FROM FFTV02-ENR                                    
           ADD 1   TO   CPT-LIGNES-REPRIS                                       
           .                                                                    
       FIN-ECRITURE-FREPRIS.   EXIT.                                            
      *                                                                         
      *================================================================*        
      *                     APPEL MODULES                              *        
      *================================================================*        
       ABEND-PROGRAMME         SECTION.                                         
           CLOSE FFTV02 FICANO FREPRIS                                          
           DISPLAY '          '                                                 
           DISPLAY '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'                      
           DISPLAY '$$         P L A N T A G E         $$'                      
           DISPLAY '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'                      
           MOVE 'BCG030'                    TO ABEND-PROG                       
           CALL WC-ABEND USING ABEND-PROG ABEND-MESS                            
           .                                                                    
       FIN-ABEND-PROGRAMME.                                                     
