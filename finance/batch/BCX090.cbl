      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BCX090.                                                   
       AUTHOR.        DSA021.                                                   
       ENVIRONMENT    DIVISION.                                                 
       CONFIGURATION  SECTION.                                                  
      *                                                                         
      *=================================================================        
      *                                                                         
      * PROGRAMME BCX090 :                                                      
      *                                                                         
      *    BUT ===>  EDITION DU JOURNAL DE CAISSE ENTREPOT                      
      *                                                                         
      *=================================================================        
      *                                                                         
       INPUT-OUTPUT   SECTION.                                                  
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCX90T ASSIGN  TO FCX90T.                                     
      *--                                                                       
           SELECT FCX90T ASSIGN  TO FCX90T                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ICX090 ASSIGN  TO ICX090.                                     
      *--                                                                       
           SELECT ICX090 ASSIGN  TO ICX090                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *---------------------------                                              
      *    DEFINITION DE  FCX90                                                 
      *---------------------------                                              
       FD   FCX90T                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
            COPY FCX85.                                                         
      *---------------------------                                              
      *    DEFINITION DE  ICX090                                                
      *---------------------------                                              
       FD   ICX090                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   ENR-ICX090.                                                         
            05 CODE-SAUT            PIC X(001).                                 
            05 ENREG-ICX090         PIC X(132).                                 
      *                                                                         
      *=================================================================        
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *----------------------------------------                                 
      *    DESCRIPTION DES DIFFERENTS FICHIERS                                  
      *----------------------------------------                                 
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      *     ZONE D'APPEL AU MODULE ABEND                                        
            COPY ABENDCOP.                                                      
      *-----------------------------------------------------------------        
      *     ZONE DU MODULE DE TRAITEMENT DES DATES  ( BETDATC )                 
      *                                                                         
            COPY WORKDATC.                                                      
      *-----------------------------------------------------------------        
       01  DATHEUR     PIC S9(13)  COMP-3  VALUE +0.                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RET         PIC S9(4)   COMP    VALUE +0.                            
      *--                                                                       
       01  RET         PIC S9(4) COMP-5    VALUE +0.                            
      *}                                                                        
      *-----------------------------------------------------------------        
       01  I                              PIC 9(02) VALUE ZEROES.               
       01  J                              PIC 9(02) VALUE ZEROES.               
       01  K                              PIC 9(02) VALUE ZEROES.               
       01  Y                              PIC 9(02) VALUE ZEROES.               
       01  Z                              PIC 9(02) VALUE ZEROES.               
      *-----------------------------------------------------------------        
       01  W-PREM-MONT-NEG                PIC X(01) VALUE '0'.                  
           88 PREM-MONT-NEG                         VALUE '0'.                  
       01  W-PREM-LECTURE-FCX90T          PIC X(01) VALUE '0'.                  
           88 PREM-LECTURE-FCX90T                   VALUE '0'.                  
       01  W-PREM-LIG-TOURNEE             PIC X(01) VALUE '0'.                  
       01  W-PREM-LIG-DTLIVR              PIC X(01) VALUE '0'.                  
       01  W-PREM-LIG-ENCAISS             PIC X(01) VALUE '0'.                  
       01  W-NB-LIGNES                    PIC 999   VALUE ZEROES.               
       01  W-OLD-DTLIVR                   PIC X(08) VALUE SPACES.               
       01  W-OLD-PROFTOUR.                                                      
           02  W-OLD-PROFIL               PIC X(05) VALUE SPACES.               
           02  W-OLD-TOURNEE              PIC X(03) VALUE SPACES.               
       01  W-OLD-VENTE                    PIC X(13) VALUE SPACES.               
       01  W-PROFTOUR.                                                          
           02  W-PROFIL                   PIC X(05) VALUE SPACES.               
           02  W-TOURNEE                  PIC X(03) VALUE SPACES.               
       01  W-DSAISIE-JJMMAA.                                                    
           02  W-D-JJ                     PIC X(02) VALUE SPACES.               
           02  FILLER                     PIC X(01) VALUE '/'.                  
           02  W-D-MM                     PIC X(02) VALUE SPACES.               
           02  FILLER                     PIC X(01) VALUE '/'.                  
           02  W-D-AA                     PIC X(02) VALUE SPACES.               
       01  W-DSAISIE.                                                           
           02  W-DSAIS-SS                 PIC X(02) VALUE SPACES.               
           02  W-DSAIS-AA                 PIC X(02) VALUE SPACES.               
           02  W-DSAIS-MM                 PIC X(02) VALUE SPACES.               
           02  W-DSAIS-JJ                 PIC X(02) VALUE SPACES.               
       01  W-DATE-SYSTEME.                                                      
           02  W-SYS-AA                   PIC X(02) VALUE SPACES.               
           02  W-SYS-MM                   PIC X(02) VALUE SPACES.               
           02  W-SYS-JJ                   PIC X(02) VALUE SPACES.               
       01  W-DATE-JJMMAA.                                                       
           02  W-JJ                       PIC X(02) VALUE SPACES.               
           02  FILLER                     PIC X(01) VALUE '/'.                  
           02  W-MM                       PIC X(02) VALUE SPACES.               
           02  FILLER                     PIC X(01) VALUE '/'.                  
           02  W-AA                       PIC X(02) VALUE SPACES.               
       01  W-DATE-VALID                   PIC X(08) VALUE SPACES.               
      *-----------------------------------------------------------------        
       01  TABLE-CUMUL-TOURNEE.                                                 
           02  TAB-LIG1     OCCURS 10 INDEXED BY IND1.                          
               04  TAB-MREG1              PIC X(05).                            
               04  CUM-MTT-POS1           PIC S9(09)V99.                        
               04  CUM-MTT-NEG1           PIC S9(09)V99.                        
      *-----------------------------------------------------------------        
       01  TABLE-CUMUL-DTLIVR.                                                  
           02  TAB-LIG2     OCCURS 10 INDEXED BY IND2.                          
               04  TAB-MREG2              PIC X(05).                            
               04  CUM-MTT-POS2           PIC S9(09)V99.                        
               04  CUM-MTT-NEG2           PIC S9(09)V99.                        
      *-----------------------------------------------------------------        
       01  TABLE-CUMUL-ENCAISS.                                                 
           02  TAB-LIG3     OCCURS 10 INDEXED BY IND3.                          
               04  TAB-MREG3              PIC X(05).                            
               04  CUM-MTT-POS3           PIC S9(09)V99.                        
               04  CUM-MTT-NEG3           PIC S9(09)V99.                        
      *-----------------------------------------------------------------        
       01  CPT-PAGE                       PIC 9(03) VALUE ZEROES.               
       01  CPT-LIG                        PIC 9(02) VALUE ZEROES.               
       01  W-COLONNE                      PIC 9(01) VALUE ZEROES.               
      *-----------------------------------------------------------------        
       01  W-FIN-FCX90                    PIC X(01) VALUE '0'.                  
           88  FIN-FCX90                            VALUE '1'.                  
      *-----------------------------------------------------------------        
      *     DESCRIPTION DES LIGNES DE L'ETAT                                    
      *-----------------------------------------------------------------        
       01  LIGNE-ICX090                   PIC X(132)  VALUE SPACES.             
       01  LIGNE-ENTETE-1.                                                      
           02  FILLER           PIC X(47) VALUE SPACES.                         
           02  FILLER           PIC X(85) VALUE                                 
           'E T A B L I S S E M E N T S    D A R T Y '.                         
       01  LIGNE-BLANCHE        PIC X(132) VALUE SPACES.                        
       01  LIGNE-ENTETE-2.                                                      
           02  FILLER           PIC X(05) VALUE SPACES.                         
           02  FILLER           PIC X(12) VALUE 'ETAT ICX090'.                  
           02  FILLER           PIC X(30) VALUE SPACES.                         
           02  FILLER           PIC X(73) VALUE                                 
                                'JOURNAL DE CAISSE ENTREPOT'.                   
           02  DATE-EDITION     PIC X(08)  VALUE SPACES.                        
           02  FILLER           PIC X(04)  VALUE SPACES.                        
       01  LIGNE-ENTETE-3.                                                      
           02  FILLER           PIC X(05) VALUE SPACES.                         
           02  FILLER           PIC X(41) VALUE SPACES.                         
           02  FILLER           PIC X(20) VALUE                                 
                                'VALIDATION DEPOT DU '.                         
           02  DATE-VALID       PIC X(08)  VALUE SPACES.                        
           02  FILLER           PIC X(47)  VALUE SPACES.                        
           02  FILLER           PIC X(04)  VALUE 'PAGE'.                        
           02  NO-PAGE          PIC ZZ9.                                        
           02  FILLER           PIC X(04)  VALUE SPACES.                        
       01  LIGNE-ENTETE-4.                                                      
           02  FILLER           PIC X(05)  VALUE SPACES.                        
           02  FILLER           PIC X(49)  VALUE                                
               'NAT   DATE     PROFIL/         CLIENT            '.             
           02  FILLER           PIC X(54)  VALUE                                
               'N� MAG / N� VENTE       MTT   M.REG       MTT   M.REG'.         
           02  FILLER           PIC X(18)  VALUE '       MTT   M.REG'.          
           02  FILLER           PIC X(06)  VALUE SPACES.                        
       01  LIGNE-ENTETE-5.                                                      
           02  FILLER           PIC X(05)  VALUE SPACES.                        
           02  FILLER           PIC X(127) VALUE                                
                                '      LIVR      TOURNEE'.                      
       01  LIGNE-ENTETE-6.                                                      
           02  FILLER           PIC X(75)  VALUE SPACES.                        
           02  FILLER           PIC X(20)  VALUE 'MONTANTS > 0'.                
           02  FILLER           PIC X(20)  VALUE 'MONTANTS < 0'.                
           02  FILLER           PIC X(17)  VALUE SPACES.                        
       01  LIGNE-POINTILLE.                                                     
           02  FILLER           PIC X(20)  VALUE '....................'.        
           02  FILLER           PIC X(112) VALUE SPACES.                        
       01  LIGNE-DETAIL-1.                                                      
           02  FILLER       PIC X(06)  VALUE SPACES.                            
           02  NAT          PIC X(01)  VALUE SPACES.                            
           02  FILLER       PIC X(02)  VALUE SPACES.                            
           02  DATE-LIVR    PIC X(08)  VALUE SPACES.                            
           02  FILLER       PIC X(02)  VALUE SPACES.                            
           02  PROFIL       PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(01)  VALUE SPACES.                            
           02  TOURNEE      PIC X(03)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  NOM-CLIENT   PIC X(20)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MAG1         PIC X(03)  VALUE SPACES.                            
           02  MAG2         PIC X(03)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE ' / '.                             
           02  NO-VENTE     PIC X(07)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MTT1         PIC -ZZZZ9V,99.                                     
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG1        PIC X(05).                                          
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MTT2         PIC -ZZZZ9V,99.                                     
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG2        PIC X(05).                                          
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MTT3         PIC -ZZZZ9V,99.                                     
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG3        PIC X(05).                                          
           02  FILLER       PIC X(08)  VALUE SPACES.                            
       01  LIGNE-DETAIL-2.                                                      
           02  FILLER       PIC X(20)  VALUE SPACES.                            
           02  FILLER       PIC X(14)  VALUE 'TOTAL TOURNEE '.                  
           02  PROFIL2      PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(01)  VALUE '/'.                               
           02  TOURNEE2     PIC X(03)  VALUE SPACES.                            
           02  FILLER       PIC X(27)  VALUE SPACES.                            
           02  MONT-POS-2   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-2   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-2   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-2   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
       01  LIGNE-DETAIL-2B.                                                     
           02  FILLER       PIC X(70)  VALUE SPACES.                            
           02  MONT-POS-2B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-2B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-2B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-2B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
       01  LIGNE-DETAIL-3.                                                      
           02  FILLER       PIC X(20)  VALUE SPACES.                            
           02  FILLER       PIC X(24)  VALUE 'TOTAL DATE DE LIVRAISON '.        
           02  DT-LIVR-3    PIC X(08)  VALUE SPACES.                            
           02  FILLER       PIC X(18)  VALUE SPACES.                            
           02  MONT-POS-3   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-3   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-3   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-3   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
       01  LIGNE-DETAIL-3B.                                                     
           02  FILLER       PIC X(70)  VALUE SPACES.                            
           02  MONT-POS-3B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-3B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-3B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-3B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
       01  LIGNE-DETAIL-4.                                                      
           02  FILLER       PIC X(20)  VALUE SPACES.                            
           02  FILLER       PIC X(19)  VALUE 'TOTAL ENCAISSEMENT '.             
           02  DT-ENCAISS-4 PIC X(08)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
           02  MONT-POS-4   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-4   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-4   PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-4   PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(23)  VALUE SPACES.                            
       01  LIGNE-DETAIL-4B.                                                     
           02  FILLER       PIC X(70)  VALUE SPACES.                            
           02  MONT-POS-4B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-POS-4B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MONT-NEG-4B  PIC -ZZZZZZZ9V,99 BLANK WHEN ZERO.                  
           02  FILLER       PIC X(03)  VALUE SPACES.                            
           02  MREG-NEG-4B  PIC X(05)  VALUE SPACES.                            
           02  FILLER       PIC X(35)  VALUE SPACES.                            
      *                                                                         
      *-----------------------------------------------------------------        
      *     VARIABLES DB2                                                       
      *-----------------------------------------------------------------        
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      *                                                                         
      *=================================================================        
      *                         ___________________                             
      *                        !                   !                            
      *                        ! PROGRAMME BCX090  !                            
      *                        !___________________!                            
      *                                 !                                       
      *           ______________________!_______________________                
      *          !                      !                       !               
      *    ______!_______       ________!__________        _____!______         
      *   !              !     !                   !      !            !        
      *   ! DEBUT-BCX090 !     ! TRAITEMENT-BCX090 !      ! FIN-BCX090 !        
      *   !______________!     !___________________!      !___________ !        
      *                                                                         
      *                                                                         
      *=================================================================        
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *=================================================================        
      *                                                                         
       PROGRAMME-BCX090.                                                        
           PERFORM DEBUT-BCX090.                                                
           PERFORM TRAITEMENT-BCX090 UNTIL FIN-FCX90.                           
           PERFORM BCX090-FIN.                                                  
           GOBACK.                                                              
      *                                                                         
      *                                                                         
      *=================================================================        
      *                                                                         
       DEBUT-BCX090 SECTION.                                                    
      *                                                                         
      *=================================================================        
      *                                                                         
           PERFORM OUVERTURE-FICHIERS.                                          
      *                                                                         
           INITIALIZE TABLE-CUMUL-TOURNEE.                                      
           INITIALIZE TABLE-CUMUL-DTLIVR.                                       
           INITIALIZE TABLE-CUMUL-ENCAISS.                                      
           MOVE ZEROES  TO I J K.                                               
      *                                                                         
       FIN-DEBUT-BCX090. EXIT.                                                  
      *                                                                         
      *                                                                         
      *---------------------------                                              
       OUVERTURE-FICHIERS SECTION.                                              
      *---------------------------                                              
      *                                                                         
           OPEN INPUT  FCX90T                                                   
                OUTPUT ICX090.                                                  
      *                                                                         
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *                                                                         
      *=================================================================        
      *                                                                         
       TRAITEMENT-BCX090 SECTION.                                               
      *                                                                         
      *=================================================================        
      *                                                                         
           PERFORM LIRE-FCX90.                                                  
           IF FIN-FCX90                                                         
              MOVE SPACES    TO FCX85-ENREG                                     
              PERFORM EDITER-LIGNE-DETAIL                                       
              PERFORM EDITER-TOTAL-TOURNEE                                      
              PERFORM EDITER-TOTAL-DTLIVR                                       
              PERFORM EDITER-TOTAL-ENCAISS                                      
              GO TO FIN-TRAITEMENT-BCX090                                       
           END-IF.                                                              
           IF FCX85-DSAISIE NOT = W-OLD-DTLIVR                                  
              IF W-OLD-DTLIVR  = SPACES                                         
                 PERFORM EDITER-ENTETE                                          
                 PERFORM EDITER-SOUS-ENTETE                                     
                 MOVE ZERO     TO W-COLONNE                                     
                 MOVE '0'      TO W-PREM-MONT-NEG                               
                 PERFORM FORMATER-LIGNE-DETAIL                                  
                 MOVE FCX85-DSAISIE   TO W-OLD-DTLIVR                           
                 MOVE FCX85-CTOURNEE  TO W-OLD-TOURNEE                          
                 MOVE FCX85-CPROTOUR  TO W-OLD-PROFIL                           
                 MOVE FCX85-SOCLIEUVENTE TO W-OLD-VENTE                         
              ELSE                                                              
                 PERFORM EDITER-LIGNE-DETAIL                                    
                 PERFORM EDITER-TOTAL-TOURNEE                                   
                 INITIALIZE TABLE-CUMUL-TOURNEE                                 
                 MOVE ZERO            TO I                                      
                 PERFORM EDITER-TOTAL-DTLIVR                                    
                 INITIALIZE TABLE-CUMUL-DTLIVR                                  
                 MOVE ZERO            TO J                                      
                 MOVE ZERO            TO W-COLONNE                              
                 MOVE '0'      TO W-PREM-MONT-NEG                               
                 PERFORM FORMATER-LIGNE-DETAIL                                  
                 MOVE FCX85-DSAISIE   TO W-OLD-DTLIVR                           
                 MOVE FCX85-CTOURNEE  TO W-OLD-TOURNEE                          
                 MOVE FCX85-CPROTOUR  TO W-OLD-PROFIL                           
                 MOVE FCX85-SOCLIEUVENTE TO W-OLD-VENTE                         
              END-IF                                                            
           ELSE                                                                 
              MOVE FCX85-CTOURNEE     TO W-TOURNEE                              
              MOVE FCX85-CPROTOUR     TO W-PROFIL                               
              IF W-PROFTOUR NOT = W-OLD-PROFTOUR                                
                 PERFORM EDITER-LIGNE-DETAIL                                    
                 PERFORM EDITER-TOTAL-TOURNEE                                   
                 INITIALIZE TABLE-CUMUL-TOURNEE                                 
                 MOVE ZERO            TO I                                      
                 MOVE ZERO            TO W-COLONNE                              
                 MOVE '0'      TO W-PREM-MONT-NEG                               
                 PERFORM FORMATER-LIGNE-DETAIL                                  
                 MOVE FCX85-CTOURNEE  TO W-OLD-TOURNEE                          
                 MOVE FCX85-CPROTOUR  TO W-OLD-PROFIL                           
                 MOVE FCX85-SOCLIEUVENTE TO W-OLD-VENTE                         
              ELSE                                                              
                 IF FCX85-SOCLIEUVENTE NOT = W-OLD-VENTE                        
                    PERFORM EDITER-LIGNE-DETAIL                                 
                    MOVE ZERO         TO W-COLONNE                              
                    MOVE '0'          TO W-PREM-MONT-NEG                        
                    PERFORM FORMATER-LIGNE-DETAIL                               
                    MOVE FCX85-SOCLIEUVENTE TO W-OLD-VENTE                      
                 ELSE                                                           
                    PERFORM FORMATER-LIGNE-DETAIL                               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-BCX090. EXIT.                                             
      *                                                                         
      *-----------------------------------                                      
       LIRE-FCX90               SECTION.                                        
      *-----------------------------------                                      
      *                                                                         
           READ FCX90T  AT END                                                  
                MOVE '1'   TO W-FIN-FCX90.                                      
           IF PREM-LECTURE-FCX90T                                               
              MOVE FCX85-DTOPE   TO W-DATE-VALID                                
              MOVE '1'  TO W-PREM-LECTURE-FCX90T                                
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FCX90.             EXIT.                                        
      *                                                                         
      *--------------------------------------                                   
       FORMATER-LIGNE-DETAIL     SECTION.                                       
      *--------------------------------------                                   
      *    IF FCX85-PREGLTVTE < ZERO                                            
      *       DISPLAY '06-FORMATER, COLONNE = ' W-COLONNE                       
      *       DISPLAY '06-FORMATER, MONTANT = ' FCX85-PREGLTVTE                 
      *       DISPLAY '06-FORMATER, FLAG    = ' W-PREM-MONT-NEG                 
      *    END-IF.                                                              
           IF (FCX85-PREGLTVTE < ZEROES AND PREM-MONT-NEG)                      
              OR (W-COLONNE = 3)                                                
                 PERFORM EDITER-LIGNE-DETAIL                                    
                 MOVE ZERO TO W-COLONNE                                         
              IF FCX85-PREGLTVTE < ZERO                                         
                 MOVE '1'  TO W-PREM-MONT-NEG                                   
              END-IF                                                            
           END-IF.                                                              
           ADD 1  TO W-COLONNE.                                                 
           IF W-COLONNE =  1                                                    
      *       DISPLAY '06-FORMATER, COLONNE = ' W-COLONNE                       
              MOVE FCX85-DSAISIE        TO W-DSAISIE                            
              MOVE W-DSAIS-JJ           TO W-D-JJ                               
              MOVE W-DSAIS-MM           TO W-D-MM                               
              MOVE W-DSAIS-AA           TO W-D-AA                               
              MOVE W-DSAISIE-JJMMAA     TO DATE-LIVR                            
              MOVE FCX85-CPROTOUR       TO PROFIL                               
              MOVE FCX85-CTOURNEE       TO TOURNEE                              
              MOVE FCX85-LNOM           TO NOM-CLIENT                           
              MOVE FCX85-NSOCIETE       TO MAG1                                 
              MOVE FCX85-NLIEU          TO MAG2                                 
              MOVE FCX85-NVENTE         TO NO-VENTE                             
              MOVE FCX85-PREGLTVTE      TO MTT1                                 
              MOVE FCX85-CMODPAIMT      TO MREG1                                
              IF FCX85-PREGLTVTE < ZERO                                         
                 MOVE 'S'               TO NAT                                  
              ELSE                                                              
                 MOVE 'C'               TO NAT                                  
              END-IF                                                            
           END-IF.                                                              
           IF W-COLONNE =  2                                                    
      *       DISPLAY '06-FORMATER, COLONNE = ' W-COLONNE                       
              MOVE FCX85-PREGLTVTE      TO MTT2                                 
              MOVE FCX85-CMODPAIMT      TO MREG2                                
           END-IF.                                                              
           IF W-COLONNE =  3                                                    
      *       DISPLAY '06-FORMATER, COLONNE = ' W-COLONNE                       
              MOVE FCX85-PREGLTVTE      TO MTT3                                 
              MOVE FCX85-CMODPAIMT      TO MREG3                                
           END-IF.                                                              
      ***  PREPARATION DE CUMULS ***                                            
      ***  1) CUMULS PAR TOURNEE ***                                            
           SET IND1    TO 1.                                                    
           SEARCH TAB-LIG1                                                      
              AT END                                                            
                 ADD 1                    TO I                                  
                 MOVE FCX85-CMODPAIMT     TO TAB-MREG1(I)                       
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-NEG1(I)                    
                 ELSE                                                           
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-POS1(I)                    
                 END-IF                                                         
              WHEN TAB-MREG1(IND1) = FCX85-CMODPAIMT                            
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-NEG1(IND1)                 
      *             DISPLAY '06-CUM-MTT-NEG1 = ' CUM-MTT-NEG1(IND1)             
                 ELSE                                                           
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-POS1(IND1)                 
                 END-IF                                                         
           END-SEARCH.                                                          
      *    DISPLAY '06-FORMATER, FIN-CUMUL/TOURNEE'.                            
      ***  2) CUMULS PAR DATE DE LIVRAISON ***                                  
           SET IND2    TO 1.                                                    
           SEARCH TAB-LIG2                                                      
              AT END                                                            
                 ADD 1                    TO J                                  
                 MOVE FCX85-CMODPAIMT     TO TAB-MREG2(J)                       
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-NEG2(J)                    
                 ELSE                                                           
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-POS2(J)                    
                 END-IF                                                         
              WHEN TAB-MREG2(IND2) = FCX85-CMODPAIMT                            
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-NEG2(IND2)                 
                 ELSE                                                           
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-POS2(IND2)                 
                 END-IF                                                         
           END-SEARCH.                                                          
      *    DISPLAY '06-FORMATER, FIN-CUMUL/DT-LIVR'.                            
      ***  3) CUMULS TOTAL ENCAISSEMENT ***                                     
           SET IND3    TO 1.                                                    
           SEARCH TAB-LIG3                                                      
              AT END                                                            
                 ADD 1                    TO K                                  
                 MOVE FCX85-CMODPAIMT     TO TAB-MREG3(K)                       
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-NEG3(K)                    
                 ELSE                                                           
                    MOVE FCX85-PREGLTVTE  TO CUM-MTT-POS3(K)                    
                 END-IF                                                         
              WHEN TAB-MREG3(IND3) = FCX85-CMODPAIMT                            
                 IF FCX85-PREGLTVTE < ZEROES                                    
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-NEG3(IND3)                 
                 ELSE                                                           
                    ADD  FCX85-PREGLTVTE  TO CUM-MTT-POS3(IND3)                 
                 END-IF                                                         
           END-SEARCH.                                                          
      *    DISPLAY '06-FORMATER, FIN-CUMUL/ENCAISS'.                            
       FIN-FORMATER-LIGNE-DETAIL.   EXIT.                                       
            EJECT                                                               
      *--------------------------------------                                   
       EDITER-LIGNE-DETAIL     SECTION.                                         
      *--------------------------------------                                   
           PERFORM TESTER-RUPTURE-PAGE.                                         
           MOVE LIGNE-DETAIL-1          TO LIGNE-ICX090.                        
           PERFORM EDITER-LIGNE.                                                
           MOVE SPACES                  TO LIGNE-DETAIL-1.                      
           ADD 1 TO W-NB-LIGNES.                                                
      *    DISPLAY '06B-EDITER-LIGNE-DETAIL = ' W-NB-LIGNES.                    
       FIN-EDITER-LIGNE-DETAIL.   EXIT.                                         
            EJECT                                                               
      *--------------------------------------                                   
       TESTER-RUPTURE-PAGE    SECTION.                                          
      *--------------------------------------                                   
      *    DISPLAY '07-TRAITER-RUPTURE-PAGE-CPT-PAGE = ' CPT-PAGE.              
           IF CPT-LIG > 55                                                      
              MOVE ZEROES     TO CPT-LIG                                        
              PERFORM EDITER-ENTETE                                             
           END-IF.                                                              
       FIN-TESTER-RUPTURE-PAGE.   EXIT.                                         
            EJECT                                                               
      *--------------------------------------                                   
       EDITER-ENTETE       SECTION.                                             
      *--------------------------------------                                   
      *    DISPLAY '08-EDITER-ENTETE'.                                          
           ADD 1                  TO CPT-PAGE.                                  
           MOVE ZEROES            TO CPT-LIG.                                   
           MOVE LIGNE-ENTETE-1    TO LIGNE-ICX090.                              
           WRITE ENR-ICX090 FROM LIGNE-ICX090 AFTER ADVANCING PAGE.             
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           ACCEPT W-DATE-SYSTEME  FROM DATE.                                    
           MOVE W-SYS-JJ          TO W-JJ.                                      
           MOVE W-SYS-MM          TO W-MM.                                      
           MOVE W-SYS-AA          TO W-AA.                                      
           MOVE W-DATE-JJMMAA  TO DATE-EDITION.                                 
           MOVE LIGNE-ENTETE-2    TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE CPT-PAGE          TO NO-PAGE.                                   
           MOVE W-DATE-VALID      TO DATE-VALID.                                
           MOVE LIGNE-ENTETE-3    TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
       FIN-EDITER-ENTETE.     EXIT.                                             
           EJECT                                                                
      *--------------------------------------                                   
       EDITER-SOUS-ENTETE       SECTION.                                        
      *--------------------------------------                                   
           IF CPT-LIG > 50                                                      
              MOVE 56   TO CPT-LIG                                              
              PERFORM EDITER-ENTETE                                             
           END-IF.                                                              
           MOVE LIGNE-ENTETE-4    TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-ENTETE-5    TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE     TO LIGNE-ICX090.                              
           PERFORM EDITER-LIGNE.                                                
       FIN-EDITER-SOUS-ENTETE.     EXIT.                                        
           EJECT                                                                
      *--------------------------------                                         
       EDITER-TOTAL-TOURNEE    SECTION.                                         
      *--------------------------------                                         
      *    DISPLAY '09-EDITER-TOTAL-TOURNEE'.                                   
           IF CPT-LIG > 48                                                      
              MOVE  56    TO CPT-LIG                                            
              PERFORM TESTER-RUPTURE-PAGE                                       
           END-IF.                                                              
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-ENTETE-6   TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE W-OLD-PROFIL     TO PROFIL2.                                    
           MOVE W-OLD-TOURNEE    TO TOURNEE2.                                   
      *    DISPLAY '09-PROFIL/TOURNEE = ' W-OLD-PROFTOUR.                       
           MOVE '0'   TO W-PREM-LIG-TOURNEE.                                    
           PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > 10                           
              MOVE SPACES   TO MREG-POS-2 MREG-NEG-2                            
              MOVE ZEROES   TO MONT-POS-2 MONT-NEG-2                            
              MOVE SPACES   TO MREG-POS-2B MREG-NEG-2B                          
              MOVE ZEROES   TO MONT-POS-2B MONT-NEG-2B                          
              IF CUM-MTT-POS1(Y) NOT = ZEROES                                   
      *          DISPLAY '09-CUM-POS1, Y = ' CUM-MTT-POS1(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG1(Y)       TO MREG-POS-2                       
                    MOVE CUM-MTT-POS1(Y)    TO MONT-POS-2                       
                 ELSE                                                           
                    MOVE TAB-MREG1(Y)       TO MREG-POS-2B                      
                    MOVE CUM-MTT-POS1(Y)    TO MONT-POS-2B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-NEG1(Y) NOT = ZEROES                                   
      *          DISPLAY '09-CUM-NEG1, Y = ' CUM-MTT-NEG1(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG1(Y)       TO MREG-NEG-2                       
                    MOVE CUM-MTT-NEG1(Y)    TO MONT-NEG-2                       
                 ELSE                                                           
                    MOVE TAB-MREG1(Y)       TO MREG-NEG-2B                      
                    MOVE CUM-MTT-NEG1(Y)    TO MONT-NEG-2B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-POS1(Y) NOT = 0  OR CUM-MTT-NEG1(Y) NOT = 0            
                 IF W-PREM-LIG-TOURNEE = '0'                                    
                    MOVE LIGNE-DETAIL-2       TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE '1'                  TO W-PREM-LIG-TOURNEE             
                    MOVE SPACES               TO MREG-NEG-2 MREG-POS-2          
                    MOVE ZEROES               TO MONT-NEG-2 MONT-POS-2          
                 ELSE                                                           
                    MOVE LIGNE-DETAIL-2B      TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE SPACES              TO MREG-NEG-2B MREG-POS-2B         
                    MOVE ZEROES              TO MONT-NEG-2B MONT-POS-2B         
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-POINTILLE            TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-POINTILLE            TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           IF FCX85-DSAISIE = W-OLD-DTLIVR                                      
              PERFORM EDITER-SOUS-ENTETE                                        
           END-IF.                                                              
       FIN-EDITER-TOTAL-TOURNEE.   EXIT.                                        
           EJECT                                                                
      *--------------------------------                                         
       EDITER-TOTAL-DTLIVR     SECTION.                                         
      *--------------------------------                                         
      *    DISPLAY '10-EDITER-TOTAL-DTLIVR'.                                    
           IF CPT-LIG > 48                                                      
              MOVE  56    TO CPT-LIG                                            
              PERFORM TESTER-RUPTURE-PAGE                                       
           END-IF.                                                              
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-ENTETE-6   TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE W-OLD-DTLIVR     TO W-DSAISIE.                                  
           MOVE W-DSAIS-JJ       TO W-D-JJ.                                     
           MOVE W-DSAIS-MM       TO W-D-MM.                                     
           MOVE W-DSAIS-AA       TO W-D-AA.                                     
           MOVE W-DSAISIE-JJMMAA TO DT-LIVR-3.                                  
           MOVE '0'   TO W-PREM-LIG-DTLIVR.                                     
           PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > 10                           
              MOVE SPACES   TO MREG-POS-3 MREG-NEG-3                            
              MOVE ZEROES   TO MONT-POS-3 MONT-NEG-3                            
              MOVE SPACES   TO MREG-POS-3B MREG-NEG-3B                          
              MOVE ZEROES   TO MONT-POS-3B MONT-NEG-3B                          
              IF CUM-MTT-POS2(Y) NOT = ZEROES                                   
      *          DISPLAY '09-CUM-POS2, Y = ' CUM-MTT-POS2(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG2(Y)       TO MREG-POS-3                       
                    MOVE CUM-MTT-POS2(Y)    TO MONT-POS-3                       
                 ELSE                                                           
                    MOVE TAB-MREG2(Y)       TO MREG-POS-3B                      
                    MOVE CUM-MTT-POS2(Y)    TO MONT-POS-3B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-NEG2(Y) NOT = ZEROES                                   
      *          DISPLAY '09-CUM-NEG2, Y = ' CUM-MTT-NEG2(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG2(Y)       TO MREG-NEG-3                       
                    MOVE CUM-MTT-NEG2(Y)    TO MONT-NEG-3                       
                 ELSE                                                           
                    MOVE TAB-MREG2(Y)       TO MREG-NEG-3B                      
                    MOVE CUM-MTT-NEG2(Y)    TO MONT-NEG-3B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-POS2(Y) NOT = 0  OR CUM-MTT-NEG2(Y) NOT = 0            
                 IF W-PREM-LIG-DTLIVR = '0'                                     
                    MOVE LIGNE-DETAIL-3       TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE '1'                  TO W-PREM-LIG-DTLIVR              
                    MOVE SPACES               TO MREG-NEG-3 MREG-POS-3          
                    MOVE ZEROES               TO MONT-NEG-3 MONT-POS-3          
                 ELSE                                                           
                    MOVE LIGNE-DETAIL-3B      TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE SPACES              TO MREG-NEG-3B MREG-POS-3B         
                    MOVE ZEROES              TO MONT-NEG-3B MONT-POS-3B         
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-POINTILLE            TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-POINTILLE            TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE              TO LIGNE-ICX090.                     
           PERFORM EDITER-LIGNE.                                                
           IF FIN-FCX90                                                         
              GO TO FIN-EDITER-TOTAL-DTLIVR                                     
           ELSE                                                                 
              PERFORM EDITER-SOUS-ENTETE                                        
           END-IF.                                                              
       FIN-EDITER-TOTAL-DTLIVR.   EXIT.                                         
           EJECT                                                                
      *--------------------------------                                         
       EDITER-TOTAL-ENCAISS    SECTION.                                         
      *--------------------------------                                         
      *    DISPLAY '11-EDITER-TOTAL-ENCAISS'.                                   
           IF CPT-LIG > 48                                                      
              MOVE  56    TO CPT-LIG                                            
              PERFORM TESTER-RUPTURE-PAGE                                       
           END-IF.                                                              
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-ENTETE-6   TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE LIGNE-BLANCHE    TO LIGNE-ICX090.                               
           PERFORM EDITER-LIGNE.                                                
           MOVE W-DATE-VALID     TO DT-ENCAISS-4.                               
           MOVE '0'   TO W-PREM-LIG-ENCAISS.                                    
           PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > 10                           
              MOVE SPACES   TO MREG-POS-4 MREG-NEG-4                            
              MOVE ZEROES   TO MONT-POS-4 MONT-NEG-4                            
              MOVE SPACES   TO MREG-POS-4B MREG-NEG-4B                          
              MOVE ZEROES   TO MONT-POS-4B MONT-NEG-4B                          
              IF CUM-MTT-POS3(Y) NOT = ZEROES                                   
      *                                                                         
      *          DISPLAY '09-CUM-POS3, Y = ' CUM-MTT-POS3(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG3(Y)       TO MREG-POS-4                       
                    MOVE CUM-MTT-POS3(Y)    TO MONT-POS-4                       
                 ELSE                                                           
                    MOVE TAB-MREG3(Y)       TO MREG-POS-4B                      
                    MOVE CUM-MTT-POS3(Y)    TO MONT-POS-4B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-NEG3(Y) NOT = ZEROES                                   
      *          DISPLAY '09-CUM-NEG3, Y = ' CUM-MTT-NEG3(Y) '/' Y              
                 IF Y = 1                                                       
                    MOVE TAB-MREG3(Y)       TO MREG-NEG-4                       
                    MOVE CUM-MTT-NEG3(Y)    TO MONT-NEG-4                       
                 ELSE                                                           
                    MOVE TAB-MREG3(Y)       TO MREG-NEG-4B                      
                    MOVE CUM-MTT-NEG3(Y)    TO MONT-NEG-4B                      
                 END-IF                                                         
              END-IF                                                            
              IF CUM-MTT-POS3(Y) NOT = 0  OR CUM-MTT-NEG3(Y) NOT = 0            
                 IF W-PREM-LIG-ENCAISS = '0'                                    
                    MOVE LIGNE-DETAIL-4       TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE '1'                  TO W-PREM-LIG-ENCAISS             
                    MOVE SPACES               TO MREG-NEG-4 MREG-POS-4          
                    MOVE ZEROES               TO MONT-NEG-4 MONT-POS-4          
                 ELSE                                                           
                    MOVE LIGNE-DETAIL-4B      TO LIGNE-ICX090                   
                    PERFORM EDITER-LIGNE                                        
                    MOVE SPACES              TO MREG-NEG-4B MREG-POS-4B         
                    MOVE ZEROES              TO MONT-NEG-4B MONT-POS-4B         
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-EDITER-TOTAL-ENCAISS.  EXIT.                                         
           EJECT                                                                
      *-----------------------------                                            
       EDITER-LIGNE    SECTION.                                                 
      *-----------------------------                                            
           WRITE ENR-ICX090 FROM LIGNE-ICX090.                                  
           ADD 1 TO CPT-LIG.                                                    
       FIN-EDITER-LIGNE.   EXIT.                                                
           EJECT                                                                
      *                                                                         
      *=================================================================        
      *                                                                         
       BCX090-FIN SECTION.                                                      
      *                                                                         
      *=================================================================        
      *                                                                         
           CLOSE FCX90T ICX090.                                                 
           DISPLAY '*** F I N   N O R M A L E ***'.                             
      *                                                                         
       FIN-BCX090-FIN. EXIT.                                                    
      *                                                                         
      *                                                                         
      *                                                                         
      *-----------------------------------                                      
       PLANTAGE SECTION.                                                        
      *-----------------------------------                                      
      *                                                                         
           PERFORM  FIN-ANORMALE.                                               
           MOVE  'BCX090' TO ABEND-PROG.                                        
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                          
      *                                                                         
       FIN-PLANTAGE. EXIT.                                                      
      *                                                                         
      *                                                                         
      *-----------------------------------                                      
       FIN-ANORMALE SECTION.                                                    
      *-----------------------------------                                      
      *                                                                         
           DISPLAY  '***********************************************'.          
           DISPLAY  '*** INTERRUPTION ANORMALE DU PROGRAMME      ***'.          
           DISPLAY  '***               BCX090                    ***'.          
           DISPLAY  '***********************************************'.          
       FIN-FIN-ANORMALE. EXIT.                                                  
