      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 19/10/2016 0        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF00B.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      * MODIF 20/02/2001 -  DSA047                                     *        
      * AJOUT DU CONTROLE DE LA DEVISE :                               *        
      *  - SI LA DEVISE EST 'FRF' OU 'EUR'      -> ON NE FAIT RIEN     *        
      *  - SI LA DEVISE N'EST PAS RENSEIGNEE    -> ON LA FORCE A 'FRF' *        
      *  - SI LA DEVISE N'EST NI 'FRF' NI 'EUR' -> ON CREE 1 ANOMALIE  *        
      *                                            NON BLOQUANTE       *        
      * LA MODIFICATION EST REFERENCEE PAR 2002 DANS LA COL A          *        
      ******************************************************************        
      ** MODIFICATION POUR MARKETPLACE                                 *        
      ** SIGNET MA0811                                                 *        
      ** MODIFICATION POUR MARKETPLACE                                 *        
      ** TEST MODIFIE ==> SIGNET MA2611                                *        
      ** MODIFICATION POUR MARKETPLACE                                 *        
      ******************************************************************        
      ** LE TEST SUR LE N� IDENT DOIT ETRE EFFECTUE SUR L'ENREG. CAISSE*        
      ** TEST MODIFIE ==> SIGNET MA1702                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCARTBQE  ASSIGN TO FCARTBQE                                  
      *                     FILE STATUS IS ST-CBQ.                              
      *--                                                                       
           SELECT FCARTBQE  ASSIGN TO FCARTBQE                                  
                            FILE STATUS IS ST-CBQ                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FCARTBQE                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FCARTBQE-RECORD          PIC X(80).                                  
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF00B.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                 PIC S9(4)     COMP   VALUE ZERO.               
      *--                                                                       
           05  L                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
2002       05  W-DEVISE-FRF      PIC X(3)             VALUE 'FRF'.              
2002       05  W-DEVISE-EUR      PIC X(3)             VALUE 'EUR'.              
           05  W-SIGNE           PIC S9        COMP-3 VALUE +1.                 
           05  W-MTNET           PIC S9(13)V99 COMP-3 VALUE ZERO.               
           05  ST-CBQ            PIC 99               VALUE ZERO.               
           05  W-ZERO            PIC X(20)            VALUE ALL '0'.            
           05  W-BETDATC         PIC X(8)             VALUE 'BETDATC'.          
MA1702     05  W-NIDENT          PIC X(7)             VALUE SPACES.             
       01  DSECT-CARTBQE.                                                       
           05  CBQ-CODENR         PIC XX.                                       
               88  CBQ-TETE                   VALUE '90'.                       
               88  CBQ-FIN                    VALUE '98'.                       
               88  CBQ-CAISSE                 VALUE '20'.                       
               88  CBQ-DETAIL                 VALUES '10' '12' '14' '17'        
                                                     '50' '52' '54'             
                                                     '57'.                      
               88  CBQ-REMB                   VALUES '35' '36'.                 
               88  CBQ-CREDIT                 VALUES '10' '50' '14' '54'        
                                                     '36'.                      
               88  CBQ-DEBIT                  VALUES '12' '52' '17' '57'        
                                                     '35'.                      
               88  CBQ-TOTAL                  VALUES '92' '94' '96'.            
           05  CBQ-DATE           PIC X(6).                                     
           05  CBQ-NSOCIETE       PIC X(3).                                     
           05  CBQ-NLIEU          PIC X(3).                                     
           05  CBQ-NCAISSE        PIC X(3).                                     
           05  CBQ-NPORTEUR       PIC X(19).                                    
           05  CBQ-TETE-CAISSE.                                                 
               10  CBQ-NIDENT         PIC X(10).                                
               10  CBQ-NLOT           PIC X(7).                                 
               10  CBQ-NREMISE        PIC X(6).                                 
               10  CBQ-FILLER         PIC X(21).                                
               10  FILLER         REDEFINES CBQ-FILLER.                         
                   15  FILLER         PIC X(7).                                 
                   15  CBQ-MTREMB     PIC 9(6)V99.                              
                   15  FILLER         PIC X(6).                                 
           05  CBQ-LIGNE-DETAIL   REDEFINES CBQ-TETE-CAISSE.                    
               10  CBQ-TOPCLUB        PIC X.                                    
               10  CBQ-NAUTOR         PIC X(4).                                 
               10  CBQ-TOPAUTO        PIC X.                                    
               10  CBQ-MTBRUT         PIC 9(10)V99.                             
               10  CBQ-MTCOM          PIC 9(6)V99.                              
               10  CBQ-MTNET          PIC 9(10)V99.                             
2002           10  CBQ-CDEVISE        PIC X(3).                                 
               10  FILLER             PIC X(3).                                 
       01  TETE-CARTBQE.                                                        
           05  FILLER             PIC X(24).                                    
           05  CBQ-DFICORG        PIC X(6).                                     
           05  FILLER             PIC X(50).                                    
       01  FIN-CARTBQE.                                                         
           05  FILLER             PIC X(24).                                    
           05  CBQ-NFICORG        PIC 9(6).                                     
           05  FILLER             PIC X(50).                                    
           COPY FIF000.                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       LINKAGE         SECTION.                                                 
      *                                                                         
       01  L-ORGANISME.                                                         
           05  L-CORGANISME     PIC XXX.                                        
           05  L-FSTATUS        PIC 99.                                         
           05  L-NBRECUS        PIC S9(5)       COMP-3.                         
           05  L-NBREJETS       PIC S9(5)       COMP-3.                         
           05  L-LGIDENT        PIC S999        COMP-3.                         
           05  L-SUFFIXE        PIC X.                                          
           05  L-REPRISE        PIC XXX.                                        
           05  L-DFICORG        PIC X(8).                                       
           05  L-NFICORG        PIC S9(7)       COMP-3.                         
      *                                                                         
       01  L-FIF000          PIC X(192).                                        
      *                                                                         
       01  L-DATE            PIC X(8).                                          
      *                                                                         
       PROCEDURE DIVISION USING L-ORGANISME L-FIF000 L-DATE.                    
      *---------------------------------------                                  
       MODULE-BIF00B                   SECTION.                                 
      *---------------------------------------                                  
           MOVE L-LGIDENT           TO L.                                       
           MOVE L-FIF000            TO DSECT-IF000.                             
           IF L-FSTATUS = 99                                                    
              OPEN INPUT FCARTBQE                                               
              MOVE ST-CBQ           TO L-FSTATUS                                
           END-IF.                                                              
           IF ST-CBQ = 00                                                       
              PERFORM LECTURE-FCARTBQE                                          
           END-IF.                                                              
           IF ST-CBQ = 00                                                       
              PERFORM TRAITEMENT-CARTBQE                                        
           END-IF.                                                              
           IF ST-CBQ = 10                                                       
              CLOSE FCARTBQE                                                    
           END-IF.                                                              
           GOBACK.                                                              
      *---------------------------------------                                  
       TRAITEMENT-CARTBQE              SECTION.                                 
      *---------------------------------------                                  
           PERFORM LECTURE-FCARTBQE                                             
                   UNTIL ST-CBQ = 10                                            
                   OR    CBQ-CAISSE                                             
                   OR    CBQ-DETAIL                                             
                   OR    CBQ-REMB.                                              
           IF ST-CBQ NOT = 10                                                   
              IF CBQ-CAISSE                                                     
                 PERFORM TRAITEMENT-ENTETE-CAISSE                               
              END-IF                                                            
              IF CBQ-DETAIL                                                     
              OR CBQ-REMB                                                       
                 PERFORM TRAITEMENT-FICHIER-CBQ                                 
                 ADD  1                TO L-NBRECUS                             
                 MOVE DSECT-IF000      TO L-FIF000                              
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       TRAITEMENT-ENTETE-CAISSE        SECTION.                                 
      *---------------------------------------                                  
           MOVE SPACES              TO IF000-CODANO.                            
      *    MOVE L-DATE              TO IF000-DFINANCE.                          
           MOVE CBQ-DATE            TO GFAMJ-1.                                 
           MOVE '7'                 TO GFDATA.                                  
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DREMISE                         
           ELSE                                                                 
              MOVE '05'                TO IF000-CODANO                          
              MOVE CBQ-DATE            TO IF000-DREMISE                         
           END-IF.                                                              
           MOVE SPACES              TO IF000-NIDENT.                            
MA1702     MOVE SPACES              TO W-NIDENT.                                
           MOVE CBQ-NIDENT (1:L)    TO IF000-NIDENT.                            
           IF IF000-CODANO = SPACES                                             
              IF CBQ-NIDENT (1:L) NOT > W-ZERO (1:L)                            
              OR CBQ-NIDENT (1:L) NOT NUMERIC                                   
                 MOVE '01'                TO IF000-CODANO                       
MA1702        ELSE                                                              
MA1702           MOVE CBQ-NIDENT (1:L)    TO W-NIDENT                           
              END-IF                                                            
           END-IF.                                                              
           MOVE CBQ-NREMISE         TO IF000-NREMISE.                           
           PERFORM LECTURE-FCARTBQE                                             
                   UNTIL ST-CBQ = 10                                            
                   OR    CBQ-DETAIL                                             
                   OR    CBQ-REMB.                                              
      *---------------------------------------                                  
       TRAITEMENT-FICHIER-CBQ          SECTION.                                 
      *---------------------------------------                                  
           MOVE CBQ-DATE            TO GFAMJ-1.                                 
           MOVE '7'                 TO GFDATA.                                  
           PERFORM APPEL-BETDATC.                                               
           IF GFVDAT = '1'                                                      
              MOVE GFSAMJ-0            TO IF000-DOPER                           
              MOVE GFSAMJ-0            TO IF000-DFINANCE                        
           ELSE                                                                 
              MOVE CBQ-DATE            TO IF000-DOPER                           
              MOVE CBQ-DATE            TO IF000-DFINANCE                        
              IF IF000-CODANO = SPACES                                          
                 MOVE '02'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
MA1702     IF W-NIDENT = '4767191'                                              
MA2611       MOVE 'E'               TO IF000-CTYPMVT                            
MA2611     ELSE                                                                 
MA2611       MOVE '*'               TO IF000-CTYPMVT                            
MA2611     END-IF.                                                              
MA2611     MOVE CBQ-NPORTEUR        TO IF000-NPORTEUR.                          
MA2611*    MOVE '*'                 TO IF000-CTYPMVT.                           
           MOVE 1                   TO IF000-NFACTURES.                         
           IF CBQ-CREDIT                                                        
              MOVE +1               TO W-SIGNE                                  
           END-IF.                                                              
           IF CBQ-DEBIT                                                         
              MOVE -1               TO W-SIGNE                                  
           END-IF.                                                              
           IF CBQ-DETAIL                                                        
              COMPUTE IF000-MTBRUT = CBQ-MTBRUT * W-SIGNE                       
              COMPUTE IF000-MTCOM  = CBQ-MTCOM * W-SIGNE                        
              COMPUTE IF000-MTNET  = CBQ-MTNET * W-SIGNE                        
           ELSE                                                                 
MA1702*       MOVE CBQ-NIDENT          TO IF000-NIDENT                          
MA1702        MOVE W-NIDENT            TO IF000-NIDENT                          
              COMPUTE IF000-MTBRUT  = CBQ-MTREMB * W-SIGNE                      
              MOVE ZERO                TO IF000-MTCOM                           
              COMPUTE IF000-MTNET   = CBQ-MTREMB * W-SIGNE                      
           END-IF.                                                              
      *                                                                         
2002       PERFORM CONTROLE-DEVISE.                                             
           PERFORM CONTROLE-REJETS.                                             
      *                                                                         
      *---------------------------------------                                  
2002   CONTROLE-DEVISE                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
2002       IF CBQ-CDEVISE = SPACES THEN                                         
2002          MOVE W-DEVISE-FRF TO IF000-CDEVISE                                
2002       ELSE                                                                 
2002          MOVE CBQ-CDEVISE TO IF000-CDEVISE                                 
2002       END-IF.                                                              
2002  *                                                                         
2002   FIN-CONTROLE-DEVISE.              EXIT.                                  
      *                                                                         
      *---------------------------------------                                  
       CONTROLE-REJETS                 SECTION.                                 
      *---------------------------------------                                  
           IF IF000-CODANO = SPACES                                             
              IF IF000-DFINANCE < IF000-DOPER                                   
                 MOVE '04'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-DREMISE < IF000-DOPER                                    
                 MOVE '06'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-NREMISE NOT > '000000'                                   
                 MOVE '07'                TO IF000-CODANO                       
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTBRUT = ZERO                                            
                 MOVE '08'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              IF IF000-MTNET = ZERO                                             
                 MOVE '09'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
           IF IF000-CODANO = SPACES                                             
              COMPUTE W-MTNET = IF000-MTBRUT - IF000-MTCOM                      
              IF IF000-MTNET NOT = W-MTNET                                      
                 MOVE '10'             TO IF000-CODANO                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
2002       IF IF000-CDEVISE = W-DEVISE-FRF OR                                   
2002          IF000-CDEVISE = W-DEVISE-EUR THEN                                 
2002          NEXT SENTENCE                                                     
2002        ELSE                                                                
2002          IF IF000-CODANO = SPACES                                          
2002             MOVE '33'             TO IF000-CODANO                          
2002             MOVE 'C'              TO IF000-TYPANO                          
2002          END-IF                                                            
2002       END-IF.                                                              
      *                                                                         
      *---------------------------------------                                  
       LECTURE-FCARTBQE                SECTION.                                 
      *---------------------------------------                                  
           READ FCARTBQE INTO DSECT-CARTBQE.                                    
           MOVE ST-CBQ              TO L-FSTATUS.                               
           IF ST-CBQ = 00                                                       
              IF CBQ-TETE                                                       
                 MOVE DSECT-CARTBQE    TO TETE-CARTBQE                          
                 MOVE CBQ-DFICORG      TO L-DFICORG                             
              END-IF                                                            
              IF CBQ-FIN                                                        
                 MOVE DSECT-CARTBQE    TO FIN-CARTBQE                           
                 MOVE CBQ-NFICORG      TO L-NFICORG                             
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       APPEL-BETDATC                   SECTION.                                 
      *---------------------------------------                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
