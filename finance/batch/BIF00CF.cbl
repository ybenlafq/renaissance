      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.     BIF00CF.                                                 
       AUTHOR.          DSA047. AD. APSIDE.                                     
      *                                                                         
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : NOUVEAU FORMAT POUR LE FICHIER DES FINANCEMENTS *        
      *                DE SOFINCO                                      *        
      *  PROGRAMME   : BIF00CF                                         *        
      *  CREATION    : 18/04/2002                                      *        
      *  FONCTION    : REFORMATAGE DU NOUVEAU FICHIER ENVOYE PAR       *        
      *                SOFINCO POUR L'INTEGRER DANS L'ANCIEN FORMAT    *        
      *                                                                *        
      ******************************************************************        
      * MODIF AD - 21/05/02 - ON MET DANS TOUS LES CAS 0 DANS DANS     *        
      *                       LE MONTANT VERSE ( AD02 EN COL 1 )       *        
      ******************************************************************        
      * MODIF AD - 11/06/02 - LE NOUVEAU FICHIER A ETE AGRANDI POUR    *        
      *                       QUE LE NOM DU CLIENT SOIT RECUPERE       *        
      *                       ( 1106 EN COL 1 )                        *        
      ******************************************************************        
      * MODIF AD - 29/07/03 - CORRECTION                               *        
      *                       LE COMPTEUR D'ENREGISTREMENT N'EST PAS   *        
      *                       INITIALISE EN FIN DE FICHIER             *        
      *                       ( 2907 EN COL 1 )                        *        
      ******************************************************************        
      * MODIF OA - 28/11/05 - ADAPTATION                               *        
      *                     1 LES DOSSIERS DE CREDIT ETANT SAISIS SUR  *        
      *                       UN EXTRANET SOFINCO, PRISE EN COMPTE DU  *        
      *                       NOUVEAU CODE ORIGINE ( OAL EN COL 1 )    *        
      *                     2 LES N� DE FACTURE (=DBC) SONT SUR 7, N�  *        
      *                       DE VENTE, ON COMBLE AVEC XX POUR PASSER  *        
      *                       A 9                                      *        
      ******************************************************************        
      * MODIF EC - 16/02/09 - ADAPTATION                               *        
      *                       ON TRAITE LES LIGNES D'ANNULATION DE     *        
      *                       CREDIT                                   *        
      *                       ( 0209 EN COL 1 )                        *        
      * MODIF MA - 20/10/15 - RAJOUT D'UN FICHIER PARAMETRE POUR TRAITER        
      *                       LA LOA. SIGNET MA2010.                   *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FSOFNEW     ASSIGN TO FSOFNEW.                                
      *--                                                                       
           SELECT FSOFNEW     ASSIGN TO FSOFNEW                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FSOFINCO    ASSIGN TO FSOFINCO.                               
      *--                                                                       
           SELECT FSOFINCO    ASSIGN TO FSOFINCO                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
MA2010*    SELECT FPARAM      ASSIGN TO FPARAM.                                 
      *--                                                                       
           SELECT FPARAM      ASSIGN TO FPARAM                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *----FICHIER FSOFNEW ( ENTREE )                                           
       FD  FSOFNEW                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
1106  *01  FSOFNEW-RECORD           PIC X(140).                                 
1106   01  FSOFNEW-RECORD           PIC X(200).                                 
      *                                                                         
      *----FICHIER FSOFINCO ( SORTIE )                                          
       FD  FSOFINCO                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FSOFINCO-RECORD          PIC X(120).                                 
      *                                                                         
MA2010*----FICHIER FPARAM   ( ENTREE )                                          
       FD  FPARAM                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
MA2010 01  FPARAM-RECORD            PIC X(080).                                 
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
       01  W-MONTANT                  PIC 9(7)V99.                              
       01  W-CPT-ENR                  PIC 9(8) VALUE 0.                         
      * NFACTURE SOFINCO (= BDC DARTY) 12 CAR.                                  
      * -> UNIQUEMENT N� DE VENTE SUR 7                                         
       01  W-FAC.                                                               
           05  W-FAC-NUMERO           PIC X(07).                                
           05  W-FAC-VIDE             PIC X(05).                                
      * LETTRAGE (= BDC DARTY)                                                  
      * -> IL FAUT LE NUM DE LIEU SUR DEUX POSITIONS...                         
      *    OU AUTRE CHOSE A DEFAUT !!!                                          
       01  W-BDC.                                                               
           05  W-BDC-MAG              PIC X(02) VALUE 'XX'.                     
           05  W-BDC-NUMERO           PIC X(07).                                
           05  FILLER                 PIC X(03) VALUE SPACES.                   
      *                                                                         
       01  DSECT-SOFNEW.                                                        
 01        05  NEW-CODENR             PIC XXX.                                  
               88  NEW-TETE           VALUE 'AAA'.                              
               88  NEW-FIN            VALUE 'ZZZ'.                              
 04        05  NEW-DTRAITEMENT        PIC X(08).                                
 12        05  NEW-NVENDEUR           PIC X(11).                                
 23        05  NEW-DACHAT             PIC X(08).                                
 31        05  NEW-ORIGINE            PIC X.                                    
 32        05  NEW-MTT-DEB-ACH-CPT    PIC 9(7)V99.                              
 41        05  NEW-MTT-CRE-ACH-CPT    PIC 9(7)V99.                              
 50        05  NEW-MTT-DEB-ACH-CRE    PIC 9(7)V99.                              
 59        05  NEW-MTT-CRE-ACH-CRE    PIC 9(7)V99.                              
 68        05  NEW-NCONTRAT           PIC X(11).                                
 79        05  NEW-MTT-ANNUL          PIC 9(7)V99.                              
 88        05  NEW-NCOMMERCANT        PIC X(09).                                
 97        05  NEW-BAREME             PIC X(08).                                
105        05  NEW-NFACTURE           PIC X(12).                                
117        05  NEW-NCARTE             PIC X(19).                                
136        05  NEW-CDEVISE            PIC X(03).                                
139        05  NEW-TOP-ACHAT1         PIC X.                                    
1106  *    05  FILLER                 PIC X.                                    
1106       05  NEW-NOMCLIENT          PIC X(15).                                
1106       05  FILLER                 PIC X(46).                                
      *                                                                         
       01  TETE-SOFNEW.                                                         
           05  NEW-CODE-ENTETE    PIC X(03).                                    
           05  NEW-INDENT-SOC     PIC X(08).                                    
           05  NEW-INDENT-FIC     PIC X(12).                                    
           05  NEW-NUM-SEQ        PIC 9(08).                                    
           05  NEW-DCREAT         PIC X(08).                                    
           05  NEW-DTRAIT         PIC X(08).                                    
           05  FILLER             PIC X(93).                                    
       01  FIN-SOFNEW.                                                          
           05  NEW-CODE-FIN       PIC X(03).                                    
           05  NEW-INDENT-SOC-FIN PIC X(08).                                    
           05  NEW-INDENT-FIC-FIN PIC X(08).                                    
           05  NEW-NFICORG        PIC 9(8).                                     
           05  FILLER             PIC X(109).                                   
      *                                                                         
       01  DSECT-SOFINCO.                                                       
           05  SOF-CODENR         PIC XXX.                                      
               88  SOF-TETE           VALUE 'AAA'.                              
               88  SOF-FIN            VALUE 'ZZZ'.                              
           05  SOF-NVENDEUR       PIC X(11).                                    
           05  SOF-CENSEIGNE      PIC XX.                                       
           05  SOF-NDOSSIER       PIC X(11).                                    
           05  SOF-NOMCLIENT      PIC X(15).                                    
           05  SOF-BDC            PIC X(12).                                    
           05  SOF-MTACHAT        PIC 9(6)V99.                                  
           05  SOF-MTVERSE        PIC 9(6)V99.                                  
           05  SOF-MTFINANCE      PIC 9(6)V99.                                  
           05  SOF-DACHAT         PIC X(8).                                     
           05  SOF-DFINANCE       PIC X(8).                                     
           05  SOF-RFINANCE       PIC X(7).                                     
           05  SOF-RENSEIGNE      PIC XX.                                       
           05  SOF-CDEVISE        PIC X(03).                                    
           05  SOF-CTYPMVT        PIC X(03).                                    
      *    05  FILLER             PIC X(11).                                    
0209       05  SOF-WANNUL         PIC X(01).                                    
0209       05  FILLER             PIC X(10).                                    
      *                                                                         
       01  TETE-SOFINCO.                                                        
           05  SOF-CODE-ENTETE    PIC X(03).                                    
           05  SOF-INDENT-SOC     PIC X(08).                                    
           05  SOF-INDENT-FIC     PIC X(08).                                    
           05  SOF-NUM-SEQ        PIC 9(08).                                    
           05  SOF-DCREAT         PIC X(08).                                    
           05  SOF-DTRAITEMENT    PIC X(08).                                    
           05  FILLER             PIC X(70).                                    
       01  FIN-SOFINCO.                                                         
           05  SOF-CODE-FIN       PIC X(03).                                    
           05  SOF-INDENT-SOC-FIN PIC X(08).                                    
           05  SOF-INDENT-FIC-FIN PIC X(08).                                    
           05  SOF-NFICORG        PIC 9(8).                                     
           05  FILLER             PIC X(93).                                    
      *                                                                         
       01  FLAG-FSOF               PIC X(01) VALUE 'O'.                         
           88 SUITE-FSOF                     VALUE 'O'.                         
           88 FIN-FSOF                       VALUE 'N'.                         
      *                                                                         
       01  FLAG-FSOFNEW            PIC X(01) VALUE 'O'.                         
           88 SUITE-FSOFNEW                  VALUE 'O'.                         
           88 FIN-FSOFNEW                    VALUE 'N'.                         
      *                                                                         
MA2010 01  FLAG-SOFLOA             PIC X(01) VALUE 'O'.                         
           88 TOP-SOF                        VALUE 'O'.                         
           88 TOP-NSOF                       VALUE 'N'.                         
MA2010 01  W-PARAM                 PIC X(03) VALUE SPACES.                      
      *                                                                         
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
       01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND                                                *          
      *--------------------------------------------------------------*          
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BIF00CF.                                                          
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BIF00CF.                EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '**************************************************'.        
           DISPLAY '*              DEBUT DE BIF00CF                  *' .       
           DISPLAY '**************************************************'.        
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
MA2010     OPEN INPUT   FSOFNEW FPARAM                                          
           OPEN OUTPUT  FSOFINCO                                                
      *                                                                         
      *----LECTURE / VERIFICATION FICHIER FSOFNEW                               
           MOVE 'O'  TO  FLAG-FSOFNEW                                           
           READ  FSOFNEW  INTO  DSECT-SOFNEW                                    
             AT END                                                             
                DISPLAY '*** FSOFNEW VIDE  *** '                                
                DISPLAY '--> RIEN A TRAITER'                                    
                SET FIN-FSOFNEW  TO  TRUE                                       
           END-READ.                                                            
MA2010     READ  FPARAM                                                         
             AT END                                                             
                DISPLAY '************************************'                  
                DISPLAY '*** FPARAM VIDE ARRET TRAITEMENT ***'                  
                PERFORM SORTIE-ANORMALE                                         
           END-READ.                                                            
           MOVE FPARAM-RECORD (1:3) TO W-PARAM                                  
           SET TOP-SOF              TO TRUE                                     
           IF W-PARAM NOT = 'LOA' AND NOT = 'SOF'                               
              DISPLAY '********************************'                        
              DISPLAY '*** FPARAM INCORRECT : ' W-PARAM                         
              DISPLAY '*** VALEURS POSSIBLES LOA OU SOF'                        
              PERFORM SORTIE-ANORMALE                                           
           ELSE                                                                 
              IF W-PARAM = 'LOA'                                                
                 SET TOP-NSOF       TO TRUE                                     
              END-IF                                                            
MA2010     END-IF.                                                              
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *                                                                         
      *----------------------------------------                                 
       MODULE-TRAITEMENT               SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           IF FLAG-FSOFNEW = 'N' THEN                                           
              DISPLAY '***** AUCUNE DEMANDE AUJOURD''HUI *****'                 
           ELSE                                                                 
              PERFORM TRAIT-ENREG                                               
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT.             EXIT.                                 
      *                                                                         
      *****************************************************************         
      *                 M O D U L E - S O R T I E                     *         
      *****************************************************************         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
MA2010     CLOSE FSOFINCO  FSOFNEW FPARAM                                       
           DISPLAY '**************************************************'         
           DISPLAY '*          BIF00CF TERMINE NORMALEMENT           *'         
           DISPLAY '**************************************************'         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '**************************************************'         
           DISPLAY '*         PROGRAMME BIF00CF INTERROMPU           *'         
           DISPLAY '**************************************************'         
           DISPLAY ABEND-MESS                                                   
           MOVE 'BIF00CF'   TO  ABEND-PROG                                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND   USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL  MW-ABEND   USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
      *--------------------------------                                         
       TRAIT-ENREG             SECTION.                                         
      *--------------------------------                                         
      *                                                                         
           PERFORM UNTIL FIN-FSOFNEW                                            
      *                                                                         
              EVALUATE NEW-CODENR                                               
                  WHEN 'AAA'                                                    
      *                ----- ENTETE DU FICHIER                                  
                       MOVE  DSECT-SOFNEW        TO  TETE-SOFNEW                
      *                                                                         
                       MOVE  NEW-CODE-ENTETE     TO  SOF-CODE-ENTETE            
                       MOVE  NEW-INDENT-SOC      TO  SOF-INDENT-SOC             
                       MOVE  NEW-INDENT-FIC(1:8) TO  SOF-INDENT-FIC             
                       MOVE  NEW-NUM-SEQ         TO  SOF-NUM-SEQ                
                       MOVE  NEW-DCREAT          TO  SOF-DCREAT                 
                       MOVE  NEW-DTRAIT          TO  SOF-DTRAITEMENT            
                       WRITE  FSOFINCO-RECORD  FROM  TETE-SOFINCO               
                  WHEN 'BNN'                                                    
      *                ----- JOURNAL DETAILLE DES FINANCEMENTS                  
AD02  *                IF  NEW-ORIGINE  NOT = 'A' THEN                          
AD02  *                IF  NEW-ORIGINE > SPACES AND NOT = 'A' THEN              
0209                   IF  NEW-ORIGINE > SPACES                                 
                          ADD  NEW-MTT-CRE-ACH-CPT                              
                               TO  NEW-MTT-CRE-ACH-CRE                          
                                   GIVING W-MONTANT                             
                          MOVE W-MONTANT            TO  SOF-MTACHAT             
      *                                                                         
                          MOVE  'AH9'  TO  SOF-CODENR                           
      *                                                                         
AD02                      MOVE  0      TO  SOF-MTVERSE                          
      *                                                                         
0209                      MOVE 'N'     TO SOF-WANNUL                            
                          EVALUATE NEW-ORIGINE                                  
                              WHEN '1'                                          
OAL   *                          --- ORIGINE TPE <=> CARTE DARTY                
AD02  *                          ADD  NEW-MTT-DEB-ACH-CPT                       
AD02  *                               TO NEW-MTT-DEB-ACH-CPT                    
AD02  *                          GIVING W-MONTANT                               
AD02  *                          MOVE W-MONTANT      TO  SOF-MTVERSE            
                                 MOVE  'DAR'  TO  SOF-CTYPMVT                   
                              WHEN '2'                                          
OAL   *                          --- ORIGINE MINITEL <=> VENTE A CREDIT         
AD02  *                          MOVE  0      TO  SOF-MTVERSE                   
MA2010*                          MOVE  'SOF'  TO  SOF-CTYPMVT                   
MA2010                           MOVE  W-PARAM TO SOF-CTYPMVT                   
OAL                           WHEN '3'                                          
OAL   *                          --- ORIGINE EXTRANET <=> VENTE A CREDIT        
MA2010*                          MOVE 'SOF'        TO SOF-CTYPMVT               
MA2010                           MOVE  W-PARAM     TO SOF-CTYPMVT               
OAL                              MOVE NEW-NFACTURE TO W-FAC                     
OAL                              IF W-FAC > SPACES                              
OAL                                AND W-FAC-VIDE <= SPACES                     
OAL                                 MOVE W-FAC-NUMERO TO W-BDC-NUMERO           
OAL                                 MOVE W-BDC        TO NEW-NFACTURE           
OAL                              END-IF                                         
0209                          WHEN 'A'                                          
0209  *                          --- ANNULATION D'UN CREDIT                     
0209  *                          SI LA ZONE NFACTURE EST RENSEIGNEE AVEC        
0209  *                           UN NUM DE CARTE ALORS ANNULATION D'UN         
0209  *                           PAIEMENT PAR CARTE DARTY SINON                
0209  *                           ANNULATION D'UN CREDIT                        
0209                             MOVE 'O'          TO SOF-WANNUL                
0209                             MOVE NEW-NFACTURE TO W-FAC                     
0209                             IF W-FAC > SPACES                              
0209                               AND W-FAC-VIDE <= SPACES                     
MA2010*                             MOVE 'SOF'        TO SOF-CTYPMVT            
MA2010                              MOVE W-PARAM      TO SOF-CTYPMVT            
0209                                MOVE W-FAC-NUMERO TO W-BDC-NUMERO           
0209                                MOVE W-BDC        TO NEW-NFACTURE           
0209                             ELSE                                           
0209                                MOVE 'DAR'        TO SOF-CTYPMVT            
0209                             END-IF                                         
0209  *                          MOVE NEW-MTT-DEB-ACH-CRE                       
0609                             MOVE NEW-MTT-DEB-ACH-CPT                       
0209                               TO SOF-MTACHAT                               
MA2010                           IF W-PARAM = 'LOA'                             
                                   MOVE NEW-MTT-DEB-ACH-CRE                     
                                   TO SOF-MTACHAT                               
MA2010                           END-IF                                         
                              WHEN 'R'                                          
      *                          -----REMISE AU PERSONNEL                       
AD02  *                          ADD  NEW-MTT-DEB-ACH-CPT                       
AD02  *                               TO NEW-MTT-DEB-ACH-CPT                    
AD02  *                          GIVING W-MONTANT                               
AD02  *                          MOVE W-MONTANT      TO  SOF-MTVERSE            
                                 MOVE  'DAR'  TO  SOF-CTYPMVT                   
                              WHEN OTHER                                        
                                   DISPLAY 'ORIGINE INCONNUE !!! '              
                                           NEW-ORIGINE                          
                                   STRING 'ORIGINE INCONNUE : '                 
                                          NEW-ORIGINE                           
                                          DELIMITED BY SIZE                     
                                          INTO  ABEND-MESS                      
                                   PERFORM SORTIE-ANORMALE                      
                          END-EVALUATE                                          
      *                                                                         
                          COMPUTE W-MONTANT  =   NEW-MTT-CRE-ACH-CPT            
                                               + NEW-MTT-CRE-ACH-CRE            
                                               - NEW-MTT-DEB-ACH-CPT            
                                               - NEW-MTT-DEB-ACH-CRE            
                          MOVE W-MONTANT         TO  SOF-MTFINANCE              
      *                                                                         
0209                      IF NEW-ORIGINE = 'A'                                  
0209  *                      MOVE NEW-MTT-DEB-ACH-CRE TO SOF-MTFINANCE          
0609                         COMPUTE W-MONTANT = NEW-MTT-DEB-ACH-CPT            
0609                                             - NEW-MTT-CRE-ACH-CPT          
MA2010                       IF W-PARAM = 'LOA'                                 
                                COMPUTE W-MONTANT = NEW-MTT-DEB-ACH-CRE         
                                                  - NEW-MTT-CRE-ACH-CRE         
MA2010                       END-IF                                             
0609                         MOVE W-MONTANT    TO SOF-MTFINANCE                 
0209                      END-IF                                                
      *                                                                         
                          MOVE  NEW-NVENDEUR     TO  SOF-NVENDEUR               
                          MOVE  NEW-NCONTRAT     TO  SOF-NDOSSIER               
                          MOVE  NEW-NFACTURE     TO  SOF-BDC                    
                          MOVE  NEW-DTRAITEMENT  TO  SOF-DFINANCE               
                          MOVE  NEW-DACHAT       TO  SOF-DACHAT                 
1106                      MOVE  NEW-NOMCLIENT    TO  SOF-NOMCLIENT              
1106  *                   MOVE  SPACES           TO  SOF-NOMCLIENT              
                          MOVE  SPACES           TO  SOF-CENSEIGNE              
                                                     SOF-RFINANCE               
                                                     SOF-RENSEIGNE              
                          MOVE  NEW-CDEVISE      TO  SOF-CDEVISE                
                          ADD   1                TO  W-CPT-ENR                  
                          WRITE FSOFINCO-RECORD  FROM DSECT-SOFINCO             
      *                                                                         
                       END-IF                                                   
      *                                                                         
                  WHEN 'ZZZ'                                                    
      *                ----- FIN DU FICHIER                                     
                       MOVE  DSECT-SOFNEW        TO  FIN-SOFNEW                 
      *                                                                         
                       MOVE  NEW-CODE-FIN        TO  SOF-CODE-FIN               
                       MOVE  NEW-INDENT-SOC-FIN  TO  SOF-INDENT-SOC-FIN         
                       MOVE  NEW-INDENT-FIC-FIN(1:8)                            
                             TO  SOF-INDENT-FIC-FIN                             
                       MOVE  W-CPT-ENR           TO  SOF-NFICORG                
                       WRITE  FSOFINCO-RECORD  FROM  FIN-SOFINCO                
2907                   MOVE 0 TO W-CPT-ENR                                      
              END-EVALUATE                                                      
      *                                                                         
      *-------LECTURE ENREG SUIVANT                                             
              READ FSOFNEW  INTO  DSECT-SOFNEW                                  
                AT END                                                          
                   SET FIN-FSOFNEW  TO  TRUE                                    
              END-READ                                                          
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAIT-ENREG.         EXIT.                                           
      *                                                                         
      *                                                                         
      *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*         
      *      C'EST LA FIN DU PROGRAMME : Y A PLUS RIEN APRES          *         
      *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*         
      *                                                                         
      *                                                                         
