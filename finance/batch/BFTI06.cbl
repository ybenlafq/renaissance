      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.BFTI06.                                                       
       AUTHOR.                                                                  
      ***************************************************************           
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *           
      ***************************************************************           
      *                                                                         
      *  PROJET     : CONVERGENCE                                               
      *  PROGRAMME  : BFTI06                                                    
      *  CREATION   : 07/08/27                                                  
      *  FONCTION   :                                                           
      *  - GENERATION DU FICHIER DES ECRITURES COMPTABLES REJETEES              
      *                                                                         
      *  PERIODICITE: JOURNALIER.                                               
      *                                                                         
      *****************************************************************         
0313  * DSA015 - EC - MARS 2013 - SIGNET 0313                         *         
      *  UTILISATION D'UN NOUVEAU TYPE DE TIERS : LE TIERS CPD        *         
      *   ALIMENTATION DE ZONES SPECIFIQUES LIEES A CES TIERS         *         
      *   TAILLE FFTV02 PASSE A 400                                   *         
      *****************************************************************         
0913  * DSA015 - EC - SEPT 2013 - SIGNET 0913                         *         
      *  CREATION EN SORTIE DU FICHIER DES PIECES AYANT CORRECTEMENT  *         
      *   PASSEES L'ICS MAIS AU FORMAT FFTI00 => FICHIER FFTI01       *         
      *****************************************************************         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI00  ASSIGN TO FFTI00.                                    
      *--                                                                       
            SELECT FFTI00  ASSIGN TO FFTI00                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
0913  *     SELECT FFTI01  ASSIGN TO FFTI01.                                    
      *--                                                                       
            SELECT FFTI01  ASSIGN TO FFTI01                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI05  ASSIGN TO FFTI05.                                    
      *--                                                                       
            SELECT FFTI05  ASSIGN TO FFTI05                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTV02  ASSIGN TO FFTV02.                                    
      *--                                                                       
            SELECT FFTV02  ASSIGN TO FFTV02                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTV05  ASSIGN TO FFTV05.                                    
      *--                                                                       
            SELECT FFTV05  ASSIGN TO FFTV05                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FREPRIS ASSIGN TO FREPRIS.                                   
      *--                                                                       
            SELECT FREPRIS ASSIGN TO FREPRIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
       FD   FREPRIS RECORDING F                                                 
                    BLOCK 0 RECORDS                                             
                    LABEL RECORD STANDARD.                                      
      *                                                                         
      *   ENRGISTREMENT REPRISE                                                 
      *                                                                         
       01   FREPRIS-CLE PIC X(23).                                              
      *                                                                         
       FD   FFTI00 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                                       
      *                                                                         
       01   FFTI00-ENR.                                                         
         02 FFTI00-ENREG  PIC X(400).                                           
      *                                                                         
0913   FD   FFTI01 RECORDING F                                                  
"                  BLOCK 0 RECORDS                                              
"                  LABEL RECORD STANDARD.                                       
"     *                                                                         
"      01   FFTI01-ENR.                                                         
0913     02 FFTI01-ENREG  PIC X(400).                                           
      *                                                                         
       FD   FFTI05 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                                       
      *                                                                         
       01   FFTI05-ENR    PIC X(400).                                           
      *                                                                         
       FD   FFTV02 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                                       
      *                                                                         
       01   FFTV02-ENR.                                                         
0313       02  FFTV02-ENREG   PIC X(400).                                       
      *                                                                         
       FD   FFTV05 RECORDING F BLOCK 0 RECORDS                                  
                    LABEL RECORD STANDARD.                                      
      *****************************************************************         
      *        FICHIER DES MOUVEMENTS COMPTABLES ISSUS DES INTERFACES *         
      *        - NOM  = FFTV05                                        *         
      *                            RECSIZE =  200.200                 *         
      *****************************************************************         
0313   01   FFTV05-ENR PIC X(400).                                              
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
          COPY SYBWDIV0.                                                        
      *                                                                         
       01  ETAT-FREPRIS        PIC X VALUE '1'.                                 
           88 FIN-FREPRIS      VALUE '0'.                                       
           88 NON-FIN-FREPRIS  VALUE '1'.                                       
      *                                                                         
       01  ETAT-FFTI00         PIC X VALUE '1'.                                 
           88 FIN-FFTI00       VALUE '0'.                                       
           88 NON-FIN-FFTI00   VALUE '1'.                                       
      *                                                                         
       01  ETAT-FFTV02         PIC X VALUE '1'.                                 
           88 FIN-FFTV02       VALUE '0'.                                       
           88 NON-FIN-FFTV02   VALUE '1'.                                       
      *                                                                         
       01  W-ERREUR       PIC X VALUE SPACE.                                    
       01  W-DATTRANS     PIC X(8).                                             
       01  W-FFTI00-CLE   PIC X(23).                                            
       01  W-FFTV02-CLE   PIC X(23).                                            
      **                                                                        
       01 NOM-MODULE            PIC X(8).                                       
      **                                                                        
      * ZONE DE RUPTURE                                                         
      *                                                                         
       01  DTRAIT-PREC     PIC X(08).                                           
       01  CINTERFACE-PREC PIC X(05).                                           
       01  NPIECE-PREC     PIC X(10).                                           
      *                                                                         
      * COMPTEURS                                                               
      *                                                                         
       01  WCPT-FFTI00   PIC 9(06)  VALUE ZEROS.                                
       01  WCPT-FFTV02   PIC 9(06)  VALUE ZEROS.                                
       01  WCPT-FFTI05   PIC 9(06)  VALUE ZEROS.                                
0913   01  WCPT-FFTI01   PIC 9(06)  VALUE ZEROS.                                
       01  WCPT-FFTV05   PIC 9(06)  VALUE ZEROS.                                
       01  WCPT-FREPRIS  PIC 9(06)  VALUE ZEROS.                                
      *                                                                         
      * INDICES                                                                 
      *                                                                         
       01  I         PIC 9(4) VALUE 0.                                          
      *                                                                         
       01  W-TABLEAU.                                                           
           02  TABLEAU-CAISSE   OCCURS 3000.                                    
             03  TAB-CLE          PIC X(23).                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *               C O P Y   D E   C O N T R O L E                 *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           COPY  SYBWDATH.                                                      
           COPY  SYBWDATE.                                                      
           COPY  ABENDCOP.                                                      
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *         ZONES     BUFFER      D'ENTREE   /  SORTIE            *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       01  FILLER    PIC X(16)    VALUE   '*** Z-INOUT ****'.                   
       01  Z-INOUT   PIC X(4096)  VALUE  SPACE.                                 
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *       ZONES     DE      GESTION     DES     ERREURS           *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           COPY SYBWERRO.                                                       
      *                                                                         
       LINKAGE SECTION.                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
           DISPLAY '**********************************************'.            
           DISPLAY '* ICS *                BFTI06                *'.            
           DISPLAY '**********************************************'.            
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
           DISPLAY '**********************************************'.            
           DISPLAY '* ICS *  LE BFTI06 S''EST TERMINE NORMALEMENT *'.           
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
       MODULE-ENTREE SECTION.                                                   
      *                                                                         
           OPEN INPUT  FFTV02 FFTI00 FREPRIS                                    
0913       OPEN OUTPUT FFTV05 FFTI05 FFTI01                                     
           SET NON-FIN-FREPRIS TO TRUE                                          
           SET NON-FIN-FFTI00  TO TRUE                                          
           SET NON-FIN-FFTV02  TO TRUE                                          
           READ FREPRIS AT END                                                  
               DISPLAY '* FICHIER REPRISE VIDE                       *'         
                SET FIN-FREPRIS TO TRUE                                         
           END-READ                                                             
      *                                                                         
      * CHARGEMENT DU FICHIER DES CAISSES EN ERREUR EN TABLE INTERNE            
      *                                                                         
           INITIALIZE W-TABLEAU                                                 
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 3000                         
                   OR FIN-FREPRIS                                               
              MOVE FREPRIS-CLE      TO TAB-CLE (I)                              
              PERFORM LECTURE-FREPRIS                                           
           END-PERFORM.                                                         
           IF NON-FIN-FREPRIS                                                   
              MOVE 'TABLE PIECES REJETEES TROP PETITE' TO ABEND-MESS            
              DISPLAY 'FREPRIS-CLE : ' FREPRIS-CLE                              
              PERFORM ABANDON-PROGRAMME                                         
           END-IF.                                                              
           READ FFTI00 AT END                                                   
               DISPLAY '* FICHIER FFTI00 VIDE                        *'         
                SET FIN-FFTI00 TO TRUE                                          
           END-READ.                                                            
      *                                                                         
      ******************************************************************        
       MODULE-TRAITEMENT SECTION.                                               
      *                                                                         
           PERFORM UNTIL FIN-FFTI00                                             
              SET NON-TROUVE TO TRUE                                            
              MOVE FFTI00-ENREG (233:8)  TO W-FFTI00-CLE(1:8)                   
              MOVE FFTI00-ENREG (1:5)    TO W-FFTI00-CLE(9:5)                   
              MOVE FFTI00-ENREG (6:10)   TO W-FFTI00-CLE(14:10)                 
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 3000                      
                 OR TROUVE OR TAB-CLE (I ) = SPACES                             
                 IF W-FFTI00-CLE = TAB-CLE (I)                                  
                    SET TROUVE TO TRUE                                          
                 END-IF                                                         
              END-PERFORM                                                       
              IF TROUVE                                                         
                 PERFORM ECRITURE-FFTI05                                        
              ELSE                                                              
0913             PERFORM ECRITURE-FFTI01                                        
              END-IF                                                            
              PERFORM LECTURE-FFTI00                                            
           END-PERFORM                                                          
      *                                                                         
           READ FFTV02 AT END                                                   
               DISPLAY '* FICHIER FFTV02 VIDE                        *'         
                SET FIN-FFTV02 TO TRUE                                          
           END-READ                                                             
           PERFORM UNTIL FIN-FFTV02                                             
              MOVE FFTV02-ENREG (43:8)   TO W-FFTV02-CLE(1:8)                   
              MOVE FFTV02-ENREG (13:5)   TO W-FFTV02-CLE(9:5)                   
              MOVE FFTV02-ENREG (18:10)  TO W-FFTV02-CLE(14:10)                 
              SET NON-TROUVE TO TRUE                                            
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 3000                      
                 OR TROUVE OR TAB-CLE (I ) = SPACES                             
                 IF W-FFTV02-CLE = TAB-CLE (I)                                  
                    SET TROUVE TO TRUE                                          
                 END-IF                                                         
              END-PERFORM                                                       
              IF NON-TROUVE                                                     
                 PERFORM ECRITURE-FFTV05                                        
              END-IF                                                            
              PERFORM LECTURE-FFTV02                                            
           END-PERFORM.                                                         
      *                                                                         
      ******************************************************************        
       MODULE-SORTIE SECTION.                                                   
      *                                                                         
0913       CLOSE FFTI00 FFTI05 FFTV02 FFTV05 FREPRIS FFTI01.                    
      *                                                                         
           DISPLAY '*                                            *'.            
           DISPLAY '* FICHIERS EN ENTREE                         *'.            
           DISPLAY '*   ENR. DES INTERFACES CUMULEES .... '                     
                    WCPT-FFTI00 ' *'.                                           
           DISPLAY '*   MOUVEMENTS A COMPTABILISER ...... '                     
                    WCPT-FFTV02 ' *'.                                           
           DISPLAY '*   CLEF DES MOUVEMENTS EN ERREUR ... '                     
                    WCPT-FREPRIS ' *'.                                          
           DISPLAY '* FICHIERS EN SORTIE                         *'.            
           DISPLAY '*   MOUVEMENTS A REPRENDRE .......... '                     
                    WCPT-FFTI05 ' *'.                                           
           DISPLAY '*   MOUVEMENTS A COMPTABILISER ...... '                     
                    WCPT-FFTV05 ' *'.                                           
           DISPLAY '*                                            *'.            
      *                                                                         
      ******************************************************************        
       LECTURE-FREPRIS SECTION.                                                 
      *                                                                         
           ADD 1 TO WCPT-FREPRIS                                                
           READ FREPRIS AT END                                                  
                SET FIN-FREPRIS TO TRUE                                         
           END-READ.                                                            
      *                                                                         
       LECTURE-FFTI00 SECTION.                                                  
      *                                                                         
           ADD 1 TO WCPT-FFTI00                                                 
           READ FFTI00 AT END                                                   
                SET FIN-FFTI00 TO TRUE                                          
           END-READ.                                                            
      *                                                                         
       LECTURE-FFTV02 SECTION.                                                  
      *                                                                         
           ADD 1 TO WCPT-FFTV02                                                 
           READ FFTV02 AT END                                                   
                SET FIN-FFTV02 TO TRUE                                          
           END-READ.                                                            
      *                                                                         
       ECRITURE-FFTI05 SECTION.                                                 
      *                                                                         
           ADD 1 TO WCPT-FFTI05                                                 
           MOVE FFTI00-ENR TO FFTI05-ENR                                        
           WRITE FFTI05-ENR.                                                    
      *                                                                         
0913   ECRITURE-FFTI01 SECTION.                                                 
"     *                                                                         
"          ADD 1 TO WCPT-FFTI01                                                 
"          MOVE FFTI00-ENR TO FFTI01-ENR                                        
0913       WRITE FFTI01-ENR.                                                    
      *                                                                         
       ECRITURE-FFTV05 SECTION.                                                 
      *                                                                         
           ADD 1 TO WCPT-FFTV05                                                 
           MOVE FFTV02-ENREG TO FFTV05-ENR                                      
           WRITE FFTV05-ENR.                                                    
      *                                                                         
      ******************************************************************        
      ***  PROCEDURE DE PLANTAGE                                                
      ******************************************************************        
       ABANDON-PROGRAMME SECTION.                                               
      *-----------------                                                        
0913       CLOSE FFTI00 FFTV02 FREPRIS FFTI05 FFTV05 FFTI01.                    
           MOVE 'BFTI06' TO ABEND-PROG.                                         
           MOVE 'ABEND'    TO NOM-MODULE.                                       
           DISPLAY ' '.                                                         
           DISPLAY ABEND-PROG ' : FIN ANORMALE...'.                             
           DISPLAY ABEND-MESS.                                                  
           CALL NOM-MODULE    USING ABEND-PROG ABEND-MESS.                      
