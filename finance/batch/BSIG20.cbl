      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BSIG20.                                     
       AUTHOR. A-PHILIPPO   DSA005.                                             
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : SIGAGIP/CS                                      *        
      *  PROGRAMME   : BSIG20                                          *        
      *  CREATION    : 22/09/1997                                      *        
      *  OBJET       : INTERFACE PAIE SIGAGIP/CS - GCT                 *        
      *                -------------------------------                 *        
      *  FONCTION    : TRANSFORMATION DES MONTANTS DU FICHIER D'EXPLO  *        
      *                DE X(13) EN COMP-3 POUR POUVOIR FAIRE DU        *        
      *                SUM.                                            *        
      *                                                                *        
      *  FICHIER ENTREE : FEXPLO  LG = 80                              *        
      *                                                                *        
      *  FICHIER SORTIE : FSIG20  LG = 80                              *        
      *     --> FICHIER PRIS EN ENTREE DU TRI/SUM AVANT LE BSIG20      *        
      *                                                                *        
      *----------------------------------------------------------------*        
      * MODIF AP DSA005 - LE 26/03/99                                           
      * AJOUT D'UNE ZONE DANS LE FICHIER FEXPLO : NATURE DE CONTRAT             
      * ==> MODIF DANS DEFINITION DE FEXPLO                                     
      *  - ZONE "CONTRAT" RENOMMEE EN "TYPCON"                                  
      *  - CREATION ZONE "NATCON"                                               
      * ==> MODIF DANS DEFINITION DE FSIG20                                     
      *  - ZONE "CONTRAT" RENOMMEE EN "TYPCON"                                  
      *  - CREATION ZONE "NATCON"                                               
      * FSIG20-NATCON ALIMENTEE PAR FEXPLO-NATCON                               
      *----------------------------------------------------------------*        
      * MODIF AP DSA005 - LE 02/11/99                                           
      * SI LE MONTANT SALARIAL ET LE MONTANT PATRONAL SONT TOUS LES 2           
      * A ZERO, ON N'ECRIT PAS LE MOUVEMENT DANS LE FICHIER DE SORTIE           
      * MAIS DISPLAY DANS LA SYSOUT                                             
      * ==> EVITER DES PLANTAGES DANBS LE BSIG25 POUR CAUSE D'UNE               
      * RUBRIQUE NON PARAMETREE ALORS QUE LES 2 MONTANTS SONT A 0               
      *     MODIF REFERENCEE PAR 11/99 EN COL 1                                 
      *----------------------------------------------------------------         
      * MODIF AP DSA005 - 04/06/03                                              
      * SUPPRESSION ACCES AUX TABLES DE HR V1 ==> PLUS DE CONTROLE DB2          
      * ==> MODIF REF 'SAV' EN COL 1                                            
      *----------------------------------------------------------------         
      * MODIF AP DSA005 - 26/12/03                                              
      * DDME WAI CHENG : AJOUT MATRICULE EN FIN DE FICHIER                      
      * ==> MODIF REF 'MAT' EN COL 1                                            
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FEXPLO      ASSIGN TO FEXPLO.                                 
      *--                                                                       
           SELECT FEXPLO      ASSIGN TO FEXPLO                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FSIG20      ASSIGN TO FSIG20.                                 
      *--                                                                       
           SELECT FSIG20      ASSIGN TO FSIG20                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *----FICHIER D'EXPLO ISSU DE SIGA                                         
       FD  FEXPLO                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FEXPLO-ENREG.                                                        
1          05 FEXPLO-NSOC         PIC X(03).                                    
4          05 FEXPLO-ETABADM      PIC X(08).                                    
12         05 FEXPLO-NETAB        PIC X(03).                                    
15         05 FEXPLO-TYPCON       PIC X(02).                                    
17         05 FEXPLO-ANALYT       PIC X(15).                                    
32         05 FEXPLO-RUBRIQUE     PIC X(03).                                    
35         05 FEXPLO-SENS         PIC X(01).                                    
36         05 FEXPLO-PERIODE      PIC X(04).                                    
40         05 FEXPLO-MONTSAL      PIC X(13).                                    
53         05 FEXPLO-MONTPAT      PIC X(13).                                    
66         05 FEXPLO-WCUMUL       PIC X(01).                                    
67         05 FEXPLO-NATCON       PIC X(02).                                    
69         05 FEXPLO-MATRIC       PIC X(07).                                    
76         05 FEXPLO-PERORIG      PIC X(04).                                    
40         05 FILLER              PIC X(01).                                    
      *-------LG = 80                                                           
      *                                                                         
      *----FICHIER DE SORTIE, AVAEC LES MONTANTS EN COMP-3                      
       FD  FSIG20                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FSIG20-ENREG.                                                        
1          05 FSIG20-NSOC         PIC X(03).                                    
4          05 FSIG20-NETAB        PIC X(03).                                    
7          05 FSIG20-PERIODE      PIC X(04).                                    
11         05 FSIG20-RUBRIQUE     PIC X(03).                                    
14         05 FSIG20-TYPCON       PIC X(02).                                    
16         05 FSIG20-ETABADM      PIC X(08).                                    
24         05 FSIG20-ANALYT       PIC X(15).                                    
39         05 FSIG20-SENS         PIC X(01).                                    
40         05 FSIG20-WCUMUL       PIC X(01).                                    
41         05 FSIG20-MONTSAL      PIC S9(15)V9(2) COMP-3.                       
50         05 FSIG20-MONTPAT      PIC S9(15)V9(2) COMP-3.                       
59         05 FSIG20-NATCON       PIC X(02).                                    
MAT 61     05 FSIG20-MATRIC       PIC X(07).                                    
MAT 68     05 FSIG20-FILLER       PIC X(13).                                    
      *-------LG = 80                                                           
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY ABENDCOP.                                                       
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL                
      *--------------------------------------------------------------*          
           COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
      *                                                                         
      *****COPY WORKDATC.                                                       
      *                                                                         
      *                                                                         
MAT   ***  EXEC SQL INCLUDE SQLCA        END-EXEC.                              
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES AU PROGRAMME                                         
      *--------------------------------------------------------------*          
      *                                                                         
       01 FILLER.                                                               
          02 CPT-FEXPLO         PIC S9(7) COMP-3  VALUE 0.                      
          02 CPT-FSIG20         PIC S9(7) COMP-3  VALUE 0.                      
      *                                                                         
          02 FLAG-FEXPLO        PIC X(01) VALUE '0'.                            
           88 SUITE-FEXPLO                VALUE '0'.                            
           88 FIN-FEXPLO                  VALUE '1'.                            
      *                                                                         
          02 W-MATRIC           PIC X(07).                                      
          02 W-ANALYT           PIC X(15).                                      
          02 W-ANALYT-GROUPE.                                                   
             05 W-ANALYT-NSOC     PIC X(03).                                    
             05 W-ANALYT-NETAB    PIC X(03).                                    
             05 FILLER            PIC X(01).                                    
             05 W-ANALYT-SECTION  PIC X(06).                                    
          02 W-ETABADM            PIC X(08).                                    
      *                                                                         
          02 CALL-UTILITIES.                                                    
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *      05 ABEND    PIC X(08) VALUE 'ABEND   '.                            
      *--                                                                       
             05 MW-ABEND    PIC X(08) VALUE 'ABEND   '.                         
      *}                                                                        
      *                                                                         
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                           
      *--------------------------------------------------------------*          
       01 FILLER   PIC X(16)    VALUE '*** Z-INOUT ****'.                       
       01 Z-INOUT  PIC X(4096)  VALUE SPACE.                                    
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                           
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
           COPY SYKWMONT.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BSIG20.                                                           
      *--------------                                                           
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
      *------------------------------                                           
       MODULE-ENTREE      SECTION.                                              
      *------------------------------                                           
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT  FEXPLO                                                   
                OUTPUT FSIG20.                                                  
      *                                                                         
      *----LECTURE / VERIFICATION FICHIER FEXPLO                                
           MOVE '0'  TO  FLAG-FEXPLO.                                           
           READ FEXPLO                                                          
             AT END                                                             
                DISPLAY '*** FEXPLO VIDE  *** '                                 
                DISPLAY '--> RIEN A TRAITER'                                    
                SET FIN-FEXPLO  TO  TRUE                                        
             NOT END                                                            
                COMPUTE CPT-FEXPLO = CPT-FEXPLO + 1                             
           END-READ.                                                            
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
      *                                                                         
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *--------------------------------                                         
       MODULE-TRAITEMENT       SECTION.                                         
      *--------------------------------                                         
      *                                                                         
           PERFORM UNTIL FIN-FEXPLO                                             
              MOVE FEXPLO-NSOC      TO  FSIG20-NSOC                             
              MOVE FEXPLO-NETAB     TO  FSIG20-NETAB                            
              MOVE FEXPLO-ETABADM   TO  FSIG20-ETABADM                          
              MOVE FEXPLO-TYPCON    TO  FSIG20-TYPCON                           
              MOVE FEXPLO-ANALYT    TO  FSIG20-ANALYT                           
              MOVE FEXPLO-RUBRIQUE  TO  FSIG20-RUBRIQUE                         
              MOVE FEXPLO-SENS      TO  FSIG20-SENS                             
              MOVE FEXPLO-PERIODE   TO  FSIG20-PERIODE                          
              MOVE FEXPLO-WCUMUL    TO  FSIG20-WCUMUL                           
              MOVE FEXPLO-NATCON    TO  FSIG20-NATCON                           
MAT           MOVE FEXPLO-MATRIC    TO  FSIG20-MATRIC                           
              MOVE SPACES           TO  FSIG20-FILLER                           
      *                                                                         
      *-------CONVERSION MONTANT SALARIAL                                       
              IF FEXPLO-MONTSAL > SPACES                                        
                 MOVE FEXPLO-MONTSAL  TO  W-MONT-USER                           
                 PERFORM TRAITEMENT-MONTANT                                     
                 IF W-MONT-NUMERIC = 'N'                                        
                    DISPLAY '   W-MONT-USER = ' W-MONT-USER                     
                    DISPLAY '   W-MONT-FILE = ' W-MONT-FILE                     
                    MOVE 'MONT SALARIAL NON NUMERIQUE' TO ABEND-MESS            
                    PERFORM SORTIE-ANORMALE                                     
                 ELSE                                                           
                    MOVE W-MONT-FILE  TO  FSIG20-MONTSAL                        
                 END-IF                                                         
              ELSE                                                              
                 MOVE 0  TO  FSIG20-MONTSAL                                     
              END-IF                                                            
      *                                                                         
      *-------CONVERSION MONTANT PATRONAL                                       
              IF FEXPLO-MONTPAT > SPACES                                        
                 MOVE FEXPLO-MONTPAT  TO  W-MONT-USER                           
                 PERFORM TRAITEMENT-MONTANT                                     
                 IF W-MONT-NUMERIC = 'N'                                        
                    DISPLAY '   W-MONT-USER = ' W-MONT-USER                     
                    DISPLAY '   W-MONT-FILE = ' W-MONT-FILE                     
                    MOVE 'MONT PATRONAL NON NUMERIQUE' TO ABEND-MESS            
                    PERFORM SORTIE-ANORMALE                                     
                 ELSE                                                           
                    MOVE W-MONT-FILE  TO  FSIG20-MONTPAT                        
                 END-IF                                                         
              ELSE                                                              
                 MOVE 0  TO  FSIG20-MONTPAT                                     
              END-IF                                                            
      *                                                                         
SAV---*----MISE EN COMMENTAIRE 04/06/03                                         
      *-------SI ANALYT A BLANC, RECHERCHE DE LA DERNIERE AFFECTATION           
      *       IF FEXPLO-ANALYT NOT > SPACES                                     
      *          MOVE FEXPLO-MATRIC  TO  W-MATRIC                               
      *          PERFORM SELECT-ZY1K                                            
      *          DISPLAY 'RECHE. SECTION MATRICULE : ' W-MATRIC                 
      *            ' --> ' W-ANALYT                                             
      *          MOVE W-ANALYT       TO  FSIG20-ANALYT                          
      *          MOVE W-ANALYT       TO  W-ANALYT-GROUPE                        
      *                                                                         
      *          IF FEXPLO-NSOC NOT > SPACES AND                                
      *             W-ANALYT-NSOC > SPACES                                      
      *             MOVE W-ANALYT-NSOC  TO  FSIG20-NSOC                         
      *          END-IF                                                         
      *                                                                         
      *          IF FEXPLO-NETAB NOT > SPACES AND                               
      *             W-ANALYT-NETAB > SPACES                                     
      *             MOVE W-ANALYT-NETAB  TO  FSIG20-NETAB                       
      *          END-IF                                                         
      *       END-IF                                                            
      *                                                                         
      *-------SI ETABADM A BLANC, RECHERCHE DE LA DERNIERE AFFECTATION          
      *       IF FEXPLO-ETABADM NOT > SPACES                                    
      *          MOVE FEXPLO-MATRIC  TO  W-MATRIC                               
      *          PERFORM SELECT-ZYAF                                            
      *          DISPLAY 'RECHE. AFFECT. MATRICULE : ' W-MATRIC                 
      *            ' --> ' W-ETABADM                                            
      *          MOVE W-ETABADM      TO  FSIG20-ETABADM                         
      *       END-IF                                                            
SAV---*----FIN MISE EN COMMENTAIRE 04/06/03                                     
      *                                                                         
      *-------ECRITURE FICHIER FSIG20                                           
11/99 *-------NB : UNIQUEMENT SI MONTANT SAL ET/OU PAT <> 0                     
11/99         IF (FSIG20-MONTSAL NOT = 0) OR                                    
11/99            (FSIG20-MONTPAT NOT = 0)                                       
                  WRITE FSIG20-ENREG                                            
                  COMPUTE CPT-FSIG20 = CPT-FSIG20 + 1                           
11/99         ELSE                                                              
11/99            DISPLAY 'REJ: ' FEXPLO-ENREG                                   
11/99         END-IF                                                            
      *                                                                         
      *-------LECTURE ENREG SUIVANT                                             
              READ FEXPLO                                                       
                AT END                                                          
                   SET FIN-FEXPLO  TO  TRUE                                     
                NOT END                                                         
                   COMPUTE CPT-FEXPLO = CPT-FEXPLO + 1                          
              END-READ                                                          
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT.   EXIT.                                           
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                 M O D U L E - S O R T I E                     *         
      *****************************************************************         
      *---------------------------                                              
       MODULE-SORTIE      SECTION.                                              
      *---------------------------                                              
      *                                                                         
      *----FERMETURE DES FICHIERS                                               
           CLOSE  FEXPLO  FSIG20.                                               
      *                                                                         
      *                                                                         
           DISPLAY '********************************************'.              
           DISPLAY '*** PROGRAMME BSIG20 TERMINE NORMALEMENT ***'.              
           DISPLAY '********************************************'.              
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES FEXPLO    : ' CPT-FEXPLO.                  
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE ECRITURES FSIG20   : ' CPT-FSIG20.                  
           DISPLAY '*'.                                                         
           DISPLAY '*'.                                                         
           DISPLAY '********************************************'.              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN .                                                           
      *--                                                                       
           EXIT PROGRAM .                                                       
      *}                                                                        
      *                                                                         
      *---------------------------                                              
       SORTIE-ANORMALE    SECTION.                                              
      *---------------------------                                              
      *----FERMETURE DES FICHIERS                                               
           CLOSE  FEXPLO  FSIG20.                                               
      *                                                                         
           DISPLAY '*************************************'.                     
           DISPLAY '**** PROGRAMME BSIG20 INTERROMPU ****'.                     
           DISPLAY '*************************************'.                     
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES FEXPLO  : ' CPT-FEXPLO.                    
           DISPLAY '*  NBRE ECRITURES FSIG20 : ' CPT-FSIG20.                    
           DISPLAY '*'.                                                         
           DISPLAY '*************************************'.                     
           MOVE 'BSIG20'    TO  ABEND-PROG.                                     
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND   USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL  MW-ABEND   USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.    EXIT.                                            
      *                                                                         
      *--------------------------------------------------------------*          
      *     MODULES D'ABANDON                                                   
      *--------------------------------------------------------------*          
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
       12-COPY SECTION. CONTINUE. COPY SYKPMONT.                                
      *--------------------------------------------------------------*          
      *     MODULES DB2                                                         
      *--------------------------------------------------------------*          
MAT   *AIDA-SQL-04 SECTION. CONTINUE. COPY SYKSRETC.                            
      *                                                                         
      *                                                                         
SAV---*-----MISE EN COMMENTAIRE LE 04/06/03                                     
      *---------------------------                                              
      *SELECT-ZY1K     SECTION.                                                 
      *---------------------------                                              
      *                                                                         
      *    MOVE FUNC-SELECT          TO TRACE-SQL-FUNCTION.                     
      *                                                                         
      *    EXEC SQL SELECT  B.ANALYT                                            
      *               INTO  :W-ANALYT                                           
      *               FROM  ZY1K B, ZY00 A                                      
      *               WHERE A.MATCLE = :W-MATRIC                                
      *                 AND B.NUDOSS = A.NUDOSS                                 
      *                 AND B.DATFIN = (SELECT MAX(C.DATFIN)                    
      *                                 FROM   ZY1K C                           
      *                                 WHERE  C.NUDOSS = B.NUDOSS)             
      *    END-EXEC.                                                            
      *                                                                         
      *    IF SQLCODE = -811                                                    
      *       SET TROUVE  TO  TRUE                                              
      *    ELSE                                                                 
      *       PERFORM TEST-CODE-RETOUR-SQL                                      
      *    END-IF.                                                              
      *    IF NON-TROUVE                                                        
      *       MOVE SPACES  TO  W-ANALYT                                         
      *    END-IF.                                                              
      *                                                                         
      *FIN-SELECT-ZY1K.  EXIT.                                                  
      *                                                                         
      *                                                                         
      *---------------------------                                              
      *SELECT-ZYAF     SECTION.                                                 
      *---------------------------                                              
      *                                                                         
      *    MOVE FUNC-SELECT          TO TRACE-SQL-FUNCTION.                     
      *                                                                         
      *    EXEC SQL SELECT  B.ETABLI                                            
      *               INTO  :W-ETABADM                                          
      *               FROM  ZYAF B, ZY00 A                                      
      *               WHERE A.MATCLE = :W-MATRIC                                
      *                 AND B.NUDOSS = A.NUDOSS                                 
      *                 AND B.DATFIN = (SELECT MAX(C.DATFIN)                    
      *                                 FROM   ZYAF C                           
      *                                 WHERE  C.NUDOSS = B.NUDOSS)             
      *    END-EXEC.                                                            
      *                                                                         
      *    IF SQLCODE = -811                                                    
      *       SET TROUVE  TO  TRUE                                              
      *    ELSE                                                                 
      *       PERFORM TEST-CODE-RETOUR-SQL                                      
      *    END-IF.                                                              
      *    IF NON-TROUVE                                                        
      *       MOVE SPACES  TO  W-ETABADM                                        
      *    END-IF.                                                              
      *                                                                         
      *FIN-SELECT-ZY1K.  EXIT.                                                  
SAV---*-----FIN MISE EN COMMENTAIRE LE 04/06/03                                 
      *                                                                         
