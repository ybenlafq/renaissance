      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BCC007.                                     
       AUTHOR.  DSA042.                                                         
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : CONTROLE DES CAISSES                            *        
      *                                                                *        
      *  FONCTION    : ==> ALIMENTATION DANS UN FICHIER DES REMONTEES  *        
      *                    DE CAISSE SYNTHETIQUE                       *        
      *                ==> MISE EN FORME FICHIER                       *        
      *                                                                *        
      *  FICHIERS ENTREE: FFCC04E                                      *        
      *           SORTIE: FFCC04S                                      *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FFCC04E     ASSIGN TO FFCC04E.                                
      *--                                                                       
           SELECT FFCC04E     ASSIGN TO FFCC04E                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FFCC04S     ASSIGN TO FFCC04S.                                
      *--                                                                       
           SELECT FFCC04S     ASSIGN TO FFCC04S                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *                                                                         
      *----FICHIER EN ENTREE                                                    
      *                                                                         
        FD  FFCC04E                                                             
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD                                               
            DATA RECORD REC-FFCC04E.                                            
        01  REC-FFCC04E           PIC X(100).                                   
      *----FICHIER EN SORTIE                                                    
        FD  FFCC04S                                                             
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD                                               
            DATA RECORD REC-FFCC04S.                                            
        01  REC-FFCC04S           PIC X(100).                                   
      *                                                                         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
           COPY ABENDCOP.                                                       
           COPY SYBWDIV0.                                                       
      *                                                                         
      *                                                                         
        01  REC-FE.                                                             
          02 FE-CMVT             PIC X(03).                                     
          02 FE-NSOCIETE         PIC X(03).                                     
          02 FE-NLIEU            PIC X(03).                                     
          02 FE-DCAISSE          PIC X(08).                                     
0312      02 FE-NCAISSE          PIC X(03).                                     
0312      02 FE-NOPERATEUR       PIC X(07).                                     
0312      02 FE-HEUREDCN         PIC X(06).                                     
          02 FE-CPAICPT          PIC X(03).                                     
          02 FE-MTLOG            PIC S9(13)V9(0002) COMP-3.                     
          02 FE-MTDCN            PIC S9(13)V9(0002) COMP-3.                     
        01  REC-FS.                                                             
          02 FS-CMVT             PIC X(03).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-NSOCIETE         PIC X(03).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-NLIEU            PIC X(03).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-DCAISSE          PIC X(08).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-NCAISSE          PIC X(03).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-NOPERATEUR       PIC X(07).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-HEUREDCN         PIC X(06).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-CPAICPT          PIC X(03).                                     
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-MTLOG            PIC +++++++++9,99.                             
          02 FILLER              PIC X VALUE ';'.                               
          02 FS-MTDCN            PIC +++++++++9,99.                             
          02 FILLER              PIC X VALUE ';'.                               
       01  WS-STAT-FFCC04E            PIC 9(01)           VALUE 0.              
          88 FIN-FFCC04E                                 VALUE 1.               
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01 DATE-COMPILE.                                                         
          05 COMPILE-JJ                  PIC XX.                                
          05 FILLER                      PIC X      VALUE '/'.                  
          05 COMPILE-MM                  PIC XX.                                
          05 FILLER                      PIC XXX    VALUE '/20'.                
          05 COMPILE-AA                  PIC XX.                                
       01 FILLER.                                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CPT-LECT           PIC S9(8) COMP  VALUE 0.                        
      *--                                                                       
          02 CPT-LECT           PIC S9(8) COMP-5  VALUE 0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CPT-ECRT           PIC S9(8) COMP  VALUE 0.                        
      *--                                                                       
          02 CPT-ECRT           PIC S9(8) COMP-5  VALUE 0.                      
      *}                                                                        
      *                                                                         
      *                                                                         
          02 CALL-UTILITIES.                                                    
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *      05 ABEND    PIC X(08) VALUE 'ABEND   '.                            
      *--                                                                       
             05 MW-ABEND    PIC X(08) VALUE 'ABEND   '.                         
      *}                                                                        
             05 BETDATC  PIC X(08) VALUE 'BETDATC '.                            
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                           
      *--------------------------------------------------------------*          
       01 FILLER   PIC X(16)    VALUE '*** Z-INOUT ****'.                       
       01 Z-INOUT  PIC X(4096)  VALUE SPACE.                                    
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                           
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BCC007.                                                           
      *--------------                                                           
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FFCC04E.                         
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
      *------------------------------                                           
       MODULE-ENTREE      SECTION.                                              
      *------------------------------                                           
      *                                                                         
           DISPLAY  '+-----------------------------------------------+'         
           DISPLAY  '!        B C C 0 0 7                            !' 00002400
           DISPLAY  '+-----------------------------------------------+' 00002350
           DISPLAY  '!          PROGRAMME DEBUTE NORMALEMENT         !'.        
           DISPLAY  '!                                               !'.        
           MOVE WHEN-COMPILED               TO Z-WHEN-COMPILED                  
           MOVE Z-WHEN-COMPILED(4:2)        TO COMPILE-JJ                       
           MOVE Z-WHEN-COMPILED(7:2)        TO COMPILE-AA                       
           MOVE Z-WHEN-COMPILED(1:2)        TO COMPILE-MM                       
D          DISPLAY  '! VERSION DU 15/05/2012 - COMPILE DU ' DATE-COMPILE        
D          DISPLAY '            '                                               
      *    RECUPERATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  '!PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2)00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  '!HEURE     :'  WS-HHMMSSCC                         00002400
           DISPLAY  '+-----------------------------------------------+' 00002350
           OPEN INPUT  FFCC04E                                                  
               OUTPUT  FFCC04S.                                                 
      *--- 1ERE LECTURE                                                         
           PERFORM LECTURE-FFCC04E.                                             
      *                                                                         
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
      *                                                                         
      *--------------------------------                                         
       MODULE-TRAITEMENT       SECTION.                                         
      *--------------------------------                                         
      *                                                                         
           MOVE FE-CMVT         TO   FS-CMVT                                    
           MOVE FE-NSOCIETE     TO   FS-NSOCIETE                                
           MOVE FE-NLIEU        TO   FS-NLIEU                                   
           MOVE FE-DCAISSE      TO   FS-DCAISSE                                 
0312       MOVE FE-NCAISSE      TO   FS-NCAISSE                                 
0312       MOVE FE-NOPERATEUR   TO   FS-NOPERATEUR                              
0312       MOVE FE-HEUREDCN     TO   FS-HEUREDCN                                
           MOVE FE-CPAICPT      TO   FS-CPAICPT                                 
           MOVE FE-MTLOG        TO   FS-MTLOG                                   
           MOVE FE-MTDCN        TO   FS-MTDCN.                                  
           PERFORM ECRITURE-FFCC04S.                                            
      *--- LECTURE SUIVANTE                                                     
           PERFORM LECTURE-FFCC04E.                                             
      *                                                                         
       FIN-MODULE-TRAITEMENT.   EXIT.                                           
      *                                                                         
      *                                                                         
      *                                                                         
       ECRITURE-FFCC04S   SECTION.                                              
      *--------------------------                                               
           WRITE REC-FFCC04S FROM REC-FS.                                       
           ADD 1 TO CPT-ECRT.                                                   
       FIN-ECRITURE-FFCC04S. EXIT.                                              
      *                                                                         
      *                                                                         
       LECTURE-FFCC04E   SECTION.                                               
      *--------------------------                                               
           READ FFCC04E INTO REC-FE                                             
             AT END                                                             
                SET FIN-FFCC04E    TO TRUE                                      
             NOT AT END                                                         
                ADD  1             TO CPT-LECT                                  
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FFCC04E. EXIT.                                               
      *                                                                         
      *                                                                         
      *---------------------------                                              
       MODULE-SORTIE      SECTION.                                              
      *---------------------------                                              
      *                                                                         
      *----FERMETURE DES FICHIERS                                               
           CLOSE  FFCC04E FFCC04S.                                              
      *                                                                         
      *                                                                         
           DISPLAY '********************************************'.              
           DISPLAY '*** PROGRAMME BCC007 TERMINE NORMALEMENT ***'.              
           DISPLAY '********************************************'.              
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES          : ' CPT-LECT.                     
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE ECRITURES         : ' CPT-ECRT.                     
           DISPLAY '*'.                                                         
           DISPLAY '********************************************'.              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN .                                                           
      *--                                                                       
           EXIT PROGRAM .                                                       
      *}                                                                        
      *                                                                         
      *---------------------------                                              
       FIN-ANORMALE    SECTION.                                                 
      *---------------------------                                              
      *                                                                         
           DISPLAY '*************************************'.                     
           DISPLAY '**** PROGRAMME BCC007 INTERROMPU ****'.                     
           DISPLAY '*************************************'.                     
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE LECTURES          : ' CPT-LECT.                     
           DISPLAY '*'.                                                         
           DISPLAY '*  NBRE ECRITURES         : ' CPT-ECRT.                     
           DISPLAY '*'.                                                         
           DISPLAY '*************************************'.                     
           MOVE 'BCC007'    TO  ABEND-PROG.                                     
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND   USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL  MW-ABEND   USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
      *                                                                         
       FIN-FIN-ANORMALE.    EXIT.                                               
      *                                                                         
      *--------------------------------------------------------------*          
      *     MODULES D'ABANDON                                                   
      *--------------------------------------------------------------*          
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
      *--------------------------                                               
