      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BIF020.                                                     
       AUTHOR.  C-EST-MAURICE.                                                  
      ******************************************************************        
      *                                                                *        
      *   PROGRAMME D'ECRITURE DES STATISTIQUES DANS LE FICHIER        *        
      *   DESTINE A ETRE RECUPERE PAR UN MICRO                         *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE     ASSIGN TO FDATE                                     
      *                     FILE STATUS IS ST-DATE.                             
      *--                                                                       
           SELECT FDATE     ASSIGN TO FDATE                                     
                            FILE STATUS IS ST-DATE                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIFSTATC  ASSIGN TO FIFSTATC                                  
      *                     FILE STATUS IS ST-CUM.                              
      *--                                                                       
           SELECT FIFSTATC  ASSIGN TO FIFSTATC                                  
                            FILE STATUS IS ST-CUM                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIFSTATI  ASSIGN TO FIFSTATI                                  
      *                     FILE STATUS IS ST-IN.                               
      *--                                                                       
           SELECT FIFSTATI  ASSIGN TO FIFSTATI                                  
                            FILE STATUS IS ST-IN                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIFSTATO  ASSIGN TO FIFSTATO                                  
      *                     FILE STATUS IS ST-OUT.                              
      *--                                                                       
           SELECT FIFSTATO  ASSIGN TO FIFSTATO                                  
                            FILE STATUS IS ST-OUT                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-RECORD.                                                        
           05  FDATE-DATE           PIC X(8).                                   
           05  FILLER               PIC X(72).                                  
       FD  FIFSTATC                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FIFSTATC-RECORD          PIC X(80).                                  
       FD  FIFSTATI                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FIFSTATI-RECORD          PIC X(80).                                  
       FD  FIFSTATO                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FIFSTATO-RECORD          PIC X(80).                                  
       WORKING-STORAGE SECTION.                                                 
       01  WS-BIF020.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                 PIC S9(4)    COMP   VALUE ZERO.                
      *--                                                                       
           05  I                 PIC S9(4) COMP-5   VALUE ZERO.                 
      *}                                                                        
           05  ST-DATE           PIC 99              VALUE ZERO.                
           05  ST-CUM            PIC 99              VALUE ZERO.                
           05  ST-IN             PIC 99              VALUE ZERO.                
           05  ST-OUT            PIC 99              VALUE ZERO.                
           05  W-DLIMITE         PIC X(8)            VALUE SPACES.              
           05  W-BETDATC         PIC X(8)            VALUE 'BETDATC'.           
           COPY FIFSTAT.                                                        
           COPY WORKDATC.                                                       
           COPY ABENDCOP.                                                       
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *---------------------------------------                                  
       MODULE-BIF020                   SECTION.                                 
      *---------------------------------------                                  
           OPEN INPUT FDATE                                                     
                      FIFSTATC                                                  
                      FIFSTATI                                                  
               OUTPUT FIFSTATO.                                                 
           IF ST-DATE NOT = '00'                                                
              MOVE 'PB OPEN FDATE, STATUS = ' TO ABEND-MESS                     
              MOVE ST-DATE                    TO ABEND-CODE                     
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           IF ST-CUM NOT = '00'                                                 
              MOVE 'PB OPEN FIFSTATC, STATUS = ' TO ABEND-MESS                  
              MOVE ST-CUM                        TO ABEND-CODE                  
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           IF ST-IN NOT = '00'                                                  
              MOVE 'PB OPEN FIFSTATI, STATUS = ' TO ABEND-MESS                  
              MOVE ST-IN                         TO ABEND-CODE                  
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           IF ST-OUT NOT = '00'                                                 
              MOVE 'PB OPEN FIFSTATO, STATUS = ' TO ABEND-MESS                  
              MOVE ST-OUT                        TO ABEND-CODE                  
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           READ FDATE.                                                          
           IF ST-DATE NOT = '00'                                                
              MOVE 'PB LECTURE FDATE, STATUS = ' TO ABEND-MESS                  
              MOVE ST-DATE                       TO ABEND-CODE                  
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           MOVE FDATE-DATE          TO GFJJMMSSAA.                              
           MOVE '01'                TO GFJOUR.                                  
           MOVE 3                   TO GFAJOUP.                                 
           MOVE 'B'                 TO GFDATA.                                  
           CALL W-BETDATC USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE GF-MESS-ERR      TO ABEND-MESS                               
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
           MOVE GFSAMJ-0            TO W-DLIMITE.                               
           PERFORM LECTURE-FIFSTATC.                                            
           PERFORM TRAITEMENT-CUMULS UNTIL ST-CUM = 10                          
           PERFORM LECTURE-FIFSTATI.                                            
           PERFORM TRAITEMENT-BIF020 UNTIL ST-IN = 10                           
           CLOSE FDATE                                                          
                 FIFSTATC                                                       
                 FIFSTATI                                                       
                 FIFSTATO.                                                      
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *---------------------------------------                                  
       TRAITEMENT-CUMULS               SECTION.                                 
      *---------------------------------------                                  
           IF IFSTAT-DJOUR NOT < W-DLIMITE                                      
              PERFORM ECRITURE-FIFSTATO                                         
           END-IF.                                                              
           PERFORM LECTURE-FIFSTATC.                                            
      *---------------------------------------                                  
       TRAITEMENT-BIF020               SECTION.                                 
      *---------------------------------------                                  
           PERFORM ECRITURE-FIFSTATO.                                           
           PERFORM LECTURE-FIFSTATI.                                            
      *---------------------------------------                                  
       LECTURE-FIFSTATC                SECTION.                                 
      *---------------------------------------                                  
           READ FIFSTATC INTO DSECT-IFSTAT.                                     
           IF ST-CUM NOT = '00' AND                                             
              ST-CUM NOT = '10'                                                 
              MOVE 'PB LECTURE FIFSTATC, STATUS = ' TO ABEND-MESS               
              MOVE ST-CUM                           TO ABEND-CODE               
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
      *---------------------------------------                                  
       LECTURE-FIFSTATI                SECTION.                                 
      *---------------------------------------                                  
           READ FIFSTATI INTO DSECT-IFSTAT.                                     
           IF ST-IN NOT = '00' AND                                              
              ST-IN NOT = '10'                                                  
              MOVE 'PB LECTURE FIFSTATI, STATUS = ' TO ABEND-MESS               
              MOVE ST-IN                            TO ABEND-CODE               
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
      *---------------------------------------                                  
       ECRITURE-FIFSTATO               SECTION.                                 
      *---------------------------------------                                  
           WRITE FIFSTATO-RECORD FROM DSECT-IFSTAT.                             
           IF ST-OUT NOT = '00'                                                 
              MOVE 'PB ECRITURE FIFSTATO, STATUS = ' TO ABEND-MESS              
              MOVE ST-OUT                            TO ABEND-CODE              
              GO TO ABANDON-PROGRAMME                                           
           END-IF.                                                              
      *---------------------------------------                                  
       ABANDON-PROGRAMME               SECTION.                                 
      *---------------------------------------                                  
           MOVE  'BIF020'  TO    ABEND-PROG.                                    
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
