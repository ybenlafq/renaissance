      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                             
       PROGRAM-ID.       BFF038.                                                
       AUTHOR. A-DURAND  DSA047.                                                
      *****************************************************************         
      *                                                               *         
      *  BBBBBBBB  FFFFFFFFF FFFFFFFFF  0000000  33333333  888888888  *         
      *  BBBBBBBBB FFFFFFFFF FFFFFFFFF 000000000 333333333 888888888  *         
      *  BBB   BBB FFF       FFF       000   000       333 888   888  *         
      *  BBB   BBB FFF       FFF       000  0000      333  888   888  *         
      *  BBBBBBBB  FFFFFF    FFFFFF    000 00000    3333   888888888  *         
      *  BBBBBBBB  FFFFFF    FFFFFF    00000 000    3333   888888888  *         
      *  BBB   BBB FFF       FFF       0000  000      333  888   888  *         
      *  BBB   BBB FFF       FFF       000   000       333 888   888  *         
      *  BBBBBBBBB FFF       FFF       000000000 333333333 888888888  *         
      *  BBBBBBBB  FFF       FFF        0000000  33333333  888888888  *         
      *                                                               *         
      *****************************************************************         
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : GCA - BASES DE RISTOURNES                       *        
      *  PROGRAMME   : BFF038                                          *        
      *  CREATION    : MAI 2001                                        *        
      *  FONCTION    : CREER FICHIER UTILISE PAR LE GENERATEUR D'ETAT.*         
      *                L'ETAT REPREND TOUS LES AVOIRS ET FACTURES GRF, *        
      *                AYANT IMPACTES LES BASES DE RISTOURNES OU NON , *        
      *                AYANT DONN� LIEU � R�TROCESSION.                *        
      ******************************************************************        
      *                  GCA - BASES DE RISTOURNES                    *         
      *                                                               *         
      * ETAT BATCH IFF038                                             *         
      * BASES DE RISTOURNES                                           *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IFF038I ASSIGN TO IFF038I.                                    
      *--                                                                       
           SELECT IFF038I ASSIGN TO IFF038I                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IFF038O ASSIGN TO IFF038O.                                    
      *--                                                                       
           SELECT IFF038O ASSIGN TO IFF038O                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
       FD  IFF038O                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  IFF038O-ENREG.                                                       
           05 IFF038O-ASA   PIC X.                                              
           05 IFF038O-LIGNE PIC X(132).                                         
      *                                                                         
       FD  IFF038I                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  IFF038I-ENREG    PIC X(047).                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES AU PROGRAMME                              *          
      *--------------------------------------------------------------*          
      *                                                                         
      *----ZONE POUR STOCKER LE MONTANT                                         
       01  W-MTT.                                                               
           03  W-MONTANT-REM        PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-DETAIL     PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-A-CONV     PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-CONV       PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-DEVISE     PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-DEV-EUR    PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-MONTANT-SOC        PIC S9(11)V9(2) COMP-3 VALUE 0.             
           03  W-TAUX               PIC S9(03)V9(2) COMP-3 VALUE 0.             
      *                                                                         
      *----VARIABLES L'HEURE ET LA DATE                                         
       01  W-TIME.                                                              
           05 W-HEURE           PIC 9(2).                                       
           05 W-MINUTE          PIC 9(2).                                       
           05 W-SECONDE         PIC 9(2).                                       
           05 FILLER            PIC 9(2).                                       
       01  W-DATE               PIC 9(6).                                       
       01  W-HEURE-COMPLETE.                                                    
           03 W-HEURE-C         PIC 9(02)  VALUE 0.                             
           03 FILLER            PIC X      VALUE 'H'.                           
           03 W-MINUTE-C        PIC 9(02)  VALUE 0.                             
       01  W-DATE-COMPLETE.                                                     
           03 W-JOUR-C          PIC 9(02)  VALUE 0.                             
           03 FILLER            PIC X      VALUE '/'.                           
           03 W-MOIS-C          PIC 9(02)  VALUE 0.                             
           03 FILLER            PIC X(03)  VALUE '/20'.                         
           03 W-ANNEE-C         PIC 9(02)  VALUE 0.                             
       01  W-DATE-CONV.                                                         
           03 W-S               PIC X(02)  VALUE '20'.                          
           03 W-A               PIC 9(02)  VALUE 0.                             
           03 W-M               PIC 9(02)  VALUE 0.                             
           03 W-J               PIC 9(02)  VALUE 0.                             
       01  W-DATE-CONVERTIE     PIC X(08).                                      
      *                                                                         
      *----VARIABLES POUR GERER LES RUPTURES                                    
       01  W-ANC-SOC            PIC X(03)  VALUE SPACES.                        
       01  W-ANC-DEV            PIC X(03)  VALUE SPACES.                        
       01  W-ANC-AUX            PIC X(03)  VALUE SPACES.                        
       01  W-ANC-PREF           PIC X(05)  VALUE SPACES.                        
       01  W-NVX-SOC            PIC X(03)  VALUE SPACES.                        
       01  W-NVX-DEV            PIC X(03)  VALUE SPACES.                        
       01  W-NVX-AUX            PIC X(03)  VALUE SPACES.                        
       01  W-NVX-PREF           PIC X(05)  VALUE SPACES.                        
      *                                                                         
       01  RUPTURE-SOC          PIC X VALUE 'O'.                                
           88 RUPT-SOC                VALUE 'O'.                                
           88 PAS-RUPT-SOC            VALUE 'N'.                                
      *                                                                         
       01  RUPTURE-DEV          PIC X VALUE 'O'.                                
           88 RUPT-DEV                VALUE 'O'.                                
           88 PAS-RUPT-DEV            VALUE 'N'.                                
      *                                                                         
      *----VARIABLES FICHIER                                                    
       01  IFF038I-ENR.                                                         
           03 IFF038I-SOCDEST   PIC X(03)               VALUE SPACES.           
           03 IFF038I-DEVISE    PIC X(03)               VALUE SPACES.           
           03 IFF038I-AUX       PIC X(03)               VALUE SPACES.           
           03 IFF038I-FRS       PIC X(09)               VALUE SPACES.           
           03 IFF038I-MCIVIL    PIC X(14)               VALUE SPACES.           
           03 IFF038I-NSOC      PIC X(03)               VALUE SPACES.           
           03 IFF038I-TX        PIC 9(3)V9(2)           VALUE 0.                
           03 IFF038I-MONTANT   PIC S9(11)V9(2) COMP-3  VALUE 0.                
      *                                                                         
      *----VARIABLES ETATS                                                      
       01  IFF038O-E01.                                                         
           03 FILLER            PIC X(06)  VALUE 'IFF038'.                      
           03 FILLER            PIC X(27)  VALUE SPACES.                        
           03 FILLER            PIC X(20)  VALUE 'ETAT SYNTHETIQUE DE '.        
           03 FILLER            PIC X(10)  VALUE 'RISTOURNES'.                  
           03 FILLER            PIC X(27)  VALUE SPACES.                        
           03 FILLER            PIC X(09)  VALUE 'EDITE LE '.                   
           03 IFF038O-DATE      PIC X(10)  VALUE SPACES.                        
           03 FILLER            PIC X(03)  VALUE ' A '.                         
           03 IFF038O-HEURE     PIC X(05)  VALUE SPACES.                        
           03 FILLER            PIC X(06)  VALUE ' PAGE '.                      
           03 IFF038O-PAGE      PIC Z(04)9 VALUE SPACES.                        
           03 FILLER            PIC X(05)  VALUE SPACES.                        
       01  IFF038O-E02.                                                         
           03 FILLER            PIC X(43)  VALUE SPACES.                        
           03 IFF038O-MCIVIL    PIC X(14)  VALUE SPACES.                        
           03 FILLER            PIC X(75)  VALUE SPACES.                        
       01  IFF038O-E03.                                                         
           03 FILLER            PIC X(08)  VALUE 'SOCIETE '.                    
           03 IFF038O-NSOC      PIC X(03)  VALUE SPACES.                        
           03 FILLER            PIC X(121) VALUE SPACES.                        
       01  IFF038O-E04.                                                         
           03 FILLER            PIC X(19)  VALUE 'SOCIETE RETROCEDEE '.         
           03 IFF038O-SOCDEST   PIC X(03)  VALUE SPACES.                        
           03 FILLER            PIC X(110) VALUE SPACES.                        
       01  IFF038O-E05.                                                         
           03 FILLER            PIC X(50)  VALUE SPACES.                        
           03 FILLER            PIC X(18)  VALUE 'MONTANT REMISE EUR'.          
           03 FILLER            PIC X(20)  VALUE SPACES.                        
           03 LIB-DEVISE        PIC X(15)  VALUE SPACES.                        
           03 NOM-DEVISE        PIC X(03)  VALUE SPACES.                        
           03 FILLER            PIC X(26)  VALUE SPACES.                        
       01  IFF038O-E06.                                                         
           03 FILLER            PIC X(07)  VALUE 'DEVISE '.                     
           03 IFF038O-DEVISE    PIC X(03)  VALUE SPACES.                        
           03 FILLER            PIC X(122) VALUE SPACES.                        
       01  IFF038O-D01.                                                         
           03 FILLER            PIC X(15)  VALUE SPACES.                        
           03 FILLER            PIC X(07)  VALUE ' TOTAL '.                     
           03 IFF038O-AUX       PIC X(03)  VALUE SPACES.                        
           03 FILLER            PIC X(01)  VALUE SPACES.                        
           03 IFF038O-PREFIXE   PIC X(05)  VALUE SPACES.                        
           03 FILLER            PIC X(19)  VALUE SPACES.                        
           03 IFF038O-MONT-EUR  PIC -Z(10)9,99 VALUE SPACES.                    
           03 FILLER            PIC X(20)  VALUE SPACES.                        
           03 IFF038O-MONT-DEV  PIC -Z(10)9,99 BLANK WHEN ZERO.                 
           03 FILLER            PIC X(34)  VALUE SPACES.                        
       01  IFF038O-P01.                                                         
           03 FILLER            PIC X(50)  VALUE SPACES.                        
           03 FILLER            PIC X(18)  VALUE ALL '='.                       
           03 FILLER            PIC X(20)  VALUE SPACES.                        
           03 IFF038O-EGAL      PIC X(18)  VALUE SPACES.                        
           03 FILLER            PIC X(26)  VALUE SPACES.                        
       01  IFF038O-P02.                                                         
           03 FILLER            PIC X(44)  VALUE SPACES.                        
           03 FILLER            PIC X(06)  VALUE 'TOTAL '.                      
           03 IFF038O-TOT-EUR   PIC -Z(10)9,99 VALUE SPACES.                    
           03 FILLER            PIC X(20)  VALUE SPACES.                        
           03 IFF038O-TOT-DEV   PIC -Z(10)9,99 BLANK WHEN ZERO.                 
           03 FILLER            PIC X(26)  VALUE SPACES.                        
       01  IFF038O-P03.                                                         
           03 FILLER            PIC X(36)  VALUE SPACES.                        
           03 FILLER            PIC X(14)  VALUE 'TOTAL SOCIETE '.              
           03 IFF038O-TOT-SOC   PIC -Z(10)9,99 VALUE SPACES.                    
           03 FILLER            PIC X(67)  VALUE SPACES.                        
      *                                                                         
      *----CONTROL POUR L'ETAT DES FICHIERS ET DES VUES                         
       01  ETAT-IFF038I         PIC X VALUE 'O'.                                
           88 FIN-IFF038I             VALUE 'N'.                                
           88 NON-FIN-IFF038I         VALUE 'O'.                                
      *                                                                         
      *----COMPTEURS D'ENREGISTREMENTS                                          
       01 CPT-LU                PIC S9(10) COMP-3    VALUE 0.                   
       01 CPT-ECRIT             PIC  9(02)           VALUE 0.                   
       01 CPT-PAGE              PIC  9(10)           VALUE 0.                   
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER                PIC X(16)   VALUE '*** Z-INOUT ****'.           
       01 Z-INOUT               PIC X(4096) VALUE SPACE.                        
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC                                      *          
      *--------------------------------------------------------------*          
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND                PIC X(8) VALUE 'ABEND'.                         
      *--                                                                       
       01  MW-ABEND                PIC X(8) VALUE 'ABEND'.                      
      *}                                                                        
           COPY WORKDATC.                                                       
       01  BETDATC              PIC X(10) VALUE 'BETDATC'.                      
      *                                                                         
      ******************************************************************        
      * MODULE MEU00 : CONVERSION DE DEVISE                                     
           COPY COMMEU00.                                                       
       01  MEU000               PIC  X(08)          VALUE 'MEU000'.             
       01  W-NBDECIMX.                                                          
           05  W-NBDECIM9       PIC 9.                                          
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BFF038.                                                           
      *-------------------                                                      
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           IF ETAT-IFF038I = 'O' THEN                                           
              PERFORM MODULE-TRAITEMENT                                         
           END-IF                                                               
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BFF038.            EXIT.                                      
      *                                                                         
      *-----------------------------------                                      
       MODULE-ENTREE              SECTION.                                      
      *-----------------------------------                                      
      *                                                                         
           DISPLAY '**************************************************'         
           DISPLAY '*              DEBUT DE BFF038                   *'         
           DISPLAY '**************************************************'         
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT  IFF038I                                                  
           OPEN OUTPUT IFF038O                                                  
      *                                                                         
      *----RECUPERATION DE LA DATE ET DE L'HEURE SYSTEME                        
           DISPLAY '**********************************************'             
           ACCEPT W-TIME FROM TIME.                                             
           ACCEPT W-DATE FROM DATE.                                             
           MOVE W-HEURE  TO W-HEURE-C                                           
           MOVE W-MINUTE TO W-MINUTE-C                                          
           MOVE W-DATE(5:2) TO W-JOUR-C                                         
                               W-J                                              
           MOVE W-DATE(3:2) TO W-MOIS-C                                         
                               W-M                                              
           MOVE W-DATE(1:2) TO W-ANNEE-C                                        
                               W-A                                              
           MOVE W-DATE-CONV TO W-DATE-CONVERTIE                                 
           DISPLAY '* DEBUT DE TRAITEMENT * '  W-DATE(5:2) '/'                  
                   W-DATE(3:2) '/' W-DATE(1:2) ' * '                            
                                        W-HEURE ':' W-MINUTE ' *'               
           DISPLAY '**********************************************'             
           DISPLAY ' '                                                          
           DISPLAY '**********************************************'             
           DISPLAY '*                IFF038                      *'             
           DISPLAY '**********************************************'             
      *                                                                         
      *----PREMIERE LECTURE                                                     
           SET NON-FIN-IFF038I TO TRUE                                          
           PERFORM LECTURE-IFF038I                                              
           IF ETAT-IFF038I = 'N' THEN                                           
              PERFORM ECRITURE-ENTETE-GENERALE                                  
           END-IF                                                               
           PERFORM ALIMENTATION-ANC                                             
           DISPLAY '**********************************************'.            
      *                                                                         
       FIN-MODULE-ENTREE.            EXIT.                                      
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *                                                                         
      *-----------------------------------                                      
       MODULE-TRAITEMENT          SECTION.                                      
      *-----------------------------------                                      
      *                                                                         
           PERFORM UNTIL FIN-IFF038I                                            
              IF W-ANC-SOC NOT = W-NVX-SOC THEN                                 
                 PERFORM RUPTURE-SOCIETE                                        
              ELSE                                                              
                 IF W-ANC-DEV NOT = W-NVX-DEV THEN                              
                    PERFORM RUPTURE-DEVISE                                      
                 ELSE                                                           
                    IF W-ANC-AUX NOT = W-NVX-AUX                                
                       OR W-ANC-PREF NOT = W-NVX-PREF  THEN                     
                       PERFORM RUPTURE-DETAIL                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              MOVE IFF038I-TX  TO W-TAUX                                        
              MULTIPLY IFF038I-MONTANT BY W-TAUX                                
                       GIVING W-MONTANT-REM                                     
              COMPUTE W-MONTANT-REM = W-MONTANT-REM  / 100                      
              ADD W-MONTANT-REM TO W-MONTANT-DETAIL                             
              PERFORM LECTURE-IFF038I                                           
           END-PERFORM                                                          
      *                                                                         
      *----TRAITEMENT DU DERNIER ENREGISTREMENT                                 
           PERFORM POSITIONNEMENT-FLAG                                          
           PERFORM ECRITURE-DETAIL                                              
           PERFORM ECRITURE-PIED-DEVISE                                         
           PERFORM ECRITURE-PIED-SOC.                                           
      *                                                                         
       FIN-MODULE-TRAITEMENT.        EXIT.                                      
      *                                                                         
      *---------------------------                                              
       LECTURE-IFF038I    SECTION.                                              
      *---------------------------                                              
      *                                                                         
           READ IFF038I INTO IFF038I-ENR                                        
              AT END SET FIN-IFF038I TO TRUE                                    
              NOT AT END ADD 1 TO CPT-LU                                        
           END-READ                                                             
      *                                                                         
           MOVE IFF038I-SOCDEST   TO W-NVX-SOC                                  
           MOVE IFF038I-DEVISE    TO W-NVX-DEV                                  
           MOVE IFF038I-AUX       TO W-NVX-AUX                                  
           MOVE IFF038I-FRS(1:1)  TO W-NVX-PREF.                                
      *                                                                         
       FIN-LECTURE-IFF038I. EXIT.                                               
      *                                                                         
      *---------------------------                                              
       ALIMENTATION-ANC   SECTION.                                              
      *---------------------------                                              
      *                                                                         
           MOVE W-NVX-SOC  TO W-ANC-SOC                                         
           MOVE W-NVX-DEV  TO W-ANC-DEV                                         
           MOVE W-NVX-AUX  TO W-ANC-AUX                                         
           MOVE W-NVX-PREF TO W-ANC-PREF.                                       
      *                                                                         
       FIN-ALIMENTATION-ANC.   EXIT.                                            
      *                                                                         
      *---------------------------                                              
       RUPTURE-SOCIETE    SECTION.                                              
      *---------------------------                                              
      *                                                                         
           PERFORM POSITIONNEMENT-FLAG                                          
           PERFORM ECRITURE-DETAIL                                              
           PERFORM ECRITURE-PIED-DEVISE                                         
           PERFORM ECRITURE-PIED-SOC                                            
           PERFORM ALIMENTATION-ANC                                             
           MOVE 0 TO W-MONTANT-DETAIL                                           
           MOVE 0 TO W-MONTANT-DEVISE                                           
           MOVE 0 TO W-MONTANT-DEV-EUR                                          
           MOVE 0 TO W-MONTANT-SOC                                              
           SET RUPT-SOC TO TRUE                                                 
           SET RUPT-DEV TO TRUE.                                                
      *                                                                         
       FIN-RUPTURE-SOCIETE.    EXIT.                                            
      *                                                                         
      *-----------------------------                                            
       RUPTURE-DEVISE       SECTION.                                            
      *-----------------------------                                            
      *                                                                         
           PERFORM POSITIONNEMENT-FLAG                                          
           PERFORM ECRITURE-DETAIL                                              
           PERFORM ECRITURE-PIED-DEVISE                                         
           PERFORM ALIMENTATION-ANC                                             
           MOVE 0 TO W-MONTANT-DETAIL                                           
           MOVE 0 TO W-MONTANT-DEVISE                                           
           MOVE 0 TO W-MONTANT-DEV-EUR                                          
           SET RUPT-DEV TO TRUE.                                                
      *                                                                         
       FIN-RUPTURE-DEVISE.     EXIT.                                            
      *                                                                         
      *-----------------------------                                            
       RUPTURE-DETAIL       SECTION.                                            
      *-----------------------------                                            
      *                                                                         
           PERFORM POSITIONNEMENT-FLAG                                          
           PERFORM ECRITURE-DETAIL                                              
           PERFORM ALIMENTATION-ANC                                             
           MOVE 0 TO W-MONTANT-DETAIL.                                          
      *                                                                         
      *-----------------------------                                            
       POSITIONNEMENT-FLAG  SECTION.                                            
      *-----------------------------                                            
      *                                                                         
           IF RUPTURE-SOC = 'O' THEN                                            
              PERFORM ECRITURE-ENTETE-SOCDEST                                   
              SET PAS-RUPT-SOC TO TRUE                                          
           END-IF                                                               
           IF RUPTURE-DEV = 'O' THEN                                            
              PERFORM ECRITURE-ENTETE-DEVISE                                    
              SET PAS-RUPT-DEV TO TRUE                                          
           END-IF.                                                              
      *                                                                         
       FIN-RUPTURE-DETAIL.     EXIT.                                            
      *                                                                         
      ******************************************************************        
      *                                                                         
      ******************************************************************        
       CALCUL-EURO SECTION.                                                     
      *                                                                         
           COMPUTE COMM-MEU00-PVALE = W-MONTANT-A-CONV *                        
                                     (10 ** W-NBDECIM9)                         
           CALL MEU000 USING COMM-MEU00-APPLI                                   
           IF COMM-MEU00-CODRET < 0                                             
              PERFORM ABEND-MEU000                                              
           ELSE                                                                 
              EVALUATE COMM-MEU00-NBDECIM-S                                     
                WHEN ('0')                                                      
                  MOVE COMM-MEU00-PVALS   TO W-MONTANT-CONV                     
                WHEN ('1')                                                      
                  MOVE COMM-MEU00-PVALS-1 TO W-MONTANT-CONV                     
                WHEN ('2')                                                      
                  MOVE COMM-MEU00-PVALS-2 TO W-MONTANT-CONV                     
               WHEN ('3')                                                       
                 MOVE COMM-MEU00-PVALS-3  TO W-MONTANT-CONV                     
               WHEN ('4')                                                       
                 MOVE COMM-MEU00-PVALS-4  TO W-MONTANT-CONV                     
               WHEN ('5')                                                       
                 MOVE COMM-MEU00-PVALS-5  TO W-MONTANT-CONV                     
               WHEN ('6')                                                       
                 MOVE COMM-MEU00-PVALS-6  TO W-MONTANT-CONV                     
             END-EVALUATE                                                       
           END-IF.                                                              
      *****************************************************************         
      *                 M O D U L E - S O R T I E                     *         
      *****************************************************************         
      *-----------------------------------                                      
       MODULE-SORTIE              SECTION.                                      
      *-----------------------------------                                      
      *                                                                         
           DISPLAY '********************************************'               
           DISPLAY '***       PROGRAMME BFF038 TERMINE       ***'               
           DISPLAY '***       AFFICHAGE DES COMPTEURS        ***'               
           DISPLAY '********************************************'               
           DISPLAY '* NB ENR LUS      : ' CPT-LU                                
           DISPLAY '* NB ENR ECRITS   : ' CPT-ECRIT                             
           DISPLAY '********************************************'               
           CLOSE IFF038I                                                        
           CLOSE IFF038O                                                        
           DISPLAY '**************************************************'         
           DISPLAY '*          BFF038 TERMINE NORMALEMENT            *'         
           DISPLAY '**************************************************'         
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
      *-----------------------------------                                      
       SORTIE-ANORMALE            SECTION.                                      
      *-----------------------------------                                      
           DISPLAY '**************************************************'         
           DISPLAY '*         PROGRAMME BFF038 INTERROMPU            *'         
           DISPLAY '**************************************************'         
           DISPLAY ABEND-MESS                                                   
           MOVE 'BFF038'    TO  ABEND-PROG                                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND   USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL  MW-ABEND   USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
       FIN-SORTIE-ANORMALE.          EXIT.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *     MODULES D'ABANDON                                        *          
      *--------------------------------------------------------------*          
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
      *                                                                         
      ******************************************************************        
      *   LE MODULE BOVIDE S'EST PLANTE !                                       
      ******************************************************************        
       ABEND-MEU000 SECTION.                                                    
           DISPLAY 'CODRET MEU000 = ' COMM-MEU00-CODRET                         
           MOVE COMM-MEU00-LMESS TO ABEND-MESS.                                 
           GO TO SORTIE-ANORMALE.                                               
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-ENTETE-GENERALE      SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
           ADD 1 TO CPT-PAGE                                                    
           MOVE 1 TO IFF038O-ASA                                                
           MOVE W-DATE-COMPLETE TO IFF038O-DATE                                 
           MOVE W-HEURE-COMPLETE TO IFF038O-HEURE                               
           MOVE CPT-PAGE TO IFF038O-PAGE                                        
           MOVE IFF038O-E01 TO IFF038O-LIGNE                                    
           WRITE IFF038O-ENREG                                                  
           MOVE ' ' TO IFF038O-ASA                                              
           MOVE IFF038I-MCIVIL TO IFF038O-MCIVIL                                
           MOVE IFF038O-E02 TO IFF038O-LIGNE                                    
           WRITE IFF038O-ENREG                                                  
           MOVE 0 TO IFF038O-ASA                                                
           MOVE IFF038I-NSOC TO IFF038O-NSOC                                    
           MOVE IFF038O-E03  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG.                                                 
      *                                                                         
       FIN-ECRITURE-ENTETE-GENERALE.    EXIT.                                   
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-ENTETE-SOCDEST       SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
           PERFORM ECRITURE-ENTETE-GENERALE                                     
           MOVE  0              TO IFF038O-ASA                                  
           MOVE  W-ANC-SOC      TO IFF038O-SOCDEST                              
           MOVE  IFF038O-E04    TO IFF038O-LIGNE                                
           WRITE IFF038O-ENREG.                                                 
      *                                                                         
       FIN-ECRITURE-ENTETE-SOCDEST.     EXIT.                                   
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-ENTETE-DEVISE        SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
           MOVE 0 TO IFF038O-ASA                                                
           MOVE W-ANC-DEV    TO IFF038O-DEVISE                                  
           MOVE IFF038O-E06  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG                                                  
           IF W-ANC-DEV NOT = 'EUR' THEN                                        
              MOVE 'MONTANT REMISE ' TO LIB-DEVISE                              
              MOVE W-ANC-DEV         TO NOM-DEVISE                              
           ELSE                                                                 
              MOVE SPACES TO LIB-DEVISE                                         
              MOVE SPACES TO NOM-DEVISE                                         
           END-IF                                                               
           MOVE 0 TO IFF038O-ASA                                                
           MOVE IFF038O-E05  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG.                                                 
      *                                                                         
       FIN-ECRITURE-ENTETE-DEVISE.     EXIT.                                    
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-DETAIL               SECTION.                                   
      *--------------------------------------                                   
           MOVE ' '         TO IFF038O-ASA                                      
           MOVE W-ANC-AUX   TO IFF038O-AUX                                      
           IF W-ANC-PREF = '1' THEN                                             
              MOVE 'BLANC'  TO IFF038O-PREFIXE                                  
           ELSE                                                                 
              MOVE 'BRUN'   TO IFF038O-PREFIXE                                  
           END-IF                                                               
           IF W-ANC-DEV = 'EUR' THEN                                            
              MOVE W-MONTANT-DETAIL    TO IFF038O-MONT-EUR                      
              ADD  W-MONTANT-DETAIL    TO W-MONTANT-DEV-EUR                     
           ELSE                                                                 
              INITIALIZE  COMM-MEU00-APPLI                                      
              MOVE W-ANC-SOC            TO  COMM-MEU00-NSOCIETE                 
              MOVE W-ANC-DEV            TO  COMM-MEU00-CDEVORIG                 
              MOVE 2                    TO  W-NBDECIM9                          
              MOVE W-NBDECIMX           TO  COMM-MEU00-NBDECIM-E                
              MOVE 'EUR'                TO  COMM-MEU00-CDEVDEST                 
              MOVE W-DATE-CONVERTIE     TO  COMM-MEU00-DEFFET                   
              MOVE W-MONTANT-DETAIL     TO  W-MONTANT-A-CONV                    
              PERFORM CALCUL-EURO                                               
              MOVE W-MONTANT-DETAIL     TO  IFF038O-MONT-DEV                    
              ADD  W-MONTANT-DETAIL     TO  W-MONTANT-DEVISE                    
              ADD  W-MONTANT-CONV       TO  W-MONTANT-DEV-EUR                   
              MOVE W-MONTANT-CONV       TO  IFF038O-MONT-EUR                    
           END-IF                                                               
           MOVE IFF038O-D01  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG                                                  
           MOVE 0  TO  IFF038O-MONT-DEV                                         
           MOVE 0  TO  IFF038O-MONT-EUR                                         
           ADD 1 TO CPT-ECRIT.                                                  
      *                                                                         
       FIN-ECRITURE-DETAIL.             EXIT.                                   
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-PIED-DEVISE          SECTION.                                   
      *--------------------------------------                                   
           IF W-ANC-DEV NOT = 'EUR' THEN                                        
              MOVE W-MONTANT-DEVISE     TO  IFF038O-TOT-DEV                     
              MOVE ALL '=' TO IFF038O-EGAL                                      
           END-IF                                                               
           MOVE W-MONTANT-DEV-EUR TO IFF038O-TOT-EUR                            
           ADD  W-MONTANT-DEV-EUR TO W-MONTANT-SOC                              
           MOVE 0 TO IFF038O-ASA                                                
           MOVE IFF038O-P01  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG                                                  
           MOVE ' ' TO IFF038O-ASA                                              
           MOVE IFF038O-P02   TO IFF038O-LIGNE                                  
           WRITE IFF038O-ENREG                                                  
           MOVE SPACES TO IFF038O-EGAL                                          
           MOVE SPACES TO NOM-DEVISE                                            
           MOVE 0 TO IFF038O-TOT-EUR                                            
           MOVE 0 TO IFF038O-TOT-DEV.                                           
      *                                                                         
       FIN-ECRITURE-PIED-DEVISE.        EXIT.                                   
      *                                                                         
      *--------------------------------------                                   
       ECRITURE-PIED-SOC             SECTION.                                   
      *--------------------------------------                                   
           MOVE '-' TO IFF038O-ASA                                              
           MOVE W-MONTANT-SOC TO IFF038O-TOT-SOC                                
           MOVE IFF038O-P03  TO IFF038O-LIGNE                                   
           WRITE IFF038O-ENREG.                                                 
      *                                                                         
       FIN-ECRITURE-PIED-SOC.           EXIT.                                   
      *                                                                         
